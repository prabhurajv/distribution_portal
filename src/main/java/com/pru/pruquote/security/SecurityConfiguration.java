package com.pru.pruquote.security;

import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.access.vote.AuthenticatedVoter;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.access.vote.UnanimousBased;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.expression.WebExpressionVoter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.header.writers.StaticHeadersWriter;
import org.springframework.web.filter.CharacterEncodingFilter;

import com.pru.pruquote.handler.LoginFailureHandler;
import com.pru.pruquote.handler.LoginSuccessHandler;
import com.pru.pruquote.handler.LogoutSuccess;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	DataSource dataSource;
	
	@Autowired
	private SessionRegistry sessionRegistry;
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
		
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//add filter before csrf
		CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        http.addFilterBefore(filter,CsrfFilter.class);

		http.authorizeRequests()
			.antMatchers("/login**")
				.permitAll()
			.antMatchers("/")
				.authenticated()
			.antMatchers("/*")
				.authenticated()
//			.antMatchers("/**")
//				.authenticated()
//			.antMatchers("/webjars**")
//				.permitAll()
//			.antMatchers("/webjars/**")
//				.permitAll()
//			.antMatchers("/resources**")
//				.permitAll()
//			.antMatchers("/resources/**")
//				.permitAll()
			.antMatchers("/forcelogout**")
				.authenticated()
			.antMatchers("/prumyfamily**")
				.authenticated()
			.antMatchers("/prumyfamily/**")
				.authenticated()
			.antMatchers("/newprumyfamily**")
				.authenticated()
			.antMatchers("/edusave**")
				.authenticated()
			.antMatchers("/edusave/**")
				.authenticated()
			.antMatchers("/newedusave**")
				.authenticated()
			.antMatchers("/editedusave**")
				.authenticated()
			.antMatchers("/easylife**")
				.authenticated()
			.antMatchers("/easylife/**")
				.authenticated()
			.antMatchers("/neweasylife**")
				.authenticated()
			.antMatchers("/loanassure**")
				.authenticated()
			.antMatchers("/loanassure/**")
				.authenticated()
			.antMatchers("/newloanassure**")
				.authenticated()
			.antMatchers("/referral**")
				.access("hasAuthority('ACCESS_REFERRAL')")
			.antMatchers("/referral/edit/**")
				.access("hasAuthority('ACCESS_REFERRAL')")
			.antMatchers("/referral/comment/**")
				.access("hasAuthority('ACCESS_REFERRAL')")
			.antMatchers("/referral/detail/**")
				.access("hasAuthority('ACCESS_REFERRAL_MGT')")
			.antMatchers("/managereferral**")
				.access("hasAuthority('ACCESS_REFERRAL_MGT')")
			.antMatchers("/managereferral/**")
				.access("hasAuthority('ACCESS_REFERRAL_MGT')")
			.antMatchers("/newreferral**")
				.access("hasAuthority('ACCESS_REFERRAL')")
//				.accessDecisionManager(accessDecisionManager())
			.antMatchers("/referrer**")
				.access("hasAuthority('ACCESS_REFERRER')")
			.antMatchers("/newreferrer**")
				.access("hasAuthority('ACCESS_REFERRER')")
			.antMatchers("/referrer/edit/**")
				.access("hasAuthority('ACCESS_REFERRER')")
			.antMatchers("/MyPendingCase/**")
				.access("hasAuthority('ACCESS_PENDING')")
			.and()
			.exceptionHandling().accessDeniedPage("/403")
			.and()
			.formLogin()
				.loginPage("/login")
				.usernameParameter("username")
				.passwordParameter("pwd")
				.successHandler(loginSuccessHandler())
				.failureHandler(loginFailureHandler())
			.and()
			.csrf()
			.and()
			.exceptionHandling().accessDeniedPage("/403")
			.and()
			.logout()
				//.logoutRequestMatcher(new AntPathRequestMatcher("/logout")) // use GET request
				.logoutSuccessUrl("/login?logout")
//				.logoutSuccessHandler(logoutSuccessHandler())
//				.logoutUrl("/j_spring_security_logout")
				.invalidateHttpSession(true)
				.deleteCookies("JSESSIONID")
				.permitAll()
			.and()
			.sessionManagement().sessionFixation().migrateSession()
//				.invalidSessionUrl("/login?expired")
				.sessionAuthenticationErrorUrl("/login?expired")
				.maximumSessions(1)
//				.maxSessionsPreventsLogin(true)
				.sessionRegistry(sessionRegistry)
				.expiredUrl("/login?expired=1");
		
		http.headers()
			.httpStrictTransportSecurity()
			.and()
			.xssProtection()
			.xssProtectionEnabled(true)
			.and()
			.addHeaderWriter(new StaticHeadersWriter("HttpOnly", "true"));
	}
	
	@Bean(name = "passwordEncoder")
	public PasswordEncoder passwordEncoder(){
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}
	
	// Work around https://jira.spring.io/browse/SEC-2855
    @Bean(name = "sessionRegistry")
    public SessionRegistry sessionRegistry() {        
        return new SessionRegistryImpl();
    }
    
    @Bean
    public RoleVoter roleVoter(){
    	RoleVoter roleVoter = new RoleVoter();
    	roleVoter.setRolePrefix("");
    	return roleVoter;
    }
    
    @Bean
    public AccessDecisionManager accessDecisionManager(){
    	List<AccessDecisionVoter<? extends Object>> decisionVoters = 
    			Arrays.asList(roleVoter(), new WebExpressionVoter(), new AuthenticatedVoter());
    	
    	return new UnanimousBased(decisionVoters);
    }
    
    @Bean
    public LoginFailureHandler loginFailureHandler() {
    	return new LoginFailureHandler();
    }
    
    @Bean
    public LoginSuccessHandler loginSuccessHandler() {
    	return new LoginSuccessHandler();
    }
    
    @Bean
    public LogoutSuccess logoutSuccessHandler(){
    	return new LogoutSuccess();
    }
	
	/* Equivalent to below xml code written in eapsfront-context.xml
	 *
	 	<http auto-config="true">
			<intercept-url pattern="/agent**" access="USER,ADMIN" />
		</http>

		<authentication-manager>
		  <authentication-provider>
		    <user-service>
				<user name="admin" password="12345" authorities="USER,ADMIN" />
				<user name="test" password="12345" authorities="USER" />
		    </user-service>
		  </authentication-provider>
		</authentication-manager>
	 *
	 */
}
