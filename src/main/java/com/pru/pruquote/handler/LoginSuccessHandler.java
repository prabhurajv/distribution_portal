package com.pru.pruquote.handler;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import com.pru.pruquote.dao.ILogDao;
import com.pru.pruquote.dao.IUserDao;
import com.pru.pruquote.model.User;
import com.pru.pruquote.service.LogService;
import com.pru.pruquote.service.UserService;
import com.pru.pruquote.utility.AnonymousHelper;

public class LoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	@Autowired
	private ILogDao logDao;
	
	@Autowired
	private IUserDao userDao;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private LogService logService;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		String username = auth.getName();
		String action = "login successfully";
		String ip = request.getRemoteHost();
		String pip = request.getRemoteHost();
		Date date = new Date();
		
		logService.logUserAction(username, action, ip, pip, date);
		userService.resetLoginAttemptAndLastLoginDate(username);
		
		User user = userService.getUser(username);
		Date pwdExpiredDate = user.getPwdExpiredDate();
		
		if(pwdExpiredDate != null){
			int days = AnonymousHelper.getLapseDays(pwdExpiredDate.getTime())*(-1);
			if(days>0 && days<=15){			
				request.getSession().setAttribute("isExpiredSoon", "Your password is going to expired in " + (days>1?days +" days!":"today!"));
			}
		}
		
		if(user.isFirstLogin()){
			getRedirectStrategy().sendRedirect(request, response, "/changepwd");
		}else{
			getRedirectStrategy().sendRedirect(request, response, "/");	
		}
		
//		getRedirectStrategy().sendRedirect(request, response, "/");	
	}

}
