package com.pru.pruquote.dao.impl;

import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.dao.ILogDao;
import com.pru.pruquote.model.Log;
import com.pru.pruquote.model.User;
import com.pru.pruquote.model.UserHistory;

@Repository
@Transactional
public class LogDaoImpl implements ILogDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void logUserAction(String username, String action, String ip, String pip, Date date) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		
		Log log = new Log();
		log.setAction(action);
		log.setIp(ip);
		log.setPip(pip);
		log.setCreatedDate(date);
		
		Criteria crit = session.createCriteria(User.class);
		
		Transaction tx = session.beginTransaction();
		
		try{
			crit.add(Restrictions.eq("username", username));
			
			User user = null;
			
			if(crit.list().size() > 0){
				user = (User)crit.list().get(0);
				
				log.setUserIdPk(user.getId());
			}
			
			session.save(log);
			tx.commit();
			
		}catch(HibernateException e) {
			tx.rollback();
			e.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
	}

	@Override
	public void logChangePwd(String username, String action, String oldPwd, String operator, Date date) {
		// TODO Auto-generated method stub
		Session session = null;
		session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		UserHistory history = new UserHistory();
		history.setDescription(action);
		history.setPwd(oldPwd);
		history.setCreatedBy(operator);
		history.setCreatedDate(date);
		
		Criteria crit = session.createCriteria(User.class);
		
		try{
			crit.add(Restrictions.eq("username", username));
			
			User user = null;
			
			if(crit.list().size() > 0){
				user = (User)crit.list().get(0);
				
				history.setUserIdPk(user.getId());
			}
			
			session.saveOrUpdate(history);
			tx.commit();
		}catch(HibernateException e) {
			e.printStackTrace();
			tx.rollback();
		}finally{
			session.flush();
			session.close();
		}
	}

}
