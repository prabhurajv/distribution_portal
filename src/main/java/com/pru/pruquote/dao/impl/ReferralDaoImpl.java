package com.pru.pruquote.dao.impl;

import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.dao.IReferralDao;
import com.pru.pruquote.model.Referral;
import com.pru.pruquote.model.ReferralFollowUpComment;
import com.pru.pruquote.utility.GlobalVar;

@Repository
@Transactional
public class ReferralDaoImpl implements IReferralDao {
	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public int save(Referral referral, String username) {
		Session session = null;
		session = sessionFactory.openSession();
		
		int affectedRow = 0;
		
		Transaction trx = null;
		
		try{
			trx = session.beginTransaction();
			session.save(referral);
			trx.commit();
			
			affectedRow = 1;
		}catch(HibernateException ex){
			if(trx != null) trx.rollback();
			
			ex.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		return affectedRow;
	}

	@Override
	public Referral getReferral(String rid, String username) {
		Session session = null;
		session = sessionFactory.openSession();
		
		Referral referral = new Referral();
		
		Criterion critRid = Restrictions.eq("rID", Long.parseLong(rid));
		Criterion critUsername = Restrictions.eq("trnBy", username);
		
		LogicalExpression andExp = Restrictions.and(critRid, critUsername);
		
		Criteria crit = session.createCriteria(Referral.class);
		crit.add(andExp);
		
		try{
			referral = (Referral)crit.list().get(0);
		}catch(HibernateException ex){
			ex.printStackTrace();
		}catch(IndexOutOfBoundsException ex){
			ex.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		return referral;
	}

	@Override
	public int update(long rid, Referral referral) {
		Session session = null;
		session = sessionFactory.openSession();
		
		int affectedRow = 0;
		Referral mReferral = null;
		
		Transaction trx = null;
		
		try{
			trx = session.beginTransaction();
			
			mReferral = (Referral) session.get(referral.getClass(), rid);
			
			if(mReferral != null){
				mReferral.setAgntNum(referral.getAgntNum());
				mReferral.setAPE(referral.getAPE());
				mReferral.setAppDate(referral.getAppDate());
				mReferral.setClsBankAppNum(referral.getClsBankAppNum());
				mReferral.setClsBankUploadStatus(referral.getClsBankUploadStatus());
				mReferral.setClsBankSoDate(referral.getClsBankSoDate());
				mReferral.setClsBankSoStatus(referral.getClsBankSoStatus());
				mReferral.setClsBankUploadDate(referral.getClsBankUploadDate());
				mReferral.setClsBankUploadRemark(referral.getClsBankUploadRemark());
				mReferral.setCusAFPremium(referral.getCusAFPremium());
				mReferral.setCusAge(referral.getCusAge());
				mReferral.setCusFacebook(referral.getCusFacebook());
				mReferral.setCusMStatus(referral.getCusMStatus());
				mReferral.setCusName(referral.getCusName());
				mReferral.setCusNochd(referral.getCusNochd());
				mReferral.setCusPhone(referral.getCusPhone());
				mReferral.setCusSalaryMonthly(referral.getCusSalaryMonthly());
				mReferral.setCusSalaryYearly(referral.getCusSalaryYearly());
				mReferral.setCusSex(referral.getCusSex());
				mReferral.setCusOcc(referral.getCusOcc());
				mReferral.setCusAgeChd(referral.getCusAgeChd());
				mReferral.setCusPhone2(referral.getCusPhone2());
				mReferral.setCusEmail(referral.getCusEmail());
				
				mReferral.setCusAprStatus(referral.getCusAprStatus());
				mReferral.setCusAprDate(referral.getCusAprDate());
				
				mReferral.setExisCusStatus(referral.getExisCusStatus());
				mReferral.setFlwupCmtRemarkLast(referral.getFlwupCmtRemarkLast());
				mReferral.setFlwupCmtTypeLast(referral.getFlwupCmtTypeLast());
				mReferral.setFlwupTrnDate(referral.getFlwupTrnDate());
				mReferral.setIP(referral.getIP());
				mReferral.setLastTrnBy(SecurityContextHolder.getContext().getAuthentication().getName());
				mReferral.setLastTrnDateTime(new Date());
				mReferral.setOrgAgntNum(referral.getOrgAgntNum());
				mReferral.setOrgBranch(referral.getOrgBranch());
				mReferral.setpIP(referral.getpIP());
				mReferral.setrDate(referral.getrDate());
				mReferral.setReferralBy(referral.getReferralBy());
				mReferral.setrRemark(referral.getrRemark());
				mReferral.setrType(referral.getrType());
				mReferral.setSucAppDate(referral.getSucAppDate());
				mReferral.setSucAppRemark(referral.getSucAppRemark());
				mReferral.setSucAppStatus(referral.getSucAppStatus());
				mReferral.setSalSucSalPreStatus(referral.getSalSucSalPreStatus());
				mReferral.setSalSucSalPreDate(referral.getSalSucSalPreDate());
				mReferral.setValidFlag(referral.getValidFlag());
				mReferral.setSalTypRecPrd(referral.getSalTypRecPrd());
				mReferral.setSalDurPre(referral.getSalDurPre());
				mReferral.setSalRecAffApe(referral.getSalRecAffApe());
				mReferral.setSalMthBasExp(referral.getSalMthBasExp());
				mReferral.setSalRecSA(referral.getSalRecSA());
				mReferral.setSalRecPolTerm(referral.getSalRecPolTerm());
				mReferral.setSalRecPrdPkgUsd(referral.getSalRecPrdPkgUsd());
				mReferral.setSalPolTerm(referral.getSalPolTerm());
				mReferral.setSalEpcClsSal(referral.getSalEpcClsSal());
				mReferral.setSalPolMode(referral.getSalPolMode());
				mReferral.setSalEpcClsPeriod(referral.getSalEpcClsPeriod());
				mReferral.setClsPrdType(referral.getClsPrdType());
				mReferral.setClsPolTerm(referral.getClsPolTerm());
				mReferral.setClsPolMode(referral.getClsPolMode());
				mReferral.setCustomerFrom(referral.getCustomerFrom());
				mReferral.setDraft(referral.getDraft());
//				mReferral.setCdate(referral.getCdate());
				
				session.update(mReferral);
				
				trx.commit();
				
				affectedRow = 1;
			}
			
		}catch(HibernateException ex){
			if(trx != null) trx.rollback();
			ex.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		return affectedRow;
	}

	@Override
	public int saveComment(ReferralFollowUpComment comment) {
		Session session = null;
		session = sessionFactory.openSession();
		
		int affectedRow = 0;
		
		Transaction trx = null;
		
		try{
			trx = session.beginTransaction();
			session.save(comment);
			trx.commit();
			
			affectedRow = 1;
		}catch(HibernateException ex){
			if(trx != null) trx.rollback();
			ex.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		return affectedRow;
	}

	@Override
	public Referral getReferral(String rid) {
		Session session = null;
		session = sessionFactory.openSession();
		
		Referral referral = new Referral();
		
		Criteria crit = session.createCriteria(Referral.class);
		crit.add(Restrictions.eq("rID", Long.parseLong(rid)));
		
		try{
			referral = (Referral)crit.list().get(0);
		}catch(HibernateException ex){
			ex.printStackTrace();
		}catch(IndexOutOfBoundsException ex){
			ex.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		return referral;
	}

	@Override
	public Referral getReferralMgt(String rid, String username) {
		Session session = null;
		session = sessionFactory.openSession();
		
		boolean isValid = false;
		
		Referral referral = new Referral();
		
		Criteria crit = session.createCriteria(Referral.class);
		crit.add(Restrictions.eq("rID", Long.parseLong(rid)));
		
		try{
			referral = (Referral)crit.list().get(0);
			
			if(referral.getrID() > 0){
				Query query = session.createSQLQuery("select * from " + GlobalVar.db_name + ".spsearch_referral_byagentmgt_cnt(:agntnum, :userID)");
				query.setParameter("agntnum", referral.getTrnBy());
				query.setParameter("userID", username);
				
				if(query.list().size()>0){
					isValid = Integer.parseInt(query.list().get(0).toString()) > 0;
				}
				
				if(!isValid){
					referral = new Referral();
				}
			}
		}catch(HibernateException ex){
			ex.printStackTrace();
		}catch(IndexOutOfBoundsException ex){
			ex.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			session.flush();
			session.close();
		}
		
		return referral;
	}

}
