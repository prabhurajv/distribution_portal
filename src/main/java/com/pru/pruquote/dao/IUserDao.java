package com.pru.pruquote.dao;

import com.pru.pruquote.model.User;

public interface IUserDao {
	public User getUser(String username);
	public void setLoginAttempt(User user);
	public int updateUserPwd(User user);
	public boolean isPwdAlreadyUsed(long userIdPk, String newRawPwd);
	public void disableUser();
	public void resetLoginAttemptAndLastLoginDate(String username);
	public void disableUser(User user);
}
