package com.pru.pruquote.dao;

import com.pru.pruquote.model.Referral;
import com.pru.pruquote.model.ReferralFollowUpComment;

public interface IReferralDao {
	public int save(Referral referral, String username);
	public Referral getReferral(String rid, String username);
	public Referral getReferral(String rid);
	public Referral getReferralMgt(String rid, String username);
	public int update(long rid, Referral referral);
	public int saveComment(ReferralFollowUpComment comment);
}
