package com.pru.pruquote.dao;

import java.util.Date;

public interface ILogDao {
	public void logUserAction(String username, String action, String ip, String pip, Date date);
	public void logChangePwd(String username, String action, String oldPwd, String operator, Date date);
}
