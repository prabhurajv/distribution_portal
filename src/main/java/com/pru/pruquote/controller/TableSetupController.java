package com.pru.pruquote.controller;


import java.security.Principal;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.pru.pruquote.model.Discountrate;
import com.pru.pruquote.model.Itemitem;
import com.pru.pruquote.model.Itemtabl;
import com.pru.pruquote.model.Mbrate;
import com.pru.pruquote.model.Modalfactor;
import com.pru.pruquote.model.Premiumrate;
import com.pru.pruquote.model.Surrender;
import com.pru.pruquote.model.UABFactor;
import com.pru.pruquote.service.TableSetupService;

//import dummiesmind.breadcrumb.springmvc.annotations.Link;

@Controller
@PreAuthorize("hasAuthority('ACCESS_TABLESETUP')")
public class TableSetupController {	
	
	@Autowired	
	private TableSetupService tableSetupService;
	
	@ModelAttribute("itemtable")
    public Itemtabl Constructor(){
    	return new Itemtabl();
    }
	
	@ModelAttribute("itemitem")
    public Itemitem itemConstructor(){
    	return new Itemitem();
    }
	@ModelAttribute("premiumrate")
    public Premiumrate premiumrateConstructor(){
    	return new Premiumrate();
    }
	@ModelAttribute("discountrate")
    public Discountrate discountrateConstructor(){
    	return new Discountrate();
    }
	@ModelAttribute("mbrate")
    public Mbrate mbrateConstructor(){
    	return new Mbrate();
    }
	@ModelAttribute("modalfactor")
    public Modalfactor modalfactorConstructor(){
    	return new Modalfactor();
    }
	@ModelAttribute("surrender")
    public Surrender surrenderConstructor(){
    	return new Surrender();
    }
	@ModelAttribute("uabfactor")
    public UABFactor uabfactorConstructor(){
    	return new UABFactor();
    }
	
//	@Link(label="Item Table", family="TableSetupController", parent = "" )
	@RequestMapping(value =  "/tablesetup", method = RequestMethod.GET)
	public ModelAndView tableList() {

		ModelAndView model = new ModelAndView();			
		model.addObject("itemtables", tableSetupService.itemtablFindAll());		
		model.setViewName("tablesetup");
		return model;
	}
	
//	@Link(label="New Item Table", family="TableSetupController", parent = "Item Table" )
	@RequestMapping(value =  "/newitemtable", method = RequestMethod.GET)
	public String newItemtable(Model model) {		
		model.addAttribute("mode", "New");
		return "edititemtable";
	}
	
//	@Link(label="New Item Table", family="TableSetupController", parent = "Item Table" )
	@RequestMapping(value="/newitemtable",method=RequestMethod.POST)
	public String processNewItemtable(@Valid @ModelAttribute("itemtable") Itemtabl itemtable
			,BindingResult result
			,Principal principal,Model model){
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newItemtable(model);			
		}
		itemtable.setSynDate(new Date());
		itemtable.setCoUser(principal.getName());				
		tableSetupService.itemTableSave(itemtable);
		
		return "redirect:/tablesetup";
	}
	
//	@Link(label="Edit Item Table", family="TableSetupController", parent = "Item Table" )
	@RequestMapping(value =  "/tablesetup/{itemTableId}/edit", method = RequestMethod.GET)
	public String editItemTable(@PathVariable("itemTableId") Long itemTableId,Model model) {
		Itemtabl itemtable=tableSetupService.itemtablFindOne(itemTableId);	
		if(itemtable==null) throw new ResourceNotFoundException();
		model.addAttribute("itemtable", itemtable);
		model.addAttribute("mode", "Edit");
		return "edititemtable";

	}
	
//	@Link(label="Edit Item Table", family="TableSetupController", parent = "Item Table" )
	@RequestMapping(value =  "/tablesetup/{itemTableId}/edit", method = RequestMethod.POST)
	public String processItemTable(@PathVariable("itemTableId") Long itemTableId
			,@Valid @ModelAttribute("itemtable") Itemtabl itemtable
			,BindingResult result,Model model,Principal principal) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return editItemTable(itemTableId,model);			
		}
		itemtable.setSynDate(new Date());
		itemtable.setCoUser(principal.getName());
		tableSetupService.itemTableSave(itemtable);
		return "redirect:/tablesetup";													

	}
	
//	@Link(label="Item Item", family="TableSetupController", parent = "Item Table" )
	@RequestMapping(value =  "/tablesetup/{itemTableId}/item", method = RequestMethod.GET)
	public String itemitemlist(@PathVariable("itemTableId") Long itemTableId,Model model) {
		Itemtabl itemtable=tableSetupService.itemtablFindOneWithItem(itemTableId);		
		model.addAttribute("itemTableId", itemTableId);
		model.addAttribute("itemitems", itemtable.getItemitems());												
		return "itemitem";
	}
	
//	@Link(label="New Item Item", family="TableSetupController", parent = "Item Item" )
	@RequestMapping(value =  "/tablesetup/{itemTableId}/newitem", method = RequestMethod.GET)
	public String newitemitem(@PathVariable("itemTableId") Long itemTableId,Model model) {				
		if(tableSetupService.itemtablFindOne(itemTableId)==null) throw new ResourceNotFoundException();
		model.addAttribute("mode", "New");
		return "edititemitem";
	}
	
//	@Link(label="New Item Item", family="TableSetupController", parent = "Item Item" )
	@RequestMapping(value =  "/tablesetup/{itemTableId}/newitem", method = RequestMethod.POST)
	public String processNewitemitem(@PathVariable("itemTableId") Long itemTableId
			,@Valid @ModelAttribute("itemitem") Itemitem itemitem
			,BindingResult result,Model model,Principal principal) {				
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newitemitem(itemTableId,model);			
		}								
		Itemtabl itemtabl=tableSetupService.itemtablFindOne(itemTableId);
		if(itemtabl==null) throw new ResourceNotFoundException();
		itemitem.setItemtabl(itemtabl);
		itemitem.setSynDate(new Date());
		itemitem.setCoUser(principal.getName());
		tableSetupService.itemItemSave(itemitem);
		return "redirect:/tablesetup/"+itemTableId+"/item";
	}
	
//	@Link(label="Edit Item Item", family="TableSetupController", parent = "Item Item" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/edit", method = RequestMethod.GET)
	public String edititemitem(
			@PathVariable("itemItemId") Long itemItemId,Model model) {
		Itemitem itemitem=tableSetupService.itemitemFindOne(itemItemId);
		if(itemitem==null) throw new ResourceNotFoundException();
		model.addAttribute("itemitem", itemitem);	
		model.addAttribute("mode", "Edit");
		return "edititemitem";
	}
	
//	@Link(label="Edit Item Item", family="TableSetupController", parent = "Item Item" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/edit", method = RequestMethod.POST)
	public String processEdititemitem(@PathVariable("itemItemId") Long itemItemId
			,@Valid @ModelAttribute("itemitem") Itemitem itemitem
			,BindingResult result,Model model,Principal principal) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return edititemitem(itemItemId,model);			
		}		
		itemitem.setSynDate(new Date());
		itemitem.setCoUser(principal.getName());
		tableSetupService.itemItemSave(itemitem);
		return "redirect:/tablesetup/"+itemitem.getItemtabl().getId()+"/item";
	}
	
	
//	Premiumrate Rate 
//	@Link(label="Premium Rates", family="TableSetupController", parent = "Item Item" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/premiumrate", method = RequestMethod.GET)
	public String premiumRateList(
			@PathVariable("itemItemId") int itemItemId,Model model) {
		List<Premiumrate> premiumRates=tableSetupService.premiumrateFindByItemId(itemItemId);			
		Itemitem itemitem=tableSetupService.itemitemFindOne((long)itemItemId);
		if(itemitem==null) throw new ResourceNotFoundException();
		model.addAttribute("itemItemId", itemItemId);
		model.addAttribute("itemName", itemitem.getItemLatin());
		model.addAttribute("premiumrates", premiumRates);												
		return "premiumrate";
	}
	
//	@Link(label="New Premium Rates", family="TableSetupController", parent = "Premium Rates" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/newpremiumrate", method = RequestMethod.GET)
	public String newPremiumRate(
			@PathVariable("itemItemId") int itemItemId,Model model) {
		if(tableSetupService.itemitemFindOne((long) itemItemId)==null) throw new ResourceNotFoundException();
		Premiumrate premiumRate=new Premiumrate();
		premiumRate.setCoPremiumRate(itemItemId);
		model.addAttribute("premiumrate", premiumRate);												
		return "editpremiumrate";
	}
	
//	@Link(label="New Premium Rates", family="TableSetupController", parent = "Premium Rates" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/newpremiumrate", method = RequestMethod.POST)
	public String processNewPremiumRate(@PathVariable("itemItemId") int itemItemId
			,@Valid @ModelAttribute("premiumrate") Premiumrate premiumrate
			,BindingResult result,Model model,Principal principal) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newPremiumRate(itemItemId,model);			
		}
		if(tableSetupService.itemitemFindOne((long) itemItemId)==null) throw new ResourceNotFoundException();
		premiumrate.setCoPremiumRate(itemItemId);
		premiumrate.setSynDate(new Date());
		premiumrate.setCoUser(principal.getName());
		tableSetupService.premiumRateSave(premiumrate);
		return "redirect:/tablesetup/item/"+premiumrate.getCoPremiumRate()+"/premiumrate";
	}
	
//	@Link(label="Edit Premium Rates", family="TableSetupController", parent = "Premium Rates" )
	@RequestMapping(value =  "/tablesetup/item/premiumrate/{premiumRateId}/edit", method = RequestMethod.GET)
	public String premiumRateEdit(
			@PathVariable("premiumRateId") Long premiumRateId,Model model) {		
		Premiumrate premiumrate=tableSetupService.premiumrateFindOne(premiumRateId);
		if(premiumrate==null) throw new ResourceNotFoundException();
		model.addAttribute("premiumrate", premiumrate);												
		return "editpremiumrate";
	}
	
//	@Link(label="Edit Premium Rates", family="TableSetupController", parent = "Premium Rates" )
	@RequestMapping(value =  "/tablesetup/item/premiumrate/{premiumRateId}/edit", method = RequestMethod.POST)
	public String processPremiumRateEdit(@PathVariable("premiumRateId") Long premiumRateId
			,@Valid @ModelAttribute("premiumrate") Premiumrate premiumrate
			,BindingResult result,Model model,Principal principal) {		
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return premiumRateEdit(premiumRateId,model);			
		}
		premiumrate.setSynDate(new Date());
		premiumrate.setCoUser(principal.getName());
		tableSetupService.premiumRateSave(premiumrate);
		return "redirect:/tablesetup/item/"+premiumrate.getCoPremiumRate()+"/premiumrate";
	}
//	End Premium Rate
	
//	Start Discount Rate
//	@Link(label="Discount Rates", family="TableSetupController", parent = "Item Item" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/discountrate", method = RequestMethod.GET)
	public String discountRateList(
			@PathVariable("itemItemId") Long itemItemId,Model model) {
		List<Discountrate> discountRates=tableSetupService.discountrateFindByItemId(itemItemId);
		Itemitem itemitem=tableSetupService.itemitemFindOne(itemItemId);
		if(itemitem==null) throw new ResourceNotFoundException();
		model.addAttribute("itemItemId", itemItemId);
		model.addAttribute("itemName", itemitem.getItemLatin());
		model.addAttribute("discountrates", discountRates);												
		return "discountrate";
	}
	
//	@Link(label="New Discount Rates", family="TableSetupController", parent = "Discount Rates" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/newdiscountrate", method = RequestMethod.GET)
	public String newDiscountRate(
			@PathVariable("itemItemId") Long itemItemId,Model model) {
		if(tableSetupService.itemitemFindOne(itemItemId)==null) throw new ResourceNotFoundException();
		Discountrate discountRate=new Discountrate();
		discountRate.setCoDiscountRate(itemItemId);
		model.addAttribute("discountrate", discountRate);												
		return "editdiscountrate";
	}
	
//	@Link(label="New Discount Rates", family="TableSetupController", parent = "Discount Rates" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/newdiscountrate", method = RequestMethod.POST)
	public String processNewDiscountRate(@PathVariable("itemItemId") Long itemItemId
			,@Valid @ModelAttribute("discountrate") Discountrate discountrate
			,BindingResult result,Model model,Principal principal) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newDiscountRate(itemItemId,model);			
		}
		if(tableSetupService.itemitemFindOne(itemItemId)==null) throw new ResourceNotFoundException();
		discountrate.setCoDiscountRate(itemItemId);
		discountrate.setSynDate(new Date());
		discountrate.setCoUser(principal.getName());
		tableSetupService.discountRateSave(discountrate);
		return "redirect:/tablesetup/item/"+itemItemId+"/discountrate";
	}
	
//	@Link(label="Edit Discount Rates", family="TableSetupController", parent = "Discount Rates" )
	@RequestMapping(value =  "/tablesetup/item/discountrate/{discountRateId}/edit", method = RequestMethod.GET)
	public String discountRateEdit(
			@PathVariable("discountRateId") Long discountRateId,Model model) {		
		Discountrate discountrate=tableSetupService.discountrateFindOne(discountRateId);
		if(discountrate==null) throw new ResourceNotFoundException();
		model.addAttribute("discountrate", discountrate);												
		return "editdiscountrate";
	}
	
//	@Link(label="Edit Discount Rates", family="TableSetupController", parent = "Discount Rates" )
	@RequestMapping(value =  "/tablesetup/item/discountrate/{discountRateId}/edit", method = RequestMethod.POST)
	public String processDiscountRateEdit(@PathVariable("discountRateId") Long discountRateId
			,@Valid @ModelAttribute("discountrate") Discountrate discountrate
			,BindingResult result,Model model,Principal principal) {		
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return premiumRateEdit(discountRateId,model);			
		}
		discountrate.setSynDate(new Date());
		discountrate.setCoUser(principal.getName());
		tableSetupService.discountRateSave(discountrate);
		return "redirect:/tablesetup/item/"+discountrate.getCoDiscountRate()+"/discountrate";
	}
	
//	Start MB Rate
//	@Link(label="MB Rates", family="TableSetupController", parent = "Item Item" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/mbrate", method = RequestMethod.GET)
	public String mbRateList(@PathVariable("itemItemId") Long itemItemId,Model model) {
		List<Mbrate> mbRates=tableSetupService.mbrateFindByItemId(itemItemId);			
		Itemitem itemitem=tableSetupService.itemitemFindOne(itemItemId);
		if(itemitem==null) throw new ResourceNotFoundException();
		model.addAttribute("itemItemId", itemItemId);
		model.addAttribute("itemName", itemitem.getItemLatin());
		model.addAttribute("mbrates", mbRates);												
		return "mbrate";
	}
	
//	@Link(label="New MB Rates", family="TableSetupController", parent = "MB Rates" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/newmbrate", method = RequestMethod.GET)
	public String newMbRate(
			@PathVariable("itemItemId") Long itemItemId,Model model) {
		if(tableSetupService.itemitemFindOne(itemItemId)==null) throw new ResourceNotFoundException();
		Mbrate mbRate=new Mbrate();
		mbRate.setCoMbrate(itemItemId);
		model.addAttribute("mbrate", mbRate);												
		return "editmbrate";
	}
	
//	@Link(label="New MB Rates", family="TableSetupController", parent = "MB Rates" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/newmbrate", method = RequestMethod.POST)
	public String processNewMbRate(@PathVariable("itemItemId") Long itemItemId
			,@Valid @ModelAttribute("mbrate") Mbrate mbrate
			,BindingResult result,Model model,Principal principal) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newMbRate(itemItemId,model);			
		}
		if(tableSetupService.itemitemFindOne(itemItemId)==null) throw new ResourceNotFoundException();
		mbrate.setCoMbrate(itemItemId);
		mbrate.setSynDate(new Date());
		mbrate.setCoUser(principal.getName());
		tableSetupService.mbRateSave(mbrate);
		return "redirect:/tablesetup/item/"+itemItemId+"/mbrate";
	}
	
//	@Link(label="Edit MB Rates", family="TableSetupController", parent = "MB Rates" )
	@RequestMapping(value =  "/tablesetup/item/mbrate/{mbRateId}/edit", method = RequestMethod.GET)
	public String mbRateEdit(
			@PathVariable("mbRateId") Long mbRateId,Model model) {		
		Mbrate mbrate=tableSetupService.mbrateFindOne(mbRateId);
		if(mbrate==null) throw new ResourceNotFoundException();
		model.addAttribute("mbrate", mbrate);												
		return "editmbrate";
	}
	
//	@Link(label="Edit MB Rates", family="TableSetupController", parent = "MB Rates" )
	@RequestMapping(value =  "/tablesetup/item/mbrate/{mbRateId}/edit", method = RequestMethod.POST)
	public String processMbRateEdit(@PathVariable("mbRateId") Long mbRateId
			,@Valid @ModelAttribute("mbrate") Mbrate mbrate
			,BindingResult result,Model model,Principal principal) {		
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return mbRateEdit(mbRateId,model);			
		}
		mbrate.setSynDate(new Date());
		mbrate.setCoUser(principal.getName());
		tableSetupService.mbRateSave(mbrate);
		return "redirect:/tablesetup/item/"+mbrate.getCoMbrate()+"/mbrate";
	}
	
//	Start Modal Factor
//	@Link(label="Modal Factor", family="TableSetupController", parent = "Item Item" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/modalfactor", method = RequestMethod.GET)
	public String modalFactorList(@PathVariable("itemItemId") Long itemItemId,Model model) {
		List<Modalfactor> modalfactors=tableSetupService.modalFactorFindByItemId(itemItemId);			
		Itemitem itemitem=tableSetupService.itemitemFindOne(itemItemId);
		if(itemitem==null) throw new ResourceNotFoundException();
		model.addAttribute("itemItemId", itemItemId);
		model.addAttribute("itemName", itemitem.getItemLatin());
		model.addAttribute("modalfactors", modalfactors);												
		return "modalfactor";
	}
	
//	@Link(label="New Modal Factor", family="TableSetupController", parent = "Modal Factor" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/newmodalfactor", method = RequestMethod.GET)
	public String newModalFactor(
			@PathVariable("itemItemId") Long itemItemId,Model model) {
		if(tableSetupService.itemitemFindOne(itemItemId)==null) throw new ResourceNotFoundException();
		Modalfactor modalfactor=new Modalfactor();
		modalfactor.setCoModalFactor(itemItemId);
		model.addAttribute("modalfactor", modalfactor);												
		return "editmodalfactor";
	}
	
//	@Link(label="New Modal Factor", family="TableSetupController", parent = "Modal Factor" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/newmodalfactor", method = RequestMethod.POST)
	public String processNewModalFactor(@PathVariable("itemItemId") Long itemItemId
			,@Valid @ModelAttribute("modalfactor") Modalfactor modalfactor
			,BindingResult result,Model model,Principal principal) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newModalFactor(itemItemId,model);			
		}
		if(tableSetupService.itemitemFindOne(itemItemId)==null) throw new ResourceNotFoundException();
		modalfactor.setCoModalFactor(itemItemId);
		modalfactor.setSynDate(new Date());
		modalfactor.setCoUser(principal.getName());
		tableSetupService.modalFactorSave(modalfactor);
		return "redirect:/tablesetup/item/"+itemItemId+"/modalfactor";
	}
	
//	@Link(label="Edit Modal Factor", family="TableSetupController", parent = "Modal Factor" )
	@RequestMapping(value =  "/tablesetup/item/modalfactor/{modalFactorId}/edit", method = RequestMethod.GET)
	public String modalFactorEdit(
			@PathVariable("modalFactorId") Long modalFactorId,Model model) {		
		Modalfactor modalfactor=tableSetupService.modalFactorFindOne(modalFactorId);
		if(modalfactor==null) throw new ResourceNotFoundException();
		model.addAttribute("modalfactor", modalfactor);												
		return "editmodalfactor";
	}
	
//	@Link(label="Edit Modal Factor", family="TableSetupController", parent = "Modal Factor" )
	@RequestMapping(value =  "/tablesetup/item/modalfactor/{modalFactorId}/edit", method = RequestMethod.POST)
	public String processModalFactorEdit(@PathVariable("modalFactorId") Long modalFactorId
			,@Valid @ModelAttribute("modalfactor") Modalfactor modalfactor
			,BindingResult result,Model model,Principal principal) {		
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return modalFactorEdit(modalFactorId,model);			
		}
		modalfactor.setSynDate(new Date());
		modalfactor.setCoUser(principal.getName());
		tableSetupService.modalFactorSave(modalfactor);
		return "redirect:/tablesetup/item/"+modalfactor.getCoModalFactor()+"/modalfactor";
	}
	
//	Start Surrender
//	@Link(label="Surrender", family="TableSetupController", parent = "Item Item" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/surrender", method = RequestMethod.GET)
	public String surrenderList(@PathVariable("itemItemId") Long itemItemId,Model model) {
		List<Surrender> surrenders=tableSetupService.surrenderFindByItemId(itemItemId);			
		Itemitem itemitem=tableSetupService.itemitemFindOne(itemItemId);
		if(itemitem==null) throw new ResourceNotFoundException();
		model.addAttribute("itemItemId", itemItemId);
		model.addAttribute("itemName", itemitem.getItemLatin());
		model.addAttribute("surrenders", surrenders);												
		return "surrender";
	}
	
//	@Link(label="New Surrender", family="TableSetupController", parent = "Surrender" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/newsurrender", method = RequestMethod.GET)
	public String newSurrender(
			@PathVariable("itemItemId") Long itemItemId,Model model) {
		if(tableSetupService.itemitemFindOne(itemItemId)==null) throw new ResourceNotFoundException();
		Surrender surrender=new Surrender();
		surrender.setCoSurrender(itemItemId);
		model.addAttribute("modalfactor", surrender);												
		return "editsurrender";
	}
	
//	@Link(label="New Surrender", family="TableSetupController", parent = "Surrender" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/newsurrender", method = RequestMethod.POST)
	public String processNewSurrender(@PathVariable("itemItemId") Long itemItemId
			,@Valid @ModelAttribute("surrender") Surrender surrender
			,BindingResult result,Model model,Principal principal) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newSurrender(itemItemId,model);			
		}
		if(tableSetupService.itemitemFindOne(itemItemId)==null) throw new ResourceNotFoundException();
		surrender.setCoSurrender(itemItemId);
		surrender.setSynDate(new Date());
		surrender.setCoUser(principal.getName());
		tableSetupService.surrenderSave(surrender);
		return "redirect:/tablesetup/item/"+itemItemId+"/surrender";
	}
	
//	@Link(label="Edit Surrender", family="TableSetupController", parent = "Surrender" )
	@RequestMapping(value =  "/tablesetup/item/surrender/{surrenderId}/edit", method = RequestMethod.GET)
	public String surrenderEdit(
			@PathVariable("surrenderId") Long surrenderId,Model model) {		
		Surrender surrender=tableSetupService.surrenderFindOne(surrenderId);
		if(surrender==null) throw new ResourceNotFoundException();
		model.addAttribute("surrender", surrender);												
		return "editsurrender";
	}
	
//	@Link(label="Edit Surrender", family="TableSetupController", parent = "Surrender" )
	@RequestMapping(value =  "/tablesetup/item/surrender/{surrenderId}/edit", method = RequestMethod.POST)
	public String processSurrenderEdit(@PathVariable("surrenderId") Long surrenderId
			,@Valid @ModelAttribute("surrender") Surrender surrender
			,BindingResult result,Model model,Principal principal) {		
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return modalFactorEdit(surrenderId,model);			
		}
		surrender.setSynDate(new Date());
		surrender.setCoUser(principal.getName());
		tableSetupService.surrenderSave(surrender);
		return "redirect:/tablesetup/item/"+surrender.getCoSurrender()+"/surrender";
	}
//	Start UABFactor
//	@Link(label="UABFactor", family="TableSetupController", parent = "Item Item" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/uabfactor", method = RequestMethod.GET)
	public String uabfactorList(@PathVariable("itemItemId") Long itemItemId,Model model) {
		List<UABFactor> uabfactors=tableSetupService.uabFactorFindByItemId(itemItemId);			
		Itemitem itemitem=tableSetupService.itemitemFindOne(itemItemId);
		if(itemitem==null) throw new ResourceNotFoundException();
		model.addAttribute("itemItemId", itemItemId);
		model.addAttribute("itemName", itemitem.getItemLatin());
		model.addAttribute("uabfactors", uabfactors);												
		return "uabfactor";
	}
	
//	@Link(label="New UABFactor", family="TableSetupController", parent = "UABFactor" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/newuabfactor", method = RequestMethod.GET)
	public String newUABFactor(
			@PathVariable("itemItemId") Long itemItemId,Model model) {
		if(tableSetupService.itemitemFindOne(itemItemId)==null) throw new ResourceNotFoundException();
		UABFactor uabfactor=new UABFactor();
		uabfactor.setCoUABFactor(itemItemId);
		model.addAttribute("uabfactor", uabfactor);												
		return "edituabfactor";
	}
	
//	@Link(label="New UABFactor", family="TableSetupController", parent = "UABFactor" )
	@RequestMapping(value =  "/tablesetup/item/{itemItemId}/newuabfactor", method = RequestMethod.POST)
	public String processNewUABFactor(@PathVariable("itemItemId") Long itemItemId
			,@Valid @ModelAttribute("uabfactor") UABFactor uabfactor
			,BindingResult result,Model model,Principal principal) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newUABFactor(itemItemId,model);			
		}
		if(tableSetupService.itemitemFindOne(itemItemId)==null) throw new ResourceNotFoundException();
		uabfactor.setCoUABFactor(itemItemId);
		uabfactor.setSynDate(new Date());
		uabfactor.setCoUser(principal.getName());
		tableSetupService.uabFactorSave(uabfactor);
		return "redirect:/tablesetup/item/"+itemItemId+"/uabfactor";
	}
	
//	@Link(label="Edit UABFactor", family="TableSetupController", parent = "UABFactor" )
	@RequestMapping(value =  "/tablesetup/item/uabfactor/{uabfactorId}/edit", method = RequestMethod.GET)
	public String uabfactorEdit(
			@PathVariable("uabfactorId") Long uabfactorId,Model model) {		
		UABFactor uabfactor=tableSetupService.uabFactorFindOne(uabfactorId);
		if(uabfactor==null) throw new ResourceNotFoundException();
		model.addAttribute("uabfactor", uabfactor);												
		return "edituabfactor";
	}
	
//	@Link(label="Edit UABFactor", family="TableSetupController", parent = "UABFactor" )
	@RequestMapping(value =  "/tablesetup/item/uabfactor/{uabfactorId}/edit", method = RequestMethod.POST)
	public String processUabfactorEdit(@PathVariable("uabfactorId") Long uabfactorId
			,@Valid @ModelAttribute("uabfactor") UABFactor uabfactor
			,BindingResult result,Model model,Principal principal) {		
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return modalFactorEdit(uabfactorId,model);			
		}
		uabfactor.setSynDate(new Date());
		uabfactor.setCoUser(principal.getName());
		tableSetupService.uabFactorSave(uabfactor);
		return "redirect:/tablesetup/item/"+uabfactor.getCoUABFactor()+"/uabfactor";
	}
}
