package com.pru.pruquote.controller;

import java.io.IOException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.pru.pruquote.model.OtherCollectionAndLapse;
import com.pru.pruquote.service.OtherCollectionandLapseService;
import com.pru.pruquote.utility.AnonymousHelper;

@Controller
@PreAuthorize("hasAnyAuthority('ACCESS_COLLECTION_LAPSE')")
public class OtherCollectionandLapseController {

	@Autowired
	private OtherCollectionandLapseService otherCollectionAndLapseService;

	@RequestMapping(value = "/othercollectionandlapse", method = RequestMethod.GET)
	public ModelAndView collectionandLapse(Principal principal, HttpServletRequest req) {
		String type = req.getParameter("type");
		String ipc = req.getParameter("ipc");
		ModelAndView model = new ModelAndView();
		model.addObject("pageTitle", "Collection and Lapse");
		model.addObject("type", type);
		if (ipc != null) {
			model.addObject("ipc", ipc);
		}
		model.setViewName("collectionandlapse/othercollectionandlapse");
		return model;
	}

	@ResponseBody
	@RequestMapping(value = "/getothercollection", method = RequestMethod.GET)
	public String getCollection(Principal principal, HttpServletRequest req) {
		String result = "";
	
		List<OtherCollectionAndLapse> list  = new ArrayList<OtherCollectionAndLapse>();
		result = AnonymousHelper.getServerSideDatasource(req, list);

		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/advancesearchothercollection", method = RequestMethod.GET)
	public String AdvanceSearchCollection(Principal principal, HttpServletRequest req) {
		String result = "";

		String poappnum = req.getParameter("poappnum");
		String dob = req.getParameter("dob");
		String formattedDob = "";
		try

		{
			if (poappnum == null || poappnum.trim().equals("")) {
				throw new Exception("Policy Number or Application Number is invalid");
			}

			if (dob == null || dob.trim().equals("")) {
				throw new Exception("Date of birth is invalid");
			}

			if (!AnonymousHelper.isDateValid(dob)) {
				throw new Exception("Date of Birth is not valid");
			}
			
			Date formatDate = new SimpleDateFormat(AnonymousHelper.DATE_FORMAT).parse(dob);
			formattedDob =  new SimpleDateFormat("yyyy-MM-dd").format(formatDate);
			
			if(formattedDob == null || formattedDob.trim().equals(""))
			{
				throw new Exception("Date of birth is invalid");
			}
			
		} catch (Exception e) {
			return result;
		}
		
		List<OtherCollectionAndLapse> list = otherCollectionAndLapseService.getListOtherCollectionandLapse(poappnum, formattedDob);

		result = AnonymousHelper.getServerSideDatasource(req, list);

		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/csvadvancesearchothercollection")
	public void GetCSVPersistencies(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Date date = new Date();

		String objkey = request.getParameter("objkey");
		List<OtherCollectionAndLapse> list = otherCollectionAndLapseService.getCollectionandLapse(objkey);
		AnonymousHelper.ExportCSV(response, list, new OtherCollectionAndLapse(), list.get(0).getChdrAppnum()+"_"+list.get(0).getPoName()+"_"+new SimpleDateFormat("ddMMyyyy").format(date));
	}
}