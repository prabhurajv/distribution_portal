package com.pru.pruquote.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.pru.pruquote.model.AcbProduct;
import com.pru.pruquote.model.CollectionAndLapse;
import com.pru.pruquote.model.Referral;
import com.pru.pruquote.model.ReferralAcbProduct;
import com.pru.pruquote.model.ReferralList;
import com.pru.pruquote.model.UpsellingAndCrosssellingModel;
import com.pru.pruquote.model.listPruquote;
import com.pru.pruquote.service.BranchService;
import com.pru.pruquote.service.ReferralService;
import com.pru.pruquote.service.UpsellingAndCrosssellingService;
import com.pru.pruquote.utility.AnonymousHelper;

@Controller
@PreAuthorize("hasAnyAuthority('ACCESS_UPSELL_CROSSSELL')")
public class UpsellingAndCrosssellingController {
	
	@Autowired	
	private UpsellingAndCrosssellingService upsellingAndCrosssellingService;
	
	@RequestMapping(value =  "/upsellingandcrossselling", method = RequestMethod.GET)
	public ModelAndView referral(Principal principal) {
		ModelAndView model = new ModelAndView();			
			
		model.addObject("pageTitle", "Upselling And Cross selling");
		model.setViewName("upsellingandcrossselling/upsellingandcrossselling");
		return model;
	}
	
	@ResponseBody
	@RequestMapping(value = "/getallupsellingandcrossselling", method = RequestMethod.GET)
	public String getUpselling(Principal principal, HttpServletRequest req) {
		String result = "";
		int sortCol = Integer.valueOf(req.getParameter("order[0][column]"));
		String sortColDir = req.getParameter("order[0][dir]");
		String search = req.getParameter("search[value]");
		String columnsName = req.getParameter("columns[" + sortCol + "][data]");
		List<UpsellingAndCrosssellingModel> getAllUpsellAndCrosssell = upsellingAndCrosssellingService.listUpsellingAndCrossselling(principal.getName());
		//here to search 
		if (search != null && search != "") {
			String sanitizedSearchKeyword = AnonymousHelper.sanitizeInput(search);
			getAllUpsellAndCrosssell = getAllUpsellAndCrosssell.stream()
					.filter(u -> (u.getAgentNum() != null && u.getAgentNum().contains(sanitizedSearchKeyword))
							|| (u.getAgentName() != null && u.getAgentName().contains(sanitizedSearchKeyword))
							|| (u.getPoNumber() != null && u.getPoNumber().contains(sanitizedSearchKeyword))
							|| (u.getCusName() != null && u.getCusName().contains(sanitizedSearchKeyword))
							|| (u.getCusSex() != null && u.getCusSex().contains(sanitizedSearchKeyword))
							|| (u.getCusMobile1() != null && u.getCusMobile1().contains(sanitizedSearchKeyword))
							|| (u.getCusMobile2() != null && u.getCusMobile2().contains(sanitizedSearchKeyword))
							|| (u.getCusMstatus() != null && u.getCusMstatus().contains(sanitizedSearchKeyword))
							|| (u.getCusNoChild() != null && u.getCusNoChild().contains(sanitizedSearchKeyword))
							|| (u.getCusOccupation() != null && u.getCusOccupation().contains(sanitizedSearchKeyword))
							|| (u.getIssDate() != null && u.getIssDate().toString().contains(sanitizedSearchKeyword))
							|| (u.getBdmCode() != null && u.getBdmCode().contains(sanitizedSearchKeyword))
							|| (u.getBdmName() != null && u.getBdmName().contains(sanitizedSearchKeyword))
							|| (u.getBranchCode() != null && u.getBranchCode().contains(sanitizedSearchKeyword))
							|| (u.getApe() != null && u.getApe().toString().contains(sanitizedSearchKeyword))
							|| (u.getSumAssure() != null && u.getSumAssure().toString().contains(sanitizedSearchKeyword))
							|| (u.getUab() != null && u.getUab().toString().contains(sanitizedSearchKeyword))
							|| (u.getPoTransferStatus() != null && u.getPoTransferStatus().contains(sanitizedSearchKeyword))
							|| (u.getApproachStatus() != null && u.getApproachStatus().contains(sanitizedSearchKeyword)))
					.collect(Collectors.toList());
		}
		if (sortColDir.equalsIgnoreCase("asc")) {
			if (columnsName.equalsIgnoreCase("agentNum"))
				Collections.sort(getAllUpsellAndCrosssell, UpsellingAndCrosssellingModel.AgentNumAsc);
			else if (columnsName.equalsIgnoreCase("agentName"))
				Collections.sort(getAllUpsellAndCrosssell, UpsellingAndCrosssellingModel.AgentNameAsc);
			else if (columnsName.equalsIgnoreCase("poNumber"))
				Collections.sort(getAllUpsellAndCrosssell, UpsellingAndCrosssellingModel.PoNumberAsc);
			else if (columnsName.equalsIgnoreCase("cusName"))
				Collections.sort(getAllUpsellAndCrosssell, UpsellingAndCrosssellingModel.CusNameAsc);
			else if (columnsName.equalsIgnoreCase("cusSex"))
				Collections.sort(getAllUpsellAndCrosssell, UpsellingAndCrosssellingModel.CusSexAsc);
			else if (columnsName.equalsIgnoreCase("cusMobile1"))
				Collections.sort(getAllUpsellAndCrosssell, UpsellingAndCrosssellingModel.CusMobile1Asc);
			else if (columnsName.equalsIgnoreCase("cusMobile2"))
				Collections.sort(getAllUpsellAndCrosssell, UpsellingAndCrosssellingModel.CusMobile2Asc);
			else if (columnsName.equalsIgnoreCase("cusMstatus"))
				Collections.sort(getAllUpsellAndCrosssell , UpsellingAndCrosssellingModel.CusMstatusAsc);
			else if (columnsName.equalsIgnoreCase("cusMstatus"))
				Collections.sort(getAllUpsellAndCrosssell , UpsellingAndCrosssellingModel.CusMstatusAsc);
			
		}else {
			if (columnsName.equalsIgnoreCase("agentNum"))
				Collections.sort(getAllUpsellAndCrosssell, UpsellingAndCrosssellingModel.AgentNameDesc);
			else if (columnsName.equalsIgnoreCase("agentName"))
				Collections.sort(getAllUpsellAndCrosssell, UpsellingAndCrosssellingModel.AgentNameDesc);
			else if (columnsName.equalsIgnoreCase("poNumber"))
				Collections.sort(getAllUpsellAndCrosssell, UpsellingAndCrosssellingModel.PoNumberDesc);
			else if (columnsName.equalsIgnoreCase("cusName"))
				Collections.sort(getAllUpsellAndCrosssell, UpsellingAndCrosssellingModel.CusNameDesc);
			else if (columnsName.equalsIgnoreCase("cusSex"))
				Collections.sort(getAllUpsellAndCrosssell, UpsellingAndCrosssellingModel.CusSexDesc);
			else if (columnsName.equalsIgnoreCase("cusMobile1"))
				Collections.sort(getAllUpsellAndCrosssell, UpsellingAndCrosssellingModel.CusMobile1Desc);
			else if (columnsName.equalsIgnoreCase("cusMobile2"))
				Collections.sort(getAllUpsellAndCrosssell, UpsellingAndCrosssellingModel.CusMobile2Asc);
			else if (columnsName.equalsIgnoreCase("cusMstatus"))
				Collections.sort(getAllUpsellAndCrosssell , UpsellingAndCrosssellingModel.CusMstatusDesc);
		}
		result = AnonymousHelper.getServerSideDatasource(req, getAllUpsellAndCrosssell);
		return result;
	}

}
