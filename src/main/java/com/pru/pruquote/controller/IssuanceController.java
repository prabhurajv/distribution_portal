package com.pru.pruquote.controller;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pru.pruquote.model.Issuance;
import com.pru.pruquote.model.Submission;
import com.pru.pruquote.service.IssuanceService;
import com.pru.pruquote.service.SubmissionService;
import com.pru.pruquote.utility.AnonymousHelper;

@Controller
@PreAuthorize("hasAnyAuthority('ACCESS_SUBISS_SUMMARY,ACCESS_SUBISS_DETAIL')")
public class IssuanceController {
	@Autowired
	private IssuanceService issuanceService;
	
	@RequestMapping(value =  "/getmyissuances", method = RequestMethod.GET)
	public ModelAndView getMyIssuances(Principal principal, HttpServletRequest req) {
		ModelAndView model = new ModelAndView();
//		String status = req.getParameter("status");
//		if(status != null) {
//			model.addObject("status", status);
//		}
//				
		model.addObject("pageTitle", "My Issuance");
//		model.addObject("getAgent", referralService.listSearchReferralMgtParam(principal.getName(),"", "", "", "", "", "").get(5));
		model.setViewName("issuance/issuance");
		return model;
	}
	
	@ResponseBody
	@RequestMapping(value =  "/getissuances", method = RequestMethod.GET)
	public String getIssuances(Principal principal) {
		String result = "";
		String userid = principal.getName();
		ObjectMapper mapper = new ObjectMapper();
		List<Issuance> getAllIssuances = issuanceService.getListIssuance(userid);
		try {
			result = mapper.writeValueAsString(getAllIssuances);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value = "/issuancesummary", method = RequestMethod.GET)
	@ResponseBody
	public String issuanceSummary(HttpServletRequest req, Principal principal){
		String result = "";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object[]> list = new HashMap<>();
		List<Object[]> resultList = new ArrayList<>();
		String userid = principal.getName();
		String summaryby = req.getParameter("summaryby");
		
		try {
			if(summaryby.toLowerCase().equals("branch")){
				list = AnonymousHelper.getDistinctValueForSummary(issuanceService.getListIssuance(userid)
						, "getBranchCode", "getBranchCode", "getBranchName", "getChdrNum", "getChdrApe");	
			}else if(summaryby.toLowerCase().equals("agent")){
				list = AnonymousHelper.getDistinctValueForSummary(issuanceService.getListIssuance(userid)
						, "getAgntNum", "getAgntNum", "getAgntName", "getChdrNum", "getChdrApe");	
			}
			
			List<String> keyList = new ArrayList<String>(list.keySet());

			for(String key : keyList){
				int count = 0;
				BigDecimal sum = BigDecimal.valueOf(0);
				String summaryKey;
				String summaryName;
				
				List<Object> summary = Arrays.asList(list.get(key));
				
				count = summary.size();
				sum = sum.add(BigDecimal.valueOf(summary.stream().map(o -> 
							Double.valueOf(((Object[])o)[3].toString()))
							.reduce(Double.valueOf(0), Double::sum)))
							.setScale(2, BigDecimal.ROUND_HALF_UP);

				summaryKey = ((Object[])list.get(key)[0])[0].toString();
				summaryName =  AnonymousHelper.replaceNullString(((Object[])list.get(key)[0])[1], "");
				
				Object[] objForDisplay = new Object[4];
				objForDisplay[0] = summaryKey;
				objForDisplay[1] = summaryName;
				objForDisplay[2] = count;
				objForDisplay[3] = sum;
				
				resultList.add(objForDisplay);
			}
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(resultList.size() > 0){
			try {
				result = mapper.writeValueAsString(resultList);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result;
	}
}