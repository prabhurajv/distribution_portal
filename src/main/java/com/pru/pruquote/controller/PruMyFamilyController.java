package com.pru.pruquote.controller;


import java.math.BigDecimal;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.node.BigIntegerNode;
import com.pru.pruquote.model.Itemitem;
import com.pru.pruquote.model.Itemtabl;
import com.pru.pruquote.model.Occupation;

//import dummiesmind.breadcrumb.springmvc.annotations.Link;

import com.pru.pruquote.model.PruquoteParm;
import com.pru.pruquote.model.QuoteOccupation;
import com.pru.pruquote.model.User;
import com.pru.pruquote.model.listPruquote;
import com.pru.pruquote.service.QuoteService;
import com.pru.pruquote.service.DDService;
import com.pru.pruquote.service.ListQuoteService;
import com.pru.pruquote.service.LogService;
import com.pru.pruquote.service.OccupationService;
import com.pru.pruquote.service.SecurityService;
import com.pru.pruquote.service.TableSetupService;
import com.pru.pruquote.utility.AnonymousHelper;
import com.pru.pruquote.utility.GlobalVar;


@Controller
@PreAuthorize("hasAuthority('ACCESS_PRUMYFAMILY')")
public class PruMyFamilyController {	
	
	@Autowired	
	private QuoteService quoteService;
	
	@Autowired	
	private ListQuoteService listQuoteService;
	
	@Autowired	
	private TableSetupService tableSetupService;
	
	@Autowired	
	private SecurityService securityService;
	
	@Autowired
	private OccupationService occupationService;
	
	@Autowired
	private LogService logService;
	
	@Autowired
	private DDService ddService;
	
	@ModelAttribute("pruquoteparm")
    public PruquoteParm pruquoteparmConstructor(){
    	return new PruquoteParm();
    }
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
        binder.registerCustomEditor(Date.class,"commDate",new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class,"la1dateOfBirth",new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class,"syndate",new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class,"la2dateOfBirth",new CustomDateEditor(dateFormat, false));
    }

	@RequestMapping(value =  "/prumyfamily", method = RequestMethod.GET)
	public ModelAndView prumyfamilyForm(Principal principal) {
		ModelAndView model = new ModelAndView();			
		model.addObject("pruMyFamilyList", quoteService.viewQuote("BTR1", principal.getName()));
		model.addObject("pageTitle", "PRU MyFamily");
		model.setViewName("quote/prumyfamily");
		return model;
	}
		
	
	@RequestMapping(value =  "/pruquote_myfamily_repkhmer/{quoteId}", method = RequestMethod.GET)
	public ModelAndView myFamilyPreviewKhmer(@PathVariable("quoteId") Long quoteId,Principal principal, HttpServletRequest req) {
		ModelAndView model = new ModelAndView();	
		List<listPruquote> listPruquoteParms=listQuoteService.listQuote(principal.getName(), Long.toString(quoteId));
		List<PruquoteParm> pruquoteParms=quoteService.findValidQuote(quoteId, "BTR1", principal.getName());
		if(listPruquoteParms.size()<=0 & pruquoteParms.size()<=0){
			throw new ResourceNotFoundException();
		}
		else if (listPruquoteParms.size()>=0){
			pruquoteParms=quoteService.findValidQuote(quoteId);
			//model.addObject("name","");
			//model.addObject("code","");
		}
		else {
			if(pruquoteParms.size()<=0) {
				throw new ResourceNotFoundException();
				}
			else{
				//User user=securityService.userFindOne(principal.getName());
				//model.addObject("name",user.getFullNameKH());
				//model.addObject("code",user.getUserId());	
			}
		}
		
		List<String> results=quoteService.generateMyFamilyKhmerQuote(quoteId, "Khmer","RN");
		
		model.addObject("header1",results.get(0));
		model.addObject("body1",results.get(1));
		model.addObject("body2",results.get(2));
		model.addObject("body3",results.get(3));		
		model.addObject("body4",results.get(4));
		model.addObject("body5",results.get(5));
		model.addObject("serialno",results.get(6));
		
		User user=securityService.findUserByUsername(pruquoteParms.get(0).getCoUser());
		model.addObject("name",user.getFullNameKh());
		model.addObject("code",user.getUserId());
		
		if(pruquoteParms.get(0).getCoL1Rtr2()==15)
			model.setViewName("report/PruQuote_MyFamily_RepKhmer");
		else
			model.setViewName("report/PruQuote_MyFamily_RepKhmer_NO_Saver");
		
		model.addObject("version", GlobalVar.version);
		
		model.addObject("quoteOcc", occupationService.getQuoteOccupationByQuoteId(quoteId));
		Occupation occ =   occupationService.getOccupationByEngName("Others");
		if (occ == null) {
			model.addObject("occOtherId",0);
		} else {
			model.addObject("occOtherId",occ.getId());	
		}
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object onbehalf = attr.getRequest().getSession().getAttribute("isonbehalf");
		model.addObject("isonbehalf", onbehalf);
		attr.getRequest().getSession().removeAttribute("isonbehalf");
		
		if (onbehalf.equals("15")) {
			String action = "Quote Id = " + quoteId + "and On be half = " + "Yes";
			String ip = req.getRemoteHost();
			String pip = req.getRemoteHost();
			Date date = new Date();
			logService.logUserAction(user.getUserId(), action, ip, pip, date);
		}
		
		return model;
	}
	@RequestMapping(value =  "/pruquote_myfamily_replatin/{quoteId}", method = RequestMethod.GET)
	public ModelAndView myFamilyPreviewLatin(@PathVariable("quoteId") Long quoteId,Principal principal, HttpServletRequest req) {
		ModelAndView model = new ModelAndView();	

		List<listPruquote> listPruquoteParms=listQuoteService.listQuote(principal.getName(), Long.toString(quoteId));
		List<PruquoteParm> pruquoteParms=quoteService.findValidQuote(quoteId, "BTR1", principal.getName());
		if(listPruquoteParms.size()<=0 & pruquoteParms.size()<=0){
			throw new ResourceNotFoundException();
		}
		else if (listPruquoteParms.size()>=0){
			pruquoteParms=quoteService.findValidQuote(quoteId);
		}
		else {
			if(pruquoteParms.size()<=0) {
				throw new ResourceNotFoundException();
				}
			else{
				
			}
		}
		
		List<String> results=quoteService.generateMyFamilyLatinQuote(quoteId, "Latin","RN");
		model.addObject("header1",results.get(0));
		model.addObject("body1",results.get(1));
		model.addObject("body2",results.get(2));
		model.addObject("body3",results.get(3));		
		model.addObject("body4",results.get(4));
		model.addObject("body5",results.get(5));
		model.addObject("serialno1",results.get(6));
		model.addObject("serialno2",results.get(7));
		
		User user=securityService.findUserByUsername(pruquoteParms.get(0).getCoUser());
		model.addObject("name",user.getFullNameKh());
		model.addObject("code",user.getUserId());
		
		if(pruquoteParms.get(0).getCoL1Rtr2()==15){
			model.setViewName("report/PruQuote_MyFamily_RepLatin");
		}
		else{
			if(pruquoteParms.get(0).getCoL1Rsr1()==15 || pruquoteParms.get(0).getCoL1Rtr1()==15 || pruquoteParms.get(0).getCoL2Rsr1()==15 || pruquoteParms.get(0).getCoL2Rtr1()==15 )
				model.setViewName("report/PruQuote_MyFamily_RepLatin");
			else
				model.setViewName("report/PruQuote_MyFamily_RepLatin_NO_Saver");
		}
		
		model.addObject("version", GlobalVar.version);
		
		model.addObject("quoteOcc", occupationService.getQuoteOccupationByQuoteId(quoteId));
		Occupation occ =   occupationService.getOccupationByEngName("Others");
		if (occ == null) {
			model.addObject("occOtherId",0);
		} else {
			model.addObject("occOtherId",occ.getId());	
		}
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object onbehalf = attr.getRequest().getSession().getAttribute("isonbehalf");
		model.addObject("isonbehalf", onbehalf);
		attr.getRequest().getSession().removeAttribute("isonbehalf");
		
		if (onbehalf.equals("15")) {
			String action = "Quote Id = " + quoteId + "and On be half = " + "Yes";
			String ip = req.getRemoteHost();
			String pip = req.getRemoteHost();
			Date date = new Date();
			logService.logUserAction(user.getUserId(), action, ip, pip, date);
		}
		
		return model;
	}
	
	@RequestMapping(value =  "/newprumyfamily", method = RequestMethod.GET)
	public String newprumyfamilyForm(Model model) {
		model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
		model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%BTR1%"));
		model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%BTR1%"));
		model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
		model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long)9));
		model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
		model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
		model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("pruMyFamilyRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("isAffectiveDate", new Date().compareTo(affectiveDate));
		model.addAttribute("pageTitle", "New PRU MyFamily");
		
		List<Itemitem> prumyfamilyMinBasicSA = tableSetupService.itemitemFindNameByReserve1("myfamilyMinBasicSA", 1);
		List<Itemitem> pcbPrumyfamily = tableSetupService.itemitemFindNameByReserve1("pcbPRUmyfamily", 1);
		List<Itemitem> pcbPrumyfamilyMinSAMultiply = tableSetupService.itemitemFindNameByReserve1("pcbPRUmyfamilyMinSAMultiply", 1);
		
		if(prumyfamilyMinBasicSA != null && prumyfamilyMinBasicSA.size() > 0)
			model.addAttribute("prumyfamilyMinBasicSA", prumyfamilyMinBasicSA.get(0).getReserve2());
		
		if(pcbPrumyfamily != null && pcbPrumyfamily.size() > 0)
			model.addAttribute("pcbPrumyfamily", pcbPrumyfamily.get(0).getReserve2());
		
		if(pcbPrumyfamilyMinSAMultiply != null && pcbPrumyfamilyMinSAMultiply.size() > 0)
			model.addAttribute("pcbPrumyfamilyMinSAMultiply", pcbPrumyfamilyMinSAMultiply.get(0).getReserve2());
		
		model.addAttribute("OccupationDdl", occupationService.getAllOccupationDdl());
		
		return "quote/newprumyfamily";
	}
	@RequestMapping(value =  "/newprumyfamily", method = RequestMethod.POST,params="save")
	public String processNewprumyfamilyForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,Model model,Principal principal, HttpServletRequest req) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newprumyfamilyForm(model);
		}
		if(!validatePrumyfamily(pruquoteparm)) {
            model.addAttribute("errors", "Invalid quotation for PRUmy family!");
            return newprumyfamilyForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR1");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("pruMyFamilyRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int isAffectiveDate = new Date().compareTo(affectiveDate);
		if (isAffectiveDate > 0) {
			pruquoteparm.setCoL1Rtr2(16);
			pruquoteparm.setL1Rtr2(new BigDecimal(0));
		}
		quoteService.pruQuoteParamSave(pruquoteparm);
		
		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");
		if(!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1",1 ,principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if(!occpo.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo),"occpo",1 ,principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);	
		}
		if(!occla2.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2),"occla2",1 ,principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		
		return "redirect:/prumyfamily";
	}
	
	@RequestMapping(value =  "/newprumyfamily", method = RequestMethod.POST, params="previewkh")
	public String processNewprumyfamilyFormPreviewKh(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,Model model,Principal principal,HttpServletRequest request,RedirectAttributes redirectAttributes, HttpServletRequest req) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newprumyfamilyForm(model);			
		}
		if(!validatePrumyfamily(pruquoteparm)) {
            model.addAttribute("errors", "Invalid quotation for PRUmy family!");
            return newprumyfamilyForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR1");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("pruMyFamilyRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int isAffectiveDate = new Date().compareTo(affectiveDate);
		if (isAffectiveDate > 0) {
			pruquoteparm.setCoL1Rtr2(16);
			pruquoteparm.setL1Rtr2(new BigDecimal(0));
		}
		quoteService.pruQuoteParamSave(pruquoteparm);
//		model.addAttribute("previewurl", "/pruquote_myfamily_repkhmer/"+pruquoteparm.getId());
//		return editPruMyFamily(pruquoteparm.getId(),model);
		if(pruquoteparm.getCoL1Rtr2()==15){
			Object[] prusaver=(Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
			if(prusaver[3].equals("FALSE".toLowerCase())){
				redirectAttributes.addFlashAttribute("errorPruSaver","Prusaver MB should between "+prusaver[1]+"-"+prusaver[2]);
				return "redirect:/prumyfamily/"+pruquoteparm.getId()+"/edit";			
			}			
		}
		
		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");
		if(!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1",1 ,principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if(!occpo.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo),"occpo",1 ,principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if(!occla2.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2),"occla2",1 ,principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/pruquote_myfamily_repkhmer/"+pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview PruMyFamily Khmer Quote No " +pruquoteparm.getId(), principal.getName());
		return "redirect:/prumyfamily/"+pruquoteparm.getId()+"/edit";

	}
	
	@RequestMapping(value =  "/newprumyfamily", method = RequestMethod.POST, params="previewen")
	public String processNewprumyfamilyFormPreviewEn(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,Model model,Principal principal,HttpServletRequest request,RedirectAttributes redirectAttributes, HttpServletRequest req) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newprumyfamilyForm(model);			
		}
		if(!validatePrumyfamily(pruquoteparm)) {
            model.addAttribute("errors", "Invalid quotation for PRUmy family!");
            return newprumyfamilyForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR1");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("pruMyFamilyRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int isAffectiveDate = new Date().compareTo(affectiveDate);
		if (isAffectiveDate > 0) {
			pruquoteparm.setCoL1Rtr2(16);
			pruquoteparm.setL1Rtr2(new BigDecimal(0));
		}
		quoteService.pruQuoteParamSave(pruquoteparm);
//		model.addAttribute("previewurl", "/pruquote_myfamily_replatin/"+pruquoteparm.getId());
//		return editPruMyFamily(pruquoteparm.getId(),model);
		if(pruquoteparm.getCoL1Rtr2()==15){
			Object[] prusaver=(Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
			if(prusaver[3].equals("FALSE".toLowerCase())){
				redirectAttributes.addFlashAttribute("errorPruSaver","Prusaver MB should between "+prusaver[1]+"-"+prusaver[2]);
				return "redirect:/prumyfamily/"+pruquoteparm.getId()+"/edit";		
			}			
		}
		
		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");
		if(!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1",1 ,principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if(!occpo.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo),"occpo",1 ,principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if(!occla2.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2),"occla2",1 ,principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/pruquote_myfamily_replatin/"+pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview PruMyFamily English Quote No " +pruquoteparm.getId(), principal.getName());
		return "redirect:/prumyfamily/"+pruquoteparm.getId()+"/edit";

	}
	
	@RequestMapping(value =  "/prumyfamily/{id}/edit", method = RequestMethod.GET)
	public String editPruMyFamily(@PathVariable("id") Long id,Model model) {
		PruquoteParm pruquoteParm=quoteService.getValidQuote(id, "BTR1", SecurityContextHolder.getContext().getAuthentication().getName());
		if(pruquoteParm==null) throw new ResourceNotFoundException();
		model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
		model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%BTR1%"));
		model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%BTR1%"));
		model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
		model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long)9));
		model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
		model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
		model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
		model.addAttribute("occupations", occupationService.getAllOccupationDdl());
		model.addAttribute("pruquoteparm",pruquoteParm);
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("pruMyFamilyRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("isAffectiveDate", new Date().compareTo(affectiveDate));
		model.addAttribute("pageTitle", "Edit PRU MyFamily");
		
		List<Itemitem> prumyfamilyMinBasicSA = tableSetupService.itemitemFindNameByReserve1("myfamilyMinBasicSA", 1);
		List<Itemitem> pcbPrumyfamily = tableSetupService.itemitemFindNameByReserve1("pcbPRUmyfamily", 1);
		List<Itemitem> pcbPrumyfamilyMinSAMultiply = tableSetupService.itemitemFindNameByReserve1("pcbPRUmyfamilyMinSAMultiply", 1);
		
		if(prumyfamilyMinBasicSA != null && prumyfamilyMinBasicSA.size() > 0)
			model.addAttribute("prumyfamilyMinBasicSA", prumyfamilyMinBasicSA.get(0).getReserve2());
		
		if(pcbPrumyfamily != null && pcbPrumyfamily.size() > 0)
			model.addAttribute("pcbPrumyfamily", pcbPrumyfamily.get(0).getReserve2());
		
		if(pcbPrumyfamilyMinSAMultiply != null && pcbPrumyfamilyMinSAMultiply.size() > 0)
			model.addAttribute("pcbPrumyfamilyMinSAMultiply", pcbPrumyfamilyMinSAMultiply.get(0).getReserve2());
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object quoteUrl = attr.getRequest().getSession().getAttribute("previewurl");
		if(quoteUrl != null){
			model.addAttribute("quoteUrl", quoteUrl.toString());	
		}
		attr.getRequest().getSession().removeAttribute("previewurl");
		
		List<QuoteOccupation> qcs = occupationService.getQuoteOccupationByQuoteId(pruquoteParm.getId());
		for (QuoteOccupation qc : qcs) {
			String tmpRemark = qc.getRemark();
			if (tmpRemark.equals("occla1")) {
				model.addAttribute("occla1selected",qc.getOccId());	
			}
			if (tmpRemark.equals("occpo")) {
				model.addAttribute("occposelected",qc.getOccId());
			}
			if (tmpRemark.equals("occla2")) {
				model.addAttribute("occla2selected",qc.getOccId());
			}
		}
		
		return "quote/editprumyfamily";
	}
	@RequestMapping(value =  "/prumyfamily/{id}/edit", method = RequestMethod.POST, params="save")
	public String processEditprumyfamilyForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,@PathVariable("id") Long id,Model model,Principal principal, HttpServletRequest req) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%BTR1%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%BTR1%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long)9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
			return "quote/editprumyfamily";			
		}
		if(!validatePrumyfamily(pruquoteparm)) {
            model.addAttribute("errors", "Invalid quotation for PRUmy family!");
            return newprumyfamilyForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR1");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("pruMyFamilyRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int isAffectiveDate = new Date().compareTo(affectiveDate);
		if (isAffectiveDate > 0) {
			pruquoteparm.setCoL1Rtr2(16);
			pruquoteparm.setL1Rtr2(new BigDecimal(0));
		}
		quoteService.pruQuoteParamSave(pruquoteparm);
		
		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");
		
		if(!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(), "occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1",1 ,principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);			
		}
		if(!occpo.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(), "occpo");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo), "occpo",1 ,principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occpo));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if(!occla2.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(), "occla2");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2), "occla2",1 ,principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla2));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		
		return "redirect:/prumyfamily";
	}
	@RequestMapping(value =  "/prumyfamily/{id}/edit", method = RequestMethod.POST, params="previewkh")
	public String processEditprumyfamilyFormKh(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,@PathVariable("id") Long id,Model model,Principal principal,HttpServletRequest request,RedirectAttributes redirectAttributes, HttpServletRequest req) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%BTR1%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%BTR1%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long)9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
			return "quote/editprumyfamily";						
		}
		if(!validatePrumyfamily(pruquoteparm)) {
            model.addAttribute("errors", "Invalid quotation for PRUmy family!");
            return newprumyfamilyForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR1");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("pruMyFamilyRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int isAffectiveDate = new Date().compareTo(affectiveDate);
		if (isAffectiveDate > 0) {
			pruquoteparm.setCoL1Rtr2(16);
			pruquoteparm.setL1Rtr2(new BigDecimal(0));
		}
		quoteService.pruQuoteParamSave(pruquoteparm);
//		model.addAttribute("previewurl", "/pruquote_myfamily_repkhmer/"+pruquoteparm.getId());
//		return editPruMyFamily(pruquoteparm.getId(),model);
		
		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");
		
		if(!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(), "occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1",1 ,principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);			
		}
		if(!occpo.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(), "occpo");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo), "occpo",1 ,principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occpo));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if(!occla2.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(), "occla2");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2), "occla2",1 ,principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla2));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		
		if(pruquoteparm.getCoL1Rtr2()==15){
			Object[] prusaver=(Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
			if(prusaver[3].equals("FALSE".toLowerCase())){
				redirectAttributes.addFlashAttribute("errorPruSaver","Prusaver MB should between "+prusaver[1]+"-"+prusaver[2]);
				return "redirect:/prumyfamily/"+pruquoteparm.getId()+"/edit";			
			}			
		}
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/pruquote_myfamily_repkhmer/"+pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview PruMyFamily Khmer Quote No " +pruquoteparm.getId(), principal.getName());
		return "redirect:/prumyfamily/"+pruquoteparm.getId()+"/edit";

	}
	@RequestMapping(value =  "/prumyfamily/{id}/edit", method = RequestMethod.POST, params="previewen")
	public String processEditprumyfamilyFormEn(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,@PathVariable("id") Long id,Model model,Principal principal,HttpServletRequest request,RedirectAttributes redirectAttributes, HttpServletRequest req) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%BTR1%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%BTR1%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long)9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
			return "quote/editprumyfamily";					
		}
		if(!validatePrumyfamily(pruquoteparm)) {
            model.addAttribute("errors", "Invalid quotation for PRUmy family!");
            return newprumyfamilyForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR1");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("pruMyFamilyRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int isAffectiveDate = new Date().compareTo(affectiveDate);
		if (isAffectiveDate > 0) {
			pruquoteparm.setCoL1Rtr2(16);
			pruquoteparm.setL1Rtr2(new BigDecimal(0));
		}
		quoteService.pruQuoteParamSave(pruquoteparm);
//		model.addAttribute("previewurl", "/pruquote_myfamily_replatin/"+pruquoteparm.getId());
//		return editPruMyFamily(pruquoteparm.getId(),model);
		
		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");
		
		if(!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(), "occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1",1 ,principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);			
		}
		if(!occpo.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(), "occpo");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo), "occpo",1 ,principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occpo));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if(!occla2.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(), "occla2");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2), "occla2",1 ,principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla2));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		
		if(pruquoteparm.getCoL1Rtr2()==15){
			Object[] prusaver=(Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
			if(prusaver[3].equals("FALSE".toLowerCase())){
				redirectAttributes.addFlashAttribute("errorPruSaver","Prusaver MB should between "+prusaver[1]+"-"+prusaver[2]);
				return "redirect:/prumyfamily/"+pruquoteparm.getId()+"/edit";			
			}			
		}
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/pruquote_myfamily_replatin/"+pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview PruMyFamily English Quote No " +pruquoteparm.getId(), principal.getName());
		return "redirect:/prumyfamily/"+pruquoteparm.getId()+"/edit";

	}
	private boolean validatePrumyfamily(PruquoteParm p) {
        double prumyfamilyMinBasicSA = Double.valueOf(tableSetupService.itemitemFindNameByReserve1("myfamilyMinBasicSA", 1).get(0).getReserve2());
        String pcbPrumyfamily = tableSetupService.itemitemFindNameByReserve1("pcbPRUmyfamily", 1).get(0).getReserve2();
        double pcbPrumyfamilyMinSAMultiply = Double.valueOf(tableSetupService.itemitemFindNameByReserve1("pcbPRUmyfamilyMinSAMultiply", 1).get(0).getReserve2());
        
        if(prumyfamilyMinBasicSA > 0 && pcbPrumyfamily != null && !pcbPrumyfamily.equals("") && 
                     pcbPrumyfamilyMinSAMultiply > 0) {
               double minBasicSA = p.getBasicPlanSa().doubleValue();
               String pcb = p.getCoL1Rtr1() + "";
               double pcbSA = p.getL1Rtr1().doubleValue();
               
               if(prumyfamilyMinBasicSA > minBasicSA)
                     return false;
               
               if(pcbPrumyfamily.equals("Yes") && pcb.equals("16"))
                     return false;
               
               if(pcbPrumyfamily.equals("Yes") && pcbSA == 0)
                     return false;
               
               if((prumyfamilyMinBasicSA * pcbPrumyfamilyMinSAMultiply) > pcbSA)
                     return false;
               
        }
        
        return true;
	}
	
//here is all function about for quote 
	private String processNewHelper(PruquoteParm pruquoteparm, BindingResult result, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req, Boolean isDD) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			return newprumyfamilyForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR1");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		if (!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/prumyfamily_pruquote_dd_form/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		attr.getRequest().getSession().setAttribute("isDD", isDD);
		securityService.saveLogWithInfo(request, "Preview PruMyFamily DD Form No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/prumyfamily/" + pruquoteparm.getId() + "/edit";
	}

	@RequestMapping(value = "/newprumyfamily", method = RequestMethod.POST, params = "previewDD")
	public String processNewprumyfamilyDDFormPreview(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest request,
			RedirectAttributes redirectAttributes, HttpServletRequest req) {
		
		return processNewHelper(pruquoteparm, result, model, principal, request, redirectAttributes, req, true);
	}

	@RequestMapping(value = "/newprumyfamily", method = RequestMethod.POST, params = "previewSO")
	public String processNewprumyfamilySOFormPreview(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest request,
			RedirectAttributes redirectAttributes, HttpServletRequest req) {

		return processNewHelper(pruquoteparm, result, model, principal, request, redirectAttributes, req, false);
	}

	@RequestMapping(value = "/prumyfamily_pruquote_dd_form/{quoteId}", method = RequestMethod.GET)
	public ModelAndView PruMyFamilyPreviewDDForm(@PathVariable("quoteId") Long quoteId, Principal principal,
			HttpServletRequest req) throws ParseException {
		ModelAndView model = new ModelAndView();
		
		Map<String, Object> generateDD = null;

		try {
			generateDD = ddService.generateDD(quoteId, "BTR1");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (generateDD == null)
			throw new ResourceNotFoundException();

		for (String key : generateDD.keySet()) {
			model.addObject(key, generateDD.get(key));
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		
		Object isDD = attr.getRequest().getSession().getAttribute("isDD");
		model.addObject("isDD", isDD);
		
		model.addObject("version", GlobalVar.version);

		Object onbehalf = attr.getRequest().getSession().getAttribute("isonbehalf");
		model.addObject("isonbehalf", onbehalf);
		attr.getRequest().getSession().removeAttribute("isonbehalf");
		attr.getRequest().getSession().removeAttribute("isDD");

		// if (onbehalf.equals("15")) {
		// String action = "Print DD Form on Quote Id = " + quoteId + "and On be half =
		// " + "Yes";
		// String ip = req.getRemoteHost();
		// String pip = req.getRemoteHost();
		// Date date = new Date();
		// logService.logUserAction(user.getUserId(), action, ip, pip, date);
		// }

		model.setViewName("report/PruQuote_DD_Form");
		return model;
	}

	private String processEditHelper(PruquoteParm pruquoteparm, BindingResult result, Long id, Model model,
			Principal principal, HttpServletRequest request, RedirectAttributes redirectAttributes,
			HttpServletRequest req, Boolean isDD) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR1%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR1%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long) 9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
			return "quote/editprumyfamily";
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR1");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		
		
		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		
		if (!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/prumyfamily_pruquote_dd_form/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		attr.getRequest().getSession().setAttribute("isDD", isDD);
		
		securityService.saveLogWithInfo(request, "Preview PruMyFamily Khmer Quote No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/prumyfamily/" + pruquoteparm.getId() + "/edit";
	}

	@RequestMapping(value = "/prumyfamily/{id}/edit", method = RequestMethod.POST, params = "previewDD")
	public String processEditprumyfamilyPreviewDDForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req) {
		return processEditHelper(pruquoteparm, result, id, model, principal, request, redirectAttributes, req, true);
	}
	
	@RequestMapping(value = "/prumyfamily/{id}/edit", method = RequestMethod.POST, params = "previewSO")
	public String processEditprumyfamilyPreviewSOForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req) {
		return processEditHelper(pruquoteparm, result, id, model, principal, request, redirectAttributes, req, false);
	}

}