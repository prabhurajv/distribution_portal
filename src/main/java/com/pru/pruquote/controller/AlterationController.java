package com.pru.pruquote.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pru.pruquote.model.Alteration;
import com.pru.pruquote.model.CollectionAndLapse;
import com.pru.pruquote.service.AlterationService;

@Controller
@PreAuthorize("hasAuthority('ACCESS_ALTERATION')")
public class AlterationController {
	@Autowired
	private AlterationService alterationService;
	
	@ResponseBody
	@RequestMapping(value =  "/countalterations", method = RequestMethod.GET)
	public String countAlterations(Principal principal) {
		String result = "";
		String userid = principal.getName();
		ObjectMapper mapper = new ObjectMapper();
		List<Alteration> getAllAlteration = alterationService.getListAlteration(userid);
		Long countCompleted = getAllAlteration.stream().filter(s -> s.getStatus().trim().equals("Completed")).count();
		Long countPending = getAllAlteration.stream().filter(s -> s.getStatus().trim().equals("Pending")).count();
		List<Object> obj = new ArrayList<Object>();
		obj.add(countCompleted);	
		obj.add(countPending);
		try {
			result = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value =  "/getalterations", method = RequestMethod.GET)
	public String getAlterations(Principal principal) {
		String result = "";
		String userid = principal.getName();
		ObjectMapper mapper = new ObjectMapper();
		List<Alteration> getAllAlteration = alterationService.getListAlteration(userid);
		Stream<Alteration> getAlterationCompleted = getAllAlteration.stream().filter(s -> s.getStatus().trim().equals("Completed"));
		Stream<Alteration> getAlterationPending = getAllAlteration.stream().filter(s -> s.getStatus().trim().equals("Pending"));
		List<Object> obj = new ArrayList<Object>();
		obj.add(getAlterationCompleted.collect(Collectors.toList()));	
		obj.add(getAlterationPending.collect(Collectors.toList()));
		try {
			result = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}
}