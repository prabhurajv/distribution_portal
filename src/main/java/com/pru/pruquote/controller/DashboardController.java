
package com.pru.pruquote.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pru.pruquote.gluster.GlusterAuthnFailedException;
import com.pru.pruquote.gluster.GlusterFs;
import com.pru.pruquote.model.CodeDesc;
import com.pru.pruquote.model.CollectionAndLapse;
import com.pru.pruquote.model.FailWelcomeCall;
import com.pru.pruquote.model.Issuance;
import com.pru.pruquote.model.PendingAck;
import com.pru.pruquote.model.PendingCase;
import com.pru.pruquote.model.PendingCaseAgentName;
import com.pru.pruquote.model.PendingCaseAgentNumber;
import com.pru.pruquote.model.PendingCaseAgentPhone;
import com.pru.pruquote.model.PendingCaseApe;
import com.pru.pruquote.model.PendingCaseApplicationNumber;
import com.pru.pruquote.model.PendingCaseChdrKeyDate;
import com.pru.pruquote.model.PendingCaseClientName;
import com.pru.pruquote.model.PendingCaseFoNum;
import com.pru.pruquote.model.PendingCaseNtuDate;
import com.pru.pruquote.model.PendingCasePenalDoctor;
import com.pru.pruquote.model.PendingCasePendingPeriod;
import com.pru.pruquote.model.PendingCasePolicyNumber;
import com.pru.pruquote.model.PendingCasePolicyPhoneNumber;
import com.pru.pruquote.model.PendingCaseStatus;
import com.pru.pruquote.model.Persistency;
import com.pru.pruquote.model.PersistencyDetail;
import com.pru.pruquote.model.Submission;
import com.pru.pruquote.property.IEapsProperties;
import com.pru.pruquote.property.IPendingCase;
import com.pru.pruquote.service.CollectionandLapseService;
import com.pru.pruquote.service.FailWelcomeCallService;
import com.pru.pruquote.service.LogService;
import com.pru.pruquote.service.PendingCaseService;
import com.pru.pruquote.service.ReferralService;
import com.pru.pruquote.utility.AES;
import com.pru.pruquote.utility.AnonymousHelper;

@Controller
@PreAuthorize("hasAnyAuthority('ACCESS_PENDING,ACCESS_PERSISTENCY')")
public class DashboardController {

	@Autowired
	private CollectionandLapseService collectionAndLapseService;
	
	@Autowired
	private PendingCaseService pendingCaseService;

	@Autowired
	private ReferralService referralService;

	@Autowired
	private IEapsProperties properties;
	
	@Autowired
	private LogService logService;
	
	@Autowired
	private FailWelcomeCallService failWcService;

	@RequestMapping(value = "/mypending", method = RequestMethod.GET)
	public ModelAndView pendingForm(Principal principal, HttpServletRequest req) {
		ModelAndView model = new ModelAndView();
		String status = req.getParameter("status");
		String days = "";
		if (status != null && status.equals("NTUDATE")) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.DATE, Integer.parseInt(req.getParameter("day")));
			days = sdf.format(new Date()) + "-" + sdf.format(c.getTime());
		} else if (status != null
				&& (status.equals("IS") || status.equals("IA") || status.equals("ME") || status.equals("CAL"))) {
			days = req.getParameter("day");
		}
		if (status != null) {
			model.addObject("status", status);
			model.addObject("days", days);
		}

		model.addObject("pageTitle", "My Pending");
		model.addObject("getAgent",
				referralService.listSearchReferralMgtParam(principal.getName(), "", "", "", "", "", "").get(5));
		model.setViewName("pending/mypending");
		return model;
	}

	@RequestMapping(value = "/getAllPending", method = RequestMethod.GET)
	@ResponseBody
	public String AllPending(Principal principal) {
		String userid = principal.getName();
		List<PendingCase> allMyPendingCase = pendingCaseService.pendingCases(userid);
		Stream<PendingCase> allCalPendingCase = allMyPendingCase.stream()
				.filter(s -> s.getPendingStatus().equals("CAL"));
		Stream<PendingCase> allMePendingCase = allMyPendingCase.stream()
				.filter(s -> s.getPendingStatus().startsWith("ME"));
		Stream<PendingCase> allIsPendingCase = allMyPendingCase.stream()
				.filter(s -> s.getPendingStatus().startsWith("IA"));
		allMyPendingCase.sort((p1, p2) -> p2.getRefreshTime().compareTo(p1.getRefreshTime()));

		String totalMypending = allMyPendingCase.size() + "";
		String totalMePending = allMePendingCase.count() + "";
		String totalCalPending = allCalPendingCase.count() + "";
		String totalIsPending = allIsPendingCase.count() + "";
		String refreshTime = allMyPendingCase.get(0).getRefreshTime() + "";

		String allPendings = totalMypending + "," + totalMePending + "," + totalIsPending + "," + totalCalPending + ","
				+ refreshTime;
		return allPendings;
	}

	@RequestMapping(value = "/getPersistencies", method = RequestMethod.GET)
	@ResponseBody
	public String GetPersistencies(Principal principal) {
		String result = "";
		String userid = principal.getName();
		ObjectMapper mapper = new ObjectMapper();
		List<Persistency> allPersistencies = pendingCaseService.selectPersistency(userid);
		// Persistency s1 = new Persistency(new BigDecimal(10), new
		// BigDecimal(20), new BigDecimal(30));
		// Persistency s2 = new Persistency(new BigDecimal(10), new
		// BigDecimal(20), new BigDecimal(30));
		// Persistency s3 = new Persistency(new BigDecimal(10), new
		// BigDecimal(20), new BigDecimal(30));
		// allPersistencies.add(s1);
		// allPersistencies.add(s2);
		// allPersistencies.add(s3);

		BigDecimal sumif = allPersistencies.stream().map(Persistency::getIfApe).reduce(BigDecimal.ZERO,
				BigDecimal::add);

		BigDecimal sumla = allPersistencies.stream().map(Persistency::getLaApe).reduce(BigDecimal.ZERO,
				BigDecimal::add);

		BigDecimal sumsu = allPersistencies.stream().map(Persistency::getSuApe).reduce(BigDecimal.ZERO,
				BigDecimal::add);

		Double percent = (1 - (Double.parseDouble(sumla.toString()) + Double.parseDouble(sumsu.toString()))
				/ (Double.parseDouble(sumif.toString())
						+ (Double.parseDouble(sumla.toString()) + Double.parseDouble(sumsu.toString()))))
				* 100;

		List<Object> obj = new ArrayList<Object>();
		obj.add(sumif);
		obj.add(sumla);
		obj.add(sumsu);
		obj.add(percent);
		obj.add(allPersistencies);

		try {
			result = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return result;
	}
	
	@RequestMapping(value = "/csvpersistencies")
	public void GetCSVPersistencies(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String userid = request.getUserPrincipal().getName();
		String isimpactpersistency = request.getParameter("isimpactpersistency") == null ? null
				: request.getParameter("isimpactpersistency").toString();
		
		//REQUEST FROM TICKET 2349 , get only LA, SU, TR and it is the valid , move it into service for any criteria	
		List<CollectionAndLapse> list = collectionAndLapseService.getListPersistencies(userid,isimpactpersistency);
		AnonymousHelper.ExportCSV(response, list, new CollectionAndLapse(), "Persistencies Detail-" + isimpactpersistency + "-" + new Date());
	}
	
	@RequestMapping(value = "/csvpersistenciescount")
	@ResponseBody
	public int GetPersistenciesCount(HttpServletRequest request) {
		int result = 0;
		String userid = request.getUserPrincipal().getName();
		String isimpactpersistency = request.getParameter("isimpactpersistency") == null ? null
				: request.getParameter("isimpactpersistency").toString();
		
		result = collectionAndLapseService.getCountPersistencies(userid, isimpactpersistency); 
		
		return result;
	}
	
	@RequestMapping(value = "/logviewpersistency", method = RequestMethod.GET)
	@ResponseBody
	public void LogViewPersistency(Principal principal, HttpServletRequest req) {
		String userid = principal.getName();
		String action = "Log View Persistency";
		String ip = req.getRemoteHost();
		String pip = req.getRemoteHost();
		Date date = new Date();
		logService.logUserAction(userid, action, ip, pip, date);
	}
	
	@RequestMapping(value = "/getpersistenciessumarry", method = RequestMethod.GET)
	@ResponseBody
	public String GetPersistenciesSumarry(Principal principal) {
		String result = "";
		String userid = principal.getName();
		ObjectMapper mapper = new ObjectMapper();
		List<Object[]> persistencySumarry = pendingCaseService.selectPersistencySummary(userid);

		try {
			result = mapper.writeValueAsString(persistencySumarry);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return result;
	}

	@RequestMapping(value = "/getAllCodeDesc", method = RequestMethod.GET)
	@ResponseBody
	public String AllCodeDesc() {
		String result = "";
		ObjectMapper mapper = new ObjectMapper();
		List<CodeDesc> allCodeDescs = pendingCaseService.codeDesc();

		try {
			result = mapper.writeValueAsString(allCodeDescs);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return result;
	}

	@RequestMapping(value = "/getAllPendingSummary", method = RequestMethod.GET)
	@ResponseBody
	public String getAllStatus(Principal principal, HttpServletRequest req) {
		String userid = principal.getName();
		String summaryby = req.getParameter("summaryby");
		String result = "";
		ObjectMapper mapper = new ObjectMapper();
		List<Object[]> allStatus = pendingCaseService.selectAllStatus(userid, summaryby);
		try {
			result = mapper.writeValueAsString(allStatus);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping(value = "/getListSubmission", method = RequestMethod.GET)
	@ResponseBody
	public String getListSubmission(Principal principal, HttpServletRequest req) throws ParseException {
		String userid = principal.getName();
		String result = "";
		String weekth = req.getParameter("weekth");
		String summaryby = req.getParameter("summaryby");
		ObjectMapper mapper = new ObjectMapper();
		Date today = new SimpleDateFormat("dd/MM/yyyy").parse(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
		Date yesterday = new SimpleDateFormat("dd/MM/yyyy").parse(AnonymousHelper.getYesterdayDate(today));

		List<Submission> allSubmission = pendingCaseService.listSubmission(userid, summaryby);
		if (weekth.equals("Today")) {
			allSubmission = allSubmission.stream().filter(a -> a.getSubDate().equals(today))
					.collect(Collectors.toList());
		} else if (weekth.equals("Yesterday")) {
			allSubmission = allSubmission.stream().filter(a -> a.getSubDate().equals(yesterday))
					.collect(Collectors.toList());
		} else if (weekth.equals("Month to Date")) {
			allSubmission = allSubmission.stream().filter(a -> AnonymousHelper.isMonthToDate(today, a.getSubDate()))
					.collect(Collectors.toList());
		} else {
			allSubmission = allSubmission.stream().filter(a -> a.getWeekth().equals(weekth))
					.collect(Collectors.toList());
		}

		try {
			result = mapper.writeValueAsString(allSubmission);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping(value = "/getListNetIssue", method = RequestMethod.GET)
	@ResponseBody
	public String getListNetIssue(Principal principal, HttpServletRequest req) throws ParseException {
		String userid = principal.getName();
		String result = "";
		String weekth = req.getParameter("weekth");
		String summaryby = req.getParameter("summaryby");
		ObjectMapper mapper = new ObjectMapper();
		Date today = new SimpleDateFormat("dd/MM/yyyy").parse(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
		Date yesterday = new SimpleDateFormat("dd/MM/yyyy").parse(AnonymousHelper.getYesterdayDate(today));

		List<Issuance> allIssuance = pendingCaseService.listIssue(userid, summaryby);
		if (weekth.equals("Today")) {
			allIssuance = allIssuance.stream().filter(a -> a.getChdrIssDate().equals(today))
					.collect(Collectors.toList());
		} else if (weekth.equals("Yesterday")) {
			allIssuance = allIssuance.stream().filter(a -> a.getChdrIssDate().equals(yesterday))
					.collect(Collectors.toList());
		} else if (weekth.equals("Month to Date")) {
			allIssuance = allIssuance.stream().filter(a -> AnonymousHelper.isMonthToDate(today, a.getChdrIssDate()))
					.collect(Collectors.toList());
		} else {
			allIssuance = allIssuance.stream().filter(a -> a.getWeekth().equals(weekth)).collect(Collectors.toList());
		}

		try {
			result = mapper.writeValueAsString(allIssuance);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping(value = "/MyPendingCase", method = RequestMethod.GET)
	@ResponseBody
	public String MyPendingCase(Principal principal, HttpServletRequest req) {
		String result = "";
		String userid = principal.getName();
		ObjectMapper mapper = new ObjectMapper();

		List<PendingCase> allMyPendingCase = pendingCaseService.pendingCases(userid);

		String fonum = req.getParameter("fonum").toString();
		if (fonum != null && !fonum.equals("")) {
			IPendingCase foNum = new PendingCaseFoNum();
			allMyPendingCase = foNum.filterPendingCase(allMyPendingCase, fonum, "=");
		}

		String chdrnum = req.getParameter("chdrnum").toString();
		if (!chdrnum.equals("")) {
			IPendingCase policyNumber = new PendingCasePolicyNumber();
			allMyPendingCase = policyNumber.filterPendingCase(allMyPendingCase, chdrnum, "=");
		}

		String appnum = req.getParameter("appnum").toString();
		if (!appnum.equals("")) {
			IPendingCase applicationNumber = new PendingCaseApplicationNumber();
			allMyPendingCase = applicationNumber.filterPendingCase(allMyPendingCase, appnum, "=");
		}

		String po_name = req.getParameter("po_name").toString();
		if (!po_name.equals("")) {
			IPendingCase policyName = new PendingCaseClientName();
			allMyPendingCase = policyName.filterPendingCase(allMyPendingCase, po_name, "like");
		}

		String chdr_ape = req.getParameter("chdr_ape").toString();
		if (!chdr_ape.equals("")) {
			String chdr_apeoperator = req.getParameter("chdr_apeoperator").toString();
			IPendingCase ape = new PendingCaseApe();
			if (chdr_apeoperator.equals("between")) {
				String chdr_apeto = req.getParameter("chdr_apeto").toString();
				allMyPendingCase = ape.filterPendingCase(allMyPendingCase, chdr_ape, chdr_apeoperator, chdr_apeto);
			} else {
				allMyPendingCase = ape.filterPendingCase(allMyPendingCase, chdr_ape, chdr_apeoperator);
			}
		}

		String status = req.getParameter("status");
		if (!status.equals("")) {
			IPendingCase statuses = new PendingCaseStatus();
			allMyPendingCase = statuses.filterPendingCase(allMyPendingCase, status, "=");
		}

		String po_phonenumber = req.getParameter("po_phonenumber");
		if (!po_phonenumber.equals("")) {
			IPendingCase policyPhoneNumber = new PendingCasePolicyPhoneNumber();
			allMyPendingCase = policyPhoneNumber.filterPendingCase(allMyPendingCase, po_phonenumber, "=");
		}

		String agnt_num = req.getParameter("agnt_num");
		if (!agnt_num.equals("")) {
			IPendingCase agentNumber = new PendingCaseAgentNumber();
			allMyPendingCase = agentNumber.filterPendingCase(allMyPendingCase, agnt_num, "=");
		}

		String agent_name = req.getParameter("agent_name");
		if (!agent_name.equals("")) {
			IPendingCase agentName = new PendingCaseAgentName();
			allMyPendingCase = agentName.filterPendingCase(allMyPendingCase, agent_name, "=");
		}

		String agnt_phone = req.getParameter("agnt_phone");
		if (!agnt_phone.equals("")) {
			IPendingCase agentPhone = new PendingCaseAgentPhone();
			allMyPendingCase = agentPhone.filterPendingCase(allMyPendingCase, agnt_phone, "=");
		}

		String penal_doctor = req.getParameter("penal_doctor");
		if (!penal_doctor.equals("")) {
			IPendingCase penalDoctor = new PendingCasePenalDoctor();
			allMyPendingCase = penalDoctor.filterPendingCase(allMyPendingCase, penal_doctor, "=");
		}

		String chdr_key_date = req.getParameter("chdr_key_date");
		if (!chdr_key_date.equals("")) {
			String chdr_key_dateoperator = req.getParameter("chdr_key_dateoperator");
			IPendingCase chdrKeyDate = new PendingCaseChdrKeyDate();
			if (chdr_key_dateoperator.equals("between")) {
				String chdr_key_dateto = req.getParameter("chdr_key_dateto");
				allMyPendingCase = chdrKeyDate.filterPendingCase(allMyPendingCase, chdr_key_date, chdr_key_dateoperator,
						chdr_key_dateto);
			} else {
				allMyPendingCase = chdrKeyDate.filterPendingCase(allMyPendingCase, chdr_key_date,
						chdr_key_dateoperator);
			}
		}

		String ntu_date = req.getParameter("ntu_date");
		if (!ntu_date.equals("")) {
			String ntu_dateoperator = req.getParameter("ntu_dateoperator");
			IPendingCase ntuDate = new PendingCaseNtuDate();
			if (ntu_dateoperator.equals("between")) {
				String ntu_dateto = req.getParameter("ntu_dateto");
				allMyPendingCase = ntuDate.filterPendingCase(allMyPendingCase, ntu_date, ntu_dateoperator, ntu_dateto);
			} else {
				allMyPendingCase = ntuDate.filterPendingCase(allMyPendingCase, ntu_date, ntu_dateoperator);
			}

		}

		String pending_period = req.getParameter("pending_period");
		if (!pending_period.equals("")) {
			String pending_periodoperator = req.getParameter("pending_periodoperator");
			IPendingCase pendingPeriod = new PendingCasePendingPeriod();
			if (pending_periodoperator.equals("between")) {
				String pending_periodto = req.getParameter("pending_periodto");
				allMyPendingCase = pendingPeriod.filterPendingCase(allMyPendingCase, pending_period,
						pending_periodoperator, pending_periodto);
			} else {
				allMyPendingCase = pendingPeriod.filterPendingCase(allMyPendingCase, pending_period,
						pending_periodoperator);
			}
		}

		try {
			result = mapper.writeValueAsString(allMyPendingCase);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping(value = "/getAllSubAndIssue", method = RequestMethod.GET)
	@ResponseBody
	public String getAllSubAndIssue(Principal principal) {
		String userid = principal.getName();
		String result = "";
		ObjectMapper mapper = new ObjectMapper();

		List<Object[]> allSubAndIssue = pendingCaseService.selectAllSubAndIssue(userid);
		try {
			result = mapper.writeValueAsString(allSubAndIssue);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping(value = "/getimmediateaction", method = RequestMethod.GET)
	@ResponseBody
	public String GetImmediateActioin(Principal principal) {
		String result = "";
		String userid = principal.getName();
		ObjectMapper mapper = new ObjectMapper();
		List<Object> selectAllImmediateActioins = pendingCaseService.selectImmediateaction(userid);

		try {
			result = mapper.writeValueAsString(selectAllImmediateActioins);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return result;
	}
	
	@RequestMapping(value = "/getcustomertofollowup", method = RequestMethod.GET)
	@ResponseBody
	public String GetCustomertoFollowup(Principal principal) {
		String result = "";
		String userid = principal.getName();
		ObjectMapper mapper = new ObjectMapper();
		List<Object[]> getCustomertoFollowup = referralService.listReferralToFollowup(userid);

		try {
			result = mapper.writeValueAsString(getCustomertoFollowup);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return result;
	}

	@RequestMapping(value = "/MyPendingCase/download", method = RequestMethod.GET)
	public String downloadPolicy(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		String pendingStatus = request.getParameter("StatusCode");
		String policyNum = request.getParameter("PolicyNumber");
		String fupNo = request.getParameter("FollowUpNumber");
		String pendingStatusGroup = "";
		String fileName = "";

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		GlusterFs fs = new GlusterFs();
		String username = AES.decrypt(properties.getUser());
		String pwd = AES.decrypt(properties.getPwd());
		// String username = "aps:swiftTest";
		// String pwd = "testing";
		String ip = "";
		Date downloadDate = new Date();
		try {
			if (pendingStatus != null && policyNum != null && fupNo != null) {
				if (pendingCaseService.isPolicyOwner(auth.getName(), policyNum)) {
					if (pendingStatus.startsWith("ME") || pendingStatus.startsWith("IS"))
						pendingStatusGroup = pendingStatus.substring(0, 2);
					else if (pendingStatus.startsWith("CAL"))
						pendingStatusGroup = pendingStatus;

					fileName = pendingStatusGroup + "/" + pendingStatus + "-" + policyNum + "-" + fupNo;

					fs.authenticate(username, pwd);
					fs.getFileToLocal(fileName, response, true);
				}
			}

		} catch (GlusterAuthnFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("exMsg", e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("exMsg", e.getMessage());
		}

		return "downloadletter";
	}
	@PreAuthorize("hasAnyAuthority('ACCESS_FAIL_WC')")
    @RequestMapping(value = "/failwelcomecall", method = RequestMethod.GET)
    @ResponseBody
    public String getFailWelcomeCall(Principal principal) throws JsonProcessingException {
           String result = "";
           String userid = principal.getName();
           ObjectMapper mapper = new ObjectMapper();
           
           List<FailWelcomeCall> list = failWcService.listFailWelcomeCall(userid);
           
           if(list != null) {
                  result = mapper.writeValueAsString(list);
           }
           
           return result;
    }
}