package com.pru.pruquote.controller;


import java.math.BigDecimal;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.owasp.html.Sanitizers;
import org.owasp.html.PolicyFactory;

import com.pru.pruquote.model.Itemitem;
import com.pru.pruquote.model.PruquoteParm;
import com.pru.pruquote.model.User;
import com.pru.pruquote.model.listPruquote;
import com.pru.pruquote.service.QuoteService;
import com.pru.pruquote.service.SecurityService;
import com.pru.pruquote.service.TableSetupService;
import com.pru.pruquote.utility.AnonymousHelper;
import com.pru.pruquote.utility.GlobalVar;
import com.pru.pruquote.service.ListQuoteService;
import com.pru.pruquote.service.LogService;;

@Controller
@PreAuthorize("hasAuthority('ACCESS_EDUSAVE')")
public class EduSaveController {	
	
	@Autowired	
	private QuoteService quoteService;
	
	@Autowired	
	private ListQuoteService listQuoteService;
	
	@Autowired	
	private TableSetupService tableSetupService;
	
	@Autowired	
	private SecurityService securityService;
	
	@Autowired
	private LogService logService;
	
	@ModelAttribute("pruquoteparm")
    public PruquoteParm pruquoteparmConstructor(){
    	return new PruquoteParm();
    }
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
        binder.registerCustomEditor(Date.class,"commDate",new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class,"la1dateOfBirth",new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class,"syndate",new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class,"la2dateOfBirth",new CustomDateEditor(dateFormat, false));
    }				

	@RequestMapping(value =  "/pruquote_edusave_repkhmer/{quoteId}", method = RequestMethod.GET)
	public ModelAndView eduSavePreviewKhmer(@PathVariable("quoteId") Long quoteId,Principal principal, HttpServletRequest req) throws ParseException {
		ModelAndView model = new ModelAndView();	
		List<listPruquote> listPruquoteParms=listQuoteService.listQuote(principal.getName(), Long.toString(quoteId));
		List<PruquoteParm> pruquoteParms=quoteService.findValidQuote(quoteId, "BTR2", principal.getName());
		if(listPruquoteParms.size()<=0 & pruquoteParms.size()<=0){
			throw new ResourceNotFoundException();
		}
		else if (listPruquoteParms.size()>=0){
			pruquoteParms=quoteService.findValidQuote(quoteId);
			//model.addObject("name","");
			//model.addObject("code","");
		}
		else {
			if(pruquoteParms.size()<=0) {
				throw new ResourceNotFoundException();
				}
			else{
				//User user=securityService.userFindOne(principal.getName());
				//model.addObject("name",user.getFullNameKH());
				//model.addObject("code",user.getUserId());	
			}
		}
		List<String> results=quoteService.generateEduSaveKhmerQuote(quoteId, "Khmer","RN");
		model.addObject("header1",results.get(0));
		model.addObject("body1",results.get(1));
		model.addObject("body2",results.get(2));
		model.addObject("body3A",results.get(3));
		model.addObject("body3B",results.get(4));
		model.addObject("footer1",results.get(5));
		model.addObject("footer2",results.get(6));
		model.addObject("serialno1",results.get(7));
		model.addObject("serialno2",results.get(8));
		model.addObject("body4",results.get(9));
		model.addObject("body5",results.get(10));
		model.addObject("body5",results.get(10));
		model.addObject("PS",results.get(11));
	
		User user=securityService.findUserByUsername(pruquoteParms.get(0).getCoUser());
		model.addObject("name",user.getFullNameKh());
		model.addObject("code",user.getUserId());
		
		if(pruquoteParms.get(0).getCoL1Rtr2()==15)
			model.setViewName("report/PruQuote_EducarePlus_RepKhmer");
		else
			model.setViewName("report/PruQuote_EducarePlus_RepKhmer_NO_Saver");
		
		model.addObject("version", GlobalVar.version);
		
		List<Itemitem> eduSaveExpiredDate = tableSetupService.itemitemFindNameByReserve1("eduSaveExpiredDate", 1);
		List<Itemitem> eduSaveExpiredMsg = tableSetupService.itemitemFindNameByReserve1("eduSaveExpiredMsg", 1);
		if(eduSaveExpiredDate != null && eduSaveExpiredMsg != null && 
				eduSaveExpiredMsg.size() > 0 && eduSaveExpiredDate.size() > 0) {
			model.addObject("eduSaveExpiredDate", new Date().compareTo(AnonymousHelper.getFormattedDate(eduSaveExpiredDate.get(0).getReserve2()
					, "yyyy-MM-dd")));
			model.addObject("eduSaveExpiredMsg", eduSaveExpiredMsg.get(0).getReserve2());
		}
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object onbehalf = attr.getRequest().getSession().getAttribute("isonbehalf");
		model.addObject("isonbehalf", onbehalf);
		attr.getRequest().getSession().removeAttribute("isonbehalf");
		
		if (onbehalf.equals("15")) {
			String action = "Quote Id = " + quoteId + "On be half = " + "Yes";
			String ip = req.getRemoteHost();
			String pip = req.getRemoteHost();
			Date date = new Date();
			logService.logUserAction(user.getUserId(), action, ip, pip, date);
		}
		
		return model;
	}

	@RequestMapping(value =  "/pruquote_edusave_replatin/{quoteId}", method = RequestMethod.GET)
	public ModelAndView eduSavePreviewLatin(@PathVariable("quoteId") Long quoteId,Principal principal, HttpServletRequest req) throws ParseException {
		ModelAndView model = new ModelAndView();
		
		List<listPruquote> listPruquoteParms=listQuoteService.listQuote(principal.getName(), Long.toString(quoteId));
		List<PruquoteParm> pruquoteParms=quoteService.findValidQuote(quoteId, "BTR2", principal.getName());
		if(listPruquoteParms.size()<=0 & pruquoteParms.size()<=0){
			throw new ResourceNotFoundException();
		}
		else if (listPruquoteParms.size()>=0){
			pruquoteParms=quoteService.findValidQuote(quoteId);
			//model.addObject("name","");
			//model.addObject("code","");
		}
		else {
			if(pruquoteParms.size()<=0) {
				throw new ResourceNotFoundException();
				}
			else{
				//User user=securityService.userFindOne(principal.getName());
				//model.addObject("name",user.getFullNameKH());
				//model.addObject("code",user.getUserId());	
			}
		}
		
		List<String> results=quoteService.generateEduSaveLatinQuote(quoteId, "Latin","RN");
		model.addObject("header1",results.get(0));
		model.addObject("body1",results.get(1));
		model.addObject("body2",results.get(2));
		model.addObject("body3",results.get(3));
		model.addObject("footer1",results.get(4));
		model.addObject("serialno1",results.get(5));
		model.addObject("serialno2",results.get(6));
		model.addObject("body4",results.get(7));
		model.addObject("body5",results.get(8));
		model.addObject("PS",results.get(9));
		
		User user = securityService.findUserByUsername(pruquoteParms.get(0).getCoUser());
		model.addObject("name",user.getFullNameKh());
		model.addObject("code",user.getUserId());
		
		if(pruquoteParms.get(0).getCoL1Rtr2()==15)
			model.setViewName("report/PruQuote_EducarePlus_RepLatin");
		else
			model.setViewName("report/PruQuote_EducarePlus_RepLatin");
		
		model.addObject("version", GlobalVar.version);
		
		List<Itemitem> eduSaveExpiredDate = tableSetupService.itemitemFindNameByReserve1("eduSaveExpiredDate", 1);
		List<Itemitem> eduSaveExpiredMsg = tableSetupService.itemitemFindNameByReserve1("eduSaveExpiredMsg", 1);
		if(eduSaveExpiredDate != null && eduSaveExpiredMsg != null && 
				eduSaveExpiredMsg.size() > 0 && eduSaveExpiredDate.size() > 0) {
			model.addObject("eduSaveExpiredDate", new Date().compareTo(AnonymousHelper.getFormattedDate(eduSaveExpiredDate.get(0).getReserve2()
							, "yyyy-MM-dd")));
			model.addObject("eduSaveExpiredMsg", eduSaveExpiredMsg.get(0).getReserve2());
		}
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object onbehalf = attr.getRequest().getSession().getAttribute("isonbehalf");
		model.addObject("isonbehalf", onbehalf);
		attr.getRequest().getSession().removeAttribute("isonbehalf");
		
		if (onbehalf.equals("15")) {
			String action = "Quote Id = " + quoteId + "On be half = " + "Yes";
			String ip = req.getRemoteHost();
			String pip = req.getRemoteHost();
			Date date = new Date();
			logService.logUserAction(user.getUserId(), action, ip, pip, date);
		}
		
		return model;
	}		
	@RequestMapping(value =  "/edusave", method = RequestMethod.GET)
	public ModelAndView edusaveForm(Principal principal) {
		ModelAndView model = new ModelAndView();			
		model.addObject("edusaveList", quoteService.viewQuote("BTR2", principal.getName()));
		model.addObject("pageTitle", "EduSave");
		model.setViewName("quote/edusave");
		return model;
	}
	@RequestMapping(value =  "/newedusave", method = RequestMethod.GET)
	public String newedusaveForm(Model model) {
		model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
		model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%BTR2%"));
		model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%BTR2%"));
		model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
		model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long)9));
		model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
		model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
		model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("eduSaveRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("isAffectiveDate", new Date().compareTo(affectiveDate));
		model.addAttribute("pageTitle", "New EduSave");
		return "quote/newedusave";
	}
	@RequestMapping(value =  "/newedusave", method = RequestMethod.POST,params="save")
	public String processNewedusaveForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,Model model,Principal principal) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newedusaveForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR2");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("pruMyFamilyRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int isAffectiveDate = new Date().compareTo(affectiveDate);
		if (isAffectiveDate > 0) {
			pruquoteparm.setCoL1Rtr2(16);
			pruquoteparm.setL1Rtr2(new BigDecimal(0));
		}
		quoteService.pruQuoteParamSave(pruquoteparm);
		return "redirect:/edusave";
	}
	@RequestMapping(value =  "/newedusave", method = RequestMethod.POST, params="previewkh")
	public String processNewedusaveFormPreviewKh(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,Model model,Principal principal,HttpServletRequest request,RedirectAttributes redirectAttributes) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newedusaveForm(model);			
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR2");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("pruMyFamilyRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int isAffectiveDate = new Date().compareTo(affectiveDate);
		if (isAffectiveDate > 0) {
			pruquoteparm.setCoL1Rtr2(16);
			pruquoteparm.setL1Rtr2(new BigDecimal(0));
		}
		quoteService.pruQuoteParamSave(pruquoteparm);
		if(pruquoteparm.getCoL1Rtr2()==15){
			Object[] prusaver=(Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
			if(prusaver[3].equals("FALSE".toLowerCase())){
				redirectAttributes.addFlashAttribute("errorPruSaver","Prusaver MB should between "+prusaver[1]+"-"+prusaver[2]);
				return "redirect:/edusave/"+pruquoteparm.getId()+"/edit";			
			}			
		}
//		model.addAttribute("previewurl", "/pruquote_edusave_repkhmer/"+pruquoteparm.getId());
//		return editEduSave(pruquoteparm.getId(),model);
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/pruquote_edusave_repkhmer/"+pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", request.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview EduSave Khmer Quote No " +pruquoteparm.getId(), principal.getName());
		return "redirect:/edusave/"+pruquoteparm.getId()+"/edit";
	}
	@RequestMapping(value =  "/newedusave", method = RequestMethod.POST, params="previewen")
	public String processNewedusaveFormPreviewEn(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,Model model,Principal principal,HttpServletRequest request,RedirectAttributes redirectAttributes) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newedusaveForm(model);			
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR2");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("pruMyFamilyRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int isAffectiveDate = new Date().compareTo(affectiveDate);
		if (isAffectiveDate > 0) {
			pruquoteparm.setCoL1Rtr2(16);
			pruquoteparm.setL1Rtr2(new BigDecimal(0));
		}
		quoteService.pruQuoteParamSave(pruquoteparm);
//		model.addAttribute("previewurl", "/pruquote_edusave_replatin/"+pruquoteparm.getId());
//		return editEduSave(pruquoteparm.getId(),model);
		if(pruquoteparm.getCoL1Rtr2()==15){
			Object[] prusaver=(Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
			if(prusaver[3].equals("FALSE".toLowerCase())){
				redirectAttributes.addFlashAttribute("errorPruSaver","Prusaver MB should between "+prusaver[1]+"-"+prusaver[2]);
				return "redirect:/edusave/"+pruquoteparm.getId()+"/edit";			
			}			
		}
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/pruquote_edusave_replatin/"+pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", request.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview EduSave English Quote No " +pruquoteparm.getId(), principal.getName());
		return "redirect:/edusave/"+pruquoteparm.getId()+"/edit";
	}
	
	@RequestMapping(value =  "/edusave/{id}/edit", method = RequestMethod.GET)
	public String editEduSave(@PathVariable("id") Long id,Model model) {
		PruquoteParm pruquoteParm=quoteService.getValidQuote(id, "BTR2", SecurityContextHolder.getContext().getAuthentication().getName());		
		if(pruquoteParm==null) throw new ResourceNotFoundException();
		model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
		model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%BTR2%"));
		model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%BTR2%"));
		model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
		model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long)9));
		model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
		model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
		model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
		model.addAttribute("pruquoteparm",pruquoteParm);
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("pruMyFamilyRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("isAffectiveDate", new Date().compareTo(affectiveDate));
		model.addAttribute("pageTitle", "Edit EduSave");
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object quoteUrl = attr.getRequest().getSession().getAttribute("previewurl");
		if(quoteUrl != null){
			model.addAttribute("quoteUrl", quoteUrl.toString());	
		}
		attr.getRequest().getSession().removeAttribute("previewurl");
		
		return "quote/editedusave";
	}
	@RequestMapping(value =  "/edusave/{id}/edit", method = RequestMethod.POST, params="save")
	public String processEditedusaveForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,@PathVariable("id") Long id,Model model,Principal principal) {
		if(result.hasErrors()){			
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%BTR2%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%BTR2%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long)9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
			return "quote/editedusave";
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR2");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("pruMyFamilyRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int isAffectiveDate = new Date().compareTo(affectiveDate);
		if (isAffectiveDate > 0) {
			pruquoteparm.setCoL1Rtr2(16);
			pruquoteparm.setL1Rtr2(new BigDecimal(0));
		}
		quoteService.pruQuoteParamSave(pruquoteparm);
		return "redirect:/edusave";
	}
	@RequestMapping(value =  "/edusave/{id}/edit", method = RequestMethod.POST, params="previewkh")
	public String processEditedusaveKh(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,@PathVariable("id") Long id,Model model,Principal principal,HttpServletRequest request,RedirectAttributes redirectAttributes ) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%BTR2%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%BTR2%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long)9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
			return "quote/editedusave";			
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR2");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("pruMyFamilyRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int isAffectiveDate = new Date().compareTo(affectiveDate);
		if (isAffectiveDate > 0) {
			pruquoteparm.setCoL1Rtr2(16);
			pruquoteparm.setL1Rtr2(new BigDecimal(0));
		}
		quoteService.pruQuoteParamSave(pruquoteparm);
		if(pruquoteparm.getCoL1Rtr2()==15){
			Object[] prusaver=(Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
			if(prusaver[3].equals("FALSE".toLowerCase())){
				redirectAttributes.addFlashAttribute("errorPruSaver","Prusaver MB should between "+prusaver[1]+"-"+prusaver[2]);
				return "redirect:/edusave/"+pruquoteparm.getId()+"/edit";			
			}			
		}
//		return editEduSave(id,model);
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/pruquote_edusave_repkhmer/"+pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", request.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview EduSave Khmer Quote No " +pruquoteparm.getId(), principal.getName());
		return "redirect:/edusave/"+pruquoteparm.getId()+"/edit";
	}
	@RequestMapping(value =  "/edusave/{id}/edit", method = RequestMethod.POST, params="previewen")
	public String processEditedusaveEn(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,@PathVariable("id") Long id,Model model,Principal principal,HttpServletRequest request,RedirectAttributes redirectAttributes) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%BTR2%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%BTR2%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long)9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
			return "quote/editedusave";	
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR2");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		Date affectiveDate = null;
		try {
			affectiveDate = AnonymousHelper.getFormattedDate(tableSetupService.itemExpireRiderLongTermBuilder("pruMyFamilyRiderLongTermSavingBuilderExpiredDate", 1),
					"yyyy-MM-dd");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int isAffectiveDate = new Date().compareTo(affectiveDate);
		if (isAffectiveDate > 0) {
			pruquoteparm.setCoL1Rtr2(16);
			pruquoteparm.setL1Rtr2(new BigDecimal(0));
		}
		quoteService.pruQuoteParamSave(pruquoteparm);
//		return editEduSave(id,model);
		if(pruquoteparm.getCoL1Rtr2()==15){
			Object[] prusaver=(Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
			if(prusaver[3].equals("FALSE".toLowerCase())){
				redirectAttributes.addFlashAttribute("errorPruSaver","Prusaver MB should between "+prusaver[1]+"-"+prusaver[2]);
				return "redirect:/edusave/"+pruquoteparm.getId()+"/edit";			
			}			
		}
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/pruquote_edusave_replatin/"+pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", request.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview EduSave English Quote No " +pruquoteparm.getId(), principal.getName());
		return "redirect:/edusave/"+pruquoteparm.getId()+"/edit";
	}
	
	@RequestMapping(value =  "/getMBBySA", method = RequestMethod.GET)
	@ResponseBody
	public String getMBBySA(@RequestParam BigDecimal SA,@RequestParam String premiumTerm){		
		PolicyFactory sanitizer = Sanitizers.FORMATTING.and(Sanitizers.BLOCKS);		
		return sanitizer.sanitize(quoteService.getMBBySA(SA, premiumTerm));
	}
	@RequestMapping(value =  "/getSAByMB", method = RequestMethod.GET)
	@ResponseBody
	public String getSAByMB(@RequestParam BigDecimal MB,@RequestParam String premiumTerm){		
		PolicyFactory sanitizer = Sanitizers.FORMATTING.and(Sanitizers.BLOCKS);		
		return sanitizer.sanitize(quoteService.getSAByMB(MB, premiumTerm));
	}
}