package com.pru.pruquote.controller;

import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.pru.pruquote.model.Role;
import com.pru.pruquote.model.RolePermission;
import com.pru.pruquote.service.SecurityService;

//import dummiesmind.breadcrumb.springmvc.annotations.Link;

@Controller
@PreAuthorize("hasAuthority('ACCESS_ROLE')")
public class RoleController {	
	
	@Autowired	
	private SecurityService securityService;
	
	@ModelAttribute("rolePermission")
    public RolePermission Constructor(){
    	return new RolePermission();
    }
	
	@ModelAttribute("roles")
    public List<Role> rolesConstructor(){
    	return securityService.roleFindAll();
    }
			
	@ModelAttribute("role")
    public Role roleConstructor(){
    	return new Role();
    }			
	
//	@Link(label="Role", family="RoleController", parent = "" )
	@RequestMapping(value =  "/role", method = RequestMethod.GET)
	public ModelAndView rolemgt() {

		ModelAndView model = new ModelAndView("rolemgt");
		model.addObject("roles", securityService.roleFindAll());
		return model;

	}
	
//	@Link(label="New Role", family="RoleController", parent = "Role")
	@RequestMapping(value =  "/newrole", method = RequestMethod.GET)
	public ModelAndView newRole() {

		ModelAndView model = new ModelAndView();	
		model.setViewName("newrole");
		return model;

	}
	
//	@Link(label="New Role", family="RoleController", parent = "Role")
	@RequestMapping(value =  "/newrole", method = RequestMethod.POST)
	public String addNewRole(@Valid @ModelAttribute("role") Role role
			,BindingResult result,Principal principal,Model model,HttpServletRequest request) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return "newrole";			
		}				
		
		role.setCreatedDate(new Date());
		role.setCreatedBy(principal.getName());
		role.setStatus((byte) 1);
		
		securityService.roleSave(role);
		String info="create role "+role.getId()+":"+role.getRoleName();
		securityService.saveLogWithInfo(request, info, principal.getName());
		return "redirect:/role";
	}	
	
//	@Link(label="Edit Role", family="RoleController", parent = "Role")
	@RequestMapping(value =  "/role/{roleId}/edit", method = RequestMethod.GET)
	public String editRole(@PathVariable("roleId") Integer roleId,Model model) {
		Role role=securityService.findRoleById(roleId);		
		Set<RolePermission> rolePermissions=securityService.rolePermissionFindByRoleNotDelete(roleId);
		
		model.addAttribute("roleId", roleId); 
		model.addAttribute("rolePermissions", rolePermissions);
		model.addAttribute("permissions", securityService.getPermissionNotExsitInRole(roleId));		
		model.addAttribute(role);						
		return "editrole";

	}
	
//	@Link(label="Edit Role", family="RoleController", parent = "Role")
	@RequestMapping(value =  "/role/{roleId}/edit", method = RequestMethod.POST)
	public String processEditRole(@PathVariable("roleId") Integer roleId
			,@Valid @ModelAttribute("role") Role role
			,BindingResult result,Model model,Principal principal,HttpServletRequest request) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return editRole(roleId,model);			
		}
		if(role.getStatus()==0){
			Role tempRole=securityService.findRoleById(roleId);
			if(securityService.userRoleFindByRole(tempRole).size()>0){
				ObjectError error = new ObjectError("role","Unable to disable, this role is in used!");
				result.addError(error);
				model.addAttribute("errors", result.getAllErrors());
				return editRole(roleId,model);	
			}
		}
		role.setLastUpdatedDate(new Date());
		role.setLastUpdatedBy(principal.getName());
		securityService.roleSave(role);
		String info="edit role "+role.getId()+":"+role.getRoleName();
		securityService.saveLogWithInfo(request, info, principal.getName());
		return "redirect:/role";
	}
	
	@RequestMapping(value =  "/role/{roleId}/edit/rolepermission", method = RequestMethod.POST)
	public String processEditRolePermission(@PathVariable("roleId") Integer roleId
			,@Valid @ModelAttribute("rolePermission") RolePermission rolePermission
			,BindingResult result,Model model,Principal principal,HttpServletRequest request) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return "redirect:/role/"+roleId+"/edit#tabrolePermission";			
		}
		rolePermission.setRole(securityService.findRoleById(roleId));
		rolePermission.setPermission(securityService.permissionFindOne(rolePermission.getPermission().getPermissionId()));
		rolePermission.setCreatedDate(new Date());
		rolePermission.setCreatedBy(principal.getName());
		securityService.rolePermissionSave(rolePermission);
		String info="add permission "+rolePermission.getPermission().getPermissionId()+":"+rolePermission.getPermission().getPermissionName()
				+" to role "+rolePermission.getRole().getId()+":"+rolePermission.getRole().getRoleName();
		securityService.saveLogWithInfo(request, info, principal.getName());
		
		return "redirect:/role/"+roleId+"/edit#tabrolePermission";
	}
	
	@RequestMapping(value =  "/role/{roleId}/remove/rolepermission/{rolePermissionId}", method = RequestMethod.POST)
	public String processRemoveRolePermission(@PathVariable("roleId") Integer roleId
			,@PathVariable("rolePermissionId") Integer rolePermissionId,Model model,Principal principal,HttpServletRequest request) {
		
		RolePermission rolePermission=securityService.rolePermissionFindOne(rolePermissionId);		
		rolePermission.setLastUpdatedDate(new Date());
		rolePermission.setLastUpdatedBy(principal.getName());
		securityService.rolePermissionSave(rolePermission);
		String info="remove permission "+rolePermission.getPermission().getPermissionId()+":"+rolePermission.getPermission().getPermissionName()
				+" from role "+rolePermission.getRole().getId()+":"+rolePermission.getRole().getRoleName();
		securityService.saveLogWithInfo(request, info, principal.getName());
		
		return "redirect:/role/"+roleId+"/edit#tabrolePermission";
	}
}