package com.pru.pruquote.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pru.pruquote.dao.IUserDao;
import com.pru.pruquote.model.User;
import com.pru.pruquote.service.UserService;
import com.pru.pruquote.utility.GlobalVar;

@Controller
public class MainController {
//	@Autowired
//	private IUserDao userDao;
//	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(ModelMap model, HttpServletRequest request){
//		User user = userDao.getUser(SecurityContextHolder.getContext().getAuthentication().getName());
//		
//		if(user != null && user.isFirstLogin()){
//			return "redirect:/changepwd";
//		}
		
		Object expiredPwdMsg = request.getSession().getAttribute("isExpiredSoon");
		
		model.addAttribute("pageTitle", "PCLA Distribution Portal");
		model.addAttribute("version", GlobalVar.version);
		
		if(expiredPwdMsg != null){
			model.addAttribute("isExpiredSoon", expiredPwdMsg.toString());	
		}
		
		return "index";
	}
	
	@RequestMapping(value = "/403")
	public String getAccessDeniedPage(ModelMap model){
//		model.addAttribute("pageTitle", "Access Denied");
		
		return "error";
	}
	
	@ExceptionHandler(HttpSessionRequiredException.class)
	public String handleSessionExpired() {
		return "redirect:/login";
	}
	
	@RequestMapping(value = "/keep-alive", method = RequestMethod.GET)
	@ResponseBody
	public void keepAlive(HttpServletRequest request){
		request.getSession().setMaxInactiveInterval(15*60);
	}
	
	@RequestMapping(value = "/getloginname", method = RequestMethod.GET)
    @ResponseBody
    public String getLoginName(Principal principal){
           String fullname = principal.getName();
           String username = principal.getName();
           
           User user = userService.getUser(username);
           
           if(user != null){
                  fullname = user.getFullName();
           }
           
           return fullname;
    }

}
