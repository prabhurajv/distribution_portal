package com.pru.pruquote.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.security.Principal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pru.pruquote.model.CollectionAndLapse;
import com.pru.pruquote.model.CollectionandLapseBcuStatus;
import com.pru.pruquote.model.CollectionandLapseBranchCode;
import com.pru.pruquote.model.CollectionandLapseComment;
import com.pru.pruquote.model.CollectionandLapseDate;
import com.pru.pruquote.model.CollectionandLapseDbmCode;
import com.pru.pruquote.model.CollectionandLapseDueDate;
import com.pru.pruquote.model.CollectionandLapseDuration;
import com.pru.pruquote.model.CollectionandLapseIsImpactPersistency;
import com.pru.pruquote.model.CollectionandLapseReason;
import com.pru.pruquote.model.CollectionandLapsedAgentCode;
import com.pru.pruquote.model.CollectionandLapsedAgentStatus;
import com.pru.pruquote.model.CollectionandlapseApe;
import com.pru.pruquote.model.PersistencyDetail;
import com.pru.pruquote.model.PruquoteParm;
import com.pru.pruquote.model.QuoteOccupation;
import com.pru.pruquote.property.ICollectionAndLapsed;
import com.pru.pruquote.service.CollectionandLapseService;
import com.pru.pruquote.service.DDService;
import com.pru.pruquote.service.ReferralService;
import com.pru.pruquote.service.TableSetupService;
import com.pru.pruquote.utility.AnonymousHelper;
import com.pru.pruquote.utility.GlobalVar;

@Controller
@PreAuthorize("hasAnyAuthority('ACCESS_COLLECTION_LAPSE')")
public class CollectionandLapseController {

	@Autowired
	private CollectionandLapseService collectionAndLapseService;

	@Autowired
	private TableSetupService tableSetupService;

	@Autowired
	private ReferralService referralService;

	@Autowired
	private DDService ddService;

	@RequestMapping(value = "/collectionandlapse", method = RequestMethod.GET)
	public ModelAndView collectionandLapse(Principal principal, HttpServletRequest req) {
		String userid = principal.getName();
		String type = req.getParameter("type");
		String ipc = req.getParameter("ipc");

		ModelAndView model = new ModelAndView();

		// to get list of collection and lapsed
		List<CollectionAndLapse> getAllCollectionAndLapse = collectionAndLapseService.getListCollectionandLapse(userid);
		Stream<CollectionAndLapse> allCollections = getAllCollectionAndLapse.stream()
				.filter(s -> s.getObjType().trim().equals("CL"));
		Stream<CollectionAndLapse> allLapses = getAllCollectionAndLapse.stream()
				.filter(s -> s.getObjType().trim().equals("LA"));
		List<CollectionAndLapse> list = allCollections.collect(Collectors.toList());
		List<CollectionAndLapse> listla = allLapses.collect(Collectors.toList());

		// Declare list of collection
		List<String> branchCodeAndName = new ArrayList<String>();
		List<String> agentCodeAndName = new ArrayList<String>();
		List<String> bdmCode = new ArrayList<String>();
		List<String> agentStatus = new ArrayList<String>();
		List<String> bcuCallStatus = new ArrayList<String>();
		List<String> productList = new ArrayList<String>();

		// Declare list of lapsed
		List<String> branchCodeAndNamela = new ArrayList<String>();
		List<String> agentCodeAndNamela = new ArrayList<String>();
		List<String> bdmCodela = new ArrayList<String>();
		List<String> agentStatusla = new ArrayList<String>();
		List<String> bcuCallStatusla = new ArrayList<String>();
		List<String> productListLA = new ArrayList<String>();

		List<String> isImpactPersistencyla = new ArrayList<String>();
		isImpactPersistencyla.add("Q1");
		isImpactPersistencyla.add("Q2");
		isImpactPersistencyla.add("Q3");
		isImpactPersistencyla.add("Q4");

		List<String> lapsedReasonla = new ArrayList<String>();

		for (Iterator<CollectionAndLapse> i = list.iterator(); i.hasNext();) {
			CollectionAndLapse item = i.next();
			branchCodeAndName.add(item.getBranchCode() + "-" + item.getBranchName());
			agentCodeAndName.add(item.getAgntNum() + "-" + item.getAgntName());
			bdmCode.add(item.getAgntSupcode());
			agentStatus.add(item.getAgntStatus());
			bcuCallStatus.add(item.getCcStatus());
			productList.add(item.getProduct());
		}

		for (Iterator<CollectionAndLapse> j = listla.iterator(); j.hasNext();) {
			CollectionAndLapse itemla = j.next();
			branchCodeAndNamela.add(itemla.getBranchCode() + "-" + itemla.getBranchName());
			agentCodeAndNamela.add(itemla.getAgntNum() + "-" + itemla.getAgntName());
			bdmCodela.add(itemla.getAgntSupcode());
			agentStatusla.add(itemla.getAgntStatus());
			bcuCallStatusla.add(itemla.getCcStatus());
			// isImpactPersistencyla.add(itemla.getIsImpactPersistency());
			lapsedReasonla.add(itemla.getLapsedReason());
			productListLA.add(itemla.getProduct());
		}

		// get list of branch code and name by collection
		branchCodeAndName = branchCodeAndName.stream().distinct().collect(Collectors.toList());
		model.addObject("branchCodeAndName", branchCodeAndName);

		branchCodeAndNamela = branchCodeAndNamela.stream().distinct().collect(Collectors.toList());
		model.addObject("branchCodeAndNamela", branchCodeAndNamela);

		// get list of agent code and name
		agentCodeAndName = agentCodeAndName.stream().distinct().collect(Collectors.toList());
		model.addObject("agentCodeAndName", agentCodeAndName);

		agentCodeAndNamela = agentCodeAndNamela.stream().distinct().collect(Collectors.toList());
		model.addObject("agentCodeAndNamela", agentCodeAndNamela);

		// get agent supervisor code (BDM Code)
		bdmCode = bdmCode.stream().distinct().collect(Collectors.toList());
		model.addObject("bdmCode", bdmCode);

		// distinct of product from containing list
		productList = productList.stream().distinct().collect(Collectors.toList());
		model.addObject("productList", productList);

		bdmCodela = bdmCodela.stream().distinct().collect(Collectors.toList());
		model.addObject("bdmCodela", bdmCodela);

		// get agent status
		agentStatus = agentStatus.stream().distinct().collect(Collectors.toList());
		model.addObject("agentStatus", agentStatus);

		agentStatusla = agentStatusla.stream().distinct().collect(Collectors.toList());
		model.addObject("agentStatusla", agentStatusla);

		// get call center status (BCU Call Status)
		bcuCallStatus = bcuCallStatus.stream().distinct().collect(Collectors.toList());
		model.addObject("bcuCallStatus", bcuCallStatus);

		bcuCallStatusla = bcuCallStatusla.stream().distinct().collect(Collectors.toList());
		model.addObject("bcuCallStatusla", bcuCallStatusla);

		// isImpactPersistencyla =
		// isImpactPersistencyla.stream().distinct().collect(Collectors.toList());
		model.addObject("isImpactPersistencyla", isImpactPersistencyla);

		lapsedReasonla = lapsedReasonla.stream().distinct().collect(Collectors.toList());
		model.addObject("lapsedReasonla", lapsedReasonla);

		// distinct of product from containing list
		productListLA = productListLA.stream().distinct().collect(Collectors.toList());
		model.addObject("productListLA", productListLA);

		model.addObject("pageTitle", "Collection and Laps");
		model.addObject("type", type);
		if (ipc != null) {
			model.addObject("ipc", ipc);
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("podcsn", req.getParameter("podcsn"));

		model.setViewName("collectionandlapse/collectionandlapse");
		return model;
	}

	@ResponseBody
	@RequestMapping(value = "/getcollection", method = RequestMethod.GET)
	public String getCollection(Principal principal, HttpServletRequest req) {
		String result = "";
		int sortCol = Integer.valueOf(req.getParameter("order[0][column]"));
		String sortColDir = req.getParameter("order[0][dir]");
		String search = req.getParameter("search[value]");
		String columnsName = req.getParameter("columns[" + sortCol + "][data]");

		String userid = principal.getName();

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object podcsn = attr.getRequest().getSession().getAttribute("podcsn");

		List<CollectionAndLapse> getAllCollectionAndLapse = null;
		if (podcsn != null)
			getAllCollectionAndLapse = collectionAndLapseService.getListCollectionandLapseFilterByPODecision(userid,
					(String) podcsn);
		else
			getAllCollectionAndLapse = collectionAndLapseService.getListCollectionandLapse(userid);

		Stream<CollectionAndLapse> allCollections = getAllCollectionAndLapse.stream()
				.filter(s -> s.getObjType().trim().equals("CL"));
		List<CollectionAndLapse> list = allCollections.collect(Collectors.toList());
		if (search != null && search != "") {
			String sanitizedSearchKeyword = AnonymousHelper.sanitizeInput(search);
			list = list.stream()
					.filter(o -> o.getChdrNum().contains(sanitizedSearchKeyword)
							|| o.getChdrAppnum().contains(sanitizedSearchKeyword)
							|| o.getClntMobile1().contains(sanitizedSearchKeyword)
							|| o.getClntMobile2().contains(sanitizedSearchKeyword)
							|| o.getChdrPwithTax().toString().contains(sanitizedSearchKeyword)
							|| o.getChdrBillfreq().contains(sanitizedSearchKeyword)
							|| o.getChdrDueDate().toString().contains(sanitizedSearchKeyword) ||
							// o.getChdrLapseDate().toString().contains(sanitizedSearchKeyword) ||
							o.getLapsedReason().contains(sanitizedSearchKeyword)
							|| o.getBcuRemark().contains(sanitizedSearchKeyword)
							|| o.getCustomerResponse().contains(sanitizedSearchKeyword)
							|| o.getCcStatus().contains(sanitizedSearchKeyword)
							|| o.getAgntStatus().contains(sanitizedSearchKeyword)
							|| o.getCcRemark().contains(sanitizedSearchKeyword)
							|| o.getPoName().contains(sanitizedSearchKeyword)
							|| o.getChdrApe().toString().contains(sanitizedSearchKeyword))
					.collect(Collectors.toList());
		}
		if (sortColDir.equalsIgnoreCase("asc")) {
			if (columnsName.equalsIgnoreCase("chdrNum"))
				Collections.sort(list, CollectionAndLapse.ChdrNumComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrAppnum"))
				Collections.sort(list, CollectionAndLapse.ChdrAppnumComparatorAsc);
			else if (columnsName.equalsIgnoreCase("clntMobile1"))
				Collections.sort(list, CollectionAndLapse.clntMobile1ComparatorAsc);
			else if (columnsName.equalsIgnoreCase("clntMobile2"))
				Collections.sort(list, CollectionAndLapse.clntMobile2ComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrPwithTax"))
				Collections.sort(list, CollectionAndLapse.chdrPwithTaxComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrBillfreq"))
				Collections.sort(list, CollectionAndLapse.chdrBillfreqComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrDueDate"))
				Collections.sort(list, CollectionAndLapse.chdrDueDateComparatorAsc);
			// else if(columnsName.equalsIgnoreCase("chdrLapseDate"))
			// Collections.sort(list, CollectionAndLapse.chdrLapseDateComparatorAsc);
			else if (columnsName.equalsIgnoreCase("lapsedReason"))
				Collections.sort(list, CollectionAndLapse.lapsedReasonComparatorAsc);
			else if (columnsName.equalsIgnoreCase("bcuRemark"))
				Collections.sort(list, CollectionAndLapse.bcuRemarkComparatorAsc);
			else if (columnsName.equalsIgnoreCase("customerResponse"))
				Collections.sort(list, CollectionAndLapse.customerResponseComparatorAsc);
			else if (columnsName.equalsIgnoreCase("ccStatus"))
				Collections.sort(list, CollectionAndLapse.ccStatusComparatorAsc);
			else if (columnsName.equalsIgnoreCase("agntStatus"))
				Collections.sort(list, CollectionAndLapse.agntStatusComparatorAsc);
			else if (columnsName.equalsIgnoreCase("agntccRemark"))
				Collections.sort(list, CollectionAndLapse.agntCcRemarkComparatorAsc);
			else if (columnsName.equalsIgnoreCase("poName"))
				Collections.sort(list, CollectionAndLapse.poNameComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrApe"))
				Collections.sort(list, CollectionAndLapse.chdrApeComparatorAsc);
		} else {
			if (columnsName.equalsIgnoreCase("chdrNum"))
				Collections.sort(list, CollectionAndLapse.ChdrNumComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrAppnum"))
				Collections.sort(list, CollectionAndLapse.ChdrAppnumComparatorDesc);
			else if (columnsName.equalsIgnoreCase("clntMobile1"))
				Collections.sort(list, CollectionAndLapse.clntMobile1ComparatorDesc);
			else if (columnsName.equalsIgnoreCase("clntMobile2"))
				Collections.sort(list, CollectionAndLapse.clntMobile2ComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrPwithTax"))
				Collections.sort(list, CollectionAndLapse.chdrPwithTaxComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrBillfreq"))
				Collections.sort(list, CollectionAndLapse.chdrBillfreqComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrDueDate"))
				Collections.sort(list, CollectionAndLapse.chdrDueDateComparatorDesc);
			// else if(columnsName.equalsIgnoreCase("chdrLapseDate"))
			// Collections.sort(list, CollectionAndLapse.chdrLapseDateComparatorDesc);
			else if (columnsName.equalsIgnoreCase("lapsedReason"))
				Collections.sort(list, CollectionAndLapse.lapsedReasonComparatorDesc);
			else if (columnsName.equalsIgnoreCase("bcuRemark"))
				Collections.sort(list, CollectionAndLapse.bcuRemarkComparatorDesc);
			else if (columnsName.equalsIgnoreCase("customerResponse"))
				Collections.sort(list, CollectionAndLapse.customerResponseComparatorDesc);
			else if (columnsName.equalsIgnoreCase("ccStatus"))
				Collections.sort(list, CollectionAndLapse.ccStatusComparatorDesc);
			else if (columnsName.equalsIgnoreCase("agntStatus"))
				Collections.sort(list, CollectionAndLapse.agntStatusComparatorDesc);
			else if (columnsName.equalsIgnoreCase("ccRemark"))
				Collections.sort(list, CollectionAndLapse.agntCcRemarkComparatorDesc);
			else if (columnsName.equalsIgnoreCase("poName"))
				Collections.sort(list, CollectionAndLapse.poNameComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrApe"))
				Collections.sort(list, CollectionAndLapse.chdrApeComparatorDesc);
		}

		result = AnonymousHelper.getServerSideDatasource(req, list);

		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/advancesearchcollection", method = RequestMethod.GET)
	public String AdvanceSearchCollection(Principal principal, HttpServletRequest req) {
		String result = "";
		int sortCol = Integer.valueOf(req.getParameter("order[0][column]"));
		String sortColDir = req.getParameter("order[0][dir]");
		String search = req.getParameter("search[value]");
		String columnsName = req.getParameter("columns[" + sortCol + "][data]");
		String userid = principal.getName();

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object podcsn = attr.getRequest().getSession().getAttribute("podcsn");

		List<CollectionAndLapse> getAllCollectionAndLapse = null;
		if (podcsn != null)
			getAllCollectionAndLapse = collectionAndLapseService.getListCollectionandLapseFilterByPODecision(userid,
					(String) podcsn);
		else
			getAllCollectionAndLapse = collectionAndLapseService.getListCollectionandLapse(userid);

		Stream<CollectionAndLapse> allCollections = getAllCollectionAndLapse.stream()
				.filter(s -> s.getObjType().trim().equals("CL"));
		List<CollectionAndLapse> list = allCollections.collect(Collectors.toList());

		String branchcodeandname = req.getParameter("branchcodeandname").toString();
		if (branchcodeandname != null && !branchcodeandname.equals("") && !branchcodeandname.equals("0")) {
			ICollectionAndLapsed branchCode = new CollectionandLapseBranchCode();
			list = branchCode.filterCollectionAndLapse(list, branchcodeandname, "=");
		}

		String agentcodeandname = req.getParameter("agentcodeandname").toString();
		if (agentcodeandname != null && !agentcodeandname.equals("") && !agentcodeandname.equals("0")) {
			ICollectionAndLapsed agentCodeAndName = new CollectionandLapsedAgentCode();
			list = agentCodeAndName.filterCollectionAndLapse(list, agentcodeandname, "=");
		}

		String bdmcode = req.getParameter("bdmcode").toString();
		if (bdmcode != null && !bdmcode.equals("") && !bdmcode.equals("0")) {
			ICollectionAndLapsed bdmCode = new CollectionandLapseDbmCode();
			list = bdmCode.filterCollectionAndLapse(list, bdmcode, "=");
		}

		String agentstatus = req.getParameter("agentstatus").toString();
		if (agentstatus != null && !agentstatus.equals("") && !agentstatus.equals("0")) {
			ICollectionAndLapsed agentStatus = new CollectionandLapsedAgentStatus();
			list = agentStatus.filterCollectionAndLapse(list, agentstatus, "=");
		}

		String bcucallstatus = req.getParameter("bcucallstatus").toString();
		if (bcucallstatus != null && !bcucallstatus.equals("") && !bcucallstatus.equals("0")) {
			ICollectionAndLapsed bcuCallStatus = new CollectionandLapseBcuStatus();
			list = bcuCallStatus.filterCollectionAndLapse(list, bcucallstatus, "=");
		}

		String duedate = req.getParameter("duedate");
		if (!duedate.equals("")) {
			String duedateoperator = req.getParameter("duedateoperator");
			ICollectionAndLapsed dueDate = new CollectionandLapseDueDate();
			if (duedateoperator.equals("between")) {
				String duedateto = req.getParameter("duedateto");
				if (!duedateto.equals("")) {
					list = dueDate.filterCollectionAndLapse(list, duedate, duedateoperator, duedateto);
				}
			} else {
				list = dueDate.filterCollectionAndLapse(list, duedate, duedateoperator);
			}
		}

		String ape = req.getParameter("ape");
		if (!ape.equals("")) {
			String apeoperator = req.getParameter("apeoperator");
			ICollectionAndLapsed Ape = new CollectionandlapseApe();
			if (apeoperator.equals("between")) {
				String apeto = req.getParameter("apeto");
				if (!apeto.equals("")) {
					list = Ape.filterCollectionAndLapse(list, ape, apeoperator, apeto);
				}
			} else {
				list = Ape.filterCollectionAndLapse(list, ape, apeoperator);
			}
		}

		// add filter on product on 23-01-2019
		String product = req.getParameter("product").toString();
		if (product != null && !product.equals("") && !product.equals("0")) {
			list = list.stream().filter(s -> s.getProduct().equalsIgnoreCase(product)).collect(Collectors.toList());
		}

		if (search != null && search != "") {
			String sanitizedSearchKeyword = AnonymousHelper.sanitizeInput(search);
			list = list.stream()
					.filter(o -> o.getChdrNum().contains(sanitizedSearchKeyword)
							|| o.getChdrAppnum().contains(sanitizedSearchKeyword)
							|| o.getClntMobile1().contains(sanitizedSearchKeyword)
							|| o.getClntMobile2().contains(sanitizedSearchKeyword)
							|| o.getChdrPwithTax().toString().contains(sanitizedSearchKeyword)
							|| o.getChdrBillfreq().contains(sanitizedSearchKeyword)
							|| o.getChdrDueDate().toString().contains(sanitizedSearchKeyword) ||
							// o.getChdrLapseDate().toString().contains(sanitizedSearchKeyword) ||
							o.getLapsedReason().contains(sanitizedSearchKeyword)
							|| o.getBcuRemark().contains(sanitizedSearchKeyword)
							|| o.getCustomerResponse().contains(sanitizedSearchKeyword)
							|| o.getCcStatus().contains(sanitizedSearchKeyword)
							|| o.getAgntStatus().contains(sanitizedSearchKeyword)
							|| o.getCcRemark().contains(sanitizedSearchKeyword)
							|| o.getPoName().contains(sanitizedSearchKeyword)
							|| o.getChdrApe().toString().contains(sanitizedSearchKeyword))
					.collect(Collectors.toList());
		}
		if (sortColDir.equalsIgnoreCase("asc")) {
			if (columnsName.equalsIgnoreCase("chdrNum"))
				Collections.sort(list, CollectionAndLapse.ChdrNumComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrAppnum"))
				Collections.sort(list, CollectionAndLapse.ChdrAppnumComparatorAsc);
			else if (columnsName.equalsIgnoreCase("clntMobile1"))
				Collections.sort(list, CollectionAndLapse.clntMobile1ComparatorAsc);
			else if (columnsName.equalsIgnoreCase("clntMobile2"))
				Collections.sort(list, CollectionAndLapse.clntMobile2ComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrPwithTax"))
				Collections.sort(list, CollectionAndLapse.chdrPwithTaxComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrBillfreq"))
				Collections.sort(list, CollectionAndLapse.chdrBillfreqComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrDueDate"))
				Collections.sort(list, CollectionAndLapse.chdrDueDateComparatorAsc);
			// else if(columnsName.equalsIgnoreCase("chdrLapseDate"))
			// Collections.sort(list, CollectionAndLapse.chdrLapseDateComparatorAsc);
			else if (columnsName.equalsIgnoreCase("lapsedReason"))
				Collections.sort(list, CollectionAndLapse.lapsedReasonComparatorAsc);
			else if (columnsName.equalsIgnoreCase("bcuRemark"))
				Collections.sort(list, CollectionAndLapse.bcuRemarkComparatorAsc);
			else if (columnsName.equalsIgnoreCase("customerResponse"))
				Collections.sort(list, CollectionAndLapse.customerResponseComparatorAsc);
			else if (columnsName.equalsIgnoreCase("ccStatus"))
				Collections.sort(list, CollectionAndLapse.ccStatusComparatorAsc);
			else if (columnsName.equalsIgnoreCase("agntStatus"))
				Collections.sort(list, CollectionAndLapse.agntStatusComparatorAsc);
			else if (columnsName.equalsIgnoreCase("ccRemark"))
				Collections.sort(list, CollectionAndLapse.agntCcRemarkComparatorAsc);
			else if (columnsName.equalsIgnoreCase("poName"))
				Collections.sort(list, CollectionAndLapse.poNameComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrApe"))
				Collections.sort(list, CollectionAndLapse.chdrApeComparatorAsc);
			else if (columnsName.equalsIgnoreCase("product"))
				Collections.sort(list, CollectionAndLapse.productComparatorAsc);
		} else {
			if (columnsName.equalsIgnoreCase("chdrNum"))
				Collections.sort(list, CollectionAndLapse.ChdrNumComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrAppnum"))
				Collections.sort(list, CollectionAndLapse.ChdrAppnumComparatorDesc);
			else if (columnsName.equalsIgnoreCase("clntMobile1"))
				Collections.sort(list, CollectionAndLapse.clntMobile1ComparatorDesc);
			else if (columnsName.equalsIgnoreCase("clntMobile2"))
				Collections.sort(list, CollectionAndLapse.clntMobile2ComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrPwithTax"))
				Collections.sort(list, CollectionAndLapse.chdrPwithTaxComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrBillfreq"))
				Collections.sort(list, CollectionAndLapse.chdrBillfreqComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrDueDate"))
				Collections.sort(list, CollectionAndLapse.chdrDueDateComparatorDesc);
			// else if(columnsName.equalsIgnoreCase("chdrLapseDate"))
			// Collections.sort(list, CollectionAndLapse.chdrLapseDateComparatorDesc);
			else if (columnsName.equalsIgnoreCase("lapsedReason"))
				Collections.sort(list, CollectionAndLapse.lapsedReasonComparatorDesc);
			else if (columnsName.equalsIgnoreCase("bcuRemark"))
				Collections.sort(list, CollectionAndLapse.bcuRemarkComparatorDesc);
			else if (columnsName.equalsIgnoreCase("customerResponse"))
				Collections.sort(list, CollectionAndLapse.customerResponseComparatorDesc);
			else if (columnsName.equalsIgnoreCase("ccStatus"))
				Collections.sort(list, CollectionAndLapse.ccStatusComparatorDesc);
			else if (columnsName.equalsIgnoreCase("agntStatus"))
				Collections.sort(list, CollectionAndLapse.agntStatusComparatorDesc);
			else if (columnsName.equalsIgnoreCase("ccRemark"))
				Collections.sort(list, CollectionAndLapse.agntCcRemarkComparatorDesc);
			else if (columnsName.equalsIgnoreCase("poName"))
				Collections.sort(list, CollectionAndLapse.poNameComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrApe"))
				Collections.sort(list, CollectionAndLapse.chdrApeComparatorDesc);
			else if (columnsName.equalsIgnoreCase("product"))
				Collections.sort(list, CollectionAndLapse.productComparatorDesc);
		}

		result = AnonymousHelper.getServerSideDatasource(req, list);

		return result;
	}

	private List<CollectionAndLapse> GetAdvanceSearchLapsed(Principal principal, HttpServletRequest req) {

		String userid = principal.getName();

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object podcsn = attr.getRequest().getSession().getAttribute("podcsn");

		List<CollectionAndLapse> getAllCollectionAndLapse = null;
		if (podcsn != null)
			getAllCollectionAndLapse = collectionAndLapseService.getListCollectionandLapseFilterByPODecision(userid,
					(String) podcsn);
		else
			getAllCollectionAndLapse = collectionAndLapseService.getListCollectionandLapse(userid);

		Stream<CollectionAndLapse> allCollections = getAllCollectionAndLapse.stream()
				.filter(s -> s.getObjType().trim().equals("LA"));
		List<CollectionAndLapse> list = allCollections.collect(Collectors.toList());

		String branchcodeandname = req.getParameter("branchcodeandname") == null ? null
				: req.getParameter("branchcodeandname").toString();
		if (branchcodeandname != null && !branchcodeandname.equals("") && !branchcodeandname.equals("0")) {
			ICollectionAndLapsed branchCode = new CollectionandLapseBranchCode();
			list = branchCode.filterCollectionAndLapse(list, branchcodeandname, "=");
		}

		String agentcodeandname = req.getParameter("agentcodeandname") == null ? null
				: req.getParameter("agentcodeandname").toString();
		if (agentcodeandname != null && !agentcodeandname.equals("") && !agentcodeandname.equals("0")) {
			ICollectionAndLapsed agentCodeAndName = new CollectionandLapsedAgentCode();
			list = agentCodeAndName.filterCollectionAndLapse(list, agentcodeandname, "=");
		}

		String bdmcode = req.getParameter("bdmcode") == null ? null : req.getParameter("bdmcode").toString();
		if (bdmcode != null && !bdmcode.equals("") && !bdmcode.equals("0")) {
			ICollectionAndLapsed bdmCode = new CollectionandLapseDbmCode();
			list = bdmCode.filterCollectionAndLapse(list, bdmcode, "=");
		}

		String agentstatus = req.getParameter("agentstatus") == null ? null
				: req.getParameter("agentstatus").toString();
		if (agentstatus != null && !agentstatus.equals("") && !agentstatus.equals("0")) {
			ICollectionAndLapsed agentStatus = new CollectionandLapsedAgentStatus();
			list = agentStatus.filterCollectionAndLapse(list, agentstatus, "=");
		}

		String bcucallstatus = req.getParameter("bcucallstatus") == null ? null
				: req.getParameter("bcucallstatus").toString();
		if (bcucallstatus != null && !bcucallstatus.equals("") && !bcucallstatus.equals("0")) {
			ICollectionAndLapsed bcuCallStatus = new CollectionandLapseBcuStatus();
			list = bcuCallStatus.filterCollectionAndLapse(list, bcucallstatus, "=");
		}

		String duedate = req.getParameter("duedate") == null ? null : req.getParameter("duedate");
		if (duedate != null && !duedate.equals("")) {
			String duedateoperator = req.getParameter("duedateoperator");
			ICollectionAndLapsed dueDate = new CollectionandLapseDueDate();
			if (duedateoperator.equals("between")) {
				String duedateto = req.getParameter("duedateto");
				if (!duedateto.equals("")) {
					list = dueDate.filterCollectionAndLapse(list, duedate, duedateoperator, duedateto);
				}
			} else {
				list = dueDate.filterCollectionAndLapse(list, duedate, duedateoperator);
			}
		}

		String isimpactpersistency = req.getParameter("isimpactpersistency") == null ? null
				: req.getParameter("isimpactpersistency").toString();
		if (isimpactpersistency != null && !isimpactpersistency.equals("") && !isimpactpersistency.equals("0")) {
			ICollectionAndLapsed isImpactPersistency = new CollectionandLapseIsImpactPersistency();
			list = isImpactPersistency.filterCollectionAndLapse(list, isimpactpersistency, "=");
		}

		String duration = req.getParameter("duration") == null ? null : req.getParameter("duration");
		if (duration != null && !duration.equals("")) {
			String durationoperator = req.getParameter("durationoperator");
			ICollectionAndLapsed durationOperator = new CollectionandLapseDuration();
			if (durationOperator.equals("between")) {
				String durationto = req.getParameter("durationto");
				if (!durationto.equals("")) {
					list = durationOperator.filterCollectionAndLapse(list, duration, durationoperator, durationto);
				}
			} else {
				list = durationOperator.filterCollectionAndLapse(list, duration, durationoperator);
			}
		}

		String reason = req.getParameter("reason") == null ? null : req.getParameter("reason").toString();
		if (reason != null && !reason.equals("") && !reason.equals("0")) {
			ICollectionAndLapsed Reason = new CollectionandLapseReason();
			list = Reason.filterCollectionAndLapse(list, reason, "=");
		}

		String date = req.getParameter("date") == null ? null : req.getParameter("date");
		if (date != null && !date.equals("")) {
			String dateoperator = req.getParameter("dateoperator");
			ICollectionAndLapsed dateOperator = new CollectionandLapseDate();
			if (dateoperator.equals("between")) {
				String dateto = req.getParameter("dateto");
				if (!dateto.equals("")) {
					list = dateOperator.filterCollectionAndLapse(list, date, dateoperator, dateto);
				}
			} else {
				list = dateOperator.filterCollectionAndLapse(list, date, dateoperator);
			}
		}

		String ape = req.getParameter("ape") == null ? null : req.getParameter("ape");
		if (ape != null && !ape.equals("")) {
			String apeoperator = req.getParameter("apeoperator");
			ICollectionAndLapsed Ape = new CollectionandlapseApe();
			if (apeoperator.equals("between")) {
				String apeto = req.getParameter("apeto");
				if (!apeto.equals("")) {
					list = Ape.filterCollectionAndLapse(list, ape, apeoperator, apeto);
				}
			} else {
				list = Ape.filterCollectionAndLapse(list, ape, apeoperator);
			}
		}

		// add filter on product on 23-01-2019
		String product = req.getParameter("product").toString();
		if (product != null && !product.equals("") && !product.equals("0")) {
			list = list.stream().filter(s -> s.getProduct().equalsIgnoreCase(product)).collect(Collectors.toList());
		}

		return list;
	}
	// MOVE TO DASHBOARD CONTROLLER
	// @ResponseBody
	// @RequestMapping(value = "/csvpersistencies")
	// public void GetCSVPersistencies(HttpServletRequest request,
	// HttpServletResponse response) throws IOException {
	// String userid = request.getUserPrincipal().getName();
	// String isimpactpersistency = request.getParameter("isimpactpersistency") ==
	// null ? null
	// : request.getParameter("isimpactpersistency").toString();
	//
	// //REQUEST FROM TICKET 2349 , get only LA, SU, TR and it is the valid , move
	// it into service for any criteria
	// List<CollectionAndLapse> list =
	// collectionAndLapseService.getListPersistencies(userid,isimpactpersistency);
	// AnonymousHelper.ExportCSV(response, list, new CollectionAndLapse(),
	// "Persistencies Detail-" + isimpactpersistency + "-" + new Date());
	// }

	@ResponseBody
	@RequestMapping(value = "/csvlapse")
	public void GetCSVLapse(HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<CollectionAndLapse> list = GetAdvanceSearchLapsed(request.getUserPrincipal(), request);
		AnonymousHelper.ExportCSV(response, list, new CollectionAndLapse(), "Lapsed Detail-" + new Date());
	}

	@ResponseBody
	@RequestMapping(value = "/advancesearchlapsed", method = RequestMethod.GET)
	public String AdvanceSearchLapsed(Principal principal, HttpServletRequest req) {
		String result = "";

		int sortCol = Integer.valueOf(req.getParameter("order[0][column]"));
		String sortColDir = req.getParameter("order[0][dir]");
		String search = req.getParameter("search[value]");
		String columnsName = req.getParameter("columns[" + sortCol + "][data]");

		List<CollectionAndLapse> list = GetAdvanceSearchLapsed(principal, req);
		if (search != null && search != "") {
			String sanitizedSearchKeyword = AnonymousHelper.sanitizeInput(search);
			list = list.stream()
					.filter(o -> o.getChdrNum().contains(sanitizedSearchKeyword)
							|| o.getChdrAppnum().contains(sanitizedSearchKeyword)
							|| o.getClntMobile1().contains(sanitizedSearchKeyword)
							|| o.getClntMobile2().contains(sanitizedSearchKeyword)
							|| o.getChdrPwithTax().toString().contains(sanitizedSearchKeyword)
							|| o.getChdrBillfreq().contains(sanitizedSearchKeyword)
							|| o.getChdrDueDate().toString().contains(sanitizedSearchKeyword) ||
							// o.getChdrLapseDate().toString().contains(sanitizedSearchKeyword) ||
							o.getLapsedReason().contains(sanitizedSearchKeyword)
							|| o.getBcuRemark().contains(sanitizedSearchKeyword)
							|| o.getCustomerResponse().contains(sanitizedSearchKeyword)
							|| o.getCcStatus().contains(sanitizedSearchKeyword)
							|| o.getAgntStatus().contains(sanitizedSearchKeyword)
							|| o.getCcRemark().contains(sanitizedSearchKeyword)
							|| o.getPoName().contains(sanitizedSearchKeyword)
							|| o.getChdrApe().toString().contains(sanitizedSearchKeyword))
					.collect(Collectors.toList());
		}
		if (sortColDir.equalsIgnoreCase("asc")) {
			if (columnsName.equalsIgnoreCase("chdrNum"))
				Collections.sort(list, CollectionAndLapse.ChdrNumComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrAppnum"))
				Collections.sort(list, CollectionAndLapse.ChdrAppnumComparatorAsc);
			else if (columnsName.equalsIgnoreCase("clntMobile1"))
				Collections.sort(list, CollectionAndLapse.clntMobile1ComparatorAsc);
			else if (columnsName.equalsIgnoreCase("clntMobile2"))
				Collections.sort(list, CollectionAndLapse.clntMobile2ComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrPwithTax"))
				Collections.sort(list, CollectionAndLapse.chdrPwithTaxComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrBillfreq"))
				Collections.sort(list, CollectionAndLapse.chdrBillfreqComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrDueDate"))
				Collections.sort(list, CollectionAndLapse.chdrDueDateComparatorAsc);
			// else if(columnsName.equalsIgnoreCase("chdrLapseDate"))
			// Collections.sort(list, CollectionAndLapse.chdrLapseDateComparatorAsc);
			else if (columnsName.equalsIgnoreCase("lapsedReason"))
				Collections.sort(list, CollectionAndLapse.lapsedReasonComparatorAsc);
			else if (columnsName.equalsIgnoreCase("bcuRemark"))
				Collections.sort(list, CollectionAndLapse.bcuRemarkComparatorAsc);
			else if (columnsName.equalsIgnoreCase("customerResponse"))
				Collections.sort(list, CollectionAndLapse.customerResponseComparatorAsc);
			else if (columnsName.equalsIgnoreCase("ccStatus"))
				Collections.sort(list, CollectionAndLapse.ccStatusComparatorAsc);
			else if (columnsName.equalsIgnoreCase("agntStatus"))
				Collections.sort(list, CollectionAndLapse.agntStatusComparatorAsc);
			else if (columnsName.equalsIgnoreCase("ccRemark"))
				Collections.sort(list, CollectionAndLapse.agntCcRemarkComparatorAsc);
			else if (columnsName.equalsIgnoreCase("poName"))
				Collections.sort(list, CollectionAndLapse.poNameComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrApe"))
				Collections.sort(list, CollectionAndLapse.chdrApeComparatorAsc);
			else if (columnsName.equalsIgnoreCase("product"))
				Collections.sort(list, CollectionAndLapse.productComparatorAsc);
		} else {
			if (columnsName.equalsIgnoreCase("chdrNum"))
				Collections.sort(list, CollectionAndLapse.ChdrNumComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrAppnum"))
				Collections.sort(list, CollectionAndLapse.ChdrAppnumComparatorDesc);
			else if (columnsName.equalsIgnoreCase("clntMobile1"))
				Collections.sort(list, CollectionAndLapse.clntMobile1ComparatorDesc);
			else if (columnsName.equalsIgnoreCase("clntMobile2"))
				Collections.sort(list, CollectionAndLapse.clntMobile2ComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrPwithTax"))
				Collections.sort(list, CollectionAndLapse.chdrPwithTaxComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrBillfreq"))
				Collections.sort(list, CollectionAndLapse.chdrBillfreqComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrDueDate"))
				Collections.sort(list, CollectionAndLapse.chdrDueDateComparatorDesc);
			// else if(columnsName.equalsIgnoreCase("chdrLapseDate"))
			// Collections.sort(list, CollectionAndLapse.chdrLapseDateComparatorDesc);
			else if (columnsName.equalsIgnoreCase("lapsedReason"))
				Collections.sort(list, CollectionAndLapse.lapsedReasonComparatorDesc);
			else if (columnsName.equalsIgnoreCase("bcuRemark"))
				Collections.sort(list, CollectionAndLapse.bcuRemarkComparatorDesc);
			else if (columnsName.equalsIgnoreCase("customerResponse"))
				Collections.sort(list, CollectionAndLapse.customerResponseComparatorDesc);
			else if (columnsName.equalsIgnoreCase("ccStatus"))
				Collections.sort(list, CollectionAndLapse.ccStatusComparatorDesc);
			else if (columnsName.equalsIgnoreCase("agntStatus"))
				Collections.sort(list, CollectionAndLapse.agntStatusComparatorDesc);
			else if (columnsName.equalsIgnoreCase("ccRemark"))
				Collections.sort(list, CollectionAndLapse.agntCcRemarkComparatorDesc);
			else if (columnsName.equalsIgnoreCase("poName"))
				Collections.sort(list, CollectionAndLapse.poNameComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrApe"))
				Collections.sort(list, CollectionAndLapse.chdrApeComparatorDesc);
			else if (columnsName.equalsIgnoreCase("product"))
				Collections.sort(list, CollectionAndLapse.productComparatorDesc);
		}
		result = AnonymousHelper.getServerSideDatasource(req, list);

		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/getlapsed", method = RequestMethod.GET)
	public String getLapsed(Principal principal, HttpServletRequest req) {
		String result = "";
		int sortCol = Integer.valueOf(req.getParameter("order[0][column]"));
		String sortColDir = req.getParameter("order[0][dir]");
		String search = req.getParameter("search[value]");
		String columnsName = req.getParameter("columns[" + sortCol + "][data]");

		String userid = principal.getName();

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object podcsn = attr.getRequest().getSession().getAttribute("podcsn");

		List<CollectionAndLapse> getAllCollectionAndLapse = null;
		if (podcsn != null)
			getAllCollectionAndLapse = collectionAndLapseService.getListCollectionandLapseFilterByPODecision(userid,
					(String) podcsn);
		else
			getAllCollectionAndLapse = collectionAndLapseService.getListCollectionandLapse(userid);

		Stream<CollectionAndLapse> allCollections = getAllCollectionAndLapse.stream()
				.filter(s -> s.getObjType().trim().equals("LA"));
		List<CollectionAndLapse> list = allCollections.collect(Collectors.toList());
		if (search != null && search != "") {
			String sanitizedSearchKeyword = AnonymousHelper.sanitizeInput(search);
			list = list.stream()
					.filter(o -> o.getChdrNum().contains(sanitizedSearchKeyword)
							|| o.getChdrAppnum().contains(sanitizedSearchKeyword)
							|| o.getClntMobile1().contains(sanitizedSearchKeyword)
							|| o.getClntMobile2().contains(sanitizedSearchKeyword)
							|| o.getChdrPwithTax().toString().contains(sanitizedSearchKeyword)
							|| o.getChdrBillfreq().contains(sanitizedSearchKeyword)
							|| o.getChdrDueDate().toString().contains(sanitizedSearchKeyword)
							|| o.getChdrLapseDate().toString().contains(sanitizedSearchKeyword)
							|| o.getLapsedReason().contains(sanitizedSearchKeyword)
							|| o.getBcuRemark().contains(sanitizedSearchKeyword)
							|| o.getCustomerResponse().contains(sanitizedSearchKeyword)
							|| o.getCcStatus().contains(sanitizedSearchKeyword)
							|| o.getAgntStatus().contains(sanitizedSearchKeyword)
							|| o.getIsImpactPersistency().contains(sanitizedSearchKeyword)
							|| o.getCcRemark().contains(sanitizedSearchKeyword)
							|| o.getPoName().contains(sanitizedSearchKeyword)
							|| o.getChdrApe().toString().contains(sanitizedSearchKeyword))
					.collect(Collectors.toList());
		}
		if (sortColDir.equalsIgnoreCase("asc")) {
			if (columnsName.equalsIgnoreCase("chdrNum"))
				Collections.sort(list, CollectionAndLapse.ChdrNumComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrAppnum"))
				Collections.sort(list, CollectionAndLapse.ChdrAppnumComparatorAsc);
			else if (columnsName.equalsIgnoreCase("clntMobile1"))
				Collections.sort(list, CollectionAndLapse.clntMobile1ComparatorAsc);
			else if (columnsName.equalsIgnoreCase("clntMobile2"))
				Collections.sort(list, CollectionAndLapse.clntMobile2ComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrPwithTax"))
				Collections.sort(list, CollectionAndLapse.chdrPwithTaxComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrBillfreq"))
				Collections.sort(list, CollectionAndLapse.chdrBillfreqComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrDueDate"))
				Collections.sort(list, CollectionAndLapse.chdrDueDateComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrLapseDate"))
				Collections.sort(list, CollectionAndLapse.chdrLapseDateComparatorAsc);
			else if (columnsName.equalsIgnoreCase("lapsedReason"))
				Collections.sort(list, CollectionAndLapse.lapsedReasonComparatorAsc);
			else if (columnsName.equalsIgnoreCase("bcuRemark"))
				Collections.sort(list, CollectionAndLapse.bcuRemarkComparatorAsc);
			else if (columnsName.equalsIgnoreCase("customerResponse"))
				Collections.sort(list, CollectionAndLapse.customerResponseComparatorAsc);
			else if (columnsName.equalsIgnoreCase("ccStatus"))
				Collections.sort(list, CollectionAndLapse.ccStatusComparatorAsc);
			else if (columnsName.equalsIgnoreCase("agntStatus"))
				Collections.sort(list, CollectionAndLapse.agntStatusComparatorAsc);
			else if (columnsName.equalsIgnoreCase("IsImpactPersistency"))
				Collections.sort(list, CollectionAndLapse.agntIsImpactPersistencyComparatorAsc);
			else if (columnsName.equalsIgnoreCase("ccRemark"))
				Collections.sort(list, CollectionAndLapse.agntCcRemarkComparatorAsc);
			else if (columnsName.equalsIgnoreCase("poName"))
				Collections.sort(list, CollectionAndLapse.poNameComparatorAsc);
			else if (columnsName.equalsIgnoreCase("chdrApe"))
				Collections.sort(list, CollectionAndLapse.chdrApeComparatorAsc);
			else if (columnsName.equalsIgnoreCase("product"))
				Collections.sort(list, CollectionAndLapse.productComparatorAsc);
		} else {
			if (columnsName.equalsIgnoreCase("chdrNum"))
				Collections.sort(list, CollectionAndLapse.ChdrNumComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrAppnum"))
				Collections.sort(list, CollectionAndLapse.ChdrAppnumComparatorDesc);
			else if (columnsName.equalsIgnoreCase("clntMobile1"))
				Collections.sort(list, CollectionAndLapse.clntMobile1ComparatorDesc);
			else if (columnsName.equalsIgnoreCase("clntMobile2"))
				Collections.sort(list, CollectionAndLapse.clntMobile2ComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrPwithTax"))
				Collections.sort(list, CollectionAndLapse.chdrPwithTaxComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrBillfreq"))
				Collections.sort(list, CollectionAndLapse.chdrBillfreqComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrDueDate"))
				Collections.sort(list, CollectionAndLapse.chdrDueDateComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrLapseDate"))
				Collections.sort(list, CollectionAndLapse.chdrLapseDateComparatorDesc);
			else if (columnsName.equalsIgnoreCase("lapsedReason"))
				Collections.sort(list, CollectionAndLapse.lapsedReasonComparatorDesc);
			else if (columnsName.equalsIgnoreCase("bcuRemark"))
				Collections.sort(list, CollectionAndLapse.bcuRemarkComparatorDesc);
			else if (columnsName.equalsIgnoreCase("customerResponse"))
				Collections.sort(list, CollectionAndLapse.customerResponseComparatorDesc);
			else if (columnsName.equalsIgnoreCase("ccStatus"))
				Collections.sort(list, CollectionAndLapse.ccStatusComparatorDesc);
			else if (columnsName.equalsIgnoreCase("agntStatus"))
				Collections.sort(list, CollectionAndLapse.agntStatusComparatorDesc);
			else if (columnsName.equalsIgnoreCase("IsImpactPersistency"))
				Collections.sort(list, CollectionAndLapse.agntIsImpactPersistencyComparatorDesc);
			else if (columnsName.equalsIgnoreCase("ccRemark"))
				Collections.sort(list, CollectionAndLapse.agntCcRemarkComparatorDesc);
			else if (columnsName.equalsIgnoreCase("poName"))
				Collections.sort(list, CollectionAndLapse.poNameComparatorDesc);
			else if (columnsName.equalsIgnoreCase("chdrApe"))
				Collections.sort(list, CollectionAndLapse.chdrApeComparatorDesc);
			else if (columnsName.equalsIgnoreCase("product"))
				Collections.sort(list, CollectionAndLapse.productComparatorDesc);
		}

		result = AnonymousHelper.getServerSideDatasource(req, list);

		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/getcountcollectionandlapses", method = RequestMethod.GET)
	public String getCountCollectionandLapses(Principal principal) {
		String result = "";
		String userid = principal.getName();
		ObjectMapper mapper = new ObjectMapper();
		List<CollectionAndLapse> getAllCollectionAndLapse = collectionAndLapseService.getListCollectionandLapse(userid);
		Long CountAllCollections = getAllCollectionAndLapse.stream().filter(s -> s.getObjType().trim().equals("CL"))
				.count();
		Double SumAPECollections = getAllCollectionAndLapse.stream().filter(s -> s.getObjType().trim().equals("CL"))
				.mapToDouble(s -> s.getChdrPwithTax().doubleValue()).sum();
		Long CountAllLapse = getAllCollectionAndLapse.stream().filter(s -> s.getObjType().trim().equals("LA")).count();
		Double SumAPELapse = getAllCollectionAndLapse.stream().filter(s -> s.getObjType().trim().equals("LA"))
				.mapToDouble(s -> s.getChdrApe().doubleValue()).sum();
		List<Object> obj = new ArrayList<Object>();
		obj.add(CountAllCollections);
		obj.add(CountAllLapse);
		obj.add(SumAPECollections);
		obj.add(SumAPELapse);
		try {
			result = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping(value = "/collectionsummary", method = RequestMethod.GET)
	@ResponseBody
	public String collectionSummary(HttpServletRequest req, Principal principal) {
		String result = "";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object[]> list = new HashMap<>();
		List<Object[]> resultList = new ArrayList<>();
		String userid = principal.getName();
		String summaryby = req.getParameter("summaryby");

		try {
			List<CollectionAndLapse> collectionList = collectionAndLapseService.getListCollectionandLapse(userid)
					.stream().filter(o -> o.getObjType().equals("CL")).collect(Collectors.toList());

			if (summaryby.toLowerCase().equals("branch")) {
				list = AnonymousHelper.getDistinctValueForSummary(collectionList, "getBranchCode", "getBranchCode",
						"getBranchName", "getChdrNum", "getChdrPwithTax");
			} else if (summaryby.toLowerCase().equals("agent")) {
				list = AnonymousHelper.getDistinctValueForSummary(collectionList, "getAgntNum", "getAgntNum",
						"getAgntName", "getChdrNum", "getChdrPwithTax");
			}

			List<String> keyList = new ArrayList<String>(list.keySet());

			for (String key : keyList) {
				int count = 0;
				BigDecimal sum = BigDecimal.valueOf(0);
				String summaryKey;
				String summaryName;

				List<Object> summary = Arrays.asList(list.get(key));

				count = summary.size();
				sum = sum.add(BigDecimal.valueOf(summary.stream().map(o -> Double.valueOf(((Object[]) o)[3].toString()))
						.reduce(Double.valueOf(0), Double::sum))).setScale(2, BigDecimal.ROUND_HALF_UP);

				summaryKey = ((Object[]) list.get(key)[0])[0].toString();
				summaryName = AnonymousHelper.replaceNullString(((Object[]) list.get(key)[0])[1], "");

				Object[] objForDisplay = new Object[4];
				objForDisplay[0] = summaryKey;
				objForDisplay[1] = summaryName;
				objForDisplay[2] = count;
				objForDisplay[3] = sum;

				resultList.add(objForDisplay);
			}
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultList.size() > 0) {
			try {
				result = mapper.writeValueAsString(resultList);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return result;
	}

	@RequestMapping(value = "/lapsesummary", method = RequestMethod.GET)
	@ResponseBody
	public String lapseSummary(HttpServletRequest req, Principal principal) {
		String result = "";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object[]> list = new HashMap<>();
		List<Object[]> resultList = new ArrayList<>();
		String userid = principal.getName();
		String summaryby = req.getParameter("summaryby");

		try {
			List<CollectionAndLapse> lapsedList = collectionAndLapseService.getListCollectionandLapse(userid).stream()
					.filter(o -> o.getObjType().equals("LA")).collect(Collectors.toList());

			if (summaryby.toLowerCase().equals("branch")) {
				list = AnonymousHelper.getDistinctValueForSummary(lapsedList, "getBranchCode", "getBranchCode",
						"getBranchName", "getChdrNum", "getChdrApe");
			} else if (summaryby.toLowerCase().equals("agent")) {
				list = AnonymousHelper.getDistinctValueForSummary(lapsedList, "getAgntNum", "getAgntNum", "getAgntName",
						"getChdrNum", "getChdrApe");
			}

			List<String> keyList = new ArrayList<String>(list.keySet());

			for (String key : keyList) {
				int count = 0;
				BigDecimal sum = BigDecimal.valueOf(0);
				String summaryKey;
				String summaryName;

				List<Object> summary = Arrays.asList(list.get(key));

				count = summary.size();
				sum = sum.add(BigDecimal.valueOf(summary.stream().map(o -> Double.valueOf(((Object[]) o)[3].toString()))
						.reduce(Double.valueOf(0), Double::sum))).setScale(2, BigDecimal.ROUND_HALF_UP);

				summaryKey = ((Object[]) list.get(key)[0])[0].toString();
				summaryName = AnonymousHelper.replaceNullString(((Object[]) list.get(key)[0])[1], "");

				Object[] objForDisplay = new Object[4];
				objForDisplay[0] = summaryKey;
				objForDisplay[1] = summaryName;
				objForDisplay[2] = count;
				objForDisplay[3] = sum;

				resultList.add(objForDisplay);
			}
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultList.size() > 0) {
			try {
				result = mapper.writeValueAsString(resultList);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return result;
	}

	@RequestMapping(value = "/getcollectionandlapsecomment", method = RequestMethod.GET)
	@ResponseBody
	public String getCollectionandLapseComment(HttpServletRequest req) {
		String result = "";
		String objkey = req.getParameter("objkey");
		String objtype = req.getParameter("objtype");
		ObjectMapper mapper = new ObjectMapper();
		List<CollectionandLapseComment> listComments = collectionAndLapseService.listComment(objkey, objtype);
		try {
			result = mapper.writeValueAsString(listComments);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping(value = "/addcollectionandlapsecomment", method = RequestMethod.GET)
	@ResponseBody
	public String addCollectionandLapseComment(HttpServletRequest req, Principal principal) {
		String result = "";
		String objkey = req.getParameter("objkey");
		String objtype = req.getParameter("objtype");
		String comment = req.getParameter("commenttext");
		String commentby = principal.getName();
		ObjectMapper mapper = new ObjectMapper();
		CollectionandLapseComment clc = new CollectionandLapseComment();
		clc.setTabKey(objkey);
		clc.setObjType(objtype);
		clc.setCcText(comment);
		clc.setCcBy(commentby);
		clc.setCcByName(commentby);
		clc.setCcDate(new Date());
		collectionAndLapseService.AddCollectionandLapseComment(clc);

		List<CollectionandLapseComment> listComments = collectionAndLapseService.listComment(objkey, objtype);
		try {
			result = mapper.writeValueAsString(listComments);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping(value = "/bcucallstatussummary", method = RequestMethod.GET)
	@ResponseBody
	public String bcuCallStatusSummary(HttpServletRequest req, Principal principal) {
		String result = "";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object[]> list = new HashMap<>();
		List<Object[]> resultList = new ArrayList<>();
		String userid = principal.getName();
		String type = req.getParameter("type");

		try {
			List<CollectionAndLapse> fullList = collectionAndLapseService.getListCollectionandLapse(userid).stream()
					.filter(o -> o.getObjType().equals(type) && !o.getCcStatus().equals(""))
					.collect(Collectors.toList());

			list = AnonymousHelper.getDistinctValueForSummary(fullList, "getCcStatus", "getCcStatus", "getChdrApe");

			List<String> keyList = new ArrayList<String>(list.keySet());

			for (String key : keyList) {
				int count = 0;
				BigDecimal sum = BigDecimal.valueOf(0);
				String summaryKey;

				List<Object> summary = Arrays.asList(list.get(key));

				count = summary.size();
				sum = sum.add(BigDecimal.valueOf(summary.stream().map(o -> Double.valueOf(((Object[]) o)[1].toString()))
						.reduce(Double.valueOf(0), Double::sum))).setScale(2, BigDecimal.ROUND_HALF_UP);

				summaryKey = ((Object[]) list.get(key)[0])[0].toString();

				Object[] objForDisplay = new Object[3];
				objForDisplay[0] = summaryKey;
				objForDisplay[1] = count;
				objForDisplay[2] = sum;

				resultList.add(objForDisplay);
			}
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultList.size() > 0) {
			try {
				result = mapper.writeValueAsString(resultList);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return result;
	}

	@RequestMapping(value = "/lapseducsummary", method = RequestMethod.GET)
	@ResponseBody
	public String lapsedUcSummary(HttpServletRequest req, Principal principal) {
		String result = "";
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object[]> list = new HashMap<>();
		List<Object[]> resultList = new ArrayList<>();
		String userid = principal.getName();
		String type = req.getParameter("type");
		String t = tableSetupService.itemitemFindNameByReserve1("lapseUcStatus", 1).get(0).getReserve2();
		String[] test = t.split("\\|\\|");
		List<String> test2 = Arrays.asList(test);

		try {
			List<CollectionAndLapse> fullList = collectionAndLapseService
					.getListCollectionandLapse(userid).stream().filter(o -> o.getObjType().equals(type)
							&& !o.getCcStatus().equals("") && test2.contains(o.getCcStatus().toLowerCase()))
					.collect(Collectors.toList());

			double totalCount = fullList.size();

			list = AnonymousHelper.getDistinctValueForSummary(fullList, "getCcStatus", "getCcStatus", "getChdrApe");

			List<String> keyList = new ArrayList<String>(list.keySet());

			for (String key : keyList) {
				double count = 0;
				double percentage = 0.00;
				BigDecimal sum = BigDecimal.valueOf(0);
				String summaryKey;

				List<Object> summary = Arrays.asList(list.get(key));

				count = summary.size();
				percentage = Math.round((count / totalCount) * 10000.0) / 10000.0; // 4 decimal places
				sum = sum.add(BigDecimal.valueOf(summary.stream().map(o -> Double.valueOf(((Object[]) o)[1].toString()))
						.reduce(Double.valueOf(0), Double::sum))).setScale(2, BigDecimal.ROUND_HALF_UP);

				summaryKey = ((Object[]) list.get(key)[0])[0].toString();

				Object[] objForDisplay = new Object[4];
				objForDisplay[0] = summaryKey;
				objForDisplay[1] = percentage;
				objForDisplay[2] = count;
				objForDisplay[3] = sum;

				resultList.add(objForDisplay);
			}
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultList.size() > 0) {
			try {
				result = mapper.writeValueAsString(resultList);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return result;
	}

	@PreAuthorize("hasAnyAuthority('ACCESS_BDMCODE')")
	@RequestMapping(value = "/getbdmcode", method = RequestMethod.GET)
	@ResponseBody
	public String getBdmcode() {
		String result = "";
		List<List<Object[]>> list = referralService.listSearchReferralMgtParam(
				SecurityContextHolder.getContext().getAuthentication().getName(), "", "", "", "", "", "");

		ObjectMapper mapper = new ObjectMapper();

		try {
			result = mapper.writeValueAsString(list.get(2));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}
	//reinstatement controller
	@RequestMapping(value = "/reinstatementDDForm/{objKey}", method = RequestMethod.GET)
	public ModelAndView reinstatementDDForm(@PathVariable("objKey") String objKey, Principal principal,
			HttpServletRequest req) throws ParseException {

		ModelAndView model = reinstatementFormHelper(objKey, principal, req);
		model.addObject("isDD", true);
		return model;
	}

	@RequestMapping(value = "/reinstatementSOForm/{objKey}", method = RequestMethod.GET)
	public ModelAndView reinstatementSOForm(@PathVariable("objKey") String objKey, Principal principal,
			HttpServletRequest req) throws ParseException {

		ModelAndView model = reinstatementFormHelper(objKey, principal, req);
		model.addObject("isDD", false);
		return model;
	}

	private ModelAndView reinstatementFormHelper(String objKey, Principal principal, HttpServletRequest req)
			throws ParseException {
		ModelAndView model = new ModelAndView();

		Map<String, Object> generateDDForm = null;

		try {
			generateDDForm = ddService.generateReinstatementService(objKey);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (generateDDForm == null) {
			throw new ResourceNotFoundException();
		}

		for (String key : generateDDForm.keySet()) {
			model.addObject(key, generateDDForm.get(key));
		}

		model.addObject("version", GlobalVar.version);
		model.setViewName("report/PruQuote_DD_Form");

		return model;
	}

}