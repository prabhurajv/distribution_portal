package com.pru.pruquote.controller;

import java.security.Principal;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.pru.pruquote.model.Permission;
import com.pru.pruquote.service.SecurityService;


@Controller
@PreAuthorize("hasAuthority('ACCESS_PERMISSION')")
public class PermissionController {	
	
	@Autowired	
	private SecurityService securityService;
		
	
	@ModelAttribute("permission")
	public Permission permissionConstructor(){
    	return new Permission();
    }	
		
		
	@RequestMapping(value =  "/permission", method = RequestMethod.GET)
	public ModelAndView permissionmgt() {

		ModelAndView model = new ModelAndView("permissionmgt");
		model.addObject("permissions", securityService.permissionFindAll());
		return model;
	}
	
	@RequestMapping(value =  "/newpermission", method = RequestMethod.GET)
	public ModelAndView newPermission() {

		ModelAndView model = new ModelAndView();		
		model.setViewName("newpermission");
		return model;
	}
	
	@RequestMapping(value="/newpermission",method=RequestMethod.POST)
	public String addNewPermission(@Valid @ModelAttribute("permission") Permission permission
			,BindingResult result
			,Principal principal,Model model,HttpServletRequest request){
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return "newpermission";			
		}
		String info="create permission "+permission.getPermissionId()+":"+permission.getPermissionName();
		permission.setCreatedDate(new Date());
		permission.setCreatedBy(principal.getName());
		permission.setStatus((byte) 1);
		
		securityService.permissionSave(permission);
		securityService.saveLogWithInfo(request, info, principal.getName());
		return "redirect:/permission";
	}
	
	@RequestMapping(value =  "/permission/{permissionId}/edit", method = RequestMethod.GET)
	public String editPermission(@PathVariable("permissionId") Integer permissionId,Model model) {
		Permission permission=securityService.permissionFindOne(permissionId);				
		model.addAttribute(permission);						
		return "editpermission";

	}
	
	@RequestMapping(value =  "/permission/{permissionId}/edit", method = RequestMethod.POST)
	public String processEditUser(@PathVariable("permissionId") Integer permissionId
			,@Valid @ModelAttribute("permission") Permission permission
			,BindingResult result,Model model,Principal principal,HttpServletRequest request) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return editPermission(permissionId,model);			
		}
		if(permission.getStatus()==0){
			
			if(securityService.rolePermissionFindByPermissionNotDelete(permissionId).size()>0){
				ObjectError error = new ObjectError("permission","Unable to disable, this permission is in used!");
				result.addError(error);
				model.addAttribute("errors", result.getAllErrors());
				return editPermission(permissionId,model);	
			}
		}
		String info="edit permission "+permission.getPermissionId()+":"+permission.getPermissionName();
		permission.setLastUpdatedDate(new Date());
		permission.setLastUpdatedBy(principal.getName());
		securityService.permissionSave(permission);
		securityService.saveLogWithInfo(request, info, principal.getName());
		return "redirect:/permission";
	}
}