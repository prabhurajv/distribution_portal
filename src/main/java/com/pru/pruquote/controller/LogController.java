package com.pru.pruquote.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.pru.pruquote.service.SecurityService;


@Controller
@PreAuthorize("hasAuthority('ACCESS_LOG')")
public class LogController {	
	
	@Autowired	
	private SecurityService securityService;
		
				
		
//	@Link(label="Log", family="LogController", parent = "")
//	@RequestMapping(value =  "/log", method = RequestMethod.GET)
//	public ModelAndView logmgt() {
//
//		ModelAndView model = new ModelAndView("logmgt");
//		Date endTime=new Date();
//		Calendar cal = Calendar.getInstance(); 
//		cal.set(Calendar.HOUR_OF_DAY, 23);
//		cal.set(Calendar.MINUTE, 59);
//		cal.set(Calendar.SECOND, 59);
//		cal.set(Calendar.MILLISECOND, 0);
//		endTime=cal.getTime();
//		
//		cal.set(Calendar.HOUR_OF_DAY, 0);
//		cal.set(Calendar.MINUTE, 0);
//		cal.set(Calendar.SECOND, 0);
//		cal.set(Calendar.MILLISECOND, 0);
//		cal.add(Calendar.DATE, -7);
//		Date startTime;
//		startTime=cal.getTime();		
//		
//		model.addObject("logs", securityService.getLogByDate(startTime, endTime));
//		return model;
//	}
	
	@RequestMapping(value =  "/log", method = RequestMethod.GET)
	public ModelAndView logRetrieve(@RequestParam(required=false) String DateFrom,@RequestParam(required=false) String DateTo) {

		ModelAndView model = new ModelAndView("logmgt");
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		
		Date endTime;
		Date startTime;
		
		try {
			endTime=sdf.parse(DateTo);
			startTime=sdf.parse(DateFrom);
			Calendar cal = Calendar.getInstance(); 
			cal.setTime(endTime);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			cal.set(Calendar.MILLISECOND, 0);
			endTime=cal.getTime();
			
			cal.setTime(startTime);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);			
			
			startTime=cal.getTime();		
			model.addObject("startDate", sdf.format(startTime));
			model.addObject("endDate", sdf.format(endTime));
			model.addObject("logs", securityService.getLogByDate(startTime, endTime));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Calendar cal = Calendar.getInstance(); 
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			cal.set(Calendar.MILLISECOND, 0);
			endTime=cal.getTime();
			
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			cal.add(Calendar.DATE, -7);			
			startTime=cal.getTime();		
			
			model.addObject("startDate", sdf.format(startTime));
			model.addObject("endDate", sdf.format(endTime));
			model.addObject("logs", securityService.getLogByDate(startTime, endTime));
			return model;
//			e.printStackTrace();
		}
				
		return model;
	}

	
}