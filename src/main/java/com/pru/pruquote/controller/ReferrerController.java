package com.pru.pruquote.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pru.pruquote.dao.IUserDao;
import com.pru.pruquote.model.Referrer;
import com.pru.pruquote.service.ReferrerService;
import com.pru.pruquote.service.UserService;
import com.pru.pruquote.utility.AnonymousHelper;

@Controller
@PreAuthorize("hasAuthority('ACCESS_REFERRER')")
public class ReferrerController {
	@Autowired
	private ReferrerService referrerService;
	
	@Autowired
	private IUserDao userDao;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/referrer", method = RequestMethod.GET)
	public String referrerHome(ModelMap model){
		model.put("pageTitle", "Referrer Management");
		
		return "referral/referrer";
	}
	
	@ResponseBody
	@RequestMapping(value = "/referrer/search", method = RequestMethod.POST)
	public String searchReferrer(HttpServletRequest request){
		String result = "";
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			List<Referrer> list = referrerService.listReferrer(SecurityContextHolder.getContext().getAuthentication().getName(), 
					request.getParameter("keyword").toString());
			
			result = mapper.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (HibernateException e){
			e.printStackTrace();
		}
		
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/referrer/search/serverside", method = RequestMethod.POST)
	public String searchReferrerServerSide(HttpServletRequest request){
		String result = "";
		int sortCol = Integer.valueOf(request.getParameter("order[0][column]"));
        String sortColDir = request.getParameter("order[0][dir]");
        String search = request.getParameter("search[value]");
        String columnsName = request.getParameter("columns[" + sortCol + "][data]");
		
		try {
			List<Referrer> list = referrerService.listReferrer(SecurityContextHolder.getContext().getAuthentication().getName(), 
					request.getParameter("keyword").toString());
			
			if(search != null && search != "") {
				String sanitizedSearchKeyword = AnonymousHelper.sanitizeInput(search);
				list = list.stream().filter(o -> o.getStaffName().toLowerCase().contains(sanitizedSearchKeyword.toLowerCase()) ||
						o.getStaffBranch().toLowerCase().contains(sanitizedSearchKeyword.toLowerCase()) ||
						o.getStaffPosition().toLowerCase().contains(sanitizedSearchKeyword.toLowerCase()) ||
						(o.getIsCertified() + "").equals(sanitizedSearchKeyword) ||
						(o.getStatus() + "").equals(sanitizedSearchKeyword)).collect(Collectors.toList());
			}
			
			result = AnonymousHelper.getServerSideDatasource(request, list);
		} catch (HibernateException e){
			e.printStackTrace();
		}
		
		return result;
	}
	
	@RequestMapping(value = "/newreferrer", method = RequestMethod.GET)
	public String newReferrer(ModelMap model, @Valid Referrer referrer, BindingResult result){
		model.put("referrer", new Referrer());
		model.put("pageTitle", "New Referrer");
		model.put("positions", referrerService.listPosition());
		
		String user = SecurityContextHolder.getContext().getAuthentication().getName();
		
		if("326625,326913,318766,330643,329019,281709".contains(user)){
			model.put("branches", referrerService.listAllBranch());
		}else{
			model.put("branches", referrerService.listSpecificBranch(userService.getUser(user).getBranchId()));
		}
		
		return "referral/newreferrer";
	}
	
	@RequestMapping(value = "/newreferrer", method = RequestMethod.POST)
	public String saveReferrer(ModelMap model, @Valid Referrer referrer, BindingResult result, HttpServletRequest request) throws ParseException{
		if(result.hasErrors()){
			model.put("referrer", referrer);
			model.put("positions", referrerService.listPosition());
			
			String user = SecurityContextHolder.getContext().getAuthentication().getName();
			
			if("326625,326913,318766,330643,329019,281709".contains(user)){
				model.put("branches", referrerService.listAllBranch());
			}else{
				model.put("branches", referrerService.listSpecificBranch(userService.getUser(user).getBranchId()));
			}
			
			return "referral/newreferrer";
		}
		
		referrer.setStaffName(AnonymousHelper.sanitizeInput(referrer.getStaffName()));
		referrer.setStaffPosition(AnonymousHelper.sanitizeInput(referrer.getStaffPosition()));
		referrer.setStaffBranch(AnonymousHelper.sanitizeInput(referrer.getStaffBranch()));
		referrer.setStaffId(AnonymousHelper.sanitizeInput(referrer.getStaffId()));
		referrer.setValidFlag(referrer.isValid() ? 1 : 0);
		referrer.setTrnBy(SecurityContextHolder.getContext().getAuthentication().getName());
		referrer.setTrnDate(AnonymousHelper.getFormattedDate(request.getParameter("trnDate"), "yyyy-MM-dd"));
		referrerService.referrerSave(referrer);
		
		model.put("referrer", new Referrer());
		
		return "redirect:/newreferrer";
	}
	
	@RequestMapping(value = "/referrer/edit/{id}", method = RequestMethod.GET)
	public String editReferrer(ModelMap model, @PathVariable int id, @Valid Referrer referrer, BindingResult result){
		model.put("referrer", referrerService.getOneReferrer(id));
		model.put("pageTitle", "Edit Referrer");
		model.put("positions", referrerService.listPosition());
		
		String user = SecurityContextHolder.getContext().getAuthentication().getName();
		
		if("326625,326913,318766,330643,329019,281709".contains(user)){
			model.put("branches", referrerService.listAllBranch());
		}else{
			model.put("branches", referrerService.listSpecificBranch(userService.getUser(user).getBranchId()));
		}
		
		return "referral/editreferrer";
	}
	
	@RequestMapping(value = "/referrer/edit/{id}", method = RequestMethod.POST)
	public String updateReferrer(ModelMap model, @PathVariable int id, @Valid Referrer referrer, BindingResult result, HttpServletRequest request) throws ParseException{
		if(result.hasErrors()){
			model.put("referrer", referrer);
			model.put("positions", referrerService.listPosition());
			
			String user = SecurityContextHolder.getContext().getAuthentication().getName();
			
			if("326625,326913,318766,330643,329019,281709".contains(user)){
				model.put("branches", referrerService.listAllBranch());
			}else{
				model.put("branches", referrerService.listSpecificBranch(userService.getUser(user).getBranchId()));
			}
			
			return "referral/editreferrer";
		}
		
		referrer.setStaffName(AnonymousHelper.sanitizeInput(referrer.getStaffName()));
		referrer.setStaffPosition(AnonymousHelper.sanitizeInput(referrer.getStaffPosition()));
		referrer.setStaffBranch(AnonymousHelper.sanitizeInput(referrer.getStaffBranch()));
		referrer.setStaffId(AnonymousHelper.sanitizeInput(referrer.getStaffId()));
		referrer.setValidFlag(referrer.isValid() ? 1 : 0);
		referrer.setTrnBy(SecurityContextHolder.getContext().getAuthentication().getName());
		referrer.setTrnDate(AnonymousHelper.getFormattedDate(request.getParameter("trnDate"), "yyyy-MM-dd"));
		referrerService.referrerSave(referrer);
		
		model.put("referrer", new Referrer());
		
		return "redirect:/referrer";
	}
}
