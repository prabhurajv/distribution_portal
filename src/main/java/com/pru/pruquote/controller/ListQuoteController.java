package com.pru.pruquote.controller;


import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import com.pru.pruquote.model.PruquoteParm;
import com.pru.pruquote.model.User;
import com.pru.pruquote.model.listPruquote;
import com.pru.pruquote.service.QuoteService;
import com.pru.pruquote.service.SecurityService;
import com.pru.pruquote.service.TableSetupService;
import com.pru.pruquote.service.ListQuoteService;

@Controller
@PreAuthorize("hasAuthority('ACCESS_LIST_QUOTE')")
public class ListQuoteController {	
	
	@Autowired	
	private ListQuoteService ListQuoteService;
	 
	@Autowired	
	private QuoteService quoteService;
	
	@Autowired	
	private TableSetupService tableSetupService;
	
	
	@ModelAttribute("pruquoteparm")
    public PruquoteParm pruquoteparmConstructor(){
    	return new PruquoteParm();
    }
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
        binder.registerCustomEditor(Date.class,"commDate",new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class,"la1dateOfBirth",new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class,"syndate",new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class,"la2dateOfBirth",new CustomDateEditor(dateFormat, false));
    }
			
	@RequestMapping(value =  "/listquote", method = RequestMethod.GET)
	public ModelAndView ListQuote(Principal principal){
		ModelAndView model = new ModelAndView();			
		model.addObject("listQuote", ListQuoteService.listQuote(principal.getName(),""));		
		model.setViewName("listquote");
		return model;
	}
}