package com.pru.pruquote.controller;


import java.math.BigDecimal;
import java.security.Principal;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.owasp.html.Sanitizers;
import org.owasp.html.PolicyFactory;

//import dummiesmind.breadcrumb.springmvc.annotations.Link;

import com.pru.pruquote.model.PruquoteParm;
import com.pru.pruquote.model.User;
import com.pru.pruquote.service.QuoteService;
import com.pru.pruquote.service.SecurityService;
import com.pru.pruquote.service.TableSetupService;
import com.pru.pruquote.service.UABService;
import com.pru.pruquote.utility.GlobalVar;


@Controller
@PreAuthorize("hasAnyAuthority('ACCESS_EDUSAVE,ACCESS_EDUSMART,ACCESS_PRUMYFAMILY,ACCESS_MORTGAGE')")
public class UABController {	
	
	@Autowired	
	private UABService uabService;
	
	@Autowired
	private QuoteService quoteService;
	
	@Autowired	
	private TableSetupService tableSetupService;
	
	@Autowired	
	private SecurityService securityService;
	
	@ModelAttribute("pruquoteparm")
    public PruquoteParm pruquoteparmConstructor(){
    	return new PruquoteParm();
    }
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
        binder.registerCustomEditor(Date.class,"commDate",new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class,"la1dateOfBirth",new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class,"syndate",new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class,"la2dateOfBirth",new CustomDateEditor(dateFormat, false));
    }
					
	@RequestMapping(value =  "/pruquote_uabnote_repkhmer/{quoteId}", method = RequestMethod.GET)
	public ModelAndView eduSavePreviewKhmer(@PathVariable("quoteId") Long quoteId,Principal principal) {
		ModelAndView model = new ModelAndView();	
		List<PruquoteParm> pruquoteParms=quoteService.findValidQuote(quoteId, principal.getName());
		if(pruquoteParms.size()<=0) throw new ResourceNotFoundException();		
		List<List<Object[]>> results=uabService.generateUAB(quoteId);
		
		String nome = ((Object[])results.get(1).toArray()[0])[0].toString();
		
		if(nome.equals("No ME")){
			if(results.get(3).toArray().length > 0 && ((Object[])results.get(3).toArray()[0]).length > 0){
				nome = ((Object[])results.get(3).toArray()[0])[0].toString();
			}
		}
		
		model.addObject("L1Info",results.get(0));
		model.addObject("L1Exam",results.get(1));
		model.addObject("L2Info",results.get(2));
		model.addObject("L2Exam",results.get(3));
		model.addObject("Product",pruquoteParms.get(0).getProduct());
		model.addObject("nome", nome);
		
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd"); 
        
        Date dOBDate;
        try {
        	dOBDate=dt.parse(pruquoteParms.get(0).getLa1dateOfBirth().toString());
		} catch (Exception e) {
			dOBDate=new Date();
		}
        
        String version = dt.format(dOBDate).replace("-", "") + "-" + String.format("%06d", quoteId);
        model.addObject("serialno", version);
        model.addObject("version", GlobalVar.version);
        model.setViewName("report/PruQuote_UAB_RepKhmer");
		return model;
	}
	
	@RequestMapping(value =  "/pruquote_uabnote_replatin/{quoteId}", method = RequestMethod.GET)
	public ModelAndView eduSavePreviewLatin(@PathVariable("quoteId") Long quoteId,Principal principal) {
		ModelAndView model = new ModelAndView();	
		List<PruquoteParm> pruquoteParms=quoteService.findValidQuote(quoteId, principal.getName());
		if(pruquoteParms.size()<=0) throw new ResourceNotFoundException();		
		List<List<Object[]>> results=uabService.generateUAB(quoteId);
		
		String nome = ((Object[])results.get(1).toArray()[0])[0].toString();
		
		if(nome.equals("No ME")){
			if(results.get(3).toArray().length > 0 && ((Object[])results.get(3).toArray()[0]).length > 0){
				nome = ((Object[])results.get(3).toArray()[0])[0].toString();
			}
		}
		
		model.addObject("L1Info",results.get(0));
		model.addObject("L1Exam",results.get(1));
		model.addObject("L2Info",results.get(2));
		model.addObject("L2Exam",results.get(3));
		model.addObject("Product",pruquoteParms.get(0).getProduct());
		model.addObject("nome", nome);
		
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd"); 
        
        Date dOBDate;
        try {
        	dOBDate=dt.parse(pruquoteParms.get(0).getLa1dateOfBirth().toString());
		} catch (Exception e) {
			dOBDate=new Date();
		}
        
        String version = dt.format(dOBDate).replace("-", "") + "-" + String.format("%06d", quoteId);
        model.addObject("serialno",version);
        model.addObject("version", GlobalVar.version);
        model.setViewName("report/PruQuote_UAB_RepLatin");
		return model;
	}
				
	
}