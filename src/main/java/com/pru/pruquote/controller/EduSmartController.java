package com.pru.pruquote.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.owasp.html.Sanitizers;
import org.apache.commons.io.IOUtils;
import org.owasp.html.PolicyFactory;

import com.pru.pruquote.model.Occupation;
import com.pru.pruquote.model.PruquoteParm;
import com.pru.pruquote.model.QuoteOccupation;
import com.pru.pruquote.model.User;
import com.pru.pruquote.model.listPruquote;
import com.pru.pruquote.service.QuoteService;
import com.pru.pruquote.service.SecurityService;
import com.pru.pruquote.service.TableSetupService;
import com.pru.pruquote.utility.AnonymousHelper;
import com.pru.pruquote.utility.GlobalVar;
import com.pru.pruquote.service.DDService;
import com.pru.pruquote.service.ListQuoteService;
import com.pru.pruquote.service.LogService;
import com.pru.pruquote.service.OccupationService;;

@Controller
@PreAuthorize("hasAuthority('ACCESS_EDUSMART')")
public class EduSmartController {

	@Autowired
	private QuoteService quoteService;

	@Autowired
	private ListQuoteService listQuoteService;

	@Autowired
	private TableSetupService tableSetupService;

	@Autowired
	private SecurityService securityService;

	@Autowired
	private OccupationService occupationService;

	@Autowired
	private LogService logService;

	@Autowired
	private DDService ddService;

	@ModelAttribute("pruquoteparm")
	public PruquoteParm pruquoteparmConstructor() {
		return new PruquoteParm();
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, "commDate", new CustomDateEditor(dateFormat, false));
		binder.registerCustomEditor(Date.class, "la1dateOfBirth", new CustomDateEditor(dateFormat, false));
		binder.registerCustomEditor(Date.class, "syndate", new CustomDateEditor(dateFormat, false));
		binder.registerCustomEditor(Date.class, "la2dateOfBirth", new CustomDateEditor(dateFormat, false));
	}

	@RequestMapping(value = "/pruquote_edusmart_repkhmer/{quoteId}", method = RequestMethod.GET)
	public ModelAndView eduSmartPreviewKhmer(@PathVariable("quoteId") Long quoteId, Principal principal,
			HttpServletRequest req) {
		ModelAndView model = new ModelAndView();
		List<listPruquote> listPruquoteParms = listQuoteService.listQuote(principal.getName(), Long.toString(quoteId));
		List<PruquoteParm> pruquoteParms = quoteService.findValidQuote(quoteId, "BTR4", principal.getName());
		if (listPruquoteParms.size() <= 0 & pruquoteParms.size() <= 0) {
			throw new ResourceNotFoundException();
		} else if (listPruquoteParms.size() >= 0) {
			pruquoteParms = quoteService.findValidQuote(quoteId);
			// model.addObject("name","");
			// model.addObject("code","");
		} else {
			if (pruquoteParms.size() <= 0) {
				throw new ResourceNotFoundException();
			} else {
				// User user=securityService.userFindOne(principal.getName());
				// model.addObject("name",user.getFullNameKH());
				// model.addObject("code",user.getUserId());
			}
		}
		List<String> results = quoteService.generateEduSmartKhmerQuote(quoteId, "Khmer", "RN");
		model.addObject("header1", results.get(0));
		model.addObject("body1", results.get(1));
		model.addObject("body2", results.get(2));
		model.addObject("body3A", results.get(3));
		model.addObject("body3B", results.get(4));
		model.addObject("footer1", results.get(5));
		model.addObject("footer2", results.get(6));
		model.addObject("serialno1", results.get(7));
		model.addObject("serialno2", results.get(8));
		model.addObject("body4", results.get(9));
		model.addObject("body5", results.get(10));
		model.addObject("body5", results.get(10));
		model.addObject("PS", results.get(11));

		User user = securityService.findUserByUsername(pruquoteParms.get(0).getCoUser());
		model.addObject("name", user.getFullNameKh());
		model.addObject("code", user.getUserId());

		model.setViewName("report/PruQuote_Edusmart_RepKhmer_NO_Saver");

		model.addObject("version", GlobalVar.version);
		model.addObject("quoteOcc", occupationService.getQuoteOccupationByQuoteId(quoteId));
		Occupation occ = occupationService.getOccupationByEngName("Others");
		if (occ == null) {
			model.addObject("occOtherId", 0);
		} else {
			model.addObject("occOtherId", occ.getId());
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object onbehalf = attr.getRequest().getSession().getAttribute("isonbehalf");
		model.addObject("isonbehalf", onbehalf);
		attr.getRequest().getSession().removeAttribute("isonbehalf");

		if (onbehalf.equals("15")) {
			String action = "Quote Id = " + quoteId + "On be half = " + "Yes";
			String ip = req.getRemoteHost();
			String pip = req.getRemoteHost();
			Date date = new Date();
			logService.logUserAction(user.getUserId(), action, ip, pip, date);
		}

		return model;
	}

	@RequestMapping(value = "/pruquote_edusmart_replatin/{quoteId}", method = RequestMethod.GET)
	public ModelAndView eduSmartPreviewLatin(@PathVariable("quoteId") Long quoteId, Principal principal,
			HttpServletRequest req) {
		ModelAndView model = new ModelAndView();

		List<listPruquote> listPruquoteParms = listQuoteService.listQuote(principal.getName(), Long.toString(quoteId));
		List<PruquoteParm> pruquoteParms = quoteService.findValidQuote(quoteId, "BTR4", principal.getName());
		if (listPruquoteParms.size() <= 0 & pruquoteParms.size() <= 0) {
			throw new ResourceNotFoundException();
		} else if (listPruquoteParms.size() >= 0) {
			pruquoteParms = quoteService.findValidQuote(quoteId);
			// model.addObject("name","");
			// model.addObject("code","");
		} else {
			if (pruquoteParms.size() <= 0) {
				throw new ResourceNotFoundException();
			} else {
				// User user=securityService.userFindOne(principal.getName());
				// model.addObject("name",user.getFullNameKH());
				// model.addObject("code",user.getUserId());
			}
		}

		List<String> results = quoteService.generateEduSmartLatinQuote(quoteId, "Latin", "RN");
		model.addObject("header1", results.get(0));
		model.addObject("body1", results.get(1));
		model.addObject("body2", results.get(2));
		model.addObject("body3", results.get(3));
		model.addObject("footer1", results.get(4));
		model.addObject("serialno1", results.get(5));
		model.addObject("serialno2", results.get(6));
		model.addObject("body4", results.get(7));
		model.addObject("body5", results.get(8));
		model.addObject("PS", results.get(9));
		User user = securityService.findUserByUsername(pruquoteParms.get(0).getCoUser());
		model.addObject("name", user.getFullNameKh());
		model.addObject("code", user.getUserId());
		model.setViewName("report/PruQuote_Edusmart_RepLatin_NO_Saver");
		model.addObject("version", GlobalVar.version);
		model.addObject("quoteOcc", occupationService.getQuoteOccupationByQuoteId(quoteId));
		Occupation occ = occupationService.getOccupationByEngName("Others");
		if (occ == null) {
			model.addObject("occOtherId", 0);
		} else {
			model.addObject("occOtherId", occ.getId());
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object onbehalf = attr.getRequest().getSession().getAttribute("isonbehalf");
		model.addObject("isonbehalf", onbehalf);
		attr.getRequest().getSession().removeAttribute("isonbehalf");

		if (onbehalf.equals("15")) {
			String action = "Quote Id = " + quoteId + "and On be half = " + "Yes";
			String ip = req.getRemoteHost();
			String pip = req.getRemoteHost();
			Date date = new Date();
			logService.logUserAction(user.getUserId(), action, ip, pip, date);
		}

		return model;
	}

	@RequestMapping(value = "/edusmart", method = RequestMethod.GET)
	public ModelAndView edusmartForm(Principal principal) {
		ModelAndView model = new ModelAndView();
		model.addObject("edusmartList", quoteService.viewQuote("BTR4", principal.getName()));
		model.addObject("pageTitle", "eduCARE");
		model.setViewName("quote/edusmart");
		return model;
	}

	@RequestMapping(value = "/newedusmart", method = RequestMethod.GET)
	public String newedusmartForm(Model model) {
		model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
		model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%BTR4%"));
		model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%BTR4%"));
		model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
		model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long)9, true));
		model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
		model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
		model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
		model.addAttribute("OccupationDdl", occupationService.getAllOccupationDdl());
		model.addAttribute("pageTitle", "New eduCARE");
		return "quote/newedusmart";
	}

	@RequestMapping(value = "/newedusmart", method = RequestMethod.POST, params = "save")
	public String processNewedusmartForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest req) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			return newedusmartForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR4");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");
		if (!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occpo.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo), "occpo", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occla2.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2), "occla2", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		return "redirect:/edusmart";
	}

	@RequestMapping(value = "/newedusmart", method = RequestMethod.POST, params = "previewkh")
	public String processNewedusmartFormPreviewKh(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest request,
			RedirectAttributes redirectAttributes, HttpServletRequest req) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			return newedusmartForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR4");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);
		if (pruquoteparm.getCoL1Rtr2() == 15) {
			Object[] prusaver = (Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
			if (prusaver[3].equals("FALSE".toLowerCase())) {
				redirectAttributes.addFlashAttribute("errorPruSaver",
						"Prusaver MB should between " + prusaver[1] + "-" + prusaver[2]);
				return "redirect:/edusmart/" + pruquoteparm.getId() + "/edit";
			}
		}

		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");
		if (!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occpo.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo), "occpo", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occla2.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2), "occla2", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		// model.addAttribute("previewurl",
		// "/pruquote_edusave_repkhmer/"+pruquoteparm.getId());
		// return editEduSave(pruquoteparm.getId(),model);

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl",
				"/pruquote_edusmart_repkhmer/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview EduSmart Khmer Quote No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/edusmart/" + pruquoteparm.getId() + "/edit";
	}

	@RequestMapping(value = "/newedusmart", method = RequestMethod.POST, params = "previewen")
	public String processNewedusmartFormPreviewEn(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest request,
			RedirectAttributes redirectAttributes, HttpServletRequest req) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			return newedusmartForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR4");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);
		// model.addAttribute("previewurl",
		// "/pruquote_edusave_replatin/"+pruquoteparm.getId());
		// return editEduSave(pruquoteparm.getId(),model);
		if (pruquoteparm.getCoL1Rtr2() == 15) {
			Object[] prusaver = (Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
			if (prusaver[3].equals("FALSE".toLowerCase())) {
				redirectAttributes.addFlashAttribute("errorPruSaver",
						"Prusaver MB should between " + prusaver[1] + "-" + prusaver[2]);
				return "redirect:/edusmart/" + pruquoteparm.getId() + "/edit";
			}
		}

		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");
		if (!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occpo.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo), "occpo", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occla2.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2), "occla2", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl",
				"/pruquote_edusmart_replatin/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview EduSmart English Quote No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/edusmart/" + pruquoteparm.getId() + "/edit";
	}
	
	@RequestMapping(value =  "/edusmart/{id}/edit", method = RequestMethod.GET)
	public String editEduSmart(@PathVariable("id") Long id,Model model) {
		PruquoteParm pruquoteParm=quoteService.getValidQuote(id, "BTR4", SecurityContextHolder.getContext().getAuthentication().getName());		
		if(pruquoteParm==null) throw new ResourceNotFoundException();
		model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
		model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%BTR4%"));
		model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%BTR4%"));
		model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
		model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long)9, true));
		model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5, true));
		model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
		model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
		model.addAttribute("occupations", occupationService.getAllOccupationDdl());
		model.addAttribute("pruquoteparm", pruquoteParm);
		model.addAttribute("pageTitle", "Edit eduCARE");

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object quoteUrl = attr.getRequest().getSession().getAttribute("previewurl");
		if (quoteUrl != null) {
			model.addAttribute("quoteUrl", quoteUrl.toString());
		}
		attr.getRequest().getSession().removeAttribute("previewurl");

		// PruquoteParm pruquotepar = pruquoteParm;
		// List<Occupation> occs = pruquotepar.getOccupations();
		// for ( Occupation occ : occs ) {
		// int occid = occ.getId();
		// QuoteOccupation quoteOcc =
		// occupationService.getQuoteOccupationByQuoteandOccId(pruquoteParm.getId(),
		// occid);
		// String tmpRemark = quoteOcc.getRemark();
		// if (tmpRemark.equals("occla1")) {
		// model.addAttribute("occla1selected",occid);
		// }
		// if (tmpRemark.equals("occpo")) {
		// model.addAttribute("occposelected",occid);
		// }
		// if (tmpRemark.equals("occla2")) {
		// model.addAttribute("occla2selected",occid);
		// }
		// }

		List<QuoteOccupation> qcs = occupationService.getQuoteOccupationByQuoteId(pruquoteParm.getId());
		for (QuoteOccupation qc : qcs) {
			String tmpRemark = qc.getRemark();
			if (tmpRemark.equals("occla1")) {
				model.addAttribute("occla1selected", qc.getOccId());
			}
			if (tmpRemark.equals("occpo")) {
				model.addAttribute("occposelected", qc.getOccId());
			}
			if (tmpRemark.equals("occla2")) {
				model.addAttribute("occla2selected", qc.getOccId());
			}
		}

		return "quote/editedusmart";
	}

	@RequestMapping(value = "/edusmart/{id}/edit", method = RequestMethod.POST, params = "save")
	public String processEditedusmartForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest req) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR4%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR4%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long) 9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
			return "quote/editedusmart";
		}
		PruquoteParm pruquoteParm = quoteService.getValidQuote(pruquoteparm.getId(), "BTR4",
				SecurityContextHolder.getContext().getAuthentication().getName());

		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR4");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		pruquoteparm.setOccupations(pruquoteParm.getOccupations());
		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");

		if (!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occpo.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occpo");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo), "occpo", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occpo));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occla2.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla2");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2), "occla2", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla2));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		return "redirect:/edusmart";
	}

	@RequestMapping(value = "/edusmart/{id}/edit", method = RequestMethod.POST, params = "previewkh")
	public String processEditedusmartKh(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR4%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR4%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long) 9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
			return "quote/editedusave";
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR4");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");

		if (!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occpo.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occpo");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo), "occpo", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occpo));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occla2.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla2");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2), "occla2", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla2));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		if (pruquoteparm.getCoL1Rtr2() == 15) {
			Object[] prusaver = (Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
			if (prusaver[3].equals("FALSE".toLowerCase())) {
				redirectAttributes.addFlashAttribute("errorPruSaver",
						"Prusaver MB should between " + prusaver[1] + "-" + prusaver[2]);
				return "redirect:/edusmart/" + pruquoteparm.getId() + "/edit";
			}
		}
		// return editEduSave(id,model);
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl",
				"/pruquote_edusmart_repkhmer/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview EduSave Khmer Quote No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/edusmart/" + pruquoteparm.getId() + "/edit";
	}

	@RequestMapping(value = "/edusmart/{id}/edit", method = RequestMethod.POST, params = "previewen")
	public String processEditedusmartEn(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR4%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR4%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long) 9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
			return "quote/editedusmart";
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR4");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");

		if (!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occpo.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occpo");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo), "occpo", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occpo));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occla2.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla2");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2), "occla2", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla2));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		// return editEduSave(id,model);
		if (pruquoteparm.getCoL1Rtr2() == 15) {
			Object[] prusaver = (Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
			if (prusaver[3].equals("FALSE".toLowerCase())) {
				redirectAttributes.addFlashAttribute("errorPruSaver",
						"Prusaver MB should between " + prusaver[1] + "-" + prusaver[2]);
				return "redirect:/edusmart/" + pruquoteparm.getId() + "/edit";
			}
		}
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl",
				"/pruquote_edusmart_replatin/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview EduSave English Quote No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/edusmart/" + pruquoteparm.getId() + "/edit";
	}

	@RequestMapping(value = "/getMBBySASmart", method = RequestMethod.GET)
	@ResponseBody
	public String getMBBySA(@RequestParam BigDecimal SA, @RequestParam String premiumTerm) {
		PolicyFactory sanitizer = Sanitizers.FORMATTING.and(Sanitizers.BLOCKS);
		return sanitizer.sanitize(quoteService.getMBBySASmart(SA, premiumTerm));
	}

	@RequestMapping(value = "/getSAByMBSmart", method = RequestMethod.GET)
	@ResponseBody
	public String getSAByMB(@RequestParam BigDecimal MB, @RequestParam String premiumTerm) {
		PolicyFactory sanitizer = Sanitizers.FORMATTING.and(Sanitizers.BLOCKS);
		return sanitizer.sanitize(quoteService.getSAByMBSmart(MB, premiumTerm));
	}

	@RequestMapping(value = "/files/{occId}", method = RequestMethod.GET)
	public void getFile(@PathVariable("occId") int occId, HttpServletResponse response) {
		try {
			Occupation occ = occupationService.getOccupationById(occId);
			// get your file as InputStream
			InputStream is = new ByteArrayInputStream(occ.getFile().getFileOcc());
			// copy it to response's OutputStream
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition",
					"attachment; filename=\"" + occ.getFile().getFileName() + ".pdf\"");
			IOUtils.copy(is, response.getOutputStream());
			response.flushBuffer();
		} catch (IOException ex) {
			throw new RuntimeException("IOError writing file to output stream");
		}

	}

	// here is the function to generate quote

	private String processNewHelper(PruquoteParm pruquoteparm, BindingResult result, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req, Boolean isDD) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			return newedusmartForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR4");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		if (!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/educare_pruquote_dd_form/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		attr.getRequest().getSession().setAttribute("isDD", isDD);
		securityService.saveLogWithInfo(request, "Preview EduSmart DD Form No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/edusmart/" + pruquoteparm.getId() + "/edit";
	}

	@RequestMapping(value = "/newedusmart", method = RequestMethod.POST, params = "previewDD")
	public String processNewedusmartDDFormPreview(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest request,
			RedirectAttributes redirectAttributes, HttpServletRequest req) {

		return processNewHelper(pruquoteparm, result, model, principal, request, redirectAttributes, req, true);
	}

	@RequestMapping(value = "/newedusmart", method = RequestMethod.POST, params = "previewSO")
	public String processNewedusmartSOFormPreview(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest request,
			RedirectAttributes redirectAttributes, HttpServletRequest req) {

		return processNewHelper(pruquoteparm, result, model, principal, request, redirectAttributes, req, false);
	}

	@RequestMapping(value = "/educare_pruquote_dd_form/{quoteId}", method = RequestMethod.GET)
	public ModelAndView EduSmartPreviewDDForm(@PathVariable("quoteId") Long quoteId, Principal principal,
			HttpServletRequest req) throws ParseException {
		ModelAndView model = new ModelAndView();

		Map<String, Object> generateDD = null;

		try {
			generateDD = ddService.generateDD(quoteId, "BTR4");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (generateDD == null)
			throw new ResourceNotFoundException();

		for (String key : generateDD.keySet()) {
			model.addObject(key, generateDD.get(key));
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

		Object isDD = attr.getRequest().getSession().getAttribute("isDD");
		model.addObject("isDD", isDD);

		model.addObject("version", GlobalVar.version);

		Object onbehalf = attr.getRequest().getSession().getAttribute("isonbehalf");
		model.addObject("isonbehalf", onbehalf);
		attr.getRequest().getSession().removeAttribute("isonbehalf");
		attr.getRequest().getSession().removeAttribute("isDD");

		// if (onbehalf.equals("15")) {
		// String action = "Print DD Form on Quote Id = " + quoteId + "and On be half =
		// " + "Yes";
		// String ip = req.getRemoteHost();
		// String pip = req.getRemoteHost();
		// Date date = new Date();
		// logService.logUserAction(user.getUserId(), action, ip, pip, date);
		// }

		model.setViewName("report/PruQuote_DD_Form");
		return model;
	}

	private String processEditHelper(PruquoteParm pruquoteparm, BindingResult result, Long id, Model model,
			Principal principal, HttpServletRequest request, RedirectAttributes redirectAttributes,
			HttpServletRequest req, Boolean isDD) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR4%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR4%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long) 9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
			return "quote/editedusmart";
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR4");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());

		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");

		if (!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/educare_pruquote_dd_form/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		attr.getRequest().getSession().setAttribute("isDD", isDD);

		securityService.saveLogWithInfo(request, "Preview EduSmart Khmer Quote No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/edusmart/" + pruquoteparm.getId() + "/edit";
	}

	@RequestMapping(value = "/edusmart/{id}/edit", method = RequestMethod.POST, params = "previewDD")
	public String processEditedusmartPreviewDDForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req) {
		return processEditHelper(pruquoteparm, result, id, model, principal, request, redirectAttributes, req, true);
	}

	@RequestMapping(value = "/edusmart/{id}/edit", method = RequestMethod.POST, params = "previewSO")
	public String processEditedusmartPreviewSOForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req) {
		return processEditHelper(pruquoteparm, result, id, model, principal, request, redirectAttributes, req, false);
	}
}