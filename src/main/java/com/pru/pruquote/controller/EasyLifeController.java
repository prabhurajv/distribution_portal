package com.pru.pruquote.controller;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.pru.pruquote.model.Occupation;
import com.pru.pruquote.model.PruquoteParm;
import com.pru.pruquote.model.QuoteOccupation;
import com.pru.pruquote.model.User;
import com.pru.pruquote.model.listPruquote;
import com.pru.pruquote.service.QuoteService;
import com.pru.pruquote.service.SecurityService;
import com.pru.pruquote.service.TableSetupService;
import com.pru.pruquote.utility.AnonymousHelper;
import com.pru.pruquote.utility.GlobalVar;
import com.pru.pruquote.service.DDService;
import com.pru.pruquote.service.ListQuoteService;
import com.pru.pruquote.service.LogService;
import com.pru.pruquote.service.OccupationService;

@Controller
@PreAuthorize("hasAuthority('ACCESS_EASY_LIFE')")
public class EasyLifeController {

	@Autowired
	private QuoteService quoteService;

	@Autowired
	private ListQuoteService listQuoteService;

	@Autowired
	private TableSetupService tableSetupService;

	@Autowired
	private SecurityService securityService;

	@Autowired
	private OccupationService occupationService;

	@Autowired
	private LogService logService;
	
	@Autowired
	private DDService ddService;

	@ModelAttribute("pruquoteparm")
	public PruquoteParm pruquoteparmConstructor() {
		return new PruquoteParm();
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, "commDate", new CustomDateEditor(dateFormat, false));
		binder.registerCustomEditor(Date.class, "la1dateOfBirth", new CustomDateEditor(dateFormat, false));
		binder.registerCustomEditor(Date.class, "syndate", new CustomDateEditor(dateFormat, false));
		binder.registerCustomEditor(Date.class, "la2dateOfBirth", new CustomDateEditor(dateFormat, false));
	}

	@RequestMapping(value = "/easylife", method = RequestMethod.GET)
	public ModelAndView easylifeForm(Principal principal) {
		ModelAndView model = new ModelAndView();
		model.addObject("easylifeList", quoteService.viewQuote("BTR3", principal.getName()));
		model.addObject("pageTitle", "Easy Life");
		model.setViewName("quote/easylife");
		return model;
	}

	@RequestMapping(value = "/pruquote_easylife_repkhmer/{quoteId}", method = RequestMethod.GET)
	public ModelAndView easyLifePreviewKhmer(@PathVariable("quoteId") Long quoteId, Principal principal,
			HttpServletRequest req) {
		ModelAndView model = new ModelAndView();
		List<listPruquote> listPruquoteParms = listQuoteService.listQuote(principal.getName(), Long.toString(quoteId));
		List<PruquoteParm> pruquoteParms = quoteService.findValidQuote(quoteId, "BTR3", principal.getName());
		if (listPruquoteParms.size() <= 0 & pruquoteParms.size() <= 0) {
			throw new ResourceNotFoundException();
		} else if (listPruquoteParms.size() >= 0) {
			pruquoteParms = quoteService.findValidQuote(quoteId);
			// model.addObject("name","");
			// model.addObject("code","");
		} else {
			if (pruquoteParms.size() <= 0) {
				throw new ResourceNotFoundException();
			} else {
				// User user=securityService.userFindOne(principal.getName());
				// model.addObject("name",user.getFullNameKH());
				// model.addObject("code",user.getUserId());
			}
		}

		List<String> results = quoteService.generateEasyLifeKhmerQuote(quoteId, "Khmer", "RN");

		model.addObject("header1", results.get(0));
		model.addObject("body1", results.get(1));
		model.addObject("body2", results.get(2));
		model.addObject("body3", results.get(3));
		model.addObject("serialno", results.get(4));

		User user = securityService.findUserByUsername(pruquoteParms.get(0).getCoUser());
		model.addObject("name", user.getFullNameKh());
		model.addObject("code", user.getUserId());

		model.addObject("version", GlobalVar.version);

		model.addObject("quoteOcc", occupationService.getQuoteOccupationByQuoteId(quoteId));
		Occupation occ = occupationService.getOccupationByEngName("Others");
		if (occ == null) {
			model.addObject("occOtherId", 0);
		} else {
			model.addObject("occOtherId", occ.getId());
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object onbehalf = attr.getRequest().getSession().getAttribute("isonbehalf");
		model.addObject("isonbehalf", onbehalf);
		attr.getRequest().getSession().removeAttribute("isonbehalf");

		if (onbehalf.equals("15")) {
			String action = "Quote Id = " + quoteId + "and On be half = " + "Yes";
			String ip = req.getRemoteHost();
			String pip = req.getRemoteHost();
			Date date = new Date();
			logService.logUserAction(user.getUserId(), action, ip, pip, date);
		}

		model.setViewName("report/PruQuote_EasyLife_RepKhmer");
		return model;
	}

	@RequestMapping(value = "/pruquote_easylife_replatin/{quoteId}", method = RequestMethod.GET)
	public ModelAndView easyLifePreviewLatin(@PathVariable("quoteId") Long quoteId, Principal principal,
			HttpServletRequest req) {
		ModelAndView model = new ModelAndView();
		List<listPruquote> listPruquoteParms = listQuoteService.listQuote(principal.getName(), Long.toString(quoteId));
		List<PruquoteParm> pruquoteParms = quoteService.findValidQuote(quoteId, "BTR3", principal.getName());
		if (listPruquoteParms.size() <= 0 & pruquoteParms.size() <= 0) {
			throw new ResourceNotFoundException();
		} else if (listPruquoteParms.size() >= 0) {
			pruquoteParms = quoteService.findValidQuote(quoteId);
			// model.addObject("name","");
			// model.addObject("code","");
		} else {
			if (pruquoteParms.size() <= 0) {
				throw new ResourceNotFoundException();
			} else {
				// User user=securityService.userFindOne(principal.getName());
				// model.addObject("name",user.getFullNameKH());
				// model.addObject("code",user.getUserId());
			}
		}

		List<String> results = quoteService.generateEasyLifeLatinQuote(quoteId, "Latin", "RN");

		model.addObject("header1", results.get(0));
		model.addObject("body1", results.get(1));
		model.addObject("body2", results.get(2));
		model.addObject("body3", results.get(3));
		model.addObject("serialno1", results.get(4));
		model.addObject("serialno2", results.get(5));

		User user = securityService.findUserByUsername(pruquoteParms.get(0).getCoUser());
		model.addObject("name", user.getFullNameKh());
		model.addObject("code", user.getUserId());
		
		model.addObject("version", GlobalVar.version);

		model.addObject("quoteOcc", occupationService.getQuoteOccupationByQuoteId(quoteId));
		Occupation occ = occupationService.getOccupationByEngName("Others");
		if (occ == null) {
			model.addObject("occOtherId", 0);
		} else {
			model.addObject("occOtherId", occ.getId());
		}

		model.setViewName("report/PruQuote_EasyLife_RepLatin");

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object onbehalf = attr.getRequest().getSession().getAttribute("isonbehalf");
		model.addObject("isonbehalf", onbehalf);
		attr.getRequest().getSession().removeAttribute("isonbehalf");

		if (onbehalf.equals("15")) {
			String action = "Quote Id = " + quoteId + "and On be half = " + "Yes";
			String ip = req.getRemoteHost();
			String pip = req.getRemoteHost();
			Date date = new Date();
			logService.logUserAction(user.getUserId(), action, ip, pip, date);
		}

		return model;
	}

	@RequestMapping(value = "/neweasylife", method = RequestMethod.GET)
	public String neweasylifeForm(Model model) {
		model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
		model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR3%"));
		model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR3%"));
		model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
		model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long) 9));
		model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
		model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
		model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
		model.addAttribute("OccupationDdl", occupationService.getAllOccupationDdl());
		model.addAttribute("pageTitle", "New Easy Life");
		return "quote/neweasylife";
	}

	@RequestMapping(value = "/neweasylife", method = RequestMethod.POST, params = "save")
	public String processNeweasylifeForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest req) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			return neweasylifeForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR3");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		if (!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		return "redirect:/easylife";
	}

	@RequestMapping(value = "/neweasylife", method = RequestMethod.POST, params = "previewkh")
	public String processNeweasylifeFormPreviewKh(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest request,
			RedirectAttributes redirectAttributes, HttpServletRequest req) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			return neweasylifeForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR3");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		if (!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl",
				"/pruquote_easylife_repkhmer/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview EasyLIfe Khmer Quote No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/easylife/" + pruquoteparm.getId() + "/edit";
	}

	//
	@RequestMapping(value = "/neweasylife", method = RequestMethod.POST, params = "previewen")
	public String processNeweasylifeFormPreviewEn(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest request,
			RedirectAttributes redirectAttributes, HttpServletRequest req) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			return neweasylifeForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR3");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		if (!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl",
				"/pruquote_easylife_replatin/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview EasyLIfe English Quote No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/easylife/" + pruquoteparm.getId() + "/edit";

	}

	//
	@RequestMapping(value = "/easylife/{id}/edit", method = RequestMethod.GET)
	public String editPruMyFamily(@PathVariable("id") Long id, Model model) {
		PruquoteParm pruquoteParm = quoteService.getValidQuote(id, "BTR3",
				SecurityContextHolder.getContext().getAuthentication().getName());
		if (pruquoteParm == null)
			throw new ResourceNotFoundException();
		model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
		model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR3%"));
		model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR3%"));
		model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
		model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long) 9));
		model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
		model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
		model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
		model.addAttribute("occupations", occupationService.getAllOccupationDdl());
		model.addAttribute("pruquoteparm", pruquoteParm);
		model.addAttribute("SA", pruquoteParm.getBasicPlanSa());
		model.addAttribute("pageTitle", "Edit Easy Life");

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object quoteUrl = attr.getRequest().getSession().getAttribute("previewurl");
		if (quoteUrl != null) {
			model.addAttribute("quoteUrl", quoteUrl.toString());
		}
		attr.getRequest().getSession().removeAttribute("previewurl");

		List<QuoteOccupation> qcs = occupationService.getQuoteOccupationByQuoteId(pruquoteParm.getId());
		for (QuoteOccupation qc : qcs) {
			String tmpRemark = qc.getRemark();
			if (tmpRemark.equals("occla1")) {
				model.addAttribute("occla1selected", qc.getOccId());
			}
		}

		return "quote/editeasylife";
	}

	@RequestMapping(value = "/easylife/{id}/edit", method = RequestMethod.POST, params = "save")
	public String processEditprumyfamilyForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest req) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR3%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR3%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long) 9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
			return "quote/editeasylife";
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR3");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");

		if (!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		return "redirect:/easylife";
	}

	@RequestMapping(value = "/easylife/{id}/edit", method = RequestMethod.POST, params = "previewkh")
	public String processEditeasylifeFormKh(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR3%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR3%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long) 9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
			return "quote/editeasylife";
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR3");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");

		if (!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		// model.addAttribute("previewurl",
		// "/pruquote_myfamily_repkhmer/"+pruquoteparm.getId());
		// return editPruMyFamily(pruquoteparm.getId(),model);

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl",
				"/pruquote_easylife_repkhmer/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview EasyLife Khmer Quote No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/easylife/" + pruquoteparm.getId() + "/edit";

	}

	@RequestMapping(value = "/easylife/{id}/edit", method = RequestMethod.POST, params = "previewen")
	public String processEditeasylifeFormEn(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR3%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR3%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long) 9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
			return "quote/editeasylife";
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR3");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");

		if (!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl",
				"/pruquote_easylife_replatin/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview EasyLife English Quote No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/easylife/" + pruquoteparm.getId() + "/edit";
	}

	@RequestMapping(value = "/neweasylife", method = RequestMethod.POST, params = "easylifeapp")
	public String processNeweasylifeFormPreviewForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			return neweasylifeForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR3");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);
		String occla1 = request.getParameter("occla1");
		if (!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl",
				"/pruquote_easylife_repkhmer/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", request.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview EasyLIfe Khmer Quote No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/easylife/" + pruquoteparm.getId() + "/applicationform";
	}

	@RequestMapping(value = "/neweasylifeblankappform", method = RequestMethod.GET)
	public String processNeweasylifeBlankFormPreviewForm(Model model, Principal principal) {
		quoteService.IDU_easylife_form("0", "0", "1900-01-01", "0", principal.getName(), "0", "1900-01-01", "0", "0",
				"0", "0", "1900-01-01", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0",
				"0", "0", "0", "0", "0", "0", "1900-01-01", "0", "0", "0", "0", "0", "1900-01-01", "0", "0", "0", "0",
				"0", "1900-01-01", "0", "0", "0", "0", "0", "1900-01-01", "0", "0", "0", "0", "0", "0", "B",
				principal.getName());
		return "redirect:/easylife/blankapplicationform";
	}

	@RequestMapping(value = "/easylife/{id}/edit", method = RequestMethod.POST, params = "easylifeapp")
	public String processEditeasylifeFormPreviewForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR3%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR3%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long) 9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
			return "quote/editeasylife";
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR3");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);
		String occla1 = request.getParameter("occla1");

		if (!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl",
				"/pruquote_easylife_repkhmer/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", request.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview EasyLIfe Khmer Quote No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/easylife/" + pruquoteparm.getId() + "/applicationform";
	}

	@RequestMapping(value = "/easylife/{id}/applicationform", method = RequestMethod.GET)
	public String easylifeappform(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR3%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR3%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long) 9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
			return "quote/editeasylife";
		}
		model.addAttribute("lstEasyLife", quoteService.listEasyLife(principal.getName(), id));
		model.addAttribute("lstEasyLifeForm", quoteService.listEasyLifeForm(principal.getName(), id));

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object quoteUrl = attr.getRequest().getSession().getAttribute("previewurl");
		if (quoteUrl != null) {
			model.addAttribute("quoteUrl", quoteUrl.toString());
		}
		attr.getRequest().getSession().removeAttribute("previewurl");

		return "quote/neweasylifeappform";
	}

	@RequestMapping(value = "/pruquote_easylife_appform_repkhmer/{quoteId}", method = RequestMethod.GET)
	public String easylifeappformPreviewKhmer(@PathVariable("quoteId") Long quoteId, Model model, Principal principal) {
		model.addAttribute("lstEasyLife", quoteService.listEasyLife(principal.getName(), quoteId));
		model.addAttribute("lstEasyLifeForm", quoteService.listEasyLifeForm(principal.getName(), quoteId));
		return "report/PruQuote_EasyLife_AppForm_RepKhmer";
	}

	@RequestMapping(value = "/easylife/blankapplicationform", method = RequestMethod.GET)
	public String easylifeappblankformPreviewKhmer(Model model, Principal principal) {
		model.addAttribute("lstEasyLifeBlankForm", quoteService.listEasyLifeFormMaxApp(principal.getName(), ""));
		return "report/PruQuote_EasyLife_BlankAppForm_RepKhmer";
	}

	@RequestMapping(value = "/easylife/{quoteId}/applicationform", method = RequestMethod.POST)
	public String easylifeappformPreviewKhmer(@PathVariable("quoteId") String quoteId, Model model, Principal principal,
			HttpServletRequest request) throws ParseException {

		String txtappnum = "";

		String txtoccdate = request.getParameter("txtoccdate");
		if (txtoccdate != "") {
			Date txtoccdate1 = new SimpleDateFormat("dd/MM/yyyy").parse(txtoccdate);
			txtoccdate = new SimpleDateFormat("yyyy/MM/dd").format(txtoccdate1);
		} else {
			txtoccdate = "1900/01/01";
		}

		String txtagntname = request.getParameter("txtagntname");
		String txtchdrnum = request.getParameter("txtchdrnum");
		String txtagntnum = request.getParameter("txtagntnum");

		String txttrndate = request.getParameter("txttrndate");
		if (txttrndate != "") {
			Date txttrndate1 = new SimpleDateFormat("dd/MM/yyyy").parse(txttrndate);
			txttrndate = new SimpleDateFormat("yyyy/MM/dd").format(txttrndate1);
		} else {
			txttrndate = "1900/01/01";
		}

		String txtagntphone = request.getParameter("txtagntphone");
		String txtponamekh = request.getParameter("txtponamekh");
		String txtponameen = request.getParameter("txtponameen");
		String txtponid = request.getParameter("txtponid");

		String txtpodob = request.getParameter("txtpodob");
		if (txtpodob != "") {
			Date txtpodob1 = new SimpleDateFormat("dd/MM/yyyy").parse(txtpodob);
			txtpodob = new SimpleDateFormat("yyyy/MM/dd").format(txtpodob1);
		} else {
			txtpodob = "1900/01/01";
		}

		String txtpopob = request.getParameter("txtpopob");
		String txtponat = request.getParameter("txtponat");
		String txtpostatus = request.getParameter("txtpostatus");
		String txtpoocc = request.getParameter("txtpoocc");
		String txtpocurocc = request.getParameter("txtpocurocc");
		String txtposal = request.getParameter("txtposal");
		String txtaddr = request.getParameter("txtaddr");
		String txtpophone = request.getParameter("txtpophone");
		String txtpoemail = request.getParameter("txtpoemail");
		String txtcontry = request.getParameter("txtcontry");
		String txtcontryphone = request.getParameter("txtcontryphone");
		String txtpoheight = request.getParameter("txtpoheight");
		String txtpoweight = request.getParameter("txtpoweight");
		String txtpocig = request.getParameter("txtpocig");
		String txttrq1 = request.getParameter("txttrq1");
		String txttrq2 = request.getParameter("txttrq2");
		String txttrq3 = request.getParameter("txttrq3");
		String txttrq4 = request.getParameter("txttrq4");
		String txttrq51 = request.getParameter("txttrq51");
		String txttrq52 = request.getParameter("txttrq52");
		String txttrq6 = request.getParameter("txttrq6");

		String txtbenname1 = request.getParameter("txtbenname1");
		String txtbennamelatin1 = request.getParameter("txtbennamelatin1");
		String txtbensex1 = request.getParameter("txtbensex1");
		if (txtbensex1 == null) {
			txtbensex1 = "";
		}
		String txtbendob1 = request.getParameter("txtbendob1");
		if (txtbendob1 != "" && txtbendob1 != null) {
			Date txtbendob11 = new SimpleDateFormat("dd/MM/yyyy").parse(txtbendob1);
			txtbendob1 = new SimpleDateFormat("yyyy/MM/dd").format(txtbendob11);
		} else {
			txtbendob1 = "1900/01/01";
		}
		String txtbencon1 = request.getParameter("txtbencon1");
		String txtbenper1 = request.getParameter("txtbenper1");

		String txtbenname2 = request.getParameter("txtbenname2");
		String txtbennamelatin2 = request.getParameter("txtbennamelatin2");
		String txtbensex2 = request.getParameter("txtbensex2");
		if (txtbensex2 == null) {
			txtbensex2 = "";
		}
		String txtbendob2 = request.getParameter("txtbendob2");
		if (txtbendob2 != "" && txtbendob2 != null) {
			Date txtbendob21 = new SimpleDateFormat("dd/MM/yyyy").parse(txtbendob2);
			txtbendob2 = new SimpleDateFormat("yyyy/MM/dd").format(txtbendob21);
		} else {
			txtbendob2 = "1900/01/01";
		}
		String txtbencon2 = request.getParameter("txtbencon2");
		String txtbenper2 = request.getParameter("txtbenper2");

		String txtbenname3 = request.getParameter("txtbenname3");
		String txtbennamelatin3 = request.getParameter("txtbennamelatin3");
		String txtbensex3 = request.getParameter("txtbensex3");
		if (txtbensex3 == null) {
			txtbensex3 = "";
		}
		String txtbendob3 = request.getParameter("txtbendob3");
		if (txtbendob3 != "" && txtbendob3 != null) {
			Date txtbendob31 = new SimpleDateFormat("dd/MM/yyyy").parse(txtbendob3);
			txtbendob3 = new SimpleDateFormat("yyyy/MM/dd").format(txtbendob31);
		} else {
			txtbendob3 = "1900/01/01";
		}
		String txtbencon3 = request.getParameter("txtbencon3");
		String txtbenper3 = request.getParameter("txtbenper3");

		String txtbenname4 = request.getParameter("txtbenname4");
		String txtbennamelatin4 = request.getParameter("txtbennamelatin4");
		String txtbensex4 = request.getParameter("txtbensex4");
		if (txtbensex4 == null) {
			txtbensex4 = "";
		}
		String txtbendob4 = request.getParameter("txtbendob4");
		if (txtbendob4 != "" && txtbendob4 != null) {
			Date txtbendob41 = new SimpleDateFormat("dd/MM/yyyy").parse(txtbendob4);
			txtbendob4 = new SimpleDateFormat("yyyy/MM/dd").format(txtbendob41);
		} else {
			txtbendob4 = "1900/01/01";
		}
		String txtbencon4 = request.getParameter("txtbencon4");
		String txtbenper4 = request.getParameter("txtbenper4");
		String fatca = request.getParameter("fatca");
		String txtnote = request.getParameter("txtnote");
		quoteService.IDU_easylife_form(quoteId, txtappnum, txtoccdate, txtagntname, txtchdrnum, txtagntnum, txttrndate,
				txtagntphone, txtponamekh, txtponameen, txtponid, txtpodob, txtpopob, txtponat, txtpostatus, txtpoocc,
				txtpocurocc, txtposal, txtaddr, txtpophone, txtpoemail, txtpoheight, txtpoweight, txtpocig, txttrq1,
				txttrq2, txttrq3, txttrq4, txttrq51, txttrq52, txttrq6, txtbenname1, txtbennamelatin1, txtbensex1,
				txtbendob1, txtbencon1, txtbenper1, txtbenname2, txtbennamelatin2, txtbensex2, txtbendob2, txtbencon2,
				txtbenper2, txtbenname3, txtbennamelatin3, txtbensex3, txtbendob3, txtbencon3, txtbenper3, txtbenname4,
				txtbennamelatin4, txtbensex4, txtbendob4, txtbencon4, txtbenper4, fatca, txtcontry, txtcontryphone,
				txtnote, "I", principal.getName());
		return "redirect:/pruquote_easylife_appform_repkhmer/" + quoteId;
	}
//here to start with dd form
	
	private String processNewHelper(PruquoteParm pruquoteparm, BindingResult result, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req, Boolean isDD) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			return neweasylifeForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR3");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		if (!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/pruquote_dd_form/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		attr.getRequest().getSession().setAttribute("isDD", isDD);
		securityService.saveLogWithInfo(request, "Preview EasyLIfe DD Form No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/easylife/" + pruquoteparm.getId() + "/edit";
	}

	@RequestMapping(value = "/neweasylife", method = RequestMethod.POST, params = "previewDD")
	public String processNeweasylifeDDFormPreview(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest request,
			RedirectAttributes redirectAttributes, HttpServletRequest req) {
		
		return processNewHelper(pruquoteparm, result, model, principal, request, redirectAttributes, req, true);
	}

	@RequestMapping(value = "/neweasylife", method = RequestMethod.POST, params = "previewSO")
	public String processNeweasylifeSOFormPreview(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest request,
			RedirectAttributes redirectAttributes, HttpServletRequest req) {

		return processNewHelper(pruquoteparm, result, model, principal, request, redirectAttributes, req, false);
	}

	@RequestMapping(value = "/pruquote_dd_form/{quoteId}", method = RequestMethod.GET)
	public ModelAndView easyLifePreviewDDForm(@PathVariable("quoteId") Long quoteId, Principal principal,
			HttpServletRequest req) throws ParseException {
		ModelAndView model = new ModelAndView();
		
		Map<String, Object> generateDD = null;

		try {
			generateDD = ddService.generateDD(quoteId, "BTR3");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (generateDD == null)
			throw new ResourceNotFoundException();

		for (String key : generateDD.keySet()) {
			model.addObject(key, generateDD.get(key));
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

		Object isDD = attr.getRequest().getSession().getAttribute("isDD");
		model.addObject("isDD", isDD);
		
		model.addObject("version", GlobalVar.version);

		Object onbehalf = attr.getRequest().getSession().getAttribute("isonbehalf");
		model.addObject("isonbehalf", onbehalf);
		attr.getRequest().getSession().removeAttribute("isonbehalf");
		attr.getRequest().getSession().removeAttribute("isDD");

		// if (onbehalf.equals("15")) {
		// String action = "Print DD Form on Quote Id = " + quoteId + "and On be half =
		// " + "Yes";
		// String ip = req.getRemoteHost();
		// String pip = req.getRemoteHost();
		// Date date = new Date();
		// logService.logUserAction(user.getUserId(), action, ip, pip, date);
		// }

		model.setViewName("report/PruQuote_DD_Form");
		return model;
	}

	private String processEditHelper(PruquoteParm pruquoteparm, BindingResult result, Long id, Model model,
			Principal principal, HttpServletRequest request, RedirectAttributes redirectAttributes,
			HttpServletRequest req, Boolean isDD) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR3%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR3%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtable((long) 9));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
			return "quote/editeasylife";
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR3");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		
		
		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		
		if (!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/pruquote_dd_form/" + pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		attr.getRequest().getSession().setAttribute("isDD", isDD);
		
		securityService.saveLogWithInfo(request, "Preview EasyLife Khmer Quote No " + pruquoteparm.getId(),
				principal.getName());
		return "redirect:/easylife/" + pruquoteparm.getId() + "/edit";
	}

	@RequestMapping(value = "/easylife/{id}/edit", method = RequestMethod.POST, params = "previewDD")
	public String processEditeasylifePreviewDDForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req) {
		return processEditHelper(pruquoteparm, result, id, model, principal, request, redirectAttributes, req, true);
	}
	
	@RequestMapping(value = "/easylife/{id}/edit", method = RequestMethod.POST, params = "previewSO")
	public String processEditeasylifePreviewSOForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req) {
		return processEditHelper(pruquoteparm, result, id, model, principal, request, redirectAttributes, req, false);
	}
}