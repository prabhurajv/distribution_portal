package com.pru.pruquote.controller;

import java.math.BigDecimal;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.extras.springsecurity4.auth.Authorization;

import com.pru.pruquote.model.Itemitem;
import com.pru.pruquote.model.Occupation;
import com.pru.pruquote.model.PruquoteParm;
import com.pru.pruquote.model.QuoteOccupation;
import com.pru.pruquote.model.User;
import com.pru.pruquote.model.listPruquote;
import com.pru.pruquote.service.DDService;
import com.pru.pruquote.service.ListQuoteService;
import com.pru.pruquote.service.LogService;
import com.pru.pruquote.service.OccupationService;
import com.pru.pruquote.service.QuoteService;
import com.pru.pruquote.service.SecurityService;
import com.pru.pruquote.service.TableSetupService;
import com.pru.pruquote.utility.AnonymousHelper;
import com.pru.pruquote.utility.GlobalVar;

@Controller
@PreAuthorize("hasAuthority('ACCESS_PRUCARE')")
public class PRUcareController {

	@Autowired
	private QuoteService quoteService;

	@Autowired
	private TableSetupService tableSetupService;

	@Autowired
	private OccupationService occupationService;

	@Autowired
	private SecurityService securityService;

	@Autowired
	private LogService logService;

	@Autowired
	private ListQuoteService listQuoteService;

	@Autowired
	private DDService ddService;

	@ModelAttribute("pruquoteparm")
	public PruquoteParm pruquoteparmConstructor() {
		return new PruquoteParm();
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, "commDate", new CustomDateEditor(dateFormat, false));
		binder.registerCustomEditor(Date.class, "la1dateOfBirth", new CustomDateEditor(dateFormat, false));
		binder.registerCustomEditor(Date.class, "syndate", new CustomDateEditor(dateFormat, false));
		binder.registerCustomEditor(Date.class, "la2dateOfBirth", new CustomDateEditor(dateFormat, false));
	}

	@RequestMapping(value = "/prucare", method = RequestMethod.GET)
	public ModelAndView prucareForm(Principal principal) {
		ModelAndView model = new ModelAndView();
		model.addObject("prucareList", quoteService.viewQuote("BTR5", principal.getName()));
		model.addObject("pageTitle", "PRUcare");
		model.setViewName("quote/prucare");
		return model;
	}

	@RequestMapping(value = "/newprucare", method = RequestMethod.GET)
	public String newprucareForm(Model model) {
		model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
		model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR5%"));
		model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR5%"));
		model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
		model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtablePro((long) 9, "%BTR5%").stream()
				.sorted(Comparator.comparing(Itemitem::getItemLatin)).collect(Collectors.toList())); // order by
																										// Annually 01

		model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
		model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
		model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
		model.addAttribute("OccupationDdl", occupationService.getAllOccupationDdl());
		model.addAttribute("pageTitle", "New PureProtect");
		return "quote/newprucare";
	}

	@RequestMapping(value = "/newprucare", method = RequestMethod.POST, params = "save")
	public String processNewprucareForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest req) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			return newprucareForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR5");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());

		setRiderBasedOnPermission(pruquoteparm);

		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");
		if (!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occpo.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo), "occpo", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (occla2 != null && !occla2.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2), "occla2", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		return "redirect:/prucare";
	}

	@RequestMapping(value = "/prucare/{id}/edit", method = RequestMethod.GET)
	public String editPRUcare(@PathVariable("id") Long id, Model model) {
		PruquoteParm pruquoteParm = quoteService.getValidQuote(id, "BTR5",
				SecurityContextHolder.getContext().getAuthentication().getName());
		if (pruquoteParm == null)
			throw new ResourceNotFoundException();
		model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
		model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR5%"));
		model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR5%"));
		model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
		model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtablePro((long) 9, "%BTR5%").stream()
				.sorted(Comparator.comparing(Itemitem::getItemLatin)).collect(Collectors.toList())); // order by
																										// Annually 01
		model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
		model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
		model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
		model.addAttribute("occupations", occupationService.getAllOccupationDdl());
		model.addAttribute("pruquoteparm", pruquoteParm);
		model.addAttribute("pageTitle", "Edit PureProtect");

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object quoteUrl = attr.getRequest().getSession().getAttribute("previewurl");
		if (quoteUrl != null) {
			model.addAttribute("quoteUrl", quoteUrl.toString());
		}
		attr.getRequest().getSession().removeAttribute("previewurl");

		// PruquoteParm pruquotepar = pruquoteParm;
		// List<Occupation> occs = pruquotepar.getOccupations();
		// for ( Occupation occ : occs ) {
		// int occid = occ.getId();
		// QuoteOccupation quoteOcc =
		// occupationService.getQuoteOccupationByQuoteandOccId(pruquoteParm.getId(),
		// occid);
		// String tmpRemark = quoteOcc.getRemark();
		// if (tmpRemark.equals("occla1")) {
		// model.addAttribute("occla1selected",occid);
		// }
		// if (tmpRemark.equals("occpo")) {
		// model.addAttribute("occposelected",occid);
		// }
		// if (tmpRemark.equals("occla2")) {
		// model.addAttribute("occla2selected",occid);
		// }
		// }

		List<QuoteOccupation> qcs = occupationService.getQuoteOccupationByQuoteId(pruquoteParm.getId());
		for (QuoteOccupation qc : qcs) {
			String tmpRemark = qc.getRemark();
			if (tmpRemark.equals("occla1")) {
				model.addAttribute("occla1selected", qc.getOccId());
			}
			if (tmpRemark.equals("occpo")) {
				model.addAttribute("occposelected", qc.getOccId());
			}
			if (tmpRemark.equals("occla2")) {
				model.addAttribute("occla2selected", qc.getOccId());
			}
		}

		return "quote/editprucare";
	}

	// assign optional field that using by view to generate quote incase no rider
	private void setRiderBasedOnPermission(PruquoteParm pruquoteparm) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (!authentication.getAuthorities().stream().anyMatch(r -> r.getAuthority().equals("ACCESS_PRUCARE_FIB"))) {
			pruquoteparm.setCoL1Rsr1(16);
			pruquoteparm.setCoL2Rsr1(16);
		}

		if (!authentication.getAuthorities().stream().anyMatch(r -> r.getAuthority().equals("ACCESS_PRUCARE_PCB"))) {
			pruquoteparm.setCoL2Rtr1(16);
			pruquoteparm.setCoL1Rtr1(16);
			pruquoteparm.setCoL1Rtr2(16);
		}

		if (!authentication.getAuthorities().stream().anyMatch(r -> r.getAuthority().equals("ACCESS_PRUCARE_FIB"))
				&& !authentication.getAuthorities().stream()
						.anyMatch(r -> r.getAuthority().equals("ACCESS_PRUCARE_PCB"))) {
			pruquoteparm.setCoLa2sex(1);
		}
	}

	@RequestMapping(value = "/prucare/{id}/edit", method = RequestMethod.POST, params = "save")
	public String processEditprucareForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest req) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR5%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR5%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtablePro((long) 9, "%BTR5%"));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
			return "quote/editprucare";
		}
		PruquoteParm pruquoteParm = quoteService.getValidQuote(pruquoteparm.getId(), "BTR5",
				SecurityContextHolder.getContext().getAuthentication().getName());

		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR5");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		pruquoteparm.setOccupations(pruquoteParm.getOccupations());

		setRiderBasedOnPermission(pruquoteparm);

		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");

		if (!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occpo.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occpo");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo), "occpo", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occpo));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (occla2 != null && !occla2.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla2");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2), "occla2", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla2));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		return "redirect:/prucare";
	}

	private String processEditprucareHelper(PruquoteParm pruquoteparm, BindingResult result, Long id, Model model,
			Principal principal, HttpServletRequest request, RedirectAttributes redirectAttributes,
			HttpServletRequest req, String lang, String previewType, Boolean isDD) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long) 1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 3, "%BTR5%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long) 4, "%BTR5%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long) 8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtablePro((long) 9, "%BTR5%"));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long) 5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long) 2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long) 7));
			return "quote/editedusave";
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR5");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());

		setRiderBasedOnPermission(pruquoteparm);

		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");

		if (!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occpo.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occpo");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo), "occpo", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occpo));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (occla2 != null && !occla2.equals("")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(),
					"occla2");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2), "occla2", 1,
						principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla2));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		// return editPRUcare(id,model);
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		if (previewType == "DD") {
			attr.getRequest().getSession().setAttribute("previewurl",
					"/prucare_pruquote_dd_form/" + pruquoteparm.getId());
			attr.getRequest().getSession().setAttribute("isDD", isDD);
		} else if (previewType == "Quote") {
			if (lang.equals("kh")) {
				attr.getRequest().getSession().setAttribute("previewurl",
						"/pruquote_prucare_repkhmer/" + pruquoteparm.getId());
			} else if (lang.equals("en")) {
				attr.getRequest().getSession().setAttribute("previewurl",
						"/pruquote_prucare_replatin/" + pruquoteparm.getId());
			}
			securityService.saveLogWithInfo(request, "Preview PRUcare Khmer Quote No " + pruquoteparm.getId(),
					principal.getName());
		}
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));

		return "redirect:/prucare/" + pruquoteparm.getId() + "/edit";
	}

	@RequestMapping(value = "/prucare/{id}/edit", method = RequestMethod.POST, params = "previewkh")
	public String processEditprucareKh(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req) {

		return processEditprucareHelper(pruquoteparm, result, id, model, principal, request, redirectAttributes, req,
				"kh", "Quote", false);
	}

	@RequestMapping(value = "/prucare/{id}/edit", method = RequestMethod.POST, params = "previewen")
	public String processEditprucareEn(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req) {

		return processEditprucareHelper(pruquoteparm, result, id, model, principal, request, redirectAttributes, req,
				"en", "Quote", false);
	}

	public String processNewprucareFormPreviewHelper(PruquoteParm pruquoteparm, BindingResult result, Model model,
			Principal principal, HttpServletRequest request, RedirectAttributes redirectAttributes,
			HttpServletRequest req, String lang, String previewType, Boolean isDD) {
		if (result.hasErrors()) {
			model.addAttribute("errors", result.getAllErrors());
			return newprucareForm(model);
		}
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setProduct("BTR5");
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());

		setRiderBasedOnPermission(pruquoteparm);

		quoteService.pruQuoteParamSave(pruquoteparm);

		String occla1 = req.getParameter("occla1");
		String occpo = req.getParameter("occpo");
		String occla2 = req.getParameter("occla2");
		if (!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (!occpo.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occpo), "occpo", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		if (occla2 != null && !occla2.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla2), "occla2", 1,
					principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		if (previewType == "DD") {
			attr.getRequest().getSession().setAttribute("previewurl",
					"/prucare_pruquote_dd_form/" + pruquoteparm.getId());
			attr.getRequest().getSession().setAttribute("isDD", isDD);
		} else if (previewType == "Quote") {
			if (lang.equals("kh")) {
				attr.getRequest().getSession().setAttribute("previewurl",
						"/pruquote_prucare_repkhmer/" + pruquoteparm.getId());
			} else if (lang.equals("en")) {
				attr.getRequest().getSession().setAttribute("previewurl",
						"/pruquote_prucare_replatin/" + pruquoteparm.getId());
			}
			securityService.saveLogWithInfo(request, "Preview prucare English Quote No " + pruquoteparm.getId(),
					principal.getName());
		}

		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		return "redirect:/prucare/" + pruquoteparm.getId() + "/edit";
	}

	@RequestMapping(value = "/newprucare", method = RequestMethod.POST, params = "previewkh")
	public String processNewprucareKh(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest request,
			RedirectAttributes redirectAttributes, HttpServletRequest req) {

		return processNewprucareFormPreviewHelper(pruquoteparm, result, model, principal, request, redirectAttributes,
				req, "kh", "Quote", false);
	}

	@RequestMapping(value = "/newprucare", method = RequestMethod.POST, params = "previewen")
	public String processNewprucareEn(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest request,
			RedirectAttributes redirectAttributes, HttpServletRequest req) {

		return processNewprucareFormPreviewHelper(pruquoteparm, result, model, principal, request, redirectAttributes,
				req, "en", "Quote", false);
	}

	public ModelAndView prucarePreviewHelper(Long quoteId, Principal principal, HttpServletRequest req, String lan) {
		ModelAndView model = new ModelAndView();

		List<listPruquote> listPruquoteParms = listQuoteService.listQuote(principal.getName(), Long.toString(quoteId));
		User user = securityService.findUserByUsername(principal.getName());

		Map<String, Object> quote = null;
		if (listPruquoteParms.size() >= 0) {
			quote = quoteService.generateQuote(quoteId, lan, user.getFullNameKh(), "BTR5");
			// model.addObject("name","");
			// model.addObject("code","");
		} else {
			throw new ResourceNotFoundException();
		}

		if (quote == null)
			throw new ResourceNotFoundException();

		model.addAllObjects(quote);
		if (lan == "en") {
			model.setViewName("report/PruQuote_Prucare_Replatin");
		} else {
			model.setViewName("report/PruQuote_Prucare_RepKhmer");
		}

		model.addObject("quoteOcc", occupationService.getQuoteOccupationByQuoteId(quoteId));
		Occupation occ = occupationService.getOccupationByEngName("Others");
		if (occ == null) {
			model.addObject("occOtherId", 0);
		} else {
			model.addObject("occOtherId", occ.getId());
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object onbehalf = attr.getRequest().getSession().getAttribute("isonbehalf");
		model.addObject("isonbehalf", onbehalf);
		attr.getRequest().getSession().removeAttribute("isonbehalf");

		if (onbehalf.equals("15")) {
			String action = "Quote Id = " + quoteId + "On be half = " + "Yes";
			String ip = req.getRemoteHost();
			String pip = req.getRemoteHost();
			Date date = new Date();
			logService.logUserAction(user.getUserId(), action, ip, pip, date);
		}

		return model;
	}

	@RequestMapping(value = "/pruquote_prucare_repkhmer/{quoteId}", method = RequestMethod.GET)
	public ModelAndView prucarePreviewKhmer(@PathVariable("quoteId") Long quoteId, Principal principal,
			HttpServletRequest req) {
		return prucarePreviewHelper(quoteId, principal, req, "kh");
	}

	@RequestMapping(value = "/pruquote_prucare_replatin/{quoteId}", method = RequestMethod.GET)
	public ModelAndView prucarePreviewEn(@PathVariable("quoteId") Long quoteId, Principal principal,
			HttpServletRequest req) {
		return prucarePreviewHelper(quoteId, principal, req, "en");
	}

	//DirectDebit function
	@RequestMapping(value = "/newprucare", method = RequestMethod.POST, params = "previewDD")
	public String processNewprucareDDFormPreview(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest request,
			RedirectAttributes redirectAttributes, HttpServletRequest req) {

		return processNewprucareFormPreviewHelper(pruquoteparm, result, model, principal, request, redirectAttributes,
				req, "en", "DD", true);
	}

	@RequestMapping(value = "/newprucare", method = RequestMethod.POST, params = "previewSO")
	public String processNewprucareSOFormPreview(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, Model model, Principal principal, HttpServletRequest request,
			RedirectAttributes redirectAttributes, HttpServletRequest req) {

		return processNewprucareFormPreviewHelper(pruquoteparm, result, model, principal, request, redirectAttributes,
				req, "en", "DD", false);
	}

	@RequestMapping(value = "/prucare_pruquote_dd_form/{quoteId}", method = RequestMethod.GET)
	public ModelAndView PruCarePreviewDDForm(@PathVariable("quoteId") Long quoteId, Principal principal,
			HttpServletRequest req) throws ParseException {
		ModelAndView model = new ModelAndView();

		Map<String, Object> generateDD = null;

		try {
			generateDD = ddService.generateDD(quoteId, "BTR5");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (generateDD == null)
			throw new ResourceNotFoundException();

		for (String key : generateDD.keySet()) {
			model.addObject(key, generateDD.get(key));
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

		Object isDD = attr.getRequest().getSession().getAttribute("isDD");
		model.addObject("isDD", isDD);

		model.addObject("version", GlobalVar.version);

		Object onbehalf = attr.getRequest().getSession().getAttribute("isonbehalf");
		model.addObject("isonbehalf", onbehalf);
		attr.getRequest().getSession().removeAttribute("isonbehalf");
		attr.getRequest().getSession().removeAttribute("isDD");
		model.setViewName("report/PruQuote_DD_Form");
		return model;
	}

	@RequestMapping(value = "/prucare/{id}/edit", method = RequestMethod.POST, params = "previewDD")
	public String processEditprucarePreviewDDForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req) {
		return processEditprucareHelper(pruquoteparm, result, id, model, principal, request, redirectAttributes, req,
				"en", "DD", true);
	}

	@RequestMapping(value = "/prucare/{id}/edit", method = RequestMethod.POST, params = "previewSO")
	public String processEditprucarePreviewSOForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm,
			BindingResult result, @PathVariable("id") Long id, Model model, Principal principal,
			HttpServletRequest request, RedirectAttributes redirectAttributes, HttpServletRequest req) {
		return processEditprucareHelper(pruquoteparm, result, id, model, principal, request, redirectAttributes, req,
				"en", "DD", false);
	}

}
