package com.pru.pruquote.controller;

import java.math.BigDecimal;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

//import dummiesmind.breadcrumb.springmvc.annotations.Link;

//import com.google.gson.Gson;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pru.pruquote.model.AcbProduct;
import com.pru.pruquote.model.Occupation;
import com.pru.pruquote.model.Referral;
import com.pru.pruquote.model.ReferralAcbProduct;
import com.pru.pruquote.model.ReferralFollowUpComment;
import com.pru.pruquote.model.ReferralList;
import com.pru.pruquote.model.Referrer;
import com.pru.pruquote.model.UpsellingAndCrosssellingModel;
import com.pru.pruquote.model.UpsellingAndCrosssellingReferral;
import com.pru.pruquote.service.BranchService;
import com.pru.pruquote.service.LogService;
import com.pru.pruquote.service.OccupationService;
import com.pru.pruquote.service.ReferralService;
import com.pru.pruquote.service.ReferrerService;
import com.pru.pruquote.service.UpsellingAndCrosssellingService;
import com.pru.pruquote.utility.AnonymousHelper;
import com.pru.pruquote.utility.GlobalVar;

@Controller
@PreAuthorize("hasAnyAuthority('ACCESS_REFERRAL,ACCESS_REFERRAL_MGT')")
public class ReferralController {

	@Autowired
	private ReferralService referralService;;

	@Autowired
	private ReferrerService referrerService;

	@Autowired
	private LogService logService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private UpsellingAndCrosssellingService upsellingAndCrosssellingService;

	@Autowired
	private OccupationService occupationService;

	@ModelAttribute("referralparam")
	public Referral pruquoteparmConstructor() {
		return new Referral();
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		binder.registerCustomEditor(Date.class, "rDate", new CustomDateEditor(dateFormat, true));
		binder.registerCustomEditor(Date.class, "sucAppDate", new CustomDateEditor(dateFormat, true));
		binder.registerCustomEditor(Date.class, "clsBankSoDate", new CustomDateEditor(dateFormat, true));
		binder.registerCustomEditor(Date.class, "clsBankUploadDate", new CustomDateEditor(dateFormat, true));
		binder.registerCustomEditor(Date.class, "appDate", new CustomDateEditor(dateFormat, true));
	}

	@RequestMapping(value = "/referral", method = RequestMethod.GET)
	public ModelAndView referral(Principal principal) {
		ModelAndView model = new ModelAndView();
		// model.addObject("listReferral",referralService.listReferral(principal.getName(),""));
		model.addObject("rType", referralService.listAllReferralType("referraltype"));
		model.addObject("listReferral", new ArrayList<Referral>());
		model.addObject("pageTitle", "Referral");
		model.addObject("ReferralList", new ReferralList());
		model.setViewName("referral/referral");
		return model;
	}

	@RequestMapping(value = "/referral/search", method = RequestMethod.POST)
	@ResponseBody
	public String referralSearch(HttpServletRequest request) throws ParseException {
		ObjectMapper mapper = new ObjectMapper();

		String result = "";
		String userid = SecurityContextHolder.getContext().getAuthentication().getName();

		String trndatetimefrom = request.getParameter("trnDateTimeFrom");
		if (!AnonymousHelper.isDateValid(trndatetimefrom))
			trndatetimefrom = "";
		if (trndatetimefrom != "") {
			Date trnDateTimeFrom1 = new SimpleDateFormat("dd/MM/yyyy").parse(trndatetimefrom);
			trndatetimefrom = new SimpleDateFormat("yyyy/MM/dd").format(trnDateTimeFrom1);
		}

		String trndatetimeto = request.getParameter("trnDateTimeTo");
		if (!AnonymousHelper.isDateValid(trndatetimeto))
			trndatetimeto = "";
		if (trndatetimeto != "") {
			Date trnDateTimeTo1 = new SimpleDateFormat("dd/MM/yyyy").parse(trndatetimeto);
			trndatetimeto = new SimpleDateFormat("yyyy/MM/dd").format(trnDateTimeTo1);
		}

		String followupdatetimefrom = request.getParameter("followupDateTimeFrom");
		if (!AnonymousHelper.isDateValid(followupdatetimefrom))
			followupdatetimefrom = "";
		if (followupdatetimefrom != "") {
			Date followupDateTimeFrom1 = new SimpleDateFormat("dd/MM/yyyy").parse(followupdatetimefrom);
			followupdatetimefrom = new SimpleDateFormat("yyyy/MM/dd").format(followupDateTimeFrom1);
		}

		String followupdatetimeto = request.getParameter("followupDateTimeTo");
		if (!AnonymousHelper.isDateValid(followupdatetimeto))
			followupdatetimeto = "";
		if (followupdatetimeto != "") {
			Date followupDateTimeTo1 = new SimpleDateFormat("dd/MM/yyyy").parse(followupdatetimeto);
			followupdatetimeto = new SimpleDateFormat("yyyy/MM/dd").format(followupDateTimeTo1);
		}

		String cusinfor = request.getParameter("cusInfor");
		if (!AnonymousHelper.isKeywordAllowed(cusinfor, "^[a-zA-Z0-9\\\\-]*$"))
			cusinfor = "";

		String rtype = request.getParameter("rType");
		if (!AnonymousHelper.isNumberOnly(rtype))
			rtype = "";

		String sucappstatus = request.getParameter("sucAppStatus");
		if (!AnonymousHelper.isTextOnly(sucappstatus))
			sucappstatus = "";

		String clsbanksostatus = request.getParameter("clsBankSoStatus");
		if (!AnonymousHelper.isTextOnly(clsbanksostatus))
			clsbanksostatus = "";

		String clsbankuploadstatus = request.getParameter("clsBankUploadStatus");
		if (!AnonymousHelper.isTextOnly(clsbankuploadstatus))
			clsbankuploadstatus = "";

		String exiscusstatus = request.getParameter("exisCusStatus");
		if (!AnonymousHelper.isTextOnly(exiscusstatus))
			exiscusstatus = "";

		String referralby = request.getParameter("referralBy");
		if (!AnonymousHelper.isKeywordAllowed(referralby))
			referralby = "";

		List<ReferralList> referralList = new ArrayList<ReferralList>();

		try {
			referralList = referralService.listReferralByAgent(userid, trndatetimefrom, trndatetimeto,
					followupdatetimefrom, followupdatetimeto, cusinfor, rtype, sucappstatus, clsbanksostatus,
					clsbankuploadstatus, exiscusstatus, referralby);

			result = mapper.writeValueAsString(referralList);

			// Gson gson = new Gson();
			//
			// result = gson.toJson(referralList);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping(value = "/managereferral", method = RequestMethod.GET)
	public ModelAndView manageReferral(Principal principal) {
		ModelAndView model = new ModelAndView();
		// model.addObject("listReferral",referralService.listReferral(principal.getName(),""));
		model.addObject("rType", referralService.listAllReferralType("referraltype"));
		model.addObject("listReferral", new ArrayList<Referral>());
		model.addObject("pageTitle", "Referral");
		model.addObject("ReferralList", new ReferralList());
		// model.addObject("rbdmList", referralService.listHighLevelHK("RBDM"));
		// model.addObject("sbdmList", referralService.listHighLevelHK("SBDM"));
		// model.addObject("bdmList", referralService.listHighLevelHK("BDM"));
		// model.addObject("supList", referralService.listHighLevelHK("Sale
		// Supervisor"));
		// model.addObject("branchList", referralService.listDynamic("SELECT DISTINCT
		// trim(agnt_branch) AS branch, trim(agnt_branch) AS branchDesc FROM " +
		// GlobalVar.db_name + ".tabagnt_fc_hk ORDER BY branch"));
		// model.addObject("agntList", referralService.listDynamic("SELECT trim(agntnum)
		// AS agntnum, trim(agnt_name) AS agntname FROM " +
		// GlobalVar.db_name + ".tabagnt_fc_hk ORDER BY agntname"));
		model.setViewName("referral/manage");
		return model;
	}

	@RequestMapping(value = "/managereferral/search", method = RequestMethod.POST)
	@ResponseBody
	public String referralManageSearch(HttpServletRequest request) throws ParseException {
		ObjectMapper mapper = new ObjectMapper();

		String result = "";
		String userid = SecurityContextHolder.getContext().getAuthentication().getName();

		// String agentHierarchy = request.getParameter("agentHierarchy");
		// if(!AnonymousHelper.isKeywordAllowed(agentHierarchy, "^[a-zA-Z0-9\\\\-]*$"))
		// agentHierarchy = "";

		String trndatetimefrom = request.getParameter("trnDateTimeFrom");
		if (!AnonymousHelper.isDateValid(trndatetimefrom))
			trndatetimefrom = "";
		if (trndatetimefrom != "") {
			Date trnDateTimeFrom1 = new SimpleDateFormat("dd/MM/yyyy").parse(trndatetimefrom);
			trndatetimefrom = new SimpleDateFormat("yyyy/MM/dd").format(trnDateTimeFrom1);
		}

		String trndatetimeto = request.getParameter("trnDateTimeTo");
		if (!AnonymousHelper.isDateValid(trndatetimeto))
			trndatetimeto = "";
		if (trndatetimeto != "") {
			Date trnDateTimeTo1 = new SimpleDateFormat("dd/MM/yyyy").parse(trndatetimeto);
			trndatetimeto = new SimpleDateFormat("yyyy/MM/dd").format(trnDateTimeTo1);
		}

		String followupdatetimefrom = request.getParameter("followupDateTimeFrom");
		if (!AnonymousHelper.isDateValid(followupdatetimefrom))
			followupdatetimefrom = "";
		if (followupdatetimefrom != "") {
			Date followupDateTimeFrom1 = new SimpleDateFormat("dd/MM/yyyy").parse(followupdatetimefrom);
			followupdatetimefrom = new SimpleDateFormat("yyyy/MM/dd").format(followupDateTimeFrom1);
		}

		String followupdatetimeto = request.getParameter("followupDateTimeTo");
		if (!AnonymousHelper.isDateValid(followupdatetimeto))
			followupdatetimeto = "";
		if (followupdatetimeto != "") {
			Date followupDateTimeTo1 = new SimpleDateFormat("dd/MM/yyyy").parse(followupdatetimeto);
			followupdatetimeto = new SimpleDateFormat("yyyy/MM/dd").format(followupDateTimeTo1);
		}

		String cusinfor = request.getParameter("cusInfor");
		if (!AnonymousHelper.isKeywordAllowed(cusinfor, "^[a-zA-Z0-9\\\\-]*$"))
			cusinfor = "";

		String rtype = request.getParameter("rType");
		if (!AnonymousHelper.isNumberOnly(rtype))
			rtype = "";

		String sucappstatus = request.getParameter("sucAppStatus");
		if (!AnonymousHelper.isTextOnly(sucappstatus))
			sucappstatus = "";

		String clsbanksostatus = request.getParameter("clsBankSoStatus");
		if (!AnonymousHelper.isTextOnly(clsbanksostatus))
			clsbanksostatus = "";

		String clsbankuploadstatus = request.getParameter("clsBankUploadStatus");
		if (!AnonymousHelper.isTextOnly(clsbankuploadstatus))
			clsbankuploadstatus = "";

		String exiscusstatus = request.getParameter("exisCusStatus");
		if (!AnonymousHelper.isTextOnly(exiscusstatus))
			exiscusstatus = "";

		String referralby = request.getParameter("referralBy");
		if (!AnonymousHelper.isKeywordAllowed(referralby))
			referralby = "";

		String rbdm = request.getParameter("rbdm");
		if (!AnonymousHelper.isKeywordAllowed(rbdm))
			rbdm = "";

		String sbdm = request.getParameter("sbdm");
		if (!AnonymousHelper.isKeywordAllowed(sbdm))
			sbdm = "";

		String bdm = request.getParameter("bdm");
		if (!AnonymousHelper.isKeywordAllowed(bdm))
			bdm = "";

		String branch = request.getParameter("branch");
		if (!AnonymousHelper.isKeywordAllowed(branch))
			branch = "";

		String sup = request.getParameter("agntsup");
		if (!AnonymousHelper.isKeywordAllowed(sup))
			sup = "";

		String agntNum = request.getParameter("agnt");
		if (!AnonymousHelper.isKeywordAllowed(agntNum))
			agntNum = "";

		List<ReferralList> referralList = new ArrayList<ReferralList>();

		try {
			referralList = referralService.listReferralByAgentMgt(userid, "", trndatetimefrom, trndatetimeto,
					followupdatetimefrom, followupdatetimeto, cusinfor, rtype, sucappstatus, clsbanksostatus,
					clsbankuploadstatus, exiscusstatus, referralby, rbdm, sbdm, bdm, branch, sup, agntNum);

			result = mapper.writeValueAsString(referralList);

			// Gson gson = new Gson();
			//
			// result = gson.toJson(referralList);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping(value = "/managereferral/search/serverside", method = RequestMethod.POST)
	@ResponseBody
	public String referralManageSearchServerSide(HttpServletRequest request) throws ParseException {
		// ObjectMapper mapper = new ObjectMapper();

		String result = "";
		String userid = SecurityContextHolder.getContext().getAuthentication().getName();

		// String agentHierarchy = request.getParameter("agentHierarchy");
		// if(!AnonymousHelper.isKeywordAllowed(agentHierarchy, "^[a-zA-Z0-9\\\\-]*$"))
		// agentHierarchy = "";

		String trndatetimefrom = request.getParameter("trnDateTimeFrom");
		if (!AnonymousHelper.isDateValid(trndatetimefrom))
			trndatetimefrom = "";
		if (trndatetimefrom != "") {
			Date trnDateTimeFrom1 = new SimpleDateFormat("dd/MM/yyyy").parse(trndatetimefrom);
			trndatetimefrom = new SimpleDateFormat("yyyy/MM/dd").format(trnDateTimeFrom1);
		}

		String trndatetimeto = request.getParameter("trnDateTimeTo");
		if (!AnonymousHelper.isDateValid(trndatetimeto))
			trndatetimeto = "";
		if (trndatetimeto != "") {
			Date trnDateTimeTo1 = new SimpleDateFormat("dd/MM/yyyy").parse(trndatetimeto);
			trndatetimeto = new SimpleDateFormat("yyyy/MM/dd").format(trnDateTimeTo1);
		}

		String followupdatetimefrom = request.getParameter("followupDateTimeFrom");
		if (!AnonymousHelper.isDateValid(followupdatetimefrom))
			followupdatetimefrom = "";
		if (followupdatetimefrom != "") {
			Date followupDateTimeFrom1 = new SimpleDateFormat("dd/MM/yyyy").parse(followupdatetimefrom);
			followupdatetimefrom = new SimpleDateFormat("yyyy/MM/dd").format(followupDateTimeFrom1);
		}

		String followupdatetimeto = request.getParameter("followupDateTimeTo");
		if (!AnonymousHelper.isDateValid(followupdatetimeto))
			followupdatetimeto = "";
		if (followupdatetimeto != "") {
			Date followupDateTimeTo1 = new SimpleDateFormat("dd/MM/yyyy").parse(followupdatetimeto);
			followupdatetimeto = new SimpleDateFormat("yyyy/MM/dd").format(followupDateTimeTo1);
		}

		String cusinfor = request.getParameter("cusInfor");
		if (!AnonymousHelper.isKeywordAllowed(cusinfor, "^[a-zA-Z0-9\\\\-]*$"))
			cusinfor = "";

		String rtype = request.getParameter("rType");
		if (!AnonymousHelper.isNumberOnly(rtype))
			rtype = "";

		String sucappstatus = request.getParameter("sucAppStatus");
		if (!AnonymousHelper.isTextOnly(sucappstatus))
			sucappstatus = "";

		String clsbanksostatus = request.getParameter("clsBankSoStatus");
		if (!AnonymousHelper.isTextOnly(clsbanksostatus))
			clsbanksostatus = "";

		String clsbankuploadstatus = request.getParameter("clsBankUploadStatus");
		if (!AnonymousHelper.isTextOnly(clsbankuploadstatus))
			clsbankuploadstatus = "";

		String exiscusstatus = request.getParameter("exisCusStatus");
		if (!AnonymousHelper.isTextOnly(exiscusstatus))
			exiscusstatus = "";

		String referralby = request.getParameter("referralBy");
		if (!AnonymousHelper.isKeywordAllowed(referralby))
			referralby = "";

		String rbdm = request.getParameter("rbdm");
		if (!AnonymousHelper.isKeywordAllowed(rbdm))
			rbdm = "";

		String sbdm = request.getParameter("sbdm");
		if (!AnonymousHelper.isKeywordAllowed(sbdm))
			sbdm = "";

		String bdm = request.getParameter("bdm");
		if (!AnonymousHelper.isKeywordAllowed(bdm))
			bdm = "";

		String branch = request.getParameter("branch");
		if (!AnonymousHelper.isKeywordAllowed(branch))
			branch = "";

		String sup = request.getParameter("agntsup");
		if (!AnonymousHelper.isKeywordAllowed(sup))
			sup = "";

		String agntNum = request.getParameter("agnt");
		if (!AnonymousHelper.isKeywordAllowed(agntNum))
			agntNum = "";

		List<ReferralList> referralList = new ArrayList<ReferralList>();

		try {
			// int sortCol = Integer.valueOf(request.getParameter("order[0][column]"));
			// String sortColDir = request.getParameter("order[0][dir]");
			// String search = request.getParameter("search[value]");
			// String columnsName = request.getParameter("columns[" + sortCol + "][data]");

			referralList = referralService.listReferralByAgentMgt(userid, "", trndatetimefrom, trndatetimeto,
					followupdatetimefrom, followupdatetimeto, cusinfor, rtype, sucappstatus, clsbanksostatus,
					clsbankuploadstatus, exiscusstatus, referralby, rbdm, sbdm, bdm, branch, sup, agntNum);

			result = AnonymousHelper.getServerSideDatasource(request, referralList);
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return result;
	}

	@RequestMapping(value = "/newreferral", method = RequestMethod.GET)
	public String newReferral(Principal principal, Model model) {
		List<AcbProduct> allAcbProduct = referralService.listAcbProduct();
		List<String> nameDescFormat = new ArrayList<>();
		List<String> shortDes = allAcbProduct.stream().map(s -> s.getShortDesc()).collect(Collectors.toList());

		for (String obj : shortDes) {
			String objrepalcestring = obj.replaceAll("\\s+", "");
			nameDescFormat.add("productname-" + objrepalcestring);
			nameDescFormat.add("cusappby-" + objrepalcestring);
			nameDescFormat.add("comment-" + objrepalcestring);
		}
		model.addAttribute("rType", referralService.listAllReferralType("referraltype"));
		model.addAttribute("rReferralCommentType", referralService.listReferralCommentType());
		model.addAttribute("pageTitle", "New Referral");
		// model.addAttribute("referrer",referralService.listCertifiedReferrer(principal.getName()));
		model.addAttribute("occupation", referralService.listCusOccupation());
		model.addAttribute("listProduct", referralService.listProduct());
		model.addAttribute("listPolicyTerm", referralService.listPolicyTerm());
		model.addAttribute("listPolicyMode", referralService.listPolicyMode());
		model.addAttribute("rReferralCommentType", referralService.listReferralCommentType());
		model.addAttribute("customerFromList", referralService.listCustomerFrom());
		model.addAttribute("acbProducts", referralService.listAcbProduct());
		model.addAttribute("acbProductComments", referralService.listAcbProductComment());
		model.addAttribute("referralparam", new Referral());
		model.addAttribute("nameDescFormat", nameDescFormat);
		model.addAttribute("branchs", branchService.listBranchs());

		Object agntBranchCode = branchService.getSingleObject("tabagnt_fc_hk", "agnt_branch", "agntnum",
				principal.getName());
		model.addAttribute("agntBranchCode", agntBranchCode == null ? "" : agntBranchCode.toString().trim());

		return "referral/newreferral";
	}

	private String newReferralHelper(String cusname, String cussex, Principal principal, Model model,
			HttpServletRequest request) throws ParseException {
		String rid = "";
		Date trnDate = new Date();
		String exiscusstatus = request.getParameter("exisCusStatus");
		String rtype = request.getParameter("rType");
		Date rDate = new Date();
		String referralBy = request.getParameter("referralBy").trim();
		String rremark = request.getParameter("rRemark").trim();
		String cusage = request.getParameter("cusAge").trim();
		String cusmstatus = request.getParameter("cusMStatus");
		String cusnochd = request.getParameter("cusNochd");
		if (cusmstatus.equals("Single")) {
			cusnochd = "0";
		}
		String cusphone = request.getParameter("cusPhone").trim();
		String cusocc = request.getParameter("cusOcc");
		String agntnum = principal.getName();
		String[] acbProductsNew = null;
		String acbProducts = request.getParameter("referralAcbProductNew");

		// For Comment
		String fappate = request.getParameter("fappDate");
		String cid = request.getParameter("cid");
		String remark = request.getParameter("remark");

		// Validation Information Tab
		boolean isValid = AnonymousHelper.CheckNull(cusname, cusage, cusphone);
		if (isValid) {
			return "redirect:/referral";
		}
		if (cusnochd == null || cusnochd == "") {
			cusnochd = "0";
		}
		if (acbProducts != null) {
			acbProductsNew = acbProducts.split(",");
		}
		cusname = AnonymousHelper.sanitizeInput(cusname);
		cussex = AnonymousHelper.sanitizeInput(cussex);
		cusage = AnonymousHelper.sanitizeInput(cusage);
		cusmstatus = AnonymousHelper.sanitizeInput(cusmstatus);
		cusnochd = AnonymousHelper.sanitizeInput(cusnochd);
		cusphone = AnonymousHelper.sanitizeInput(cusphone);
		rtype = AnonymousHelper.sanitizeInput(rtype);
		rremark = AnonymousHelper.sanitizeInput(rremark);
		cusocc = AnonymousHelper.sanitizeInput(cusocc);

		Referral referral = new Referral();
		if (acbProductsNew != null && acbProductsNew.length > 0) {
			List<Object> obj = new ArrayList<Object>();
			List<ReferralAcbProduct> lstAcbProduct = new ArrayList<ReferralAcbProduct>();
			for (int i = 0; i < acbProductsNew.length; i = i + 3) {
				ArrayList<String> obj1 = new ArrayList<String>();
				for (int j = i; j < i + 3; j++) {
					obj1.add(acbProductsNew[j]);
				}
				obj.add(obj1);
			}

			for (Object ele : obj) {
				ReferralAcbProduct prd = new ReferralAcbProduct();
				ArrayList<String> lst_obj = (ArrayList<String>) ele;
				prd.setAcbPrdId(Integer.parseInt(lst_obj.get(0)));
				prd.setCreatedUserId(principal.getName());
				prd.setCreatedDatetime(new Date());
				prd.setValidFlag("1");
				prd.setReferral(referral);
				prd.setCusApproach(lst_obj.get(1));
				prd.setCommentId(Integer.parseInt(lst_obj.get(2)));
				lstAcbProduct.add(prd);
			}

			if (lstAcbProduct.size() > 0)
				referral.setReferralAcbProducts(lstAcbProduct);
		}

		createNewReferral(trnDate, exiscusstatus, rtype, rDate, referralBy, rremark, cusname, cussex, cusage,
				cusmstatus, cusnochd, cusphone, agntnum, cusocc, referral);
		referralService.save(referral, referral.getTrnBy());

		if (cid != "" || cid != "0") {
			Date fappDate = null;
			if (fappate != null &&  !fappate.equals("")) {
				Date fappate1 = new SimpleDateFormat("dd/MM/yyyy").parse(fappate);
				fappate = new SimpleDateFormat("yyyy/MM/dd").format(fappate1);
				fappDate = new SimpleDateFormat("yyyy/MM/dd").parse(fappate);
			}
			if (fappate == "" || fappate == "0") {
				fappDate = null;
			}

			ReferralFollowUpComment comment = new ReferralFollowUpComment();
			rid = referralService.ReferrerMaxID(SecurityContextHolder.getContext().getAuthentication().getName()).get(0)
					.toString();
			comment.setTrnBy(SecurityContextHolder.getContext().getAuthentication().getName());
			comment.setTrnDate(new Date());
			comment.setIp(AnonymousHelper.getClientIP(request));
			comment.setPip(AnonymousHelper.getClientIP(request));
			comment.setRid(Long.parseLong(rid));
			comment.setCid(Long.parseLong(cid));
			comment.setRemark(AnonymousHelper.sanitizeInput(remark));
			comment.setAppDate(fappDate);

			Referral referral2 = referralService.getReferral(rid + "",
					SecurityContextHolder.getContext().getAuthentication().getName());
			referral2.setFlwupCmtTypeLast(AnonymousHelper.sanitizeInput(comment.getCid() + ""));
			referral2.setFlwupCmtRemarkLast(AnonymousHelper.sanitizeInput(comment.getRemark()));
			referral2.setAppDate(comment.getAppDate());
			referral2.setFlwupTrnDate(new Date());
			referralService.update(referral2.getrID(), referral2);
		}

		return rid;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/newreferral", method = RequestMethod.POST)
	public String newReferral(Principal principal, Model model, HttpServletRequest request) throws ParseException {
		// For Information Tab

		String cusname = request.getParameter("cusName").trim();
		String cussex = request.getParameter("cusSex");

		String rid = newReferralHelper(cusname, cussex, principal, model, request);

		return "redirect:/referral/edit/" + rid;
	}

	/**
	 * Create a new referral
	 */
	private void createNewReferral(Date trnDate, String exiscusstatus, String rtype, Date rDate, String referralBy,
			String rremark, String cusname, String cussex, String cusage, String cusmstatus, String cusnochd,
			String cusphone, String agntnum, String cusocc, Referral referral) {
		referral.setTrnDateTime(new Date());
		referral.setCdate(new Date());
		referral.setExisCusStatus(exiscusstatus);
		referral.setrType(rtype);
		referral.setrDate(rDate);
		referral.setReferralBy(referralBy);
		referral.setrRemark(rremark);
		referral.setCusName(cusname);
		referral.setCusSex(cussex);
		referral.setCusAge(cusage);
		referral.setCusMStatus(cusmstatus);
		referral.setCusNochd(cusnochd);
		referral.setCusPhone(cusphone);
		referral.setTrnBy(SecurityContextHolder.getContext().getAuthentication().getName());
		referral.setLastTrnBy(SecurityContextHolder.getContext().getAuthentication().getName());
		referral.setOrgAgntNum(SecurityContextHolder.getContext().getAuthentication().getName());
		referral.setOrgBranch(referralService.getAgentBranch(referral.getOrgAgntNum()));
		referral.setCusOcc(Integer.parseInt(cusocc));
		referral.setAgntNum(agntnum);
		referral.setCusAprStatus("Yes");
		referral.setCusAprDate(new Date());
		referral.setValidFlag("1");
		referral.setDraft("1");
	}

	private void saveRreferralInfo(String exiscusstatus, String rtype, String referralBy, String rremark,
			String cusname, String cussex, String cusage, String cusmstatus, String cusnochd, String cusphone,
			String agntnum, Referral referral, String customerForm, String cusocc) {
		referral.setExisCusStatus(exiscusstatus);
		referral.setrType(rtype);
		referral.setReferralBy(referralBy);
		referral.setrRemark(rremark);
		referral.setCusName(cusname);
		referral.setCusSex(cussex);
		referral.setCusAge(cusage);
		referral.setCusMStatus(cusmstatus);
		referral.setCusNochd(cusnochd);
		referral.setCusPhone(cusphone);
		referral.setTrnBy(SecurityContextHolder.getContext().getAuthentication().getName());
		referral.setLastTrnBy(SecurityContextHolder.getContext().getAuthentication().getName());
		referral.setOrgAgntNum(SecurityContextHolder.getContext().getAuthentication().getName());
		// referral.setOrgBranch(referralService.getAgentBranch(referral.getOrgAgntNum()));
		referral.setAgntNum(agntnum);
		referral.setCusOcc(Integer.parseInt(cusocc));
		referral.setValidFlag("1");
		referral.setCustomerFrom(customerForm);
	}

	private void saveRreferralSale(String sucappstatus, Date sucAppDate, Referral referral, String salsucsalprestatus,
			Date salSucSalPreDate, String cussalarymonthly, String cussalaryyearly, String salmthbasexp,
			String salrecprdpkgusd, String cusAFPremium) {
		referral.setSucAppStatus(sucappstatus);
		String draft = referral.getDraft();
		if (draft != "2") {
			referral.setSucAppDate(sucAppDate);
		}
		// referral.setOrgBranch(referralService.getAgentBranch(referral.getOrgAgntNum()));
		referral.setSalSucSalPreStatus(salsucsalprestatus);
		if (referral.getSalSucSalPreDate() == null) {
			referral.setSalSucSalPreDate(salSucSalPreDate);
		}
		referral.setCusSalaryMonthly(Float.parseFloat(cussalarymonthly));
		referral.setCusSalaryYearly(Float.parseFloat(cussalaryyearly));
		referral.setSalMthBasExp(Integer.parseInt(salmthbasexp));
		referral.setSalRecPrdPkgUsd(Integer.parseInt(salrecprdpkgusd));
		referral.setCusAFPremium(Float.parseFloat(cusAFPremium));
		referral.setDraft("2");
	}

	private void saveReferralFinal(String clsBankAppNum, String clsBankUploadRemark, String ape, Referral referral,
			String clsprdtype, String clspolterm, String clspolmode, String customerFrom, Date clsbankuploaddate) {
		referral.setClsBankAppNum(clsBankAppNum);
		referral.setClsBankUploadRemark(clsBankUploadRemark);
		referral.setAPE(Double.parseDouble(ape));
		referral.setClsPrdType(clsprdtype);
		referral.setClsPolTerm(clspolterm);
		referral.setClsPolMode(clspolmode);
		referral.setCustomerFrom(customerFrom);
		if (referral.isClosed() == false) {
			referral.setClsBankUploadDate(clsbankuploaddate);
		}
		referral.setClosed(true);
		referral.setDraft("3");
	}

	@RequestMapping(value = "/referral/edit/{id}", method = RequestMethod.GET)
	public String editReferral(@PathVariable("id") String id, Model model, Principal principal) {
		// Check if current user has valid authorization on this referral
		Referral referralMgt = referralService.getReferralMgt(id, principal.getName());
		if (!(referralMgt.getrID() == Long.parseLong(id))) {
			return "redirect:/referral";
		}
		List<AcbProduct> allAcbProduct = referralService.listAcbProduct();
		List<String> nameDescFormat = new ArrayList<>();
		List<String> shortDes = allAcbProduct.stream().map(s -> s.getShortDesc()).collect(Collectors.toList());

		for (String obj : shortDes) {
			String objrepalcestring = obj.replaceAll("\\s+", "");
			nameDescFormat.add("productname-" + objrepalcestring);
			nameDescFormat.add("cusappby-" + objrepalcestring);
			nameDescFormat.add("comment-" + objrepalcestring);
		}
		model.addAttribute("nameDescFormat", nameDescFormat);
		model.addAttribute("rType", referralService.listReferralType());
		model.addAttribute("rReferralCommentType", referralService.listReferralCommentType());
		model.addAttribute("referrer", referralService.listReferrer(principal.getName()));
		model.addAttribute("occupation", referralService.listCusOccupation());
		model.addAttribute("listProduct", referralService.listProduct());
		model.addAttribute("listPolicyTerm", referralService.listPolicyTerm());
		model.addAttribute("listPolicyMode", referralService.listPolicyMode());
		model.addAttribute("listReferralFollowup", referralService.listReferralFollowup(id));
		model.addAttribute("pageTitle", "Edit Referral");
		model.addAttribute("referralparam", referralMgt);
		model.addAttribute("customerFromList", referralService.listCustomerFrom());
		model.addAttribute("rid", id);
		model.addAttribute("closedsale", referralMgt.isClosed());
		model.addAttribute("draft", referralMgt.getDraft());
		model.addAttribute("sucAppDate", referralMgt.getSucAppDate());
		model.addAttribute("acbProductComments", referralService.listAcbProductComment());
		model.addAttribute("branchs", branchService.listBranchs());

		// Add Agent Branch tmp
		if (referralMgt.getReferralBy().startsWith("9999999")) {
			Object agntBranchCode = referralMgt.getOrgBranch(); 
					
			model.addAttribute("agntBranchCode", agntBranchCode == null ? "" : agntBranchCode.toString().trim());
		} else {
			Object agntBranchCode = branchService.getSingleObject("tabreferrer", "staffbranch", "id",
					Integer.parseInt(referralMgt.getReferralBy()));
			model.addAttribute("agntBranchCode", agntBranchCode == null ? "" : agntBranchCode.toString().trim());
		}

		List<AcbProduct> lstAcbProducts = referralService.listAcbProduct();
		List<ReferralAcbProduct> lstReferralAcbProduct = referralMgt.getReferralAcbProducts();
		for (ReferralAcbProduct obj : lstReferralAcbProduct) {
			for (int i = 0; i < lstAcbProducts.size(); i++) {
				if (lstAcbProducts.get(i).getId() == obj.getAcbPrdId()) {
					lstAcbProducts.get(i).setCusApproach(obj.getCusApproach());
					lstAcbProducts.get(i).setCommentid(obj.getCommentId());
				}
			}
		}
		model.addAttribute("acbProducts", lstAcbProducts);

		return "referral/editreferral";
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/referral/edit/{id}", method = RequestMethod.POST, params = "btnaction=saveinfo")
	public String editReferralInfo(@PathVariable("id") String id, Principal principal, Model model,
			HttpServletRequest request, @Valid @ModelAttribute("referralparam") Referral referralparam,
			BindingResult result) throws ParseException {

		String rtype = request.getParameter("rType");
		String cusname = request.getParameter("cusName");
		String cussex = request.getParameter("cusSex");
		String cusage = request.getParameter("cusAge");
		String cusmstatus = request.getParameter("cusMStatus");
		String cusnochd = request.getParameter("cusNochd");
		if (cusmstatus.equals("Single")) {
			cusnochd = "0";
		}
		String cusphone = request.getParameter("cusPhone");
		String rremark = request.getParameter("rRemark");
		String exiscusstatus = request.getParameter("exisCusStatus");
		String agntnum = principal.getName();
		String cusocc = request.getParameter("cusOcc");
		String referralBy = request.getParameter("referralBy");
		String customerFrom = request.getParameter("customerFrom");
		String[] acbProductsNew = null;
		String acbProducts = request.getParameter("referralAcbProductNew");

		if (cusnochd == "" || cusnochd == null) {
			cusnochd = "0";
		}
		if (acbProducts != null) {
			acbProductsNew = acbProducts.split(",");
		}

		cusname = AnonymousHelper.sanitizeInput(cusname);
		cussex = AnonymousHelper.sanitizeInput(cussex);
		cusage = AnonymousHelper.sanitizeInput(cusage);
		cusmstatus = AnonymousHelper.sanitizeInput(cusmstatus);
		cusnochd = AnonymousHelper.sanitizeInput(cusnochd);
		cusphone = AnonymousHelper.sanitizeInput(cusphone);
		rtype = AnonymousHelper.sanitizeInput(rtype);
		rremark = AnonymousHelper.sanitizeInput(rremark);
		exiscusstatus = AnonymousHelper.sanitizeInput(exiscusstatus);
		cusocc = AnonymousHelper.sanitizeInput(cusocc);

		Referral referral = referralService.getReferral(id);
		saveRreferralInfo(exiscusstatus, rtype, referralBy, rremark, cusname, cussex, cusage, cusmstatus, cusnochd,
				cusphone, agntnum, referral, customerFrom, cusocc);

		Referral referralMgt = referralService.getReferral(id);

		if (acbProductsNew != null && acbProductsNew.length > 0) {
			List<Object> obj = new ArrayList<Object>();
			List<ReferralAcbProduct> lstAcbProduct = new ArrayList<ReferralAcbProduct>();
			for (int i = 0; i < acbProductsNew.length; i = i + 3) {
				ArrayList<String> obj1 = new ArrayList<String>();
				for (int j = i; j < i + 3; j++) {
					obj1.add(acbProductsNew[j]);
				}
				obj.add(obj1);
			}

			for (Object ele : obj) {
				ReferralAcbProduct prd = new ReferralAcbProduct();
				ArrayList<String> lst_obj = (ArrayList<String>) ele;
				prd.setAcbPrdId(Integer.parseInt(lst_obj.get(0)));
				prd.setCreatedUserId(principal.getName());
				prd.setCreatedDatetime(new Date());
				prd.setValidFlag("1");
				prd.setReferral(referralMgt);
				prd.setCusApproach(lst_obj.get(1));
				prd.setCommentId(Integer.parseInt(lst_obj.get(2)));
				lstAcbProduct.add(prd);
			}
			List<ReferralAcbProduct> listTmp = new ArrayList<>();

			for (ReferralAcbProduct o : lstAcbProduct) {
				if (o.getCommentId() > 0) {
					listTmp.add(o);
				}
			}

			for (ReferralAcbProduct listoldreferralacbproduct : referralMgt.getReferralAcbProducts()) {
				for (ReferralAcbProduct o : lstAcbProduct) {
					if (listoldreferralacbproduct.getAcbPrdId() == o.getAcbPrdId()) {
						listoldreferralacbproduct.setCusApproach(o.getCusApproach());
						listoldreferralacbproduct.setCommentId(o.getCommentId());
						listTmp.remove(o);
						listTmp.add(listoldreferralacbproduct);
					}
				}
			}
			referralMgt.setReferralAcbProducts(listTmp);
			referralService.referralSave(referralMgt);
		}
		referralService.update(Long.parseLong(id), referral);

		return "redirect:/referral/edit/" + referral.getrID();
	}

	@RequestMapping(value = "/referral/edit/{id}", method = RequestMethod.POST, params = "btnaction=savesale")
	public String editReferralSale(@PathVariable("id") String id, Principal principal, Model model,
			HttpServletRequest request, @Valid @ModelAttribute("referralparam") Referral referralparam,
			BindingResult result) throws ParseException {

		Referral referral = referralService.getReferral(id);

		String sucappstatus = request.getParameter("sucAppStatus");
		if (!"Yes".equals(sucappstatus)) {
			return "redirect:/referral";
		}
		String sucAppDateString = request.getParameter("sucAppDate");
		if (sucAppDateString.equals("") || sucAppDateString.equals(null)) {
			return "redirect:/referral";
		}
		Date sucAppDate = new SimpleDateFormat("dd/MM/yyyy h:mm a").parse(sucAppDateString);
		;
		String salsucsalprestatus = request.getParameter("salSucSalPreStatus");
		Date salSucSalPreDate = null;
		String cussalarymonthly = request.getParameter("cusSalaryMonthly");
		String cussalaryyearly = request.getParameter("cusSalaryYearly");
		String cusAFPremium = request.getParameter("cusAFPremium");
		String salmthbasexp = request.getParameter("salMthBasExp");
		String salrecprdpkgusd = request.getParameter("salRecPrdPkgUsd");

		// Validation Sale Activity Tab

		if ("Yes".equals(salsucsalprestatus)) {
			salSucSalPreDate = new Date();
			if (!AnonymousHelper.isNumeric(cussalarymonthly) || cussalarymonthly == "" || cussalarymonthly == null
					|| cussalarymonthly.equals("0") || cussalarymonthly.equals("0.0")) {
				return "redirect:/referral";
			}
			cussalaryyearly = Float.toString(Float.parseFloat(cussalarymonthly) * 12);
			cusAFPremium = Float.toString(((Float.parseFloat(cussalarymonthly) * 12) * 10) / 100);
			if (!AnonymousHelper.isNumeric(salmthbasexp) || salmthbasexp == "" || salmthbasexp == null
					|| salmthbasexp.equals("0") || salmthbasexp.equals("0.0")) {
				return "redirect:/referral";
			}
			if (!AnonymousHelper.isNumeric(salrecprdpkgusd) || salrecprdpkgusd == "" || salrecprdpkgusd == null
					|| salrecprdpkgusd.equals("0") || salrecprdpkgusd.equals("0.0")) {
				return "redirect:/referral";
			}
		} else {
			salSucSalPreDate = null;
			cussalarymonthly = "0";
			cussalaryyearly = "0";
			cusAFPremium = "0";
			salmthbasexp = "0";
			salrecprdpkgusd = "0";
		}

		sucappstatus = AnonymousHelper.sanitizeInput(sucappstatus);
		salsucsalprestatus = AnonymousHelper.sanitizeInput(salsucsalprestatus);
		cussalarymonthly = AnonymousHelper.sanitizeInput(cussalarymonthly);
		cussalaryyearly = AnonymousHelper.sanitizeInput(cussalaryyearly);
		cusAFPremium = AnonymousHelper.sanitizeInput(cusAFPremium);
		salmthbasexp = AnonymousHelper.sanitizeInput(salmthbasexp);
		salrecprdpkgusd = AnonymousHelper.sanitizeInput(salrecprdpkgusd);

		saveRreferralSale(sucappstatus, sucAppDate, referral, salsucsalprestatus, salSucSalPreDate, cussalarymonthly,
				cussalaryyearly, salmthbasexp, salrecprdpkgusd, cusAFPremium);
		referralService.update(Long.parseLong(id), referral);

		return "redirect:/referral/edit/" + referral.getrID();
	}

	@RequestMapping(value = "/referral/edit/{id}", method = RequestMethod.POST, params = "btnaction=savefinal")
	public String editReferralFinal(@PathVariable("id") String id, Principal principal, Model model,
			HttpServletRequest request, @Valid @ModelAttribute("referralparam") Referral referralparam,
			BindingResult result) throws ParseException {

		Referral referral = referralService.getReferral(id);
		if (referral.getDraft().equals("1") || referral.getSucAppStatus().equals("")
				|| referral.getSalSucSalPreStatus().equals("") || referral.getSalSucSalPreStatus().equals("No")) {
			return "redirect:/referral";
		}
		String clsprdtype = request.getParameter("clsPrdType");
		List<Object> listProducts = referralService.getProduct(clsprdtype);
		if (listProducts.toArray().length == 0) {
			return "redirect:/referral";
		}
		String clspolterm = request.getParameter("clsPolTerm");
		String clspolmode = request.getParameter("clsPolMode");
		if (clspolmode.equals("0")) {
			return "redirect:/referral";
		}
		String ape = request.getParameter("APE");
		if (ape.equals("0") || ape.equals("0.0")) {
			return "redirect:/referral";
		}
		String clsBankAppNum = request.getParameter("clsBankAppNum");
		if (clsBankAppNum.equals(null) || clsBankAppNum.equals("0") || clsBankAppNum.equals("")) {
			return "referral/referral";
		}
		Date clsbankuploaddate = new Date();
		String customerFrom = request.getParameter("customerFrom");
		String clsBankUploadRemark = request.getParameter("clsBankUploadRemark");

		clsprdtype = AnonymousHelper.sanitizeInput(clsprdtype);
		clspolterm = AnonymousHelper.sanitizeInput(clspolterm);
		clspolmode = AnonymousHelper.sanitizeInput(clspolmode);
		ape = AnonymousHelper.sanitizeInput(ape);
		clsBankAppNum = AnonymousHelper.sanitizeInput(clsBankAppNum);
		customerFrom = AnonymousHelper.sanitizeInput(customerFrom);
		clsBankUploadRemark = AnonymousHelper.sanitizeInput(clsBankUploadRemark);

		saveReferralFinal(clsBankAppNum, clsBankUploadRemark, ape, referral, clsprdtype, clspolterm, clspolmode,
				customerFrom, clsbankuploaddate);
		referralService.update(Long.parseLong(id), referral);

		return "redirect:/referral";
	}

	@RequestMapping(value = "/referral/comment/{id}", method = RequestMethod.GET)
	public String referralComment(@PathVariable("id") long id, Model model) {
		// Check if current user has valid authorization on this referral
		Referral referralMgt = referralService.getReferralMgt(id + "",
				SecurityContextHolder.getContext().getAuthentication().getName());
		if (!(referralMgt.getrID() == id)) {
			return "redirect:/referral";
		}

		ReferralFollowUpComment referralComment = new ReferralFollowUpComment();

		if (referralMgt != null) {
			referralComment.setAppNum(referralMgt.getClsBankAppNum());
			referralComment.setCusName(referralMgt.getCusName());
			referralComment.setCusSex(referralMgt.getCusSex());
			referralComment.setCusPhone1(referralMgt.getCusPhone());
		}

		model.addAttribute("referralComment", referralComment);
		model.addAttribute("rReferralCommentType", referralService.listReferralCommentType());
		model.addAttribute("pageTitle", "Referral Comment");
		model.addAttribute("listReferralFollowup", referralService.listReferralFollowup(Long.toString(id)));

		return "referral/referralcomment";
	}

	// @Link(label="Referral Comment", family="ReferralController", parent =
	// "Referral" )
	@RequestMapping(value = "/referral/comment/{id}", method = RequestMethod.POST)
	public String newReferralComment(@PathVariable("id") long id, Model model, HttpServletRequest request,
			@Valid @ModelAttribute("referralComment") ReferralFollowUpComment referralComment, BindingResult result) {
		if (result.hasErrors()) {
			model.addAttribute("referralComment", referralComment);
			model.addAttribute("rReferralCommentType", referralService.listReferralCommentType());
			model.addAttribute("pageTitle", "Referral Comment");
			model.addAttribute("errors", result.getAllErrors());

			return "referral/referralcomment";
		}

		if (!isCommentValid(referralComment)) {
			model.addAttribute("referralComment", referralComment);
			model.addAttribute("rReferralCommentType", referralService.listReferralCommentType());
			model.addAttribute("pageTitle", "Referral Comment");

			return "referral/referralcomment";
		}

		Referral referral = referralService.getReferral(id + "",
				SecurityContextHolder.getContext().getAuthentication().getName());

		referralComment.setTrnBy(SecurityContextHolder.getContext().getAuthentication().getName());
		referralComment.setTrnDate(new Date());
		referralComment.setIp(AnonymousHelper.getClientIP(request));
		referralComment.setPip(AnonymousHelper.getClientIP(request));
		referralComment.setRid(id);

		referral.setFlwupCmtTypeLast(AnonymousHelper.sanitizeInput(referralComment.getCid() + ""));
		referral.setFlwupCmtRemarkLast(AnonymousHelper.sanitizeInput(referralComment.getRemark()));
		referral.setAppDate(referralComment.getAppDate());
		referral.setFlwupTrnDate(new Date());

		// int affectedRow = referralService.saveComment(referralComment);
		referralService.update(referral.getrID(), referral);

		return "redirect:/referral/" + "comment/" + id;
	}

	@RequestMapping(value = "/referral/detail/{id}", method = RequestMethod.GET)
	public String referralDetail(@PathVariable("id") long id, Model model) {
		Referral referral = null;
		String userID = SecurityContextHolder.getContext().getAuthentication().getName();

		if (userID.equals("326625") || userID.equals("312898")) {
			referral = referralService.getReferral(id + "");
		} else {
			referral = referralService.getReferralMgt(id + "", userID);
		}

		model.addAttribute("pageTitle", "Referral Detail");
		model.addAttribute("referralparam", referral);
		model.addAttribute("rType", referralService.listReferralType());
		model.addAttribute("rReferralCommentType", referralService.listReferralCommentType());
		model.addAttribute("referrer", referralService.listReferrer(userID));
		model.addAttribute("occupation", referralService.listCusOccupation());
		model.addAttribute("listProduct", referralService.listProduct());
		model.addAttribute("listPolicyTerm", referralService.listPolicyTerm());
		model.addAttribute("listPolicyMode", referralService.listPolicyMode());
		model.addAttribute("listReferralFollowup", referralService.listReferralFollowup(id + ""));
		model.addAttribute("customerFromList", referralService.listCustomerFrom());

		return "referral/referraldetail";
	}

	@RequestMapping(value = "/managereferral/supbybdm", method = RequestMethod.POST)
	@ResponseBody
	public String getSupervisorByBDM(HttpServletRequest request) {
		String position = request.getParameter("position").toString();
		String bdm = request.getParameter("bdm").toString();
		String result = "";
		ObjectMapper mapper = new ObjectMapper();

		List<Object[]> list = referralService.listSupervisorByBDM(bdm, position);

		try {
			result = mapper.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	@RequestMapping(value = "/managereferral/agntbybranch", method = RequestMethod.POST)
	@ResponseBody
	public String getAgentByBranch(HttpServletRequest request) {
		String branch = request.getParameter("branch").toString();
		String result = "";
		ObjectMapper mapper = new ObjectMapper();

		List<Object[]> list = referralService.listAgentByBranch(branch);

		try {
			result = mapper.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	@RequestMapping(value = "/managereferral/branchbybdm", method = RequestMethod.POST)
	@ResponseBody
	public String getBranchByBDM(HttpServletRequest request) {
		String bdm = request.getParameter("bdm").toString();
		String result = "";
		ObjectMapper mapper = new ObjectMapper();

		List<Object[]> list = null;

		if (bdm != "") {
			list = referralService.listBranchByBDM(bdm);
		} else {
			list = referralService
					.listDynamic("SELECT DISTINCT trim(agnt_branch) AS branch, trim(agnt_branch) AS branchDesc FROM "
							+ GlobalVar.db_name + ".tabagnt_fc_hk ORDER BY branch");
		}

		try {
			result = mapper.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	@RequestMapping(value = "/managereferral/reloadparam", method = RequestMethod.POST)
	@ResponseBody
	public String reloadParam(HttpServletRequest request) {
		String result = "";
		String rbdm = request.getParameter("rbdm");
		String sbdm = request.getParameter("sbdm");
		String bdm = request.getParameter("bdm");
		String branch = request.getParameter("branch");
		String agntsup = request.getParameter("agntsup");
		String agntnum = request.getParameter("agntnum");

		List<List<Object[]>> list = referralService.listSearchReferralMgtParam(
				SecurityContextHolder.getContext().getAuthentication().getName(), rbdm, sbdm, bdm, branch, agntsup,
				agntnum);

		ObjectMapper mapper = new ObjectMapper();

		try {
			result = mapper.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	private boolean isCommentValid(ReferralFollowUpComment comment) {
		if (comment.getCid() == 0)
			return false;

		if (comment.getRemark().trim().equals(""))
			return false;

		return true;
	}

	@RequestMapping(value = "/referral/closesale", method = RequestMethod.GET)
	@ResponseBody
	public int ClosedSale(HttpServletRequest res, Principal principal) {
		String referralid = res.getParameter("rid");

		Referral referralMgt = referralService.getReferralMgt(referralid, principal.getName());
		if (!(referralMgt.getrID() == Long.parseLong(referralid))) {
			return 0;
		}
		int affectedRow = referralService.updateCloseSale(Long.parseLong(referralid), true);
		if (affectedRow > 0) {
			String action = "Closed Sale:" + referralid;
			String ip = res.getRemoteHost();
			String pip = res.getRemoteHost();
			Date date = new Date();
			logService.logUserAction(referralMgt.getTrnBy(), action, ip, pip, date);
		}
		return affectedRow;
	}

	@RequestMapping(value = "/getreferrersbyagntnumandbranchcode", method = RequestMethod.GET)
	@ResponseBody
	public String GetReferrersByAgntNumAndBranchCode(Principal principal, HttpServletRequest req) {
		String result = "";
		String userid = principal.getName();
		String branchCode = req.getParameter("branchCode");
		ObjectMapper mapper = new ObjectMapper();
		List<Object> arrReferrers = referralService.listReferrer(userid, branchCode);

		try {
			result = mapper.writeValueAsString(arrReferrers);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return result;
	}

	@RequestMapping(value = "/getcertifiedreferrersbyagntnumandbranchcode", method = RequestMethod.GET)
	@ResponseBody
	public String GetCertifiedReferrersByAgntNumAndBranchCode(Principal principal, HttpServletRequest req) {
		String result = "";
		String userid = principal.getName();
		String branchCode = req.getParameter("branchCode");
		ObjectMapper mapper = new ObjectMapper();
		List<Object> arrReferrers = referralService.listCertifiedReferrer(userid, branchCode);

		try {
			result = mapper.writeValueAsString(arrReferrers);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return result;
	}
	// @SuppressWarnings("null")
	// @RequestMapping(value = "/getAllAcbProducts", method = RequestMethod.GET)
	// @ResponseBody
	// public List<String> AllAcbProducts() {
	// List<AcbProduct> allAcbProduct = referralService.listAcbProduct();
	// List<String> nameDescFormat = new ArrayList<>();
	// List<String> shortDes = allAcbProduct.stream()
	// .map(s -> s.getShortDesc()).collect(Collectors.toList());
	//
	// for(String obj: shortDes) {
	// String objrepalcestring = obj.replaceAll("\\s+","");
	// nameDescFormat.add("cusappby-"+objrepalcestring);
	// nameDescFormat.add("comment-"+objrepalcestring);
	// }
	// return nameDescFormat;
	// }
	// ==================================================================

	// here is the controller and function for approach
	@PreAuthorize("hasAnyAuthority('CREATE_UPSELL_CROSSSELL')")
	@RequestMapping(value = "/newapproach/{id}", method = RequestMethod.GET)
	public String newApproachReferral(@PathVariable String id, Principal principal, Model model) throws Exception {
		List<AcbProduct> allAcbProduct = referralService.listAcbProduct();
		List<String> nameDescFormat = new ArrayList<>();
		List<String> shortDes = allAcbProduct.stream().map(s -> s.getShortDesc()).collect(Collectors.toList());

		for (String obj : shortDes) {
			String objrepalcestring = obj.replaceAll("\\s+", "");
			nameDescFormat.add("productname-" + objrepalcestring);
			nameDescFormat.add("cusappby-" + objrepalcestring);
			nameDescFormat.add("comment-" + objrepalcestring);
		}

		UpsellingAndCrosssellingModel getApproach = null;

		try {
			getApproach = upsellingAndCrosssellingService.getApproachData(principal.getName(), id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (getApproach == null)
			throw new ResourceNotFoundException();
		//
		// for (String key : getApproach.keySet()) {
		// model.addAttribute(key, getApproach.get(key));
		// }

		Referral referralMgt = new Referral();

		// int cusOccu = Integer.parseInt((String) getApproach.get("cusOccupation"));

		referralMgt.setCusSex((String) getApproach.getCusSex());
		referralMgt.setCusName((String) getApproach.getCusName());
		referralMgt.setrDate((Date) new Date());
		referralMgt.setCusAge(Integer.toString( getApproach.getPoCusage()));
		referralMgt.setCusPhone(getApproach.getCusMobile1());

		// List<Referrer> referrers = referrerService.listReferrer(principal.getName(),
		// (String) getApproach.get("agentNum"));
		// if (referrers.size() > 0)
		// referralMgt.setReferralBy("" + referrers.get(0).getId());

		List<ArrayList<Object>> listCusOccupations = referralService.listCusOccupation();

		// String occStr = getApproach.get("cusOccupation").toString();
		// if (occStr != null) {
		// Object[] cusOccupation = listCusOccupations.stream().filter(s ->
		// s.get(1).toString().equals(occStr))
		// .toArray();
		// if (cusOccupation.length > 0 && cusOccupation[0] != null)
		// referralMgt.setCusOcc(Integer.parseInt(cusOccupation[0].toString()));
		// }

		String[] digits = getApproach.getAgentNum().split("");
		String digit = digits[0] + digits[1];

		if (digit.equals("60")) {
			referralMgt.setReferralBy("99999990");
		} else if (digit.equals("69")) {
			referralMgt.setReferralBy("99999999");
		}
		
		model.addAttribute("referralparam", referralMgt);
		model.addAttribute("rType", referralService.listAllReferralType("approach"));
		model.addAttribute("rReferralCommentType", referralService.listReferralCommentType()); 
		model.addAttribute("pageTitle", "New Approach");
		// model.addAttribute("referrer",referralService.listCertifiedReferrer(principal.getName()));
		model.addAttribute("occupation", listCusOccupations);
		model.addAttribute("listProduct", referralService.listProduct());
		model.addAttribute("listPolicyTerm", referralService.listPolicyTerm());
		model.addAttribute("listPolicyMode", referralService.listPolicyMode());
		model.addAttribute("rReferralCommentType", referralService.listReferralCommentType());
		model.addAttribute("customerFromList", referralService.listCustomerFrom());
		model.addAttribute("acbProducts", referralService.listAcbProduct());
		model.addAttribute("acbProductComments", referralService.listAcbProductComment());
		model.addAttribute("nameDescFormat", nameDescFormat);
		model.addAttribute("branchs", branchService.listBranchs());

		Object agntBranchCode = branchService.getSingleObject("tabagnt_fc_hk", "agnt_branch", "agntnum",
				getApproach.getAgentNum());
		model.addAttribute("agntBranchCode", agntBranchCode == null ? "" : agntBranchCode.toString().trim());

		return "referral/newapproach";
	}

	@PreAuthorize("hasAnyAuthority('CREATE_UPSELL_CROSSSELL')")
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/newapproach/{id}", method = RequestMethod.POST)
	public String newReferralFromApproach(@PathVariable("id") String id, Principal principal, Model model,
			HttpServletRequest request) throws ParseException {

		UpsellingAndCrosssellingModel getApproach = null;

		try {
			getApproach = upsellingAndCrosssellingService.getApproachData(principal.getName(), id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (getApproach == null)
			throw new ResourceNotFoundException();

		// validate not allowed for already approached
		if (getApproach.getApproachStatusId().equals("1"))
			throw new ResourceNotFoundException();

		String cusname = getApproach.getCusName();
		String cussex = getApproach.getCusSex();

		String rid = newReferralHelper(cusname, cussex, principal, model, request);

		// add into table cross and up sell link with referral
		UpsellingAndCrosssellingReferral crosssell = new UpsellingAndCrosssellingReferral();
		crosssell.setObjKey(id);
		BigDecimal bd = new BigDecimal(rid);
		crosssell.setrId(bd);
		referralService.saveApproach(crosssell);

//		return "redirect:/upsellingandcrossselling";
		return "redirect:/referral/edit/" + rid;
	}

}