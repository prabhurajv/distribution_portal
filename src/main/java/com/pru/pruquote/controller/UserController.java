package com.pru.pruquote.controller;


import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.pru.pruquote.dao.ILogDao;
import com.pru.pruquote.dao.IUserDao;
import com.pru.pruquote.model.Permission;
import com.pru.pruquote.model.Pwd;
import com.pru.pruquote.model.Role;
import com.pru.pruquote.model.User;
import com.pru.pruquote.model.UserHistory;
import com.pru.pruquote.model.UserRole;
import com.pru.pruquote.service.SecurityService;
import com.pru.pruquote.service.UserService;
import com.pru.pruquote.utility.AnonymousHelper;
import com.pru.pruquote.utility.PwdEncoder;

//import dummiesmind.breadcrumb.springmvc.annotations.Link;

@Controller
@PreAuthorize("hasAnyAuthority('ACCESS_USER,ACCESS_MANUSRLOW')")
public class UserController {	
	
	@Autowired	
	private SecurityService securityService;
	
	@Autowired
	IUserDao userDao;
	
	@Autowired
	UserService userService;
	
	@Autowired
	ILogDao logDao;
	
	@ModelAttribute("userRole")
    public UserRole Constructor(){
    	return new UserRole();
    }
	
	@ModelAttribute("roles")
    public Set<Role> rolesConstructor(){
    	if(checkAuthority("ACCESS_MANUSRLOW")){
    		return securityService.roleFindByRoleLVLessThanAndStatus((byte)2, (byte) 1);
    	}else {
    		return securityService.roleFindByStatus((byte) 1); 
		}
    }
	
	@ModelAttribute("permission")
	public Permission permissionConstructor(){
    	return new Permission();
    }
	@ModelAttribute("role")
    public Role roleConstructor(){
    	return new Role();
    }
		
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
        binder.registerCustomEditor(Date.class,"user.pwdExpiredDate",new CustomDateEditor(dateFormat, true));
        binder.registerCustomEditor(Date.class,"user.accountExpiredDate",new CustomDateEditor(dateFormat, true));        
    }
	
//	@Link(label="User", family="UserController", parent = "" )
	@RequestMapping(value =  "/user", method = RequestMethod.GET)
	public ModelAndView userMgt() {

		ModelAndView model = new ModelAndView();		
    	if(checkAuthority("ACCESS_MANUSRLOW")){
    		model.addObject("users", securityService.userFindAllWithRoleLV());
    	}else {
    		model.addObject("users", securityService.userFindAllWithRole());
		}
		model.setViewName("usermgt");
		return model;

	}
	
//	@Link(label="New User", family="UserController", parent = "User" )
	@RequestMapping(value =  "/newuser", method = RequestMethod.GET)
	public ModelAndView newuser() {

		ModelAndView model = new ModelAndView();				
		model.setViewName("newuser");
		return model;

	}
	
//	@Link(label="New User", family="UserController", parent = "User" )
	@RequestMapping(value="/newuser",method=RequestMethod.POST)
	public String addRecordUser(@Valid @ModelAttribute("userRole") UserRole userRole
			,BindingResult result
			,Principal principal,Model model,HttpServletRequest request){
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return "newuser";			
		}
		if(securityService.checkDuplicateUsername(userRole.getUser().getUsername()))
		{
			ObjectError error = new ObjectError("username","Username already exist!");
			result.addError(error);
			model.addAttribute("errors", result.getAllErrors());
			return "newuser";	
		}
		
		User user=userRole.getUser();
		//*** Par Authentication Expired
//		Calendar cal = Calendar.getInstance(); 
//		cal.add(Calendar.MONTH, 3);
//		Date pwdEx=new Date();
//	    pwdEx=cal.getTime();
//	    user.setPwdExpiredDate(pwdEx);
	    //*** Attempt Log
	    user.setAttemptNo((byte)(0));
	    user.setUsername(user.getUsername().trim());
	    //*** Operator	   	    	    
	    user.setCreatedBy(principal.getName());
	    user.setCreatedDate(new Date());
//	    Encoder Authentication
	    PasswordEncoder encoder=new BCryptPasswordEncoder();
	    user.setPwd(encoder.encode(user.getPwd()));
	    user.setEnabled(true);
	    
	    userRole.setCreatedDate(new Date());
	    userRole.setCreatedBy(principal.getName());
	    userRole.setRole(securityService.findRoleById(userRole.getRole().getId()));
	    userRole.setUser(user);
	    
	    user.getUserRoles().add(userRole);
	    securityService.userSave(user);
	    
	    UserHistory userHistory=new UserHistory();
		userHistory.setUserIdPk(user.getId());
		userHistory.setPwd(user.getPwd());
		userHistory.setDescription("Initial Password!");
		userHistory.setCreatedBy(principal.getName());
		userHistory.setCreatedDate(new Date());
		securityService.userHistorySave(userHistory);
	    
	    String info="create user " + user.getUsername() +" as " + userRole.getRole().getRoleName();
	    securityService.saveLogWithInfo(request, info, principal.getName());
	    
		return "redirect:/user";
	}
	
	@Transactional(readOnly=true)
//	@Link(label="Edit User", family="UserController", parent = "User" )
	@RequestMapping(value =  "/user/{username}/edit", method = RequestMethod.GET)
	public String edituser(@PathVariable("username") String username,Model model,Principal principal) {
		User user=securityService.findUserByUsername(username);	
		if(user==null) throw new ResourceNotFoundException();
		UserRole userRole=user.getUserRoles().iterator().next();
		
		if(checkAuthority("ACCESS_MANUSRLOW")){
			User curUser=securityService.findUserByUsername(principal.getName());			
			UserRole curUserRole=curUser.getUserRoles().iterator().next();
			//current user can edit only user that have role lv lower than him
			if(userRole.getRole().getRoleLevel()>=curUserRole.getRole().getRoleLevel())
				throw new ResourceNotFoundException();
		}
		model.addAttribute(userRole);						
		return "edituser";

	}
	
	@Transactional
//	@Link(label="Edit User", family="UserController", parent = "User" )
	@RequestMapping(value =  "/user/{username}/edit", method = RequestMethod.POST)
	public String processEditUser(@PathVariable("username") String username,@Valid @ModelAttribute("userRole") UserRole userRole
			,BindingResult result,Model model,Principal principal,HttpServletRequest request) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return "edituser";			
		}
		String info="update user:"+ username +";";
		User user=securityService.findUserByUsername(username);
		User updateUser=userRole.getUser();
//		Update user
		user.setFullName(updateUser.getFullName());
		user.setFullNameKh(updateUser.getFullNameKh());
		user.setEmail(updateUser.getEmail());
		user.setUserId(updateUser.getUserId());
		user.setGender(updateUser.getGender());
		
		user.setPwdExpiredDate(updateUser.getPwdExpiredDate());		
		user.setAccountExpiredDate(updateUser.getAccountExpiredDate());
		user.setMaxAttemptNo(updateUser.getMaxAttemptNo());
//		clear user attampt when user is unlocked
		if(user.isLocked() && !updateUser.isLocked()){
			user.setAttemptNo((byte) 0);
		}
//		clear account lock date to null to prevent it auto unlock when user is manual lock
		if(user.isLocked() !=updateUser.isLocked()){
			user.setAccountLockedDate(null);
		}			
		if(!user.getUsername().equals(principal.getName())){
			user.setLocked(updateUser.isLocked());
			//when re-enable user reset last login date
			if(!user.isEnabled() && updateUser.isEnabled()){
				user.setLastLoginDate(null);				
			}
			//log when user is enable or disable
			if(user.isEnabled()!=updateUser.isEnabled()){
				securityService.saveLogWithInfo(request, username+" has been " +(updateUser.isEnabled()==true?"enabled":"disabled"), principal.getName());
			}
			user.setEnabled(updateUser.isEnabled());			
			user.setFirstLogin(updateUser.isFirstLogin());
		}
		
		user.setCreatedBy(principal.getName());
		user.setCreatedDate(new Date());
		
	    for (UserRole iteUserRole : user.getUserRoles()) {
			if(iteUserRole.getId() == userRole.getId()){
				if(iteUserRole.getRole().getId()!=userRole.getRole().getId()){
					Role role=securityService.findRoleById(userRole.getRole().getId());
					info+="change role from "+iteUserRole.getRole().getRoleName()+" to " +role.getRoleName();
					iteUserRole.setRole(role);	
					iteUserRole.setLastUpdatedDate(new Date());
					iteUserRole.setLastUpdatedBy(principal.getName());
				}				
			}
		}	    
		securityService.userSave(user);
		securityService.saveLogWithInfo(request, info, principal.getName());
		return "redirect:/user";

	}
	
	@RequestMapping(value =  "/resetpwd", method = RequestMethod.POST)	
	public String processResetPassword(@RequestParam String newpwd,@RequestParam String username,Model model,Principal principal,HttpServletRequest request) {
		
		User user=securityService.userFindOne(username);				
		if(user==null) throw new ResourceNotFoundException();
		
		BCryptPasswordEncoder encoder=new BCryptPasswordEncoder();		
		user.setPwd(encoder.encode(newpwd));
		
		Calendar cal = Calendar.getInstance(); 
		cal.add(Calendar.MONTH, 3);
		Date pwdEx=new Date();
	    pwdEx=cal.getTime();
	    user.setPwdExpiredDate(pwdEx);
	    user.setCreatedDate(new Date());
	    user.setFirstLogin(true);
	    user.setCreatedBy(principal.getName());
	    securityService.userSave(user);
	    
	    UserHistory userHistory=new UserHistory();
		userHistory.setUserIdPk(user.getId());
		userHistory.setPwd(user.getPwd());
		userHistory.setDescription("Reset Password!");
		userHistory.setCreatedBy(principal.getName());
		userHistory.setCreatedDate(new Date());
		securityService.userHistorySave(userHistory);
		String info="user "+username+"'s password has been resetted!";
		securityService.saveLogWithInfo(request, info, principal.getName());
		return "redirect:/user/"+username+"/edit";
	}
		
	public boolean checkAuthority(String auth){
		Authentication auths= SecurityContextHolder.getContext().getAuthentication();		
		return auths.getAuthorities().contains(new SimpleGrantedAuthority(auth));
	}
	
	@RequestMapping(value = "/changepwd" , method = RequestMethod.POST)
	public String changePwd(ModelMap model, HttpServletRequest request, HttpServletResponse response,
			@Valid Pwd pwd, BindingResult result) {
	    if(result.hasErrors()) {
	    	model.put("pwd", new Pwd());
	    	model.put("pageTitle", "Update Credential");
	    	model.put("error", "Something went wrong. Please try again.");
	    	
	    	return "admin/changepwd";
	    }
	    
	    User user = userService.getUser(SecurityContextHolder.getContext().getAuthentication().getName());
	    String oldPwd = user.getPwd();
	    
	    boolean isPwdValid = true;
	    
	    // Check if old pwd is correct
	    if(!PwdEncoder.matchesPwd(pwd.getOldPwd(), oldPwd)) {
	    	isPwdValid = false;
	    }
	    
	    // Check if new pwd is strong enough
	    if(isPwdValid && !AnonymousHelper.isPwdValid(pwd.getConfirmPwd())){
	    	isPwdValid = false;
	    }
	    
	    // Check if new pwd is the same as confirmed pwd
	    if(isPwdValid && (pwd.getNewPwd().compareTo(pwd.getConfirmPwd()) != 0)){
	    	isPwdValid = false;
	    }
	    
	    // Check if new pwd is the same as old pwd
	    if(isPwdValid && PwdEncoder.matchesPwd(pwd.getConfirmPwd(), oldPwd)){
	    	isPwdValid = false;
	    }
	    
	    // Check if new pwd has been used before
	    if(isPwdValid && isPwdHasBeenUsed(pwd.getConfirmPwd())){
	    	isPwdValid = false;
	    }
	    
	    if(!isPwdValid){
	    	model.put("pwd", new Pwd());
	    	model.put("pageTitle", "Update Credential");
	    	model.put("error", "Something went wrong. Please try again.");
	    	
	    	return "admin/changepwd";
	    }
	    
	    Calendar cal = Calendar.getInstance(); 
		cal.add(Calendar.MONTH, 3);
		Date pwdEx=new Date();
	    pwdEx=cal.getTime();
	    String encodedPwd = PwdEncoder.encodePwd(pwd.getConfirmPwd());
	    user.setPwd(encodedPwd);
	    user.setFirstLogin(false);
	    user.setPwdExpiredDate(pwdEx);
	    
	    int affected = userService.updateUserPwd(user);
	    
	    if(affected > 0) {
	    	model.put("changepwd", "success");
	    	
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    	String username = auth.getName();
			String action = "change password successfully";
			String ip = request.getRemoteHost();
			String pip = request.getRemoteHost();
			Date date = new Date();
			
			logDao.logUserAction(username, action, ip, pip, date);
			logDao.logChangePwd(username, action, oldPwd, user.getLastUpdatedBy(), date);
			
//			new SecurityContextLogoutHandler().logout(request, response, auth);
			
			return "admin/changepwd";
	    }else {
	    	model.put("error", "Something went wrong. Please try again.");
	    	
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    	String username = auth.getName();
			String action = "change password failed";
			String ip = request.getRemoteHost();
			String pip = request.getRemoteHost();
			Date date = new Date();
			
			logDao.logUserAction(username, action, ip, pip, date);
			
			model.put("pwd", new Pwd());
	    	model.put("pageTitle", "Update Credential");
	    	
	    	return "admin/changepwd";
	    }
	}
	
	@RequestMapping(value = "/changepwd", method = RequestMethod.GET)
	public String getChangePwdPage(ModelMap model, @Valid Pwd pwd, BindingResult result) {
		if(result == null) {
			model.put("pwd", new Pwd());
		}
		
		model.put("pageTitle", "Update Credential");
		
		return "admin/changepwd";
	}
	
	@RequestMapping(value = "/checkUsedPwd", method = RequestMethod.POST)
	@ResponseBody
	public String checkUsedPwd(HttpServletRequest request) {
		boolean isNotUsed = false;
		String newPwd = request.getParameter("newPwd");
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		User user = userService.getUser(auth.getName());
		
		isNotUsed = !userService.isPwdAlreadyUsed(user.getId(), newPwd);
		
		return isNotUsed + "";
	}
	
	private boolean isPwdHasBeenUsed(String pwd){
		boolean isUsed = false;
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		User user = userService.getUser(auth.getName());
		
		isUsed = userService.isPwdAlreadyUsed(user.getId(), pwd);
		
		return isUsed;
	}
}