package com.pru.pruquote.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pru.pruquote.model.PendingAck;
import com.pru.pruquote.service.PendingAckService;

@Controller
@PreAuthorize("hasAuthority('ACCESS_PENDINGACK')")
public class PendingAckController {

	@Autowired
	private PendingAckService pendigAckService;

	@ResponseBody
	@RequestMapping(value = "/getpendingacks", method = RequestMethod.GET)
	public String getPendingAcks(Principal principal) {
		String result = "";
		String userid = principal.getName();
		ObjectMapper mapper = new ObjectMapper();
		List<PendingAck> getAllPendingAcks = pendigAckService.getListPendingAck(userid);
		try {
			result = mapper.writeValueAsString(getAllPendingAcks);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/countpendingacks", method = RequestMethod.GET)
	public String countPendingAcks(Principal principal) {
		String result = "";
		String userid = principal.getName();
		ObjectMapper mapper = new ObjectMapper();
		List<PendingAck> getAllPendingAcks = pendigAckService.getListPendingAck(userid);
		Long countpendingack = getAllPendingAcks.stream().count();
		try {
			result = mapper.writeValueAsString(countpendingack);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}
}