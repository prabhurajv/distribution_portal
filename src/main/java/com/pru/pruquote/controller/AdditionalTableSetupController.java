package com.pru.pruquote.controller;


import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.pru.pruquote.model.Discountrate;
import com.pru.pruquote.model.Itemitem;
import com.pru.pruquote.model.Itemtabl;
import com.pru.pruquote.model.Label;
import com.pru.pruquote.model.Mbrate;
import com.pru.pruquote.model.Medicalexam;
import com.pru.pruquote.model.Modalfactor;
import com.pru.pruquote.model.Premiumrate;
import com.pru.pruquote.model.Surrender;
import com.pru.pruquote.model.UABFactor;
import com.pru.pruquote.service.AdditionalTableSetupService;
import com.pru.pruquote.service.TableSetupService;



@Controller
@PreAuthorize("hasRole('ACCESS_TABLESETUP')")
public class AdditionalTableSetupController {	
	
	@Autowired	
	private AdditionalTableSetupService tableSetupService;
		
  
	@ModelAttribute("medicalexam")
    public Medicalexam medicalexamConstructor(){
    	return new Medicalexam();
    }
	@ModelAttribute("label")
    public Label labelConstructor(){
    	return new Label();
    }
	
	
//	Start Medical Exam

	@RequestMapping(value =  "/tablesetup/item/medicalexam", method = RequestMethod.GET)
	public String medicalExamList(Model model) {
		List<Medicalexam> medicalexams=tableSetupService.medicalExamFindAll();			
		model.addAttribute("medicalexams", medicalexams);												
		return "medicalexam";
	}
	

	@RequestMapping(value =  "/tablesetup/item/newmedicalexam", method = RequestMethod.GET)
	public String newMedicalExam(Model model) {
		return "editmedicalexam";
	}
	

	@RequestMapping(value =  "/tablesetup/item/newmedicalexam", method = RequestMethod.POST)
	public String processNewMedicalExam(@Valid @ModelAttribute("medicalexam") Medicalexam medicalexam
			,BindingResult result,Model model,Principal principal) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newMedicalExam(model);			
		}
		medicalexam.setSynDate(new Date());
		medicalexam.setCoUser(principal.getName());
		tableSetupService.medicalExamSave(medicalexam);
		return "redirect:/tablesetup/item/medicalexam";
	}
	
	@RequestMapping(value =  "/tablesetup/item/medicalexam/{medicalexamId}/edit", method = RequestMethod.GET)
	public String medicalexamEdit(
			@PathVariable("medicalexamId") Long medicalexamId,Model model) {		
		Medicalexam medicalexam=tableSetupService.medicalExamFindOne(medicalexamId);
		if(medicalexam==null) throw new ResourceNotFoundException();
		model.addAttribute("medicalexam", medicalexam);												
		return "editmedicalexam";
	}
	
	@RequestMapping(value =  "/tablesetup/item/medicalexam/{medicalexamId}/edit", method = RequestMethod.POST)
	public String processmedicalexamEdit(@PathVariable("medicalexamId") Long medicalexamId
			,@Valid @ModelAttribute("medicalexam") Medicalexam medicalexam
			,BindingResult result,Model model,Principal principal) {		
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return medicalexamEdit(medicalexamId,model);			
		}
		medicalexam.setSynDate(new Date());
		medicalexam.setCoUser(principal.getName());
		tableSetupService.medicalExamSave(medicalexam);
		return "redirect:/tablesetup/item/medicalexam";
	}
//	Start Label
	@RequestMapping(value =  "/tablesetup/item/label", method = RequestMethod.GET)
	public String labelList(Model model) {
		List<Label> labels=tableSetupService.labelFindAll();			
		model.addAttribute("labels", labels);												
		return "label";
	}
	
	@RequestMapping(value =  "/tablesetup/item/newlabel", method = RequestMethod.GET)
	public String newLabel(Model model) {
		return "editlabel";
	}
	
	@RequestMapping(value =  "/tablesetup/item/newlabel", method = RequestMethod.POST)
	public String processNewLabel(@Valid @ModelAttribute("label") Label label
			,BindingResult result,Model model,Principal principal) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newLabel(model);			
		}
		label.setSynDate(new Date());
		label.setCoUser(principal.getName());
		tableSetupService.labelSave(label);
		return "redirect:/tablesetup/item/label";
	}
	
	@RequestMapping(value =  "/tablesetup/item/label/{labelId}/edit", method = RequestMethod.GET)
	public String labelEdit(
			@PathVariable("labelId") Long labelId,Model model) {		
		Label label=tableSetupService.labelFindOne(labelId);
		if(label==null) throw new ResourceNotFoundException();
		model.addAttribute("label", label);												
		return "editlabel";
	}
	
	@RequestMapping(value =  "/tablesetup/item/label/{labelId}/edit", method = RequestMethod.POST)
	public String processlabelEdit(@PathVariable("labelId") Long labelId
			,@Valid @ModelAttribute("label") Label label
			,BindingResult result,Model model,Principal principal){		
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return labelEdit(labelId,model);			
		}
		label.setSynDate(new Date());
		label.setCoUser(principal.getName());
		tableSetupService.labelSave(label);
		return "redirect:/tablesetup/item/label";
	}
}
