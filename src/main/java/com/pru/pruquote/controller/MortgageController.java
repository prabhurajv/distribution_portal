package com.pru.pruquote.controller;


import java.security.Principal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.pru.pruquote.model.Occupation;
import com.pru.pruquote.model.PruquoteParm;
import com.pru.pruquote.model.QuoteOccupation;
import com.pru.pruquote.model.User;
import com.pru.pruquote.model.listPruquote;
import com.pru.pruquote.service.QuoteService;
import com.pru.pruquote.service.SecurityService;
import com.pru.pruquote.service.TableSetupService;
import com.pru.pruquote.utility.AnonymousHelper;
import com.pru.pruquote.utility.GlobalVar;
import com.pru.pruquote.service.ListQuoteService;
import com.pru.pruquote.service.LogService;
import com.pru.pruquote.service.OccupationService;
@Controller
@PreAuthorize("hasAuthority('ACCESS_MORTGAGE')")
public class MortgageController {	
	
	@Autowired	
	private QuoteService quoteService;
	
	@Autowired
	private ListQuoteService listQuoteService;
	
	@Autowired	
	private TableSetupService tableSetupService;
	
	@Autowired	
	private SecurityService securityService;
	
	@Autowired
	private OccupationService occupationService;
	
	@Autowired
	private LogService logService;
	
	@ModelAttribute("pruquoteparm")
    public PruquoteParm pruquoteparmConstructor(){
    	return new PruquoteParm();
    }
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
        binder.registerCustomEditor(Date.class,"commDate",new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class,"la1dateOfBirth",new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class,"syndate",new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class,"la2dateOfBirth",new CustomDateEditor(dateFormat, false));
    }
			
	@RequestMapping(value =  "/loanassure", method = RequestMethod.GET)
	public ModelAndView mortgageForm(Principal principal) {
		ModelAndView model = new ModelAndView();			
		
		model.addObject("mortgageList", quoteService.viewQuote("MTR%", principal.getName()));
		model.addObject("pageTitle", "Loan Assure");
		model.setViewName("quote/mortgage");
		return model;
	}
		
	
	@RequestMapping(value =  "/pruquote_mortgage_repkhmer/{quoteId}", method = RequestMethod.GET)
	public ModelAndView mortgagePreviewKhmer(@PathVariable("quoteId") Long quoteId,Principal principal, HttpServletRequest req) {
		ModelAndView model = new ModelAndView();
		List<PruquoteParm> pruquoteProduct=quoteService.findValidQuote(quoteId);
		String product="";
		product=pruquoteProduct.get(0).getProduct();
		
		
		List<listPruquote> listPruquoteParms=listQuoteService.listQuote(principal.getName(), Long.toString(quoteId));
		List<PruquoteParm> pruquoteParms=quoteService.findValidQuote(quoteId, product, principal.getName());
		
		if(listPruquoteParms.size()<=0 & pruquoteParms.size()<=0){
			throw new ResourceNotFoundException();
		}
		else if (listPruquoteParms.size()>=0){
			pruquoteParms=quoteService.findValidQuote(quoteId);
			//model.addObject("name","");
			//model.addObject("code","");
		}
		else {
			if(pruquoteParms.size()<=0) {
				throw new ResourceNotFoundException();
				}
			else{
				//User user=securityService.userFindOne(principal.getName());
				//model.addObject("name",user.getFullNameKH());
				//model.addObject("code",user.getUserId());	
			}
		}

		List<String> results=quoteService.generateMortgageKhmerQuote(quoteId, "Khmer","RN");
		model.addObject("header1",results.get(0));
		model.addObject("body1",results.get(1));
		model.addObject("body2",results.get(2));
		model.addObject("body3",results.get(3));
		model.addObject("body4",results.get(4));
		model.addObject("body5",results.get(5));
		model.addObject("serialno",results.get(6));

		User user=securityService.findUserByUsername(principal.getName());
		model.addObject("name",user.getFullNameKh());
		model.addObject("code",user.getUserId());
		model.setViewName("report/PruQuote_Mortgage_RepKhmer");
		
		model.addObject("quoteOcc", occupationService.getQuoteOccupationByQuoteId(quoteId));
		Occupation occ =   occupationService.getOccupationByEngName("Others");
		if (occ == null) {
			model.addObject("occOtherId",0);
		} else {
			model.addObject("occOtherId",occ.getId());	
		}
		
		model.addObject("version", GlobalVar.version);
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object onbehalf = attr.getRequest().getSession().getAttribute("isonbehalf");
		model.addObject("isonbehalf", onbehalf);
		attr.getRequest().getSession().removeAttribute("isonbehalf");
		
		if (onbehalf.equals("15")) {
			String action = "Quote Id = " + quoteId + "and On be half = " + "Yes";
			String ip = req.getRemoteHost();
			String pip = req.getRemoteHost();
			Date date = new Date();
			logService.logUserAction(user.getUserId(), action, ip, pip, date);
		}
		
		return model;
	}
	@RequestMapping(value =  "/pruquote_mortgage_replatin/{quoteId}", method = RequestMethod.GET)
	public ModelAndView mortgagePreviewLatin(@PathVariable("quoteId") Long quoteId,Principal principal, HttpServletRequest req) {
		ModelAndView model = new ModelAndView();
		List<PruquoteParm> pruquoteProduct=quoteService.findValidQuote(quoteId);
		String product="";
		product=pruquoteProduct.get(0).getProduct();
		
		List<listPruquote> listPruquoteParms=listQuoteService.listQuote(principal.getName(), Long.toString(quoteId));
		List<PruquoteParm> pruquoteParms=quoteService.findValidQuote(quoteId, product, principal.getName());
		if(listPruquoteParms.size()<=0 & pruquoteParms.size()<=0){
			throw new ResourceNotFoundException();
		}
		else if (listPruquoteParms.size()>=0){
			pruquoteParms=quoteService.findValidQuote(quoteId);
			//model.addObject("name","");
			//model.addObject("code","");
		}
		else {
			if(pruquoteParms.size()<=0) {
				throw new ResourceNotFoundException();
				}
			else{
				//User user=securityService.userFindOne(principal.getName());
				//model.addObject("name",user.getFullNameKH());
				//model.addObject("code",user.getUserId());	
			}
		}
		
		List<String> results=quoteService.generateMortgageLatinQuote(quoteId, "Latin","RN");
		
		model.addObject("header1",results.get(0));
		model.addObject("body1",results.get(1));
		model.addObject("body2",results.get(2));
		model.addObject("body3",results.get(3));		
		model.addObject("body4",results.get(4));
		model.addObject("body5",results.get(5));
		model.addObject("serialno",results.get(6));
		
		User user=securityService.findUserByUsername(principal.getName());
		model.addObject("name",user.getFullName());
		model.addObject("code",user.getUserId());
		
		model.addObject("quoteOcc", occupationService.getQuoteOccupationByQuoteId(quoteId));
		Occupation occ =   occupationService.getOccupationByEngName("Others");
		if (occ == null) {
			model.addObject("occOtherId",0);
		} else {
			model.addObject("occOtherId",occ.getId());	
		}
		
		
		model.setViewName("report/PruQuote_Mortgage_RepLatin");
		
		model.addObject("version", GlobalVar.version);
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object onbehalf = attr.getRequest().getSession().getAttribute("isonbehalf");
		model.addObject("isonbehalf", onbehalf);
		attr.getRequest().getSession().removeAttribute("isonbehalf");
		
		if (onbehalf.equals("15")) {
			String action = "Quote Id = " + quoteId + "and On be half = " + "Yes";
			String ip = req.getRemoteHost();
			String pip = req.getRemoteHost();
			Date date = new Date();
			logService.logUserAction(user.getUserId(), action, ip, pip, date);
		}
		
		return model;
	}
	
	@RequestMapping(value =  "/newloanassure", method = RequestMethod.GET)
	public String newMortgageForm(Model model) {
		model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
		model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%MTR1%"));
		model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%MTR1%"));
		model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
		model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtablePro((long)9,"%MTR1%"));
		model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
		model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
		model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
		model.addAttribute("LoanAssureType", tableSetupService.itemitemFindNameByItemItemID((long)230,(long)231,(long)99999));
		model.addAttribute("OccupationDdl", occupationService.getAllOccupationDdl());
		model.addAttribute("pageTitle", "New Loan Assure");
		
		return "quote/newmortgage";
	}
	@RequestMapping(value =  "/newloanassure", method = RequestMethod.POST, params="save")
	public String processNewMortgageForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,Model model,Principal principal,HttpServletRequest req) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newMortgageForm(model);
		}
		if(pruquoteparm.getProduct().equals("230")){
			pruquoteparm.setProduct("MTR1");	
		}else if(pruquoteparm.getProduct().equals("231")){
			pruquoteparm.setProduct("MTR2");
		}
//		String Name2=request.getParameter("la2name");
		
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);
		
		String occla1 = req.getParameter("occla1");
		if(!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1",1 ,principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}
		
		return "redirect:/loanassure";
	}
	
	@RequestMapping(value =  "/newloanassure", method = RequestMethod.POST, params="previewkh")
	public String processNewMortgageFormPreviewKh(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,Model model,Principal principal,HttpServletRequest request,RedirectAttributes redirectAttributes, HttpServletRequest req) {
		
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newMortgageForm(model);			
		}
		
		if(pruquoteparm.getProduct().equals("230")){
			pruquoteparm.setProduct("MTR1");	
		}else if(pruquoteparm.getProduct().equals("231")){
			pruquoteparm.setProduct("MTR2");
		}
		
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);
		
		String occla1 = req.getParameter("occla1");
		if(!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1",1 ,principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

		/*if(pruquoteparm.getCoL1Rtr2()==15){
			Object[] prusaver=(Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
			if(prusaver[3].equals("FALSE")){
				redirectAttributes.addFlashAttribute("errorPruSaver","Prusaver MB should between "+prusaver[1]+"-"+prusaver[2]);
				return "redirect:/mortgage/"+pruquoteparm.getId()+"/edit";			
			}			
		}*/
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/pruquote_mortgage_repkhmer/"+pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview Mortgage Khmer Quote No " +pruquoteparm.getId(), principal.getName());
		return "redirect:/loanassure/"+pruquoteparm.getId()+"/edit";

	}
	
	@RequestMapping(value =  "/newloanassure", method = RequestMethod.POST, params="previewen")
	public String processNewMortgageFormPreviewEn(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,Model model,Principal principal,HttpServletRequest request,RedirectAttributes redirectAttributes, HttpServletRequest req) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			return newMortgageForm(model);			
		}
		if(pruquoteparm.getProduct().equals("230")){
			pruquoteparm.setProduct("MTR1");	
		}else if(pruquoteparm.getProduct().equals("231")){
			pruquoteparm.setProduct("MTR2");
		}
		
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);
		
		String occla1 = req.getParameter("occla1");
		if(!occla1.equals("")) {
			QuoteOccupation quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1",1 ,principal.getName(), new Date());
			occupationService.SaveQuoteOccupation(quoteOcc);
		}

//		if(pruquoteparm.getCoL1Rtr2()==15){
//			Object[] prusaver=(Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
//			if(prusaver[3].equals("FALSE")){
//				redirectAttributes.addFlashAttribute("errorPruSaver","Prusaver MB should between "+prusaver[1]+"-"+prusaver[2]);
//				return "redirect:/loanassure/"+pruquoteparm.getId()+"/edit";			
//			}			
//		}
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/pruquote_mortgage_replatin/"+pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview Mortgage English Quote No " +pruquoteparm.getId(), principal.getName());
		return "redirect:/loanassure/"+pruquoteparm.getId()+"/edit";

	}
	
	@RequestMapping(value =  "/loanassure/{id}/edit", method = RequestMethod.GET)
	public String editMortgage(@PathVariable("id") Long id,Model model) {
		PruquoteParm pruquoteParm=quoteService.getValidQuote(id, SecurityContextHolder.getContext().getAuthentication().getName());
		if(pruquoteParm==null) throw new ResourceNotFoundException();
		model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
		model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%MTR1%"));
		model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%MTR1%"));
		model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
		model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtablePro((long)9,"%MTR1%"));
		model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
		model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
		model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
		model.addAttribute("LoanAssureType", tableSetupService.itemitemFindNameByItemItemID((long)230,(long)231,(long)99999));
		model.addAttribute("occupations", occupationService.getAllOccupationDdl());
		
		if(pruquoteParm.getProduct().equals("MTR1")){
			pruquoteParm.setProduct("230");
		}else{
			pruquoteParm.setProduct("231");
		}
		model.addAttribute("pruquoteparm",pruquoteParm);
		model.addAttribute("pageTitle", "Edit Loan Assure");
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		Object quoteUrl = attr.getRequest().getSession().getAttribute("previewurl");
		if(quoteUrl != null){
			model.addAttribute("quoteUrl", quoteUrl.toString());	
		}
		attr.getRequest().getSession().removeAttribute("previewurl");
		
		List<QuoteOccupation> qcs = occupationService.getQuoteOccupationByQuoteId(pruquoteParm.getId());
		for (QuoteOccupation qc : qcs) {
			String tmpRemark = qc.getRemark();
			if (tmpRemark.equals("occla1")) {
				model.addAttribute("occla1selected",qc.getOccId());	
			}
		}
		
		return "quote/editmortgage";
	}
	@RequestMapping(value =  "/loanassure/{id}/edit", method = RequestMethod.POST, params="save")
	public String processEditMortgageForm(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,@PathVariable("id") Long id,Model model,Principal principal, HttpServletRequest req) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%MTR1%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%MTR1%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtablePro((long)9,"%MTR1%"));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
			model.addAttribute("LoanAssureType", tableSetupService.itemitemFindNameByItemItemID((long)230,(long)231,(long)99999));
			return "quote/editmortgage";			
		}
		if(pruquoteparm.getProduct().equals("230")){
			pruquoteparm.setProduct("MTR1");	
		}else if(pruquoteparm.getProduct().equals("231")){
			pruquoteparm.setProduct("MTR2");
		}
		
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);
		
		String occla1 = req.getParameter("occla1");
		
		if(!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(), "occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1",1 ,principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);			
		}
		
		return "redirect:/loanassure";
	}
	@RequestMapping(value =  "/loanassure/{id}/edit", method = RequestMethod.POST, params="previewkh")
	public String processEditMortgageFormKh(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,@PathVariable("id") Long id,Model model,Principal principal,HttpServletRequest request,RedirectAttributes redirectAttributes, HttpServletRequest req) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%MTR1%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%MTR1%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtablePro((long)9,"%MTR1%"));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
			model.addAttribute("LoanAssureType", tableSetupService.itemitemFindNameByItemItemID((long)230,(long)231,(long)99999));
			return "quote/editmortgage";						
		}
		
		if(pruquoteparm.getProduct().equals("230")){
			pruquoteparm.setProduct("MTR1");	
		}else if(pruquoteparm.getProduct().equals("231")){
			pruquoteparm.setProduct("MTR2");
		}
		
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);
		
		String occla1 = req.getParameter("occla1");
		
		if(!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(), "occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1",1 ,principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);			
		}

//		if(pruquoteparm.getCoL1Rtr2()==15){
//			Object[] prusaver=(Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
//			if(prusaver[3].equals("FALSE")){
//				redirectAttributes.addFlashAttribute("errorPruSaver","Prusaver MB should between "+prusaver[1]+"-"+prusaver[2]);
//				return "redirect:/mortgage/"+pruquoteparm.getId()+"/edit";			
//			}			
//		}
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/pruquote_mortgage_repkhmer/"+pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview Mortgage Khmer Quote No " +pruquoteparm.getId(), principal.getName());
		return "redirect:/loanassure/"+pruquoteparm.getId()+"/edit";
		
	

	}
	@RequestMapping(value =  "/loanassure/{id}/edit", method = RequestMethod.POST, params="previewen")
	public String processEditMortgageFormEn(@Valid @ModelAttribute("pruquoteparm") PruquoteParm pruquoteparm
			,BindingResult result,@PathVariable("id") Long id,Model model,Principal principal,HttpServletRequest request,RedirectAttributes redirectAttributes, HttpServletRequest req) {
		if(result.hasErrors()){
			model.addAttribute("errors", result.getAllErrors());
			model.addAttribute("RelationShip", tableSetupService.itemitemFindNameByItemtable((long)1));
			model.addAttribute("PolicyTerm", tableSetupService.itemitemFindNameByItemtablePro((long)3,"%MTR1%"));
			model.addAttribute("PremiumTerm", tableSetupService.itemitemFindNameByItemtablePro((long)4,"%MTR1%"));
			model.addAttribute("PaymentType", tableSetupService.itemitemFindNameByItemtable((long)8));
			model.addAttribute("PaymentMode", tableSetupService.itemitemFindNameByItemtablePro((long)9,"%MTR1%"));
			model.addAttribute("YesNo", tableSetupService.itemitemFindNameByItemtable((long)5));
			model.addAttribute("Sex", tableSetupService.itemitemFindNameByItemtable((long)2));
			model.addAttribute("occupationClass", tableSetupService.itemitemFindNameByItemtable((long)7));
			model.addAttribute("LoanAssureType", tableSetupService.itemitemFindNameByItemItemID((long)230,(long)231,(long)99999));
			return "quote/editmortgage";					
		}
		if(pruquoteparm.getProduct().equals("230")){
			pruquoteparm.setProduct("MTR1");	
		}else if(pruquoteparm.getProduct().equals("231")){
			pruquoteparm.setProduct("MTR2");
		}
		
		pruquoteparm = AnonymousHelper.sanitizedPruquoteParm(pruquoteparm);
		pruquoteparm.setSynDate(new Date());
		pruquoteparm.setCoUser(principal.getName());
		quoteService.pruQuoteParamSave(pruquoteparm);
		
		String occla1 = req.getParameter("occla1");
		
		if(!occla1.equals("") && !occla1.equals("0")) {
			QuoteOccupation quoteOcc = occupationService.getQuoteOccupationByQuoteandRemark(pruquoteparm.getId(), "occla1");
			if (quoteOcc == null) {
				quoteOcc = new QuoteOccupation(pruquoteparm.getId(), Integer.parseInt(occla1), "occla1",1 ,principal.getName(), new Date());
			} else {
				quoteOcc.setOccId(Integer.parseInt(occla1));
			}
			occupationService.SaveQuoteOccupation(quoteOcc);			
		}

//		if(pruquoteparm.getCoL1Rtr2()==15){
//			Object[] prusaver=(Object[]) quoteService.prusaverPremiumValidate(pruquoteparm.getId()).get(0);
//			if(prusaver[3].equals("FALSE")){
//				redirectAttributes.addFlashAttribute("errorPruSaver","Prusaver MB should between "+prusaver[1]+"-"+prusaver[2]);
//				return "redirect:/mortgage/"+pruquoteparm.getId()+"/edit";			
//			}			
//		}
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		attr.getRequest().getSession().setAttribute("previewurl", "/pruquote_mortgage_replatin/"+pruquoteparm.getId());
		attr.getRequest().getSession().setAttribute("isonbehalf", req.getParameter("onbehalf"));
		securityService.saveLogWithInfo(request, "Preview Mortgage English Quote No " +pruquoteparm.getId(), principal.getName());
		return "redirect:/loanassure/"+pruquoteparm.getId()+"/edit";

	}
	
	@RequestMapping(value = "/loanassure/generateDisbursedAmount", method = RequestMethod.POST)
	@ResponseBody
	public String GetDisbursedAmount(HttpServletRequest request)
	{
		double disbursedAmount = 0;
		
		String paymentType = request.getParameter("coPaymentType");
		String product = request.getParameter("product");
		String policyTerm = request.getParameter("coPolicyTerm");
		String sex =  request.getParameter("coLa1sex");
		String loanAmount = request.getParameter("loanAmount");
		String dob = request.getParameter("la1dateOfBirth");
		String occ = request.getParameter("commDate");
		try {
			dob = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(dob));
			occ = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd/MM/yyyy").parse(occ));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String isFundedByBank = request.getParameter("isFundedByBank");
		
		if(loanAmount.length() >= 5){
			disbursedAmount = quoteService.GetMortgageDisbursedAmount(paymentType, product, policyTerm, sex,
					loanAmount, dob, occ, isFundedByBank);
		}
		
		NumberFormat formatter = new DecimalFormat("#0.00");

		return formatter.format(disbursedAmount);
	}
	
}