package com.pru.pruquote.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.Branch;
import com.pru.pruquote.utility.GlobalVar;

@Service
public class BranchService {
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Branch> listBranchs() {
		Query query = sessionFactory.getCurrentSession().createQuery("FROM Branch");
		List<Branch> listBranchs = query.list();
		return listBranchs;
	}
	
	@Transactional
	public Object getSingleObject(String table,String fields ,String field, Object  param) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("SELECT "+ fields +" FROM "+ GlobalVar.db_name +"."+ table +" WHERE "+ field +" = :" + field)
				.setParameter(field, param);
		Object obj = query.uniqueResult();
		return obj;
	}
}
