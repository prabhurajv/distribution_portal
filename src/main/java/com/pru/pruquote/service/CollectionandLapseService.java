package com.pru.pruquote.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.CollectionAndLapse;
import com.pru.pruquote.model.CollectionandLapseComment;
import com.pru.pruquote.utility.GlobalVar;

@Service
public class CollectionandLapseService {
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private SessionFactory sessionFactory;

	private String getCollectionQuery() {
		String sql = "select" + "  obj_key as \"objKey\"" + ", obj_type as \"objType\"" + ", chdrnum as \"chdrNum\""
				+ ", chdr_appnum as \"chdrAppnum\"" + ", po_name as \"poName\"" + ", gender as \"gender\""
				+ ", clnt_mobile1 as \"clntMobile1\"" + ", clnt_mobile2 as \"clntMobile2\" "
				+ ", chdr_p_with_tax as \"chdrPwithTax\" " + ", chdr_billfreq as \"chdrBillfreq\" "
				+ ", chdr_due_date as \"chdrDueDate\" " + ", chdr_graceperiod as \"chdrGraceperiod\" "
				+ ", agntnum as \"agntNum\" " + ", agnt_name as \"agntName\" " + ", agnt_supcode as \"agntSupcode\" "
				+ ", agnt_supname as \"agntSupname\" " + ", chdr_age as \"chdrAge\" " + ", cc_status as \"ccStatus\" "
				+ ", agnt_status as \"agntStatus\" " + ", chdr_ape as \"chdrApe\" "
				+ ", chdr_lapse_date as \"chdrLapseDate\" " + ", chdr_status as \"chdrStatus\" "
				+ ", is_impact_persistency as \"isImpactPersistency\" " + ", lapsed_reason as \"lapsedReason\" "
				+ ", bcu_remark as \"bcuRemark\" " + ", customer_response as \"customerResponse\" "
				+ ", branch_code as \"branchCode\" " + ", branch_name as \"branchName\" "
				+ ", cc_remark as \"ccRemark\" " + ", bdmcode " + ", bdmname " + ", sbdmcode " + ", sbdmname "
				+ ", rbdmcode " + ", rbdmname " + ", salezone " + ", product " + ", sub_lapsed_reason " + ", occ_date "
				+ ", next_prem " + ", tele_reinstate " + ", out_amt " + ", payment_method " + ", clnt_mobile3 "
				+ ", next_payment_date " + ", stnd_ord_end_date " + ", bank_acc " + ", so_cancel_status " + ", ben "
				+ ", po_occ " + ", val_la_and_su " + "from " + GlobalVar.db_name + ".spsearch_collection(:userid)";

		return sql;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Cacheable(value = "listcollectionandlapse")
	public List<CollectionAndLapse> getListCollectionandLapse(String userid) {

		String sql = getCollectionQuery();

		Query query = sessionFactory.getCurrentSession().createSQLQuery(sql).setParameter("userid", userid)
				.setResultTransformer(Transformers.aliasToBean(CollectionAndLapse.class));

		List<CollectionAndLapse> listCollectionAndLapses = (List<CollectionAndLapse>) query.list();

		return listCollectionAndLapses;
	}
	
	//TODO: Add caching for persistencies
	@SuppressWarnings("unchecked")
	@Transactional
	@Cacheable(value = "collectionAndLapsePODecision")
	public List<CollectionAndLapse> getListCollectionandLapseFilterByPODecision(String userid, String poDecision) {

		String sql = getCollectionQuery();

		// Enhance enhance immediate action 14-02-2019 by Malen Sok
		// NOTE reserve1 First sub string is Call reason code and second is policy
		// status argument and reserve2 is number of day
		sql += " ,ap.tabitemitem i where i.cotabl=29 " + " and i.validflag=1 "
				+ " and podcsn=split_part(i.reserve1, '|', 1) "
				+ " and chdr_status like ('%' || split_part(i.reserve1, '|', 2) || '%')"
				+ " and extract(year from age(call_date, now()))*12 + extract(month from age(call_date, now())) = 0"
				+ " and i.itemkhmer = '" + poDecision + "'";

		Query query = sessionFactory.getCurrentSession().createSQLQuery(sql).setParameter("userid", userid)
				.setResultTransformer(Transformers.aliasToBean(CollectionAndLapse.class));

		List<CollectionAndLapse> listCollectionAndLapses = (List<CollectionAndLapse>) query.list();

		return listCollectionAndLapses;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Cacheable(value = "listPersistencies")
	public List<CollectionAndLapse> getListPersistencies(String userid, String term) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select" + "  obj_key as \"objKey\""
				+ ", obj_type as \"objType\"" + ", chdrnum as \"chdrNum\"" + ", chdr_appnum as \"chdrAppnum\""
				+ ", po_name as \"poName\"" + ", gender as \"gender\"" + ", clnt_mobile1 as \"clntMobile1\""
				+ ", clnt_mobile2 as \"clntMobile2\" " + ", chdr_p_with_tax as \"chdrPwithTax\" "
				+ ", chdr_billfreq as \"chdrBillfreq\" " + ", chdr_due_date as \"chdrDueDate\" "
				+ ", chdr_graceperiod as \"chdrGraceperiod\" " + ", agntnum as \"agntNum\" "
				+ ", agnt_name as \"agntName\" " + ", agnt_supcode as \"agntSupcode\" "
				+ ", agnt_supname as \"agntSupname\" " + ", chdr_age as \"chdrAge\" " + ", cc_status as \"ccStatus\" "
				+ ", agnt_status as \"agntStatus\" " + ", ape_exc_tax_chg as \"chdrApe\" "
				+ ", chdr_lapse_date as \"chdrLapseDate\" " + ", chdr_status as \"chdrStatus\" "
				+ ", is_impact_persistency as \"isImpactPersistency\" " + ", lapsed_reason as \"lapsedReason\" "
				+ ", bcu_remark as \"bcuRemark\" " + ", customer_response as \"customerResponse\" "
				+ ", branch_code as \"branchCode\" " + ", branch_name as \"branchName\" "
				+ ", cc_remark as \"ccRemark\" " + ", bdmcode " + ", bdmname " + ", sbdmcode " + ", sbdmname "
				+ ", rbdmcode " + ", rbdmname " + ", salezone " + ", product " + ", sub_lapsed_reason " + ", occ_date "
				+ ", next_prem " + ", tele_reinstate " + ", out_amt " + ", payment_method " + ", clnt_mobile3 "
				+ ", next_payment_date " + ", stnd_ord_end_date " + ", bank_acc " + ", so_cancel_status " + ", ben "
				+ ", po_occ " + ", val_la_and_su " + "from " + GlobalVar.db_name
				+ ".spsearch_collection(:userid) where  val_la_and_su=1 and chdr_status in ('LA','SU','TR') and is_impact_persistency like ('%"
				+ term + "%')").setParameter("userid", userid)
				.setResultTransformer(Transformers.aliasToBean(CollectionAndLapse.class));

		List<CollectionAndLapse> listCollectionAndLapses = (List<CollectionAndLapse>) query.list();

		return listCollectionAndLapses;
	}

	@Transactional
	public int getCountPersistencies(String userid, String term) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select cast(count(*) as int) " + "from "
				+ GlobalVar.db_name
				+ ".spsearch_collection(:userid) where val_la_and_su=1 and chdr_status in ('LA','SU','TR') and is_impact_persistency like ('%"
				+ term + "%') " + "and (case when :userid like '69%' then agnt_status='ACTIVE' ELSE 1=1 END)")
				.setParameter("userid", userid);

		return (int) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<CollectionandLapseComment> listComment(String objkey, String objtype) {
		Query query = sessionFactory.getCurrentSession()
				.createSQLQuery("SELECT * FROM ap.splist_comments(:objkey ,:objtype)").setParameter("objkey", objkey)
				.setParameter("objtype", objtype);

		List<CollectionandLapseComment> listComments = query.list();
		return listComments;
	}

	@Transactional
	public void AddCollectionandLapseComment(CollectionandLapseComment clc) {
		sessionFactory.getCurrentSession().save(clc);
	}
}