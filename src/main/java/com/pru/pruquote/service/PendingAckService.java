package com.pru.pruquote.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.PendingAck;
import com.pru.pruquote.utility.GlobalVar;

@Service
public class PendingAckService {
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Cacheable(value = "listpendingack")
	public List<PendingAck> getListPendingAck(String userid) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select" +
				 		"  obj_key as \"objKey\"" +
						", obj_type as \"objType\"" +
						", chdrnum as \"chdrNum\"" +
						", po_num as \"poNnum\"" +
						", po_name as \"poName\"" +
						", clnt_mobile1 as \"clntMobile1\" " +
						", clnt_mobile2 as \"clntMobile2\" " +
						", agntnum as \"agntNum\" " +
						", agnt_name as \"agntName\" " +
						", supervisor_name as \"supName\" " +
						", branch_code as \"branchCode\" " +
						", branch_name as \"branchName\" " +
						", pending_period as \"PendingPeriod\" " +
						"from "	+ GlobalVar.db_name + ".spsearch_pendingack(:userid)")
						.setParameter("userid", userid)
						.setResultTransformer(Transformers.aliasToBean(PendingAck.class));
		
		List<PendingAck> listPendingAck = (List<PendingAck>)query.list();
		
		return listPendingAck;
	}
}