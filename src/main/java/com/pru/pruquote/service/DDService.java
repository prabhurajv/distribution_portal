package com.pru.pruquote.service;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import javax.naming.Context;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.PruquoteParm;
import com.pru.pruquote.model.VpruquoteParm;
import com.pru.pruquote.repository.ListPruquoteParamRepository;
import com.pru.pruquote.repository.PruquoteParamRepository;

@Service
public class DDService {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	PruquoteParamRepository pruquoteParamRepository;

	@Autowired
	ListPruquoteParamRepository listPruquoteParamRepository;

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired	
	private QuoteService quoteService;
	
	@Transactional
	public Map<String, Object> generateDD(final Long quoteId, String product) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();

		Query query = sessionFactory.getCurrentSession()
				.createSQLQuery("select * from ap.vpruquote_parms vp where vp.id=:id").setParameter("id", quoteId);
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String, Object>> aliasToValueMapList = query.list();

		if (aliasToValueMapList.size() == 0)
			throw new Exception("Quote not found");

		Map<String, Object> params = aliasToValueMapList.get(0);

		String useId = params.get("couser").toString();
		String[] digits = useId.split("");
		String digit = digits[0] + digits[1];

		Boolean isLC = false, isFC = false;
		if (digit.equals("60")) {
			isLC = true;
		} else if (digit.equals("69")) {
			isFC = true;
		}

		result.put("isFC", isFC);
		result.put("isLC", isLC);

		String paymentmode = params.get("paymentmode").toString();
		BigDecimal rawpremium = null;
		
		if (product == "BTR5" || product == "BTR6")
		{
			Map<String, Object> generateQuote = quoteService.generateQuote(quoteId, "", "", product);
			if (paymentmode.equals("12")) {
				rawpremium =  (BigDecimal) generateQuote.get("totalmontlypremuimdiscount");
			}
			else if (paymentmode.equals("01")) {
				rawpremium =  (BigDecimal) generateQuote.get("totalyearlypremuimdiscount");
			}
		}
		
		if (product == "BTR4")
			rawpremium = this.calculateInitPremium(quoteId, paymentmode);

		if (product == "BTR3")
			rawpremium = this.calculateInitPremiumEasyLife(quoteId, paymentmode);

		if (product == "BTR1")
			rawpremium = this.calculateInitPremiumPruMyFamily(quoteId, paymentmode);

		if (rawpremium == null)
			throw new Exception("Premium not found");

		BigDecimal initpremium = rawpremium;
		if (paymentmode.equals("12")) {
			initpremium = rawpremium.multiply(new BigDecimal(3));
		}

		result.put("initPremiumAmount", "US$ " + String.format("%,.2f", initpremium));
		result.put("nextPremiumAmount", "US$ " + String.format("%,.2f", rawpremium));

		result.put("periodAsYear", params.get("premiumterm").toString());

		Date paymentDate = (Date) params.get("commdate");
		Calendar paymentCal = Calendar.getInstance();
		paymentCal.setTime(paymentDate);

		result.put("paymentYear", paymentCal.get(Calendar.YEAR));
		result.put("paymentMonth", String.format("%02d", paymentCal.get(Calendar.MONTH) + 1));
		result.put("paymentDay", String.format("%02d", paymentCal.get(Calendar.DAY_OF_MONTH)));

		Date fromDate = paymentDate;
		Calendar fromCal = Calendar.getInstance();
		fromCal.setTime(fromDate);

		if (paymentmode.equals("12")) {
			fromCal.add(Calendar.MONTH, 3);
		} else if (paymentmode.equals("02")) {
			fromCal.add(Calendar.MONTH, 6);
		} else if (paymentmode.equals("01")) {
			fromCal.add(Calendar.YEAR, 1);
		}

		result.put("fromYear", fromCal.get(Calendar.YEAR));
		result.put("fromMonth", String.format("%02d", fromCal.get(Calendar.MONTH) + 1));
		result.put("fromDay", String.format("%02d", fromCal.get(Calendar.DAY_OF_MONTH)));

		Calendar toCal = Calendar.getInstance();
		toCal.setTime(paymentDate);
		toCal.add(Calendar.YEAR, Integer.parseInt(params.get("premiumterm").toString()));
		if (paymentmode.equals("12")) {
			toCal.add(Calendar.MONTH, -1);
		} else if (paymentmode.equals("02")) {
			toCal.add(Calendar.MONTH, -6);
		} else if (paymentmode.equals("01")) {
			toCal.add(Calendar.YEAR, -1);
		}

		result.put("toYear", toCal.get(Calendar.YEAR));
		result.put("toMonth", String.format("%02d", toCal.get(Calendar.MONTH) + 1));
		result.put("toDay", String.format("%02d", toCal.get(Calendar.DAY_OF_MONTH)));

		boolean isAmonth = false, isSixMonth = false, isAYear = false;

		if (paymentmode.equals("12")) {
			isAmonth = true;
		} else if (paymentmode.equals("02")) {
			isSixMonth = true;
		} else if (paymentmode.equals("01")) {
			isAYear = true;
		}

		result.put("isAmonth", isAmonth);
		result.put("isSixMonth", isSixMonth);
		result.put("isAYear", isAYear);
		return result;
	}

	@Transactional
	public BigDecimal calculateInitPremium(final Long quoteId, final String paymentType) {
		BigDecimal res = sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<BigDecimal /* objectType returned */>() {
					@Override
					/* or object type you need to return to process */
					public BigDecimal execute(Connection conn) throws SQLException {
						PreparedStatement cstmt = conn.prepareStatement(
								"select * from ap.spgen_edusmart(?,?,'RN','R1','R2','R3','R4','R5','R6','R7','R8')",
								ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

						cstmt.setLong(1, quoteId);
						cstmt.setString(2, "khmer");
						// cstmt.setString(3, result);
						// Result list that would return ALL rows of ALL result sets
						ResultSet resultSet = null;
						ResultSet oDT3 = null;
						BigDecimal premium = null;
						try {
							resultSet = cstmt.executeQuery();
							// #region "lblHeader1"
							resultSet.next();
							resultSet.next();
							if (resultSet.next()) {
								oDT3 = (ResultSet) resultSet.getObject(1);
							}
							if (oDT3.first()) {
								while (oDT3.next()) {
									if (oDT3.getString("Product").equals("TT")) {
										if (oDT3.isLast()) {
											if (paymentType.equals("02")) {
												premium = oDT3.getBigDecimal("P02_Amt");
												// premuim = String.format("%,.2f", oDT3.getBigDecimal("P02_Amt"));
											} else if (paymentType.equals("12")) {
												premium = oDT3.getBigDecimal("P12_Amt");
												// premuim = String.format("%,.2f", oDT3.getBigDecimal("P12_Amt"));
											} else if (paymentType.equals("01")) {
												premium = oDT3.getBigDecimal("P01_Amt");
												// premuim = String.format("%,.2f", oDT3.getBigDecimal("P01_Amt"));
											}
											// premuim = ;
											// String p02 = String.format("%,.2f", oDT3.getBigDecimal("P02_Amt"));
											// String p12 = String.format("%,.2f", oDT3.getBigDecimal("P12_Amt"));
											// premuim = String.format("%,.2f", oDT3.getBigDecimal("P01_Amt"));
										}
									}
								}

								oDT3.beforeFirst();

							}
							oDT3.close();

						} finally {
							cstmt.close();
						}
						return premium; // this should contain All rows or objects you need for further processing
					}
				});
		return res;

	}

	@Transactional
	public BigDecimal calculateInitPremiumEasyLife(final Long quoteId, final String paymentType) {
		BigDecimal res = sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<BigDecimal /* objectType returned */>() {
					@Override
					/* or object type you need to return to process */
					public BigDecimal execute(Connection conn) throws SQLException {
						PreparedStatement cstmt = conn.prepareStatement(
								"select * from ap.spgen_pruquote_easylife(?,?,'RN','R1','R2','R3','R4','R5','R6','R7','R8')",
								ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

						cstmt.setLong(1, quoteId);
						cstmt.setString(2, "khmer");
						// cstmt.setString(3, result);
						// Result list that would return ALL rows of ALL result sets
						ResultSet resultSet = null;
						ResultSet oDT3 = null;
						BigDecimal premium = null;
						try {
							resultSet = cstmt.executeQuery();
							// #region "lblHeader1"
							resultSet.next();
							resultSet.next();
							if (resultSet.next()) {
								oDT3 = (ResultSet) resultSet.getObject(1);
							}
							if (oDT3.first()) {
								while (oDT3.next()) {
									if (oDT3.getString("Product").equals("TT")) {
										if (oDT3.isLast()) {
											if (paymentType.equals("02")) {
												premium = oDT3.getBigDecimal("P02_Amt");
												// premuim = String.format("%,.2f", oDT3.getBigDecimal("P02_Amt"));
											} else if (paymentType.equals("12")) {
												premium = oDT3.getBigDecimal("P12_Amt");
												// premuim = String.format("%,.2f", oDT3.getBigDecimal("P12_Amt"));
											} else if (paymentType.equals("01")) {
												premium = oDT3.getBigDecimal("P01_Amt");
												// premuim = String.format("%,.2f", oDT3.getBigDecimal("P01_Amt"));
											}
											// premuim = ;
											// String p02 = String.format("%,.2f", oDT3.getBigDecimal("P02_Amt"));
											// String p12 = String.format("%,.2f", oDT3.getBigDecimal("P12_Amt"));
											// premuim = String.format("%,.2f", oDT3.getBigDecimal("P01_Amt"));
										}
									}
								}

								oDT3.beforeFirst();

							}
							oDT3.close();

						} finally {
							cstmt.close();
						}
						return premium; // this should contain All rows or objects you need for further processing
					}
				});
		return res;
	}

	@Transactional
	public BigDecimal calculateInitPremiumPruMyFamily(final Long quoteId, final String paymentType) {
		BigDecimal res = sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<BigDecimal /* objectType returned */>() {
					@Override
					/* or object type you need to return to process */
					public BigDecimal execute(Connection conn) throws SQLException {

						PreparedStatement cstmt = conn.prepareStatement(
								"select * from ap.spGen_PruQuote(?,?,'RN','R1','R2','R3','R4','R5','R6','R7','R8')",
								ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

						cstmt.setLong(1, quoteId);
						cstmt.setString(2, "khmer");
						// cstmt.setString(3, result);
						// Result list that would return ALL rows of ALL result sets
						ResultSet resultSet = null;
						ResultSet oDT3 = null;
						BigDecimal premium = null;
						try {
							resultSet = cstmt.executeQuery();
							// #region "lblHeader1"
							resultSet.next();
							resultSet.next();
							if (resultSet.next()) {
								oDT3 = (ResultSet) resultSet.getObject(1);
							}
							if (oDT3.first()) {
								while (oDT3.next()) {
									if (oDT3.getString("Product").equals("TT")) {
										if (oDT3.isLast()) {
											if (paymentType.equals("02")) {
												premium = oDT3.getBigDecimal("P02_Amt");
												// premuim = String.format("%,.2f", oDT3.getBigDecimal("P02_Amt"));
											} else if (paymentType.equals("12")) {
												premium = oDT3.getBigDecimal("P12_Amt");
												// premuim = String.format("%,.2f", oDT3.getBigDecimal("P12_Amt"));
											} else if (paymentType.equals("01")) {
												premium = oDT3.getBigDecimal("P01_Amt");
												// premuim = String.format("%,.2f", oDT3.getBigDecimal("P01_Amt"));
											}
											// premuim = ;
											// String p02 = String.format("%,.2f", oDT3.getBigDecimal("P02_Amt"));
											// String p12 = String.format("%,.2f", oDT3.getBigDecimal("P12_Amt"));
											// premuim = String.format("%,.2f", oDT3.getBigDecimal("P01_Amt"));
										}
									}
								}

								oDT3.beforeFirst();

							}
							oDT3.close();

						} finally {
							cstmt.close();
						}
						return premium; // this should contain All rows or objects you need for further processing
					}
				});
		return res;

	}

	@Transactional
	public Map<String, Object> generateReinstatementService(final String objKey) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();

		Query query = sessionFactory.getCurrentSession()
				.createSQLQuery(
						"select agntnum,out_amt,next_prem,next_payment_date,prem_term,chdr_billfreq,stnd_ord_end_date"
								+ " from ap.tabcollection cl where cl.obj_key=:objKey")
				.setParameter("objKey", objKey);
		query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		List<Map<String, Object>> aliasToValueMapList = query.list();

		if (aliasToValueMapList.size() == 0)
			throw new Exception("Quote not found");

		Map<String, Object> params = aliasToValueMapList.get(0);
		// check lc or fc
		String useId = params.get("agntnum").toString();
		String[] digits = useId.split("");
		String digit = digits[0] + digits[1];

		Boolean isLC = false, isFC = false;
		if (digit.equals("60")) {
			isLC = true;
		} else if (digit.equals("69")) {
			isFC = true;
		}

		result.put("isFC", isFC);
		result.put("isLC", isLC);

		// out standing amount & next prem
		String outAmount = params.get("out_amt").toString();
		String nextPrem = params.get("next_prem").toString();
		result.put("initPremiumAmount","US$ "+ String.format("%,.2f", Double.parseDouble(outAmount)));
		result.put("nextPremiumAmount","US$ "+ String.format("%,.2f", Double.parseDouble(nextPrem)));

		// frequency
		String paymentmode = params.get("chdr_billfreq").toString();

		boolean isAmonth = false, isSixMonth = false, isAYear = false;

		if (paymentmode.equals("12")) {
			isAmonth = true;
		} else if (paymentmode.equals("02")) {
			isSixMonth = true;
		} else if (paymentmode.equals("01")) {
			isAYear = true;
		}

		result.put("isAmonth", isAmonth);
		result.put("isSixMonth", isSixMonth);
		result.put("isAYear", isAYear);

		// prem term
		result.put("periodAsYear", params.get("prem_term").toString());
		// payment date
		DateFormat paymentDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar paymentCal = Calendar.getInstance();
		paymentDate.format(paymentCal.getTime());

		result.put("paymentYear", paymentCal.get(Calendar.YEAR));
		result.put("paymentMonth", String.format("%02d", paymentCal.get(Calendar.MONTH)+1));
		result.put("paymentDay", String.format("%02d", paymentCal.get(Calendar.DAY_OF_MONTH)));

		// next payment date
		//use pattern split string to date
		Pattern pt = Pattern.compile("-");
		String[] nextPaymentDate = pt.split(params.get("next_payment_date").toString());
		result.put("fromYear", nextPaymentDate[0]);
		result.put("fromMonth", nextPaymentDate[1]);
		result.put("fromDay", nextPaymentDate[2]);
		
		// payment end date
		String[] endDates = pt.split(params.get("stnd_ord_end_date").toString());
		result.put("toYear", endDates[0]);
		result.put("toMonth", endDates[1]);
		result.put("toDay", endDates[2]);

		return result;
	}
}
