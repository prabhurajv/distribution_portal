package com.pru.pruquote.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.Label;
import com.pru.pruquote.model.Medicalexam;
import com.pru.pruquote.repository.LabelRepository;
import com.pru.pruquote.repository.MedicalExamRepository;


@Service
public class AdditionalTableSetupService {
	

	@Autowired
	MedicalExamRepository medicalExamRepository;
	
	@Autowired
	LabelRepository labelRepository;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	public List<Medicalexam> medicalExamFindAll() {
		return medicalExamRepository.findAll();
	}
	
	
	public Medicalexam medicalExamFindOne(Long id) {
		return medicalExamRepository.findOne(id);
	}
	
	@Transactional
	public void medicalExamSave(Medicalexam medicalExam){
		sessionFactory.getCurrentSession().saveOrUpdate(medicalExam);
	}	
	
	public List<Label> labelFindAll() {
		return labelRepository.findAll();
	}
	
	
	public Label labelFindOne(Long id) {
		return labelRepository.findOne(id);
	}
	
	@Transactional
	public void labelSave(Label label){
		sessionFactory.getCurrentSession().saveOrUpdate(label);
	}	
}