package com.pru.pruquote.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.Issuance;
import com.pru.pruquote.utility.GlobalVar;

@Service
public class IssuanceService {
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Cacheable(value = "listissuance")
	public List<Issuance> getListIssuance(String userid) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select" +
				 		"  chdrnum as \"chdrNum\"" +
						", chdr_appnum as \"chdrAppNum\"" +
						", po_name as \"poName\"" +
						", sub_date as \"subDate\"" +
						", chdr_iss_date as \"chdrIssDate\"" +
						", chdr_ape as \"chdrApe\" " +
						", agntnum as \"agntNum\" " +
						", agnt_name as \"agntName\" " +
						", weekth as \"weekth\" " +
						", branch_code as \"branchCode\" " +
						", branch_name as \"branchName\" " +
						"from "	+ GlobalVar.db_name + ".spsearch_issuance(:userid)")
						.setParameter("userid", userid)
						.setResultTransformer(Transformers.aliasToBean(Issuance.class));
		
		List<Issuance> listIssuances = (List<Issuance>)query.list();
		
		return listIssuances;
	}
}