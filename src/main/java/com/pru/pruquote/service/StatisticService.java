package com.pru.pruquote.service;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StatisticService {
			
	@PersistenceContext
	private EntityManager em;
	 
			
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Transactional
	public List<Object> getLogCountByDate(Date fromDate,Date toDate){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"CALL spLogin_Statistics(:fromDate,:toDate)")
				
				.setParameter("fromDate", fromDate)
				.setParameter("toDate", toDate);
		@SuppressWarnings("unchecked")
		List<Object> result=query.list();
		return result;
	}
	
	//show all dates between, even if no result
	@Transactional
	public List<Object> getLogCountByDate2(Date fromDate,Date toDate){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"CALL spLogin_Statistics2(:fromDate,:toDate)")
				
				.setParameter("fromDate", fromDate)
				.setParameter("toDate", toDate);
		@SuppressWarnings("unchecked")
		List<Object> result=query.list();
		return result;
	}
		
}