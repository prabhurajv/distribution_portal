package com.pru.pruquote.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.transaction.annotation.Transactional;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pru.pruquote.dao.IUserDao;
import com.pru.pruquote.model.Log;
import com.pru.pruquote.model.Permission;
import com.pru.pruquote.model.Role;
import com.pru.pruquote.model.RolePermission;
import com.pru.pruquote.model.User;
import com.pru.pruquote.model.UserHistory;
import com.pru.pruquote.model.UserRole;
import com.pru.pruquote.repository.LogRepository;
import com.pru.pruquote.repository.PermissionRepository;
import com.pru.pruquote.repository.RolePermissionRepository;
import com.pru.pruquote.repository.RoleRepository;
import com.pru.pruquote.repository.UserHistoryRepository;
import com.pru.pruquote.repository.UserRepository;
import com.pru.pruquote.repository.UserRoleRepository;
import com.pru.pruquote.utility.GlobalVar;

@Service
public class SecurityService {
	
	@Autowired
	LogRepository logRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	PermissionRepository permissionRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	RolePermissionRepository rolePermissionRepository;
	
	@Autowired
	UserRoleRepository userRoleRepository;
	
	@Autowired
	UserHistoryRepository userHistoryRepository;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private IUserDao userDao;
	
	@Autowired
	private UserService userService;
	
	@Transactional
	public void disableInactiveUser(){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"select "+GlobalVar.db_name+".spGen_DisableUser");
//				"EXEC spGen_DisableUser");
		query.executeUpdate();
	}

	@Transactional
	public void clearQuoteHistory(String username){
		Query query = sessionFactory.getCurrentSession().createQuery("delete ap.\"PruquoteParm\" where couser = :username and transfer='16'");
		query.setParameter("username",username);
		@SuppressWarnings("unused")
		int result = query.executeUpdate();
	}
	
	public void saveLog(Log log){
		logRepository.save(log);
	}
	
	public void saveLogWithInfo(HttpServletRequest httpRequest,String info,String user){
		
		String ipAddress = httpRequest.getHeader("X-FORWARDED-FOR");  
        if (ipAddress == null) {  
     	   ipAddress = httpRequest.getRemoteAddr();  
        }
        
        Log log=new Log();
        log.setUserIdPk(userService.getUser(user).getId());;
        log.setAction(info);
        log.setCreatedDate(new Date());
        log.setIp(ipAddress);
        log.setPip(ipAddress);
		logRepository.save(log);		
	}
	
	public List<User> userFindAll() {
		return userRepository.findAll();		
	}
	
	@Transactional(readOnly=true)
	public List<Object> userFindAllWithRole(){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"select * from "+GlobalVar.db_name+".vuserwithrole");
				
		@SuppressWarnings("unchecked")
		List<Object> users=query.list();
		return users;
	}
	
	@Transactional(readOnly=true)
	public List<Object> userFindAllWithRoleLV(){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"select * from "+GlobalVar.db_name+".vuserwithrolelv");
				
		@SuppressWarnings("unchecked")
		List<Object> users=query.list();
		return users;
	}
	
	public User userFindOne(String username) {
		return userRepository.findOne(username);		
	}	
	
	public boolean checkDuplicateUsername(String username){
		if(userRepository.findOne(username)!=null) return true;
		else return false;				
	}
	
	@Transactional(readOnly=true)
	public User findUserByUsername(String username) {
//		return userRepository.findOne(username);
		User user=userService.getUser(username);		
//		for (UserRole userRole : user.getUserRoles()) {
//			System.out.print(userRole.getRole().getRoleName());
//		}
		return user;
	}
	
	public List<Role> roleFindAll() {
		return roleRepository.findAll();		
	}
	
	public Set<Role> roleFindByStatus(Byte status){
		return roleRepository.findByStatus(status);
	}
	
	public Set<Role> roleFindByRoleLVLessThanAndStatus(Byte roleLV,Byte status){
		return roleRepository.findByRoleLVLessThanAndStatus(roleLV, status);
	}
	
	public Role findRoleById(Integer id) {
		return roleRepository.findOne(id);		
	}
	
	public List<Permission> permissionFindAll() {
		return permissionRepository.findAll();		
	}
	
	public Permission permissionFindOne(Integer id){
		return permissionRepository.findOne(id);
	}
	
	public RolePermission rolePermissionFindOne(Integer rolePermissionId){
		return rolePermissionRepository.findOne(rolePermissionId);
	}
	
	public Set<RolePermission> rolePermissionFindByPermission(Permission permission){
		return rolePermissionRepository.findByPermission(permission);
	}
	
	public Set<RolePermission> rolePermissionFindByPermissionNotDelete(Integer permissionId) {
		return rolePermissionRepository.findByPermissionValid(permissionId);
	}
	
	public Set<RolePermission> rolePermissionFindByRole(Role role){
		return rolePermissionRepository.findByRole(role);
	}
	
	public Set<RolePermission> rolePermissionFindByRoleNotDelete(Integer roleId){
		return rolePermissionRepository.findByRoleValid(roleId);
	}
	
	public Set<UserRole> userRoleFindByUser(User user){
		return userRoleRepository.findByUser(user);
	}
	
	public Set<UserRole> userRoleFindByRole(Role role){
		return userRoleRepository.findByRole(role);
	}
	
	public Set<Permission> getPermissionNotExsitInRole(Integer roleId){
		return permissionRepository.findNotExistInRole(roleId);
	}
	
	public Set<UserHistory> getUserHistoryByUsername(String username){
		return userHistoryRepository.findByUserIdPk(userService.getUser(username).getId());
	}
	public void userHistorySave(UserHistory userHistory){
		userHistoryRepository.save(userHistory);
	}
	public List<Log> getLogByDate(Date startTime,Date endTime){
		return logRepository.findByLogDateBetween(startTime, endTime);
	}
	@Transactional
	public void userSave(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
//		userRepository.save(user);
	}
	@Transactional
	public void userRoleSave(UserRole userRole) {
		sessionFactory.getCurrentSession().saveOrUpdate(userRole);
	}
	
	@Transactional
	public void permissionSave(Permission permission) {
		sessionFactory.getCurrentSession().saveOrUpdate(permission);
	}
	
	@Transactional
	public void roleSave(Role role) {
		sessionFactory.getCurrentSession().saveOrUpdate(role);
	}
	
	@Transactional
	public void rolePermissionSave(RolePermission rolePermission) {
		sessionFactory.getCurrentSession().saveOrUpdate(rolePermission);
	}

	
		
}