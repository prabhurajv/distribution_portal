package com.pru.pruquote.service;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.Log;
import com.pru.pruquote.model.PruquoteParm;
import com.pru.pruquote.model.VpruquoteParm;
import com.pru.pruquote.model.listPruquote;
import com.pru.pruquote.repository.PruquoteParamRepository;
import com.pru.pruquote.utility.GlobalVar;
import com.pru.pruquote.repository.ListPruquoteParamRepository;

@Service
public class QuoteService {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	PruquoteParamRepository pruquoteParamRepository;

	@Autowired
	ListPruquoteParamRepository listPruquoteParamRepository;

	@Autowired
	private SessionFactory sessionFactory;

	public List<PruquoteParm> pruquoteParmsFindAll() {
		return pruquoteParamRepository.findAll();
	}

	public PruquoteParm pruquoteParmsFindOne(long id) {
		return pruquoteParamRepository.findOne(id);
	}

	@Transactional
	public List<VpruquoteParm> viewQuote(String product, String user) {
		Query query = sessionFactory.getCurrentSession()
				.createSQLQuery(
						"select * from ap.vpruquote_parms vp where vp.product like :pruduct and vp.couser=:user")
				.addEntity(VpruquoteParm.class).setParameter("pruduct", product).setParameter("user", user);
		@SuppressWarnings("unchecked")
		List<VpruquoteParm> quotes = query.list();
		return quotes;
	}

	@Transactional
	public List<VpruquoteParm> viewListQuote(String user) {
		Query query = sessionFactory.getCurrentSession()
				.createSQLQuery("select * from ap.vpruquote_parms vp where  vp.couser=:user")
				.addEntity(VpruquoteParm.class).setParameter("user", user);
		@SuppressWarnings("unchecked")
		List<VpruquoteParm> quotes = query.list();
		return quotes;
	}

	@Transactional
	public String getMBBySA(BigDecimal SA, String premiumTerm) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("SELECT ap.spGetMB_by_SA(:SA,:permiumTerm)")

				.setParameter("SA", SA).setParameter("permiumTerm", premiumTerm);
		@SuppressWarnings("unchecked")
		List<Object> quotes = query.list();
		return quotes.get(0).toString();
	}

	@Transactional
	public String getSAByMB(BigDecimal MB, String premiumTerm) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("SELECT ap.spGetSA_by_MB(:MB,:permiumTerm)")
				.setParameter("MB", MB).setParameter("permiumTerm", premiumTerm);
		@SuppressWarnings("unchecked")
		List<Object> quotes = query.list();
		return quotes.get(0).toString();
	}

	@Transactional
	public void pruQuoteParamSave(PruquoteParm pruquoteParm) {
		sessionFactory.getCurrentSession().saveOrUpdate(pruquoteParm);
	}

	// **********************************
	// * Edusave Product *
	// **********************************
	@Transactional
	public List<String> generateEduSaveKhmerQuote(final Long quoteId, final String lang, final String result) {
		List<String> res = sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<String> /* objectType returned */>() {
					@Override
					/* or object type you need to return to process */
					public List<String> execute(Connection conn) throws SQLException {
						PreparedStatement cstmt = conn.prepareStatement(
								"select * from ap.spGen_PruQuote(?,?,'RN','R1','R2','R3','R4','R5','R6','R7','R8')",
								ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
						String policyTerm = "";
						String paymentMode = "";
						int MB = 0;
						int PS = 0;
						int oDT3Row = 0;
						cstmt.setLong(1, quoteId);
						cstmt.setString(2, lang);
						// cstmt.setString(3, result);
						// Result list that would return ALL rows of ALL result sets
						List<String> results = new ArrayList<String>();
						String strText = "";
						ResultSet resultSet = null;
						ResultSet oDT1 = null;
						ResultSet oDT2 = null;
						ResultSet oDT3 = null;
						try {
							resultSet = cstmt.executeQuery();

							// #region "lblHeader1"
							resultSet.next();
							oDT1 = (ResultSet) resultSet.getObject(1);
							oDT1.first();
							PS = oDT1.getInt("PruRetirement");
							paymentMode = oDT1.getString("PaymentMode");
							strText += "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr>" + "<th class='Border2' style='width:175px;text-align:left;'>&nbsp;</th>"
									+ "<th class='Border2' colspan='2'>ឈ្មោះ</th>" + "<th class='Border2'>ភេទ</th>"
									+ "<th class='Border2'>អាយុ</th>"
									+ "<th class='Border2' colspan='3'>របៀបទូទាត់</th>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='width:;text-align:left;'>ឈ្មោះអ្នកត្រូវបានធានារ៉ាប់រង</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("POName") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POSex") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POAge") + "</td>"
									+ "<td class='Border2' colspan=3 rowspan=3>" /*
																					 * + oDT1.getString("PaymentMode2")
																					 * + "/"
																					 */ + oDT1.getString("PaymentType2")
									+ "</td>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='width:;text-align:left;'>ឈ្មោះបុគ្គលដែលជាកម្មវត្ថុនៃការធានា</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("LA1Name") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Sex2") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Age") + "</td>" + "</tr>"
									+ "<tr>     "
									+ "<td class='Border2' style='width:;text-align:left;'>ឈ្មោះបុគ្គលដែលជាកម្មវត្ថុនៃការធានាបន្ថែម</td>     "
									+ "<td class='Border2' colspan=2>" + oDT1.getString("LA2Name") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA2Sex2") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA2Age") + "</td>" + "</tr>";
							SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd");

							Date dOBDate;
							try {
								dOBDate = dt.parse(oDT1.getString("LA1DateOfBirth"));
							} catch (Exception e) {
								dOBDate = new Date();
							}
							SimpleDateFormat df = new SimpleDateFormat("hhmmssSSS");
							Date date = new Date();
							String time = df.format(date);

							String lblSerialNo1 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P";
							String lblSerialNo2 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P2";
							// Particulars of Basic Plan and My Option Benefits:
							if (resultSet.next()) {
								oDT2 = (ResultSet) resultSet.getObject(1);
							}
							strText += " <tr > "
									+ "<td class='Border2' style='width:;text-align:left;'>ព័ត៌មានលម្អិតនៃគម្រោងមូលដ្ឋាន​និង<br>​លក្ខខណ្ឌបន្ថែម/ផលិតផលបំពេញបន្ថែម</td>"
									+ "<th class='Border2'>បុគ្គលដែលជាកម្មវត្ថុ​នៃការធានា</th>"
									+ "<th class='Border2'>រយៈពេលកំណត់នៃ<br>បណ្ណ​សន្យារ៉ាប់រង​​(ឆ្នាំ)    </th>"
									+ "<th class='Border2'>រយៈពេលបង់<br>បុព្វលាភរ៉ាប់រង(ឆ្នាំ)</th>"
									+ "<th class='Border2'>ទឹកប្រាក់ធានា<br>រ៉ាប់រងសរុប​ (US$)</th>"
									+ "<th class='Border2'>ប្រចាំខែ<br>(US$)</th>"
									+ "<th class='Border2'>ប្រចាំឆមាស <br>(US$)</th>"
									+ "<th class='Border2'>ប្រចាំឆ្នាំ <br>(US$)</th>" + "</tr>";
							if (oDT2.first()) {
								policyTerm = oDT2.getString("PolicyTerm");
								strText += " <tr > "
										+ "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; "
										+ oDT2.getString("ProductName") + "</td>"
										+ "<td class='Border2' style='width:'>" + oDT2.getString("Life_Assured")
										+ "</td>" + "<td class='Border2' style='width:90px'>"
										+ oDT2.getString("PolicyTerm") + "</td>"
										+ "<td class='Border2' style='width:100px'>" + oDT2.getString("PremiumTerm")
										+ "</td>" + "<td class='Border2' style='width:80px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("SA")) + "</td>"
										+ "<td class='Border2' style='width:65px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("P12_Amt")) + "</td>"
										+ "<td class='Border2' style='width:65px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("P02_Amt")) + "</td>"
										+ "<td class='Border2' style='width:65px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("P01_Amt")) + "</td>" + "</tr>";
							}

							if (resultSet.next()) {
								oDT3 = (ResultSet) resultSet.getObject(1);
							}

							// get oDT3 rows count
							if (oDT3.last())
								oDT3Row = oDT3.getRow();

							if (oDT3.first()) {

								// My Option Benefits (Also known as Riders)
								if (oDT3Row > 6) {
									strText += " <tr > "
											+ "<td class='Border2' style='width:px;text-align:left;' colspan=8>លក្ខខណ្ឌបន្ថែម/ផលិតផលបំពេញបន្ថែម</td>"
											+ "</tr>";
								}
								// oDT3.beforeFirst();
								while (oDT3.next()) {
									if (oDT3.getString("Product").equals("TT")) {
										if (oDT3.getString("ProductName").contains("%")) {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "%</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "%</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "%</th>"
													+ " </tr>";
										} else if (oDT3.isLast()) {
											String st12 = "";
											String st02 = "";
											String st01 = "";
											if (paymentMode.contains("12")) {
												st12 = "background-color:#bfc8cf;";
												st02 = "";
												st01 = "";
											} else if (paymentMode.contains("02")) {
												st12 = "";
												st02 = "background-color:#bfc8cf;";
												st01 = "";
											} else {
												st12 = "";
												st02 = "";
												st01 = "background-color:#bfc8cf;";
											}
											strText += "<tr>"
													+ "     <th class='Border2' style='width:;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:;" + st12 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:;" + st02 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:;;" + st01 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										} else {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										}
									} else if (oDT3.getString("Product").equals("TH_RTR2")) {
										strText += " <tr > "
												+ "<td class='Border2' style='width:;text-align:left;'></td>"
												+ "<th class='Border2'>បុគ្គលដែលជាកម្មវត្ថុ​នៃការធានា</th>"
												+ "<th class='Border2'>រយៈពេលកំណត់នៃ<br>ផលិតផលបំពេញ<br/>បន្ថែម​​(ឆ្នាំ)    </th>"
												+ "<th class='Border2'>រយៈពេលបង់<br>បុព្វលាភរ៉ាប់រង(ឆ្នាំ)</th>"
												+ "<th class='Border2'>អត្ថប្រយោជន៍​នៅ​កាល​បរិចេ្ឆទ​ផុត​កំណត់​នៃ​បណ្ណ​សន្យា​រ៉ាប់រង(US$)</th>"
												+ "<th class='Border2'>ប្រចាំខែ<br>(US$)</th>"
												+ "<th class='Border2'>ប្រចាំឆមាស <br>(US$)</th>"
												+ "<th class='Border2'>ប្រចាំឆ្នាំ <br>(US$)</th>" + "</tr>";
									} else {
										strText += " <tr > "
												+ "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; "
												+ oDT3.getString("ProductName") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("Life_Assured")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ oDT3.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("PremiumTerm")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("SA")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</td>"
												+ "</tr>";
									}
								}

								oDT3.beforeFirst();

								while (oDT3.next()) {

								}

							}
							strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody1- 1. Guaranteed Maturity Benefit with the Basic Policy that
							// can be Used as the Education Fund for the Child"
							ResultSet oDT4 = null;
							if (resultSet.next()) {
								oDT4 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' rowspan=3 style='width:60px;' style='background-color:#bfc8cf;'>ឆ្នាំបណ្ណសន្យា<BR>រ៉ាប់រង</th>"
									+ "<th class='Border2' rowspan=3 style='width:60px' style='background-color:#bfc8cf;'>អាយុ</th>"
									+
							/*
							 * "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>&nbsp;</th>"
							 * +
							 */
							"<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់នៃបណ្ណសន្យារ៉ាប់រង</th>"
									+ "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់នៃបណ្ណសន្យារ៉ាប់រងដែលបង់ជាដំណាក់កាល</th>"
									+ "</tr>" + "<tr>" +

									"<th class='Border2' style='width:173px;background-color:#bfc8cf;'>ការបង់ជាសរុប</th>"
									+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>សរុប</th>"
									+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>ការបង់ជាដំណាក់កាល</th>"
									+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>សរុប</th>"
									+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
							while (oDT4.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:;'>"
										+ oDT4.getString("PolicyTerm") + "</td>" + "<td class='Border2' style='width:'>"
										+ oDT4.getString("Age") + "</td>" + "<td class='Border2' style='width:'>"
										+ (String.format("%,.2f", oDT4.getBigDecimal("OPT1_LumpSum")).equals("0.00")
												? " "
												: String.format("%,.2f", oDT4.getBigDecimal("OPT1_LumpSum")))
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ (String.format("%,.2f", oDT4.getBigDecimal("OPT1_TT")).equals("0.00") ? " "
												: String.format("%,.2f", oDT4.getBigDecimal("OPT1_TT")))
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT2_Inst")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_Inst_TT")) + "</td>"
										+ "</tr>";
							}
							strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody2 -2. Life Insurance Benefit, that Helps the Family Take Care
							// of the Immediate Liabilities"
							ResultSet oDT5 = null;
							if (resultSet.next()) {
								oDT5 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>ឆ្នាំបណ្ណសន្យា<BR>រ៉ាប់រង</th>"
									+ "<th class='Border2' rowspan=3 style='width:60px'>អាយុ</th>"
									+ "<th class='Border2' rowspan=2 style='width:128px'>បុព្វលាភរ៉ាប់រងរាល់ឆ្នាំ</th>"
									+ "<th class='Border2' rowspan=2 style='width:128px'>បុព្វលាភរ៉ាប់រងសរុប</th>"
									+ "<th class='Border2' colspan=2 style='width:'>អត្ថប្រយោជន៍ក្នុងករណីទទួលមរណភាពឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍ </th>"
									+ "<th class='Border2' rowspan=2 style='width:138px'>តម្លៃទឹកប្រាក់ដែលមាន<BR>ការធានា (នៅពេលបោះបង់<BR>បណ្ណសន្យារ៉ាប់រង)(GCV)</th>"
									+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:148px'>មិនបណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
									+ "<th class='Border2' style='width:148px'>បណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
									+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
							while (oDT5.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:'>" + oDT5.getString("PolicyYear")
										+ "</td>" + "<td class='Border2' style='width:'>" + oDT5.getString("Age")
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("P_APE")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("P_Total")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Non_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("Surrender")) + "</td>" + "</tr>";
							}
							strText += "</table>";
							results.add(strText);
							// #endregion

							// #region "lblBody3 -3. Benefits with Additional Riders, that Help the Family
							// in the Further Securing Child's Education and Future"
							ResultSet oDT6 = null;
							if (resultSet.next()) {
								oDT6 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							if (oDT6.first()) {
								strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
										+ "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:160px;text-align:center;' colspan=1 rowspan=3>លក្ខខណ្ឌបន្ថែម</th>"
										+ "<th class='Border2' style='width:140px;' rowspan=3>បុគ្គលដែលជាកម្មវត្ថុ<br>នៃការធានា</th>"
										+ "<th class='Border2' style='width:' colspan=2>អត្ថប្រយោជន៍ក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍</th>"
										+ "<th class='Border2' style='width:260px' rowspan=3>សេចក្តីសម្រាយ</th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:150px' >មិនបណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
										+ "<th class='Border2' style='width:138px' >បណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
										+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:' >(US$)</th>"
										+ "<th class='Border2' style='width:' >(US$)</th>" + "</tr>";
								oDT6.beforeFirst();
								while (oDT6.next()) {

									strText += "<tr>"
											+ "<td class='Border2' style='width:160px;text-align:left;' colspan=1 >"
											+ oDT6.getString("ProductName") + "</td>"
											+ "<td class='Border2' style='width:110px' colspan=1 >"
											+ oDT6.getString("Life_Assured") + "</td>"
											+ "<td class='Border2' style='width:' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Non_Accident")) + "</td>"
											+ "<td class='Border2' style='width:' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Accident")) + "</td>"
											+ "<td class='Border2' style='width:260px'>"
											+ oDT6.getString("Payment_Terms") + "</td>" + "</tr>";
								}
								strText += "</table>";
							}
							if (policyTerm.contains("10")) {
								// if(oDT3Row==10){
								// results.add("");
								// results.add(strText);
								// }else{
								results.add(strText);
								results.add("");
								// }
							} else {
								results.add("");
								results.add(strText);
							}

							// #endregion

							// #region "Footer1"
							strText = "";
							// Life Insurance Benefit
							// if (policyTerm.contains("10"))
							// {
							// strText += "<table>";
							// strText += "<tr><td class='Border3' style='height:100px;border:0px solid
							// black;'>&nbsp;</td></tr>";
							// strText += "</table>";
							// }
							// else if (policyTerm.contains("12"))
							// {
							// strText += "<table>";
							// strText += "<tr><td class='Border3' style='height:60px;border:0px solid
							// black;'>&nbsp;</td></tr>";
							// strText += "</table>";
							// }
							//
							// //Riders
							// if (policyTerm.contains("10"))
							// {
							// if (oDT3Row == 9)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 1; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							// strText += "</table>";
							// }
							// else if (oDT3Row == 8)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 2; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							// strText += "</table>";
							// }
							// else if (oDT3Row == 7)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 3; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							//
							// strText += "</table>";
							// }
							// else if (oDT3Row == 6)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 5; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							//
							// strText += "</table>";
							// }
							// }
							// else{
							// if (oDT3Row == 9)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 6; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							// strText += "</table>";
							// }
							// else if (oDT3Row == 8)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 7; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							// strText += "</table>";
							// }
							// else if (oDT3Row == 7)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 8; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							//
							// strText += "</table>";
							// }
							// else if (oDT3Row == 6)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 10; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							//
							// strText += "</table>";
							// }
							// }
							results.add(strText);
							// #endregion

							// #region "Footer2"
							strText = "";
							// Riders
							// strText += "<table>";
							// if (policyTerm.contains("10"))
							// {
							// for (int i = 0; i < 20; i++) {
							// strText += "<tr><td class='Border3' style='height:'>&nbsp;</td></tr>";
							// }
							// }
							// else{
							// for (int i = 0; i < 16; i++) {
							// strText += "<tr><td class='Border3' style='height:'>&nbsp;</td></tr>";
							// }
							// }
							// strText += "</table>";

							results.add(strText);
							results.add(lblSerialNo1);
							results.add(lblSerialNo2);
							// #endregion

							// #region "lblBody4- 4. Guaranteed Maturity Benefit with the PruSaver"
							ResultSet oDT7 = null;
							if (resultSet.next()) {
								oDT7 = (ResultSet) resultSet.getObject(1);
								if (oDT7.first()) {
									strText = "";
									strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' rowspan=3 style='width:60px;' style='background-color:#bfc8cf;'>ឆ្នាំ​នៃ​ផលិតផលបំពេញបន្ថែម</th>"
											+ "<th class='Border2' rowspan=3 style='width:60px' style='background-color:#bfc8cf;'>អាយុ</th>"
											+
									/*
									 * "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>&nbsp;</th>"
									 * +
									 */
									"<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់នៃបណ្ណសន្យារ៉ាប់រង</th>"
											+ "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់នៃបណ្ណសន្យារ៉ាប់រងដែលបង់ជាដំណាក់កាល</th>"
											+ "</tr>" + "<tr>" +

											"<th class='Border2' style='width:173px;background-color:#bfc8cf;'>ការបង់ជាសរុប</th>"
											+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>សរុប</th>"
											+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>ការបង់ជាដំណាក់កាល</th>"
											+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>សរុប</th>"
											+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
									oDT7.beforeFirst();
									while (oDT7.next()) {
										strText += "<tr>" + "<td class='Border2' style='width:;'>"
												+ oDT7.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT7.getString("Age")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ (String.format("%,.2f", oDT7.getBigDecimal("OPT1_LumpSum"))
														.equals("0.00")
																? " "
																: String.format("%,.2f",
																		oDT7.getBigDecimal("OPT1_LumpSum")))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ (String.format("%,.2f", oDT7.getBigDecimal("OPT1_TT")).equals("0.00")
														? " "
														: String.format("%,.2f", oDT7.getBigDecimal("OPT1_TT")))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT7.getBigDecimal("OPT2_Inst")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT7.getBigDecimal("OPT1_Inst_TT")) + "</td>"
												+ "</tr>";
									}
									strText += "</table>";
									// body1
									results.add(strText);
								}
							} else {
								results.add("");
							}

							// #endregion

							// #region "lblBody5 -5. Life Insurance Benefit with PruSaver"
							ResultSet oDT8 = null;
							if (resultSet.next()) {
								oDT8 = (ResultSet) resultSet.getObject(1);
								if (oDT8.first()) {
									strText = "";
									strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' rowspan=3 style='width:60px;'>ឆ្នាំនៃ​​ផលិតផលបំពេញបន្ថែម</th>"
											+ "<th class='Border2' rowspan=3 style='width:60px'>អាយុ</th>"
											+ "<th class='Border2' rowspan=2 style='width:128px'>បុព្វលាភរ៉ាប់រងរាល់ឆ្នាំ</th>"
											+ "<th class='Border2' rowspan=2 style='width:128px'>បុព្វលាភរ៉ាប់រងសរុប</th>"
											+ "<th class='Border2' colspan=2 style='width:'>អត្ថប្រយោជន៍ក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px'>តម្លៃទឹកប្រាក់ដែលមាន<BR>ការធានា (នៅពេលបោះបង់<BR>ផលិតផលបំពេញបន្ថែម)(GCV)</th>"
											+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:148px'>មិនបណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
											+ "<th class='Border2' style='width:148px'>បណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
											+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
									oDT8.beforeFirst();
									while (oDT8.next()) {
										strText += "<tr>" + "<td class='Border2' style='width:'>"
												+ oDT8.getString("PolicyYear") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT8.getString("Age")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_APE")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_Total")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Non_Accident"))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Accident")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("Surrender")) + "</td>"
												+ "</tr>";
									}
									strText += "</table>";
									// body2
									results.add(strText);
									oDT7.close();
									oDT8.close();
								}
							} else {
								results.add("");
							}

							oDT1.close();
							oDT2.close();
							oDT3.close();
							oDT4.close();
							oDT5.close();
							oDT6.close();

						} finally {
							cstmt.close();
						}
						if (PS == 15) {
							results.add("Yes");
						} else {
							results.add("No");
						}

						return results; // this should contain All rows or objects you need for further processing
					}
				});
		return res;
	}

	@Transactional
	public List<String> generateEduSaveLatinQuote(final Long quoteId, final String lang, final String result) {
		List<String> res = sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<String> /* objectType returned */>() {
					@Override
					/* or object type you need to return to process */
					public List<String> execute(Connection conn) throws SQLException {
						PreparedStatement cstmt = conn.prepareStatement(
								"select * from ap.spGen_PruQuote(?,?,'RN','R1','R2','R3','R4','R5','R6','R7','R8')",
								ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
						String policyTerm = "";
						String paymentMode = "";
						int MB = 0;
						int PS = 0;
						int oDT3Row = 0;
						cstmt.setLong(1, quoteId);
						cstmt.setString(2, lang);
						// cstmt.setString(3, result);
						// Result list that would return ALL rows of ALL result sets
						List<String> results = new ArrayList<String>();
						String strText = "";
						ResultSet resultSet = null;
						ResultSet oDT1 = null;
						ResultSet oDT2 = null;
						ResultSet oDT3 = null;
						try {
							resultSet = cstmt.executeQuery();

							// #region "lblHeader1"
							resultSet.next();
							oDT1 = (ResultSet) resultSet.getObject(1);
							oDT1.first();
							PS = oDT1.getInt("PruRetirement");
							paymentMode = oDT1.getString("PaymentMode");
							strText += "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr>" + "<th class='Border2' style='text-align:left;'>&nbsp;</th>"
									+ "<th class='Border2' colspan='2'><u>Name</u></th>"
									+ "<th class='Border2'><u>Gender</u></th>" + "<th class='Border2'><u>Age</u></th>"
									+ "<th class='Border2' colspan='3'><u>Premium Mode</u></th>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='text-align:left;'>Policy Owner's Name:</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("POName") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POSex") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POAge") + "</td>"
									+ "<td class='Border3' colspan=3 rowspan=3>" /*
																					 * + oDT1.getString("PaymentMode2")
																					 * + "/"
																					 */ + oDT1.getString("PaymentType2")
									+ "</td>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='text-align:left;'>Main Life Assured's Name:</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("LA1Name") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Sex2") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Age") + "</td>" + "</tr>" + "<tr>"
									+ "<td class='Border3' style='text-align:left;'>Additional Life Assured's Name:</td>     "
									+ "<td class='Border3' colspan=2>" + oDT1.getString("LA2Name") + "</td>"
									+ "<td class='Border3'>" + oDT1.getString("LA2Sex2") + "</td>"
									+ "<td class='Border3'>" + oDT1.getString("LA2Age") + "</td>" + "</tr>";
							SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd");

							Date dOBDate;
							try {
								dOBDate = dt.parse(oDT1.getString("LA1DateOfBirth"));
							} catch (Exception e) {
								dOBDate = new Date();
							}

							SimpleDateFormat df = new SimpleDateFormat("hhmmssSSS");
							Date date = new Date();
							String time = df.format(date);

							String lblSerialNo1 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P";
							String lblSerialNo2 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P2";

							// Particulars of Basic Plan and My Option Benefits:
							if (resultSet.next()) {
								oDT2 = (ResultSet) resultSet.getObject(1);
							}
							strText += " <tr > "
									+ "<td class='Border2' style='width:170px;text-align:left;'>Particulars of Basic Plan and Riders:</td>"
									+ "<th class='Border2' style='width:'>Life Assured</th>"
									+ "<th class='Border2' style='width:40px'>Policy Term</th>"
									+ "<th class='Border2' style='width:75px'>Premium Payment Term</th>"
									+ "<th class='Border2' style='width:75px'>Sum Assured (US$)</th>"
									+ "<th class='Border2' style='width:70px'>Monthly (US$)</th>"
									+ "<th class='Border2' style='width:100px'>Semi-Annual (US$)</th>"
									+ "<th class='Border2' style='width:70px'>Annual (US$)</th>" + "</tr>";
							if (oDT2.first()) {
								policyTerm = oDT2.getString("PolicyTerm");

								strText += " <tr > " + "<td class='Border3' style='text-align:left;'>&nbsp;&nbsp; "
										+ oDT2.getString("ProductName") + "</td>" + "<td class='Border3' >"
										+ oDT2.getString("Life_Assured") + "</td>" + "<td class='Border3' >"
										+ oDT2.getString("PolicyTerm") + "</td>" + "<td class='Border3' >"
										+ oDT2.getString("PremiumTerm") + "</td>" + "<td class='Border3' >"
										+ String.format("%,.2f", oDT2.getBigDecimal("SA")) + "</td>"
										+ "<td class='Border3' >"
										+ String.format("%,.2f", oDT2.getBigDecimal("P12_Amt")) + "</td>"
										+ "<td class='Border3' >"
										+ String.format("%,.2f", oDT2.getBigDecimal("P02_Amt")) + "</td>"
										+ "<td class='Border3' >"
										+ String.format("%,.2f", oDT2.getBigDecimal("P01_Amt")) + "</td>" + "</tr>";
							}

							if (resultSet.next()) {
								oDT3 = (ResultSet) resultSet.getObject(1);
							}

							// get oDT3 rows count
							if (oDT3.last())
								oDT3Row = oDT3.getRow();

							if (oDT3.first()) {

								// My Option Benefits (Also known as Riders)
								if (oDT3Row > 6) {
									strText += " <tr > "
											+ "<td class='Border3' style='width:250px;text-align:left;' colspan=8>Riders</td>"
											+ "</tr>";
								}
								// oDT3.beforeFirst();
								while (oDT3.next()) {
									if (oDT3.getString("Product").equals("TT")) {
										if (oDT3.getString("ProductName").contains("%")) {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "%</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "%</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "%</th>"
													+ " </tr>";
										} else if (oDT3.isLast()) {
											String st12 = "";
											String st02 = "";
											String st01 = "";
											if (paymentMode.contains("12")) {
												st12 = "background-color:#bfc8cf;";
												st02 = "";
												st01 = "";
											} else if (paymentMode.contains("02")) {
												st12 = "";
												st02 = "background-color:#bfc8cf;";
												st01 = "";
											} else {
												st12 = "";
												st02 = "";
												st01 = "background-color:#bfc8cf;";
											}
											strText += "<tr>"
													+ "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:;" + st12 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:;" + st02 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:;;" + st01 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										} else {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										}
									} else if (oDT3.getString("Product").equals("TH_RTR2")) {
										strText += " <tr > "
												+ "<td class='Border3' style='width:170px;text-align:left;'>Additional Riders:</td>"
												+ "<th class='Border3' style='width:'>Life Assured</th>"
												+ "<th class='Border3' style='width:40px'>Policy Term</th>"
												+ "<th class='Border3' style='width:75px'>Premium Payment Term</th>"
												+ "<th class='Border3' style='width:75px'>Maturity Benifit (US$)</th>"
												+ "<th class='Border3' style='width:70px'>Monthly (US$)</th>"
												+ "<th class='Border3' style='width:100px'>Semi-Annual (US$)</th>"
												+ "<th class='Border3' style='width:70px'>Annual (US$)</th>" + "</tr>";
									} else {
										strText += " <tr > "
												+ "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; "
												+ oDT3.getString("ProductName") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("Life_Assured")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ oDT3.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("PremiumTerm")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("SA")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</td>"
												+ "</tr>";
									}

								}

								oDT3.beforeFirst();

								while (oDT3.next()) {

								}

							}
							strText += "</table>";
							// header1
							results.add(strText);

							// #endregion

							// #region "lblBody1- 1. Guaranteed Maturity Benefit with the Basic Policy that
							// can be Used as the Education Fund for the Child"
							ResultSet oDT4 = null;
							if (resultSet.next()) {
								oDT4 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>" +
							/*
							 * "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>&nbsp;</th>"
							 * +
							 */
							"<th class='Border2' rowspan=3 style='width:60px;'>Policy Year</th>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>"
									+ "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>Maturity Benefit</th>"
									+ "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>Maturity Benefit with Settlement Option</th>"
									+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:173px'>Lump Sum Payment</th>"
									+ "<th class='Border2' style='width:173px'>Total</th>"
									+ "<th class='Border2' style='width:173px'>Installment Payment</th>"
									+ "<th class='Border2' style='width:173px'>Total</th>" + "</tr>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
							while (oDT4.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:60px;'>"
										+ oDT4.getString("PolicyTerm") + "</td>"
										+ "<td class='Border2' style='width:60px'>" + oDT4.getString("Age") + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ (String.format("%,.2f", oDT4.getBigDecimal("OPT1_LumpSum")).equals("0.00")
												? " "
												: String.format("%,.2f", oDT4.getBigDecimal("OPT1_LumpSum")))
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ (String.format("%,.2f", oDT4.getBigDecimal("OPT1_TT")).equals("0.00") ? " "
												: String.format("%,.2f", oDT4.getBigDecimal("OPT1_TT")))
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT2_Inst")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_Inst_TT")) + "</td>"
										+ "</tr>";
							}
							strText += "</table>";
							// body1
							results.add(strText);

							// #endregion

							// #region "lblBody2 -2. Life Insurance Benefit, that Helps the Family Take Care
							// of the Immediate Liabilities"
							ResultSet oDT5 = null;
							if (resultSet.next()) {
								oDT5 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>Policy Year</th>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px;'>Premium every<br/>year</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px;'>Total Premium</th>"
									+ "<th class='Border2' colspan=2 style='width:276px;'>Death/TPD Benefit</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px;'>Guaranteed Cash Value(GCV)</th>"
									+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>Not Caused by an Accident</th>"
									+ "<th class='Border2' style='width:'>Caused by an Accident</th>" + "</tr>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
							while (oDT5.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:'>" + oDT5.getString("PolicyYear")
										+ "</td>" + "<td class='Border2' style='width:'>" + oDT5.getString("Age")
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("P_APE")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("P_Total")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Non_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("Surrender")) + "</td>" + "</tr>";
							}
							strText += "</table>";
							// body2
							results.add(strText);
							// #endregion

							// #region "lblBody3 -3. Benefits with Additional Riders, that Help the Family
							// in the Further Securing Child's Education and Future"
							ResultSet oDT6 = null;
							if (resultSet.next()) {
								oDT6 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							if (oDT6.first()) {
								strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
										+ "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:120px;text-align:center;' colspan=1 rowspan=3>Rider Benefits</th>"
										+ "<th class='Border2' style='width:' rowspan=3>Life Assured Name</th>"
										+ "<th class='Border2' style='width:200px;' colspan=2>Death/TPD Benefit</th>"
										+ "<th class='Border2' style='width:350px;' rowspan=3>Benefit Payment Terms</th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2'>Not Caused by an Accident</th>"
										+ "<th class='Border2'>Caused by an Accident</th>" + "</tr>"
										+ "<tr style='background-color:#bfc8cf;'>" + "<th class='Border2'>(US$)</th>"
										+ "<th class='Border2'>(US$)</th>" + "</tr>";
								oDT6.beforeFirst();
								while (oDT6.next()) {

									strText += "<tr>" + "<td class='Border2' colspan=1 >"
											+ oDT6.getString("ProductName") + "</td>"
											+ "<td class='Border2' colspan=1 >" + oDT6.getString("Life_Assured")
											+ "</td>" + "<td class='Border2' colspan=1 width=90>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Non_Accident")) + "</td>"
											+ "<td class='Border2' colspan=1 width=90>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Accident")) + "</td>"
											+ "<td class='Border2' style='width:350px' colspan=1>"
											+ oDT6.getString("Payment_Terms") + "</td>" + "</tr>";
								}
								strText += "</table>";
							}
							// body3
							results.add(strText);
							// #endregion

							// #region "Footer1"
							strText = "";
							// Life Insurance Benefit
							if (policyTerm.contains("10")) {
								strText += "<table>";
								strText += "<tr><td class='' style='height:90px'>&nbsp;</td></tr>";
								strText += "</table>";
							} else if (policyTerm.contains("12")) {
								strText += "<table>";
								strText += "<tr><td class='' style='height:60px'>&nbsp;</td></tr>";
								strText += "</table>";
							}

							// Riders
							if (oDT3Row == 9) {
								strText += "<table>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "</table>";
							} else if (oDT3Row == 8) {
								strText += "<table>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "</table>";
							} else if (oDT3Row == 7) {
								strText += "<table>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "</table>";
							} else if (oDT3Row == 6) {
								strText += "<table>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "</table>";
							}
							results.add(strText);
							// #endregion

							//
							results.add(lblSerialNo1);
							results.add(lblSerialNo2);
							// #endregion

							// #region "lblBody4- 4. Guaranteed Maturity Benefit with the PruSaver"
							ResultSet oDT7 = null;
							if (resultSet.next()) {
								oDT7 = (ResultSet) resultSet.getObject(1);
								if (oDT7.first()) {
									strText = "";
									strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
											+ "<tr style='background-color:#bfc8cf;'>" +
									/*
									 * "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>&nbsp;</th>"
									 * +
									 */
									"<th class='Border2' rowspan=3 style='width:60px;'>Rider Year</th>"
											+ "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>"
											+ "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>Maturity Benefit</th>"
											+ "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>Maturity Benefit with Settlement Option</th>"
											+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:173px'>Lump Sum Payment</th>"
											+ "<th class='Border2' style='width:173px'>Total</th>"
											+ "<th class='Border2' style='width:173px'>Installment Payment</th>"
											+ "<th class='Border2' style='width:173px'>Total</th>" + "</tr>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
									oDT7.beforeFirst();
									while (oDT7.next()) {
										strText += "<tr>" + "<td class='Border2' style='width:60px;'>"
												+ oDT7.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:60px'>" + oDT7.getString("Age")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ (String.format("%,.2f", oDT7.getBigDecimal("OPT1_LumpSum"))
														.equals("0.00")
																? " "
																: String.format("%,.2f",
																		oDT7.getBigDecimal("OPT1_LumpSum")))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ (String.format("%,.2f", oDT7.getBigDecimal("OPT1_TT")).equals("0.00")
														? " "
														: String.format("%,.2f", oDT7.getBigDecimal("OPT1_TT")))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT7.getBigDecimal("OPT2_Inst")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT7.getBigDecimal("OPT1_Inst_TT")) + "</td>"
												+ "</tr>";
									}
									strText += "</table>";
									// body1
									results.add(strText);
								}
							} else {
								results.add("");
							}

							// #endregion

							// #region "lblBody5 -5. Life Insurance Benefit with PruSaver"
							ResultSet oDT8 = null;
							if (resultSet.next()) {
								oDT8 = (ResultSet) resultSet.getObject(1);
								if (oDT8.first()) {
									strText = "";
									strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' rowspan=3 style='width:60px;'>Rider Year</th>"
											+ "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px;'>Rider premium<br/>every year</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px;'>Total Rider Premium</th>"
											+ "<th class='Border2' colspan=2 style='width:276px;'>Death/TPD Benefit</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px;'>Guaranteed Cash Value(GCV)</th>"
											+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>Not Caused by an Accident</th>"
											+ "<th class='Border2' style='width:'>Caused by an Accident</th>" + "</tr>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
									oDT8.beforeFirst();
									while (oDT8.next()) {
										strText += "<tr>" + "<td class='Border2' style='width:'>"
												+ oDT8.getString("PolicyYear") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT8.getString("Age")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_APE")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_Total")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Non_Accident"))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Accident")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("Surrender")) + "</td>"
												+ "</tr>";
									}
									strText += "</table>";
									// body2
									results.add(strText);
									oDT7.close();
									oDT8.close();
								}
							} else {
								results.add("");
							}

							// #endregion

							oDT1.close();
							oDT2.close();
							oDT3.close();
							oDT4.close();
							oDT5.close();
							oDT6.close();

						} finally {
							cstmt.close();
						}
						if (PS == 15) {
							results.add("Yes");
						} else {
							results.add("No");
						}
						return results; // this should contain All rows or objects you need for further processing
					}
				});
		return res;
	}

	// **********************************
	// * PruMyfamily Product *
	// **********************************
	@Transactional
	public List<String> generateMyFamilyKhmerQuote(final Long quoteId, final String lang, final String result) {
		List<String> res = sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<String> /* objectType returned */>() {
					@Override
					/* or object type you need to return to process */
					public List<String> execute(Connection conn) throws SQLException {
						PreparedStatement cstmt = conn.prepareStatement(
								"select * from ap.spGen_PruQuote(?,?,'RN','R1','R2','R3','R4','R5','R6','R7','R8')",
								ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
						@SuppressWarnings("unused")
						String policyTerm = "";
						String paymentMode = "";
						int oDT3Row = 0;
						cstmt.setLong(1, quoteId);
						cstmt.setString(2, lang);
						// Result list that would return ALL rows of ALL result sets
						List<String> results = new ArrayList<String>();
						String strText = "";
						ResultSet resultSet = null;
						ResultSet oDT1 = null;
						ResultSet oDT2 = null;
						ResultSet oDT3 = null;
						try {
							String errStr = "";
							try {
								resultSet = cstmt.executeQuery();
							} catch (SQLException e) {
								errStr = e.getMessage();
							}

							// #region "lblHeader1"
							resultSet.next();
							oDT1 = (ResultSet) resultSet.getObject(1);
							oDT1.first();
							paymentMode = oDT1.getString("PaymentMode");
							strText += "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr>" + "<th class='Border2' style='width:230px;text-align:left;'>&nbsp;</th>"
									+ "<th class='Border2' colspan='2'>ឈ្មោះ</th>" + "<th class='Border2'>ភេទ</th>"
									+ "<th class='Border2'>អាយុ</th>"
									+ "<th class='Border2' colspan='3'>របៀបទូទាត់</th>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='width:;text-align:left;'>ឈ្មោះអ្នកត្រូវបានធានារ៉ាប់រង</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("POName") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POSex") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POAge") + "</td>"
									+ "<td class='Border2' colspan=3 rowspan=3>" /*
																					 * + oDT1.getString("PaymentMode2")
																					 * + "/"
																					 */ + oDT1.getString("PaymentType2")
									+ "</td>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='width:;text-align:left;'>ឈ្មោះបុគ្គលដែលជាកម្មវត្ថុនៃការធានា</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("LA1Name") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Sex2") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Age") + "</td>" + "</tr>"
									+ "<tr>     "
									+ "<td class='Border2' style='width:;text-align:left;'>ឈ្មោះបុគ្គលដែលជាកម្មវត្ថុនៃការធានាបន្ថែម</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("LA2Name") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA2Sex2") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA2Age") + "</td>" + "</tr>";
							SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd");

							Date dOBDate;
							try {
								dOBDate = dt.parse(oDT1.getString("LA1DateOfBirth"));
							} catch (Exception e) {
								dOBDate = new Date();
							}

							SimpleDateFormat df = new SimpleDateFormat("hhmmssSSS");
							Date date = new Date();
							String time = df.format(date);
							String lblSerialNo1 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P";

							// Particulars of Basic Plan and My Option Benefits:
							if (resultSet.next()) {
								oDT2 = (ResultSet) resultSet.getObject(1);
							}
							strText += " <tr > "
									+ "<td class='Border2' style='width:;text-align:left;'>ព័ត៌មានលម្អិតនៃគម្រោងមូលដ្ឋាន​និង<br>​លក្ខខណ្ឌបន្ថែម/ផលិតផលបំពេញបន្ថែម</td>"
									+ "<th class='Border2'>បុគ្គលដែលជាកម្មវត្ថុ​នៃការធានា</th>"
									+ "<th class='Border2'>រយៈពេលកំណត់នៃ<br>បណ្ណ​សន្យារ៉ាប់រង​​(ឆ្នាំ)    </th>"
									+ "<th class='Border2'>រយៈពេលបង់<br>បុព្វលាភរ៉ាប់រង(ឆ្នាំ)</th>"
									+ "<th class='Border2'>ទឹកប្រាក់ធានា<br>រ៉ាប់រងសរុប​ (US$)</th>"
									+ "<th class='Border2'>ប្រចាំខែ<br>(US$)</th>"
									+ "<th class='Border2'>ប្រចាំឆមាស <br>(US$)</th>"
									+ "<th class='Border2'>ប្រចាំឆ្នាំ <br>(US$)</th>" + "</tr>";
							if (oDT2.first()) {

								policyTerm = oDT2.getString("PolicyTerm");
								strText += " <tr > "
										+ "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; "
										+ oDT2.getString("ProductName") + "</td>"
										+ "<td class='Border2' style='width:'>" + oDT2.getString("Life_Assured")
										+ "</td>" + "<td class='Border2' style='width:90px'>"
										+ oDT2.getString("PolicyTerm") + "</td>"
										+ "<td class='Border2' style='width:100px'>" + oDT2.getString("PremiumTerm")
										+ "</td>" + "<td class='Border2' style='width:80px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("SA")) + "</td>"
										+ "<td class='Border2' style='width:65px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("P12_Amt")) + "</td>"
										+ "<td class='Border2' style='width:65px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("P02_Amt")) + "</td>"
										+ "<td class='Border2' style='width:65px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("P01_Amt")) + "</td>" + "</tr>";
							}
							if (resultSet.next()) {
								oDT3 = (ResultSet) resultSet.getObject(1);
							}

							// get oDT3 rows count
							if (oDT3.last())
								oDT3Row = oDT3.getRow();

							if (oDT3.first()) {

								// My Option Benefits (Also known as Riders)
								if (oDT3Row > 6) {
									strText += " <tr > "
											+ "<td class='Border2' style='width:px;text-align:left;' colspan=8>លក្ខខណ្ឌបន្ថែម/ផលិតផលបំពេញបន្ថែម</td>"
											+ "</tr>";
								}
								// oDT3.beforeFirst();
								while (oDT3.next()) {
									if (oDT3.getString("Product").equals("TT")) {
										if (oDT3.getString("ProductName").contains("%")) {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "%</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "%</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "%</th>"
													+ " </tr>";
										} else if (oDT3.isLast()) {
											String st12 = "";
											String st02 = "";
											String st01 = "";
											if (paymentMode.contains("12")) {
												st12 = "background-color:#bfc8cf;";
												st02 = "";
												st01 = "";
											} else if (paymentMode.contains("02")) {
												st12 = "";
												st02 = "background-color:#bfc8cf;";
												st01 = "";
											} else {
												st12 = "";
												st02 = "";
												st01 = "background-color:#bfc8cf;";
											}
											strText += "<tr>"
													+ "     <th class='Border2' style='width:;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:;" + st12 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:;" + st02 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:;;" + st01 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										} else {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										}
									} else if (oDT3.getString("Product").equals("TH_RTR2")) {
										strText += " <tr > "
												+ "<td class='Border2' style='width:;text-align:left;'></td>"
												+ "<th class='Border2'>បុគ្គលដែលជាកម្មវត្ថុ​នៃការធានា</th>"
												+ "<th class='Border2'>រយៈពេលធានានៃ<br>ផលិតផលបំពេញបន្ថែម​​(ឆ្នាំ)    </th>"
												+ "<th class='Border2'>រយៈពេលបង់<br>បុព្វលាភរ៉ាប់រង(ឆ្នាំ)</th>"
												+ "<th class='Border2'>អត្ថប្រយោជន៍​នៅ​កាល​បរិចេ្ឆទ​ផុត​កំណត់​នៃ​បណ្ណ​សន្យា​រ៉ាប់រង(US$)</th>"
												+ "<th class='Border2'>ប្រចាំខែ<br>(US$)</th>"
												+ "<th class='Border2'>ប្រចាំឆមាស <br>(US$)</th>"
												+ "<th class='Border2'>ប្រចាំឆ្នាំ <br>(US$)</th>" + "</tr>";
									} else {
										strText += " <tr > "
												+ "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; "
												+ oDT3.getString("ProductName") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("Life_Assured")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ oDT3.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("PremiumTerm")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("SA")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</td>"
												+ "</tr>";
									}
								}

								oDT3.beforeFirst();

								while (oDT3.next()) {

								}

							}
							strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody1- 1. Guaranteed Maturity Benefit with the Basic Policy that
							// can be Used as the Education Fund for the Child"
							ResultSet oDT4 = null;
							if (resultSet.next()) {
								oDT4 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>&nbsp;</th>"
									+ "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់</th>"
									+ "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់បង់ជាដំណាក់កាល</th>"
									+ "</tr>" + "<tr>"
									+ "<th class='Border2' rowspan=2 style='width:60px;' style='background-color:#bfc8cf;'>ឆ្នាំបណ្ណធានា<BR>រ៉ាប់រង</th>"
									+ "<th class='Border2' rowspan=2 style='width:60px' style='background-color:#bfc8cf;'>អាយុ</th>"
									+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>ការបង់ជាសរុប</th>"
									+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>សរុប</th>"
									+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>ការបង់ជាដំណាក់កាល</th>"
									+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>សរុប</th>"
									+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
							while (oDT4.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:'>" + oDT4.getString("PolicyTerm")
										+ "</td>" + "<td class='Border2' style='width:'>" + oDT4.getString("Age")
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_LumpSum")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_TT")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT2_Inst")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_Inst_TT")) + "</td>"
										+ "</tr>";
							}
							strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody2 -2. Life Insurance Benefit, that Helps the Family Take Care
							// of the Immediate Liabilities"
							ResultSet oDT5 = null;
							if (resultSet.next()) {
								oDT5 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>ឆ្នាំបណ្ណសន្យា<BR>រ៉ាប់រង</th>"
									+ "<th class='Border2' rowspan=3 style='width:60px'>អាយុ</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px'>បុព្វលាភរ៉ាប់រង<BR>រាល់ឆ្នាំ</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px'>បុព្វលាភរ៉ាប់រងសរុប</th>"
									+ "<th class='Border2' colspan=2 style='width:'>អត្ថប្រយោជន៍ក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px'>តម្លៃទឹកប្រាក់ដែលមាន<BR>ការធានា (នៅពេលបោះបង់<BR>បណ្ណសន្យារ៉ាប់រង)(GCV)</th>"
									+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:138px'>មិនបណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
									+ "<th class='Border2' style='width:138px'>បណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
									+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
							while (oDT5.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:'>" + oDT5.getString("PolicyYear")
										+ "</td>" + "<td class='Border2' style='width:'>" + oDT5.getString("Age")
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ (String.format("%,.2f", oDT5.getBigDecimal("P_APE")).equalsIgnoreCase("0.00")
												? " "
												: String.format("%,.2f", oDT5.getBigDecimal("P_APE")))
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("P_Total")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Non_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("Surrender")) + "</td>" + "</tr>";
							}
							oDT5.last();
							strText += "<tr>"
									+ "<td class='Border2' style='width:;height:12px;text-align:left;' colspan=6><b>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់នៃបណ្ណសន្យារ៉ាប់រង (នឹងត្រូវបានទូទាត់ជូននៅថ្ងៃដែលរយៈពេលកំណត់នៃបណ្ណសន្យារ៉ាប់រងផុតកំណត់ ល្កឹកណាបណ្ណសន្យារ៉ាប់រង នៅតែមានសុពលភាពនៅកាលបរិច្ឆេទនោះ) (US$)</td>"
									+ "<td class='Border2' style='width:110px'><b>"
									+ String.format("%,.2f", oDT5.getBigDecimal("Surrender")) + "</td>" + "</tr>";
							strText += "</table>";
							results.add(strText);
							// #endregion

							// #region "lblBody3 -3. Benefits with Additional Riders, that Help the Family
							// in the Further Securing Child's Education and Future"
							ResultSet oDT6 = null;
							if (resultSet.next()) {
								oDT6 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							if (oDT6.first()) {
								strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
										+ "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:160px;text-align:center;' colspan=1 rowspan=3>លក្ខខណ្ឌបន្ថែម	</th>"
										+ "<th class='Border2' style='width:110px;' rowspan=3>បុគ្គលដែលជាកម្មវត្ថុនៃការធានា</th>"
										+ "<th class='Border2' style='width:' colspan=2>អត្ថប្រយោជន៍ក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍</th>"
										+ "<th class='Border2' style='width:260px' rowspan=3>សេចក្តីសម្រាយ</th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:138px' >មិនបណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
										+ "<th class='Border2' style='width:138px' >បណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
										+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:' >(US$)</th>"
										+ "<th class='Border2' style='width:' >(US$)</th>" + "</tr>";
								oDT6.beforeFirst();
								while (oDT6.next()) {

									strText += "<tr>"
											+ "<td class='Border2' style='width:160px;text-align:left;' colspan=1 >"
											+ oDT6.getString("ProductName") + "</td>"
											+ "<td class='Border2' style='width:110px' colspan=1 >"
											+ oDT6.getString("Life_Assured") + "</td>"
											+ "<td class='Border2' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Non_Accident")) + "</td>"
											+ "<td class='Border2' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Accident")) + "</td>"
											+ "<td class='Border2' style='width:260px' >"
											+ oDT6.getString("Payment_Terms") + "</td>" + "</tr>";
								}
								strText += "</table>";
							}

							results.add(strText);
							// #endregion

							//

							// #region "lblBody4- 4. Guaranteed Maturity Benefit with the PruSaver"
							ResultSet oDT7 = null;
							if (resultSet.next()) {
								oDT7 = (ResultSet) resultSet.getObject(1);
								if (oDT7.first()) {
									strText = "";
									strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' rowspan=3 style='width:60px;' style='background-color:#bfc8cf;'>ឆ្នាំ​នៃ​ផលិតផលបំពេញបន្ថែម</th>"
											+ "<th class='Border2' rowspan=3 style='width:60px' style='background-color:#bfc8cf;'>អាយុ</th>"
											+
									/*
									 * "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>&nbsp;</th>"
									 * +
									 */
									"<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់នៃបណ្ណសន្យារ៉ាប់រង</th>"
											+ "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់នៃបណ្ណសន្យារ៉ាប់រងដែលបង់ជាដំណាក់កាល</th>"
											+ "</tr>" + "<tr>" +

											"<th class='Border2' style='width:173px;background-color:#bfc8cf;'>ការបង់ជាសរុប</th>"
											+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>សរុប</th>"
											+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>ការបង់ជាដំណាក់កាល</th>"
											+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>សរុប</th>"
											+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
									oDT7.beforeFirst();
									while (oDT7.next()) {
										strText += "<tr>" + "<td class='Border2' style='width:;'>"
												+ oDT7.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT7.getString("Age")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ (String.format("%,.2f", oDT7.getBigDecimal("OPT1_LumpSum"))
														.equals("0.00")
																? " "
																: String.format("%,.2f",
																		oDT7.getBigDecimal("OPT1_LumpSum")))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ (String.format("%,.2f", oDT7.getBigDecimal("OPT1_TT")).equals("0.00")
														? " "
														: String.format("%,.2f", oDT7.getBigDecimal("OPT1_TT")))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT7.getBigDecimal("OPT2_Inst")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT7.getBigDecimal("OPT1_Inst_TT")) + "</td>"
												+ "</tr>";
									}
									strText += "</table>";
									// body1
									results.add(strText);
								}
							} else {
								results.add("");
							}

							// #endregion

							// #region "lblBody5 -5. Life Insurance Benefit with PruSaver"
							ResultSet oDT8 = null;
							if (resultSet.next()) {
								oDT8 = (ResultSet) resultSet.getObject(1);
								if (oDT8.first()) {
									strText = "";
									strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' rowspan=3 style='width:60px;'>ឆ្នាំ​នៃ​ផលិតផលបំពេញបន្ថែម</th>"
											+ "<th class='Border2' rowspan=3 style='width:60px'>អាយុ</th>"
											+ "<th class='Border2' rowspan=2 style='width:128px'>បុព្វលាភរ៉ាប់រងរាល់ឆ្នាំ</th>"
											+ "<th class='Border2' rowspan=2 style='width:128px'>បុព្វលាភរ៉ាប់រងសរុប</th>"
											+ "<th class='Border2' colspan=2 style='width:'>អត្ថប្រយោជន៍ក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px'>តម្លៃទឹកប្រាក់ដែលមាន<BR>ការធានា (នៅពេលបោះបង់<BR>ផលិតផលបំពេញបន្ថែម)(GCV)</th>"
											+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:148px'>មិនបណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
											+ "<th class='Border2' style='width:148px'>បណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
											+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
									oDT8.beforeFirst();
									while (oDT8.next()) {
										strText += "<tr>" + "<td class='Border2' style='width:'>"
												+ oDT8.getString("PolicyYear") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT8.getString("Age")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_APE")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_Total")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Non_Accident"))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Accident")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("Surrender")) + "</td>"
												+ "</tr>";
									}
									strText += "</table>";
									// body2
									results.add(strText);
									oDT7.close();
									oDT8.close();
								}
							} else {
								results.add("");
							}
							results.add(lblSerialNo1);

							oDT1.close();
							oDT2.close();
							oDT3.close();
							oDT4.close();
							oDT5.close();
							oDT6.close();

						} finally {
							cstmt.close();
						}

						return results; // this should contain All rows or objects you need for further processing
					}
				});
		return res;
	}

	@Transactional
	public List<String> generateMyFamilyLatinQuote(final Long quoteId, final String lang, final String result) {
		List<String> res = sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<String> /* objectType returned */>() {
					@Override
					/* or object type you need to return to process */
					public List<String> execute(Connection conn) throws SQLException {
						PreparedStatement cstmt = conn.prepareStatement(
								"select * from ap.spGen_PruQuote(?,?,'RN','R1','R2','R3','R4','R5','R6','R7','R8')",
								ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
						@SuppressWarnings("unused")
						String policyTerm = "";
						String paymentMode = "";
						int oDT3Row = 0;
						cstmt.setLong(1, quoteId);
						cstmt.setString(2, lang);
						// cstmt.setString(3, result);
						// Result list that would return ALL rows of ALL result sets
						List<String> results = new ArrayList<String>();
						String strText = "";
						ResultSet resultSet = null;
						ResultSet oDT1 = null;
						ResultSet oDT2 = null;
						ResultSet oDT3 = null;
						try {
							// cstmt.execute();

							// #region "lblHeader1"
							resultSet = cstmt.executeQuery();
							resultSet.next();
							oDT1 = (ResultSet) resultSet.getObject(1);
							oDT1.next();
							paymentMode = oDT1.getString("PaymentMode");
							strText += "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr>" + "<th class='Border2' style='text-align:left;'>&nbsp;</th>"
									+ "<th class='Border2' colspan='2'><u>Name</u></th>"
									+ "<th class='Border2'><u>Gender</u></th>" + "<th class='Border2'><u>Age</u></th>"
									+ "<th class='Border2' colspan='3'><u>Premium Mode</u></th>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='text-align:left;'>Policy Owner's Name:</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("POName") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POSex") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POAge") + "</td>"
									+ "<td class='Border2' colspan=3 rowspan=3>" /*
																					 * + oDT1.getString("PaymentMode2")
																					 * + "/"
																					 */ + oDT1.getString("PaymentType2")
									+ "</td>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='text-align:left;'>Main Life Assured's Name:</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("LA1Name") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Sex2") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Age") + "</td>" + "</tr>"
									+ "<tr>     "
									+ "<td class='Border2' style='text-align:left;'>Additional Life Assured's Name:</td>     "
									+ "<td class='Border2' colspan=2>" + oDT1.getString("LA2Name") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA2Sex2") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA2Age") + "</td>" + "</tr>";
							SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd");

							Date dOBDate;
							try {
								dOBDate = dt.parse(oDT1.getString("LA1DateOfBirth"));
							} catch (Exception e) {
								dOBDate = new Date();
							}

							SimpleDateFormat df = new SimpleDateFormat("hhmmssSSS");
							Date date = new Date();
							String time = df.format(date);

							String lblSerialNo1 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P1";
							String lblSerialNo2 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P2";

							// Particulars of Basic Plan and My Option Benefits:
							if (resultSet.next()) {
								oDT2 = (ResultSet) resultSet.getObject(1);
							}
							strText += " <tr > "
									+ "<td class='Border2' style='width:170px;text-align:left;'>Particulars of Basic Plan and Riders:</td>"
									+ "<th class='Border2' style='width:'>Life Assured</th>"
									+ "<th class='Border2' style='width:40px'>Policy Term</th>"
									+ "<th class='Border2' style='width:75px'>Premium Payment Term</th>"
									+ "<th class='Border2' style='width:75px'>Sum Assured (US$)</th>"
									+ "<th class='Border2' style='width:70px'>Monthly (US$)</th>"
									+ "<th class='Border2' style='width:100px'>Semi-Annual (US$)</th>"
									+ "<th class='Border2' style='width:70px'>Annual (US$)</th>" + "</tr>";
							if (oDT2.next()) {

								policyTerm = oDT2.getString("PolicyTerm");

								strText += " <tr > " + "<td class='Border2' style='text-align:left;'>&nbsp;&nbsp; "
										+ oDT2.getString("ProductName") + "</td>" + "<td class='Border2' >"
										+ oDT2.getString("Life_Assured") + "</td>" + "<td class='Border2' >"
										+ oDT2.getString("PolicyTerm") + "</td>" + "<td class='Border2' >"
										+ oDT2.getString("PremiumTerm") + "</td>" + "<td class='Border2' >"
										+ String.format("%,.2f", oDT2.getBigDecimal("SA")) + "</td>"
										+ "<td class='Border2' >"
										+ String.format("%,.2f", oDT2.getBigDecimal("P12_Amt")) + "</td>"
										+ "<td class='Border2' >"
										+ String.format("%,.2f", oDT2.getBigDecimal("P02_Amt")) + "</td>"
										+ "<td class='Border2' >"
										+ String.format("%,.2f", oDT2.getBigDecimal("P01_Amt")) + "</td>" + "</tr>";
							}

							if (resultSet.next()) {
								oDT3 = (ResultSet) resultSet.getObject(1);
							}

							// get oDT3 rows count
							if (oDT3.last())
								oDT3Row = oDT3.getRow();

							if (oDT3.first()) {

								// My Option Benefits (Also known as Riders)
								if (oDT3Row > 6) {
									strText += " <tr > "
											+ "<td class='Border2' style='width:250px;text-align:left;' colspan=8>Riders</td>"
											+ "</tr>";
								}
								// oDT3.beforeFirst();
								while (oDT3.next()) {
									if (oDT3.getString("Product").equals("TT")) {
										if (oDT3.getString("ProductName").contains("%")) {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "%</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "%</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "%</th>"
													+ " </tr>";
										} else if (oDT3.isLast()) {
											String st12 = "";
											String st02 = "";
											String st01 = "";
											if (paymentMode.contains("12")) {
												st12 = "background-color:#bfc8cf;";
												st02 = "";
												st01 = "";
											} else if (paymentMode.contains("02")) {
												st12 = "";
												st02 = "background-color:#bfc8cf;";
												st01 = "";
											} else {
												st12 = "";
												st02 = "";
												st01 = "background-color:#bfc8cf;";
											}
											strText += "<tr>"
													+ "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:;" + st12 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:;" + st02 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:;;" + st01 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										} else {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										}
									} else if (oDT3.getString("Product").equals("TH_RTR2")) {
										strText += " <tr > "
												+ "<td class='Border2' style='width:170px;text-align:left;'>Additional Riders:</td>"
												+ "<th class='Border2' style='width:'>Life Assured</th>"
												+ "<th class='Border2' style='width:40px'>Policy Term</th>"
												+ "<th class='Border2' style='width:75px'>Premium Payment Term</th>"
												+ "<th class='Border2' style='width:75px'>Maturity Benifit (US$)</th>"
												+ "<th class='Border2' style='width:70px'>Monthly (US$)</th>"
												+ "<th class='Border2' style='width:100px'>Semi-Annual (US$)</th>"
												+ "<th class='Border2' style='width:70px'>Annual (US$)</th>" + "</tr>";
									} else {
										strText += " <tr > "
												+ "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; "
												+ oDT3.getString("ProductName") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("Life_Assured")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ oDT3.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("PremiumTerm")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("SA")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</td>"
												+ "</tr>";
									}
								}

								oDT3.beforeFirst();

								while (oDT3.next()) {

								}

							}
							strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody1- 1. Guaranteed Maturity Benefit with the Basic Policy that
							// can be Used as the Education Fund for the Child"
							ResultSet oDT4 = null;
							if (resultSet.next()) {
								oDT4 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='text-align:center;height:30px;vertical-align:middle;' colspan=6>Maturity Benefit Settlement Option</th>"
									+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' rowspan=2 style='width:90px;'>Policy Year</th>"
									+ "<th class='Border2' rowspan=2 style='width:160px'>Age</th>"
									+ "<th class='Border2' class='Border2' style='width:140px'>Option #1:Lump sum</th>"
									+ "<th class='Border2' style='width:140px'>Total</th>"
									+ "<th class='Border2' style='width:140px'>Option #2:Installment</th>"
									+ "<th class='Border2' style='width:140px'>Total</th>" + "</tr>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:140px'>(US$)</th>"
									+ "<th class='Border2' style='width:140px'>(US$)</th>"
									+ "<th class='Border2' style='width:140px'>(US$)</th>"
									+ "<th class='Border2' style='width:140px'>(US$)</th>" + "</tr>";
							while (oDT4.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:90px'>"
										+ oDT4.getString("PolicyTerm") + "</td>"
										+ "<td class='Border2' style='width:160px'>" + oDT4.getString("Age") + "</td>"
										+ "<td class='Border2' style='width:140px'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_LumpSum")) + "</td>"
										+ "<td class='Border2' style='width:140px'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_TT")) + "</td>"
										+ "<td class='Border2' style='width:140px'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT2_Inst")) + "</td>"
										+ "<td class='Border2' style='width:140px'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_Inst_TT")) + "</td>"
										+ "</tr>";
							}
							strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody2 -2. Life Insurance Benefit, that Helps the Family Take Care
							// of the Immediate Liabilities"
							ResultSet oDT5 = null;
							if (resultSet.next()) {
								oDT5 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>Policy Year</th>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px;'>Premium every<br/>year</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px;'>Total<br/>Premium</th>"
									+ "<th class='Border2' colspan=2 style='width:276px;'>Death/TPD Benefit</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px;'>Guaranteed Cash Value(GCV)</th>"
									+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>Not Caused by an accident</th>"
									+ "<th class='Border2' style='width:'>Caused by an accident</th>" + "</tr>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";

							while (oDT5.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:'>" + oDT5.getString("PolicyYear")
										+ "</td>" + "<td class='Border2' style='width:'>" + oDT5.getString("Age")
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ (String.format("%,.2f", oDT5.getBigDecimal("P_APE")).equalsIgnoreCase("0.00")
												? " "
												: String.format("%,.2f", oDT5.getBigDecimal("P_APE")))
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("P_Total")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Non_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("Surrender")) + "</td>" + "</tr>";
							}
							oDT5.last();
							strText += "<tr>"
									+ "<td class='Border2' style='height:12px;text-align:left;' colspan=6><b>Maturity Benefit (Payable at the time of maturity, provided the policy is in force at that time) (US$)</td>"
									+ "<td class='Border2' style='width:138px'><b>"
									+ String.format("%,.2f", oDT5.getBigDecimal("Surrender")) + "</td>" + "</tr>";
							strText += "</table>";
							results.add(strText);
							// #endregion

							// #region "lblBody3 -3. Benefits with Additional Riders, that Help the Family
							// in the Further Securing Child's Education and Future"
							ResultSet oDT6 = null;
							if (resultSet.next()) {
								oDT6 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							if (oDT6.first()) {
								strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
										+ "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:160px;text-align:center;' colspan=1 rowspan=3>Rider Benefits</th>"
										+ "<th class='Border2' style='width:90px' rowspan=3>Life Assured Name</th>"
										+ "<th class='Border2' style='width:280px' colspan=2>Death/TPD Benefit</th>"
										+ "<th class='Border2' style='width:280px' rowspan=3>Benefit Payment Terms</th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:140px' >Not Caused by an Accident</th>"
										+ "<th class='Border2' style='width:140px' >Caused by an Accident</th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:140px' >(US$)</th>"
										+ "<th class='Border2' style='width:140px' >(US$)</th>" + "</tr>";
								oDT6.beforeFirst();
								while (oDT6.next()) {

									strText += "<tr>"
											+ "<td class='Border2' style='width:160px;text-align:left;' colspan=1 >"
											+ oDT6.getString("ProductName") + "</td>"
											+ "<td class='Border2' style='width:110px' colspan=1 >"
											+ oDT6.getString("Life_Assured") + "</td>"
											+ "<td class='Border2' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Non_Accident")) + "</td>"
											+ "<td class='Border2' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Accident")) + "</td>"
											+ "<td class='Border2' style='width:260px' >"
											+ oDT6.getString("Payment_Terms") + "</td>" + "</tr>";
								}
								strText += "</table>";
							}

							results.add(strText);
							// #endregion

							// #region "lblBody4- 4. Guaranteed Maturity Benefit with the PruSaver"
							ResultSet oDT7 = null;
							if (resultSet.next()) {
								oDT7 = (ResultSet) resultSet.getObject(1);
								String cursorName = oDT7.getCursorName();
								System.out.println(cursorName);
								if (oDT7.first()) {
									strText = "";
									strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
											+ "<tr style='background-color:#bfc8cf;'>" +
									/*
									 * "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>&nbsp;</th>"
									 * +
									 */
									"<th class='Border2' rowspan=3 style='width:60px;'>Rider Year</th>"
											+ "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>"
											+ "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>Maturity Benefit</th>"
											+ "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>Maturity Benefit with Settlement Option</th>"
											+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:173px'>Lump Sum Payment</th>"
											+ "<th class='Border2' style='width:173px'>Total</th>"
											+ "<th class='Border2' style='width:173px'>Installment Payment</th>"
											+ "<th class='Border2' style='width:173px'>Total</th>" + "</tr>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
									oDT7.beforeFirst();
									while (oDT7.next()) {
										strText += "<tr>" + "<td class='Border2' style='width:60px;'>"
												+ oDT7.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:60px'>" + oDT7.getString("Age")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ (String.format("%,.2f", oDT7.getBigDecimal("OPT1_LumpSum"))
														.equals("0.00")
																? " "
																: String.format("%,.2f",
																		oDT7.getBigDecimal("OPT1_LumpSum")))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ (String.format("%,.2f", oDT7.getBigDecimal("OPT1_TT")).equals("0.00")
														? " "
														: String.format("%,.2f", oDT7.getBigDecimal("OPT1_TT")))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT7.getBigDecimal("OPT2_Inst")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT7.getBigDecimal("OPT1_Inst_TT")) + "</td>"
												+ "</tr>";
									}
									strText += "</table>";
									// body4
									results.add(strText);
								}
							} else {
								results.add("");
							}

							// #endregion

							// #region "lblBody5 -5. Life Insurance Benefit with PruSaver"
							ResultSet oDT8 = null;
							if (resultSet.next()) {
								oDT8 = (ResultSet) resultSet.getObject(1);
								if (oDT8.first()) {
									strText = "";
									strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' rowspan=3 style='width:60px;'>Rider Year</th>"
											+ "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px;'>Rider Premium<br/>every year</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px;'>Total Rider Premium</th>"
											+ "<th class='Border2' colspan=2 style='width:276px;'>Death/TPD Benefit</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px;'>Guaranteed Cash Value(GCV)</th>"
											+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>Not Caused by an Accident</th>"
											+ "<th class='Border2' style='width:'>Caused by an Accident</th>" + "</tr>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
									oDT8.beforeFirst();
									while (oDT8.next()) {
										strText += "<tr>" + "<td class='Border2' style='width:'>"
												+ oDT8.getString("PolicyYear") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT8.getString("Age")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_APE")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_Total")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Non_Accident"))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Accident")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("Surrender")) + "</td>"
												+ "</tr>";
									}
									strText += "</table>";
									// body5
									results.add(strText);
									oDT7.close();
									oDT8.close();
								}
							} else {
								results.add("");
							}

							//
							results.add(lblSerialNo1);
							results.add(lblSerialNo2);

							oDT1.close();
							oDT2.close();
							oDT3.close();
							oDT4.close();
							oDT5.close();
							oDT6.close();

						} finally {
							cstmt.close();
						}

						return results; // this should contain All rows or objects you need for further processing
					}
				});
		return res;
	}

	// **********************************
	// * Easy Life Product *
	// **********************************
	@Transactional
	public List<String> generateEasyLifeKhmerQuote(final Long quoteId, final String lang, final String result) {
		List<String> res = sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<String> /* objectType returned */>() {
					@Override
					/* or object type you need to return to process */
					public List<String> execute(Connection conn) throws SQLException {
						PreparedStatement cstmt = conn.prepareStatement(
								"select * from ap.spgen_pruquote_easylife(?,?,'RN','R1','R2','R3','R4','R5','R6','R7','R8')",
								ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
						@SuppressWarnings("unused")
						String policyTerm = "";
						String paymentMode = "";
						int oDT3Row = 0;
						cstmt.setLong(1, quoteId);
						cstmt.setString(2, lang);
						// cstmt.setString(3, result);
						// Result list that would return ALL rows of ALL result sets
						List<String> results = new ArrayList<String>();
						String strText = "";
						ResultSet resultSet = null;
						ResultSet oDT1 = null;
						ResultSet oDT2 = null;
						ResultSet oDT3 = null;
						try {
							resultSet = cstmt.executeQuery();

							// #region "lblHeader1"
							resultSet.next();
							oDT1 = (ResultSet) resultSet.getObject(1);
							oDT1.first();
							paymentMode = oDT1.getString("PaymentMode");
							strText += "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr>" + "<th class='Border2' style='width:230px;text-align:left;'>&nbsp;</th>"
									+ "<th class='Border2' colspan='2'>ឈ្មោះ</th>" + "<th class='Border2'>ភេទ</th>"
									+ "<th class='Border2'>អាយុ</th>"
									+ "<th class='Border2' colspan='2'>របៀបទូទាត់</th>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='width:;text-align:left;'>ឈ្មោះអ្នកត្រូវបានធានារ៉ាប់រង</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("POName") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POSex") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POAge") + "</td>"
									+ "<td class='Border2' colspan=2 rowspan=2>" /*
																					 * + oDT1.getString("PaymentMode2")
																					 * + "/"
																					 */ + oDT1.getString("PaymentType2")
									+ "</td>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='width:;text-align:left;'>ឈ្មោះបុគ្គលដែលជាកម្មវត្ថុនៃការធានា</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("LA1Name") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Sex2") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Age") + "</td>" + "</tr>";
							SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd");

							Date dOBDate;
							try {
								dOBDate = dt.parse(oDT1.getString("LA1DateOfBirth"));
							} catch (Exception e) {
								dOBDate = new Date();
							}

							SimpleDateFormat df = new SimpleDateFormat("hhmmssSSS");
							Date date = new Date();
							String time = df.format(date);

							String lblSerialNo1 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P";

							// Particulars of Basic Plan and My Option Benefits:
							if (resultSet.next()) {
								oDT2 = (ResultSet) resultSet.getObject(1);
							}
							strText += " <tr > "
									+ "<td class='Border2' style='width:;text-align:left;'>ព័ត៌មានលម្អិតនៃគម្រោង</td>"
									+ "<th class='Border2'>បុគ្គលដែលជាកម្មវត្ថុ​នៃការធានា</th>"
									+ "<th class='Border2'>រយៈពេលកំណត់នៃ<br>បណ្ណ​សន្យារ៉ាប់រង​​(ឆ្នាំ)    </th>"
									+ "<th class='Border2'>រយៈពេលបង់<br>បុព្វលាភរ៉ាប់រង(ឆ្នាំ)</th>"
									+ "<th class='Border2'>ទឹកប្រាក់ធានា<br>រ៉ាប់រងសរុប​ (US$)</th>"
									+ "<th class='Border2'>ប្រចាំឆមាស <br>(US$)</th>"
									+ "<th class='Border2'>ប្រចាំឆ្នាំ <br>(US$)</th>" + "</tr>";
							if (oDT2.first()) {

								policyTerm = oDT2.getString("PolicyTerm");
								strText += " <tr > "
										+ "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; "
										+ oDT2.getString("ProductName") + "</td>"
										+ "<td class='Border2' style='width:'>" + oDT2.getString("Life_Assured")
										+ "</td>" + "<td class='Border2' style='width:90px'>"
										+ oDT2.getString("PolicyTerm") + "</td>"
										+ "<td class='Border2' style='width:100px'>" + oDT2.getString("PremiumTerm")
										+ "</td>" + "<td class='Border2' style='width:80px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("SA")) + "</td>"
										+ "<td class='Border2' style='width:65px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("P02_Amt")) + "</td>"
										+ "<td class='Border2' style='width:65px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("P01_Amt")) + "</td>" + "</tr>";
							}
							if (resultSet.next()) {
								oDT3 = (ResultSet) resultSet.getObject(1);
							}

							// get oDT3 rows count
							if (oDT3.last())
								oDT3Row = oDT3.getRow();

							if (oDT3.first()) {

								// My Option Benefits (Also known as Riders)
								if (oDT3Row > 6) {
									strText += " <tr > "
											+ "<td class='Border2' style='width:px;text-align:left;' colspan=8>លក្ខខណ្ឌបន្ថែម/ផលិតផលបំពេញបន្ថែម</td>"
											+ "</tr>";
								}
								// oDT3.beforeFirst();
								while (oDT3.next()) {
									if (oDT3.getString("Product").equals("TT")) {
										if (oDT3.getString("ProductName").contains("%")) {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "%</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "%</th>"
													+ " </tr>";
										} else if (oDT3.isLast()) {
											String st12 = "";
											String st02 = "";
											String st01 = "";
											if (paymentMode.contains("12")) {
												st12 = "background-color:#bfc8cf;border:2px solid black;margin:2px;font-size:14px;";
												st02 = "";
												st01 = "";
											} else if (paymentMode.contains("02")) {
												st12 = "";
												st02 = "background-color:#bfc8cf;border:2px solid black;margin:2px;font-size:14px;";
												st01 = "";
											} else {
												st12 = "";
												st02 = "";
												st01 = "background-color:#bfc8cf;border:2px solid black;margin:2px;font-size:14px;";
											}
											strText += "<tr>"
													+ "     <th class='Border2' style='width:;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:;" + st02 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:;;" + st01 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										} else {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										}
									} else if (oDT3.getString("Product").equals("TH_RTR2")) {
										strText += " <tr > "
												+ "<td class='Border2' style='width:;text-align:left;'></td>"
												+ "<th class='Border2'>បុគ្គលដែលជាកម្មវត្ថុ​នៃការធានា</th>"
												+ "<th class='Border2'>រយៈពេលធានានៃ<br>ផលិតផលបំពេញបន្ថែម​​(ឆ្នាំ)    </th>"
												+ "<th class='Border2'>រយៈពេលបង់<br>បុព្វលាភរ៉ាប់រង(ឆ្នាំ)</th>"
												+ "<th class='Border2'>អត្ថប្រយោជន៍​នៅ​កាល​បរិចេ្ឆទ​ផុត​កំណត់​នៃ​បណ្ណ​សន្យា​រ៉ាប់រង(US$)</th>"
												+ "<th class='Border2'>ប្រចាំឆមាស <br>(US$)</th>"
												+ "<th class='Border2'>ប្រចាំឆ្នាំ <br>(US$)</th>" + "</tr>";
									} else {
										strText += " <tr > "
												+ "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; "
												+ oDT3.getString("ProductName") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("Life_Assured")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ oDT3.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("PremiumTerm")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("SA")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</td>"
												+ "</tr>";
									}
								}

								oDT3.beforeFirst();

								while (oDT3.next()) {

								}

							}
							strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody1- 1. Guaranteed Maturity Benefit with the Basic Policy that
							// can be Used as the Education Fund for the Child"
							ResultSet oDT4 = null;
							if (resultSet.next()) {
								oDT4 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>&nbsp;</th>"
									+ "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់</th>"
									+ "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់បង់ជាដំណាក់កាល</th>"
									+ "</tr>" + "<tr>"
									+ "<th class='Border2' rowspan=2 style='width:60px;' style='background-color:#bfc8cf;'>ឆ្នាំបណ្ណធានា<BR>រ៉ាប់រង</th>"
									+ "<th class='Border2' rowspan=2 style='width:60px' style='background-color:#bfc8cf;'>អាយុ</th>"
									+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>ការបង់ជាសរុប</th>"
									+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>សរុប</th>"
									+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>ការបង់ជាដំណាក់កាល</th>"
									+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>សរុប</th>"
									+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
							while (oDT4.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:'>" + oDT4.getString("PolicyTerm")
										+ "</td>" + "<td class='Border2' style='width:'>" + oDT4.getString("Age")
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_LumpSum")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_TT")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT2_Inst")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_Inst_TT")) + "</td>"
										+ "</tr>";
							}
							strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody2 -2. Life Insurance Benefit, that Helps the Family Take Care
							// of the Immediate Liabilities"
							ResultSet oDT5 = null;
							if (resultSet.next()) {
								oDT5 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>ឆ្នាំបណ្ណសន្យា<BR>រ៉ាប់រង</th>"
									+ "<th class='Border2' rowspan=3 style='width:60px'>អាយុ</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px'>បុព្វលាភរ៉ាប់រង<BR>រាល់ឆ្នាំ</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px'>បុព្វលាភរ៉ាប់រងសរុប</th>"
									+ "<th class='Border2' colspan=2 style='width:'>អត្ថប្រយោជន៍ក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px'>តម្លៃទឹកប្រាក់ដែលមាន<BR>ការធានា (នៅពេលបោះបង់<BR>បណ្ណសន្យារ៉ាប់រង)(GCV)</th>"
									+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:138px'>មិនបណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
									+ "<th class='Border2' style='width:138px'>បណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
									+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
							while (oDT5.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:'>" + oDT5.getString("PolicyYear")
										+ "</td>" + "<td class='Border2' style='width:'>" + oDT5.getString("Age")
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ (String.format("%,.2f", oDT5.getBigDecimal("P_APE")).equalsIgnoreCase("0.00")
												? " "
												: String.format("%,.2f", oDT5.getBigDecimal("P_APE")))
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("P_Total")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Non_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("Surrender")) + "</td>" + "</tr>";
							}

							if (oDT5.last()) {
								strText += "<tr>"
										+ "<td class='Border2' style='width:;height:12px;text-align:left;' colspan=6><b>អត្ថប្រយោជន៍ពេលមិនមានការទាមទារសំណង (នឹងត្រូវបានទូទាត់ជូននៅថ្ងៃដែលរយៈពេលកំណត់នៃបណ្ណសន្យារ៉ាប់រងផុតកំណត់ ល្គឹកណាបណ្ណសន្យារ៉ាប់រងនៅតែមានសុពលភាពនៅកាលបរិច្ឆេទនោះ) (US$)</td>"
										+ "<td class='Border2' style='width:110px'><b>"
										+ String.format("%,.2f", oDT5.getBigDecimal("Surrender")) + "</td>" + "</tr>";
							}

							strText += "</table>";
							results.add(strText);
							// #endregion

							// #region "lblBody3 -3. Benefits with Additional Riders, that Help the Family
							// in the Further Securing Child's Education and Future"
							ResultSet oDT6 = null;
							if (resultSet.next()) {
								oDT6 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							if (oDT6.first()) {
								strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
										+ "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:160px;text-align:center;' colspan=1 rowspan=3>លក្ខខណ្ឌបន្ថែម	</th>"
										+ "<th class='Border2' style='width:110px;' rowspan=3>បុគ្គលដែលជាកម្មវត្ថុនៃការធានា</th>"
										+ "<th class='Border2' style='width:' colspan=2>អត្ថប្រយោជន៍ក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍</th>"
										+ "<th class='Border2' style='width:260px' rowspan=3>សេចក្តីសម្រាយ</th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:138px' >មិនបណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
										+ "<th class='Border2' style='width:138px' >បណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
										+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:' >(US$)</th>"
										+ "<th class='Border2' style='width:' >(US$)</th>" + "</tr>";
								oDT6.beforeFirst();
								while (oDT6.next()) {

									strText += "<tr>"
											+ "<td class='Border2' style='width:160px;text-align:left;' colspan=1 >"
											+ oDT6.getString("ProductName") + "</td>"
											+ "<td class='Border2' style='width:110px' colspan=1 >"
											+ oDT6.getString("Life_Assured") + "</td>"
											+ "<td class='Border2' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Non_Accident")) + "</td>"
											+ "<td class='Border2' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Accident")) + "</td>"
											+ "<td class='Border2' style='width:260px' >"
											+ oDT6.getString("Payment_Terms") + "</td>" + "</tr>";
								}
								strText += "</table>";
							}

							results.add(strText);
							// #endregion

							//
							results.add(lblSerialNo1);

							oDT1.close();
							oDT2.close();
							oDT3.close();
							oDT4.close();
							oDT5.close();
							oDT6.close();

						} finally {
							cstmt.close();
						}

						return results; // this should contain All rows or objects you need for further processing
					}
				});
		return res;
	}

	@Transactional
	public List<String> generateEasyLifeLatinQuote(final Long quoteId, final String lang, final String result) {
		List<String> res = sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<String> /* objectType returned */>() {
					@Override
					/* or object type you need to return to process */
					public List<String> execute(Connection conn) throws SQLException {
						PreparedStatement cstmt = conn.prepareStatement(
								"select * from ap.spgen_pruquote_easylife(?,?,'RN','R1','R2','R3','R4','R5','R6','R7','R8')",
								ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
						@SuppressWarnings("unused")
						String policyTerm = "";
						String paymentMode = "";
						int oDT3Row = 0;
						cstmt.setLong(1, quoteId);
						cstmt.setString(2, lang);
						// cstmt.setString(3, result);
						// Result list that would return ALL rows of ALL result sets
						List<String> results = new ArrayList<String>();
						String strText = "";
						ResultSet resultSet = null;
						ResultSet oDT1 = null;
						ResultSet oDT2 = null;
						ResultSet oDT3 = null;
						try {
							resultSet = cstmt.executeQuery();

							// #region "lblHeader1"
							resultSet.next();
							oDT1 = (ResultSet) resultSet.getObject(1);
							oDT1.first();
							paymentMode = oDT1.getString("PaymentMode");
							strText += "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr>" + "<th class='Border2' style='text-align:left;'>&nbsp;</th>"
									+ "<th class='Border2' colspan='2'><u>Name</u></th>"
									+ "<th class='Border2'><u>Gender</u></th>" + "<th class='Border2'><u>Age</u></th>"
									+ "<th class='Border2' colspan='2'><u>Premium Mode</u></th>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='text-align:left;'>Policy Owner's Name:</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("POName") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POSex") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POAge") + "</td>"
									+ "<td class='Border2' colspan=2 rowspan=2>" /*
																					 * + oDT1.getString("PaymentMode2")
																					 * + "/"
																					 */ + oDT1.getString("PaymentType2")
									+ "</td>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='text-align:left;'>Life Assured's Name:</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("LA1Name") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Sex2") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Age") + "</td>" + "</tr>";
							SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd");

							Date dOBDate;
							try {
								dOBDate = dt.parse(oDT1.getString("LA1DateOfBirth"));
							} catch (Exception e) {
								dOBDate = new Date();
							}

							SimpleDateFormat df = new SimpleDateFormat("hhmmssSSS");
							Date date = new Date();
							String time = df.format(date);

							String lblSerialNo1 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P1";
							String lblSerialNo2 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P2";

							// Particulars of Basic Plan and My Option Benefits:
							if (resultSet.next()) {
								oDT2 = (ResultSet) resultSet.getObject(1);
							}
							strText += " <tr > "
									+ "<td class='Border2' style='width:170px;text-align:left;'>Particulars of Plan:</td>"
									+ "<th class='Border2' style='width:'>Life Assured</th>"
									+ "<th class='Border2' style='width:40px'>Policy Term</th>"
									+ "<th class='Border2' style='width:75px'>Premium Payment Term</th>"
									+ "<th class='Border2' style='width:75px'>Sum Assured (US$)</th>"
									+ "<th class='Border2' style='width:100px'>Semi-Annual (US$)</th>"
									+ "<th class='Border2' style='width:70px'>Annual (US$)</th>" + "</tr>";
							if (oDT2.first()) {

								policyTerm = oDT2.getString("PolicyTerm");

								strText += " <tr > " + "<td class='Border2' style='text-align:left;'>&nbsp;&nbsp; "
										+ oDT2.getString("ProductName") + "</td>" + "<td class='Border2' >"
										+ oDT2.getString("Life_Assured") + "</td>" + "<td class='Border2' >"
										+ oDT2.getString("PolicyTerm") + "</td>" + "<td class='Border2' >"
										+ oDT2.getString("PremiumTerm") + "</td>" + "<td class='Border2' >"
										+ String.format("%,.2f", oDT2.getBigDecimal("SA")) + "</td>"
										+ "<td class='Border2' >"
										+ String.format("%,.2f", oDT2.getBigDecimal("P02_Amt")) + "</td>"
										+ "<td class='Border2' >"
										+ String.format("%,.2f", oDT2.getBigDecimal("P01_Amt")) + "</td>" + "</tr>";
							}

							if (resultSet.next()) {
								oDT3 = (ResultSet) resultSet.getObject(1);
							}

							// get oDT3 rows count
							if (oDT3.last())
								oDT3Row = oDT3.getRow();

							if (oDT3.first()) {

								// My Option Benefits (Also known as Riders)
								if (oDT3Row > 6) {
									strText += " <tr > "
											+ "<td class='Border2' style='width:250px;text-align:left;' colspan=8>Riders</td>"
											+ "</tr>";
								}
								// oDT3.beforeFirst();
								while (oDT3.next()) {
									if (oDT3.getString("Product").equals("TT")) {
										if (oDT3.getString("ProductName").contains("%")) {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "%</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "%</th>"
													+ " </tr>";
										} else if (oDT3.isLast()) {
											String st12 = "";
											String st02 = "";
											String st01 = "";
											if (paymentMode.contains("12")) {
												st12 = "background-color:#bfc8cf;";
												st02 = "";
												st01 = "";
											} else if (paymentMode.contains("02")) {
												st12 = "";
												st02 = "background-color:#bfc8cf;";
												st01 = "";
											} else {
												st12 = "";
												st02 = "";
												st01 = "background-color:#bfc8cf;";
											}
											strText += "<tr>"
													+ "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:;" + st02 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:;;" + st01 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										} else {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										}
									} else if (oDT3.getString("Product").equals("TH_RTR2")) {
										strText += " <tr > "
												+ "<td class='Border2' style='width:170px;text-align:left;'>Additional Riders:</td>"
												+ "<th class='Border2' style='width:'>Life Assured</th>"
												+ "<th class='Border2' style='width:40px'>Policy Term</th>"
												+ "<th class='Border2' style='width:75px'>Premium Payment Term</th>"
												+ "<th class='Border2' style='width:75px'>Maturity Benifit (US$)</th>"
												+ "<th class='Border2' style='width:100px'>Semi-Annual (US$)</th>"
												+ "<th class='Border2' style='width:70px'>Annual (US$)</th>" + "</tr>";
									} else {
										strText += " <tr > "
												+ "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; "
												+ oDT3.getString("ProductName") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("Life_Assured")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ oDT3.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("PremiumTerm")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("SA")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</td>"
												+ "</tr>";
									}
								}

								oDT3.beforeFirst();

								while (oDT3.next()) {

								}

							}
							strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody1- 1. Guaranteed Maturity Benefit with the Basic Policy that
							// can be Used as the Education Fund for the Child"
							ResultSet oDT4 = null;
							if (resultSet.next()) {
								oDT4 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='text-align:center;height:30px;vertical-align:middle;' colspan=6>Maturity Benefit Settlement Option</th>"
									+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' rowspan=2 style='width:90px;'>Policy Year</th>"
									+ "<th class='Border2' rowspan=2 style='width:160px'>Age</th>"
									+ "<th class='Border2' class='Border2' style='width:140px'>Option #1:Lump sum</th>"
									+ "<th class='Border2' style='width:140px'>Total</th>"
									+ "<th class='Border2' style='width:140px'>Option #2:Installment</th>"
									+ "<th class='Border2' style='width:140px'>Total</th>" + "</tr>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:140px'>(US$)</th>"
									+ "<th class='Border2' style='width:140px'>(US$)</th>"
									+ "<th class='Border2' style='width:140px'>(US$)</th>"
									+ "<th class='Border2' style='width:140px'>(US$)</th>" + "</tr>";
							while (oDT4.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:90px'>"
										+ oDT4.getString("PolicyTerm") + "</td>"
										+ "<td class='Border2' style='width:160px'>" + oDT4.getString("Age") + "</td>"
										+ "<td class='Border2' style='width:140px'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_LumpSum")) + "</td>"
										+ "<td class='Border2' style='width:140px'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_TT")) + "</td>"
										+ "<td class='Border2' style='width:140px'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT2_Inst")) + "</td>"
										+ "<td class='Border2' style='width:140px'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_Inst_TT")) + "</td>"
										+ "</tr>";
							}
							strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody2 -2. Life Insurance Benefit, that Helps the Family Take Care
							// of the Immediate Liabilities"
							ResultSet oDT5 = null;
							if (resultSet.next()) {
								oDT5 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>Policy Year</th>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px;'>Premium every<br/>year</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px;'>Total<br/>Premium</th>"
									+ "<th class='Border2' colspan=2 style='width:276px;'>Death/TPD Benefit</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px;'>Guaranteed Cash Value(GCV)</th>"
									+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>Not Caused by an accident</th>"
									+ "<th class='Border2' style='width:'>Caused by an accident</th>" + "</tr>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";

							while (oDT5.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:'>" + oDT5.getString("PolicyYear")
										+ "</td>" + "<td class='Border2' style='width:'>" + oDT5.getString("Age")
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ (String.format("%,.2f", oDT5.getBigDecimal("P_APE")).equalsIgnoreCase("0.00")
												? " "
												: String.format("%,.2f", oDT5.getBigDecimal("P_APE")))
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("P_Total")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Non_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("Surrender")) + "</td>" + "</tr>";
							}
							oDT5.last();
							strText += "<tr>"
									+ "<td class='Border2' style='height:12px;text-align:left;' colspan=6><b>No Claim Benefit (Payable at the time of maturity, provided the policy is in force at that time) (US$)</td>"
									+ "<td class='Border2' style='width:138px'><b>"
									+ String.format("%,.2f", oDT5.getBigDecimal("Surrender")) + "</td>" + "</tr>";
							strText += "</table>";
							results.add(strText);
							// #endregion

							// #region "lblBody3 -3. Benefits with Additional Riders, that Help the Family
							// in the Further Securing Child's Education and Future"
							ResultSet oDT6 = null;
							if (resultSet.next()) {
								oDT6 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							if (oDT6.first()) {
								strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
										+ "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:160px;text-align:center;' colspan=1 rowspan=3>Rider Benefits</th>"
										+ "<th class='Border2' style='width:90px' rowspan=3>Life Assured Name</th>"
										+ "<th class='Border2' style='width:280px' colspan=2>Death/TPD Benefit</th>"
										+ "<th class='Border2' style='width:280px' rowspan=3>Benefit Payment Terms</th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:140px' >Not Caused by an Accident</th>"
										+ "<th class='Border2' style='width:140px' >Caused by an Accident</th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:140px' >(US$)</th>"
										+ "<th class='Border2' style='width:140px' >(US$)</th>" + "</tr>";
								oDT6.beforeFirst();
								while (oDT6.next()) {

									strText += "<tr>"
											+ "<td class='Border2' style='width:160px;text-align:left;' colspan=1 >"
											+ oDT6.getString("ProductName") + "</td>"
											+ "<td class='Border2' style='width:110px' colspan=1 >"
											+ oDT6.getString("Life_Assured") + "</td>"
											+ "<td class='Border2' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Non_Accident")) + "</td>"
											+ "<td class='Border2' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Accident")) + "</td>"
											+ "<td class='Border2' style='width:260px' >"
											+ oDT6.getString("Payment_Terms") + "</td>" + "</tr>";
								}
								strText += "</table>";
							}

							results.add(strText);
							// #endregion

							//
							results.add(lblSerialNo1);
							results.add(lblSerialNo2);

							oDT1.close();
							oDT2.close();
							oDT3.close();
							oDT4.close();
							oDT5.close();
							oDT6.close();

						} finally {
							cstmt.close();
						}
						return results; // this should contain All rows or objects you need for further processing
					}
				});
		return res;
	}

	// **********************************
	// * PruMyfamily Product *
	// **********************************
	@Transactional
	public List<String> generateMortgageKhmerQuote(final Long quoteId, final String lang, final String result) {
		List<String> res = sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<String> /* objectType returned */>() {
					@Override
					/* or object type you need to return to process */
					public List<String> execute(Connection conn) throws SQLException {
						PreparedStatement cstmt = conn.prepareStatement(
								"select * from ap.spgen_pruquote_mrml(?,?,'RN','R1','R2','R3','R4','R5','R6','R7','R8')",
								ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
						@SuppressWarnings("unused")
						String policyTerm = "";
						String paymentMode = "";
						String productName = "";
						String productCode = "";
						int oDT3Row = 0;
						cstmt.setLong(1, quoteId);
						cstmt.setString(2, lang);
						// cstmt.setString(3, result);
						// Result list that would return ALL rows of ALL result sets
						List<String> results = new ArrayList<String>();
						String strText = "";
						ResultSet resultSet = null;
						ResultSet oDT1 = null;
						ResultSet oDT2 = null;
						ResultSet oDT3 = null;
						try {
							resultSet = cstmt.executeQuery();

							// #region "lblHeader1"
							resultSet.next();
							oDT1 = (ResultSet) resultSet.getObject(1);
							oDT1.first();
							paymentMode = oDT1.getString("PaymentMode");
							productCode = oDT1.getString("product");
							strText += "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr>" + "<th class='Border2' style='width:230px;text-align:left;'>&nbsp;</th>"
									+ "<th class='Border2' colspan='2'>ឈ្មោះ</th>" + "<th class='Border2'>ភេទ</th>"
									+ "<th class='Border2'>អាយុ</th>" + "<th class='Border2'>របៀបទូទាត់</th>" + "</tr>"
									+ "<tr>"
									+ "<td class='Border2' style='width:;text-align:left;'>ឈ្មោះអ្នកត្រូវបានធានារ៉ាប់រង</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("POName") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POSex") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POAge") + "</td>"
									+ "<td class='Border2' rowspan=2>" /* + oDT1.getString("PaymentMode2") + "/" */
									+ oDT1.getString("PaymentType2") + "</td>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='width:;text-align:left;'>ឈ្មោះបុគ្គលដែលជាកម្មវត្ថុនៃការធានា</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("LA1Name") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Sex2") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Age") + "</td>" + "</tr>";
							SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd");

							Date dOBDate;
							try {
								dOBDate = dt.parse(oDT1.getString("LA1DateOfBirth"));
							} catch (Exception e) {
								dOBDate = new Date();
							}

							SimpleDateFormat df = new SimpleDateFormat("hhmmssSSS");
							Date date = new Date();
							String time = df.format(date);

							String lblSerialNo1 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P";

							// Particulars of Basic Plan and My Option Benefits:
							if (resultSet.next()) {
								oDT2 = (ResultSet) resultSet.getObject(1);
							}
							strText += " <tr > "
									+ "<td class='Border2' style='width:;text-align:left;'>ព័ត៌មានលម្អិតនៃគម្រោងមូលដ្ឋាន​</td>"
									+ "<th class='Border2'>ប្រភេទអត្ថប្រយោជន៍ធានារ៉ាប់រងអាយុជីវិត</th>"
									+ "<th class='Border2'>រយៈពេលកំណត់នៃ<br>បណ្ណ​សន្យារ៉ាប់រង​​(ឆ្នាំ)</th>"
									+ "<th class='Border2'>រយៈពេលបង់ប្រាក់<br>បុព្វលាភរ៉ាប់រង(ឆ្នាំ)</th>"
									+ "<th class='Border2'>ទឹកប្រាក់ធានា<br>រ៉ាប់រងសរុប​ (US$)</th>"
									+ "<th class='Border2'>បុព្វលាភធានារ៉ាប់រង (US$)</th>" + "</tr>";
							if (oDT2.first()) {

								policyTerm = oDT2.getString("PolicyTerm");
								if (productCode.equals("MTR1")) {
									productName = "អត្ថប្រយោជន៍ធានារ៉ាប់រងថយចុះ";
								} else {
									productName = "អត្ថប្រយោជន៍ធានារ៉ាប់រងថេរ";
								}

								strText += " <tr > "
										+ "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; "
										+ oDT2.getString("ProductName") + "</td>"
										+ "<td class='Border2' style='width:'>" + productName + "</td>"
										+ "<td class='Border2' style='width:90px'>" + oDT2.getString("PolicyTerm")
										+ "</td>" + "<td class='Border2' style='width:100px'>"
										+ oDT2.getString("PremiumTerm") + "</td>"
										+ "<td class='Border2' style='width:80px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("SA")) + "</td>"
										+ "<td class='Border2' style='width:65px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("P01_Amt")) + "</td>" + "</tr>";
							}
							if (resultSet.next()) {
								oDT3 = (ResultSet) resultSet.getObject(1);
							}

							// get oDT3 rows count
							if (oDT3.last())
								oDT3Row = oDT3.getRow();

							if (oDT3.first()) {

								// My Option Benefits (Also known as Riders)
								if (oDT3Row > 6) {
									strText += " <tr > "
											+ "<td class='Border2' style='width:px;text-align:left;' colspan=8>លក្ខខណ្ឌបន្ថែម/ផលិតផលបំពេញបន្ថែម</td>"
											+ "</tr>";
								}
								// oDT3.beforeFirst();
								while (oDT3.next()) {
									if (oDT3.getString("Product").equals("TT")) {
										if (oDT3.getString("ProductName").contains("%")) {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "%</th>"
													+ " </tr>";
										} else if (oDT3.isLast()) {
											String st12 = "";
											String st02 = "";
											String st01 = "";
											if (paymentMode.contains("12")) {
												st12 = "background-color:#bfc8cf;";
												st02 = "";
												st01 = "";
											} else if (paymentMode.contains("02")) {
												st12 = "";
												st02 = "background-color:#bfc8cf;";
												st01 = "";
											} else {
												st12 = "";
												st02 = "";
												st01 = "background-color:#bfc8cf;";
											}
											strText += "<tr>"
													+ "     <th class='Border2' style='width:;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:;;" + st01 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										} else {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										}
									} else if (oDT3.getString("Product").equals("TH_RTR2")) {
										strText += " <tr > "
												+ "<td class='Border2' style='width:;text-align:left;'></td>"
												+ "<th class='Border2'>បុគ្គលដែលជាកម្មវត្ថុ​នៃការធានា</th>"
												+ "<th class='Border2'>រយៈពេលធានានៃ<br>ផលិតផលបំពេញបន្ថែម​​(ឆ្នាំ)    </th>"
												+ "<th class='Border2'>រយៈពេលបង់<br>បុព្វលាភរ៉ាប់រង(ឆ្នាំ)</th>"
												+ "<th class='Border2'>អត្ថប្រយោជន៍​នៅ​កាល​បរិចេ្ឆទ​ផុត​កំណត់​នៃ​បណ្ណ​សន្យា​រ៉ាប់រង(US$)</th>"
												+ "<th class='Border2'>ប្រចាំខែ<br>(US$)</th>"
												+ "<th class='Border2'>ប្រចាំឆមាស <br>(US$)</th>"
												+ "<th class='Border2'>ប្រចាំឆ្នាំ <br>(US$)</th>" + "</tr>";
									} else {
										strText += " <tr > "
												+ "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; "
												+ oDT3.getString("ProductName") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("Life_Assured")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ oDT3.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("PremiumTerm")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("SA")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</td>"
												+ "</tr>";
									}
								}

								oDT3.beforeFirst();

								while (oDT3.next()) {

								}

							}
							strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody1- 1. Guaranteed Maturity Benefit with the Basic Policy that
							// can be Used as the Education Fund for the Child"
							ResultSet oDT4 = null;
							if (resultSet.next()) {
								oDT4 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>&nbsp;</th>"
									+ "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់</th>"
									+ "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់បង់ជាដំណាក់កាល</th>"
									+ "</tr>" + "<tr>"
									+ "<th class='Border2' rowspan=2 style='width:60px;' style='background-color:#bfc8cf;'>ឆ្នាំបណ្ណធានា<BR>រ៉ាប់រង</th>"
									+ "<th class='Border2' rowspan=2 style='width:60px' style='background-color:#bfc8cf;'>អាយុ</th>"
									+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>ការបង់ជាសរុប</th>"
									+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>សរុប</th>"
									+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>ការបង់ជាដំណាក់កាល</th>"
									+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>សរុប</th>"
									+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>"
									+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
							while (oDT4.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:'>" + oDT4.getString("PolicyTerm")
										+ "</td>" + "<td class='Border2' style='width:'>" + oDT4.getString("Age")
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_LumpSum")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_TT")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT2_Inst")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_Inst_TT")) + "</td>"
										+ "</tr>";
							}
							strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody2 -2. Life Insurance Benefit, that Helps the Family Take Care
							// of the Immediate Liabilities"
							ResultSet oDT5 = null;
							if (resultSet.next()) {
								oDT5 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>ឆ្នាំបណ្ណសន្យា<BR>រ៉ាប់រង</th>"
									+ "<th class='Border2' rowspan=3 style='width:60px'>អាយុ</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px'>បុព្វលាភរ៉ាប់រង</th>" +
							// "<th class='Border2' rowspan=2 style='width:138px'>បុព្វលាភរ៉ាប់រងសរុប</th>"
							// +
							"<th class='Border2' style='width:'>អត្ថប្រយោជន៍ក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px'>តម្លៃទឹកប្រាក់ដែលមាន<BR>ការធានា (នៅពេលបោះបង់<BR>បណ្ណសន្យារ៉ាប់រង)(GCV)</th>"
									+ "</tr>" + "<tr style='background-color:#bfc8cf;'>" +
							// "<th class='Border2' style='width:138px'>មិនបណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
							// +
							// "<th class='Border2' style='width:138px'>បណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>" +
							"</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>(US$)</th>" +
							// "<th class='Border2' style='width:'>(US$)</th>" +
							"<th class='Border2' style='width:'>(US$)</th>" +
							// "<th class='Border2' style='width:'>(US$)</th>" +
							"<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
							while (oDT5.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:'>" + oDT5.getString("PolicyYear")
										+ "</td>" + "<td class='Border2' style='width:'>" + oDT5.getString("Age")
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ (String.format("%,.2f", oDT5.getBigDecimal("P_APE")).equalsIgnoreCase("0.00")
												? " "
												: String.format("%,.2f", oDT5.getBigDecimal("P_APE")))
										+ "</td>" +
								// "<td class='Border2' style='width:'>" +
								// String.format("%,.2f",oDT5.getBigDecimal("P_Total")) + "</td>" +
								"<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Non_Accident")) + "</td>" +
								// "<td class='Border2' style='width:'>" +
								// String.format("%,.2f",oDT5.getBigDecimal("CLAM_Accident")) + "</td>" +
								"<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("Surrender")) + "</td>" + "</tr>";
							}
							oDT5.last();
							// strText += "<tr>" +
							// "<td class='Border2' style='width:;height:12px;text-align:left;'
							// colspan=6><b>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់នៃបណ្ណសន្យារ៉ាប់រង
							// (នឹងត្រូវបានទូទាត់ជូននៅថ្ងៃដែលរយៈពេលកំណត់នៃបណ្ណសន្យារ៉ាប់រងផុតកំណត់
							// ល្កឹកណាបណ្ណសន្យារ៉ាប់រង នៅតែមានសុពលភាពនៅកាលបរិច្ឆេទនោះ) (US$)</td>" +
							// "<td class='Border2' style='width:110px'><b>" +
							// String.format("%,.2f",oDT5.getBigDecimal("Surrender")) + "</td>" +
							// "</tr>";
							strText += "</table>";
							results.add(strText);
							// #endregion

							// #region "lblBody3 -3. Benefits with Additional Riders, that Help the Family
							// in the Further Securing Child's Education and Future"
							ResultSet oDT6 = null;
							if (resultSet.next()) {
								oDT6 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							if (oDT6.first()) {
								strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
										+ "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:160px;text-align:center;' colspan=1 rowspan=3>លក្ខខណ្ឌបន្ថែម	</th>"
										+ "<th class='Border2' style='width:110px;' rowspan=3>បុគ្គលដែលជាកម្មវត្ថុនៃការធានា</th>"
										+ "<th class='Border2' style='width:' colspan=2>អត្ថប្រយោជន៍ក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍</th>"
										+ "<th class='Border2' style='width:260px' rowspan=3>សេចក្តីសម្រាយ</th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:138px' >មិនបណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
										+ "<th class='Border2' style='width:138px' >បណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
										+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:' >(US$)</th>"
										+ "<th class='Border2' style='width:' >(US$)</th>" + "</tr>";
								oDT6.beforeFirst();
								while (oDT6.next()) {

									strText += "<tr>"
											+ "<td class='Border2' style='width:160px;text-align:left;' colspan=1 >"
											+ oDT6.getString("ProductName") + "</td>"
											+ "<td class='Border2' style='width:110px' colspan=1 >"
											+ oDT6.getString("Life_Assured") + "</td>"
											+ "<td class='Border2' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Non_Accident")) + "</td>"
											+ "<td class='Border2' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Accident")) + "</td>"
											+ "<td class='Border2' style='width:260px' >"
											+ oDT6.getString("Payment_Terms") + "</td>" + "</tr>";
								}
								strText += "</table>";
							}

							results.add(strText);
							// #endregion

							//

							// #region "lblBody4- 4. Guaranteed Maturity Benefit with the PruSaver"
							ResultSet oDT7 = null;
							if (resultSet.next()) {
								oDT7 = (ResultSet) resultSet.getObject(1);
								if (oDT7.first()) {
									strText = "";
									strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' rowspan=3 style='width:60px;' style='background-color:#bfc8cf;'>ឆ្នាំ​នៃ​ផលិតផលបំពេញបន្ថែម</th>"
											+ "<th class='Border2' rowspan=3 style='width:60px' style='background-color:#bfc8cf;'>អាយុ</th>"
											+
									/*
									 * "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>&nbsp;</th>"
									 * +
									 */
									"<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់នៃបណ្ណសន្យារ៉ាប់រង</th>"
											+ "<th class='Border2' style='text-align:center;height:;vertical-align:middle;' colspan=2>អត្ថប្រយោជន៍នៅកាលបរិច្ឆេទផុតកំណត់នៃបណ្ណសន្យារ៉ាប់រងដែលបង់ជាដំណាក់កាល</th>"
											+ "</tr>" + "<tr>" +

											"<th class='Border2' style='width:173px;background-color:#bfc8cf;'>ការបង់ជាសរុប</th>"
											+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>សរុប</th>"
											+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>ការបង់ជាដំណាក់កាល</th>"
											+ "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>សរុប</th>"
											+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
									oDT7.beforeFirst();
									while (oDT7.next()) {
										strText += "<tr>" + "<td class='Border2' style='width:;'>"
												+ oDT7.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT7.getString("Age")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ (String.format("%,.2f", oDT7.getBigDecimal("OPT1_LumpSum"))
														.equals("0.00")
																? " "
																: String.format("%,.2f",
																		oDT7.getBigDecimal("OPT1_LumpSum")))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ (String.format("%,.2f", oDT7.getBigDecimal("OPT1_TT")).equals("0.00")
														? " "
														: String.format("%,.2f", oDT7.getBigDecimal("OPT1_TT")))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT7.getBigDecimal("OPT2_Inst")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT7.getBigDecimal("OPT1_Inst_TT")) + "</td>"
												+ "</tr>";
									}
									strText += "</table>";
									// body1
									results.add(strText);
								}
							} else {
								results.add("");
							}

							// #endregion

							// #region "lblBody5 -5. Life Insurance Benefit with PruSaver"
							ResultSet oDT8 = null;
							if (resultSet.next()) {
								oDT8 = (ResultSet) resultSet.getObject(1);
								if (oDT8.first()) {
									strText = "";
									strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' rowspan=3 style='width:60px;'>ឆ្នាំ​នៃ​ផលិតផលបំពេញបន្ថែម</th>"
											+ "<th class='Border2' rowspan=3 style='width:60px'>អាយុ</th>"
											+ "<th class='Border2' rowspan=2 style='width:128px'>បុព្វលាភរ៉ាប់រងរាល់ឆ្នាំ</th>"
											+ "<th class='Border2' rowspan=2 style='width:128px'>បុព្វលាភរ៉ាប់រងសរុប</th>"
											+ "<th class='Border2' colspan=2 style='width:'>អត្ថប្រយោជន៍ក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px'>តម្លៃទឹកប្រាក់ដែលមាន<BR>ការធានា (នៅពេលបោះបង់<BR>ផលិតផលបំពេញបន្ថែម)(GCV)</th>"
											+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:148px'>មិនបណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
											+ "<th class='Border2' style='width:148px'>បណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
											+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
									oDT8.beforeFirst();
									while (oDT8.next()) {
										strText += "<tr>" + "<td class='Border2' style='width:'>"
												+ oDT8.getString("PolicyYear") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT8.getString("Age")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_APE")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_Total")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Non_Accident"))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Accident")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("Surrender")) + "</td>"
												+ "</tr>";
									}
									strText += "</table>";
									// body2
									results.add(strText);
									oDT7.close();
									oDT8.close();
								}
							} else {
								results.add("");
							}
							results.add(lblSerialNo1);

							oDT1.close();
							oDT2.close();
							oDT3.close();
							oDT4.close();
							oDT5.close();
							oDT6.close();

						} finally {
							cstmt.close();
						}

						return results; // this should contain All rows or objects you need for further processing
					}
				});
		return res;
	}

	@Transactional
	public List<String> generateMortgageLatinQuote(final Long quoteId, final String lang, final String result) {
		List<String> res = sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<String> /* objectType returned */>() {
					@Override
					/* or object type you need to return to process */
					public List<String> execute(Connection conn) throws SQLException {
						PreparedStatement cstmt = conn.prepareStatement(
								"select * from ap.spgen_pruquote_mrml(?,?,'RN','R1','R2','R3','R4','R5','R6','R7','R8')",
								ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
						@SuppressWarnings("unused")
						String policyTerm = "";
						String paymentMode = "";
						String productName = "";
						int oDT3Row = 0;
						cstmt.setLong(1, quoteId);
						cstmt.setString(2, lang);
						// cstmt.setString(3, result);
						// Result list that would return ALL rows of ALL result sets
						List<String> results = new ArrayList<String>();
						String strText = "";
						ResultSet resultSet = null;
						ResultSet oDT1 = null;
						ResultSet oDT2 = null;
						ResultSet oDT3 = null;
						try {
							resultSet = cstmt.executeQuery();

							// #region "lblHeader1"
							resultSet.next();
							oDT1 = (ResultSet) resultSet.getObject(1);
							oDT1.first();
							paymentMode = oDT1.getString("PaymentMode");
							strText += "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr>" + "<th class='Border2' style='text-align:left;'>&nbsp;</th>"
									+ "<th class='Border2' colspan='2'><u>Name</u></th>"
									+ "<th class='Border2'><u>Gender</u></th>" + "<th class='Border2'><u>Age</u></th>"
									+ "<th class='Border2' colspan='3'><u>Premium Mode</u></th>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='text-align:left;'>Policy Owner's Name:</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("POName") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POSex") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POAge") + "</td>"
									+ "<td class='Border2' colspan=3 rowspan=2>" /*
																					 * + oDT1.getString("PaymentMode2")
																					 * + "/"
																					 */ + oDT1.getString("PaymentType2")
									+ "</td>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='text-align:left;'>Main Life Assured's Name:</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("LA1Name") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Sex2") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Age") + "</td>" + "</tr>";
							SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd");

							Date dOBDate;
							try {
								dOBDate = dt.parse(oDT1.getString("LA1DateOfBirth"));
							} catch (Exception e) {
								dOBDate = new Date();
							}

							SimpleDateFormat df = new SimpleDateFormat("hhmmssSSS");
							Date date = new Date();
							String time = df.format(date);

							String lblSerialNo1 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P1";
							String lblSerialNo2 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P2";

							// Particulars of Basic Plan and My Option Benefits:
							if (resultSet.next()) {
								oDT2 = (ResultSet) resultSet.getObject(1);
							}
							strText += " <tr > "
									+ "<td class='Border2' style='width:170px;text-align:left;'>Particulars of Basic Plan:</td>"
									+ "<th class='Border2' style='width:'>Type of Life Insurance Cover</th>"
									+ "<th class='Border2' style='width:40px'>Policy Term</th>"
									+ "<th class='Border2' style='width:75px'>Premium Payment Term</th>"
									+ "<th class='Border2' style='width:75px'>Sum Assured (US$)</th>" +
							// "<th class='Border2' style='width:70px'>Monthly (US$)</th>" +
							// "<th class='Border2' style='width:100px'>Semi-Annual (US$)</th>" +
							"<th class='Border2' style='width:70px'>Single Premium (US$)</th>" + "</tr>";
							if (oDT2.first()) {

								policyTerm = oDT2.getString("PolicyTerm");
								if (oDT1.getString("product").equals("MTR1")) {
									productName = "Reducing Term";
								} else {
									productName = "Level Term";
								}

								strText += " <tr > " + "<td class='Border2' style='text-align:left;'>&nbsp;&nbsp; "
										+ oDT2.getString("ProductName") + "</td>" + "<td class='Border2' >"
										+ productName + "</td>" + "<td class='Border2' >" + oDT2.getString("PolicyTerm")
										+ "</td>" + "<td class='Border2' >" + oDT2.getString("PremiumTerm") + "</td>"
										+ "<td class='Border2' >" + String.format("%,.2f", oDT2.getBigDecimal("SA"))
										+ "</td>" +
								// "<td class='Border2' >" +
								// String.format("%,.2f",oDT2.getBigDecimal("P12_Amt")) + "</td>" +
								// "<td class='Border2' >" +
								// String.format("%,.2f",oDT2.getBigDecimal("P02_Amt")) + "</td>" +
								"<td class='Border2' >" + String.format("%,.2f", oDT2.getBigDecimal("P01_Amt"))
										+ "</td>" + "</tr>";
							}

							if (resultSet.next()) {
								oDT3 = (ResultSet) resultSet.getObject(1);
							}

							// get oDT3 rows count
							if (oDT3.last())
								oDT3Row = oDT3.getRow();

							if (oDT3.first()) {

								// My Option Benefits (Also known as Riders)
								if (oDT3Row > 6) {
									strText += " <tr > "
											+ "<td class='Border2' style='width:250px;text-align:left;' colspan=8>Riders</td>"
											+ "</tr>";
								}
								// oDT3.beforeFirst();
								while (oDT3.next()) {
									if (oDT3.getString("Product").equals("TT")) {
										if (oDT3.getString("ProductName").contains("%")) {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>" +
											// " <th class='Border2' style='width:'>" +
											// String.format("%,.2f",oDT3.getBigDecimal("P12_Amt")) + "%</th>" +
											// " <th class='Border2' style='width:'>" +
											// String.format("%,.2f",oDT3.getBigDecimal("P02_Amt")) + "%</th>" +
											"     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "%</th>"
													+ " </tr>";
										} else if (oDT3.isLast()) {
											String st12 = "";
											String st02 = "";
											String st01 = "";
											if (paymentMode.contains("12")) {
												st12 = "background-color:#bfc8cf;";
												st02 = "";
												st01 = "";
											} else if (paymentMode.contains("02")) {
												st12 = "";
												st02 = "background-color:#bfc8cf;";
												st01 = "";
											} else {
												st12 = "";
												st02 = "";
												st01 = "background-color:#bfc8cf;";
											}
											strText += "<tr>"
													+ "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>" +
											// " <th class='Border2' style='width:;" + st12 + "'>" +
											// String.format("%,.2f",oDT3.getBigDecimal("P12_Amt")) + "</th>" +
											// " <th class='Border2' style='width:;" + st02 + "'>" +
											// String.format("%,.2f",oDT3.getBigDecimal("P02_Amt")) + "</th>" +
											"     <th class='Border2' style='width:;;" + st01 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										} else {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>" +
											// " <th class='Border2' style='width:'>" +
											// String.format("%,.2f",oDT3.getBigDecimal("P12_Amt")) + "</th>" +
											// " <th class='Border2' style='width:'>" +
											// String.format("%,.2f",oDT3.getBigDecimal("P02_Amt")) + "</th>" +
											"     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										}
									} else if (oDT3.getString("Product").equals("TH_RTR2")) {
										strText += " <tr > "
												+ "<td class='Border2' style='width:170px;text-align:left;'>Additional Riders:</td>"
												+ "<th class='Border2' style='width:'>Life Assured</th>"
												+ "<th class='Border2' style='width:40px'>Policy Term</th>"
												+ "<th class='Border2' style='width:75px'>Premium Payment Term</th>"
												+ "<th class='Border2' style='width:75px'>Maturity Benifit (US$)</th>"
												+ "<th class='Border2' style='width:70px'>Monthly (US$)</th>"
												+ "<th class='Border2' style='width:100px'>Semi-Annual (US$)</th>"
												+ "<th class='Border2' style='width:70px'>Annual (US$)</th>" + "</tr>";
									} else {
										strText += " <tr > "
												+ "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; "
												+ oDT3.getString("ProductName") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("Life_Assured")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ oDT3.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("PremiumTerm")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("SA")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</td>"
												+ "</tr>";
									}
								}

								oDT3.beforeFirst();

								while (oDT3.next()) {

								}

							}
							strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody1- 1. Guaranteed Maturity Benefit with the Basic Policy that
							// can be Used as the Education Fund for the Child"
							ResultSet oDT4 = null;
							if (resultSet.next()) {
								oDT4 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='text-align:center;height:30px;vertical-align:middle;' colspan=6>Maturity Benefit Settlement Option</th>"
									+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' rowspan=2 style='width:90px;'>Policy Year</th>"
									+ "<th class='Border2' rowspan=2 style='width:160px'>Age</th>"
									+ "<th class='Border2' class='Border2' style='width:140px'>Option #1:Lump sum</th>"
									+ "<th class='Border2' style='width:140px'>Total</th>"
									+ "<th class='Border2' style='width:140px'>Option #2:Installment</th>"
									+ "<th class='Border2' style='width:140px'>Total</th>" + "</tr>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:140px'>(US$)</th>"
									+ "<th class='Border2' style='width:140px'>(US$)</th>"
									+ "<th class='Border2' style='width:140px'>(US$)</th>"
									+ "<th class='Border2' style='width:140px'>(US$)</th>" + "</tr>";
							while (oDT4.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:90px'>"
										+ oDT4.getString("PolicyTerm") + "</td>"
										+ "<td class='Border2' style='width:160px'>" + oDT4.getString("Age") + "</td>"
										+ "<td class='Border2' style='width:140px'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_LumpSum")) + "</td>"
										+ "<td class='Border2' style='width:140px'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_TT")) + "</td>"
										+ "<td class='Border2' style='width:140px'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT2_Inst")) + "</td>"
										+ "<td class='Border2' style='width:140px'>"
										+ String.format("%,.2f", oDT4.getBigDecimal("OPT1_Inst_TT")) + "</td>"
										+ "</tr>";
							}
							strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody2 -2. Life Insurance Benefit, that Helps the Family Take Care
							// of the Immediate Liabilities"
							ResultSet oDT5 = null;
							if (resultSet.next()) {
								oDT5 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>Policy Year</th>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px;'>Single Premium</th>" +
							// "<th class='Border2' rowspan=2 style='width:138px;'>Total<br/>Premium</th>" +
							"<th class='Border2' style='width:276px;'>Death/Total and Permanent<br/>Disability (TPD) Benefit</th>"
									+ "<th class='Border2' rowspan=2 style='width:138px;'>Guaranteed Cash Value(GCV)</th>"
									+ "</tr>" + "<tr style='background-color:#bfc8cf;'>" +
							// "<th class='Border2' style='width:'>Not Caused by an accident</th>" +
							// "<th class='Border2' style='width:'>Caused by an accident</th>" +
							"</tr>" + "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>(US$)</th>" +
							// "<th class='Border2' style='width:'>(US$)</th>" +
							"<th class='Border2' style='width:'>(US$)</th>" +
							// "<th class='Border2' style='width:'>(US$)</th>" +
							"<th class='Border2' style='width:'>(US$)</th>" + "</tr>";

							while (oDT5.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:'>" + oDT5.getString("PolicyYear")
										+ "</td>" + "<td class='Border2' style='width:'>" + oDT5.getString("Age")
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ (String.format("%,.2f", oDT5.getBigDecimal("P_APE")).equalsIgnoreCase("0.00")
												? " "
												: String.format("%,.2f", oDT5.getBigDecimal("P_APE")))
										+ "</td>" +
								// "<td class='Border2' style='width:'>" +
								// String.format("%,.2f",oDT5.getBigDecimal("P_Total")) + "</td>" +
								"<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Non_Accident")) + "</td>" +
								// "<td class='Border2' style='width:'>" +
								// String.format("%,.2f",oDT5.getBigDecimal("CLAM_Accident")) + "</td>" +
								"<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("Surrender")) + "</td>" + "</tr>";
							}
							oDT5.last();
							// strText += "<tr>" +
							// "<td class='Border2' style='height:12px;text-align:left;'
							// colspan=6><b>Maturity Benefit (Payable at the time of maturity, provided the
							// policy is in force at that time) (US$)</td>" +
							// "<td class='Border2' style='width:138px'><b>" +
							// String.format("%,.2f",oDT5.getBigDecimal("Surrender")) + "</td>" +
							// "</tr>";
							strText += "</table>";
							results.add(strText);
							// #endregion

							// #region "lblBody3 -3. Benefits with Additional Riders, that Help the Family
							// in the Further Securing Child's Education and Future"
							ResultSet oDT6 = null;
							if (resultSet.next()) {
								oDT6 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							if (oDT6.first()) {
								strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
										+ "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:160px;text-align:center;' colspan=1 rowspan=3>Rider Benefits</th>"
										+ "<th class='Border2' style='width:90px' rowspan=3>Life Assured Name</th>"
										+ "<th class='Border2' style='width:280px' colspan=2>Death/TPD Benefit</th>"
										+ "<th class='Border2' style='width:280px' rowspan=3>Benefit Payment Terms</th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:140px' >Not Caused by an Accident</th>"
										+ "<th class='Border2' style='width:140px' >Caused by an Accident</th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:140px' >(US$)</th>"
										+ "<th class='Border2' style='width:140px' >(US$)</th>" + "</tr>";
								oDT6.beforeFirst();
								while (oDT6.next()) {

									strText += "<tr>"
											+ "<td class='Border2' style='width:160px;text-align:left;' colspan=1 >"
											+ oDT6.getString("ProductName") + "</td>"
											+ "<td class='Border2' style='width:110px' colspan=1 >"
											+ oDT6.getString("Life_Assured") + "</td>"
											+ "<td class='Border2' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Non_Accident")) + "</td>"
											+ "<td class='Border2' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Accident")) + "</td>"
											+ "<td class='Border2' style='width:260px' >"
											+ oDT6.getString("Payment_Terms") + "</td>" + "</tr>";
								}
								strText += "</table>";
							}

							results.add(strText);
							// #endregion

							// #region "lblBody4- 4. Guaranteed Maturity Benefit with the PruSaver"
							ResultSet oDT7 = null;
							if (resultSet.next()) {
								oDT7 = (ResultSet) resultSet.getObject(1);
								if (oDT7.first()) {
									strText = "";
									strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
											+ "<tr style='background-color:#bfc8cf;'>" +
									/*
									 * "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>&nbsp;</th>"
									 * +
									 */
									"<th class='Border2' rowspan=3 style='width:60px;'>Rider Year</th>"
											+ "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>"
											+ "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>Maturity Benefit</th>"
											+ "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>Maturity Benefit with Settlement Option</th>"
											+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:173px'>Lump Sum Payment</th>"
											+ "<th class='Border2' style='width:173px'>Total</th>"
											+ "<th class='Border2' style='width:173px'>Installment Payment</th>"
											+ "<th class='Border2' style='width:173px'>Total</th>" + "</tr>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
									oDT7.beforeFirst();
									while (oDT7.next()) {
										strText += "<tr>" + "<td class='Border2' style='width:60px;'>"
												+ oDT7.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:60px'>" + oDT7.getString("Age")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ (String.format("%,.2f", oDT7.getBigDecimal("OPT1_LumpSum"))
														.equals("0.00")
																? " "
																: String.format("%,.2f",
																		oDT7.getBigDecimal("OPT1_LumpSum")))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ (String.format("%,.2f", oDT7.getBigDecimal("OPT1_TT")).equals("0.00")
														? " "
														: String.format("%,.2f", oDT7.getBigDecimal("OPT1_TT")))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT7.getBigDecimal("OPT2_Inst")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT7.getBigDecimal("OPT1_Inst_TT")) + "</td>"
												+ "</tr>";
									}
									strText += "</table>";
									// body4
									results.add(strText);
								}
							} else {
								results.add("");
							}

							// #endregion

							// #region "lblBody5 -5. Life Insurance Benefit with PruSaver"
							ResultSet oDT8 = null;
							if (resultSet.next()) {
								oDT8 = (ResultSet) resultSet.getObject(1);
								if (oDT8.first()) {
									strText = "";
									strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' rowspan=3 style='width:60px;'>Rider Year</th>"
											+ "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px;'>Rider Premium<br/>every year</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px;'>Total Rider Premium</th>"
											+ "<th class='Border2' colspan=2 style='width:276px;'>Death/TPD Benefit</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px;'>Guaranteed Cash Value(GCV)</th>"
											+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>Not Caused by an Accident</th>"
											+ "<th class='Border2' style='width:'>Caused by an Accident</th>" + "</tr>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
									oDT8.beforeFirst();
									while (oDT8.next()) {
										strText += "<tr>" + "<td class='Border2' style='width:'>"
												+ oDT8.getString("PolicyYear") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT8.getString("Age")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_APE")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_Total")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Non_Accident"))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Accident")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("Surrender")) + "</td>"
												+ "</tr>";
									}
									strText += "</table>";
									// body5
									results.add(strText);
									oDT7.close();
									oDT8.close();
								}
							} else {
								results.add("");
							}

							//
							results.add(lblSerialNo1);
							results.add(lblSerialNo2);

							oDT1.close();
							oDT2.close();
							oDT3.close();
							oDT4.close();
							oDT5.close();
							oDT6.close();

						} finally {
							cstmt.close();
						}

						return results; // this should contain All rows or objects you need for further processing
					}
				});
		return res;
	}

	// **********************************
	// * EduSmart Product *
	// **********************************

	@Transactional
	public String getMBBySASmart(BigDecimal SA, String premiumTerm) {
		Query query = sessionFactory.getCurrentSession()
				.createSQLQuery("SELECT ap.spgetmb_by_sa_edusmart(:SA,:permiumTerm)").setParameter("SA", SA)
				.setParameter("permiumTerm", premiumTerm);
		@SuppressWarnings("unchecked")
		List<Object> quotes = query.list();
		return quotes.get(0).toString();
	}

	@Transactional
	public String getSAByMBSmart(BigDecimal MB, String premiumTerm) {
		Query query = sessionFactory.getCurrentSession()
				.createSQLQuery("SELECT ap.spgetsa_by_mb_edusmart(:MB,:permiumTerm)").setParameter("MB", MB)
				.setParameter("permiumTerm", premiumTerm);
		@SuppressWarnings("unchecked")
		List<Object> quotes = query.list();
		return quotes.get(0).toString();
	}

	@Transactional
	public List<String> generateEduSmartKhmerQuote(final Long quoteId, final String lang, final String result) {
		List<String> res = sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<String> /* objectType returned */>() {
					@Override
					/* or object type you need to return to process */
					public List<String> execute(Connection conn) throws SQLException {
						PreparedStatement cstmt = conn.prepareStatement(
								"select * from ap.spgen_edusmart(?,?,'RN','R1','R2','R3','R4','R5','R6','R7','R8')",
								ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
						String policyTerm = "";
						String paymentMode = "";
						String paymenttype = "";
						int MB = 0;
						int PS = 0;
						int oDT3Row = 0;
						cstmt.setLong(1, quoteId);
						cstmt.setString(2, lang);
						// cstmt.setString(3, result);
						// Result list that would return ALL rows of ALL result sets
						List<String> results = new ArrayList<String>();
						String strText = "";
						ResultSet resultSet = null;
						ResultSet oDT1 = null;
						ResultSet oDT2 = null;
						ResultSet oDT3 = null;
						String tmpText = "";
						try {
							resultSet = cstmt.executeQuery();

							// #region "lblHeader1"
							resultSet.next();
							oDT1 = (ResultSet) resultSet.getObject(1);
							oDT1.first();
							PS = oDT1.getInt("PruRetirement");
							paymentMode = oDT1.getString("PaymentMode");
							paymenttype = oDT1.getString("paymenttype");
							strText += "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr>" + "<th class='Border2' style='width:175px;text-align:left;'>&nbsp;</th>"
									+ "<th class='Border2' colspan='2'>ឈ្មោះ</th>" + "<th class='Border2'>ភេទ</th>"
									+ "<th class='Border2'>អាយុ</th>"
									+ "<th class='Border2' colspan='3'>របៀបទូទាត់</th>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='width:;text-align:left;'>ឈ្មោះម្ចាស់បណ្ណសន្យារ៉ាប់រង</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("POName") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POSex") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POAge") + "</td>"
									+ "<td class='Border2' colspan=3 rowspan=2>" /*
																					 * + oDT1.getString("PaymentMode2")
																					 * + "/"
																					 */ + oDT1.getString("PaymentType2")
									+ "</td>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='width:;text-align:left;'>ឈ្មោះអ្នកត្រូវបានធានារ៉ាប់រង</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("LA1Name") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Sex2") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Age") + "</td>" + "</tr>";
							if (!oDT1.getString("LA2Name").equals("")) {
								strText = strText.replace("rowspan=2", "rowspan=3") + "<tr>"
										+ "<td class='Border2' style='width:;text-align:left;'>ឈ្មោះអ្នកត្រូវបានធានារ៉ាប់រងបន្ថែម</td>     "
										+ "<td class='Border2' colspan=2>" + oDT1.getString("LA2Name") + "</td>"
										+ "<td class='Border2'>" + oDT1.getString("LA2Sex2") + "</td>"
										+ "<td class='Border2'>" + oDT1.getString("LA2Age") + "</td>" + "</tr>";
							}

							SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd");

							Date dOBDate;
							try {
								dOBDate = dt.parse(oDT1.getString("LA1DateOfBirth"));
							} catch (Exception e) {
								dOBDate = new Date();
							}
							SimpleDateFormat df = new SimpleDateFormat("hhmmssSSS");
							Date date = new Date();
							String time = df.format(date);

							String lblSerialNo1 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P";
							String lblSerialNo2 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P2";
							// Particulars of Basic Plan and My Option Benefits:
							if (resultSet.next()) {
								oDT2 = (ResultSet) resultSet.getObject(1);
							}
							strText += " <tr > "
									+ "<td class='Border2' style='width:;text-align:left;font-weight:600;'>ព័ត៌មានលម្អិតនៃគម្រោងមូលដ្ឋាន​និង<br>​លក្ខខណ្ឌបន្ថែម</td>"
									+ "<th class='Border2'>អ្នកត្រូវបានធានារ៉ាប់រង</th>"
									+ "<th class='Border2'>រយៈពេលកំណត់នៃ<br>បណ្ណ​សន្យារ៉ាប់រង​​<br/>(ឆ្នាំ)</th>"
									+ "<th class='Border2'>រយៈពេលបង់<br>បុព្វលាភធានារ៉ាប់រង<br/>(ឆ្នាំ)</th>"
									+ "<th class='Border2'>ទឹកប្រាក់ត្រូវ<br>បានធានា<br>រ៉ាប់រងសរុប​<br> (US$)</th>"
									+ "<th class='Border2'>ប្រចាំខែ<br>(US$)</th>"
									+ "<th class='Border2'>ប្រចាំឆមាស <br>(US$)</th>"
									+ "<th class='Border2'>ប្រចាំឆ្នាំ <br>(US$)</th>" + "</tr>";
							if (oDT2.first()) {
								policyTerm = oDT2.getString("PolicyTerm");
								strText += " <tr > "
										+ "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; "
										+ oDT2.getString("logo") + "</td>" + "<td class='Border2' style='width:'>"
										+ oDT2.getString("Life_Assured") + "</td>"
										+ "<td class='Border2' style='width:90px'>" + oDT2.getString("PolicyTerm")
										+ "</td>" + "<td class='Border2' style='width:100px'>"
										+ oDT2.getString("PremiumTerm") + "</td>"
										+ "<td class='Border2' style='width:80px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("SA")) + "</td>"
										+ "<td class='Border2' style='width:65px'>"
										+ (paymenttype.equalsIgnoreCase("C") ? "N/A"
												: String.format("%,.2f", oDT2.getBigDecimal("P12_Amt")))
										+ "</td>" + "<td class='Border2' style='width:65px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("P02_Amt")) + "</td>"
										+ "<td class='Border2' style='width:65px'>"
										+ String.format("%,.2f", oDT2.getBigDecimal("P01_Amt")) + "</td>" + "</tr>";
							}

							if (resultSet.next()) {
								oDT3 = (ResultSet) resultSet.getObject(1);
							}

							// get oDT3 rows count
							if (oDT3.last())
								oDT3Row = oDT3.getRow();

							if (oDT3.first()) {

								// My Option Benefits (Also known as Riders)
								if (oDT3Row > 6) {
									strText += " <tr > "
											+ "<td class='Border2' style='width:px;text-align:left;font-weight:600;' colspan=8>លក្ខខណ្ឌបន្ថែម</td>"
											+ "</tr>";
								}
								// oDT3.beforeFirst();
								while (oDT3.next()) {
									if (oDT3.getString("Product").equals("TT")) {
										if (oDT3.getString("ProductName").contains("%")) {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ (paymenttype.equalsIgnoreCase("C") ? "N/A"
															: String.format("%,.2f", oDT3.getBigDecimal("P12_Amt"))
																	+ "%")
													+ "</th>" + "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "%</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "%</th>"
													+ " </tr>";
										} else if (oDT3.isLast()) {
											String st12 = "";
											String st02 = "";
											String st01 = "";
											if (paymentMode.contains("12")) {
												st12 = "background-color:#bfc8cf;";
												st02 = "";
												st01 = "";
											} else if (paymentMode.contains("02")) {
												st12 = "";
												st02 = "background-color:#bfc8cf;";
												st01 = "";
											} else {
												st12 = "";
												st02 = "";
												st01 = "background-color:#bfc8cf;";
											}
											strText += "<tr>"
													+ "     <th class='Border2' style='width:;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:;" + st12 + "'>"
													+ (paymenttype.equalsIgnoreCase("C") ? "N/A"
															: String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")))
													+ "</th>" + "     <th class='Border2' style='width:;" + st02 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:;;" + st01 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										} else {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ (paymenttype.equalsIgnoreCase("C") ? "N/A"
															: String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")))
													+ "</th>" + "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										}
									} else if (oDT3.getString("Product").equals("TH_RTR2")) {
										strText += " <tr > "
												+ "<td class='Border2' style='width:;text-align:left;'></td>"
												+ "<th class='Border2'>បុគ្គលដែលជាកម្មវត្ថុ​នៃការធានា</th>"
												+ "<th class='Border2'>រយៈពេលកំណត់នៃ<br>បំពេញ<br/>បន្ថែម​​(ឆ្នាំ)</th>"
												+ "<th class='Border2'>រយៈពេលបង់<br>បុព្វលាភរ៉ាប់រង(ឆ្នាំ)</th>"
												+ "<th class='Border2'>អត្ថប្រយោជន៍​នៅ​កាល​បរិចេ្ឆទ​ផុត​កំណត់​នៃ​បណ្ណ​សន្យា​រ៉ាប់រង(US$)</th>"
												+ "<th class='Border2'>ប្រចាំខែ<br>(US$)</th>"
												+ "<th class='Border2'>ប្រចាំឆមាស <br>(US$)</th>"
												+ "<th class='Border2'>ប្រចាំឆ្នាំ <br>(US$)</th>" + "</tr>";
									} else {
										strText += " <tr > "
												+ "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; "
												+ oDT3.getString("ProductName") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("Life_Assured")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ oDT3.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("PremiumTerm")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("SA")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ (paymenttype.equalsIgnoreCase("C") ? "N/A"
														: String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</td>"
												+ "</tr>";
									}
								}

								oDT3.beforeFirst();

								while (oDT3.next()) {

								}

							}
							strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody1- 1. Guaranteed Maturity Benefit with the Basic Policy that
							// can be Used as the Education Fund for the Child"
							ResultSet oDT4 = null;
							if (resultSet.next()) {
								oDT4 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							// strText += "<table style='width:100%;border-collapse:collapse; border:1px
							// solid black;' border='0'>" +
							// "<tr style='background-color:#bfc8cf;'>" +
							//
							// "<th class='Border2' style='width:173px;background-color:#bfc8cf;'>សរុប</th>"
							// +
							// "</tr>" ;
							if (oDT4.next()) {
								strText += "<tr>"
										+ "<td colspan=8 style='font-weight:600;text-align:justify;'>អត្ថប្រយោជន៍នៅពេល​ដល់​កាលកំណត់នៃ​បណ្ណសន្យារ៉ាប់រង (នឹងត្រូវ​ទូទាត់ជូននៅ​ថ្ងៃ​ដែល​រយៈពេល​កំណត់​នៃ​បណ្ណសន្យារ៉ាប់រងផុត​កំណត់ ល្គឹកណា​បណ្ណសន្យារ៉ាប់រង​នៅ​មាន​សុពល​ភាព​​​នៅកាល​បរិច្ឆេទ​នោះ) (US$)</td>"
										+ "<td class='Border2' style='width:'><b>"
										+ (String.format("%,.2f", oDT4.getBigDecimal("OPT1_LumpSum")).equals("0.00")
												? " "
												: String.format("%,.2f", oDT4.getBigDecimal("OPT1_LumpSum")))
										+ "</b></td>" + "</tr>";
							}
							tmpText = strText;
							// strText += "</table>";
							results.add(strText);

							// #endregion

							// #region "lblBody2 -2. Life Insurance Benefit, that Helps the Family Take Care
							// of the Immediate Liabilities"
							ResultSet oDT5 = null;
							if (resultSet.next()) {
								oDT5 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>ឆ្នាំបណ្ណសន្យា<BR>រ៉ាប់រង</th>"
									+ "<th class='Border2' rowspan=3 style='width:60px'>អាយុ</th>"
									+ "<th class='Border2' rowspan=3 style='width:110px;'>បុព្វលាភធានារ៉ាប់រងរាល់ឆ្នាំ<br/>(US$)</th>"
									+ "<th class='Border2' rowspan=3 style='width:110px;'>បុព្វលាភធានារ៉ាប់រងសរុប<br/>(US$)</th>"
									+ "<th class='Border2' colspan=4 style='width:'>អត្ថប្រយោជន៍ក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍ </th>"
									+ "<th class='Border2' rowspan=3 style='width:138px;'>តម្លៃទឹកប្រាក់ត្រូវបានធានាសម្រាប់ការបោះបង់បណ្ណសន្យារ៉ាប់រង (GSV)<br/>(US$)</th>"
									+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' colspan=2 style='width:148px'>មិនបណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
									+ "<th class='Border2' colspan=2 style='width:148px'>បណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
									+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>អត្ថប្រយោជន៏សរុប<br/>(US$)</th>"
									+ "<th class='Border2' style='width:'>អត្ថប្រយោជន៍​ចំណូល​គ្រួសារ​<br/>(US$)</th>"
									+ "<th class='Border2' style='width:'>អត្ថប្រយោជន៏សរុប<br/>(US$)</th>"
									+ "<th class='Border2' style='width:'>អត្ថប្រយោជន៍​ចំណូល​គ្រួសារ​<br/>(US$)</th>"
									+ "</tr>";
							while (oDT5.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:'>" + oDT5.getString("PolicyYear")
										+ "</td>" + "<td class='Border2' style='width:'>" + oDT5.getString("Age")
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("P_APE")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("P_Total")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Non_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("AIB_Non_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("AIB_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'><b>"
										+ String.format("%,.2f", oDT5.getBigDecimal("Surrender")) + "</b></td>"
										+ "</tr>";
							}
							strText += tmpText;
							strText += "</table>";
							results.add(strText);
							// #endregion

							// #region "lblBody3 -3. Benefits with Additional Riders, that Help the Family
							// in the Further Securing Child's Education and Future"
							ResultSet oDT6 = null;
							if (resultSet.next()) {
								oDT6 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							if (oDT6.first()) {
								strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
										+ "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:160px;text-align:center;' colspan=1 rowspan=3>លក្ខខណ្ឌបន្ថែម</th>"
										+ "<th class='Border2' style='width:140px;' rowspan=3>អ្នក​ត្រូវ​បានធានារ៉ាប់រង​</th>"
										+ "<th class='Border2' style='width:' colspan=2>អត្ថប្រយោជន៍ក្នុងករណីទទួលមរណភាព ឬ<br/>​ពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍</th>"
										+ "<th class='Border2' style='width:260px' rowspan=3>សេចក្តីសម្រាយ</th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:150px' >មិនបណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
										+ "<th class='Border2' style='width:138px' >បណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
										+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:' >(US$)</th>"
										+ "<th class='Border2' style='width:' >(US$)</th>" + "</tr>";
								oDT6.beforeFirst();
								while (oDT6.next()) {

									strText += "<tr>"
											+ "<td class='Border2' style='width:160px;text-align:left;' colspan=1 >"
											+ oDT6.getString("ProductName") + "</td>"
											+ "<td class='Border2' style='width:110px' colspan=1 >"
											+ oDT6.getString("Life_Assured") + "</td>"
											+ "<td class='Border2' style='width:' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Non_Accident")) + "</td>"
											+ "<td class='Border2' style='width:' colspan=1>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Accident")) + "</td>"
											+ "<td class='Border2' style='width:260px;'>"
											+ oDT6.getString("Payment_Terms") + "</td>" + "</tr>";
								}
								strText += "</table>";
							}
							if (policyTerm.contains("10")) {
								// if(oDT3Row==10){
								// results.add("");
								// results.add(strText);
								// }else{
								results.add(strText);
								results.add("");
								// }
							} else {
								results.add("");
								results.add(strText);
							}

							// #endregion

							// #region "Footer1"
							strText = "";
							// Life Insurance Benefit
							// if (policyTerm.contains("10"))
							// {
							// strText += "<table>";
							// strText += "<tr><td class='Border3' style='height:100px;border:0px solid
							// black;'>&nbsp;</td></tr>";
							// strText += "</table>";
							// }
							// else if (policyTerm.contains("12"))
							// {
							// strText += "<table>";
							// strText += "<tr><td class='Border3' style='height:60px;border:0px solid
							// black;'>&nbsp;</td></tr>";
							// strText += "</table>";
							// }
							//
							// //Riders
							// if (policyTerm.contains("10"))
							// {
							// if (oDT3Row == 9)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 1; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							// strText += "</table>";
							// }
							// else if (oDT3Row == 8)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 2; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							// strText += "</table>";
							// }
							// else if (oDT3Row == 7)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 3; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							//
							// strText += "</table>";
							// }
							// else if (oDT3Row == 6)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 5; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							//
							// strText += "</table>";
							// }
							// }
							// else{
							// if (oDT3Row == 9)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 6; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							// strText += "</table>";
							// }
							// else if (oDT3Row == 8)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 7; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							// strText += "</table>";
							// }
							// else if (oDT3Row == 7)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 8; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							//
							// strText += "</table>";
							// }
							// else if (oDT3Row == 6)
							// {
							// strText += "<table>";
							// for (int i = 0; i < 10; i++) {
							// strText += "<tr><td class='Border3' style='border:1px solid
							// black;'>&nbsp;</td></tr>";
							// }
							//
							// strText += "</table>";
							// }
							// }
							results.add(strText);
							// #endregion

							// #region "Footer2"
							strText = "";
							// Riders
							// strText += "<table>";
							// if (policyTerm.contains("10"))
							// {
							// for (int i = 0; i < 20; i++) {
							// strText += "<tr><td class='Border3' style='height:'>&nbsp;</td></tr>";
							// }
							// }
							// else{
							// for (int i = 0; i < 16; i++) {
							// strText += "<tr><td class='Border3' style='height:'>&nbsp;</td></tr>";
							// }
							// }
							// strText += "</table>";

							results.add(strText);
							results.add(lblSerialNo1);
							results.add(lblSerialNo2);
							// #endregion

							// #region "lblBody4- 4. Guaranteed Maturity Benefit with the PruSaver"
							ResultSet oDT7 = null;
							if (resultSet.next()) {
								oDT7 = (ResultSet) resultSet.getObject(1);
								strText = "";
								strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
										+ "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' rowspan=3 style='width:60px;'>ឆ្នាំបណ្ណសន្យា<BR>រ៉ាប់រង</th>"
										+ "<th class='Border2' rowspan=3 style='width:60px'>អាយុ</th>"
										+ "<th class='Border2' rowspan=3 style='width:110px;'>បុព្វលាភធានារ៉ាប់រងរាល់ឆ្នាំ<br/>(US$)</th>"
										+ "<th class='Border2' rowspan=3 style='width:110px;'>បុព្វលាភធានារ៉ាប់រងសរុប<br/>(US$)</th>"
										+ "<th class='Border2' colspan=4 style='width:'>អត្ថប្រយោជន៍ក្នុងករណីទទួលមរណភាព  ឬ<br/>ពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍ </th>"
										+ "<th class='Border2' rowspan=3 style='width:138px;font-weight:600;'>តម្លៃទឹកប្រាក់ត្រូវ​បាន​ធានាសម្រាប់​ការ​បោះបង់បណ្ណសន្យារ៉ាប់រង (GSV)<br/>(US$)</th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' colspan=2 style='width:148px'>មិនបណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
										+ "<th class='Border2' colspan=2 style='width:148px'>បណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
										+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:'>អត្ថប្រយោជន៏សរុប<br/>(US$)</th>"
										+ "<th class='Border2' style='width:'>អត្ថប្រយោជន៍​ចំណូល​គ្រួសារ<br/>(US$)</th>"
										+ "<th class='Border2' style='width:'>អត្ថប្រយោជន៏សរុប<br/>(US$)</th>"
										+ "<th class='Border2' style='width:'>អត្ថប្រយោជន៍​ចំណូល​គ្រួសារ<br/>(US$)</th>"
										+ "</tr>";
								while (oDT7.next()) {
									strText += "<tr>" + "<td class='Border2' style='width:'>"
											+ oDT7.getString("PolicyYear") + "</td>"
											+ "<td class='Border2' style='width:'>" + oDT7.getString("Age") + "</td>"
											+ "<td class='Border2' style='width:'>"
											+ String.format("%,.2f", oDT7.getBigDecimal("P_APE")) + "</td>"
											+ "<td class='Border2' style='width:'>"
											+ String.format("%,.2f", oDT7.getBigDecimal("P_Total")) + "</td>"
											+ "<td class='Border2' style='width:'>"
											+ String.format("%,.2f", oDT7.getBigDecimal("CLAM_Non_Accident")) + "</td>"
											+ "<td class='Border2' style='width:'>"
											+ String.format("%,.2f", oDT7.getBigDecimal("AIB_Non_Accident")) + "</td>"
											+ "<td class='Border2' style='width:'>"
											+ String.format("%,.2f", oDT7.getBigDecimal("CLAM_Accident")) + "</td>"
											+ "<td class='Border2' style='width:'>"
											+ String.format("%,.2f", oDT7.getBigDecimal("AIB_Accident")) + "</td>"
											+ "<td class='Border2' style='width:'>"
											+ String.format("%,.2f", oDT7.getBigDecimal("Surrender")) + "</td>"
											+ "</tr>";
								}
								strText += "</table>";
								// body1
								results.add(strText);
							} else {
								results.add("");
							}

							// #endregion

							// #region "lblBody5 -5. Life Insurance Benefit with PruSaver"
							ResultSet oDT8 = null;
							if (resultSet.next()) {
								oDT8 = (ResultSet) resultSet.getObject(1);
								if (oDT8.first()) {
									strText = "";
									strText += "<table style='width:100%;border-collapse:collapse; border:1px solid black;' border='0'>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' rowspan=3 style='width:60px;'>ឆ្នាំនៃ​​ផលិតផលបំពេញបន្ថែម</th>"
											+ "<th class='Border2' rowspan=3 style='width:60px'>អាយុ</th>"
											+ "<th class='Border2' rowspan=2 style='width:128px'>បុព្វលាភរ៉ាប់រងរាល់ឆ្នាំ</th>"
											+ "<th class='Border2' rowspan=2 style='width:128px'>បុព្វលាភរ៉ាប់រងសរុប</th>"
											+ "<th class='Border2' colspan=2 style='width:'>អត្ថប្រយោជន៍ក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px'>តម្លៃទឹកប្រាក់ដែលមាន<BR>ការធានា (នៅពេលបោះបង់<BR>ផលិតផលបំពេញបន្ថែម)(GCV)</th>"
											+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:148px'>មិនបណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
											+ "<th class='Border2' style='width:148px'>បណ្តាលមកពីគ្រោះថ្នាក់ចៃដន្យ</th>"
											+ "</tr>" + "<tr style='width:173px;background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
									oDT8.beforeFirst();
									while (oDT8.next()) {
										strText += "<tr>" + "<td class='Border2' style='width:'>"
												+ oDT8.getString("PolicyYear") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT8.getString("Age")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_APE")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_Total")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Non_Accident"))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Accident")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("Surrender")) + "</td>"
												+ "</tr>";
									}
									strText += "</table>";
									// body2
									results.add(strText);
									oDT7.close();
									oDT8.close();
								}
							} else {
								results.add("");
							}

							oDT1.close();
							oDT2.close();
							oDT3.close();
							oDT4.close();
							oDT5.close();
							oDT6.close();

						} finally {
							cstmt.close();
						}
						if (PS == 15) {
							results.add("Yes");
						} else {
							results.add("No");
						}

						return results; // this should contain All rows or objects you need for further processing
					}
				});
		return res;
	}

	@Transactional
	public List<String> generateEduSmartLatinQuote(final Long quoteId, final String lang, final String result) {
		List<String> res = sessionFactory.getCurrentSession()
				.doReturningWork(new ReturningWork<List<String> /* objectType returned */>() {
					@Override
					/* or object type you need to return to process */
					public List<String> execute(Connection conn) throws SQLException {
						PreparedStatement cstmt = conn.prepareStatement(
								"select * from ap.spgen_edusmart(?,?,'RN','R1','R2','R3','R4','R5','R6','R7','R8')",
								ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
						String policyTerm = "";
						String paymentMode = "";
						String paymenttype = "";
						int MB = 0;
						int PS = 0;
						int oDT3Row = 0;
						cstmt.setLong(1, quoteId);
						cstmt.setString(2, lang);
						// cstmt.setString(3, result);
						// Result list that would return ALL rows of ALL result sets
						List<String> results = new ArrayList<String>();
						String strText = "";
						ResultSet resultSet = null;
						ResultSet oDT1 = null;
						ResultSet oDT2 = null;
						ResultSet oDT3 = null;
						String tmpText = "";
						try {
							resultSet = cstmt.executeQuery();

							// #region "lblHeader1"
							resultSet.next();
							oDT1 = (ResultSet) resultSet.getObject(1);
							oDT1.first();
							PS = oDT1.getInt("PruRetirement");
							paymentMode = oDT1.getString("PaymentMode");
							paymenttype = oDT1.getString("paymenttype");
							strText += "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr>" + "<th class='Border2' style='text-align:left;'>&nbsp;</th>"
									+ "<th class='Border2' colspan='2'><u>Name</u></th>"
									+ "<th class='Border2'><u>Gender</u></th>" + "<th class='Border2'><u>Age</u></th>"
									+ "<th class='Border2' colspan='3'><u>Premium Mode</u></th>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='text-align:left;'>Policy Owner's Name:</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("POName") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POSex") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("POAge") + "</td>"
									+ "<td class='Border3' colspan=3 rowspan=2>" /*
																					 * + oDT1.getString("PaymentMode2")
																					 * + "/"
																					 */ + oDT1.getString("PaymentType2")
									+ "</td>" + "</tr>" + "<tr>"
									+ "<td class='Border2' style='text-align:left;'>Main Life Assured's Name:</td>"
									+ "<td class='Border2' colspan=2>" + oDT1.getString("LA1Name") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Sex2") + "</td>"
									+ "<td class='Border2'>" + oDT1.getString("LA1Age") + "</td>" + "</tr>";
							if (!oDT1.getString("LA2Name").equals("")) {
								strText = strText.replace("rowspan=2", "rowspan=3") + "<tr>"
										+ "<td class='Border3' style='text-align:left;'>Additional Life Assured's Name:</td>     "
										+ "<td class='Border3' colspan=2>" + oDT1.getString("LA2Name") + "</td>"
										+ "<td class='Border3'>" + oDT1.getString("LA2Sex2") + "</td>"
										+ "<td class='Border3'>" + oDT1.getString("LA2Age") + "</td>" + "</tr>";
							}

							SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd");

							Date dOBDate;
							try {
								dOBDate = dt.parse(oDT1.getString("LA1DateOfBirth"));
							} catch (Exception e) {
								dOBDate = new Date();
							}

							SimpleDateFormat df = new SimpleDateFormat("hhmmssSSS");
							Date date = new Date();
							String time = df.format(date);

							String lblSerialNo1 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P";
							String lblSerialNo2 = dt.format(dOBDate).replace("-", "") + "-"
									+ String.format("%06d", quoteId) + "-" + time + "-P2";

							// Particulars of Basic Plan and My Option Benefits:
							if (resultSet.next()) {
								oDT2 = (ResultSet) resultSet.getObject(1);
							}
							strText += " <tr > "
									+ "<td class='Border2' style='width:170px;text-align:left;'>Particulars of Basic Plan and Riders:</td>"
									+ "<th class='Border2' style='width:'>Life Assured</th>"
									+ "<th class='Border2' style='width:40px'>Policy Term (Years)</th>"
									+ "<th class='Border2' style='width:75px'>Premium Payment Term (Years)</th>"
									+ "<th class='Border2' style='width:75px'>Sum Assured (US$)</th>"
									+ "<th class='Border2' style='width:70px'>Monthly<br/><center>(US$)</center></th>"
									+ "<th class='Border2' style='width:100px'>Semi-Annually<br/><center>(US$)</center></th>"
									+ "<th class='Border2' style='width:70px'>Annually<br/><center>(US$)</center></th>"
									+ "</tr>";
							if (oDT2.first()) {
								policyTerm = oDT2.getString("PolicyTerm");

								strText += " <tr > " + "<td class='Border3' style='text-align:left;'>&nbsp;&nbsp; "
										+ oDT2.getString("logo") + "</td>" + "<td class='Border3' >"
										+ oDT2.getString("Life_Assured") + "</td>" + "<td class='Border3' >"
										+ oDT2.getString("PolicyTerm") + "</td>" + "<td class='Border3' >"
										+ oDT2.getString("PremiumTerm") + "</td>" + "<td class='Border3' >"
										+ String.format("%,.2f", oDT2.getBigDecimal("SA")) + "</td>"
										+ "<td class='Border3' >"
										+ (paymenttype.equalsIgnoreCase("C") ? "N/A"
												: String.format("%,.2f", oDT2.getBigDecimal("P12_Amt")))
										+ "</td>" + "<td class='Border3' >"
										+ String.format("%,.2f", oDT2.getBigDecimal("P02_Amt")) + "</td>"
										+ "<td class='Border3' >"
										+ String.format("%,.2f", oDT2.getBigDecimal("P01_Amt")) + "</td>" + "</tr>";
							}

							if (resultSet.next()) {
								oDT3 = (ResultSet) resultSet.getObject(1);
							}

							// get oDT3 rows count
							if (oDT3.last())
								oDT3Row = oDT3.getRow();

							if (oDT3.first()) {

								// My Option Benefits (Also known as Riders)
								if (oDT3Row > 6) {
									strText += " <tr > "
											+ "<td class='Border3' style='width:250px;text-align:left;' colspan=8>Riders</td>"
											+ "</tr>";
								}
								// oDT3.beforeFirst();
								while (oDT3.next()) {
									if (oDT3.getString("Product").equals("TT")) {
										if (oDT3.getString("ProductName").contains("%")) {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ (paymenttype.equalsIgnoreCase("C") ? "N/A"
															: String.format("%,.2f", oDT3.getBigDecimal("P12_Amt"))
																	+ "%")
													+ "</th>" + "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "%</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "%</th>"
													+ " </tr>";
										} else if (oDT3.isLast()) {
											String st12 = "";
											String st02 = "";
											String st01 = "";
											if (paymentMode.contains("12")) {
												st12 = "background-color:#bfc8cf;";
												st02 = "";
												st01 = "";
											} else if (paymentMode.contains("02")) {
												st12 = "";
												st02 = "background-color:#bfc8cf;";
												st01 = "";
											} else {
												st12 = "";
												st02 = "";
												st01 = "background-color:#bfc8cf;";
											}
											strText += "<tr>"
													+ "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:;" + st12 + "'>"
													+ (paymenttype.equalsIgnoreCase("C") ? "N/A"
															: String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")))
													+ "</th>" + "     <th class='Border2' style='width:;" + st02 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:;;" + st01 + "'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										} else {
											strText += "<tr>"
													+ "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>"
													+ oDT3.getString("ProductName") + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ (paymenttype.equalsIgnoreCase("C") ? "N/A"
															: String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")))
													+ "</th>" + "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</th>"
													+ "     <th class='Border2' style='width:'>"
													+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</th>"
													+ " </tr>";
										}
									} else if (oDT3.getString("Product").equals("TH_RTR2")) {
										strText += " <tr > "
												+ "<td class='Border3' style='width:170px;text-align:left;'>Additional Riders:</td>"
												+ "<th class='Border3' style='width:'>Life Assured</th>"
												+ "<th class='Border3' style='width:40px'>Policy Term</th>"
												+ "<th class='Border3' style='width:75px'>Premium Payment Term</th>"
												+ "<th class='Border3' style='width:75px'>Maturity Benifit (US$)</th>"
												+ "<th class='Border3' style='width:70px'>Monthly (US$)</th>"
												+ "<th class='Border3' style='width:100px'>Semi-Annually (US$)</th>"
												+ "<th class='Border3' style='width:70px'>Annually (US$)</th>"
												+ "</tr>";
									} else {
										strText += " <tr > "
												+ "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; "
												+ oDT3.getString("ProductName") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("Life_Assured")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ oDT3.getString("PolicyTerm") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT3.getString("PremiumTerm")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("SA")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ (paymenttype.equalsIgnoreCase("C") ? "N/A"
														: String.format("%,.2f", oDT3.getBigDecimal("P12_Amt")))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P02_Amt")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT3.getBigDecimal("P01_Amt")) + "</td>"
												+ "</tr>";
									}

								}

								oDT3.beforeFirst();

								while (oDT3.next()) {

								}

							}
							strText += "</table>";
							// header1
							results.add(strText);

							// #endregion

							// #region "lblBody1- 1. Guaranteed Maturity Benefit with the Basic Policy that
							// can be Used as the Education Fund for the Child"
							ResultSet oDT4 = null;
							if (resultSet.next()) {
								oDT4 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							if (oDT4.next()) {
								strText += "<tr>"
										+ "<td colspan=8 style='text-align:justify;'>Maturity Benefit (Payable at the time of maturity, provided the Policy is in force at that time) (US$)</td>"
										+ "<td class='Border2' style='width:'><b>"
										+ (String.format("%,.2f", oDT4.getBigDecimal("OPT1_LumpSum")).equals("0.00")
												? " "
												: String.format("%,.2f", oDT4.getBigDecimal("OPT1_LumpSum")))
										+ "</b></td>" + "</tr>";
							}
							tmpText = strText;
							// body1
							results.add(strText);

							// #endregion

							// #region "lblBody2 -2. Life Insurance Benefit, that Helps the Family Take Care
							// of the Immediate Liabilities"
							ResultSet oDT5 = null;
							if (resultSet.next()) {
								oDT5 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
									+ "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>Policy Year</th>"
									+ "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>"
									+ "<th class='Border2' rowspan=3 style='width:110px;'>Premium paid every year<br/><center>(US$)</center></th>"
									+ "<th class='Border2' rowspan=3 style='width:110px;'>Total Premium Paid<br/><center>(US$)</center></th>"
									+ "<th class='Border2' colspan=4 style='width:276px;'>Death/Total and Permanent Disability (TPD) Benefit</th>"
									+ "<th class='Border2' rowspan=3 style='width:138px;'>Guaranteed Surrender Value<br/><center>(US$)</center></th>"
									+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' colspan=2 style='width:'>Not Caused by an Accident</th>"
									+ "<th class='Border2' colspan=2 style='width:'>Caused by an Accident</th>"
									+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
									+ "<th class='Border2' style='width:'>Sum Assured<br/><center>(US$)</center></th>"
									+ "<th class='Border2' style='width:'>Family Income Benefit<br/><center>(US$)</center></th>"
									+ "<th class='Border2' style='width:'>Sum Assured<br/><center>(US$)</center></th>"
									+ "<th class='Border2' style='width:'>Family Income Benefit<br/><center>(US$)</center></th>"
									+ "</tr>";
							while (oDT5.next()) {
								strText += "<tr>" + "<td class='Border2' style='width:'>" + oDT5.getString("PolicyYear")
										+ "</td>" + "<td class='Border2' style='width:'>" + oDT5.getString("Age")
										+ "</td>" + "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("P_APE")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("P_Total")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Non_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("AIB_Non_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("CLAM_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'>"
										+ String.format("%,.2f", oDT5.getBigDecimal("AIB_Accident")) + "</td>"
										+ "<td class='Border2' style='width:'><b>"
										+ String.format("%,.2f", oDT5.getBigDecimal("Surrender")) + "</b></td>"
										+ "</tr>";
							}
							strText += tmpText;
							strText += "</table>";
							// body2
							results.add(strText);
							// #endregion

							// #region "lblBody3 -3. Benefits with Additional Riders, that Help the Family
							// in the Further Securing Child's Education and Future"
							ResultSet oDT6 = null;
							if (resultSet.next()) {
								oDT6 = (ResultSet) resultSet.getObject(1);
							}
							strText = "";
							if (oDT6.first()) {
								strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
										+ "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:120px;text-align:center;' colspan=1 rowspan=3>Additional Rider Benefits</th>"
										+ "<th class='Border2' style='width:' rowspan=3>Life Assured Name</th>"
										+ "<th class='Border2' style='width:200px;' colspan=2>Death/TPD Benefit</th>"
										+ "<th class='Border2' style='width:350px;' rowspan=3>Benefit Payment Terms</th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2'>Not Caused by an Accident</th>"
										+ "<th class='Border2'>Caused by an Accident</th>" + "</tr>"
										+ "<tr style='background-color:#bfc8cf;'>" + "<th class='Border2'>(US$)</th>"
										+ "<th class='Border2'>(US$)</th>" + "</tr>";
								oDT6.beforeFirst();
								while (oDT6.next()) {
									strText += "<tr>" + "<td class='Border2' colspan=1 >"
											+ oDT6.getString("ProductName") + "</td>"
											+ "<td class='Border2' colspan=1 >" + oDT6.getString("Life_Assured")
											+ "</td>" + "<td class='Border2' colspan=1 width=90>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Non_Accident")) + "</td>"
											+ "<td class='Border2' colspan=1 width=90>"
											+ String.format("%,.2f", oDT6.getBigDecimal("CLAM_Accident")) + "</td>"
											+ "<td class='Border2' style='width:350px' colspan=1>"
											+ oDT6.getString("Payment_Terms") + "</td>" + "</tr>";
								}
								strText += "</table>";
							}
							// body3
							results.add(strText);
							// #endregion

							// #region "Footer1"
							strText = "";
							// Life Insurance Benefit
							if (policyTerm.contains("10")) {
								strText += "<table>";
								strText += "<tr><td class='' style='height:90px'>&nbsp;</td></tr>";
								strText += "</table>";
							} else if (policyTerm.contains("12")) {
								strText += "<table>";
								strText += "<tr><td class='' style='height:60px'>&nbsp;</td></tr>";
								strText += "</table>";
							}

							// Riders
							if (oDT3Row == 9) {
								strText += "<table>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "</table>";
							} else if (oDT3Row == 8) {
								strText += "<table>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "</table>";
							} else if (oDT3Row == 7) {
								strText += "<table>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "</table>";
							} else if (oDT3Row == 6) {
								strText += "<table>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
								strText += "</table>";
							}
							results.add(strText);
							// #endregion

							//
							results.add(lblSerialNo1);
							results.add(lblSerialNo2);
							// #endregion

							// #region "lblBody4- 4. Guaranteed Maturity Benefit with the PruSaver"
							ResultSet oDT7 = null;
							if (resultSet.next()) {
								oDT7 = (ResultSet) resultSet.getObject(1);
								strText = "";
								strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
										+ "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' rowspan=3 style='width:60px;'>Policy Year</th>"
										+ "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>"
										+ "<th class='Border2' rowspan=3 style='width:110px;'>Premium paid every year<br/><center>(US$)</center></th>"
										+ "<th class='Border2' rowspan=3 style='width:110px;'>Total Premium Paid<br/><center>(US$)</center></th>"
										+ "<th class='Border2' colspan=4 style='width:276px;'>Death/Total and Permanent Disability (TPD) Benefit</th>"
										+ "<th class='Border2' rowspan=3 style='width:138px;'>Guaranteed Surrender Value<br/><center>(US$)</center></th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' colspan=2 style='width:'>Not Caused by an Accident</th>"
										+ "<th class='Border2' colspan=2 style='width:'>Caused by an Accident</th>"
										+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
										+ "<th class='Border2' style='width:'>Sum Assured<br/><center>(US$)</center></th>"
										+ "<th class='Border2' style='width:'>Family Income Benefit<br/><center>(US$)</center></th>"
										+ "<th class='Border2' style='width:'>Sum Assured<br/><center>(US$)</center></th>"
										+ "<th class='Border2' style='width:'>Family Income Benefit<br/><center>(US$)</center></th>"
										+ "</tr>";
								while (oDT7.next()) {
									strText += "<tr>" + "<td class='Border2' style='width:'>"
											+ oDT7.getString("PolicyYear") + "</td>"
											+ "<td class='Border2' style='width:'>" + oDT7.getString("Age") + "</td>"
											+ "<td class='Border2' style='width:'>"
											+ String.format("%,.2f", oDT7.getBigDecimal("P_APE")) + "</td>"
											+ "<td class='Border2' style='width:'>"
											+ String.format("%,.2f", oDT7.getBigDecimal("P_Total")) + "</td>"
											+ "<td class='Border2' style='width:'>"
											+ String.format("%,.2f", oDT7.getBigDecimal("CLAM_Non_Accident")) + "</td>"
											+ "<td class='Border2' style='width:'>"
											+ String.format("%,.2f", oDT7.getBigDecimal("AIB_Non_Accident")) + "</td>"
											+ "<td class='Border2' style='width:'>"
											+ String.format("%,.2f", oDT7.getBigDecimal("CLAM_Accident")) + "</td>"
											+ "<td class='Border2' style='width:'>"
											+ String.format("%,.2f", oDT7.getBigDecimal("AIB_Accident")) + "</td>"
											+ "<td class='Border2' style='width:'>"
											+ String.format("%,.2f", oDT7.getBigDecimal("Surrender")) + "</td>"
											+ "</tr>";
								}
								strText += "</table>";
								// body1
								results.add(strText);
							} else {
								results.add("");
							}

							// #endregion

							// #region "lblBody5 -5. Life Insurance Benefit with PruSaver"
							ResultSet oDT8 = null;
							if (resultSet.next()) {
								oDT8 = (ResultSet) resultSet.getObject(1);
								if (oDT8.first()) {
									strText = "";
									strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' rowspan=3 style='width:60px;'>Rider Year</th>"
											+ "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px;'>Rider premium<br/>every year</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px;'>Total Rider Premium</th>"
											+ "<th class='Border2' colspan=2 style='width:276px;'>Death/TPD Benefit</th>"
											+ "<th class='Border2' rowspan=2 style='width:138px;'>Guaranteed Cash Value(GCV)</th>"
											+ "</tr>" + "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>Not Caused by an Accident</th>"
											+ "<th class='Border2' style='width:'>Caused by an Accident</th>" + "</tr>"
											+ "<tr style='background-color:#bfc8cf;'>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>"
											+ "<th class='Border2' style='width:'>(US$)</th>" + "</tr>";
									oDT8.beforeFirst();
									while (oDT8.next()) {
										strText += "<tr>" + "<td class='Border2' style='width:'>"
												+ oDT8.getString("PolicyYear") + "</td>"
												+ "<td class='Border2' style='width:'>" + oDT8.getString("Age")
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_APE")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("P_Total")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Non_Accident"))
												+ "</td>" + "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("CLAM_Accident")) + "</td>"
												+ "<td class='Border2' style='width:'>"
												+ String.format("%,.2f", oDT8.getBigDecimal("Surrender")) + "</td>"
												+ "</tr>";
									}
									strText += "</table>";
									// body2
									results.add(strText);
									oDT7.close();
									oDT8.close();
								}
							} else {
								results.add("");
							}

							// #endregion

							oDT1.close();
							oDT2.close();
							oDT3.close();
							oDT4.close();
							oDT5.close();
							oDT6.close();

						} finally {
							cstmt.close();
						}
						if (PS == 15) {
							results.add("Yes");
						} else {
							results.add("No");
						}
						return results; // this should contain All rows or objects you need for further processing
					}
				});
		return res;
	}

	public List<PruquoteParm> findValidQuote(Long id, String product, String user) {
		return pruquoteParamRepository.findByIdAndProductAndCoUserAndValidFlag(id, product, user, 1);
	}

	public List<PruquoteParm> findValidQuote(Long id, String user) {
		return pruquoteParamRepository.findByIdAndCoUserAndValidFlag(id, user, 1);
	}

	public List<PruquoteParm> findValidQuote(Long id) {
		return pruquoteParamRepository.findById(id);
	}

	/*
	 * public List<Object> findValidQuoteByGroup(String user,Long id){ //return
	 * listPruquoteParamRepository.findByIdAndCoUser(user, id); Query query =
	 * sessionFactory.getCurrentSession().createSQLQuery(
	 * "call spList_EasyLife(:userid,:id)") .setParameter("userid", user)
	 * .setParameter("id",id );
	 * 
	 * @SuppressWarnings("unchecked") List<Object> listEasyLifes=query.list();
	 * return listEasyLifes;
	 * 
	 * }
	 */
	@Transactional
	public PruquoteParm getValidQuote(Long id, String product, String user) {
		List<PruquoteParm> pruquoteParms = pruquoteParamRepository.findByIdAndProductAndCoUserAndValidFlag(id, product,
				user, 1);
		if (pruquoteParms.size() > 0) {
			PruquoteParm p = pruquoteParamRepository.findByIdAndProductAndCoUserAndValidFlag(id, product, user, 1)
					.get(0);
			if (p != null)
				Hibernate.initialize(p.getOccupations());
			return p;
		} else {
			return null;
		}
	}

	public PruquoteParm getValidQuote(Long id, String user) {
		List<PruquoteParm> pruquoteParms = pruquoteParamRepository.findByIdAndCoUserAndValidFlag(id, user, 1);
		if (pruquoteParms.size() > 0)
			return pruquoteParamRepository.findByIdAndCoUserAndValidFlag(id, user, 1).get(0);
		else {
			return null;
		}
	}

	@Transactional
	public List<Object> prusaverPremiumValidate(Long quoteId) {
		Query query = sessionFactory.getCurrentSession()
				.createSQLQuery("select * from ap.spGen_PruQuote_SaverValidat(:quoteId,'En')");
		query.setParameter("quoteId", quoteId);
		@SuppressWarnings("unchecked")
		List<Object> results = query.list();
		return results;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object> listEasyLife(String user, Long quoteId) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select * from ap.spList_EasyLife(:userid,:id)")
				.setParameter("userid", user).setParameter("id", quoteId.toString());

		List<Object> listEasyLifes = null;
		try {
			listEasyLifes = query.list();
		} catch (HibernateException ex) {
			ex.printStackTrace();
		}

		return listEasyLifes;
	}

	@Transactional
	public List<Object> listEasyLifeForm(String user, Long quoteId) {
		Query query = sessionFactory.getCurrentSession()
				.createSQLQuery("select * from ap.spList_EasyLife_Form(:userid,:id)").setParameter("userid", user)
				.setParameter("id", quoteId.toString());
		@SuppressWarnings("unchecked")
		List<Object> listEasyLifeForms = query.list();
		return listEasyLifeForms;
	}

	@Transactional
	public List<Object> listEasyLifeFormMaxApp(String user, String quoteId) {
		Query query = sessionFactory.getCurrentSession()
				.createSQLQuery("select * from ap.spList_EasyLife_FormMaxApp(:userid,:id)").setParameter("userid", user)
				.setParameter("id", quoteId);
		@SuppressWarnings("unchecked")
		List<Object> listEasyLifeFormMaxApps = query.list();
		return listEasyLifeFormMaxApps;
	}

	@Transactional
	public void IDU_easylife_form(String id, String txtappnum, String txtoccdate, String txtagntname, String txtchdrnum,
			String txtagntnum, String txttrndate, String txtagntphone, String txtponamekh, String txtponameen,
			String txtponid, String txtpodob, String txtpopob, String txtponat, String txtpostatus, String txtpoocc,
			String txtpocurocc, String txtposal, String txtaddr, String txtpophone, String txtpoemail,
			String txtpoheight, String txtpoweight, String txtpocig, String txttrq1, String txttrq2, String txttrq3,
			String txttrq4, String txttrq51, String txttrq52, String txttrq6, String txtbenname1,
			String txtbennamelatin1, String txtbensex1, String txtbendob1, String txtbencon1, String txtbenper1,
			String txtbenname2, String txtbennamelatin2, String txtbensex2, String txtbendob2, String txtbencon2,
			String txtbenper2, String txtbenname3, String txtbennamelatin3, String txtbensex3, String txtbendob3,
			String txtbencon3, String txtbenper3, String txtbenname4, String txtbennamelatin4, String txtbensex4,
			String txtbendob4, String txtbencon4, String txtbenper4, String fatca, String txtcontry,
			String txtcontryphone, String txtnote, String baction, String userid) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"select ap.spIDU_easylife_form(:id,:txtappnum,:txtoccdate,:txtagntname,:txtchdrnum,:txtagntnum,:txttrndate,:txtagntphone,:txtponamekh,"
						+ ":txtponameen,:txtponid,:txtpodob,:txtpopob,:txtponat,:txtpostatus,:txtpoocc,:txtpocurocc,:txtposal,:txtaddr,"
						+ ":txtpophone,:txtpoemail,:txtpoheight,:txtpoweight,:txtpocig,:txttrq1,:txttrq2,:txttrq3,:txttrq4,:txttrq51,"
						+ ":txttrq52,:txttrq6,:txtbenname1,:txtbennamelatin1,:txtbensex1,:txtbendob1,:txtbencon1,:txtbenper1,:txtbenname2,:txtbennamelatin2,:txtbensex2,"
						+ ":txtbendob2,:txtbencon2,:txtbenper2,:txtbenname3,:txtbennamelatin3,:txtbensex3,:txtbendob3,:txtbencon3,:txtbenper3,:txtbenname4,:txtbennamelatin4,"
						+ ":txtbensex4,:txtbendob4,:txtbencon4,:txtbenper4,:fatca,:txtcontry,:txtcontryphone,:txtnote,:baction,:userid)")
				.setParameter("id", id).setParameter("txtappnum", txtappnum).setParameter("txtoccdate", txtoccdate)
				.setParameter("txtagntname", txtagntname).setParameter("txtchdrnum", txtchdrnum)
				.setParameter("txtagntnum", txtagntnum).setParameter("txttrndate", txttrndate)
				.setParameter("txtagntphone", txtagntphone).setParameter("txtponamekh", txtponamekh)
				.setParameter("txtponameen", txtponameen).setParameter("txtponid", txtponid)
				.setParameter("txtpodob", txtpodob).setParameter("txtpopob", txtpopob)
				.setParameter("txtponat", txtponat).setParameter("txtpostatus", txtpostatus)
				.setParameter("txtpoocc", txtpoocc).setParameter("txtpocurocc", txtpocurocc)
				.setParameter("txtposal", txtposal).setParameter("txtaddr", txtaddr)
				.setParameter("txtpophone", txtpophone).setParameter("txtpoemail", txtpoemail)
				.setParameter("txtpoheight", txtpoheight).setParameter("txtpoweight", txtpoweight)
				.setParameter("txtpocig", txtpocig).setParameter("txttrq1", txttrq1).setParameter("txttrq2", txttrq2)
				.setParameter("txttrq3", txttrq3).setParameter("txttrq4", txttrq4).setParameter("txttrq51", txttrq51)
				.setParameter("txttrq52", txttrq52).setParameter("txttrq6", txttrq6)
				.setParameter("txtbenname1", txtbenname1).setParameter("txtbennamelatin1", txtbennamelatin1)
				.setParameter("txtbensex1", txtbensex1).setParameter("txtbendob1", txtbendob1)
				.setParameter("txtbencon1", txtbencon1).setParameter("txtbenper1", txtbenper1)
				.setParameter("txtbenname2", txtbenname2).setParameter("txtbennamelatin2", txtbennamelatin2)
				.setParameter("txtbensex2", txtbensex2).setParameter("txtbendob2", txtbendob2)
				.setParameter("txtbencon2", txtbencon2).setParameter("txtbenper2", txtbenper2)
				.setParameter("txtbenname3", txtbenname3).setParameter("txtbennamelatin3", txtbennamelatin3)
				.setParameter("txtbensex3", txtbensex3).setParameter("txtbendob3", txtbendob3)
				.setParameter("txtbencon3", txtbencon3).setParameter("txtbenper3", txtbenper3)
				.setParameter("txtbenname4", txtbenname4).setParameter("txtbennamelatin4", txtbennamelatin4)
				.setParameter("txtbensex4", txtbensex4).setParameter("txtbendob4", txtbendob4)
				.setParameter("txtbencon4", txtbencon4).setParameter("txtbenper4", txtbenper4)
				.setParameter("fatca", fatca).setParameter("txtcontry", txtcontry)
				.setParameter("txtcontryphone", txtcontryphone).setParameter("txtnote", txtnote)
				.setParameter("baction", 'I').setParameter("userid", userid);
		// Query query = sessionFactory.getCurrentSession().createSQLQuery("select
		// ap.spidu_easylife_form(null, null, '1900-01-01', null, '312899', null,
		// '1900-01-01', null, null, null, null, '1900-01-01', null, null, null, null,
		// null, null,null, null, null, null, null, null, null, null, null, null, null,
		// null, null, null,null, null, '1900-01-01', null, null, null,null, null,
		// '1900-01-01', null, null, null,null, null, '1900-01-01', null, null,
		// null,null, null, '1900-01-01', null, null,null,null,null,null,
		// 'B','312890')");
		// query.executeUpdate();
		try {
			query.executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			query = null;

		}
	}

	@Transactional
	public double GetMortgageDisbursedAmount(String paymentType, String product, String policyTerm, String sex,
			String loanAmount, String dob, String occ, String isFundedByBank) {
		double disbursedAmount = 0;

		Query query = sessionFactory.getCurrentSession()
				.createSQLQuery("select ap.spGenSA_By_LD (:loanAmount, "
						+ ":product, :policyTerm, :paymentType, :sex, :occ, :dob, :isFundedByBank)")
				.setParameter("paymentType", paymentType).setParameter("product", product)
				.setParameter("policyTerm", policyTerm).setParameter("sex", sex).setParameter("loanAmount", loanAmount)
				.setParameter("dob", dob).setParameter("occ", occ).setParameter("isFundedByBank", isFundedByBank);

		try {
			disbursedAmount = Double.parseDouble(query.list().get(0).toString());
		} catch (HibernateException ex) {
			ex.printStackTrace();
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
		}

		return disbursedAmount;
	}

	@SuppressWarnings("deprecation")
	@Transactional
	public Map<String, Object> generateQuote(Long quoteId, String lang, String agentName, String product) {

		Map<String, Object> quote = null;

		try {

			Query querySetting = sessionFactory.getCurrentSession()
					.createSQLQuery("select query, html, htmlkh from ap.tabprintquotesettings where product = :product")
					.setParameter("product", product);

			List<Object[]> quoteSettings = querySetting.list();
			String queryStr = quoteSettings.get(0)[0].toString();
			Query query = sessionFactory.getCurrentSession().createSQLQuery(queryStr).setParameter("quoteId", quoteId);
			query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			List<Map<String, Object>> aliasToValueMapList = query.list();

			// assign quote values
			quote = aliasToValueMapList.get(0);
			String date = new SimpleDateFormat("dd MMM yyyy hh:mm:ss aa").format(new Date());
			quote.put("printdate", date);
			quote.put("year", new SimpleDateFormat("yyyy").format(new Date()));
			quote.put("agentname", agentName);

			
			String version = "PureProtect_Ver";
			if(product == "BTR6")
			{
				version = "SafeLife_Ver";
			}
			
			String la1dateofbirth = quote.get("la1dateofbirth").toString();
			String serial01 = version + quote.get("year") +"."+ GlobalVar.version + "_" + quote.get("poname")
					+ "_" + la1dateofbirth + "_.p.1" + "_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
			String serial02 = version + quote.get("year") +"."+ GlobalVar.version + "_" + quote.get("poname")
					+ "_" + la1dateofbirth + "_.p.2" + "_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
			quote.put("serial01", serial01);
			quote.put("serial02", serial02);

			//
			// Query querySetting = sessionFactory.getCurrentSession()
			// .createSQLQuery("select query, html, htmlkh from ap.tabprintquotesettings
			// where product = :product")
			// .setParameter("product", product);
			//
			// List<Object[]> quoteSettings = querySetting.list();
			// String queryStr = quoteSettings.get(0)[0].toString();
			// Query query =
			// sessionFactory.getCurrentSession().createSQLQuery(queryStr).setParameter("quoteId",
			// quoteId);
			//
			// query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			// List<Map<String, Object>> aliasToValueMapList = query.list();
			//
			// Velocity.init();
			//
			// VelocityContext context = new VelocityContext();
			// for (String key : aliasToValueMapList.get(0).keySet()) {
			// context.put(key, aliasToValueMapList.get(0).get(key) == null ? ""
			// : aliasToValueMapList.get(0).get(key).toString());
			// }
			//
			// String date = new SimpleDateFormat("dd MMM yyyy hh:mm:ss aa").format(new
			// Date());
			// context.put("printdate", date);
			// context.put("year", new SimpleDateFormat("yyyy").format(new Date()));
			// context.put("agentname", agentName);
			//
			// context.put("version", GlobalVar.version);
			//
			// String template = null;
			// if(lang.equals("kh"))
			// {
			// template = quoteSettings.get(0)[2].toString();
			// }
			// else if (lang.equals("en"))
			// {
			// template = quoteSettings.get(0)[1].toString();
			// }
			//
			// if(template == null)
			// throw new HibernateException("Template Html cannot found");
			//
			// StringWriter writer = new StringWriter();
			// Velocity.evaluate(context, writer, "TemplateName", template);
			//
			// quote = writer.toString();
		} catch (HibernateException ex) {
			ex.printStackTrace();
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
		}
		return quote;
	}
}
