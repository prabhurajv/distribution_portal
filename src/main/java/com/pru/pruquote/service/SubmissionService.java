package com.pru.pruquote.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.Submission;
import com.pru.pruquote.utility.GlobalVar;

@Service
public class SubmissionService {
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Cacheable(value = "listsubmissionservice")
	public List<Submission> getListSubmission(String userid) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select" +
				 		"  chdrnum as \"chdrNum\"" +
						", chdr_appnum as \"chdrAppNum\"" +
						", po_name as \"poName\"" +
						", sub_date as \"subDate\"" +
						", chdr_iss_date as \"chdrIssDate\"" +
						", chdr_statcode as \"chdrStatCode\"" +
						", chdr_ntu_date as \"chdrNtuDate\"" +
						", chdr_wd_date as \"chdrWdDate\" " +
						", chdr_po_date as \"chdrPoDate\" " +
						", chdr_cf_date as \"chdrCfDate\" " +
						", chdr_ape as \"chdrApe\" " +
						", agntnum as \"agntNum\" " +
						", agnt_name as \"agntName\" " +
						", weekth as \"weekth\" " +
						", branch_code as \"branchCode\" " +
						", branch_name as \"branchName\" " +
						"from "	+ GlobalVar.db_name + ".spsearch_submission(:userid)")
						.setParameter("userid", userid)
						.setResultTransformer(Transformers.aliasToBean(Submission.class));
		
		List<Submission> listSubmissions = (List<Submission>)query.list();
		
		return listSubmissions;
	}
}