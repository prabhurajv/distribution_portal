package com.pru.pruquote.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.CodeDesc;
import com.pru.pruquote.model.CollectionAndLapse;
import com.pru.pruquote.model.Issuance;
import com.pru.pruquote.model.PendingCase;
import com.pru.pruquote.model.Persistency;
import com.pru.pruquote.model.PersistencyDetail;
import com.pru.pruquote.model.Submission;
import com.pru.pruquote.utility.GlobalVar;

@Service
public class PendingCaseService{
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Cacheable(value = "pendingCases")
	public List<PendingCase> pendingCases(String userid) {
		// TODO Auto-generated method stub
//		delayThread(10000);
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select chdrnum as \"chdrNum\"" +
						", appnum as \"appNum\"" +
						", po_name as \"poName\"" +
						", chdr_ape as \"chdrApe\"" +
						", pending_reason as \"pendingReason\"" +
						", status as \"pendingStatus\"" +
						", pending_period as \"pendingPeriod\"" +
						", ntu_date as \"ntuDate\"" +
						", refresh_time as \"refreshTime\"" +
						", po_phonenumber as \"poPhoneNumber\"" +
						", agnt_num as \"agntNum\"" +
						", agent_name as \"agntName\"" +
						", chdr_key_date as \"chdrKeyDate\"" +
						", penal_doctor as \"penalDoctor\"" +
						", agnt_phone as \"agntPhone\"" +
						", doctor_image as \"doctorImage\"" +
						", fupno as \"fupNo\" from "	+ GlobalVar.db_name + ".spsearch_pending(:userid) ORDER BY pending_period desc")
						.setParameter("userid", userid)
						.setResultTransformer(Transformers.aliasToBean(PendingCase.class));
		
		List<PendingCase> pendingCases = (List<PendingCase>)query.list();
		
		return pendingCases;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Cacheable(value = "listsubmission")
	public List<Submission> listSubmission(String userid, String summaryby) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select chdrnum as \"chdrNum\"" +
						", chdr_appnum as \"chdrAppNum\"" +
						", po_name as \"poName\"" +
						", sub_date as \"subDate\"" +
						", chdr_ape as \"chdrApe\"" +
						", agntnum as \"agntNum\"" +
						", agnt_name as \"agntName\"" +
						", weekth as \"weekth\" from "	+ GlobalVar.db_name + ".spsearch_submission(:userid)")
						.setParameter("userid", userid)
						.setResultTransformer(Transformers.aliasToBean(Submission.class));
		
		List<Submission> listSubmissions = (List<Submission>)query.list();
		
		return listSubmissions;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Cacheable(value = "listnetissue")
	public List<Issuance> listIssue(String userid, String summaryby) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select chdrnum as \"chdrNum\"" +
						", chdr_appnum as \"chdrAppNum\"" +
						", po_name as \"poName\"" +
						", chdr_iss_date as \"chdrIssDate\"" +
						", chdr_ape as \"chdrApe\"" +
						", agntnum as \"agntNum\"" +
						", agnt_name as \"agntName\"" +
						", weekth as \"weekth\" from "	+ GlobalVar.db_name + ".spsearch_issuance(:userid)")
						.setParameter("userid", userid)
						.setResultTransformer(Transformers.aliasToBean(Issuance.class));
		
		List<Issuance> listNetIssues = (List<Issuance>)query.list();
		
		return listNetIssues;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Cacheable(value = "pendingCount")
	public List<Object[]> selectAllStatus(String userid, String summaryby) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select * from "	+ GlobalVar.db_name + ".splist_countpending(:userid,:summaryby)")
				.setParameter("userid", userid)
				.setParameter("summaryby",summaryby);

		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Cacheable(value = "persistency")
	public List<Persistency> selectPersistency(String userid) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select" +
				" agntnum as \"agntNum\"" +
				", agntname as \"agntName\"" +
				", if_ape as \"ifApe\" " +
				", la_ape as \"laApe\" " +
				", su_ape as \"suApe\" " +
				", percent_for_q as \"percentForQ\" " +
				", persistency_for_q as \"persistencyForQ\" " +
				"from "	+ GlobalVar.db_name + ".splist_persistency(:userid)")
				.setParameter("userid", userid)
				.setResultTransformer(Transformers.aliasToBean(Persistency.class));
		
		List<Persistency> listPersistencies = (List<Persistency>)query.list();
		
		return listPersistencies;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Cacheable(value = "persistency")
	public List<PersistencyDetail> selectPersistencyDetail(String userid, String forq) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select" +
				" * from "	+ GlobalVar.db_name + ".splist_persistency_raw(:userid,:forq)")
				.setParameter("userid", userid)
				.setParameter("forq", forq)
				.setResultTransformer(Transformers.aliasToBean(PersistencyDetail.class));
		
		List<PersistencyDetail> listPersistencies = (List<PersistencyDetail>)query.list();
		
		return listPersistencies;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object[]> selectPersistencySummary(String userid) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select" +
				" * from "	+ GlobalVar.db_name + ".splist_persistency_summary(:userid)")
				.setParameter("userid", userid);
		
		return query.list();
	}
	
	@Transactional
	@Cacheable(value = "pendingCodeDesc")
	public List<CodeDesc> codeDesc() {
		Query query = sessionFactory.getCurrentSession().createQuery("from CodeDesc");
		@SuppressWarnings("unchecked")
		List<CodeDesc> codeDesc = (List<CodeDesc>)query.list();
		
		return codeDesc;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Cacheable(value = "subandissue")
	public List<Object[]> selectAllSubAndIssue(String userid) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select * from " + GlobalVar.db_name + ".splist_subandissue(:userid)")
				.setParameter("userid", userid);

		return query.list();
	}
	
	@Transactional
	public boolean isPolicyOwner(String userid, String policynum){
		int result = 0;
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select " + GlobalVar.db_name + 
						".spsearch_pending_cnt(:userid, :policynum)")
						.setParameter("userid", userid)
						.setParameter("policynum", policynum);
		
		try{
			result = Integer.parseInt(query.uniqueResult().toString());
		}catch(NullPointerException e){
			e.printStackTrace();
		}catch(NumberFormatException e){
			e.printStackTrace();
		}
		
		return result > 0;
	}
	

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Object> selectImmediateaction(String userid) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select action_cat, sum(total_pol), number_of_day,status_code from "	+ GlobalVar.db_name + ".splist_actions(:userid) group by action_cat, status_code, number_of_day,sort order by sort")
				.setParameter("userid", userid);

		return query.list();
	}

	private void delayThread(long mil){
		try {
			Thread.sleep(mil);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
