package com.pru.pruquote.service;

import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.controller.ResourceNotFoundException;
import com.pru.pruquote.model.Discountrate;
import com.pru.pruquote.model.Itemitem;
import com.pru.pruquote.model.Itemtabl;
import com.pru.pruquote.model.Mbrate;
import com.pru.pruquote.model.Modalfactor;
import com.pru.pruquote.model.Premiumrate;
import com.pru.pruquote.model.Surrender;
import com.pru.pruquote.model.UABFactor;
import com.pru.pruquote.repository.DiscountRateRepository;
import com.pru.pruquote.repository.ItemitemRepository;
import com.pru.pruquote.repository.ItemtablRepository;
import com.pru.pruquote.repository.MbRateRepository;
import com.pru.pruquote.repository.ModalFactorRepository;
import com.pru.pruquote.repository.PremiumRateRepository;
import com.pru.pruquote.repository.SurrenderRepository;
import com.pru.pruquote.repository.UABFactorRepository;
import com.pru.pruquote.utility.GlobalVar;


@Service
public class TableSetupService {
	
	@Autowired
	ItemtablRepository itemtablRepository;
	
	@Autowired
	ItemitemRepository itemitemRepository;
	
	@Autowired
	PremiumRateRepository premiumrateRepository;
		
	@Autowired
	DiscountRateRepository discountRateRepository;
	
	@Autowired
	MbRateRepository mbRateRepository;
	
	@Autowired
	ModalFactorRepository modalFactorRepository;
	
	@Autowired
	SurrenderRepository surrenderRepository;
	
	@Autowired
	UABFactorRepository uabFactorRepository;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Itemitem> itemitemFindAll() {
		return itemitemRepository.findAll();		
	}
	@Transactional
	public Set<Itemitem> itemitemFindNameByItemtable(Long id,boolean descending) {
		if(descending)
			return itemitemRepository.findItemNameByItemtableDesc(id);
		else 
			return itemitemRepository.findItemNameByItemtable(id);
	}
	@Transactional
	public Set<Itemitem> itemitemFindNameByItemtable(Long id) {
		return itemitemFindNameByItemtable(id,false);
	}
	
	public Set<Itemitem> itemitemFindNameByItemItemID(Long param1,Long param2,Long param3) {
		return itemitemRepository.findItemNameByItemitemID(param1,param2,param3);
	}
	
	public Set<Itemitem> itemitemFindNameByItemtablePro(Long id,String proCode) {
		return itemitemRepository.findItemNameByItemtablePro(id,proCode);
	}
	public List<Itemitem> itemitemFindNameByReserve1(String reserve1, int validFlag) {
		return itemitemRepository.findByReserve1AndValidFlag(reserve1, validFlag);
	}
	public List<Itemtabl> itemtablFindAll() {
		return itemtablRepository.findAll();		
	}	
	@Transactional(readOnly=true)
	public Itemitem itemitemFindOne(Long id) {
		return itemitemRepository.findOne(id);		
	}		
	public Itemtabl itemtablFindOne(Long id) {
		return itemtablRepository.findOne(id);		
	}
	public List<Premiumrate> premiumrateFindAll() {
		return premiumrateRepository.findAll();
	}
	
	public List<Premiumrate> premiumrateFindByItemId(int id) {
		return premiumrateRepository.findByCoPremiumRate(id);
	}
	public Premiumrate premiumrateFindOne(Long id) {
		return premiumrateRepository.findOne(id);
	}
	
	public List<Discountrate> discountrateFindAll() {
		return discountRateRepository.findAll();
	}
	public List<Discountrate> discountrateFindByItemId(Long id) {
		return discountRateRepository.findByCoDiscountRate(id);
	}
	public Discountrate discountrateFindOne(Long id) {
		return discountRateRepository.findOne(id);
	}
	
	public List<Mbrate> mbrateFindAll() {
		return mbRateRepository.findAll();
	}
	public List<Mbrate> mbrateFindByItemId(Long id) {
		return mbRateRepository.findByCoMbrate(id);
	}
	public Mbrate mbrateFindOne(Long id) {
		return mbRateRepository.findOne(id);
	}
	
	public List<Modalfactor> modalFactorFindAll() {
		return modalFactorRepository.findAll();
	}
	public List<Modalfactor> modalFactorFindByItemId(Long id) {
		return modalFactorRepository.findByCoModalFactor(id);
	}
	public Modalfactor modalFactorFindOne(Long id) {
		return modalFactorRepository.findOne(id);
	}
	
	public List<Surrender> surrenderFindAll() {
		return surrenderRepository.findAll();
	}
	public List<Surrender> surrenderFindByItemId(Long id) {
		return surrenderRepository.findByCoSurrender(id);
	}
	public Surrender surrenderFindOne(Long id) {
		return surrenderRepository.findOne(id);
	}
	public List<UABFactor> uabFactorFindAll() {
		return uabFactorRepository.findAll();
	}
	public List<UABFactor> uabFactorFindByItemId(Long id) {
		return uabFactorRepository.findByCoUABFactor(id);
	}
	public UABFactor uabFactorFindOne(Long id) {
		return uabFactorRepository.findOne(id);
	}
	@Transactional
	public void itemTableSave(Itemtabl itemtabl){
		sessionFactory.getCurrentSession().saveOrUpdate(itemtabl);
	}
	@Transactional
	public void itemItemSave(Itemitem itemitem){
		sessionFactory.getCurrentSession().saveOrUpdate(itemitem);
	}
	@Transactional
	public void premiumRateSave(Premiumrate premiumrate){
		sessionFactory.getCurrentSession().saveOrUpdate(premiumrate);
	}
	@Transactional
	public void discountRateSave(Discountrate discountrate){
		sessionFactory.getCurrentSession().saveOrUpdate(discountrate);
	}
	@Transactional
	public void mbRateSave(Mbrate mbrate){
		sessionFactory.getCurrentSession().saveOrUpdate(mbrate);
	}
	@Transactional
	public void modalFactorSave(Modalfactor modalfactor){
		sessionFactory.getCurrentSession().saveOrUpdate(modalfactor);
	}
	@Transactional
	public void surrenderSave(Surrender surrender){
		sessionFactory.getCurrentSession().saveOrUpdate(surrender);
	}
	@Transactional
	public void uabFactorSave(UABFactor uabFactor){
		sessionFactory.getCurrentSession().saveOrUpdate(uabFactor);
	}
	@Transactional
	public Itemtabl itemtablFindOneWithItem(Long id) {
		Itemtabl itemtabl=itemtablRepository.findOne(id);
		if(itemtabl==null) throw new ResourceNotFoundException();
		Set<Itemitem> items=itemitemRepository.findItemitemByItemtabl(itemtabl);
		itemtabl.setItemitems(items);
		return itemtabl;
	}
	
	@Transactional
	public String itemExpireRiderLongTermBuilder(String reserve1, int validflag) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT reserve2  FROM "+ GlobalVar.db_name +".tabitemitem where reserve1=:reserve1 and validflag=:validflag")
				.setParameter("reserve1", reserve1)
				.setParameter("validflag", validflag);
		Object result = query.uniqueResult();
		if(result != null)
			return result.toString();
		
		return "";
	}
}