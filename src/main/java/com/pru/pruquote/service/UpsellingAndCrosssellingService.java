package com.pru.pruquote.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.Referrer;
import com.pru.pruquote.model.UpsellingAndCrosssellingModel;
import com.pru.pruquote.utility.GlobalVar;

@Service
public class UpsellingAndCrosssellingService {
	
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<UpsellingAndCrosssellingModel> listUpsellingAndCrossselling(String userid){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"select"+
				" obj_key as \"objKey\"" +
				", agntnum as \"agentNum\"" +
				", agnt_name as \"agentName\"" +
				", chdrnum as \"poNumber\"" +
				", cusname as \"cusName\"" +
				", cussex as \"cusSex\"" +
				", cus_mobile1 as \"cusMobile1\"" +
				", cus_mobile2 as \"cusMobile2\"" +
				", cusmstatus as \"cusMstatus\"" +
				", cusnochd as \"cusNoChild\"" +
				", cus_occu as \"cusOccupation\"" +
				", cussalarymonthly as \"cusSalaryMonthly\"" +
				", rdate as \"issDate\"" +
				", bdmcode as \"bdmCode\"" +
				", bdmname as \"bdmName\"" +
				", branch_code as \"branchCode\"" +
				", ape as \"ape\"" +
				", sa as \"sumAssure\"" +
				", uab as \"uab\"" +
				", chdr_transfer_status as \"poTransferStatus\"" +
				", approach_status as \"approachStatus\"" +
				", approach_status_id as \"approachStatusId\"" +
				", chdr_status as \"chdrStatus\"" +
				", po_cus_age as \"poCusage\"" +
				", income_rank as \"incomeRank\"" +
				", rid as \"rId\""
				+"from " + GlobalVar.db_name+".splist_tabupselling_crossselling(:userid) order by approach_status desc")
				.setParameter("userid", userid)
				.setResultTransformer(Transformers.aliasToBean(UpsellingAndCrosssellingModel.class));
		List<UpsellingAndCrosssellingModel> list = query.list();
		return list;
	}
	
//	@Transactional
//	public UpsellingAndCrosssellingModel getOneApproach(int id){
//		Query query = sessionFactory.getCurrentSession().createQuery("select obj_key from ap.vupselling_crossselling where UpsellingAndCrosssellingModel obj_key = id")
//						.setParameter("id", id);
//		
//		UpsellingAndCrosssellingModel approach = (UpsellingAndCrosssellingModel)query.uniqueResult();
//		
//		return approach;
//	}
	
	
	
	@Transactional
	public UpsellingAndCrosssellingModel getApproachData(String userid, String id) throws Exception {
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"select"+
				" obj_key as \"objKey\"" +
				", agntnum as \"agentNum\"" +
				", agnt_name as \"agentName\"" +
				", chdrnum as \"poNumber\"" +
				", cusname as \"cusName\"" +
				", cussex as \"cusSex\"" +
				", cus_mobile1 as \"cusMobile1\"" +
				", cus_mobile2 as \"cusMobile2\"" +
				", cusmstatus as \"cusMstatus\"" +
				", cusnochd as \"cusNoChild\"" +
				", cus_occu as \"cusOccupation\"" +
				", cussalarymonthly as \"cusSalaryMonthly\"" +
				", rdate as \"issDate\"" +
				", bdmcode as \"bdmCode\"" +
				", bdmname as \"bdmName\"" +
				", branch_code as \"branchCode\"" +
				", ape as \"ape\"" +
				", sa as \"sumAssure\"" +
				", uab as \"uab\"" +
				", chdr_transfer_status as \"poTransferStatus\"" +
				", approach_status as \"approachStatus\"" +
				", approach_status_id as \"approachStatusId\"" +
				", chdr_status as \"chdrStatus\"" +
				", po_cus_age as \"poCusage\"" +
				", income_rank as \"incomeRank\"" +
				", rid as \"rId\""
				+"from " + GlobalVar.db_name+".splist_tabupselling_crossselling(:userid) where obj_key=:id")
				.setParameter("userid", userid)
				.setParameter("id", id)
				.setResultTransformer(Transformers.aliasToBean(UpsellingAndCrosssellingModel.class));
		List<UpsellingAndCrosssellingModel> list = query.list();
		return list.get(0);
	}
}
