package com.pru.pruquote.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pru.pruquote.model.Alteration;
import com.pru.pruquote.utility.GlobalVar;

@Service
public class AlterationService {
	
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Cacheable(value = "listalteration")
	public List<Alteration> getListAlteration(String userid) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select" +
				 		"  obj_key as \"objKey\"" +
						", obj_type as \"objType\"" +
						", chdrnum as \"chdrNum\"" +
						", po_num as \"poNum\"" +
						", po_name as \"poName\"" +
						", alteration_type as \"alterationType\"" +
						", status as \"status\"" +
						", pending_reason as \"pendingReason\" " +
						", request_date as \"requestDate\" " +
						", received_date as \"receivedDate\" " +
						", transaction_date as \"transactionDate\" " +
						", agntnum as \"agntNum\" " +
						", agnt_name as \"agntName\" " +
						"from "	+ GlobalVar.db_name + ".spsearch_alteration(:userid)")
						.setParameter("userid", userid)
						.setResultTransformer(Transformers.aliasToBean(Alteration.class));
		
		List<Alteration> listAlterations = (List<Alteration>)query.list();
		
		return listAlterations;
	}
}