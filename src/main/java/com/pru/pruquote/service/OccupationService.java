package com.pru.pruquote.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.Occupation;
import com.pru.pruquote.model.QuoteOccupation;

@Service
public class OccupationService {
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Occupation> getAllOccupationDdl() {
		Query query = sessionFactory.getCurrentSession().createQuery("FROM Occupation ORDER BY occOrder ASC");
		List<Occupation> listOccupations = query.list();
		return listOccupations;
	}
	
	@Transactional
	public Occupation getOccupationById(int id) {
		Query query = sessionFactory.getCurrentSession().createQuery("FROM Occupation WHERE id = :id")
				.setParameter("id", id);
		Occupation occupation = (Occupation) query.uniqueResult();
		return occupation;
	}
	@Transactional
	public Occupation getOccupationByEngName(String engname) {
		Query query = sessionFactory.getCurrentSession().createQuery("FROM Occupation WHERE engName = :engName")
				.setParameter("engName", engname);
		Occupation occupation = (Occupation) query.uniqueResult();
		return occupation;
	}

	@Transactional
	public void SaveQuoteOccupation(QuoteOccupation QuoteOcc) {		
		sessionFactory.getCurrentSession().saveOrUpdate(QuoteOcc);
	}

	@Transactional
	public QuoteOccupation getQuoteOccupationByQuoteandOccId(long quoteid, int occid) {
		Query query = sessionFactory.getCurrentSession().createQuery("FROM QuoteOccupation WHERE quoteId = :quoteid AND occId =:occid")
				.setParameter("quoteid", quoteid)
				.setParameter("occid", occid);
		QuoteOccupation quoteOcc = (QuoteOccupation) query.uniqueResult();
		return quoteOcc;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<QuoteOccupation> getQuoteOccupationByQuoteId(long quoteid) {
		Query query = sessionFactory.getCurrentSession().createQuery("FROM QuoteOccupation WHERE quoteId = :quoteid")
				.setParameter("quoteid", quoteid);
		List<QuoteOccupation> quoteOcc = (List<QuoteOccupation>) query.list();
		return quoteOcc;
	}

	@Transactional
	public QuoteOccupation getQuoteOccupationByQuoteandRemark(long quoteid, String remark) {
		Query query = sessionFactory.getCurrentSession().createQuery("FROM QuoteOccupation WHERE quoteId = :quoteid AND remark =:remark")
				.setParameter("quoteid", quoteid)
				.setParameter("remark", remark);
		QuoteOccupation quoteOcc = (QuoteOccupation) query.uniqueResult();
		return quoteOcc;
	}
}