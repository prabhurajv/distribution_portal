package com.pru.pruquote.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.Branch;
import com.pru.pruquote.model.Referrer;
import com.pru.pruquote.utility.GlobalVar;

@Service
public class ReferrerService {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	public List<Referrer> listReferrer(String userid, String keyword){
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select rid as id, staffid as \"staffId\"" +
						",staffname as \"staffName\"" +
						",staffBranch as \"staffBranch\"" +
						",staffposition as \"staffPosition\"" +
						",iscertified as \"isCertified\"" +
						",trndate as \"trnDate\"" +
						",status" +
						" from " +	GlobalVar.db_name + ".spsearch_referrer(:userid, :keyword)")
						.setParameter("userid", userid)
						.setParameter("keyword", keyword)
						.setResultTransformer(Transformers.aliasToBean(Referrer.class));
		
		@SuppressWarnings("unchecked")
		List<Referrer> list = (List<Referrer>)query.list();
		
		return list;
	}
	
	@Transactional
	public void referrerSave(Referrer referrer){
		sessionFactory.getCurrentSession().saveOrUpdate(referrer);
	}
	
	@Transactional
	public Referrer getOneReferrer(int id){
		Query query = sessionFactory.getCurrentSession().createQuery("from Referrer where id=:id")
						.setParameter("id", id);
		
		Referrer referrer = (Referrer)query.uniqueResult();
		
		return referrer;
	}
	
	@Transactional
	public List<Branch> listAllBranch(){
		Query query = sessionFactory.getCurrentSession().createQuery("from Branch");
		
		@SuppressWarnings("unchecked")
		List<Branch> branches = (List<Branch>)query.list();
		
		return branches;
	}
	
	@Transactional
	public List<Branch> listSpecificBranch(long id){
		Query query = sessionFactory.getCurrentSession().createQuery("from Branch where id=:id")
						.setParameter("id", id);
		
		@SuppressWarnings("unchecked")
		List<Branch> branches = (List<Branch>)query.list();
		
		return branches;
	}
	
	@Transactional
	public Object[] listPosition(){
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select distinct trim(staffposition) as staffposition from " +
						GlobalVar.db_name + ".tabreferrer");
		
		Object[] positions = query.list().toArray();
		
		return positions;
	}
}
