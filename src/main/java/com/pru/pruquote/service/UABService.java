package com.pru.pruquote.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.ReturningWork;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.PruquoteParm;
import com.pru.pruquote.repository.PruquoteParamRepository;

@Service
public class UABService {
			
	@PersistenceContext
	private EntityManager em;
	 
	@Autowired
	PruquoteParamRepository pruquoteParamRepository;
		
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<PruquoteParm> pruquoteParmsFindAll(){
		return pruquoteParamRepository.findAll();
	}
	public PruquoteParm pruquoteParmsFindOne(long id){
		return  pruquoteParamRepository.findOne(id);
	}
	
	@Transactional
	public void pruQuoteParamSave(PruquoteParm pruquoteParm){
		sessionFactory.getCurrentSession().saveOrUpdate(pruquoteParm);
	}
	@Transactional
	public List<List<Object[]>> generateUAB(final Long quoteId){	
		List<List<Object[]>> res = sessionFactory.getCurrentSession().doReturningWork(new ReturningWork<List<List<Object[]>> /*objectType returned*/>() {
            @Override
            /* or object type you need to return to process*/
            public List<List<Object[]>> execute(Connection conn) throws SQLException 
            {
                PreparedStatement cstmt = conn.prepareStatement("select * from ap.spGen_PruQuoteUAB(?,'R1','R2','R3','R4')"
                		, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
                cstmt.setLong(1, quoteId);
                //Result list that would return ALL rows of ALL result sets
                List<List<Object[]>> results = new ArrayList<List<Object[]>>();
                //String strText="";
                ResultSet resultSet = null;
                ResultSet oDT1=null;
                ResultSet oDT2=null;
                ResultSet oDT3=null;
                ResultSet oDT4=null;
                try
                {
                    resultSet = cstmt.executeQuery();                        
                                                
                    //#region "lblHeader1"
                    resultSet.next();
                    oDT1 = (ResultSet) resultSet.getObject(1);
                    List<Object[]> obj1=new ArrayList<Object[]>();
                    while(oDT1.next()){
                        int cols = oDT1.getMetaData().getColumnCount();
                        Object[] arr = new Object[cols];
                        for(int i=0; i<cols; i++){
                          arr[i] = oDT1.getObject(i+1);
                        }
                        obj1.add(arr);
                    }
                    results.add(obj1);
                    
                    if(resultSet.next()){
                    	oDT2 = (ResultSet) resultSet.getObject(1);
						List<Object[]> obj2=new ArrayList<Object[]>();
						while(oDT2.next()){
						    int cols = oDT2.getMetaData().getColumnCount();
						    Object[] arr = new Object[cols];
						    for(int i=0; i<cols; i++){
						    	arr[i] = oDT2.getObject(i+1);
						    }
						    obj2.add(arr);
						}
						results.add(obj2);
                    }
                    
                    if(resultSet.next()){
                    	oDT3 = (ResultSet) resultSet.getObject(1);
                    	List<Object[]> obj3=new ArrayList<Object[]>();
                        while(oDT3.next()){
                            int cols = oDT3.getMetaData().getColumnCount();
                            Object[] arr = new Object[cols];
                            for(int i=0; i<cols; i++){
                              arr[i] = oDT3.getObject(i+1);
                            }
                            obj3.add(arr);
                        }
                        results.add(obj3);
                    }
                    
                    if(resultSet.next()){
                    	oDT4 = (ResultSet) resultSet.getObject(1);
                    	List<Object[]> obj4=new ArrayList<Object[]>();
                        while(oDT4.next()){
                            int cols = oDT4.getMetaData().getColumnCount();
                            Object[] arr = new Object[cols];
                            for(int i=0; i<cols; i++){
                              arr[i] = oDT4.getObject(i+1);
                            }
                            obj4.add(arr);
                        }
                        results.add(obj4);
                    }
                   //#endregion
                                                                                                   
                                        
                       
                       oDT1.close();
                       oDT2.close();
                       oDT3.close();
                       oDT4.close();
                                               
                }catch(Exception ex){
                	ex.printStackTrace();
                }
                finally
                {
                	resultSet.close();
                	cstmt.close();
                }
                
                return results; // this should contain All rows or objects you need for further processing
            }
        });
		return res;
	}
	
	
	@Transactional
	public List<String> generateEduSaveLatinQuote(final Long quoteId,final String lang,final String result){	
		List<String> res = sessionFactory.getCurrentSession().doReturningWork(new ReturningWork<List<String> /*objectType returned*/>() {
            @Override
            /* or object type you need to return to process*/
            public List<String> execute(Connection conn) throws SQLException 
            {
                PreparedStatement cstmt = conn.prepareCall("select spGen_PruQuote(?,?,?)");
                String policyTerm="";
                String paymentMode="";
                int oDT3Row=0;
                cstmt.setLong(1, quoteId);
                cstmt.setString(2, lang);
                cstmt.setString(3, result);
                //Result list that would return ALL rows of ALL result sets
                List<String> results = new ArrayList<String>();
                String strText="";
                ResultSet oDT1=null;
                ResultSet oDT2=null;
                ResultSet oDT3=null;
                try
                {
                    cstmt.execute();                        
                                                           
                    //#region "lblHeader1"   
                    oDT1 = cstmt.getResultSet();
                    oDT1.first();                   
                    paymentMode=oDT1.getString("PaymentMode2");
                    strText += "";
                    strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>" +
                            "<tr>" +
                               "<th class='Border2' style='text-align:left;'>&nbsp;</th>" +
                               "<th class='Border2' colspan='2'><u>Name</u></th>" +
                               "<th class='Border2'><u>Gender</u></th>" +
                               "<th class='Border2'><u>Age</u></th>" +
                               "<th class='Border2' colspan='3'><u>Premium Mode</u></th>" +
                           "</tr>" +
                           "<tr>" +
                               "<td class='Border2' style='text-align:left;'>Policy Owner's Name:</td>" +
                               "<td class='Border2' colspan=2>" + oDT1.getString("POName") + "</td>" +
                               "<td class='Border2'>" + oDT1.getString("POSex") + "</td>" +
                               "<td class='Border2'>" + oDT1.getString("POAge") + "</td>" +
                               "<td class='Border3' colspan=3 rowspan=3>" /*+ oDT1.getString("PaymentMode2") + "/" */+ oDT1.getString("PaymentType2") + "</td>" +
                           "</tr>" +
                           "<tr>" +
                               "<td class='Border2' style='text-align:left;'>Main Life Assured's Name:</td>" +
                               "<td class='Border2' colspan=2>" + oDT1.getString("LA1Name") + "</td>" +
                               "<td class='Border2'>" + oDT1.getString("LA1Sex2") + "</td>" +
                               "<td class='Border2'>" + oDT1.getString("LA1Age") + "</td>" +
                           "</tr>" +
                          "<tr>     " +
                               "<td class='Border3' style='text-align:left;'>Additional Life Assured's Name:</td>     " +
                               "<td class='Border3' colspan=2>" + oDT1.getString("LA2Name") + "</td>" +
                               "<td class='Border3'>" + oDT1.getString("LA2Sex2") + "</td>" +
                               "<td class='Border3'>" + oDT1.getString("LA2Age") + "</td>" +
                           "</tr>";
                    SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd"); 
                    
                    Date dOBDate;
                    try {
                    	dOBDate=dt.parse(oDT1.getString("LA1DateOfBirth"));
					} catch (Exception e) {
						dOBDate=new Date();
					}
                    
                    String lblSerialNo1 = dt.format(dOBDate).replace("-", "") + "-" + String.format("%06d", quoteId) + "-P1";
                    String lblSerialNo2 = dt.format(dOBDate).replace("-", "") + "-" + String.format("%06d", quoteId) + "-P2";
                    
                    //Particulars of Basic Plan and My Option Benefits:
                    if(cstmt.getMoreResults()){
                    	oDT2 = cstmt.getResultSet();
                    }
                    strText += " <tr > " +
                            "<td class='Border2' style='width:170px;text-align:left;'>Particulars of Basic Plan and Riders:</td>" +
                            "<th class='Border2' style='width:'>Life Assured</th>" +
                            "<th class='Border2' style='width:40px'>Policy Term</th>" +
                            "<th class='Border2' style='width:75px'>Premium Payment Term</th>" +
                            "<th class='Border2' style='width:75px'>Sum Assured (US$)</th>" +
                            "<th class='Border2' style='width:70px'>Monthly (US$)</th>" +
                            "<th class='Border2' style='width:100px'>Semi-Annual (US$)</th>" +
                            "<th class='Border2' style='width:70px'>Annual (US$)</th>" +
                        "</tr>";
                    if (oDT2.first()) 
                    {
                    	
                    	policyTerm=oDT2.getString("PolicyTerm");
                    	
                    	strText += " <tr > " +
                                "<td class='Border3' style='text-align:left;'>&nbsp;&nbsp; " + oDT2.getString("ProductName") + "</td>" +
                                "<td class='Border3' >" + oDT2.getString("Life_Assured") + "</td>" +
                                "<td class='Border3' >" + oDT2.getString("PolicyTerm") + "</td>" +
                                "<td class='Border3' >" + oDT2.getString("PremiumTerm") + "</td>" +
                                "<td class='Border3' >" + String.format("%,.2f",oDT2.getBigDecimal("SA")) + "</td>" +
                                "<td class='Border3' >" + String.format("%,.2f",oDT2.getBigDecimal("P12_Amt")) + "</td>" +
                                "<td class='Border3' >" + String.format("%,.2f",oDT2.getBigDecimal("P02_Amt")) + "</td>" +
                                "<td class='Border3' >" + String.format("%,.2f",oDT2.getBigDecimal("P01_Amt")) + "</td>" +
                        "</tr>";
                    }

                    if(cstmt.getMoreResults()){
                    	oDT3 = cstmt.getResultSet();
                    }
                    
//                  get oDT3 rows count
                    if(oDT3.last())
                    	oDT3Row=oDT3.getRow();
                    
                    if (oDT3.first())
                    {
                	
                        //My Option Benefits (Also known as Riders)   
                    	if(oDT3Row>6)
                    	{
                    		strText += " <tr > " +
                                    "<td class='Border3' style='width:250px;text-align:left;' colspan=8>Riders</td>" +
                           "</tr>";
                    	}
//                        oDT3.beforeFirst(); 
                        while (oDT3.next())
                        {     
                        	if (oDT3.getString("Product").equals("TT"))
                            {
                            	if (oDT3.getString("ProductName").contains("%"))
                                {
                            	strText += "<tr>" +
                                        "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>" + oDT3.getString("ProductName") + "</th>" +
                                        "     <th class='Border2' style='width:'>" + String.format("%,.2f",oDT3.getBigDecimal("P12_Amt")) + "%</th>" +
                                        "     <th class='Border2' style='width:'>" + String.format("%,.2f",oDT3.getBigDecimal("P02_Amt")) + "%</th>" +
                                        "     <th class='Border2' style='width:'>" + String.format("%,.2f",oDT3.getBigDecimal("P01_Amt")) + "%</th>" +
                                        " </tr>";    
                                }
                            	else if (oDT3.isLast())
                                {
                                    String st12 = "";
                                    String st02 = "";
                                    String st01 = "";
                                    if (paymentMode.contains("Monthly"))
                                    {
                                        st12 = "background-color:#bfc8cf;";
                                        st02 = "";
                                        st01 = "";
                                    }
                                    else if (paymentMode.contains("Semi-annually"))
                                    {
                                        st12 = "";
                                        st02 = "background-color:#bfc8cf;";
                                        st01 = "";
                                    }
                                    else
                                    {
                                        st12 = "";
                                        st02 = "";
                                        st01 = "background-color:#bfc8cf;";
                                    }
                                    strText += "<tr>" +
                                               "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>" + oDT3.getString("ProductName") + "</th>" +
                                               "     <th class='Border2' style='width:;" + st12 + "'>" + String.format("%,.2f",oDT3.getBigDecimal("P12_Amt")) + "</th>" +
                                               "     <th class='Border2' style='width:;" + st02 + "'>" + String.format("%,.2f",oDT3.getBigDecimal("P02_Amt")) + "</th>" +
                                               "     <th class='Border2' style='width:;;" + st01 + "'>" + String.format("%,.2f",oDT3.getBigDecimal("P01_Amt")) + "</th>" +
                                               " </tr>";
                                }
                                else
                                {
                                    strText += "<tr>" +
                                               "     <th class='Border2' style='width:250px;text-align:left;' colspan=5>" + oDT3.getString("ProductName") + "</th>" +
                                               "     <th class='Border2' style='width:'>" + String.format("%,.2f",oDT3.getBigDecimal("P12_Amt")) + "</th>" +
                                               "     <th class='Border2' style='width:'>" + String.format("%,.2f",oDT3.getBigDecimal("P02_Amt")) + "</th>" +
                                               "     <th class='Border2' style='width:'>" + String.format("%,.2f",oDT3.getBigDecimal("P01_Amt")) + "</th>" +
                                               " </tr>";
                                }
                            }
                        	else if (oDT3.getString("Product").equals("TH_RTR2")) 
                            { 
                        		strText += " <tr > " +
                                        "<td class='Border3' style='width:170px;text-align:left;'>Additional Riders:</td>" +
                                        "<th class='Border3' style='width:'>Life Assured</th>" +
                                        "<th class='Border3' style='width:40px'>Policy Term</th>" +
                                        "<th class='Border3' style='width:75px'>Premium Payment Term</th>" +
                                        "<th class='Border3' style='width:75px'>Maturity Benifit (US$)</th>" +
                                        "<th class='Border3' style='width:70px'>Monthly (US$)</th>" +
                                        "<th class='Border3' style='width:100px'>Semi-Annual (US$)</th>" +
                                        "<th class='Border3' style='width:70px'>Annual (US$)</th>" +
                                    "</tr>";
                            }
                        	else
                            { 
                        		strText += " <tr > " +
                                        "<td class='Border2' style='width:;text-align:left;'>&nbsp;&nbsp; " + oDT3.getString("ProductName") + "</td>" +
                                        "<td class='Border2' style='width:'>" + oDT3.getString("Life_Assured") + "</td>" +
                                        "<td class='Border2' style='width:'>" + oDT3.getString("PolicyTerm") + "</td>" +
                                        "<td class='Border2' style='width:'>" + oDT3.getString("PremiumTerm") + "</td>" +
                                        "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT3.getBigDecimal("SA")) + "</td>" +
                                        "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT3.getBigDecimal("P12_Amt")) + "</td>" +
                                        "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT3.getBigDecimal("P02_Amt")) + "</td>" +
                                        "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT3.getBigDecimal("P01_Amt")) + "</td>" +
                                "</tr>";
                            }
                        	
                        	
                        }
                        
                        oDT3.beforeFirst();    

                        while (oDT3.next()) 
                        {
                            
                        }

                    }
                    strText += "</table>";
                    //header1
                    results.add(strText);
                    
                   //#endregion
                                                                                                   
                                        
                   // #region "lblBody1- 1. Guaranteed Maturity Benefit with the Basic Policy that can be Used as the Education Fund for the Child"
                    ResultSet oDT4=null;
                    if(cstmt.getMoreResults()){
                    	oDT4 = cstmt.getResultSet();
                    }
                    strText = "";
                    strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>" +
                            "<tr style='background-color:#bfc8cf;'>" +
                                /*"<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>&nbsp;</th>" +*/
                                "<th class='Border2' rowspan=3 style='width:60px;'>Policy Year</th>" +
                                "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>" +
                                "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>Maturity Benefit</th>" +
                                "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>Maturity Benefit with Settlement Option</th>" +
                            "</tr>" +
                            "<tr style='background-color:#bfc8cf;'>" +                           
                            "<th class='Border2' style='width:173px'>Lump Sum Payment</th>" +
                            "<th class='Border2' style='width:173px'>Total</th>" +
                            "<th class='Border2' style='width:173px'>Installment Payment</th>" +
                            "<th class='Border2' style='width:173px'>Total</th>" +
                            "</tr>" +
                            "<tr style='background-color:#bfc8cf;'>" +
                            "<th class='Border2' style='width:'>(US$)</th>" +
                            "<th class='Border2' style='width:'>(US$)</th>" +
                            "<th class='Border2' style='width:'>(US$)</th>" +
                            "<th class='Border2' style='width:'>(US$)</th>" +
                            "</tr>" ;
                    while (oDT4.next()) 
                    {
                    	strText += "<tr>" +
                                "<td class='Border2' style='width:60px;'>" + oDT4.getString("PolicyTerm") + "</td>" +
                                "<td class='Border2' style='width:60px'>" + oDT4.getString("Age") + "</td>" +
                                "<td class='Border2' style='width:'>" + (String.format("%,.2f",oDT4.getBigDecimal("OPT1_LumpSum")).equals("0.00") ? " " :String.format("%,.2f",oDT4.getBigDecimal("OPT1_LumpSum"))) + "</td>" +
                                "<td class='Border2' style='width:'>" + (String.format("%,.2f",oDT4.getBigDecimal("OPT1_TT")).equals("0.00") ? " " : String.format("%,.2f",oDT4.getBigDecimal("OPT1_TT"))) + "</td>" +
                                "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT4.getBigDecimal("OPT2_Inst")) + "</td>" +
                                "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT4.getBigDecimal("OPT1_Inst_TT")) + "</td>" +
                                "</tr>";                     
                    }
                    strText += "</table>";
                    //body1
                    results.add(strText);

               // #endregion

               // #region "lblBody2 -2. Life Insurance Benefit, that Helps the Family Take Care of the Immediate Liabilities"
                    ResultSet oDT5=null;
                    if(cstmt.getMoreResults()){
                    	oDT5 = cstmt.getResultSet();
                    }  
                    strText = "";                    
                    strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>" +
                            "<tr style='background-color:#bfc8cf;'>" +
                              "<th class='Border2' rowspan=3 style='width:60px;'>Policy Year</th>" +
                              "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>" +
                              "<th class='Border2' rowspan=2 style='width:138px;'>Premium every<br/>year</th>" +
                              "<th class='Border2' rowspan=2 style='width:138px;'>Total Premium</th>" +
                              "<th class='Border2' colspan=2 style='width:276px;'>Death/TPD Benefit</th>" +
                              "<th class='Border2' rowspan=2 style='width:138px;'>Guaranteed Cash Value(GCV)</th>" +
                             "</tr>" +
                            "<tr style='background-color:#bfc8cf;'>" +
                              "<th class='Border2' style='width:'>Not Caused by an Accident</th>" +
                              "<th class='Border2' style='width:'>Caused by an Accident</th>" +
                             "</tr>" +
                            "<tr style='background-color:#bfc8cf;'>" +
                              "<th class='Border2' style='width:'>(US$)</th>" +
                              "<th class='Border2' style='width:'>(US$)</th>" +
                              "<th class='Border2' style='width:'>(US$)</th>" +
                              "<th class='Border2' style='width:'>(US$)</th>" +
                              "<th class='Border2' style='width:'>(US$)</th>" +
                          "</tr>";
                    while (oDT5.next()) 
                    {
                    	strText += "<tr>" +
                                "<td class='Border2' style='width:'>" + oDT5.getString("PolicyYear") + "</td>" +
                                "<td class='Border2' style='width:'>" + oDT5.getString("Age") + "</td>" +
                                "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT5.getBigDecimal("P_APE")) + "</td>" +
                                "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT5.getBigDecimal("P_Total")) + "</td>" +
                                "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT5.getBigDecimal("CLAM_Non_Accident")) + "</td>" +
                                "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT5.getBigDecimal("CLAM_Accident")) + "</td>" +
                                "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT5.getBigDecimal("Surrender")) + "</td>" +
                            "</tr>"   ;
                    }
                    strText += "</table>";
                    //body2
                    results.add(strText);
                  //#endregion
                    
                    //#region "lblBody3 -3. Benefits with Additional Riders, that Help the Family in the Further Securing Child's Education and Future"
                    ResultSet oDT6=null;
                    if(cstmt.getMoreResults()){
                    	oDT6 = cstmt.getResultSet();
                    } 
                       strText = "";
                       if (oDT6.first()) 
                       {
                    	   strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>" +
   			                    "<tr style='background-color:#bfc8cf;'>" +
                                       "<th class='Border2' style='width:120px;text-align:center;' colspan=1 rowspan=3>Rider Benefits</th>" +
                                       "<th class='Border2' style='width:' rowspan=3>Life Assured Name</th>" +
                                       "<th class='Border2' style='width:200px;' colspan=2>Death/TPD Benefit</th>" +
                                       "<th class='Border2' style='width:350px;' rowspan=3>Benefit Payment Terms</th>" +
   			                    "</tr>" +
   			                    "<tr style='background-color:#bfc8cf;'>" +
                                       "<th class='Border2'>Not Caused by an Accident</th>" +
                                       "<th class='Border2'>Caused by an Accident</th>" +
   			                    "</tr>" +
   			                    "<tr style='background-color:#bfc8cf;'>" +
                                       "<th class='Border2'>(US$)</th>" +
                                       "<th class='Border2'>(US$)</th>" +
   			                    "</tr>" ;
                    	   oDT6.beforeFirst();
                           while (oDT6.next()) 
                           {

                        	   strText += "<tr>" +
                                       "<td class='Border2' colspan=1 >" + oDT6.getString("ProductName") + "</td>" +
                                       "<td class='Border2' colspan=1 >" + oDT6.getString("Life_Assured") + "</td>" +
                                       "<td class='Border2' colspan=1 width=90>" + String.format("%,.2f",oDT6.getBigDecimal("CLAM_Non_Accident")) + "</td>" +
                                       "<td class='Border2' colspan=1 width=90>" + String.format("%,.2f",oDT6.getBigDecimal("CLAM_Accident")) + "</td>" +
                                       "<td class='Border2' style='width:350px' colspan=1>" + oDT6.getString("Payment_Terms") + "</td>" +
		                            "</tr>" ;                          
                           }
                           strText += "</table>";
                       }
                       //body3
                       results.add(strText);
                  //#endregion   
                    
                      // #region "Footer1"
                       strText = "";
                       //Life Insurance Benefit
                       if (policyTerm.contains("10"))
                       {
                    	   strText += "<table>";
                           strText += "<tr><td class='' style='height:90px'>&nbsp;</td></tr>";
                           strText += "</table>";
                       }
                       else if (policyTerm.contains("12"))
                       {
                    	   strText += "<table>";
                           strText += "<tr><td class='' style='height:60px'>&nbsp;</td></tr>";
                           strText += "</table>";
                       }
                       
                       //Riders
                       if (oDT3Row == 9)
                       {
                    	   strText += "<table>";
                           strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
                           strText += "</table>";
                       }
                       else if (oDT3Row == 8)
                       {
                    	   strText += "<table>";
                           strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
                           strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
                           strText += "</table>";
                       }
                       else if (oDT3Row == 7)
                       {
                           strText += "<table>";
                           strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
                           strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
                           strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
                           strText += "</table>";
                       }
                       else if (oDT3Row == 6)
                       {
                           strText += "<table>";
                           strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
                           strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
                           strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
                           strText += "<tr><td class='' style='height:'>&nbsp;</td></tr>";
                           strText += "</table>";
                       }
                       results.add(strText);
                    //   #endregion

                 // 
                       results.add(lblSerialNo1);
                       results.add(lblSerialNo2);
                       //#endregion
                       
                    // #region "lblBody4- 4. Guaranteed Maturity Benefit with the PruSaver"
                       ResultSet oDT7=null;
                       if(cstmt.getMoreResults()){
                    	   oDT7 = cstmt.getResultSet();
                    	   if (oDT7.first()) 
                           {
    	                       strText = "";
    	                       strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>" +
    	                               "<tr style='background-color:#bfc8cf;'>" +
    	                                   /*"<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>&nbsp;</th>" +*/
    	                                   "<th class='Border2' rowspan=3 style='width:60px;'>Rider Year</th>" +
    	                                   "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>" +
    	                                   "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>Maturity Benefit</th>" +
    	                                   "<th class='Border2' style='text-align:center;vertical-align:middle;' colspan=2>Maturity Benefit with Settlement Option</th>" +
    	                               "</tr>" +
    	                               "<tr style='background-color:#bfc8cf;'>" +                           
    	                               "<th class='Border2' style='width:173px'>Lump Sum Payment</th>" +
    	                               "<th class='Border2' style='width:173px'>Total</th>" +
    	                               "<th class='Border2' style='width:173px'>Installment Payment</th>" +
    	                               "<th class='Border2' style='width:173px'>Total</th>" +
    	                               "</tr>" +
    	                               "<tr style='background-color:#bfc8cf;'>" +
    	                               "<th class='Border2' style='width:'>(US$)</th>" +
    	                               "<th class='Border2' style='width:'>(US$)</th>" +
    	                               "<th class='Border2' style='width:'>(US$)</th>" +
    	                               "<th class='Border2' style='width:'>(US$)</th>" +
    	                               "</tr>" ;
    	                       oDT7.beforeFirst();
    	                       while (oDT7.next()) 
    	                       {
    	                       	strText += "<tr>" +
    	                                   "<td class='Border2' style='width:60px;'>" + oDT7.getString("PolicyTerm") + "</td>" +
    	                                   "<td class='Border2' style='width:60px'>" + oDT7.getString("Age") + "</td>" +
    	                                   "<td class='Border2' style='width:'>" + (String.format("%,.2f",oDT7.getBigDecimal("OPT1_LumpSum")).equals("0.00") ? " " :String.format("%,.2f",oDT7.getBigDecimal("OPT1_LumpSum"))) + "</td>" +
    	                                   "<td class='Border2' style='width:'>" + (String.format("%,.2f",oDT7.getBigDecimal("OPT1_TT")).equals("0.00") ? " " : String.format("%,.2f",oDT7.getBigDecimal("OPT1_TT"))) + "</td>" +
    	                                   "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT7.getBigDecimal("OPT2_Inst")) + "</td>" +
    	                                   "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT7.getBigDecimal("OPT1_Inst_TT")) + "</td>" +
    	                                   "</tr>";                     
    	                       }
    	                       strText += "</table>";
    	                       //body1
    	                       results.add(strText);
                           }
                       }else{
                    	   results.add("");
                       }
                       
                       // #endregion
                       
                    // #region "lblBody5 -5. Life Insurance Benefit with PruSaver"
                       ResultSet oDT8=null;
                       if(cstmt.getMoreResults()){
                    	   oDT8 = cstmt.getResultSet();
                    	   if (oDT8.first()) 
                           {
    	                       strText = "";                    
    	                       strText += "<table style='width:100%;background-color:white;border-collapse:collapse; border:1px solid black;' border='0'>" +
    	                               "<tr style='background-color:#bfc8cf;'>" +
    	                                 "<th class='Border2' rowspan=3 style='width:60px;'>Rider Year</th>" +
    	                                 "<th class='Border2' rowspan=3 style='width:60px;'>Age</th>" +
    	                                 "<th class='Border2' rowspan=2 style='width:138px;'>Rider premium<br/>every year</th>" +
    	                                 "<th class='Border2' rowspan=2 style='width:138px;'>Total Rider Premium</th>" +
    	                                 "<th class='Border2' colspan=2 style='width:276px;'>Death/TPD Benefit</th>" +
    	                                 "<th class='Border2' rowspan=2 style='width:138px;'>Guaranteed Cash Value(GCV)</th>" +
    	                                "</tr>" +
    	                               "<tr style='background-color:#bfc8cf;'>" +
    	                                 "<th class='Border2' style='width:'>Not Caused by an Accident</th>" +
    	                                 "<th class='Border2' style='width:'>Caused by an Accident</th>" +
    	                                "</tr>" +
    	                               "<tr style='background-color:#bfc8cf;'>" +
    	                                 "<th class='Border2' style='width:'>(US$)</th>" +
    	                                 "<th class='Border2' style='width:'>(US$)</th>" +
    	                                 "<th class='Border2' style='width:'>(US$)</th>" +
    	                                 "<th class='Border2' style='width:'>(US$)</th>" +
    	                                 "<th class='Border2' style='width:'>(US$)</th>" +
    	                             "</tr>";
    	                       oDT8.beforeFirst();
    	                       while (oDT8.next()) 
    	                       {
    	                       	strText += "<tr>" +
    	                                   "<td class='Border2' style='width:'>" + oDT8.getString("PolicyYear") + "</td>" +
    	                                   "<td class='Border2' style='width:'>" + oDT8.getString("Age") + "</td>" +
    	                                   "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT8.getBigDecimal("P_APE")) + "</td>" +
    	                                   "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT8.getBigDecimal("P_Total")) + "</td>" +
    	                                   "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT8.getBigDecimal("CLAM_Non_Accident")) + "</td>" +
    	                                   "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT8.getBigDecimal("CLAM_Accident")) + "</td>" +
    	                                   "<td class='Border2' style='width:'>" + String.format("%,.2f",oDT8.getBigDecimal("Surrender")) + "</td>" +
    	                               "</tr>"   ;
    	                       }
    	                       strText += "</table>";
    	                       //body2
    	                       results.add(strText);
    	                       oDT7.close();
    	                       oDT8.close();
                           }
                       }else{
                    	   results.add("");
                       }  
                       
                     //#endregion
                       
                       oDT1.close();
                       oDT2.close();
                       oDT3.close();
                       oDT4.close();
                       oDT5.close();
                       oDT6.close();
                       
                                               
                }
                finally
                {
                	cstmt.close();
                }
                
                return results; // this should contain All rows or objects you need for further processing
            }
        });
		return res;
	}
	
			
	public List<PruquoteParm> findValidQuote(Long id,String product,String user){
		return pruquoteParamRepository.findByIdAndProductAndCoUserAndValidFlag(id, product, user,1);
	}
	public PruquoteParm getValidQuote(Long id,String product,String user){
		List<PruquoteParm> pruquoteParms=pruquoteParamRepository.findByIdAndProductAndCoUserAndValidFlag(id, product, user,1);
		if(pruquoteParms.size()>0)
			return pruquoteParamRepository.findByIdAndProductAndCoUserAndValidFlag(id, product, user,1).get(0);
		else {
			return null;
		}
	}
	@Transactional
	public List<Object> prusaverPremiumValidate(Long quoteId){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"select * from spGen_PruQuote_SaverValidat(:quoteId,'En')");
		query.setParameter("quoteId",quoteId);				
		@SuppressWarnings("unchecked")
		List<Object> results=query.list();
		return results;
	}
}