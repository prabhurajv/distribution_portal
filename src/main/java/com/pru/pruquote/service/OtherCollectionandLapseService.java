package com.pru.pruquote.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.OtherCollectionAndLapse;
import com.pru.pruquote.utility.GlobalVar;

@Service
public class OtherCollectionandLapseService {
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<OtherCollectionAndLapse> getListOtherCollectionandLapse(String poappnum, String dob) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select" +
				 		"  obj_key as \"objKey\"" +
						", obj_type as \"objType\"" +
						", chdrnum as \"chdrNum\"" +
						", chdr_appnum as \"chdrAppnum\"" +
						", po_name as \"poName\"" +
						", gender as \"gender\"" +
						", clnt_mobile1 as \"clntMobile1\"" +
						", clnt_mobile2 as \"clntMobile2\" " +
						", chdr_p_with_tax as \"chdrPwithTax\" " +
						", chdr_billfreq as \"chdrBillfreq\" " +
						", chdr_due_date as \"chdrDueDate\" " +
						", chdr_graceperiod as \"chdrGraceperiod\" " +
						", agntnum as \"agntNum\" " +
						", agnt_name as \"agntName\" " +
						", agnt_supcode as \"agntSupcode\" " +
						", agnt_supname as \"agntSupname\" " +
						", chdr_age as \"chdrAge\" " +
						", cc_status as \"ccStatus\" " +
						", agnt_status as \"agntStatus\" " +
						", chdr_ape as \"chdrApe\" " +
						", chdr_lapse_date as \"chdrLapseDate\" " +
						", chdr_status as \"chdrStatus\" " +
						", is_impact_persistency as \"isImpactPersistency\" " +
						", lapsed_reason as \"lapsedReason\" " +
						", bcu_remark as \"bcuRemark\" " +
						", customer_response as \"customerResponse\" " +
						", branch_code as \"branchCode\" " +
						", branch_name as \"branchName\" " +
						", cc_remark as \"ccRemark\" " +
						", bdmcode " +
						", bdmname " +
						", sbdmcode " +
						", sbdmname " +
						", rbdmcode " +
						", rbdmname " +
						", salezone " +
						", product " +
						", sub_lapsed_reason " +
						", occ_date " +
						", next_prem as \"next_prem\"" +
						", tele_reinstate as \"tele_reinstate\"" +
						", out_amt as \"out_amt\"" +
						", payment_method " +
						", clnt_mobile3 " +
						", next_payment_date as \"next_payment_date\"" +
						", stnd_ord_end_date as \"stnd_ord_end_date\"" +
						", bank_acc " +
						", so_cancel_status as \"so_cancel_status\"" +
						", ben " +
						", dob as \"dob\"" +
						"from "	+ GlobalVar.db_name + ".spsearch_othercollection(:poappnum, :dob)")
						.setParameter("poappnum", poappnum)
						.setParameter("dob", dob)
						.setResultTransformer(Transformers.aliasToBean(OtherCollectionAndLapse.class));
		
		List<OtherCollectionAndLapse> listCollectionAndLapses = (List<OtherCollectionAndLapse>)query.list();
		return listCollectionAndLapses;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<OtherCollectionAndLapse> getCollectionandLapse(String objKey) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select" +
				 		"  obj_key as \"objKey\"" +
						", obj_type as \"objType\"" +
						", chdrnum as \"chdrNum\"" +
						", chdr_appnum as \"chdrAppnum\"" +
						", po_name as \"poName\"" +
						", gender as \"gender\"" +
						", clnt_mobile1 as \"clntMobile1\"" +
						", clnt_mobile2 as \"clntMobile2\" " +
						", chdr_p_with_tax as \"chdrPwithTax\" " +
						", chdr_billfreq as \"chdrBillfreq\" " +
						", chdr_due_date as \"chdrDueDate\" " +
						", chdr_graceperiod as \"chdrGraceperiod\" " +
						", agntnum as \"agntNum\" " +
						", agnt_name as \"agntName\" " +
						", agnt_supcode as \"agntSupcode\" " +
						", agnt_supname as \"agntSupname\" " +
						", chdr_age as \"chdrAge\" " +
						", cc_status as \"ccStatus\" " +
						", agnt_status as \"agntStatus\" " +
						", chdr_ape as \"chdrApe\" " +
						", chdr_lapse_date as \"chdrLapseDate\" " +
						", chdr_status as \"chdrStatus\" " +
						", is_impact_persistency as \"isImpactPersistency\" " +
						", lapsed_reason as \"lapsedReason\" " +
						", bcu_remark as \"bcuRemark\" " +
						", customer_response as \"customerResponse\" " +
						", branch_code as \"branchCode\" " +
						", branch_name as \"branchName\" " +
						", cc_remark as \"ccRemark\" " +
						", bdmcode " +
						", bdmname " +
						", sbdmcode " +
						", sbdmname " +
						", rbdmcode " +
						", rbdmname " +
						", salezone " +
						", product " +
						", sub_lapsed_reason " +
						", occ_date " +
						", next_prem as \"next_prem\"" +
						", tele_reinstate as \"tele_reinstate\"" +
						", out_amt as \"out_amt\"" +
						", payment_method " +
						", clnt_mobile3 " +
						", next_payment_date as \"next_payment_date\"" +
						", stnd_ord_end_date as \"stnd_ord_end_date\"" +
						", bank_acc " +
						", so_cancel_status as \"so_cancel_status\"" +
						", ben " +
						", dob as \"dob\"" +
						"from "	+ GlobalVar.db_name + ".spsearch_othercollectionbyobjkey(:objKey)")
						.setParameter("objKey", objKey)
						.setResultTransformer(Transformers.aliasToBean(OtherCollectionAndLapse.class));
		
		List<OtherCollectionAndLapse> listCollectionAndLapses = (List<OtherCollectionAndLapse>)query.list();
		return listCollectionAndLapses;
	}
}