package com.pru.pruquote.service;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.pru.pruquote.model.AcbProduct;
import com.pru.pruquote.model.AcbProductComment;
import com.pru.pruquote.model.PruquoteParm;
import com.pru.pruquote.model.VpruquoteParm;
import com.pru.pruquote.repository.PruquoteParamRepository;
import com.pru.pruquote.utility.GlobalVar;
import com.pru.pruquote.model.Referral;
import com.pru.pruquote.model.ReferralAcbProduct;
import com.pru.pruquote.model.ReferralCustomerFrom;
import com.pru.pruquote.model.ReferralFollowUpComment;
import com.pru.pruquote.model.ReferralList;
import com.pru.pruquote.model.Referrer;
import com.pru.pruquote.model.UpsellingAndCrosssellingReferral;
 

@Service
public class ReferralService {
			
	@PersistenceContext
	private EntityManager em;
	 
	@Autowired
	PruquoteParamRepository pruquoteParamRepository;
		
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<PruquoteParm> pruquoteParmsFindAll(){
		return pruquoteParamRepository.findAll();
	}
	public PruquoteParm pruquoteParmsFindOne(long id){
		return  pruquoteParamRepository.findOne(id);
	}
	
	@ModelAttribute("referral")
    public Referral referralConstructor(){
    	return new Referral();
    }
	
	
	@Transactional
	public List<Object> listReferral(String user,String ID){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"select * from "+GlobalVar.db_name+".spList_Referral (:user,:id)")
				.setParameter("user",user)
				.setParameter("id",ID);
		@SuppressWarnings("unchecked")
		List<Object> listReferrals=query.list();
		return listReferrals;
	}
	
	@Transactional
	public List<Object> listReferralType(){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT id ,reftype FROM "+GlobalVar.db_name+".tabreferral_type where validflag='1'");
		@SuppressWarnings("unchecked")
		List<Object> listReferralTypes=query.list();
		return listReferralTypes;
	}
	
	@Transactional
	public List<Object> listAllReferralType(String type){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT id ,reftype FROM "+GlobalVar.db_name+".tabreferral_type where validflag='1' AND reserve like '%"+ type +"%' order by reftype desc");
		@SuppressWarnings("unchecked")
		List<Object> listReferralTypes=query.list();
		return listReferralTypes;
	}
	
	@Transactional
	public List<Object> listReferralCommentType(){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT cid ,cdesc  FROM "+GlobalVar.db_name+".vreferral_comment_type where validflag='1'");
		@SuppressWarnings("unchecked")
		List<Object> listReferralCommentTypes=query.list();
		return listReferralCommentTypes;
	}
	
	@Transactional
	public List<Object> listReferralFollowup(String rid){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT *  FROM "+GlobalVar.db_name+".splist_referral_followup(:rid)")
				.setParameter("rid",rid);
		@SuppressWarnings("unchecked")
		List<Object> listReferralFollowup=query.list();
		return listReferralFollowup;
	}
	
	@Transactional
	public List<Object> listReferrer(String userID){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT referrer_id, referrer_name FROM "+GlobalVar.db_name+".splist_referrer(:user)")
				.setParameter("user", userID);
		@SuppressWarnings("unchecked")
		List<Object> listReferrer=query.list();
		return listReferrer;
	}
	
	@Transactional
	public List<Object> listCertifiedReferrer(String userID){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT referrer_id, referrer_name FROM "+GlobalVar.db_name+".splist_referrer(:user) WHERE iscertified = 1")
				.setParameter("user", userID);
		@SuppressWarnings("unchecked")
		List<Object> listReferrer=query.list();
		return listReferrer;
	}
	
	@Transactional
	public List<Object> listReferrer(String userID, String branchCode){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT referrer_id, referrer_name FROM "+GlobalVar.db_name+".splist_referrer(:user, :branchcode)")
				.setParameter("user", userID)
				.setParameter("branchcode", branchCode);
		@SuppressWarnings("unchecked")
		List<Object> listCusOccupation=query.list();
		return listCusOccupation;
	}
	
	@Transactional
	public List<Object> listCertifiedReferrer(String userID, String branchCode){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT referrer_id, referrer_name FROM "+GlobalVar.db_name+".splist_referrer(:user, :branchcode) WHERE iscertified = 1")
				.setParameter("user", userID)
				.setParameter("branchcode", branchCode);
		@SuppressWarnings("unchecked")
		List<Object> listCusOccupation=query.list();
		return listCusOccupation;
	}
	
	@Transactional
	public List<Object> ReferrerMaxID(String userID){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT * FROM "+GlobalVar.db_name+".sptabreferal_maxid(:user)")
				.setParameter("user", userID);
		@SuppressWarnings("unchecked")
		List<Object> ReferrerMaxID=query.list();
		return ReferrerMaxID;
	}
	
	@Transactional
	public List<ArrayList<Object>> listCusOccupation(){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT *  FROM "+GlobalVar.db_name+".tabcus_occupation where validflag=1");
		@SuppressWarnings("unchecked")
		List<ArrayList<Object>> listReferrer=query.list();
		return listReferrer;
	}
	
	@Transactional
	public List<Object> listProduct(){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT productcode, productname FROM "+GlobalVar.db_name+".vproduct_type");
		@SuppressWarnings("unchecked")
		List<Object> listProduct=query.list();
		return listProduct;
	}
	
	@Transactional
	public List<Object> getProduct(String productCode){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT productcode, productname FROM "+GlobalVar.db_name+".vproduct_type WHERE productcode = :productcode")
				.setParameter("productcode", productCode);
		@SuppressWarnings("unchecked")
		List<Object> listProduct=query.list();
		return listProduct;
	}
	
	@Transactional
	public List<Object> listPolicyTerm(){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT fielddisplay, fieldvalue FROM "+GlobalVar.db_name+".vpolicyterm");
		@SuppressWarnings("unchecked")
		List<Object> listPolicyTerm=query.list();
		return listPolicyTerm;
	}
	
	@Transactional
	public List<Object> listPolicyMode(){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT * FROM "+GlobalVar.db_name+".vpaymentmode");
		@SuppressWarnings("unchecked")
		List<Object> listPolicyMode=query.list();
		return listPolicyMode;
	}
	
	@Transactional
	public void referralSave(Referral Referral){
		sessionFactory.getCurrentSession().saveOrUpdate(Referral);
	}
	
	@Transactional
	public void IDU_Referral(String rid
			,String trndatetime
			,String cusname,String cusphone
			,String rtype,String rdate
			,String sucappstatus,String sucappdate
			,String sucsalestatus,String sucsaledate
			,String ape,String exiscusstatus
			,String validflag,String agntnum,String baction  )
	{
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"select ap.spIDU_Referral(:rid,:trndatetime,:cusname,:cusphone,:rtype,:rdate,:sucappstatus,:sucappdate,:sucsalestatus,:sucsaledate,:ape,:exiscusstatus,:validflag,:agntnum,:baction)")
				.setParameter("rid",rid)
				.setParameter("trndatetime",trndatetime)
				.setParameter("cusname",cusname)
				.setParameter("cusphone",cusphone)
				.setParameter("rtype",rtype)
				.setParameter("rdate",rdate)
				.setParameter("sucappstatus",sucappstatus)
				.setParameter("sucappdate",sucappdate)
				.setParameter("sucsalestatus",sucsalestatus)
				.setParameter("sucsaledate",sucsaledate)
				.setParameter("ape",ape)
				.setParameter("exiscusstatus",exiscusstatus)
				.setParameter("validflag",validflag)
				.setParameter("agntnum",agntnum)
				.setParameter("baction",baction);
		query.executeUpdate();
	}
	
	@Transactional
	public List<ReferralList> listReferralByAgent(String userid,String trndatetimefrom,String trndatetimeto,String followupdatetimefrom,String followupdatetimeto,String cusinfor,String rtype
			,String sucappstatus, String clsbanksostatus,String clsbankiploadstatus,String exiscusstatus,String referralby
			){
		Query query=sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM " + GlobalVar.db_name + ".spsearch_referral_byagent(:userid,:trndatetimefrom,:trndatetimeto,:followupdatetimefrom,:followupdatetimeto,:cusinfor,:rtype,:sucappstatus,:clsbanksostatus,:clsbankiploadstatus,:exiscusstatus,:referralby)")
				.setParameter("userid",userid)
				.setParameter("trndatetimefrom", trndatetimefrom)
				.setParameter("trndatetimeto", trndatetimeto)
				.setParameter("followupdatetimefrom", followupdatetimefrom)
				.setParameter("followupdatetimeto", followupdatetimeto)
				.setParameter("cusinfor", cusinfor)
				.setParameter("rtype", rtype)
				.setParameter("sucappstatus", sucappstatus)
				.setParameter("clsbanksostatus", clsbanksostatus)
				.setParameter("clsbankiploadstatus", clsbankiploadstatus)
				.setParameter("exiscusstatus", exiscusstatus)
				.setParameter("referralby", referralby);
		@SuppressWarnings("unchecked")
		List<ReferralList> listReferrals=query.list();
		
		return listReferrals;
	}
	
	/** This is version 1 of search referral by manager level */
	@Transactional
	public List<ReferralList> listReferralByAgentMgt(String userid, String agentHierarchy, String trndatetimefrom,String trndatetimeto,String followupdatetimefrom,String followupdatetimeto,String cusinfor,String rtype
			,String sucappstatus, String clsbanksostatus,String clsbankiploadstatus,String exiscusstatus,String referralby
			){
		Query query=sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM " + GlobalVar.db_name + ".spsearch_referral_byagentmgt(:userid" +
				",:agentHierarchy,:trndatetimefrom,:trndatetimeto,:followupdatetimefrom,:followupdatetimeto,:cusinfor,:rtype,:sucappstatus,:clsbanksostatus" +
				",:clsbankiploadstatus,:exiscusstatus,:referralby)")
				.setParameter("userid",userid)
				.setParameter("agentHierarchy", agentHierarchy)
				.setParameter("trndatetimefrom", trndatetimefrom)
				.setParameter("trndatetimeto", trndatetimeto)
				.setParameter("followupdatetimefrom", followupdatetimefrom)
				.setParameter("followupdatetimeto", followupdatetimeto)
				.setParameter("cusinfor", cusinfor)
				.setParameter("rtype", rtype)
				.setParameter("sucappstatus", sucappstatus)
				.setParameter("clsbanksostatus", clsbanksostatus)
				.setParameter("clsbankiploadstatus", clsbankiploadstatus)
				.setParameter("exiscusstatus", exiscusstatus)
				.setParameter("referralby", referralby);
		@SuppressWarnings("unchecked")
		List<ReferralList> listReferrals=query.list();
		
		return listReferrals;
	}
	
	/** This is version 2 (add 6 more arguments) of search referral by manager level */
	@Transactional
	public List<ReferralList> listReferralByAgentMgt(String userid, String agentHierarchy, String trndatetimefrom
			,String trndatetimeto,String followupdatetimefrom,String followupdatetimeto,String cusinfor,String rtype
			,String sucappstatus, String clsbanksostatus,String clsbankiploadstatus,String exiscusstatus,String referralby
			,String rbdm, String sbdm, String bdm, String branch, String sup, String agntNum){
		Query query=sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM " + GlobalVar.db_name + ".spsearch_referral_byagentmgtv2(:userid" +
				",:agentHierarchy,:trndatetimefrom,:trndatetimeto,:followupdatetimefrom,:followupdatetimeto,:cusinfor,:rtype,:sucappstatus,:clsbanksostatus" +
				",:clsbankiploadstatus,:exiscusstatus,:referralby,:rbdm,:sbdm,:bdm,:branch,:sup,:agntNum)")
				.setParameter("userid",userid)
				.setParameter("agentHierarchy", agentHierarchy)
				.setParameter("trndatetimefrom", trndatetimefrom)
				.setParameter("trndatetimeto", trndatetimeto)
				.setParameter("followupdatetimefrom", followupdatetimefrom)
				.setParameter("followupdatetimeto", followupdatetimeto)
				.setParameter("cusinfor", cusinfor)
				.setParameter("rtype", rtype)
				.setParameter("sucappstatus", sucappstatus)
				.setParameter("clsbanksostatus", clsbanksostatus)
				.setParameter("clsbankiploadstatus", clsbankiploadstatus)
				.setParameter("exiscusstatus", exiscusstatus)
				.setParameter("referralby", referralby)
				.setParameter("rbdm", rbdm)
				.setParameter("sbdm", sbdm)
				.setParameter("bdm", bdm)
				.setParameter("branch", branch)
				.setParameter("sup", sup)
				.setParameter("agntNum", agntNum);
		@SuppressWarnings("unchecked")
		List<ReferralList> listReferrals=query.list();
		
		return listReferrals;
	}
	
	@Transactional
	public List<ReferralCustomerFrom> listCustomerFrom(){
		Query query = sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM " + GlobalVar.db_name + ".tabreferal_customerfrom");
		
		@SuppressWarnings("unchecked")
		List<ReferralCustomerFrom> list = query.list();
		
		return list;
	}
	
	@Transactional
	public List<Object[]> listHighLevelHK(String position){
		Query query = sessionFactory.getCurrentSession().createSQLQuery("SELECT trim(lascode) as lascode, trim(fullname) as fullname FROM "
						+ GlobalVar.db_name + ".tabagnt_mgt_infor WHERE trim(position)=:position")
				.setParameter("position", position);
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = query.list();
		
		return list;
	}
	
	@Transactional
	public List<Object[]> listSupervisorByBDM(String bdm, String position){
		Query query = sessionFactory.getCurrentSession().createSQLQuery("SELECT DISTINCT trim(lascode) as lascode, trim(fullname) as fullname FROM "
						+ GlobalVar.db_name + ".tabagnt_mgt_infor inner join " + GlobalVar.db_name + ".tabagnt_fc_hk on" 
						+ " trim(lascode)=trim(agntsup) WHERE (CASE WHEN :bdm='S09' THEN agntrbdm = :bdm ELSE agntbdm =:bdm END) AND agntsup != '' AND position=:position")
				.setParameter("bdm", bdm)
				.setParameter("position", position);
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = query.list();
		
		return list;
	}
	
	@Transactional
	public List<Object[]> listAgentByBranch(String branch){
		Query query = sessionFactory.getCurrentSession().createSQLQuery("SELECT trim(agntnum) as agntnum, trim(agnt_name) as agntname FROM "
						+ GlobalVar.db_name + ".tabagnt_fc_hk WHERE agnt_branch=:branch ORDER BY agnt_name")
				.setParameter("branch", branch);
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = query.list();
		
		return list;
	}
	
	@Transactional
	public List<Object[]> listBranchByBDM(String lascode){
		Query query = sessionFactory.getCurrentSession().createSQLQuery("SELECT DISTINCT trim(agnt_branch) as branch, trim(agnt_branch) as branchdesc FROM "
						+ GlobalVar.db_name + ".tabagnt_fc_hk WHERE agntbdm=:lascode OR agntrbdm=:lascode ORDER BY branch")
				.setParameter("lascode", lascode);
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = query.list();
		
		return list;
	}
	
	@Transactional
	public List<Object[]> listDynamic(String sql){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(sql);
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = query.list();
		
		return list;
	}
	
	@Transactional
	public List<List<Object[]>> listSearchReferralMgtParam(String userid, String rbdm, String sbdm, String bdm, String branch
			, String agntsup, String agntnum){
		List<List<Object[]>> list = null;
//		Session session = sessionFactory.openSession();
		list = sessionFactory.getCurrentSession().doReturningWork(new ReturningWork<List<List<Object[]>>>(){

			@Override
			public List<List<Object[]>> execute(Connection conn) throws SQLException {
				conn.setAutoCommit(false);
				
				PreparedStatement cstmt = conn.prepareStatement("select * from " + GlobalVar.db_name + ".splist_referral_mgt_param(?,?,?,?,?,?,?,'RN','R1','R2','R3','R4','R5','R6')"
                		, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
				cstmt.setString(1, userid);
				cstmt.setString(2, rbdm);
				cstmt.setString(3, sbdm);
				cstmt.setString(4, bdm);
				cstmt.setString(5, branch);
				cstmt.setString(6, agntsup);
				cstmt.setString(7, agntnum);
				
				ResultSet result = null;
				ResultSet result1 = null;
				ResultSet result2 = null;
				ResultSet result3 = null;
				ResultSet result4 = null;
				ResultSet result5 = null;
				ResultSet result6 = null;
				
				List<List<Object[]>> listTemp = new ArrayList<List<Object[]>>();
				
				try{
					result = cstmt.executeQuery();
					listTemp = new ArrayList<List<Object[]>>();
					
					// Get list of RBDM
					if(result.next()){
						result1 = (ResultSet)result.getObject(1);
						
						List<Object[]> list1 = new ArrayList<Object[]>();
						while(result1.next()){
							Object[] obj = new Object[2];
							obj[0] = result1.getString("lascode");
							obj[1] = result1.getString("fullname");
							
							list1.add(obj);
						}
						
						listTemp.add(list1);
					}
					
					// Get list of SBDM
					if(result.next()){
						result2 = (ResultSet)result.getObject(1);
						
						List<Object[]> list2 = new ArrayList<Object[]>();
						while(result2.next()){
							Object[] obj = new Object[2];
							obj[0] = result2.getString("lascode");
							obj[1] = result2.getString("fullname");
							
							list2.add(obj);
						}
						
						listTemp.add(list2);
					}
					
					// Get list of BDM
					if(result.next()){
						result3 = (ResultSet)result.getObject(1);
						
						List<Object[]> list3 = new ArrayList<Object[]>();
						while(result3.next()){
							Object[] obj = new Object[2];
							obj[0] = result3.getString("lascode");
							obj[1] = result3.getString("fullname");
							
							list3.add(obj);
						}
						
						listTemp.add(list3);
					}
					
					// Get list of branch
					if(result.next()){
						result4 = (ResultSet)result.getObject(1);
						
						List<Object[]> list4 = new ArrayList<Object[]>();
						while(result4.next()){
							Object[] obj = new Object[2];
							obj[0] = result4.getString("branch");
							obj[1] = result4.getString("branchDesc");
							
							list4.add(obj);
						}
						
						listTemp.add(list4);
					}
					
					// Get list of agent supervisor
					if(result.next()){
						result5 = (ResultSet)result.getObject(1);
						
						List<Object[]> list5 = new ArrayList<Object[]>();
						while(result5.next()){
							Object[] obj = new Object[2];
							obj[0] = result5.getString("lascode");
							obj[1] = result5.getString("fullname");
							
							list5.add(obj);
						}
						
						listTemp.add(list5);
					}
					
					// Get list of agent
					if(result.next()){
						result6 = (ResultSet)result.getObject(1);
						
						List<Object[]> list6 = new ArrayList<Object[]>();
						while(result6.next()){
							Object[] obj = new Object[2];
							obj[0] = result6.getString("agntnum");
							obj[1] = result6.getString("agntname");
							
							list6.add(obj);
						}
						
						listTemp.add(list6);
					}
					
					result.close();
					result1.close();
					result2.close();
					result3.close();
					result4.close();
					result5.close();
					result6.close();
				}catch(SQLException ex){
					ex.printStackTrace();
				}
				catch(Exception ex){
					ex.printStackTrace();
				}finally{
					cstmt.close();
				}
				
				return listTemp;
			}
			
		});
		
//		session.flush();
//		session.close();
		
		return list;
	}
	
	@Transactional
	public String getAgentBranch(String agntNum){
		Query query = sessionFactory.getCurrentSession().createSQLQuery("SELECT agnt_branch FROM " + GlobalVar.db_name +
				".tabagnt_fc_hk WHERE agntnum=:agntNum")
				.setParameter("agntNum", agntNum);
		
		Object result = query.uniqueResult();
		
		if(result != null)
			return result.toString();
		
		return "";
	}
	
	@Transactional
	public int updateCloseSale(long rid, boolean closed) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("UPDATE ap.tabreferal SET closed =:closed WHERE rid =:rid")
				.setParameter("rid", rid)
				.setParameter("closed", closed);
		return query.executeUpdate();
	}
	
	@Transactional
	public boolean insertCloseSaleLog(long userid, String action, String localip, String publicip, Date date) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("INSERT INTO ap.sec_logs(user_id_pk, action, local_ip, public_ip, created_date) VALUES (:userid,:action,:localip,:publicip,:date)")
				.setParameter("userid", userid)
				.setParameter("action", action)
				.setParameter("localip", localip)
				.setParameter("publicip", publicip)
				.setParameter("date", date);
		return query.executeUpdate() > 0;
	}
	
	@Transactional
	public int updateDraft(Long rid, String exiscusstatus, String rtype, Date rdate, String referralby, String rremark,
			String cusname, String cussex, String cusage, String cusmstatus, String cusnochd, int cusagechd, String cusphone,
			String cusphone2, int cusocc, String cusfacebook, String cusemail, String cusaprstatus,  Date cusaprdate,
			
			String sucappstatus,Date sucappdate,String salsucsalprestatus,Date salsucsalpredate, String saltyprecprd, int saldurpre,
			float cussalarymonthly, float cussalaryyearly, float salrecaffape, float salmthbasexp, float salrecsa, String salrecpolterm,
			int salrecprdpkgusd, String salpolterm, int salepcclssal, String salpolmode, int salepcclsperiod, String sucappremark,
			
			boolean closed, String draft) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("UPDATE ap.tabreferal SET exiscusstatus =:exiscusstatus,"
				+ "rtype =:rtype, rdate =:rdate, referralby =:referralby, rremark =:rremark, cusname =:cusname, cussex =:cussex,"
				+ "cusage =:cusage, cusmstatus =:cusmstatus,cusnochd =:cusnochd, cusagechd =:cusagechd, cusphone =:cusphone, cusphone2 =:cusphone2,"
				+ "cusocc =:cusocc, cusfacebook =:cusfacebook, cusemail =:cusemail, cusaprstatus =:cusaprstatus, cusaprdate =:cusaprdate,"
				+ "sucappstatus =:sucappstatus, sucappdate =:sucappdate, salsucsalprestatus =:salsucsalprestatus,salsucsalpredate =:salsucsalpredate,"
				+ "saltyprecprd =:saltyprecprd, saldurpre =:saldurpre,cussalarymonthly =:cussalarymonthly, cussalaryyearly =:cussalaryyearly,"
				+ "salrecaffape =:salrecaffape,salmthbasexp =:salmthbasexp,salrecsa =:salrecsa, salrecpolterm =:salrecpolterm,"
				+ "salrecprdpkgusd =:salrecprdpkgusd, salpolterm =:salpolterm, salepcclssal =:salepcclssal, salpolmode =:salpolmode,"
				+ "salepcclsperiod =:salepcclsperiod,sucappremark =:sucappremark, closed =:closed, draft =:draft"
				+ " WHERE rid =:rid")
				.setParameter("rid", rid)
				.setParameter("exiscusstatus", exiscusstatus)
				.setParameter("rtype", rtype)
				.setParameter("rdate", rdate)
				.setParameter("referralby", referralby)
				.setParameter("rremark", rremark)
				.setParameter("cusname", cusname)
				.setParameter("cussex", cussex)
				.setParameter("cusage", cusage)
				.setParameter("cusmstatus", cusmstatus)
				.setParameter("cusnochd", cusnochd)
				.setParameter("cusagechd", cusagechd)
				.setParameter("cusphone", cusphone)
				.setParameter("cusphone2", cusphone2)
				.setParameter("cusocc", cusocc)
				.setParameter("cusfacebook", cusfacebook)
				.setParameter("cusemail", cusemail)
				.setParameter("cusaprstatus", cusaprstatus)
				.setParameter("cusaprdate", cusaprdate)
				.setParameter("sucappstatus", sucappstatus)
				.setParameter("sucappdate", sucappdate)
				.setParameter("salsucsalprestatus", salsucsalprestatus)
				.setParameter("salsucsalpredate", salsucsalpredate)
				.setParameter("saltyprecprd", saltyprecprd)
				.setParameter("saldurpre", saldurpre)
				.setParameter("cussalarymonthly", cussalarymonthly)
				.setParameter("cussalaryyearly", cussalaryyearly)
				.setParameter("salrecaffape", salrecaffape)
				.setParameter("salmthbasexp", salmthbasexp)
				.setParameter("salrecsa", salrecsa)
				.setParameter("salrecpolterm", salrecpolterm)
				.setParameter("salrecprdpkgusd", salrecprdpkgusd)
				.setParameter("salpolterm", salpolterm)
				.setParameter("salepcclssal", salepcclssal)
				.setParameter("salpolmode", salpolmode)
				.setParameter("salepcclsperiod", salepcclsperiod)
				.setParameter("sucappremark", sucappremark)
				.setParameter("closed", closed)
				.setParameter("draft", draft);
				
		return query.executeUpdate();
		
	}
	
	@Transactional
	public List<ReferralAcbProduct> checkAcbProduct(int rid){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"SELECT acb_prd_id FROM ap.tabcus_acbproduct WHERE rid =:rid")
				.setParameter("rid", rid);
		List<ReferralAcbProduct> listReferralAcbProduct=query.list();
		return listReferralAcbProduct;
	}
	
	@Transactional
	public int insertOrUpdateDraftAcbProduct(ReferralAcbProduct referralacbproduct) {
		sessionFactory.getCurrentSession().saveOrUpdate(referralacbproduct);
		return 1;
	}
	
	
//	=============== Move From ReferralDaoImpl ===============
	@Transactional
	public int save(Referral referral, String username) {
		
		int affectedRow = 0;
		
		
		try{
			sessionFactory.getCurrentSession().save(referral);
			affectedRow = 1;
		}catch(HibernateException ex){
			
			ex.printStackTrace();
		}
		
		return affectedRow;
	}
	
	@Transactional
	public Referral getReferral(String rid, String username) {
		
		Referral referral = new Referral();
		
		Criterion critRid = Restrictions.eq("rID", Long.parseLong(rid));
		Criterion critUsername = Restrictions.eq("trnBy", username);
		
		LogicalExpression andExp = Restrictions.and(critRid, critUsername);
		
		Criteria crit = sessionFactory.getCurrentSession().createCriteria(Referral.class);
		crit.add(andExp);
		
		try{
			referral = (Referral)crit.list().get(0);
		}catch(HibernateException ex){
			ex.printStackTrace();
		}catch(IndexOutOfBoundsException ex){
			ex.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return referral;
	}
	
	@Transactional
	public int update(long rid, Referral referral) {
		
		int affectedRow = 0;
		Referral mReferral = null;
		
		Transaction trx = null;
		
		try{
			
			mReferral = (Referral) sessionFactory.getCurrentSession().get(referral.getClass(), rid);
			
			if(mReferral != null){
				mReferral.setAgntNum(referral.getAgntNum());
				mReferral.setAPE(referral.getAPE());
				mReferral.setAppDate(referral.getAppDate());
				mReferral.setClsBankAppNum(referral.getClsBankAppNum());
				mReferral.setClsBankUploadStatus(referral.getClsBankUploadStatus());
				mReferral.setClsBankSoDate(referral.getClsBankSoDate());
				mReferral.setClsBankSoStatus(referral.getClsBankSoStatus());
				mReferral.setClsBankUploadDate(referral.getClsBankUploadDate());
				mReferral.setClsBankUploadRemark(referral.getClsBankUploadRemark());
				mReferral.setCusAFPremium(referral.getCusAFPremium());
				mReferral.setCusAge(referral.getCusAge());
				mReferral.setCusFacebook(referral.getCusFacebook());
				mReferral.setCusMStatus(referral.getCusMStatus());
				mReferral.setCusName(referral.getCusName());
				mReferral.setCusNochd(referral.getCusNochd());
				mReferral.setCusPhone(referral.getCusPhone());
				mReferral.setCusSalaryMonthly(referral.getCusSalaryMonthly());
				mReferral.setCusSalaryYearly(referral.getCusSalaryYearly());
				mReferral.setCusSex(referral.getCusSex());
				mReferral.setCusOcc(referral.getCusOcc());
				mReferral.setCusAgeChd(referral.getCusAgeChd());
				mReferral.setCusPhone2(referral.getCusPhone2());
				mReferral.setCusEmail(referral.getCusEmail());
				
				mReferral.setCusAprStatus(referral.getCusAprStatus());
				mReferral.setCusAprDate(referral.getCusAprDate());
				
				mReferral.setExisCusStatus(referral.getExisCusStatus());
				mReferral.setFlwupCmtRemarkLast(referral.getFlwupCmtRemarkLast());
				mReferral.setFlwupCmtTypeLast(referral.getFlwupCmtTypeLast());
				mReferral.setFlwupTrnDate(referral.getFlwupTrnDate());
				mReferral.setIP(referral.getIP());
				mReferral.setLastTrnBy(SecurityContextHolder.getContext().getAuthentication().getName());
				mReferral.setLastTrnDateTime(new Date());
//				mReferral.setOrgAgntNum(referral.getOrgAgntNum());
//				mReferral.setOrgBranch(referral.getOrgBranch());
				mReferral.setpIP(referral.getpIP());
				mReferral.setrDate(referral.getrDate());
				mReferral.setReferralBy(referral.getReferralBy());
				mReferral.setrRemark(referral.getrRemark());
				mReferral.setrType(referral.getrType());
				mReferral.setSucAppDate(referral.getSucAppDate());
				mReferral.setSucAppRemark(referral.getSucAppRemark());
				mReferral.setSucAppStatus(referral.getSucAppStatus());
				mReferral.setSalSucSalPreStatus(referral.getSalSucSalPreStatus());
				mReferral.setSalSucSalPreDate(referral.getSalSucSalPreDate());
				mReferral.setValidFlag(referral.getValidFlag());
				mReferral.setSalTypRecPrd(referral.getSalTypRecPrd());
				mReferral.setSalDurPre(referral.getSalDurPre());
				mReferral.setSalRecAffApe(referral.getSalRecAffApe());
				mReferral.setSalMthBasExp(referral.getSalMthBasExp());
				mReferral.setSalRecSA(referral.getSalRecSA());
				mReferral.setSalRecPolTerm(referral.getSalRecPolTerm());
				mReferral.setSalRecPrdPkgUsd(referral.getSalRecPrdPkgUsd());
				mReferral.setSalPolTerm(referral.getSalPolTerm());
				mReferral.setSalEpcClsSal(referral.getSalEpcClsSal());
				mReferral.setSalPolMode(referral.getSalPolMode());
				mReferral.setSalEpcClsPeriod(referral.getSalEpcClsPeriod());
				mReferral.setClsPrdType(referral.getClsPrdType());
				mReferral.setClsPolTerm(referral.getClsPolTerm());
				mReferral.setClsPolMode(referral.getClsPolMode());
				mReferral.setCustomerFrom(referral.getCustomerFrom());
				mReferral.setClosed(referral.isClosed());
				mReferral.setDraft(referral.getDraft());
//				mReferral.setCdate(referral.getCdate());
				
				sessionFactory.getCurrentSession().update(mReferral);
				
				
				affectedRow = 1;
			}
			
		}catch(HibernateException ex){
			if(trx != null) trx.rollback();
			ex.printStackTrace();
		}
		
		return affectedRow;
	}
	
	@Transactional
	public int saveComment(ReferralFollowUpComment comment) {
		int affectedRow = 0;
		
		Transaction trx = null;
		
		try{
			sessionFactory.getCurrentSession().save(comment);
			
			affectedRow = 1;
		}catch(HibernateException ex){
			if(trx != null) trx.rollback();
			ex.printStackTrace();
		}
		
		return affectedRow;
	}
	
	@Transactional
	public Referral getReferral(String rid) {
		
		Referral referral = new Referral();
		
		Criteria crit = sessionFactory.getCurrentSession().createCriteria(Referral.class);
		crit.add(Restrictions.eq("rID", Long.parseLong(rid)));
		
		try{
			referral = (Referral)crit.list().get(0);
			Hibernate.initialize(referral.getReferralAcbProducts());
		}catch(HibernateException ex){
			ex.printStackTrace();
		}catch(IndexOutOfBoundsException ex){
			ex.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return referral;
	}
	
	@Transactional
	public Referral getReferralMgt(String rid, String username) {
		
		boolean isValid = false;
		
		Referral referral = new Referral();
		
		Criteria crit = sessionFactory.getCurrentSession().createCriteria(Referral.class);
		crit.add(Restrictions.eq("rID", Long.parseLong(rid)));
		
		try{
			referral = (Referral)crit.list().get(0);
			
			if(referral.getrID() > 0){
				Query query = sessionFactory.getCurrentSession().createSQLQuery("select * from " + GlobalVar.db_name + ".spsearch_referral_byagentmgt_cnt(:agntnum, :userID)");
				query.setParameter("agntnum", referral.getTrnBy());
				query.setParameter("userID", username);
				
				List<Object> objList = query.list();
				
				if(objList.size()>0){
					isValid = Integer.parseInt(objList.get(0).toString()) > 0;
					Hibernate.initialize(referral.getReferralAcbProducts());
				}
				
				if(!isValid){
					referral = new Referral();
				}
			}
		}catch(HibernateException ex){
			ex.printStackTrace();
		}catch(IndexOutOfBoundsException ex){
			ex.printStackTrace();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return referral;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<AcbProduct> listAcbProduct(){
		Query query = sessionFactory.getCurrentSession().createQuery("from AcbProduct as product where product.validFlag='1'");
		
		List<AcbProduct> list = query.list();
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<AcbProductComment> listAcbProductComment(){
		Query query = sessionFactory.getCurrentSession().createQuery("from AcbProductComment as productcomment where productcomment.validFlag='1'");
		
		List<AcbProductComment> list = query.list();
		
		return list;
	}
	
	 @SuppressWarnings("unchecked")
     @Transactional
     public List<Object[]> listReferralToFollowup(String userid){
            Query query = sessionFactory.getCurrentSession().createSQLQuery("select * from " + GlobalVar.db_name + ".splist_referral_to_followup(:userid)")
                         .setParameter("userid", userid);
            
            List<Object[]> list = query.list();
            
            return list;
     }
     
     @Transactional
 	public void saveApproach(UpsellingAndCrosssellingReferral Approach){
 		sessionFactory.getCurrentSession().saveOrUpdate(Approach);
 	}
 	

}