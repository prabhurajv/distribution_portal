package com.pru.pruquote.service;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.ReturningWork;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.PruquoteParm;
import com.pru.pruquote.model.VpruquoteParm;
import com.pru.pruquote.model.listPruquote;
import com.pru.pruquote.repository.PruquoteParamRepository;

@Service
public class ListQuoteService {
			
	@PersistenceContext
	private EntityManager em;
	 
	@Autowired
	PruquoteParamRepository pruquoteParamRepository;
		
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<listPruquote> listQuote(String user,String ID){
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"select * from ap.spList_Quote (:user,:id)")
				//.addEntity(listPruquote.class)
				.setParameter("user",user)
				.setParameter("id",ID);

		List<listPruquote> listquotes = null;
		try{
			listquotes = (List<listPruquote>) query.list();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return listquotes;
	}
	
}