package com.pru.pruquote.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.FailWelcomeCall;
import com.pru.pruquote.utility.GlobalVar;

@Service
public class FailWelcomeCallService {
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<FailWelcomeCall> listFailWelcomeCall(String userid) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("SELECT " +
				"chdr_appnum as \"chdrAppNum\"" + 
				",chdrnum as \"chdrNum\"" + 
				",po_name as \"poName\"" + 
				",gender" + 
				",po_phone1 as \"poPhone1\"" + 
				",po_phone2 as \"poPhone2\"" + 
				",po_email as \"poEmail\"" + 
				",supv_name as \"supvName\"" + 
				",agntnum as \"agntNum\"" + 
				",agntname as \"agntName\"" + 
				",agnt_phone as \"agntPhone\"" + 
				",cc_called_date as \"ccCalledDate\"" + 
				",sms_po_date as \"smsPoDate\"" + 
				",remark_from_cc as \"ccRemark\"" + 
				",cc_user as \"ccUser\"" + 
				",email_sent_date as \"emailSendDate\"" + 
				",agnt_response as \"agntResponse\"" + 
				",agnt_response_date as \"agntResponseDate\"" + 
				",agnt_po_remark as \"agntPoRemark\"" + 
				",called_date as \"calledDate\"" + 
				",called_by as \"calledBy\"" + 
				",called_status as \"calledStatus\"" + 
				",final_called_status as \"finalCalledStatus\"" + 
				",branch_code as \"branchCode\"" + 
				",branch_name as \"branchName\"" + 
				",report_date as \"reportDate\"" +
				",cc_remark as \"saleForceComment\"" +
				",cc_by as \"ccBy\"" +
				",cc_date as \"ccDate\"" +
				" FROM " + GlobalVar.db_name + ".spsearch_failwc(:userid)")
				.setParameter("userid", userid)
				.setResultTransformer(Transformers.aliasToBean(FailWelcomeCall.class));
		
		List<FailWelcomeCall> list = query.list();
		
		return list;
	}
}
