package com.pru.pruquote.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.User;
import com.pru.pruquote.model.UserHistory;
import com.pru.pruquote.utility.PwdEncoder;

@Service
public class UserService {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	public User getUser(String username) {
		User user = null;
		
		sessionFactory.getCurrentSession().enableFilter("onlyValidRolePermission").setParameter("isValid", true);
		Criteria crit = sessionFactory.getCurrentSession().createCriteria(User.class);
		
		try{
			crit.add(Restrictions.eq("username", username));
			
			user = (User) crit.uniqueResult();
			
			// Prevent LazyInitializationException when Spring try to get roles while session is already closed.
			// This exception happens because of relationship between models.
			if(user != null) {
				Hibernate.initialize(user.getRoles());
				Hibernate.initialize(user.getRoles().get(0).getPermissions());
			}
		}catch(HibernateException e){
			e.printStackTrace();
		}
		
		return user;
	}
	
	@Transactional
	public void setLoginAttempt(User user) {
		String sql = "";
		
		try{
			Query query = null;
			
			if(user.getAttemptNo() == 2) {
				sql = "UPDATE \"ap\".sec_users SET attempt_no = ?, "
						+ "locked = true, account_lock_date = CAST(? AS timestamp without time zone) WHERE username=?";
				
				query = sessionFactory.getCurrentSession().createSQLQuery(sql);
				query.setParameter(0, user.getAttemptNo() + 1);
				query.setParameter(1, new Date());
				query.setParameter(2, user.getUsername());
				
				query.executeUpdate();
			}else if(user.getAttemptNo() < 2){
				sql = "UPDATE \"ap\".sec_users SET attempt_no = ? "
						+ "WHERE username=?";
				
				query = sessionFactory.getCurrentSession().createSQLQuery(sql);
				query.setParameter(0, user.getAttemptNo() + 1);
				query.setParameter(1, user.getUsername());
				
				query.executeUpdate();
			}
		}catch(HibernateException e){
			e.printStackTrace();
		}
	}
	
	@Transactional
	public int updateUserPwd(User user) {
		int affected = 0;
		
		Query query = null;
		
		try{
			String sql = "UPDATE \"ap\".sec_users SET password = ?, first_login=?, pwd_expired_date=? WHERE username=?";
			query = sessionFactory.getCurrentSession().createSQLQuery(sql);
			query.setParameter(0, user.getPwd());
			query.setParameter(1, user.isFirstLogin());
			query.setParameter(2, user.getPwdExpiredDate());
			query.setParameter(3, user.getUsername());
			
			query.executeUpdate();

			affected = 1;
		}catch(HibernateException e) {
			e.printStackTrace();
		}
		
		return affected;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public boolean isPwdAlreadyUsed(long userIdPk, String newRawPwd) {
		boolean isUsed = false;
		
		
		Criteria crx = sessionFactory.getCurrentSession().createCriteria(UserHistory.class);
		crx.add(Restrictions.eq("userIdPk", userIdPk));
		
		List<UserHistory> userHistories = null;
		
		try{
			userHistories = crx.list();
		}catch(HibernateException e) {
			e.printStackTrace();
		}
		
		if(userHistories != null && userHistories.size() > 0) {
			for(UserHistory userHistory : userHistories) {
				if(PwdEncoder.matchesPwd(newRawPwd, userHistory.getPwd())) {
					isUsed = true;
					
					break;
				}
			}
		}
		
		return isUsed;
	}
	
	@Transactional
	public void disableUser() {

		Query query = sessionFactory.getCurrentSession().createSQLQuery("select \"ap\".spgen_disableuser()");
		
		try{
			query.executeUpdate();
		}catch(HibernateException e) {
			e.printStackTrace();
		}
	}
	
	@Transactional
	public void disableUser(User user){
		user.setEnabled(false);
		sessionFactory.getCurrentSession().update(user);
	}
	
	@Transactional
	public void resetLoginAttemptAndLastLoginDate(String username) {
		Query query = sessionFactory.getCurrentSession().createSQLQuery("update \"ap\".sec_users set attempt_no = ?, last_login_date = ? where username = ?");
		query.setParameter(0, 0);
		query.setParameter(1, new Date());
		query.setParameter(2, username);
		
		try{
			query.executeUpdate();
		}catch(HibernateException e) {
			e.printStackTrace();
		}
	}
}