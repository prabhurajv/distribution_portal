package com.pru.pruquote.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.dao.IUserDao;
import com.pru.pruquote.model.Permission;
import com.pru.pruquote.model.Role;
import com.pru.pruquote.model.User;
import com.pru.pruquote.utility.AnonymousHelper;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private IUserDao userDao;
	
	@Autowired
	private UserService userService;
	
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		//userDao.disableUser();
		
		User user = userService.getUser(username);
		
		if(user.getLastLoginDate() != null && user.isEnabled()){
			if(AnonymousHelper.getLapseDays(user.getLastLoginDate().getTime()) > 120){
				user.setEnabled(false);
				userService.disableUser(user);		
			}
		}
		
		if(user != null) {
			String pwd = user.getPwd();
			boolean enabled = user.isEnabled();
			boolean accountNonExpired = (user.getAccountExpiredDate() == null || (user.getAccountExpiredDate().getTime() - new Date().getTime()) >= 0);
			boolean credentialsNonExpired = (user.getPwdExpiredDate() == null || (user.getPwdExpiredDate().getTime() - new Date().getTime()) >= 0);
			boolean accountNonLocked = (user.isLocked() == false);
			
			Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			
			for (Role role : user.getRoles()) {
				// SimpleGrantedAuthority replaces GrantedAuthorityImpl
				
				for(Permission permission : role.getPermissions()){
					authorities.add(new SimpleGrantedAuthority(permission.getPermissionName()));
				}
			}
			
			org.springframework.security.core.userdetails.User securityUser = 
					new org.springframework.security.core.userdetails.User(username, pwd, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
			
			return securityUser;
		}else {
			throw new UsernameNotFoundException("Username or password is invalid!");
		}
	}
	
//	private List<String> getPrivileges(Collection<Role> roles) {
//		  
//        List<String> permissions = new ArrayList<>();
//        List<Permission> collection = new ArrayList<>();
//        for (Role role : roles) {
//            collection.addAll(role.getPrivileges());
//        }
//        for (Permission item : collection) {
//            permissions.add(item.getName());
//        }
//        return permissions;
//    }
//
//	private List<GrantedAuthority> getGrantedAuthorities(List<String> permissions) {
//        List<GrantedAuthority> authorities = new ArrayList<>();
//        for (String permission : permissions) {
//            authorities.add(new SimpleGrantedAuthority(permission));
//        }
//        return authorities;
//    }
}
