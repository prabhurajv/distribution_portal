package com.pru.pruquote.service;

import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pru.pruquote.model.Log;
import com.pru.pruquote.model.User;
import com.pru.pruquote.model.UserHistory;

@Service
public class LogService {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	public void logUserAction(String username, String action, String ip, String pip, Date date) {

		Log log = new Log();
		log.setAction(action);
		log.setIp(ip);
		log.setPip(pip);
		log.setCreatedDate(date);
		
		Criteria crit = sessionFactory.getCurrentSession().createCriteria(User.class);
		crit.add(Restrictions.eq("username", username));
		
		User user = null;
		
		if(crit.list().size() > 0){
			user = (User)crit.list().get(0);
			
			log.setUserIdPk(user.getId());
		}
		
		sessionFactory.getCurrentSession().save(log);
	}
	
	@Transactional
	public void logChangePwd(String username, String action, String oldPwd, String operator, Date date) {
		
		UserHistory history = new UserHistory();
		history.setDescription(action);
		history.setPwd(oldPwd);
		history.setCreatedBy(operator);
		history.setCreatedDate(date);
		
		Criteria crit = sessionFactory.getCurrentSession().createCriteria(User.class);
		
		crit.add(Restrictions.eq("username", username));
		
		User user = null;
		
		if(crit.list().size() > 0){
			user = (User)crit.list().get(0);
			
			history.setUserIdPk(user.getId());
		}
		
		sessionFactory.getCurrentSession().saveOrUpdate(history);
	}
}