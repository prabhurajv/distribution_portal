package com.pru.pruquote.property;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Component;

@Component
@Configuration
//@PropertySource("classpath:eaps.properties")
public class EapsProperties implements IEapsProperties {
//	@Value("${user}")
//	private String user;
//
//	@Value("${pwd}")
//	private String pwd;
	
	//To resolve ${} in @Value
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}

//  @Override
//  public String getUser() {
//         return "6z4NakOy/ll3Jms/oUeu6po5cpDD7aCnuNGtZWgMHXk=";
//  }
//
//  @Override
//  public String getPwd() {
//         return "UTsTvaM565vydNRFkOzZ2fQBTpLwa2K3ADlcvYaJFGI=";
//  }
	
	public String getUser(){
		return System.getProperty("user");
	}
	public String getPwd(){
		return System.getProperty("pwd");
	}

}
