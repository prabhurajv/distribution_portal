package com.pru.pruquote.property;

import java.util.List;

import com.pru.pruquote.model.CollectionAndLapse;

public interface ICollectionAndLapsed {
	public List<CollectionAndLapse> filterCollectionAndLapse(List<CollectionAndLapse> collectionAndLapse, String criteria , String operator);
	public List<CollectionAndLapse> filterCollectionAndLapse(List<CollectionAndLapse> collectionAndLapse, String criteria , String operator, String criterialcompare);
}
