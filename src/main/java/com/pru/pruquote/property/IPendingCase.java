package com.pru.pruquote.property;
import java.util.List;
import com.pru.pruquote.model.PendingCase;

public interface IPendingCase {
	public List<PendingCase> filterPendingCase(List<PendingCase> pendingCase, String criteria , String operator);
	public List<PendingCase> filterPendingCase(List<PendingCase> pendingCase, String criteria , String operator, String criterialcompare);
}
