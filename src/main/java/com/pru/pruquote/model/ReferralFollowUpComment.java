package com.pru.pruquote.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name = "tabreferral_followup_comment", catalog = GlobalVar.db_name)
public class ReferralFollowUpComment {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "followup_id")
	@SequenceGenerator(name = "followup_id", sequenceName = GlobalVar.db_name + ".followup_id", allocationSize = 1)
	@Column(name = "fid")
	private long fid;
	
	@Column(name = "rid")
	private long rid;
	
	@Column(name = "appdate")
	private Date appDate;
	
	@Column(name = "cid")
	private long cid;
	
	@Column(name = "remark")
	private String remark;
	
	@Column(name = "trndate")
	private Date trnDate;
	
	@Column(name = "trnby")
	private String trnBy;
	
	@Column(name = "ip")
	private String ip;
	
	@Column(name = "pip")
	private String pip;
	
	@Transient
	private String cusName;
	
	@Transient
	private String cusSex;
	
	@Transient
	private String cusPhone1;
	
	
	@Transient
	private String appNum;
	
	public long getFid() {
		return fid;
	}
	public void setFid(long fid) {
		this.fid = fid;
	}
	public long getRid() {
		return rid;
	}
	public void setRid(long rid) {
		this.rid = rid;
	}
	public Date getAppDate() {
		return appDate;
	}
	public void setAppDate(Date appDate) {
		this.appDate = appDate;
	}
	public long getCid() {
		return cid;
	}
	public void setCid(long cid) {
		this.cid = cid;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getTrnDate() {
		return trnDate;
	}
	public void setTrnDate(Date trnDate) {
		this.trnDate = trnDate;
	}
	public String getTrnBy() {
		return trnBy;
	}
	public void setTrnBy(String trnBy) {
		this.trnBy = trnBy;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getPip() {
		return pip;
	}
	public void setPip(String pip) {
		this.pip = pip;
	}
	public String getCusName() {
		return cusName;
	}
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
	public String getCusSex() {
		return cusSex;
	}
	public void setCusSex(String cusSex) {
		this.cusSex = cusSex;
	}
	public String getCusPhone1() {
		return cusPhone1;
	}
	public void setCusPhone1(String cusPhone1) {
		this.cusPhone1 = cusPhone1;
	}
	public String getAppNum() {
		return appNum;
	}
	public void setAppNum(String appNum) {
		this.appNum = appNum;
	}
}
