package com.pru.pruquote.model;

import java.util.Date;

public class FailWelcomeCall {
	private String chdrAppNum;
	private String chdrNum;
	private String poName;
	private String gender;
	private String poPhone1;
	private String poPhone2;
	private String poEmail;
	private String supvName;
	private String agntNum;
	private String agntName;
	private String agntPhone;
	private Date ccCalledDate;
	private Date smsPoDate;
	private String ccRemark;
	private String ccUser;
	private Date emailSendDate;
	private String agntResponse;
	private Date agntResponseDate;
	private String agntPoRemark;
	private Date calledDate;
	private String calledBy;
	private String calledStatus;
	private String finalCalledStatus;
	private String branchCode;
	private String branchName;
	private Date reportDate;
	private String saleForceComment;
	private String ccBy;
	private Date ccDate;
	
	public String getChdrAppNum() {
		return chdrAppNum;
	}
	public void setChdrAppNum(String chdrAppNum) {
		this.chdrAppNum = chdrAppNum;
	}
	public String getChdrNum() {
		return chdrNum;
	}
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}
	public String getPoName() {
		return poName;
	}
	public void setPoName(String poName) {
		this.poName = poName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPoPhone1() {
		return poPhone1;
	}
	public void setPoPhone1(String poPhone1) {
		this.poPhone1 = poPhone1;
	}
	public String getPoPhone2() {
		return poPhone2;
	}
	public void setPoPhone2(String poPhone2) {
		this.poPhone2 = poPhone2;
	}
	public String getPoEmail() {
		return poEmail;
	}
	public void setPoEmail(String poEmail) {
		this.poEmail = poEmail;
	}
	public String getSupvName() {
		return supvName;
	}
	public void setSupvName(String supvName) {
		this.supvName = supvName;
	}
	public String getAgntNum() {
		return agntNum;
	}
	public void setAgntNum(String agntNum) {
		this.agntNum = agntNum;
	}
	public String getAgntName() {
		return agntName;
	}
	public void setAgntName(String agntName) {
		this.agntName = agntName;
	}
	public String getAgntPhone() {
		return agntPhone;
	}
	public void setAgntPhone(String agntPhone) {
		this.agntPhone = agntPhone;
	}
	public Date getCcCalledDate() {
		return ccCalledDate;
	}
	public void setCcCalledDate(Date ccCalledDate) {
		this.ccCalledDate = ccCalledDate;
	}
	public Date getSmsPoDate() {
		return smsPoDate;
	}
	public void setSmsPoDate(Date smsPoDate) {
		this.smsPoDate = smsPoDate;
	}
	public String getCcRemark() {
		return ccRemark;
	}
	public void setCcRemark(String ccRemark) {
		this.ccRemark = ccRemark;
	}
	public String getCcUser() {
		return ccUser;
	}
	public void setCcUser(String ccUser) {
		this.ccUser = ccUser;
	}
	public Date getEmailSendDate() {
		return emailSendDate;
	}
	public void setEmailSendDate(Date emailSendDate) {
		this.emailSendDate = emailSendDate;
	}
	public String getAgntResponse() {
		return agntResponse;
	}
	public void setAgntResponse(String agntResponse) {
		this.agntResponse = agntResponse;
	}
	public Date getAgntResponseDate() {
		return agntResponseDate;
	}
	public void setAgntResponseDate(Date agntResponseDate) {
		this.agntResponseDate = agntResponseDate;
	}
	public String getAgntPoRemark() {
		return agntPoRemark;
	}
	public void setAgntPoRemark(String agntPoRemark) {
		this.agntPoRemark = agntPoRemark;
	}
	public Date getCalledDate() {
		return calledDate;
	}
	public void setCalledDate(Date calledDate) {
		this.calledDate = calledDate;
	}
	public String getCalledBy() {
		return calledBy;
	}
	public void setCalledBy(String calledBy) {
		this.calledBy = calledBy;
	}
	public String getCalledStatus() {
		return calledStatus;
	}
	public void setCalledStatus(String calledStatus) {
		this.calledStatus = calledStatus;
	}
	public String getFinalCalledStatus() {
		return finalCalledStatus;
	}
	public void setFinalCalledStatus(String finalCalledStatus) {
		this.finalCalledStatus = finalCalledStatus;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	public String getSaleForceComment() {
		return saleForceComment;
	}
	public void setSaleForceComment(String saleForceComment) {
		this.saleForceComment = saleForceComment;
	}
	public String getCcBy() {
		return ccBy;
	}
	public void setCcBy(String ccBy) {
		this.ccBy = ccBy;
	}
	public Date getCcDate() {
		return ccDate;
	}
	public void setCcDate(Date ccDate) {
		this.ccDate = ccDate;
	}
	
}
