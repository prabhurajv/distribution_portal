package com.pru.pruquote.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name = "file", catalog = GlobalVar.db_name)
public class File implements Serializable {
	private static final long serialVersionUID = 2260117902522764908L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	
	@Column(name = "file_name", length = 100, nullable = false)
	private String fileName;
	
	@Column(name = "file_occ", nullable = false)
	private byte[] fileOcc;
	
	@Column(name = "isdeleted")
	private int isDeleted;
	
	@Column(name = "created_by", length = 50)
	private String  createdBy;
	
	@Column(name = "created_date")
	private Date createdDate;
	
	@Column(name = "modified_by", length = 50)
	private String modifiedBy;
	
	@Column(name = "modified_date")
	private Date modifiedDate;
	
//	@ManyToMany(cascade = CascadeType.ALL)
//	@JoinTable(
//		name = "file_occupation",
//		catalog = GlobalVar.db_name,
//		joinColumns = { @JoinColumn(name = "file_id") },
//		inverseJoinColumns = { @JoinColumn(name = "occ_id") }
//	)
//	private List<Occupation> occupations;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public byte[] getFileOcc() {
		return fileOcc;
	}

	public void setFileOcc(byte[] fileOcc) {
		this.fileOcc = fileOcc;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

//	public List<Occupation> getOccupations() {
//		return occupations;
//	}
//
//	public void setOccupations(List<Occupation> occupations) {
//		this.occupations = occupations;
//	}
//	
	
}