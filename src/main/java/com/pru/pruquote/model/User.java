package com.pru.pruquote.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="\"ap\".sec_users")
public class User {
	@Id
	@Column(name="\"id\"")
	private long id;
	
	@Column(name="\"username\"")
	private String username;
	
	@Column(name="\"password\"")
	private String pwd;
	
	@Column(name="\"user_id\"")
	private String userId;
	
	@Column(name="\"full_name\"")
	private String fullName;
	
	@Column(name="\"full_name_kh\"")
	private String fullNameKh;
	
	@Column(name="\"gender\"")
	private String gender;
	
	@Column(name="\"email\"")
	private String email;
	
	@Column(name="\"account_expired_date\"")
	private Date accountExpiredDate;
	
	@Column(name="\"pwd_expired_date\"")
	private Date pwdExpiredDate;
	
	@Column(name="\"last_login_date\"")
	private Date lastLoginDate;
	
	@Column(name="\"account_lock_date\"")
	private Date accountLockDate;
	
	@Column(name="\"enabled\"")
	private boolean enabled;
	
	@Column(name="\"first_login\"")
	private boolean firstLogin;
	
	@Column(name="\"locked\"")
	private boolean locked;
	
	@Column(name="\"attempt_no\"")
	private int attemptNo;
	
	@Column(name="\"max_attempt_no\"")
	private int maxAttemptNo;
	
	@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(name = "\"ap\".sec_user_roles",
				joinColumns = @JoinColumn(name = "user_id_pk"),
				inverseJoinColumns = @JoinColumn(name = "role_id"))
	private List<Role> roles;
	
	@Column(name="\"branch_id\"")
	private int branchId;
	
	@Column(name="\"parent_id\"")
	private int parentId;
	
	@Column(name="\"post_id\"")
	private int postId;
	
	@Column(name="\"created_date\"")
	private Date createdDate;
	
	@Column(name="\"created_by\"")
	private String createdBy;
	
	@Column(name="\"last_updated_date\"")
	private Date lastUpdatedDate;
	
	@Column(name="\"last_updated_by\"")
	private String lastUpdatedBy;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user",cascade=CascadeType.ALL)
	private List<UserRole> userRoles;

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPwd() {
		return this.pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFullNameKh() {
		return this.fullNameKh;
	}

	public void setFullNameKh(String fullNameKh) {
		this.fullNameKh = fullNameKh;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getAccountExpiredDate() {
		return this.accountExpiredDate;
	}

	public void setAccountExpiredDate(Date accountExpiredDate) {
		this.accountExpiredDate = accountExpiredDate;
	}

	public Date getPwdExpiredDate() {
		return this.pwdExpiredDate;
	}

	public void setPwdExpiredDate(Date pwdExpiredDate) {
		this.pwdExpiredDate = pwdExpiredDate;
	}

	public Date getLastLoginDate() {
		return this.lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public Date getAccountLockedDate() {
		return this.accountLockDate;
	}

	public void setAccountLockedDate(Date accountLockedDate) {
		this.accountLockDate = accountLockedDate;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.enabled = isEnabled;
	}

	public boolean isFirstLogin() {
		return this.firstLogin;
	}

	public void setFirstLogin(boolean isFirstLogin) {
		this.firstLogin = isFirstLogin;
	}

	public boolean isLocked() {
		return this.locked;
	}

	public void setLocked(boolean isLocked) {
		this.locked = isLocked;
	}

	public int getAttemptNo() {
		return this.attemptNo;
	}

	public void setAttemptNo(int attemptNo) {
		this.attemptNo = attemptNo;
	}

	public int getMaxAttemptNo() {
		return this.maxAttemptNo;
	}

	public void setMaxAttemptNo(int maxAttemptNo) {
		this.maxAttemptNo = maxAttemptNo;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public int getBranchId() {
		return this.branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public int getParentId() {
		return this.parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public int getPostId() {
		return this.postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public List<UserRole> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

}