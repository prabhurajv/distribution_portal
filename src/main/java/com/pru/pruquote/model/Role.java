package com.pru.pruquote.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterJoinTable;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="\"ap\".sec_roles")
@FilterDef(name = "onlyValidRolePermission", parameters = {
		@ParamDef(name = "isValid", type = "boolean")
})
public class Role {
	@Id
	@Column(name="\"role_id\"")
	private int roleId;
	
	@Column(name="\"role_name\"")
	private String roleName;
	
	@Column(name="\"role_desc\"")
	private String roleDesc;
	
	@Column(name="\"role_lv\"")
	private int roleLV;
	
	@Column(name="\"status\"")
	private int status;
	
	@ManyToMany(mappedBy = "roles")
	private List<User> users;
	
	@Column(name="\"created_date\"")
	private Date createdDate;
	
	@Column(name="\"created_by\"")
	private String createdBy;
	
	@Column(name="\"last_updated_date\"")
	private Date lastUpdatedDate;
	
	@Column(name="\"last_updated_by\"")
	private String lastUpdatedBy;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "role")
	private List<UserRole> userRoles = new ArrayList<UserRole>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "role",cascade=CascadeType.ALL)
	private List<RolePermission> rolePermissions = new ArrayList<RolePermission>();
	
	@ManyToMany(cascade = CascadeType.MERGE)
	@JoinTable(name = "\"ap\".sec_role_permissions",
				joinColumns = @JoinColumn(name = "role_id"),
				inverseJoinColumns = @JoinColumn(name = "permission_id"))
	@FilterJoinTable(name = "onlyValidRolePermission", condition = "is_valid = :isValid")
	private List<Permission> permissions;

	public int getId() {
		return this.roleId;
	}

	public void setId(int id) {
		this.roleId = id;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDesc() {
		return this.roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public int getRoleLevel() {
		return this.roleLV;
	}

	public void setRoleLevel(int roleLevel) {
		this.roleLV = roleLevel;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date updatedDate) {
		this.lastUpdatedDate = updatedDate;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public List<UserRole> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public List<RolePermission> getRolePermissions() {
		return rolePermissions;
	}

	public void setRolePermissions(List<RolePermission> rolePermissions) {
		this.rolePermissions = rolePermissions;
	}

	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}
}