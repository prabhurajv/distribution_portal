package com.pru.pruquote.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name = "\"tabcus_acbproduct\"", catalog = GlobalVar.db_name)
public class ReferralAcbProduct {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "\"acb_prd_id\"")
	private int acbPrdId;
	
	@Column(name = "\"validflag\"")
	private String validFlag;
	
	private String createdUserId;
	private Date createdDatetime;
	private String modifiedUserId;
	private Date modifiedDatetime;
	private String cusApproach;
	private int commentId;
	
	@ManyToOne
	@JoinColumn(name = "rid")
	private Referral referral;
	
	@ManyToOne
	@JoinColumn(name = "acb_prd_id", insertable = false, updatable = false)
	private AcbProduct acbProduct;
	
	@ManyToOne
	@JoinColumn(name = "commentid", insertable = false, updatable = false)
	@NotFound(action = NotFoundAction.IGNORE)
	private AcbProductComment acbProductComment;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAcbPrdId() {
		return acbPrdId;
	}
	public void setAcbPrdId(int acbPrdId) {
		this.acbPrdId = acbPrdId;
	}
	public String getValidFlag() {
		return validFlag;
	}
	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}
	public String getCreatedUserId() {
		return createdUserId;
	}
	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}
	public Date getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public String getModifiedUserId() {
		return modifiedUserId;
	}
	public void setModifiedUserId(String modifiedUserId) {
		this.modifiedUserId = modifiedUserId;
	}
	public Date getModifiedDatetime() {
		return modifiedDatetime;
	}
	public void setModifiedDatetime(Date modifiedDatetime) {
		this.modifiedDatetime = modifiedDatetime;
	}
	public Referral getReferral() {
		return referral;
	}
	public void setReferral(Referral referral) {
		this.referral = referral;
	}
	public AcbProduct getAcbProduct() {
		return acbProduct;
	}
	public void setAcbProduct(AcbProduct acbProduct) {
		this.acbProduct = acbProduct;
	}
	public String getCusApproach() {
		return cusApproach;
	}
	public void setCusApproach(String cusApproach) {
		this.cusApproach = cusApproach;
	}
	public int getCommentId() {
		return commentId;
	}
	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}
}
