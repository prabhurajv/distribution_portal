package com.pru.pruquote.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class Submission implements Serializable {
	
	private static final long serialVersionUID = 2260117902522764908L;
	
	private String chdrNum;
	private String chdrAppNum;
	private String poName;
	private Date subDate;
	private Date chdrKeyDate;
	private Date chdrIssDate;
	private String chdrStatCode;
	private Date chdrNtuDate;
	private Date chdrWdDate;
	private Date chdrPoDate;
	private Date chdrCfDate;
	private BigDecimal chdrApe;
	private String agntNum;
	private String agntName;
	private String weekth;
	private String branchCode;
	private String branchName;
	
	public String getChdrNum() {
		return chdrNum;
	}

	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}

	public String getChdrAppNum() {
		return chdrAppNum;
	}

	public void setChdrAppNum(String chdrAppNum) {
		this.chdrAppNum = chdrAppNum;
	}

	public String getPoName() {
		return poName;
	}

	public void setPoName(String poName) {
		this.poName = poName;
	}

	public Date getSubDate() {
		return subDate;
	}

	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}

	public BigDecimal getChdrApe() {
		return chdrApe;
	}

	public void setChdrApe(BigDecimal chdrApe) {
		this.chdrApe = chdrApe;
	}

	public String getAgntNum() {
		return agntNum;
	}

	public void setAgntNum(String agntNum) {
		this.agntNum = agntNum;
	}

	public String getAgntName() {
		return agntName;
	}

	public void setAgntName(String agntName) {
		this.agntName = agntName;
	}

	public String getWeekth() {
		return weekth;
	}

	public void setWeekth(String weekth) {
		this.weekth = weekth;
	}

	public Date getChdrKeyDate() {
		return chdrKeyDate;
	}

	public void setChdrKeyDate(Date chdrKeyDate) {
		this.chdrKeyDate = chdrKeyDate;
	}

	public Date getChdrIssDate() {
		return chdrIssDate;
	}

	public void setChdrIssDate(Date chdrIssDate) {
		this.chdrIssDate = chdrIssDate;
	}

	public String getChdrStatCode() {
		return chdrStatCode;
	}

	public void setChdrStatCode(String chdrStatCode) {
		this.chdrStatCode = chdrStatCode;
	}

	public Date getChdrNtuDate() {
		return chdrNtuDate;
	}

	public void setChdrNtuDate(Date chdrNtuDate) {
		this.chdrNtuDate = chdrNtuDate;
	}

	public Date getChdrWdDate() {
		return chdrWdDate;
	}

	public void setChdrWdDate(Date chdrWdDate) {
		this.chdrWdDate = chdrWdDate;
	}

	public Date getChdrPoDate() {
		return chdrPoDate;
	}

	public void setChdrPoDate(Date chdrPoDate) {
		this.chdrPoDate = chdrPoDate;
	}

	public Date getChdrCfDate() {
		return chdrCfDate;
	}

	public void setChdrCfDate(Date chdrCfDate) {
		this.chdrCfDate = chdrCfDate;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

}