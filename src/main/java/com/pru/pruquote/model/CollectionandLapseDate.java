package com.pru.pruquote.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.pru.pruquote.property.ICollectionAndLapsed;

public class CollectionandLapseDate implements ICollectionAndLapsed {

	Date startdate ;
	Date enddate ;
	DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	public List<CollectionAndLapse> filterCollectionAndLapse(List<CollectionAndLapse> collectionAndLapse,
			String criteria, String operator) {
		try {
			startdate = format.parse(criteria);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if(operator.equals("<")) {
			return collectionAndLapse.stream().filter(s -> s.getChdrLapseDate().before(startdate)).collect(Collectors.toList());
		} else if(operator.equals(">")) {
			return collectionAndLapse.stream().filter(s -> s.getChdrLapseDate().after(startdate)).collect(Collectors.toList());
		} else if(operator.equals("<=")) {
			return collectionAndLapse.stream().filter(s -> s.getChdrLapseDate().before(startdate)).collect(Collectors.toList());
		} else if(operator.equals(">=")) {
			return collectionAndLapse.stream().filter(s -> s.getChdrLapseDate().after(startdate)).collect(Collectors.toList());
		} else  {
			return collectionAndLapse.stream().filter(s -> s.getChdrLapseDate().equals(startdate)).collect(Collectors.toList());
		}
	}

	@Override
	public List<CollectionAndLapse> filterCollectionAndLapse(List<CollectionAndLapse> collectionAndLapse,
			String criteria, String operator, String criterialcompare) {
		try {
			startdate = format.parse(criteria);
			enddate = format.parse(criterialcompare);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(criteria));
			c.add(Calendar.DATE, -1);
			startdate = format.parse(sdf.format(c.getTime()));
			
			c.setTime(sdf.parse(criterialcompare));
			c.add(Calendar.DATE, 1);
			enddate = format.parse(sdf.format(c.getTime()));
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return collectionAndLapse.stream().filter(s -> s.getChdrLapseDate().after(startdate) && s.getChdrLapseDate().before(enddate)).collect(Collectors.toList());
	}

}
