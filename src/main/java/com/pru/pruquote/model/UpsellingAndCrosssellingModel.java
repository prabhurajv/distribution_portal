package com.pru.pruquote.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Column;

public class UpsellingAndCrosssellingModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4455415747867943146L;
	
	@Column(name="\"obj_key\"")
	private String objKey;
	
	@Column(name="\"agntnum\"")
	private String agentNum;
	
	@Column(name="\"agnt_name\"")
	private String agentName;
	
	@Column(name="\"chdrnum\"")
	private String poNumber;
	
	@Column(name="\"cusname\"")
	private String cusName;
	
	@Column(name="\"cussex\"")
	private String cusSex;
	
	@Column(name="\"cus_mobile1\"")
	private String cusMobile1;
	
	@Column(name="\"cus_mobile2\"")
	private String cusMobile2;
	
	@Column(name="\"cusmstatus\"")
	private String cusMstatus;
	
	@Column(name="\"cusnochd\"")
	private String cusNoChild;
	
	@Column(name="\"cus_occu\"")
	private String cusOccupation;
	
	@Column(name="\"cussalarymonthly\"")
	private BigDecimal cusSalaryMonthly;
	
	@Column(name="\"rdate\"")
	private Date issDate;
	
	@Column(name="\"bdmcode\"")
	private String bdmCode;
	
	@Column(name="\"bdmname\"")
	private String bdmName;
	
	@Column(name="\"branch_code\"")
	private String branchCode;
	
	@Column(name="\"ape\"")
	private BigDecimal ape;
	
	@Column(name="\"sa\"")
	private BigDecimal sumAssure;
	
	@Column(name="\"chdr_status\"")
	private String chdrStatus;
	
	@Column(name="\"chdr_transfer_status\"")
	private String poTransferStatus;
	
	@Column(name="\"approach_status\"")
	private String approachStatus;
	
	@Column(name="\"approach_status_id\"")
	private String approachStatusId;
	
	@Column(name="\"uab\"")
	private BigDecimal uab;
	
	@Column(name="\"po_cus_age\"")
	private int poCusage;
	
	@Column(name="\"income_rank\"")
	private int incomeRank;
	
	@Column(name="\"rid\"")
	private BigDecimal rId;
	
	public static Comparator<UpsellingAndCrosssellingModel> AgentNumAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getAgentNum().compareToIgnoreCase(uac2.getAgentNum());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> AgentNameAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getAgentName().compareToIgnoreCase(uac2.getAgentName());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> PoNumberAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getPoNumber().compareToIgnoreCase(uac2.getPoNumber());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> CusNameAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getCusName().compareToIgnoreCase(uac2.getCusName());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> CusSexAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getCusSex().compareToIgnoreCase(uac2.getCusSex());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> CusMstatusAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getCusMstatus().compareToIgnoreCase(uac2.getCusMstatus());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> CusMobile1Asc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getCusMobile1().compareToIgnoreCase(uac2.getCusMobile1());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> CusMobile2Asc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getCusMobile2().compareToIgnoreCase(uac2.getCusMobile2());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> CusNoChildAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getCusNoChild().compareToIgnoreCase(uac2.getCusNoChild());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> CusOccupationAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getCusOccupation().compareToIgnoreCase(uac2.getCusOccupation());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> CusSalaryMonthlyAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getCusSalaryMonthly().toString().compareToIgnoreCase(uac2.getCusSalaryMonthly().toString());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> IssDateAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getIssDate().toString().compareToIgnoreCase(uac2.getIssDate().toString());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> BdmCodeAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getBdmCode().compareToIgnoreCase(uac2.getBdmCode());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> BdmNameAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getBdmCode().compareToIgnoreCase(uac2.getBdmCode());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> BranchCodeAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getBranchCode().compareToIgnoreCase(uac2.getBranchCode());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> ApeAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getApe().toString().compareToIgnoreCase(uac2.getApe().toString());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> SaAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getSumAssure().toString().compareToIgnoreCase(uac2.getSumAssure().toString());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> UabAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getUab().toString().compareToIgnoreCase(uac2.getUab().toString());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> poTransferAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getPoTransferStatus().compareToIgnoreCase(uac2.getPoTransferStatus());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> ApproachStatusAsc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getApproachStatus().compareToIgnoreCase(uac2.getApproachStatus());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> AgentNumDesc = new Comparator<UpsellingAndCrosssellingModel>(){

		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac2.getAgentName().compareToIgnoreCase(uac1.getAgentName());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> AgentNameDesc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getAgentName().compareToIgnoreCase(uac2.getAgentName());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> PoNumberDesc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getPoNumber().compareToIgnoreCase(uac2.getPoNumber());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> CusNameDesc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getCusName().compareToIgnoreCase(uac2.getCusName());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> CusSexDesc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getCusSex().compareToIgnoreCase(uac2.getCusSex());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> CusMstatusDesc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getCusMstatus().compareToIgnoreCase(uac2.getCusMstatus());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> CusMobile1Desc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getCusMobile1().compareToIgnoreCase(uac2.getCusMobile1());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> CusMobile2Desc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getCusMobile2().compareToIgnoreCase(uac2.getCusMobile2());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> IssDateDesc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getIssDate().toString().compareToIgnoreCase(uac2.getIssDate().toString());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> BdmCodeDesc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getBdmCode().compareToIgnoreCase(uac2.getBdmCode());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> BdmNameDesc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getBdmCode().compareToIgnoreCase(uac2.getBdmCode());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> BranchCodeDesc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getBranchCode().compareToIgnoreCase(uac2.getBranchCode());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> ApeDesc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getApe().toString().compareToIgnoreCase(uac2.getApe().toString());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> SaDesc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getSumAssure().toString().compareToIgnoreCase(uac2.getSumAssure().toString());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> UabDesc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getUab().toString().compareToIgnoreCase(uac2.getUab().toString());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> poTransferDesc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getPoTransferStatus().compareToIgnoreCase(uac2.getPoTransferStatus());
		}
		
	};
	
	public static Comparator<UpsellingAndCrosssellingModel> ApproachStatusDesc = new Comparator<UpsellingAndCrosssellingModel>() {
		@Override
		public int compare(UpsellingAndCrosssellingModel uac1, UpsellingAndCrosssellingModel uac2) {
			// TODO Auto-generated method stub
			return uac1.getApproachStatus().compareToIgnoreCase(uac2.getApproachStatus());
		}
		
	};
	
	public String getAgentNum() {
		return agentNum;
	}

	public void setAgentNum(String agentNum) {
		this.agentNum = agentNum;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getCusSex() {
		return cusSex;
	}

	public void setCusSex(String cusSex) {
		this.cusSex = cusSex;
	}

	public String getCusMstatus() {
		return cusMstatus;
	}

	public void setCusMstatus(String cusMstatus) {
		this.cusMstatus = cusMstatus;
	}

	public String getCusNoChild() {
		return cusNoChild;
	}

	public void setCusNoChild(String cusNoChild) {
		this.cusNoChild = cusNoChild;
	}

	public String getCusOccupation() {
		return cusOccupation;
	}

	public void setCusOccupation(String cusOccupation) {
		this.cusOccupation = cusOccupation;
	}


	public BigDecimal getCusSalaryMonthly() {
		return cusSalaryMonthly;
	}

	public void setCusSalaryMonthly(BigDecimal cusSalaryMonthly) {
		this.cusSalaryMonthly = cusSalaryMonthly;
	}

	
	public String getCusMobile1() {
		return cusMobile1;
	}

	public void setCusMobile1(String cusMobile1) {
		this.cusMobile1 = cusMobile1;
	}

	public String getCusMobile2() {
		return cusMobile2;
	}

	public void setCusMobile2(String cusMobile2) {
		this.cusMobile2 = cusMobile2;
	}

	public Date getIssDate() {
		return issDate;
	}

	public void setIssDate(Date issDate) {
		this.issDate = issDate;
	}

	public String getBdmCode() {
		return bdmCode;
	}

	public void setBdmCode(String bdmCode) {
		this.bdmCode = bdmCode;
	}

	public String getBdmName() {
		return bdmName;
	}

	public void setBdmName(String bdmName) {
		this.bdmName = bdmName;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public BigDecimal getApe() {
		return ape;
	}

	public void setApe(BigDecimal ape) {
		this.ape = ape;
	}
	
	public String getChdrStatus() {
		return chdrStatus;
	}

	public void setChdrStatus(String chdrStatus) {
		this.chdrStatus = chdrStatus;
	}

	public BigDecimal getSumAssure() {
		return sumAssure;
	}

	public void setSumAssure(BigDecimal sumAssure) {
		this.sumAssure = sumAssure;
	}

	public String getObjKey() {
		return objKey;
	}

	public void setObjKey(String objKey) {
		this.objKey = objKey;
	}


	public String getPoTransferStatus() {
		return poTransferStatus;
	}

	public void setPoTransferStatus(String poTransferStatus) {
		this.poTransferStatus = poTransferStatus;
	}

	public String getApproachStatus() {
		return approachStatus;
	}

	public void setApproachStatus(String approachStatus) {
		this.approachStatus = approachStatus;
	}

	public String getApproachStatusId() {
		return approachStatusId;
	}

	public void setApproachStatusId(String approachStatusId) {
		this.approachStatusId = approachStatusId;
	}

	public BigDecimal getUab() {
		return uab;
	}

	public void setUab(BigDecimal uab) {
		this.uab = uab;
	}

	public int getPoCusage() {
		return poCusage;
	}

	public void setPoCusage(int poCusage) {
		this.poCusage = poCusage;
	}

	public int getIncomeRank() {
		return incomeRank;
	}

	public void setIncomeRank(int incomeRank) {
		this.incomeRank = incomeRank;
	}

	public BigDecimal getrId() {
		return rId;
	}

	public void setrId(BigDecimal rId) {
		this.rId = rId;
	}

}
