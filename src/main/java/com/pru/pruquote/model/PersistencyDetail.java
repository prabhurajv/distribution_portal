package com.pru.pruquote.model;

import java.io.Serializable;

import com.pru.pruquote.utility.ExportBinding;

public class PersistencyDetail implements Serializable {

	private static final long serialVersionUID = 2260117902522764908L;
	private String obj_key;
	private String obj_type;

	@ExportBinding("Agent Code")
	private String agntnum;
	
	@ExportBinding("Agent Name")
	private String agntname;
	
	@ExportBinding("Supervisor/BD Name")
	private String supervisor;
	
	@ExportBinding("BDM Name")
	private String bdmname;
	
	@ExportBinding("SBDM Name")
	private String sbdmname;
	
	@ExportBinding("RBDM Name")
	private String rbdmname;

	@ExportBinding("# of Policy")
	private String chdrnum;
	
	@ExportBinding("Branch Name")
	private String branchname;
	
	@ExportBinding("# of App")
	private String chdrappnum;
	
	@ExportBinding("Client Name")
	private String clntname;
	
	@ExportBinding("Billing Frequency")
	private String billfreq;
	
	@ExportBinding("Standing Order Date")
	private String occdate;
	
	@ExportBinding("Premium")
	private String premium;
	
	@ExportBinding("Premium Due Date")
	private String duedate;
	
	@ExportBinding("APE")
	private String ape;
	
	@ExportBinding("Phone 1")
	private String phone1;
	@ExportBinding("Phone 2")
	private String phone2;
	
	private String for_q;

	public String getObj_key() {
		return obj_key;
	}

	public void setObj_key(String obj_key) {
		this.obj_key = obj_key;
	}

	public String getObj_type() {
		return obj_type;
	}

	public void setObj_type(String obj_type) {
		this.obj_type = obj_type;
	}

	public String getAgntnum() {
		return agntnum;
	}

	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}

	public String getAgntname() {
		return agntname;
	}

	public void setAgntname(String agntname) {
		this.agntname = agntname;
	}

	public String getBranchname() {
		return branchname;
	}

	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}

	public String getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public String getBdmname() {
		return bdmname;
	}

	public void setBdmname(String bdmname) {
		this.bdmname = bdmname;
	}

	public String getSbdmname() {
		return sbdmname;
	}

	public void setSbdmname(String sbdmname) {
		this.sbdmname = sbdmname;
	}

	public String getRbdmname() {
		return rbdmname;
	}

	public void setRbdmname(String rbdmname) {
		this.rbdmname = rbdmname;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getChdrappnum() {
		return chdrappnum;
	}

	public void setChdrappnum(String chdrappnum) {
		this.chdrappnum = chdrappnum;
	}

	public String getClntname() {
		return clntname;
	}

	public void setClntname(String clntname) {
		this.clntname = clntname;
	}

	public String getBillfreq() {
		return billfreq;
	}

	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	public String getDuedate() {
		return duedate;
	}

	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}

	public String getApe() {
		return ape;
	}

	public void setApe(String ape) {
		this.ape = ape;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getFor_q() {
		return for_q;
	}

	public void setFor_q(String for_q) {
		this.for_q = for_q;
	}
}
