package com.pru.pruquote.model;

import java.io.Serializable;
import java.util.Date;

public class PendingAck implements Serializable  {
	private static final long serialVersionUID = 2260117902522764908L;
	
	private String objKey;
	private String objType;
	private String chdrNum;
	private String poNnum;
	private String poName;
	private String clntMobile1;
	private String clntMobile2;
	private String supName;
	private String branchCode;
	private String branchName;
	private Integer PendingPeriod;
	private String chdrAppNum;
	private String agntNum;
	private String agntName;
	private Date dispatchDate;
	private Date chdrIssDate;
	private String agntPhone;
	
	public String getObjKey() {
		return objKey;
	}
	public void setObjKey(String objKey) {
		this.objKey = objKey;
	}
	public String getObjType() {
		return objType;
	}
	public void setObjType(String objType) {
		this.objType = objType;
	}
	public String getChdrNum() {
		return chdrNum;
	}
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}
	public String getPoNnum() {
		return poNnum;
	}
	public void setPoNnum(String poNnum) {
		this.poNnum = poNnum;
	}
	public String getPoName() {
		return poName;
	}
	public void setPoName(String poName) {
		this.poName = poName;
	}
	public String getClntMobile1() {
		return clntMobile1;
	}
	public void setClntMobile1(String clntMobile1) {
		this.clntMobile1 = clntMobile1;
	}
	public String getClntMobile2() {
		return clntMobile2;
	}
	public void setClntMobile2(String clntMobile2) {
		this.clntMobile2 = clntMobile2;
	}
	public String getSupName() {
		return supName;
	}
	public void setSupName(String supName) {
		this.supName = supName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public Integer getPendingPeriod() {
		return PendingPeriod;
	}
	public void setPendingPeriod(Integer pendingPeriod) {
		PendingPeriod = pendingPeriod;
	}
	public String getChdrAppNum() {
		return chdrAppNum;
	}
	public void setChdrAppNum(String chdrAppNum) {
		this.chdrAppNum = chdrAppNum;
	}
	public String getAgntNum() {
		return agntNum;
	}
	public void setAgntNum(String agntNum) {
		this.agntNum = agntNum;
	}
	public String getAgntName() {
		return agntName;
	}
	public void setAgntName(String agntName) {
		this.agntName = agntName;
	}
	public Date getDispatchDate() {
		return dispatchDate;
	}
	public void setDispatchDate(Date dispatchDate) {
		this.dispatchDate = dispatchDate;
	}
	public Date getChdrIssDate() {
		return chdrIssDate;
	}
	public void setChdrIssDate(Date chdrIssDate) {
		this.chdrIssDate = chdrIssDate;
	}
	public String getAgntPhone() {
		return agntPhone;
	}
	public void setAgntPhone(String agntPhone) {
		this.agntPhone = agntPhone;
	}
}
