package com.pru.pruquote.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name="\"ap\".sec_logs")
public class Log implements java.io.Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_logs_log_id_seq1") //Auto number primary key
	@SequenceGenerator(name="sec_logs_log_id_seq1", sequenceName=GlobalVar.db_name + ".sec_logs_log_id_seq1", allocationSize=1)
	@Column(name="\"log_id\"")
	private long id;
	
	@Column(name="\"user_id_pk\"")
	private long userIdPk;
	
	@Column(name="\"action\"")
	private String action;
	
	@Column(name="\"local_ip\"")
	private String ip;
	
	@Column(name="\"public_ip\"")
	private String pip;
	
	@Column(name="\"created_date\"")
	private Date logDate;

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserIdPk() {
		return this.userIdPk;
	}

	public void setUserIdPk(long userIdPk) {
		this.userIdPk = userIdPk;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPip() {
		return this.pip;
	}

	public void setPip(String pip) {
		this.pip = pip;
	}

	public Date getCreatedDate() {
		return this.logDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.logDate = createdDate;
	}

}