package com.pru.pruquote.model;

import java.util.List;
import java.util.stream.Collectors;

import com.pru.pruquote.property.IPendingCase;

public class PendingCasePolicyPhoneNumber implements IPendingCase {

	@Override
	public List<PendingCase> filterPendingCase(List<PendingCase> pendingCase, String criteria, String operator) {
		return pendingCase.stream().filter(s -> s.getPoPhoneNumber().startsWith(criteria)).collect(Collectors.toList());
	}

	@Override
	public List<PendingCase> filterPendingCase(List<PendingCase> pendingCase, String criteria, String operator,
			String criterialcompare) {
		return null;
	}

}
