package com.pru.pruquote.model;

import java.io.Serializable;
import java.util.Date;

public class Alteration implements Serializable{
	private static final long serialVersionUID = 2260117902522764908L;
	
	private String objKey;
	private String objType;
	private String chdrNum;
	private String poNum;
	private String poName;
	private String alterationType;
	private String status;
	private String pendingReason;
	private Date requestDate;
	private Date receivedDate;
	private Date transactionDate;
	private String agntNum;
	private String agntName;
	public String getObjKey() {
		return objKey;
	}
	public void setObjKey(String objKey) {
		this.objKey = objKey;
	}
	public String getObjType() {
		return objType;
	}
	public void setObjType(String objType) {
		this.objType = objType;
	}
	public String getChdrNum() {
		return chdrNum;
	}
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}
	public String getPoNum() {
		return poNum;
	}
	public void setPoNum(String poNum) {
		this.poNum = poNum;
	}
	public String getPoName() {
		return poName;
	}
	public void setPoName(String poName) {
		this.poName = poName;
	}
	public String getAlterationType() {
		return alterationType;
	}
	public void setAlterationType(String alterationType) {
		this.alterationType = alterationType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPendingReason() {
		return pendingReason;
	}
	public void setPendingReason(String pendingReason) {
		this.pendingReason = pendingReason;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public Date getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getAgntNum() {
		return agntNum;
	}
	public void setAgntNum(String agntNum) {
		this.agntNum = agntNum;
	}
	public String getAgntName() {
		return agntName;
	}
	public void setAgntName(String agntName) {
		this.agntName = agntName;
	}
	
	
}