package com.pru.pruquote.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.pru.pruquote.property.IPendingCase;

public class PendingCaseChdrKeyDate implements IPendingCase {
	Date startdate ;
	Date enddate ;
	DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	public List<PendingCase> filterPendingCase(List<PendingCase> pendingCase, String criteria, String operator) {
		try {
			startdate = format.parse(criteria);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return pendingCase.stream().filter(s -> s.getChdrKeyDate().equals(startdate)).collect(Collectors.toList());
	}

	@Override
	public List<PendingCase> filterPendingCase(List<PendingCase> pendingCase, String criteria, String operator,
			String criterialcompare) {
		try {
			enddate = format.parse(criterialcompare);
			startdate = format.parse(criteria);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return pendingCase.stream().filter(s -> s.getChdrKeyDate().after(startdate) && s.getChdrKeyDate().before(enddate)).collect(Collectors.toList());
	}

}
