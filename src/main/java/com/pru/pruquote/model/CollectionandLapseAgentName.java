package com.pru.pruquote.model;

import java.util.List;
import java.util.stream.Collectors;

import com.pru.pruquote.property.ICollectionAndLapsed;

public class CollectionandLapseAgentName implements ICollectionAndLapsed {

	@Override
	public List<CollectionAndLapse> filterCollectionAndLapse(List<CollectionAndLapse> collectionAndLapse,
			String criteria, String operator) {
		// TODO Auto-generated method stub
		return collectionAndLapse.stream().filter(s -> s.getAgntName().contains(criteria)).collect(Collectors.toList());
	}

	@Override
	public List<CollectionAndLapse> filterCollectionAndLapse(List<CollectionAndLapse> collectionAndLapse,
			String criteria, String operator, String criterialcompare) {
		// TODO Auto-generated method stub
		return null;
	}

}
