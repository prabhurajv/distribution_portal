package com.pru.pruquote.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Persistency implements Serializable {
	private static final long serialVersionUID = 2260117902522764908L;
	
	private String objKey;
	private String objType;
	private String agntNum;
	private String agntName;
	private String channel;
	private String ssCode;
	private String agntRegion;
	private Date agntJoinDate;
	private BigDecimal ifApe;
	private BigDecimal laApe;
	private BigDecimal suApe;
	private String persistencyForQ;
	private BigDecimal percentForQ;
	public String getObjKey() {
		return objKey;
	}
	public void setObjKey(String objKey) {
		this.objKey = objKey;
	}
	public String getObjType() {
		return objType;
	}
	public void setObjType(String objType) {
		this.objType = objType;
	}
	public String getAgntNum() {
		return agntNum;
	}
	public void setAgntNum(String agntNum) {
		this.agntNum = agntNum;
	}
	public String getAgntName() {
		return agntName;
	}
	public void setAgntName(String agntName) {
		this.agntName = agntName;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getSsCode() {
		return ssCode;
	}
	public void setSsCode(String ssCode) {
		this.ssCode = ssCode;
	}
	public String getAgntRegion() {
		return agntRegion;
	}
	public void setAgntRegion(String agntRegion) {
		this.agntRegion = agntRegion;
	}
	public Date getAgntJoinDate() {
		return agntJoinDate;
	}
	public void setAgntJoinDate(Date agntJoinDate) {
		this.agntJoinDate = agntJoinDate;
	}
	public BigDecimal getIfApe() {
		return ifApe;
	}
	public void setIfApe(BigDecimal ifApe) {
		this.ifApe = ifApe;
	}
	public BigDecimal getLaApe() {
		return laApe;
	}
	public void setLaApe(BigDecimal laApe) {
		this.laApe = laApe;
	}
	public BigDecimal getSuApe() {
		return suApe;
	}
	public void setSuApe(BigDecimal suApe) {
		this.suApe = suApe;
	}
	public String getPersistencyForQ() {
		return persistencyForQ;
	}
	public void setPersistencyForQ(String persistencyForQ) {
		this.persistencyForQ = persistencyForQ;
	}
	public BigDecimal getPercentForQ() {
		return percentForQ;
	}
	public void setPercentForQ(BigDecimal percentForQ) {
		this.percentForQ = percentForQ;
	}
}