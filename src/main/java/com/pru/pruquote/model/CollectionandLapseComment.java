package com.pru.pruquote.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name = "tabcomment", catalog= GlobalVar.db_name)
public class CollectionandLapseComment implements Serializable {
private static final long serialVersionUID = 2260117902522764908L;
	
	@Column(name="cc_id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String ccId;
	
	@Column(name = "tab_key")
	private String tabKey;
	
	@Column(name = "obj_type")
	private String objType;
	
	@Column(name = "cc_date")
	private Date ccDate;
	
	@Column(name = "cc_text")
	private String ccText;
	
	@Column(name = "cc_by")
	private String ccBy;
	
	@Transient
	private String ccByName;
	
	public String getCcId() {
		return ccId;
	}
	public void setCcId(String ccId) {
		this.ccId = ccId;
	}
	public String getTabKey() {
		return tabKey;
	}
	public void setTabKey(String tabKey) {
		this.tabKey = tabKey;
	}
	public String getObjType() {
		return objType;
	}
	public void setObjType(String objType) {
		this.objType = objType;
	}
	public Date getCcDate() {
		return ccDate;
	}
	public void setCcDate(Date ccDate) {
		this.ccDate = ccDate;
	}
	public String getCcText() {
		return ccText;
	}
	public void setCcText(String ccText) {
		this.ccText = ccText;
	}
	public String getCcBy() {
		return ccBy;
	}
	public void setCcBy(String ccBy) {
		this.ccBy = ccBy;
	}
	public String getCcByName() {
		return ccByName;
	}
	public void setCcByName(String ccByName) {
		this.ccByName = ccByName;
	}
}