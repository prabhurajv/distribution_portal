package com.pru.pruquote.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name="tabreferal", catalog = GlobalVar.db_name)
public class Referral implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7005952142797631161L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "referral_rid") //Auto number primary key
	@SequenceGenerator(name="referral_rid", sequenceName=GlobalVar.db_name + ".referral_rid", allocationSize=1)
	@Column(name = "\"rid\"", unique = true, nullable = false)
	private long rID;
	
	@Column(name="\"cusname\"")
	private String cusName;
	
	@Column(name="\"cussex\"")
	private String cusSex;
	
	@Column(name="\"cusage\"")
	private String cusAge;
	
	@Column(name="\"cusmstatus\"")
	private String cusMStatus;
	
	@Column(name="\"cusnochd\"")
	private String cusNochd;
	
	@Column(name="\"cusphone\"")
	private String cusPhone;
	
	@Column(name="\"cusfacebook\"")
	private String cusFacebook;

	@Column(name="\"cussalarymonthly\"")
	private Float cusSalaryMonthly;

	@Column(name="\"cussalaryyearly\"")
	private Float cusSalaryYearly;
	
	@Column(name="\"cusafpremium\"")
	private Float cusAFPremium;
	
	@Column(name="\"rtype\"")
	private String rType;
	
	@Column(name="\"rdate\"",nullable=true)
	private Date rDate;
	
	@Column(name="\"rremark\"")
	private String rRemark;
	
	@Column(name="\"sucappstatus\"")
	private String sucAppStatus;
	
	@Column(name="\"sucappdate\"",nullable=true)
	private Date sucAppDate;
	
	@Column(name="\"sucappremark\"")
	private String sucAppRemark;
	
	@Column(name="\"clsbanksostatus\"")
	private String clsBankSoStatus;
	
	@Column(name="\"clsbanksodate\"",nullable=true)
	private Date clsBankSoDate;

	@Column(name="\"clsbankappnum\"")
	private String clsBankAppNum;
	
	@Column(name="\"clsbankuploadstatus\"")
	private String clsBankUploadStatus;
	
	@Column(name="\"clsbankuploaddate\"",nullable=true)
	private Date clsBankUploadDate;
	
	@Column(name="\"clsbankuploadremark\"")
	private String clsBankUploadRemark;
	
	@Column(name="\"ape\"")
	private double APE;
	
	@Column(name="\"exiscusstatus\"")
	private String exisCusStatus;
	
	@Column(name="\"referralby\"")
	private String referralBy;
	
	@Column(name="\"appdate\"",nullable=true)
	private Date appDate;
	
	@Column(name="\"flwupcmttypelast\"")
	private String flwupCmtTypeLast;
	
	@Column(name="\"flwupcmtremarklast\"")
	private String flwupCmtRemarkLast;
	
	@Column(name="\"orgagntnum\"")
	private String orgAgntNum;
	
	@Column(name="\"orgbranch\"")
	private String orgBranch;
	
	@Column(name="\"agntnum\"")
	private String agntNum;
	
	@Column(name="\"validflag\"")
	private String validFlag;
	
	@Column(name="\"trndatetime\"")
	private Date trnDateTime;
	
	@Column(name="\"trnby\"")
	private String trnBy;
	
	@Column(name="\"lasttrndatetime\"")
	private Date lastTrnDateTime;
	
	@Column(name="\"lasttrnby\"")
	private String lastTrnBy;
	
	@Column(name="\"ip\"")
	private String IP;
	
	@Column(name="\"pip\"")
	private String pIP;

	@Column(name="\"cusagechd\"")
	private int cusAgeChd;
	
	@Column(name="\"cusphone2\"")
	private String cusPhone2;
	
	@Column(name="\"cusocc\"")
	private int cusOcc;
	
	@Column(name="\"cusemail\"")
	private String cusEmail;
	
	@Column(name="\"cusaprstatus\"")
	private String cusAprStatus;
	
	@Column(name="\"cusaprdate\"", nullable=true)
	private Date cusAprDate;
	
	@Column(name="\"salsucsalprestatus\"")
	private String salSucSalPreStatus;
	
	@Column(name="\"salsucsalpredate\"", nullable=true)
	private Date salSucSalPreDate;
	
	@Column(name="\"saltyprecprd\"")
	private String salTypRecPrd;
	
	@Column(name="\"saldurpre\"")
	private int salDurPre;
	
	@Column(name="\"salrecaffape\"")
	private Float salRecAffApe;
	
	@Column(name="\"salmthbasexp\"")
	private int salMthBasExp;
	
	@Column(name="\"salrecsa\"")
	private int salRecSA;
	
	@Column(name="\"salrecpolterm\"")
	private String salRecPolTerm;
	
	@Column(name="\"salrecprdpkgusd\"")
	private int salRecPrdPkgUsd;
	
	@Column(name="\"salpolterm\"")
	private int salPolTerm;
	
	@Column(name="\"salepcclssal\"")
	private int salEpcClsSal;
	
	@Column(name="\"salpolmode\"")
	private String salPolMode;
	
	@Column(name="\"salepcclsperiod\"")
	private int salEpcClsPeriod;
	
	@Column(name="\"salflwupnxtdate\"",nullable=true)
	private Date salFlwupNxtDate;
	
	@Column(name="\"salcusansdetail\"")
	private String salCusAnsDetail;
	
	@Column(name="\"flwuptrndate\"",nullable=true)
	private Date flwupTrnDate;
	
	@Column(name="\"clsprdtype\"")
	private String clsPrdType;
	
	@Column(name="\"clspolterm\"")
	private String clsPolTerm;
	
	@Column(name="\"clspolmode\"")
	private String clsPolMode;
	
	@Column(name = "\"customerfrom\"")
	private String customerFrom;
	
	@Column(name = "\"cdate\"")
	private Date cdate;
	
	@Column(name = "\"closed\"")
	private boolean closed;
	
	@Column(name = "\"draft\"")
	private String draft;
	
	@OneToMany(mappedBy = "referral")
	@Cascade({CascadeType.SAVE_UPDATE})
	private List<ReferralAcbProduct> referralAcbProducts;
	
	public long getrID() {
		return rID;
	}

	public void setrID(long rID) {
		this.rID = rID;
	}

	public String getCusName() {
		if(cusName == null){
			cusName = "";
		}
		
		return cusName.trim();
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getCusSex() {
		if(cusSex == null){
			cusSex = "";
		}
		
		return cusSex.trim();
	}

	public void setCusSex(String cusSex) {
		this.cusSex = cusSex;
	}

	public String getCusAge() {
		if(cusAge == null){
			cusAge = "";
		}
		
		return cusAge.trim();
	}

	public void setCusAge(String cusAge) {
		this.cusAge = cusAge;
	}

	public String getCusMStatus() {
		if(cusMStatus == null){
			cusMStatus = "";
		}
		
		return cusMStatus.trim();
	}

	public void setCusMStatus(String cusMStatus) {
		this.cusMStatus = cusMStatus;
	}

	public String getCusNochd() {
		if(cusNochd == null){
			cusNochd = "";
		}
		
		return cusNochd.trim();
	}

	public void setCusNochd(String cusNochd) {
		this.cusNochd = cusNochd;
	}

	public String getCusPhone() {
		if(cusPhone == null){
			cusPhone = "";
		}
		
		return cusPhone.trim();
	}

	public void setCusPhone(String cusPhone) {
		this.cusPhone = cusPhone;
	}

	public String getCusFacebook() {
		if(cusFacebook == null){
			cusFacebook = "";
		}
		
		return cusFacebook.trim();
	}

	public void setCusFacebook(String cusFacebook) {
		this.cusFacebook = cusFacebook;
	}

	public Float getCusSalaryMonthly() {
		return cusSalaryMonthly;
	}

	public void setCusSalaryMonthly(Float cusSalaryMonthly) {
		this.cusSalaryMonthly = cusSalaryMonthly;
	}

	public Float getCusSalaryYearly() {
		return cusSalaryYearly;
	}

	public void setCusSalaryYearly(Float cusSalaryYearly) {
		this.cusSalaryYearly = cusSalaryYearly;
	}

	public Float getCusAFPremium() {
		return cusAFPremium;
	}

	public void setCusAFPremium(Float cusAFPremium) {
		this.cusAFPremium = cusAFPremium;
	}

	public String getrType() {
		if(rType == null){
			rType = "";
		}
		
		return rType.trim();
	}

	public void setrType(String rType) {
		this.rType = rType;
	}

	public Date getrDate() {
		return rDate;
	}

	public void setrDate(Date rDate) {
		this.rDate = rDate;
	}

	public String getrRemark() {
		if(rRemark == null){
			rRemark = "";
		}
		
		return rRemark.trim();
	}

	public void setrRemark(String rRemark) {
		this.rRemark = rRemark;
	}

	public String getSucAppStatus() {
		if(sucAppStatus == null){
			sucAppStatus = "";
		}
		
		return sucAppStatus.trim();
	}

	public void setSucAppStatus(String sucAppStatus) {
		this.sucAppStatus = sucAppStatus;
	}

	public Date getSucAppDate() {
		return sucAppDate;
	}

	public void setSucAppDate(Date sucAppDate) {
		this.sucAppDate = sucAppDate;
	}

	public String getSucAppRemark() {
		if(sucAppRemark == null){
			sucAppRemark = "";
		}
		
		return sucAppRemark.trim();
	}

	public void setSucAppRemark(String sucAppRemark) {
		this.sucAppRemark = sucAppRemark;
	}


	public String getClsBankSoStatus() {
		if(clsBankSoStatus == null){
			clsBankSoStatus = "";
		}
		
		return clsBankSoStatus.trim();
	}

	public void setClsBankSoStatus(String clsBankSoStatus) {
		this.clsBankSoStatus = clsBankSoStatus;
	}

	public Date getClsBankSoDate() {
		return clsBankSoDate;
	}

	public void setClsBankSoDate(Date clsBankSoDate) {
		this.clsBankSoDate = clsBankSoDate;
	}

	public String getClsBankAppNum() {
		if(clsBankAppNum == null){
			clsBankAppNum = "";
		}
		
		return clsBankAppNum.trim();
	}

	public void setClsBankAppNum(String clsBankAppNum) {
		this.clsBankAppNum = clsBankAppNum;
	}

	public String getClsBankUploadStatus() {
		if(clsBankUploadStatus == null){
			clsBankUploadStatus = "";
		}
		
		return clsBankUploadStatus.trim();
	}

	public void setClsBankUploadStatus(String clsBankUploadStatus) {
		this.clsBankUploadStatus = clsBankUploadStatus;
	}

	public Date getClsBankUploadDate() {
		return clsBankUploadDate;
	}

	public void setClsBankUploadDate(Date clsBankUploadDate) {
		this.clsBankUploadDate = clsBankUploadDate;
	}

	public String getClsBankUploadRemark() {
		if(clsBankUploadRemark == null){
			clsBankUploadRemark = "";
		}
		
		return clsBankUploadRemark.trim();
	}

	public void setClsBankUploadRemark(String clsBankUploadRemark) {
		this.clsBankUploadRemark = clsBankUploadRemark;
	}

	public double getAPE() {
		return APE;
	}

	public void setAPE(double aPE) {
		APE = aPE;
	}

	public String getExisCusStatus() {
		if(exisCusStatus == null){
			exisCusStatus = "";
		}
		
		return exisCusStatus.trim();
	}

	public void setExisCusStatus(String exisCusStatus) {
		this.exisCusStatus = exisCusStatus;
	}

	public String getReferralBy() {
		if(referralBy == null){
			referralBy = "";
		}
		
		return referralBy.trim();
	}

	public void setReferralBy(String referralBy) {
		this.referralBy = referralBy;
	}

	public Date getAppDate() {
		return appDate;
	}

	public void setAppDate(Date appDate) {
		this.appDate = appDate;
	}

	public String getFlwupCmtTypeLast() {
		if(flwupCmtTypeLast == null){
			flwupCmtTypeLast = "";
		}
		
		return flwupCmtTypeLast.trim();
	}

	public void setFlwupCmtTypeLast(String flwupCmtTypeLast) {
		this.flwupCmtTypeLast = flwupCmtTypeLast;
	}

	public String getFlwupCmtRemarkLast() {
		if(flwupCmtRemarkLast == null){
			flwupCmtRemarkLast = "";
		}
		
		return flwupCmtRemarkLast.trim();
	}

	public void setFlwupCmtRemarkLast(String flwupCmtRemarkLast) {
		this.flwupCmtRemarkLast = flwupCmtRemarkLast;
	}

	public String getOrgAgntNum() {
		if(orgAgntNum == null){
			orgAgntNum = "";
		}
		
		return orgAgntNum.trim();
	}

	public void setOrgAgntNum(String orgAgntNum) {
		this.orgAgntNum = orgAgntNum;
	}

	public String getOrgBranch() {
		if(orgBranch == null){
			orgBranch = "";
		}
		
		return orgBranch.trim();
	}

	public void setOrgBranch(String orgBranch) {
		this.orgBranch = orgBranch;
	}

	public String getAgntNum() {
		if(agntNum == null){
			agntNum = "";
		}
		
		return agntNum.trim();
	}

	public void setAgntNum(String agntNum) {
		this.agntNum = agntNum;
	}

	public String getValidFlag() {
		if(validFlag == null){
			validFlag = "";
		}
		
		return validFlag.trim();
	}

	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}

	public Date getTrnDateTime() {
		return trnDateTime;
	}

	public void setTrnDateTime(Date trnDateTime) {
		this.trnDateTime = trnDateTime;
	}

	public String getTrnBy() {
		if(trnBy == null){
			trnBy = "";
		}
		
		return trnBy.trim();
	}

	public void setTrnBy(String trnBy) {
		this.trnBy = trnBy;
	}

	public Date getLastTrnDateTime() {
		return lastTrnDateTime;
	}

	public void setLastTrnDateTime(Date lastTrnDateTime) {
		this.lastTrnDateTime = lastTrnDateTime;
	}

	public String getLastTrnBy() {
		if(lastTrnBy == null){
			lastTrnBy = "";
		}
		
		return lastTrnBy.trim();
	}

	public void setLastTrnBy(String lastTrnBy) {
		this.lastTrnBy = lastTrnBy;
	}

	public String getIP() {
		if(IP == null){
			IP = "";
		}
		
		return IP.trim();
	}

	public void setIP(String iP) {
		IP = iP;
	}

	public String getpIP() {
		if(pIP == null){
			pIP = "";
		}
		
		return pIP.trim();
	}

	public void setpIP(String pIP) {
		this.pIP = pIP;
	}

	public int getCusAgeChd() {
		return cusAgeChd;
	}

	public void setCusAgeChd(int cusAgeChd) {
		this.cusAgeChd = cusAgeChd;
	}

	public String getCusPhone2() {
		if(cusPhone2 == null){
			cusPhone2 = "";
		}
		
		return cusPhone2.trim();
	}

	public void setCusPhone2(String cusPhone2) {
		this.cusPhone2 = cusPhone2;
	}

	public int getCusOcc() {
		return cusOcc;
	}

	public void setCusOcc(int cusOcc) {
		this.cusOcc = cusOcc;
	}

	public String getCusEmail() {
		if(cusEmail == null){
			cusEmail = "";
		}
		
		return cusEmail.trim();
	}

	public void setCusEmail(String cusEmail) {
		this.cusEmail = cusEmail;
	}

	public String getCusAprStatus() {
		if(cusAprStatus == null){
			cusAprStatus = "";
		}
		
		return cusAprStatus.trim();
	}

	public void setCusAprStatus(String cusAprStatus) {
		this.cusAprStatus = cusAprStatus;
	}

	public Date getCusAprDate() {
		return cusAprDate;
	}

	public void setCusAprDate(Date cusAprDate) {
		this.cusAprDate = cusAprDate;
	}

	public String getSalSucSalPreStatus() {
		if(salSucSalPreStatus == null){
			salSucSalPreStatus = "";
		}
		
		return salSucSalPreStatus.trim();
	}

	public void setSalSucSalPreStatus(String salSucSalPreStatus) {
		this.salSucSalPreStatus = salSucSalPreStatus;
	}

	public Date getSalSucSalPreDate() {
		return salSucSalPreDate;
	}

	public void setSalSucSalPreDate(Date salSucSalPreDate) {
		this.salSucSalPreDate = salSucSalPreDate;
	}

	public String getSalTypRecPrd() {
		if(salTypRecPrd == null){
			salTypRecPrd = "";
		}
		
		return salTypRecPrd.trim();
	}

	public void setSalTypRecPrd(String salTypRecPrd) {
		this.salTypRecPrd = salTypRecPrd;
	}

	public int getSalDurPre() {
		return salDurPre;
	}

	public void setSalDurPre(int salDurPre) {
		this.salDurPre = salDurPre;
	}

	public Float getSalRecAffApe() {
		return salRecAffApe;
	}

	public void setSalRecAffApe(Float salRecAffApe) {
		this.salRecAffApe = salRecAffApe;
	}

	public int getSalMthBasExp() {
		return salMthBasExp;
	}

	public void setSalMthBasExp(int salMthBasExp) {
		this.salMthBasExp = salMthBasExp;
	}

	public int getSalRecSA() {
		return salRecSA;
	}

	public void setSalRecSA(int salRecSA) {
		this.salRecSA = salRecSA;
	}

	public String getSalRecPolTerm() {
		if(salRecPolTerm == null){
			salRecPolTerm = "";
		}
		
		return salRecPolTerm.trim();
	}

	public void setSalRecPolTerm(String salRecPolTerm) {
		this.salRecPolTerm = salRecPolTerm;
	}

	public int getSalRecPrdPkgUsd() {
		return salRecPrdPkgUsd;
	}

	public void setSalRecPrdPkgUsd(int salRecPrdPkgUsd) {
		this.salRecPrdPkgUsd = salRecPrdPkgUsd;
	}

	public int getSalPolTerm() {
		return salPolTerm;
	}

	public void setSalPolTerm(int salPolTerm) {
		this.salPolTerm = salPolTerm;
	}

	public int getSalEpcClsSal() {
		return salEpcClsSal;
	}

	public void setSalEpcClsSal(int salEpcClsSal) {
		this.salEpcClsSal = salEpcClsSal;
	}

	public String getSalPolMode() {
		if(salPolMode == null){
			salPolMode = "";
		}
		
		return salPolMode.trim();
	}

	public void setSalPolMode(String salPolMode) {
		this.salPolMode = salPolMode;
	}

	public int getSalEpcClsPeriod() {
		return salEpcClsPeriod;
	}

	public void setSalEpcClsPeriod(int salEpcClsPeriod) {
		this.salEpcClsPeriod = salEpcClsPeriod;
	}

	public Date getSalFlwupNxtDate() {
		return salFlwupNxtDate;
	}

	public void setSalFlwupNxtDate(Date salFlwupNxtDate) {
		this.salFlwupNxtDate = salFlwupNxtDate;
	}

	public String getSalCusAnsDetail() {
		if(salCusAnsDetail == null){
			salCusAnsDetail = "";
		}
		
		return salCusAnsDetail.trim();
	}

	public void setSalCusAnsDetail(String salCusAnsDetail) {
		this.salCusAnsDetail = salCusAnsDetail;
	}

	public Date getFlwupTrnDate() {
		return flwupTrnDate;
	}

	public void setFlwupTrnDate(Date flwupTrnDate) {
		this.flwupTrnDate = flwupTrnDate;
	}

	public String getClsPrdType() {
		if(clsPrdType == null){
			clsPrdType = "";
		}
		
		return clsPrdType.trim();
	}

	public void setClsPrdType(String clsPrdType) {
		this.clsPrdType = clsPrdType;
	}

	public String getClsPolTerm() {
		if(clsPolTerm == null){
			clsPolTerm = "";
		}
		
		return clsPolTerm.trim();
	}

	public void setClsPolTerm(String clsPolTerm) {
		this.clsPolTerm = clsPolTerm;
	}

	public String getClsPolMode() {
		if(clsPolMode == null){
			clsPolMode = "";
		}
		
		return clsPolMode.trim();
	}

	public void setClsPolMode(String clsPolMode) {
		this.clsPolMode = clsPolMode;
	}

	public String getCustomerFrom() {
		return customerFrom;
	}

	public void setCustomerFrom(String customerFrom) {
		this.customerFrom = customerFrom;
	}

	public Date getCdate() {
		return cdate;
	}

	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}

	public boolean isClosed() {
		return closed;	
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
	}

	public String getDraft() {
		return draft;
	}

	public void setDraft(String draft) {
		this.draft = draft;
	}

	public List<ReferralAcbProduct> getReferralAcbProducts() {
		return referralAcbProducts;
	}

	public void setReferralAcbProducts(List<ReferralAcbProduct> referralAcbProducts) {
		this.referralAcbProducts = referralAcbProducts;
	}
	
	public List<Integer> acbPrdIds(){
		List<Integer> list = new ArrayList<Integer>();
		for(ReferralAcbProduct referralAcbProduct : referralAcbProducts){
			if(referralAcbProduct.getValidFlag().trim().equals("1")) 
				list.add(referralAcbProduct.getAcbPrdId());
		}
		
		return list;
	}
}
