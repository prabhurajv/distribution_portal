package com.pru.pruquote.model;

import java.util.List;
import java.util.stream.Collectors;

import com.pru.pruquote.property.IPendingCase;

public class PendingCasePendingPeriod implements IPendingCase {

	@Override
	public List<PendingCase> filterPendingCase(List<PendingCase> pendingCase, String criteria, String operator) {
		int criteriaint = Integer.parseInt(criteria);
		if(operator.equals("<")) {
			return pendingCase.stream().filter(s -> s.getPendingPeriod() < criteriaint).collect(Collectors.toList());
		} else if(operator.equals(">")) {
			return pendingCase.stream().filter(s -> s.getPendingPeriod() > criteriaint).collect(Collectors.toList());
		} else if(operator.equals("<=")) {
			return pendingCase.stream().filter(s -> s.getPendingPeriod() <= criteriaint).collect(Collectors.toList());
		} else if(operator.equals(">=")) {
			return pendingCase.stream().filter(s -> s.getPendingPeriod() >= criteriaint).collect(Collectors.toList());
		} else  {
			return pendingCase.stream().filter(s -> s.getPendingPeriod() == (criteriaint)).collect(Collectors.toList());
		}
	}

	@Override
	public List<PendingCase> filterPendingCase(List<PendingCase> pendingCase, String criteria, String operator,
			String criterialcompare) {
		int criteriato = Integer.parseInt(criteria);
		int criteriaend = Integer.parseInt(criterialcompare);
		return pendingCase.stream().filter(s -> s.getPendingPeriod()<= criteriato && s.getPendingPeriod() >= criteriaend).collect(Collectors.toList());
	}

}
