package com.pru.pruquote.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name = "tabcodedesc", catalog = GlobalVar.db_name)
public class CodeDesc implements Serializable {
	private static final long serialVersionUID = 2260117902522764908L;
	
	@Column(name = "id")
	@Id
	private int id;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "codedesc")
	private String codeDesc;
	
	@Column(name = "codedetail")
	private String codeDetail;
	
	@Column(name = "validflag")
	private String validFlag;
	
	@Column(name = "createduserid")
	private String createdUserId;
	
	@Column(name = "createddatetime")
	private Date createdDateTime;
	
	@Column(name = "modifieduserid")
	private String modifiedUserId;
	
	@Column(name = "modifieddatetime")
	private Date modifiedDatetime;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCodeDesc() {
		return codeDesc;
	}

	public void setCodeDesc(String codeDesc) {
		this.codeDesc = codeDesc;
	}

	public String getCodeDetail() {
		return codeDetail;
	}

	public void setCodeDetail(String codeDetail) {
		this.codeDetail = codeDetail;
	}

	public String getValidFlag() {
		return validFlag;
	}

	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}

	public String getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getModifiedUserId() {
		return modifiedUserId;
	}

	public void setModifiedUserId(String modifiedUserId) {
		this.modifiedUserId = modifiedUserId;
	}

	public Date getModifiedDatetime() {
		return modifiedDatetime;
	}

	public void setModifiedDatetime(Date modifiedDatetime) {
		this.modifiedDatetime = modifiedDatetime;
	}
	
	
}