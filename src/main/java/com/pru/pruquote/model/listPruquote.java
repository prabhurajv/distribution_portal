package com.pru.pruquote.model;

// Generated Nov 13, 2014 3:46:18 PM by Hibernate Tools 3.6.0

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

import com.pru.pruquote.utility.GlobalVar;

/**
 * VpruquoteParmId generated by hbm2java
 */
//@Table(name = "vpruquote_parms", catalog = GlobalVar.db_name)

@Table(name = "listPruquote")
@NamedStoredProcedureQueries({
   @NamedStoredProcedureQuery(name = "spList_Quote", 
                              procedureName = "spList_Quote",
                              parameters = {
                                 @StoredProcedureParameter(mode = ParameterMode.IN, name = "userid", type = String.class),
                                 @StoredProcedureParameter(mode = ParameterMode.IN, name = "ID", type = String.class)
                              })
})
@Entity
public class listPruquote implements java.io.Serializable {

	private long id;
	private String product;
	private String productDesc;
	private Date commDate;
	private String la1name;
	private String policyTerm;
	private String paymentType2;
	private String paymentMode2;
	private BigDecimal basicPlanSa;
	private String coUser;
	private String fullName;
	private Date synDate; 
	
	
	public listPruquote() {
		
	}
	
	
	
	@Id
	@Column(name = "\"ID\"", nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "\"Product\"", length = 4)
	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}
	
	@Column(name = "\"ProductDesc\"", length = 50)
	public String getProductDesc() {
		return this.productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	@Column(name = "\"CommDate\"", nullable = false, length = 19)
	public Date getCommDate() {
		return this.commDate;
	}

	public void setCommDate(Date commDate) {
		this.commDate = commDate;
	}

	@Column(name = "\"LA1Name\"", nullable = false, length = 250)
	public String getLa1name() {
		return this.la1name;
	}

	public void setLa1name(String la1name) {
		this.la1name = la1name;
	}
	
	@Column(name = "\"PolicyTerm\"", nullable = false, length = 250)
	public String getPolicyTerm() {
		return this.policyTerm;
	}

	public void setPolicyTerm(String policyTerm) {
		this.policyTerm = policyTerm;
	}

	@Column(name = "\"PaymentType2\"", length = 250)
	public String getPaymentType2() {
		return this.paymentType2;
	}

	public void setPaymentType2(String paymentType2) {
		this.paymentType2 = paymentType2;
	}

	@Column(name = "\"PaymentMode2\"", length = 250)
	public String getPaymentMode2() {
		return this.paymentMode2;
	}

	public void setPaymentMode2(String paymentMode2) {
		this.paymentMode2 = paymentMode2;
	}

	@Column(name = "\"BasicPlan_SA\"", nullable = false, precision = 18)
	public BigDecimal getBasicPlanSa() {
		return this.basicPlanSa;
	}

	public void setBasicPlanSa(BigDecimal basicPlanSa) {
		this.basicPlanSa = basicPlanSa;
	}

	@Column(name = "\"CoUser\"", length = 45)
	public String getCoUser() {
		return this.coUser;
	}

	public void setCoUser(String coUser) {
		this.coUser = coUser;
	}
	
	@Column(name = "\"FullName\"", length = 45)
	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	@Column(name = "\"SynDate\"", length = 19)
	public Date getSynDate() {
		return this.synDate;
	}

	public void setSynDate(Date synDate) {
		this.synDate = synDate;
	}
}
