package com.pru.pruquote.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name = "\"tabreferrer\"", catalog = GlobalVar.db_name)
public class Referrer implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -392486195847478706L;

	@Id
	@Column(name = "\"id\"")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "referrer_rid")
	@SequenceGenerator(name = "referrer_rid", sequenceName = GlobalVar.db_name + ".referrer_rid", allocationSize = 1)
	private int id;
	
	@Column(name = "\"staffid\"")
	private String staffId;
	
	@Column(name = "\"staffname\"")
	private String staffName;
	
	@Column(name = "\"staffbranch\"")
	private String staffBranch;
	
	@Column(name = "\"staffposition\"")
	private String staffPosition;
	
	@Column(name = "\"validflag\"")
	private int validFlag = 1;
	
	@Column(name = "\"iscertified\"")
	private int isCertified = 1;
	
	@Transient
	private boolean valid = true;
	
	@Column(name = "\"trnby\"")
	private String trnBy;
	
	@Column(name = "\"trndate\"")
	private Date trnDate;
	
	@Column(name = "\"employee_status\"")
	private int status = 1;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStaffId() {
		if(staffId != null){
			return staffId.trim();
		}
		return staffId;
	}
	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}
	public String getStaffName() {
		if(staffName != null){
			return staffName.trim();
		}
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getStaffPosition() {
		if(staffPosition != null){
			return staffPosition.trim();
		}
		return staffPosition;
	}
	public void setStaffPosition(String staffPosition) {
		this.staffPosition = staffPosition;
	}
	public int getValidFlag() {
		return validFlag;
	}
	public void setValidFlag(int validFlag) {
		this.validFlag = validFlag;
	}
	public int getIsCertified() {
		return isCertified;
	}
	public void setIsCertified(int isCertified) {
		this.isCertified = isCertified;
	}
	public String getTrnBy() {
		return trnBy;
	}
	public void setTrnBy(String trnBy) {
		this.trnBy = trnBy;
	}
	public Date getTrnDate() {
		return trnDate;
	}
	public void setTrnDate(Date trnDate) {
		this.trnDate = trnDate;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getStaffBranch() {
		if(staffBranch != null){
			return staffBranch.trim();
		}
		return staffBranch;
	}
	public void setStaffBranch(String staffBranch) {
		this.staffBranch = staffBranch;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
}
