package com.pru.pruquote.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name = "tabacbproduct", catalog = GlobalVar.db_name)
public class AcbProduct {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "\"shortdesc\"")
	private String shortDesc;
	
	@Column(name = "\"longdesc\"")
	private String longDesc;
	
	@Column(name = "\"validflag\"")
	private String validFlag;
	
	@Column(name = "\"createduserid\"")
	private String createdUserId;
	
	@Column(name = "\"createddatetime\"")
	private Date createdDateTime;
	
	@Column(name = "\"modifieduserid\"")
	private String modifiedUserId;
	
	@Column(name = "\"modifieddatetime\"")
	private Date modifiedDateTime;
	
	@Transient
	private String cusApproach = "";
	
	@Transient
	private int commentid;
	
	@OneToMany(mappedBy = "acbProduct")
	private List<ReferralAcbProduct> referralAcbProducts;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getShortDesc() {
		return shortDesc;
	}
	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}
	public String getLongDesc() {
		return longDesc;
	}
	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}
	public String getValidFlag() {
		return validFlag;
	}
	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}
	public String getCreatedUserId() {
		return createdUserId;
	}
	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getModifiedUserId() {
		return modifiedUserId;
	}
	public void setModifiedUserId(String modifiedUserId) {
		this.modifiedUserId = modifiedUserId;
	}
	public Date getModifiedDateTime() {
		return modifiedDateTime;
	}
	public void setModifiedDateTime(Date modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}
	public String getCusApproach() {
		return cusApproach;
	}
	public void setCusApproach(String cusApproach) {
		this.cusApproach = cusApproach;
	}
	public int getCommentid() {
		return commentid;
	}
	public void setCommentid(int commentid) {
		this.commentid = commentid;
	}
}
