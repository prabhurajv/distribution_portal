package com.pru.pruquote.model;

import java.util.List;
import java.util.stream.Collectors;

import com.pru.pruquote.property.IPendingCase;

public class PendingCaseFoNum implements IPendingCase {

	@Override
	public List<PendingCase> filterPendingCase(List<PendingCase> pendingCase, String criteria, String operator) {
		Integer criterialint = Integer.valueOf(criteria);
		return pendingCase.stream().filter(s -> s.getFupNo() == criterialint).collect(Collectors.toList());
	}

	@Override
	public List<PendingCase> filterPendingCase(List<PendingCase> pendingCase, String criteria, String operator,
			String criterialcompare) {
		// TODO Auto-generated method stub
		return null;
	}

}
