package com.pru.pruquote.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;

import com.pru.pruquote.utility.ExportBinding;

public class OtherCollectionAndLapse implements Serializable {
	private static final long serialVersionUID = 2260117902522764908L;
	
	private String objKey;
	private String objType;


	private String bdmcode;
	
	private String sbdmcode;
	
	private String rbdmcode;
	
	@ExportBinding("Agent No")
	private String agntNum;
	
	@ExportBinding("Agent Name")
	private String agntName;
	
	@ExportBinding("Sale Unit")
	private String branchName;
	
	@ExportBinding("Agent Supervisor")
	private String agntSupname;

	@ExportBinding("Agent Region")
	private String bdmname;

	@ExportBinding("Senior BDM")
	private String sbdmname;

	@ExportBinding("Regional BDM")
	private String rbdmname;

	@ExportBinding("Sale Zone")
	private String salezone;
	
	@ExportBinding("Policy number")
	private String chdrNum;

	@ExportBinding("Application Number")
	private String chdrAppnum;

	@ExportBinding("Next Premium")
	private String next_prem;
	
	@ExportBinding("Total Outstanding Premium")  
	private String out_amt;
	
	@ExportBinding("APE")
	private BigDecimal chdrApe;

	@ExportBinding("Product")
	private String product;

	@ExportBinding("PO Name")
	private String poName;

	@ExportBinding("Beneficiary")
	private String ben;

	@ExportBinding("Bill Fequency")
	private String chdrBillfreq;

	@ExportBinding("Payment Method")
	private String payment_method;
	
	@ExportBinding("Due Date")
	private Date chdrDueDate;

	@ExportBinding("Policy Transfer")
	private String agntStatus;	
	
	@ExportBinding("Lapsed Duration (day)")
	private int chdrGraceperiod;
	
	@ExportBinding("Impact Persistency")
	private String isImpactPersistency;

	@ExportBinding("Policy Duration")
	private int chdrAge;
	
	@ExportBinding("Lapsed Reason")
	private String lapsedReason;

	@ExportBinding("Sub Lapsed Reason")
	private String sub_lapsed_reason;
	
	@ExportBinding("Customer Response")
	private String customerResponse;

	@ExportBinding("Mobile Phone 1")
	private String clntMobile1;
	
	@ExportBinding("Mobile Phone 2")
	private String clntMobile2;

	@ExportBinding("Mobile Phone 3")
	private String clnt_mobile3;
	
	@ExportBinding("Standing Order Date")
	private Date occ_date;

	@ExportBinding("Next Payment Date")
	private String next_payment_date;
	
	@ExportBinding("Payment End Date")
	private String stnd_ord_end_date;
	
	@ExportBinding("Cancel Standing Order")
	private String so_cancel_status;
	
	@ExportBinding("Gender")
	private String gender;
	
	@ExportBinding("Policy Status")
	private String chdrStatus;

	@ExportBinding("Tele Reinstatement")
	private String tele_reinstate;

	@ExportBinding("Bank Account")
	private String bank_acc;
	
	@ExportBinding("Sales Force Comment")
	private String ccRemark;
	
	//@ExportBinding("Date Of Birth")
	private Date dob;
	
	private BigDecimal chdrPwithTax;
	
	private String agntSupcode;
	
	private String ccStatus;

	private Date chdrLapseDate;
	
	private String bcuRemark;
	
	private String branchCode;
	public String getPayment_method() {
		return payment_method;
	}
	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}
	
	public String getSo_cancel_status() {
		return so_cancel_status;
	}
	public void setSo_cancel_status(String so_cancel_status) {
		this.so_cancel_status = so_cancel_status;
	}
	public String getClnt_mobile3() {
		return clnt_mobile3;
	}
	public void setClnt_mobile3(String clnt_mobile3) {
		this.clnt_mobile3 = clnt_mobile3;
	}
	public String getNext_payment_date() {
		return next_payment_date;
	}
	public void setNext_payment_date(String next_payment_date) {
		this.next_payment_date = next_payment_date;
	}
	public String getStnd_ord_end_date() {
		return stnd_ord_end_date;
	}
	public void setStnd_ord_end_date(String stnd_ord_end_date) {
		this.stnd_ord_end_date = stnd_ord_end_date;
	}
	public String getBank_acc() {
		return bank_acc;
	}
	public void setBank_acc(String bank_acc) {
		this.bank_acc = bank_acc;
	}
	public String getBdmcode() {
		return bdmcode;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	
	public String getNext_prem() {
		return next_prem;
	}
	public void setNext_prem(String next_prem) {
		this.next_prem = next_prem;
	}
	public void setBdmcode(String bdmcode) {
		this.bdmcode = bdmcode;
	}
	public String getBdmname() {
		return bdmname;
	}
	public void setBdmname(String bdmname) {
		this.bdmname = bdmname;
	}
	public String getSbdmcode() {
		return sbdmcode;
	}
	public void setSbdmcode(String sbdmcode) {
		this.sbdmcode = sbdmcode;
	}
	public String getSbdmname() {
		return sbdmname;
	}
	public void setSbdmname(String sbdmname) {
		this.sbdmname = sbdmname;
	}
	public String getRbdmcode() {
		return rbdmcode;
	}
	public void setRbdmcode(String rbdmcode) {
		this.rbdmcode = rbdmcode;
	}
	public String getRbdmname() {
		return rbdmname;
	}
	public void setRbdmname(String rbdmname) {
		this.rbdmname = rbdmname;
	}
	public String getSalezone() {
		return salezone;
	}
	public void setSalezone(String salezone) {
		this.salezone = salezone;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getSub_lapsed_reason() {
		return sub_lapsed_reason;
	}
	public void setSub_lapsed_reason(String sub_lapsed_reason) {
		this.sub_lapsed_reason = sub_lapsed_reason;
	}
	public Date getOcc_date() {
		return occ_date;
	}
	public void setOcc_date(Date occ_date) {
		this.occ_date = occ_date;
	}
	public String getTele_reinstate() {
		return tele_reinstate;
	}
	public void setTele_reinstate(String tele_reinstate) {
		this.tele_reinstate = tele_reinstate;
	}
	
	public String getObjKey() {
		return objKey;
	}
	public void setObjKey(String objKey) {
		this.objKey = objKey;
	}
	public String getObjType() {
		return objType;
	}
	public void setObjType(String objType) {
		this.objType = objType;
	}
	public String getChdrNum() {
		return chdrNum;
	}
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}
	public String getChdrAppnum() {
		return chdrAppnum;
	}
	public void setChdrAppnum(String chdrAppnum) {
		this.chdrAppnum = chdrAppnum;
	}
	public String getPoName() {
		return poName;
	}
	public void setPoName(String poName) {
		this.poName = poName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getClntMobile1() {
		if(clntMobile1 == null)
			clntMobile1 = "";
		return clntMobile1;
	}
	public void setClntMobile1(String clntMobile1) {
		this.clntMobile1 = clntMobile1;
	}
	public String getClntMobile2() {
		if(clntMobile2 == null)
			clntMobile2 = "";
		return clntMobile2;
	}
	public void setClntMobile2(String clntMobile2) {
		this.clntMobile2 = clntMobile2;
	}
	public BigDecimal getChdrPwithTax() {
		return chdrPwithTax;
	}
	public void setChdrPwithTax(BigDecimal chdrPwithTax) {
		this.chdrPwithTax = chdrPwithTax;
	}
	public String getChdrBillfreq() {
		return chdrBillfreq;
	}
	public void setChdrBillfreq(String chdrBillfreq) {
		this.chdrBillfreq = chdrBillfreq;
	}
	public Date getChdrDueDate() {
		return chdrDueDate;
	}
	public void setChdrDueDate(Date chdrDueDate) {
		this.chdrDueDate = chdrDueDate;
	}
	public int getChdrGraceperiod() {
		return chdrGraceperiod;
	}
	public void setChdrGraceperiod(int chdrGraceperiod) {
		this.chdrGraceperiod = chdrGraceperiod;
	}
	public String getAgntNum() {
		return agntNum;
	}
	public void setAgntNum(String agntNum) {
		this.agntNum = agntNum;
	}
	public String getAgntName() {
		return agntName;
	}
	public void setAgntName(String agntName) {
		this.agntName = agntName;
	}
	public String getAgntSupcode() {
		if(agntSupcode == null)
			agntSupcode = "";
		return agntSupcode;
	}
	public void setAgntSupcode(String agntSupcode) {
		this.agntSupcode = agntSupcode;
	}
	public String getAgntSupname() {
		if(agntSupname == null)
			agntSupname = "";
		return agntSupname;
	}
	public void setAgntSupname(String agntSupname) {
		this.agntSupname = agntSupname;
	}
	public int getChdrAge() {
		return chdrAge;
	}
	public void setChdrAge(int chdrAge) {
		this.chdrAge = chdrAge;
	}
	public String getCcStatus() {
		if(ccStatus == null)
			ccStatus = "";
		return ccStatus;
	}
	public void setCcStatus(String ccStatus) {
		this.ccStatus = ccStatus;
	}
	public String getAgntStatus() {
		if(agntStatus == null)
			agntStatus = "";
		return agntStatus;
	}
	public void setAgntStatus(String agntStatus) {
		this.agntStatus = agntStatus;
	}
	public BigDecimal getChdrApe() {
		return chdrApe;
	}
	public void setChdrApe(BigDecimal chdrApe) {
		this.chdrApe = chdrApe;
	}
	public Date getChdrLapseDate() {
		if(chdrStatus == null)
			chdrStatus = "";
		return chdrLapseDate;
	}
	public void setChdrLapseDate(Date chdrLapseDate) {
		this.chdrLapseDate = chdrLapseDate;
	}
	public String getChdrStatus() {
		if(chdrStatus == null)
			chdrStatus = "";
		return chdrStatus;
	}
	public void setChdrStatus(String chdrStatus) {
		this.chdrStatus = chdrStatus;
	}
	public String getIsImpactPersistency() {
		if(isImpactPersistency == null)
			isImpactPersistency = "";
		return isImpactPersistency;
	}
	public void setIsImpactPersistency(String isImpactPersistency) {
		this.isImpactPersistency = isImpactPersistency;
	}
	public String getLapsedReason() {
		if(lapsedReason == null)
			lapsedReason = "";
		return lapsedReason;
	}
	public void setLapsedReason(String lapsedReason) {
		this.lapsedReason = lapsedReason;
	}
	public String getBcuRemark() {
		if(bcuRemark == null)
			bcuRemark = "";
		return bcuRemark;
	}
	public void setBcuRemark(String bcuRemark) {
		this.bcuRemark = bcuRemark;
	}
	public String getCustomerResponse() {
		if(customerResponse == null)
			customerResponse = "";
		return customerResponse;
	}
	public void setCustomerResponse(String customerResponse) {
		this.customerResponse = customerResponse;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	
	public String getCcRemark() {
		if(ccRemark == null)
			ccRemark = "";
		return ccRemark;
	}
	public void setCcRemark(String ccRemark) {
		this.ccRemark = ccRemark;
	}

	public String getOut_amt() {
		return out_amt;
	}
	
	public void setOut_amt(String out_amt) {
		this.out_amt = out_amt;
	}

	public static Comparator<OtherCollectionAndLapse> ChdrNumComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getChdrNum().compareToIgnoreCase(clandla2.getChdrNum());
        }
    };
 
    public static Comparator<OtherCollectionAndLapse> ChdrNumComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getChdrNum().compareToIgnoreCase(clandla1.getChdrNum());
        }
 	};
 	
 
 	public static Comparator<OtherCollectionAndLapse> ChdrAppnumComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getChdrAppnum().compareToIgnoreCase(clandla2.getChdrAppnum());
        }
 	};
 
 	public static Comparator<OtherCollectionAndLapse> ChdrAppnumComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getChdrAppnum().compareToIgnoreCase(clandla1.getChdrAppnum());
        }
 	};

 	public static Comparator<OtherCollectionAndLapse> clntMobile1ComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getClntMobile1().compareToIgnoreCase(clandla2.getClntMobile1());
        }
    };
    
 	public static Comparator<OtherCollectionAndLapse> clntMobile1ComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getClntMobile1().compareToIgnoreCase(clandla1.getClntMobile1());
        }
 	};
 	
 	public static Comparator<OtherCollectionAndLapse> clntMobile2ComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getClntMobile2().compareToIgnoreCase(clandla2.getClntMobile2());
        }
    };
    
 	public static Comparator<OtherCollectionAndLapse> clntMobile2ComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getClntMobile2().compareToIgnoreCase(clandla1.getClntMobile2());
        }
 	};
 	
 	public static Comparator<OtherCollectionAndLapse> chdrPwithTaxComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getChdrPwithTax().toString().compareToIgnoreCase(clandla2.getChdrPwithTax().toString());
        }
    };
    
 	public static Comparator<OtherCollectionAndLapse> chdrPwithTaxComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getChdrPwithTax().toString().compareToIgnoreCase(clandla1.getChdrPwithTax().toString());
        }
 	};
 	
 	public static Comparator<OtherCollectionAndLapse> chdrBillfreqComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getChdrBillfreq().compareToIgnoreCase(clandla2.getChdrBillfreq());
        }
    };
    
 	public static Comparator<OtherCollectionAndLapse> chdrBillfreqComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getChdrBillfreq().compareToIgnoreCase(clandla1.getChdrBillfreq());
        }
 	};
 	
 	public static Comparator<OtherCollectionAndLapse> chdrDueDateComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getChdrDueDate().toString().compareToIgnoreCase(clandla2.getChdrDueDate().toString());
        }
    };
    
 	public static Comparator<OtherCollectionAndLapse> chdrDueDateComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getChdrDueDate().toString().compareToIgnoreCase(clandla1.getChdrDueDate().toString());
        }
 	};
 	
 	public static Comparator<OtherCollectionAndLapse> chdrLapseDateComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getChdrLapseDate().toString().compareToIgnoreCase(clandla2.getChdrLapseDate().toString());
        }
    };
    
 	public static Comparator<OtherCollectionAndLapse> chdrLapseDateComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getChdrLapseDate().toString().compareToIgnoreCase(clandla1.getChdrLapseDate().toString());
        }
 	};
	
 	public static Comparator<OtherCollectionAndLapse> lapsedReasonComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getLapsedReason().compareToIgnoreCase(clandla2.getLapsedReason());
        }
    };
    
 	public static Comparator<OtherCollectionAndLapse> lapsedReasonComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getLapsedReason().compareToIgnoreCase(clandla1.getLapsedReason());
        }
 	};
	
 	public static Comparator<OtherCollectionAndLapse> bcuRemarkComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getBcuRemark().compareToIgnoreCase(clandla2.getBcuRemark());
        }
    };
    
 	public static Comparator<OtherCollectionAndLapse> bcuRemarkComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getBcuRemark().compareToIgnoreCase(clandla1.getBcuRemark());
        }
 	};
	
 	public static Comparator<OtherCollectionAndLapse> customerResponseComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getCustomerResponse().compareToIgnoreCase(clandla2.getCustomerResponse());
        }
    };
    
 	public static Comparator<OtherCollectionAndLapse> customerResponseComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getCustomerResponse().compareToIgnoreCase(clandla1.getCustomerResponse());
        }
 	};
 	
 	public static Comparator<OtherCollectionAndLapse> ccStatusComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getCcStatus().compareToIgnoreCase(clandla2.getCcStatus());
        }
    };
    
 	public static Comparator<OtherCollectionAndLapse> ccStatusComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getCcStatus().compareToIgnoreCase(clandla1.getCcStatus());
        }
 	};
 	
 	public static Comparator<OtherCollectionAndLapse> agntStatusComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getAgntStatus().compareToIgnoreCase(clandla2.getAgntStatus());
        }
    };
    
 	public static Comparator<OtherCollectionAndLapse> agntStatusComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getAgntStatus().compareToIgnoreCase(clandla1.getAgntStatus());
        }
 	};
 	
 	public static Comparator<OtherCollectionAndLapse> agntIsImpactPersistencyComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getIsImpactPersistency().compareToIgnoreCase(clandla2.getIsImpactPersistency());
        }
    };
    
 	public static Comparator<OtherCollectionAndLapse> agntIsImpactPersistencyComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getIsImpactPersistency().compareToIgnoreCase(clandla1.getIsImpactPersistency());
        }
 	};
 	
 	public static Comparator<OtherCollectionAndLapse> agntCcRemarkComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getCcRemark().compareToIgnoreCase(clandla2.getCcRemark());
        }
    };
    
 	public static Comparator<OtherCollectionAndLapse> agntCcRemarkComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getCcRemark().compareToIgnoreCase(clandla1.getCcRemark());
        }
 	};
 	
 	public static Comparator<OtherCollectionAndLapse> poNameComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getPoName().compareToIgnoreCase(clandla2.getPoName());
        }
    };
    
 	public static Comparator<OtherCollectionAndLapse> poNameComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getPoName().compareToIgnoreCase(clandla1.getPoName());
        }
 	};
 	
 	public static Comparator<OtherCollectionAndLapse> chdrApeComparatorAsc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla1.getChdrApe().toString().compareToIgnoreCase(clandla2.getChdrApe().toString());
        }
    };
    
 	public static Comparator<OtherCollectionAndLapse> chdrApeComparatorDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getChdrApe().toString().compareToIgnoreCase(clandla1.getChdrApe().toString());
        }
 	};
 	
 	 public static Comparator<OtherCollectionAndLapse> dobAsc = new Comparator<OtherCollectionAndLapse>(){
         public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
                return clandla1.getDob().toString().compareToIgnoreCase(clandla2.getDob().toString());
         }
     };
     
 	public static Comparator<OtherCollectionAndLapse> dobDesc = new Comparator<OtherCollectionAndLapse>(){
        public int compare(OtherCollectionAndLapse clandla1, OtherCollectionAndLapse clandla2){
               return clandla2.getDob().toString().compareToIgnoreCase(clandla1.getDob().toString());
        }
 	};
 	
}