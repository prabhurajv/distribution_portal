package com.pru.pruquote.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"ap\".tabup_crossselling_referral")
public class UpsellingAndCrosssellingReferral implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9092415386037392211L;
	
	@Id
	@Column(name = "obj_key")
	private String objKey;
	
	@Column(name = "rid")
	private BigDecimal rId;

	public String getObjKey() {
		return objKey;
	}

	public void setObjKey(String objKey) {
		this.objKey = objKey;
	}

	public BigDecimal getrId() {
		return rId;
	}

	public void setrId(BigDecimal rId) {
		this.rId = rId;
	}
	
	
}
