package com.pru.pruquote.model;

import java.io.Serializable;

public class ReferralList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String rid;
	String cusname;
	String cussex;
	String referraltype;
	String referraltypedate;
	String sucappointstatus;
	String sucappstatusdate;
	String ape;
	String existcusstatus;
	String agntnum;
	String agntName;
	
	public  ReferralList() {
		// TODO Auto-generated constructor stub
	}
	public  ReferralList(String rid ,String cusname ,String cussex ,String referraltype ,String referraltypedate ,String sucappointstatus 
			,String sucappstatusdate ,String ape ,String existcusstatus ,String agntnum, String agntName) {
		this.rid = rid;
		this.cusname = cusname;
		this.cussex = cussex;
		this.referraltype = referraltype;
		this.referraltypedate = referraltypedate;
		this.sucappointstatus = sucappointstatus;
		this.sucappstatusdate = sucappstatusdate;
		this.ape = ape;
		this.existcusstatus = existcusstatus;
		this.agntnum = agntnum;
		this.agntName = agntName;
	}
	public String getRid() {
		return rid;
	}
	public String getCusname() {
		return cusname;
	}
	public String getCussex() {
		return cussex;
	}
	public String getReferraltype() {
		return referraltype;
	}
	public String getReferraltypedate() {
		return referraltypedate;
	}
	public String getSucappointstatus() {
		return sucappointstatus;
	}
	public String getSucappstatusdate() {
		return sucappstatusdate;
	}
	public String getApe() {
		return ape;
	}
	public String getExistcusstatus() {
		return existcusstatus;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public String getAgntName() {
		return agntName;
	}
}
