package com.pru.pruquote.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name = "tabpolicypending", catalog = GlobalVar.db_name)
public class PendingCase implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2260117902522764908L;

	@Column(name = "chdrnum")
	@Id
	private String chdrNum;
	
	@Column(name = "appnum")
	private String appNum;
	
	@Column(name = "po_name")
	private String poName;
	
	@Column(name = "chdr_ape")
	private BigDecimal chdrApe;
	
	@Column(name = "pending_reason")
	private String pendingReason;
	
	@Column(name = "status")
	private String pendingStatus;
	
	@Column(name = "pending_period")
	private Integer pendingPeriod;
	
	@Column(name = "ntu_date")
	private Date ntuDate;
	
	@Column(name = "refresh_time")
	private Date refreshTime;
	
	@Column(name = "po_phonenumber")
	private String poPhoneNumber;
	
	@Column(name = "agnt_num")
	private String agntNum;
	
	@Column(name = "agent_name")
	private String agntName;
	
	@Column(name = "chdr_key_date")
	private Date chdrKeyDate;
	
	@Column(name = "penal_doctor")
	private String penalDoctor;
	
	@Column(name = "agnt_phone")
	private String agntPhone;
	
	@Column(name = "doctor_image")
	private String doctorImage;
	
	@Column(name = "fupno")
	private int fupNo;
	
	public String getChdrNum() {
		return chdrNum;
	}
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}
	public String getAppNum() {
		return appNum;
	}
	public void setAppNum(String appNum) {
		this.appNum = appNum;
	}
	public String getPoName() {
		return poName;
	}
	public void setPoName(String poName) {
		this.poName = poName;
	}
	public BigDecimal getChdrApe() {
		return chdrApe;
	}
	public void setChdrApe(BigDecimal chdrApe) {
		this.chdrApe = chdrApe;
	}
	public String getPendingReason() {
		return pendingReason;
	}
	public void setPendingReason(String pendingReason) {
		this.pendingReason = pendingReason;
	}
	public String getPendingStatus() {
		return pendingStatus;
	}
	public void setPendingStatus(String pendingStatus) {
		this.pendingStatus = pendingStatus;
	}
	public Integer getPendingPeriod() {
		return pendingPeriod;
	}
	public void setPendingPeriod(Integer pendingPeriod) {
		this.pendingPeriod = pendingPeriod;
	}
	public Date getNtuDate() {
		return ntuDate;
	}
	public void setNtuDate(Date ntuDate) {
		this.ntuDate = ntuDate;
	}
	public Date getRefreshTime() {
		return refreshTime;
	}
	public void setRefreshTime(Date refreshTime) {
		this.refreshTime = refreshTime;
	}
	public String getPoPhoneNumber() {
		return poPhoneNumber;
	}
	public void setPoPhoneNumber(String poPhoneNumber) {
		this.poPhoneNumber = poPhoneNumber;
	}
	public String getAgntNum() {
		return agntNum;
	}
	public void setAgntNum(String agntNum) {
		this.agntNum = agntNum;
	}
	public String getAgntName() {
		return agntName;
	}
	public void setAgntName(String agntName) {
		this.agntName = agntName;
	}
	public Date getChdrKeyDate() {
		return chdrKeyDate;
	}
	public void setChdrKeyDate(Date chdrKeyDate) {
		this.chdrKeyDate = chdrKeyDate;
	}
	public String getPenalDoctor() {
		return penalDoctor;
	}
	public void setPenalDoctor(String penalDoctor) {
		this.penalDoctor = penalDoctor;
	}
	public String getAgntPhone() {
		return agntPhone;
	}
	public void setAgntPhone(String agntPhone) {
		this.agntPhone = agntPhone;
	}
	public String getDoctorImage() {
		return doctorImage;
	}
	public void setDoctorImage(String doctorImage) {
		this.doctorImage = doctorImage;
	}
	public int getFupNo() {
		return fupNo;
	}
	public void setFupNo(int fupNo) {
		this.fupNo = fupNo;
	}
	
	public PendingCase(){}
	
	public PendingCase(String chdrNum, String appNum, String poName, BigDecimal chdrApe, String pendingReason
			, String pendingStatus, int pendingPeriod, Date ntuDate, Date refreshTime, String poPhoneNumber
			, String agntNum, String agntName, Date chdrKeyDate, String penalDoctor, String agntPhone
			, String doctorImage, int fupNo){
		this.chdrNum = chdrNum;
		this.appNum = appNum;
		this.poName = poName;
		this.chdrApe = chdrApe;
		this.pendingReason = pendingReason;
		this.pendingStatus = pendingStatus;
		this.pendingPeriod = pendingPeriod;
		this.ntuDate = ntuDate;
		this.refreshTime = refreshTime;
		this.poPhoneNumber = poPhoneNumber;
		this.agntNum = agntNum;
		this.agntName = agntName;
		this.chdrKeyDate = chdrKeyDate;
		this.penalDoctor = penalDoctor;
		this.agntPhone = agntPhone;
		this.doctorImage = doctorImage;
		this.fupNo = fupNo;
	}
}
