package com.pru.pruquote.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name = "occupation", catalog = GlobalVar.db_name)
public class Occupation implements Serializable {
	private static final long serialVersionUID = 2260117902522764908L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "engname", length = 50, nullable = false)
	private String engName;

	@Column(name = "khname", length = 50, nullable = false)
	private String khName;
	
	@Column(name = "occ_order", nullable = false)
	private int occOrder;

	@Column(name = "isdeleted")
	private int isDeleted;

	@Column(name = "created_by", length = 50)
	private String createdBy;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by", length = 50)
	private String modifiedBy;

	@Column(name = "modified_date")
	private Date modifiedDate;

	@Column(name = "file_id")
	private int fileId;
	// @ManyToMany(mappedBy = "occupations", cascade = { CascadeType.ALL })
	// private Set<File> files;

	@ManyToOne
	@JoinColumn(name = "file_id", referencedColumnName = "id", insertable = false, updatable = false)
	@NotFound(action = NotFoundAction.IGNORE)
	private File file;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "quote_occupation", catalog = GlobalVar.db_name, joinColumns = {
			@JoinColumn(name = "occ_id") }, inverseJoinColumns = { @JoinColumn(name = "quote_id") })
	private List<PruquoteParm> pruQuoteParms;

	public List<PruquoteParm> getPruQuoteParms() {
		return pruQuoteParms;
	}

	public void setPruQuoteParms(List<PruquoteParm> pruQuoteParms) {
		this.pruQuoteParms = pruQuoteParms;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFileId() {
		return fileId;
	}

	public void setFileId(int fileId) {
		this.fileId = fileId;
	}

	public String getEngName() {
		return engName;
	}

	public void setEngName(String engName) {
		this.engName = engName;
	}

	public String getKhName() {
		return khName;
	}

	public void setKhName(String khName) {
		this.khName = khName;
	}
	

	public int getOccOrder() {
		return occOrder;
	}

	public void setOccOrder(int occOrder) {
		this.occOrder = occOrder;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	// public Set<File> getFiles() {
	// return files;
	// }
	//
	//
	// public void setFiles(Set<File> files) {
	// this.files = files;
	// }

}