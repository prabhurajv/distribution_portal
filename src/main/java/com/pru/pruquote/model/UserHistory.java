package com.pru.pruquote.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name="\"ap\".sec_user_history")
public class UserHistory {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sec_user_history_user_history_id_seq") //Auto number primary key
	@SequenceGenerator(name="sec_user_history_user_history_id_seq", sequenceName=GlobalVar.db_name + ".sec_user_history_user_history_id_seq", allocationSize=1)
	@Column(name="user_history_id")
	private long id;
	
	@Column(name="user_id_pk")
	private long userIdPk;
	
	@Column(name="password")
	private String pwd;
	
	@Column(name="description")
	private String description;
	
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="created_by")
	private String createdBy;

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserIdPk() {
		return this.userIdPk;
	}

	public void setUserIdPk(long userIdPk) {
		this.userIdPk = userIdPk;
	}

	public String getPwd() {
		return this.pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

}