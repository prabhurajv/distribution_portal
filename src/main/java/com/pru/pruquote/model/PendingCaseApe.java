package com.pru.pruquote.model;

import java.util.List;
import java.util.stream.Collectors;


import com.pru.pruquote.property.IPendingCase;

public class PendingCaseApe implements IPendingCase {

	@Override
	public List<PendingCase> filterPendingCase(List<PendingCase> pendingCase, String criteria, String operator) {
		Double  bd= Double.parseDouble(criteria);
		if(operator.equals("<")) {
			return pendingCase.stream().filter(s -> s.getChdrApe().doubleValue() < bd).collect(Collectors.toList());
		} else if(operator.equals(">")) {
			return pendingCase.stream().filter(s -> s.getChdrApe().doubleValue() > bd).collect(Collectors.toList());
		} else if(operator.equals("<=")) {
			return pendingCase.stream().filter(s -> s.getChdrApe().doubleValue() <= bd).collect(Collectors.toList());
		} else if(operator.equals(">=")) {
			return pendingCase.stream().filter(s -> s.getChdrApe().doubleValue() >= bd).collect(Collectors.toList());
		} else  {
			return pendingCase.stream().filter(s -> s.getChdrApe().equals(bd)).collect(Collectors.toList());
		}
		
	}

	@Override
	public List<PendingCase> filterPendingCase(List<PendingCase> pendingCase, String criteria, String operator,
			String criterialcompare) {
		Double  criteriastart= Double.parseDouble(criteria);
		Double  criteriaend= Double.parseDouble(criterialcompare);
		return pendingCase.stream().filter(s -> s.getChdrApe().doubleValue() >= criteriastart && s.getChdrApe().doubleValue() <= criteriaend).collect(Collectors.toList());
	}

}
