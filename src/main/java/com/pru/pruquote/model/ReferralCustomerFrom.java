package com.pru.pruquote.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name = "tabreferal_customerfrom", catalog = GlobalVar.db_name)
public class ReferralCustomerFrom {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"")
	private long id;
	
	@Column(name = "\"short_desc\"")
	private String shortDesc;
	
	@Column(name = "\"long_desc\"")
	private String longDesc;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getShortDesc() {
		return shortDesc.trim();
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getLongDesc() {
		return longDesc.trim();
	}

	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}
}
