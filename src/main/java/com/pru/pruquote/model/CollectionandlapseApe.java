package com.pru.pruquote.model;

import java.util.List;
import java.util.stream.Collectors;

import com.pru.pruquote.property.ICollectionAndLapsed;

public class CollectionandlapseApe implements ICollectionAndLapsed {

	@Override
	public List<CollectionAndLapse> filterCollectionAndLapse(List<CollectionAndLapse> collectionAndLapse,
			String criteria, String operator) {
		Double  bd= Double.parseDouble(criteria);
		if(operator.equals("<")) {
			return collectionAndLapse.stream().filter(s -> s.getChdrApe().doubleValue() < bd).collect(Collectors.toList());
		} else if(operator.equals(">")) {
			return collectionAndLapse.stream().filter(s -> s.getChdrApe().doubleValue() > bd).collect(Collectors.toList());
		} else if(operator.equals("<=")) {
			return collectionAndLapse.stream().filter(s -> s.getChdrApe().doubleValue() <= bd).collect(Collectors.toList());
		} else if(operator.equals(">=")) {
			return collectionAndLapse.stream().filter(s -> s.getChdrApe().doubleValue() >= bd).collect(Collectors.toList());
		} else  {
			return collectionAndLapse.stream().filter(s -> s.getChdrApe().doubleValue() == bd).collect(Collectors.toList());
		}
	}

	@Override
	public List<CollectionAndLapse> filterCollectionAndLapse(List<CollectionAndLapse> collectionAndLapse,
			String criteria, String operator, String criterialcompare) {
		Double  criteriastart= Double.parseDouble(criteria);
		Double  criteriaend= Double.parseDouble(criterialcompare);
		return collectionAndLapse.stream().filter(s -> s.getChdrApe().doubleValue() >= criteriastart && s.getChdrApe().doubleValue() <= criteriaend).collect(Collectors.toList());
	}

}
