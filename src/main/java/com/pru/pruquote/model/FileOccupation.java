package com.pru.pruquote.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name = "file_occupation", catalog = GlobalVar.db_name)
public class FileOccupation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	
	@Column(name = "file_id", nullable = false)
	private int fileId;
	
	@Column(name = "occ_id", nullable = false)
	private int occId;
	
	@ManyToOne
	@JoinColumn(name = "occ_id", nullable = false, insertable = false, updatable = false)
	private Occupation occupation;
	
	@ManyToOne
	@JoinColumn(name = "file_id", nullable = false, insertable = false, updatable = false)
	private File file;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public int getFileId() {
		return fileId;
	}

	public void setFileId(int fileId) {
		this.fileId = fileId;
	}

	public int getOccId() {
		return occId;
	}

	public void setOccId(int occId) {
		this.occId = occId;
	}
}