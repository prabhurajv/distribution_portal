package com.pru.pruquote.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.pru.pruquote.property.IPendingCase;

public class PendingCaseNtuDate implements IPendingCase {
	Date startdate ;
	Date enddate ;
	DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	@Override
	public List<PendingCase> filterPendingCase(List<PendingCase> pendingCase, String criteria, String operator) {
		try {
			startdate = format.parse(criteria);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return pendingCase.stream().filter(s -> s.getNtuDate().equals(startdate)).collect(Collectors.toList());
	}

	@Override
	public List<PendingCase> filterPendingCase(List<PendingCase> pendingCase, String criteria, String operator,
			String criterialcompare) {
		
		try {
			startdate = format.parse(criteria);
			enddate = format.parse(criterialcompare);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(criteria));
			c.add(Calendar.DATE, -1);
			startdate = format.parse(sdf.format(c.getTime()));
			
			c.setTime(sdf.parse(criterialcompare));
			c.add(Calendar.DATE, 1);
			enddate = format.parse(sdf.format(c.getTime()));
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return pendingCase.stream().filter(s -> s.getNtuDate().after(startdate) && s.getNtuDate().before(enddate)).collect(Collectors.toList());
	}

}
