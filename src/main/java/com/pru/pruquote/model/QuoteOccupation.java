package com.pru.pruquote.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name = "quote_occupation", catalog = GlobalVar.db_name)
public class QuoteOccupation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	
	@Column(name = "quote_id", nullable = false)
	private long quoteId;
	
	@Column(name = "occ_id", nullable = false)
	private int occId;
	
	@Column(name = "remark")
	private String remark;
	
	@Column(name = "validflag", nullable = false)
	private int validFlag;
	
	@Column(name = "createduserid")
	private String createdUserId;
	
	@Column(name = "modifieduserid")
	private String modifiedUserId;
	
	@Column(name = "createddatetime")
	private Date createdDatetime;
	
	@Column(name = "modifieddatetime")
	private Date modifiedDatetime;
	
	public QuoteOccupation() {
		
	}
	
	public QuoteOccupation(long quoteid, int occid, String remark, int validflag, String createduserid, Date createddatetime) {
		this.quoteId = quoteid;
		this.occId = occid;
		this.remark = remark;
		this.createdUserId = createduserid;
		this.validFlag = validflag;
		this.createdDatetime = createddatetime;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public long getQuoteId() {
		return quoteId;
	}

	public void setQuoteId(long quoteId) {
		this.quoteId = quoteId;
	}

	public int getOccId() {
		return occId;
	}

	public void setOccId(int occId) {
		this.occId = occId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public int getValidFlag() {
		return validFlag;
	}

	public void setValidFlag(int validFlag) {
		this.validFlag = validFlag;
	}

	public String getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}

	public String getModifiedUserId() {
		return modifiedUserId;
	}

	public void setModifiedUserId(String modifiedUserId) {
		this.modifiedUserId = modifiedUserId;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public Date getModifiedDatetime() {
		return modifiedDatetime;
	}

	public void setModifiedDatetime(Date modifiedDatetime) {
		this.modifiedDatetime = modifiedDatetime;
	}
	
}