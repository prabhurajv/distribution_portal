package com.pru.pruquote.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.pru.pruquote.utility.GlobalVar;

@Entity
@Table(name = "tabreferal_commenttype", catalog = GlobalVar.db_name)
public class ReferralCommentType {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "")
	@SequenceGenerator(name = "", sequenceName = GlobalVar.db_name + "", allocationSize = 1)
	@Column(name = "cid")
	private long cid;
	
	@Column(name = "cdesc")
	private String cdesc;
	
	@Column(name = "validflag")
	private String validFlag;
	
	@Column(name = "trndatetime")
	private Date trnDateTime;
	
	@Column(name = "trnby")
	private String trnBy;
	
	@Column(name = "lasttrndatetime")
	private Date lastTrnDateTime;
	
	@Column(name = "lasttrnby")
	private String lastTrnBy;
	
	@Column(name = "ip")
	private String ip;
	
	@Column(name = "pip")
	private String pip;
	
	public long getCid() {
		return cid;
	}
	public void setCid(long cid) {
		this.cid = cid;
	}
	public String getCdesc() {
		return cdesc;
	}
	public void setCdesc(String cdesc) {
		this.cdesc = cdesc;
	}
	public String getValidFlag() {
		return validFlag;
	}
	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}
	public Date getTrnDateTime() {
		return trnDateTime;
	}
	public void setTrnDateTime(Date trnDateTime) {
		this.trnDateTime = trnDateTime;
	}
	public String getTrnBy() {
		return trnBy;
	}
	public void setTrnBy(String trnBy) {
		this.trnBy = trnBy;
	}
	public Date getLastTrnDateTime() {
		return lastTrnDateTime;
	}
	public void setLastTrnDateTime(Date lastTrnDateTime) {
		this.lastTrnDateTime = lastTrnDateTime;
	}
	public String getLastTrnBy() {
		return lastTrnBy;
	}
	public void setLastTrnBy(String lastTrnBy) {
		this.lastTrnBy = lastTrnBy;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getPip() {
		return pip;
	}
	public void setPip(String pip) {
		this.pip = pip;
	}
}
