package com.pru.pruquote.utility;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.pru.pruquote.model.PruquoteParm;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.lang.reflect.Field;

public class AnonymousHelper {
	public final static String DATE_FORMAT = "dd/MM/yyyy";

	public static String getClientIP(HttpServletRequest request) {
		// HttpServletRequest request = (HttpServletRequest)
		// FacesContext.getCurrentInstance().getExternalContext().getRequest();
		// String ipAddress = request.getHeader("X-FORWARDED-FOR");
		// if (ipAddress == null) {
		// ipAddress = request.getRemoteAddr();
		// }

		String ipAddress = request.getRemoteHost();

		return ipAddress;
	}

	public static String getReverseString(String original) {
		return new StringBuilder(original).reverse().toString();
	}

	public static boolean isPwdValid(String pwd) {
		boolean isValid = false;

		String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=])(?=\\S+$).{8,}$";

		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(pwd);

		if (m.find()) {
			isValid = true;
		}

		return isValid;
	}

	public static boolean isKeywordAllowed(String keyword) {
		boolean isValid = false;

		if (keyword == null)
			return false;

		String pattern = "^[a-zA-Z0-9]*$";

		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(keyword);

		if (m.find()) {
			isValid = true;
		}

		return isValid;
	}

	public static boolean isKeywordAllowed(String keyword, String pattern) {
		boolean isValid = false;

		if (keyword == null)
			return false;

		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(keyword);

		if (m.find()) {
			isValid = true;
		}

		return isValid;
	}

	public static boolean isTextOnly(String text) {
		boolean isValid = false;

		if (text == null)
			return false;

		String pattern = "^[a-zA-Z]*$";

		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(text);

		if (m.find()) {
			isValid = true;
		}

		return isValid;
	}

	public static boolean isNumberOnly(String number) {
		boolean isValid = false;

		if (number == null)
			return false;

		String pattern = "^[0-9]*$";

		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(number);

		if (m.find()) {
			isValid = true;
		}

		return isValid;
	}

	public static boolean isDateValid(String date) {
		try {
			if (date == null)
				return false;

			DateFormat df = new SimpleDateFormat(DATE_FORMAT);
			df.setLenient(false);
			df.parse(date);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	public static String replaceWithTextIfNotValid(String text, String defaultText) {
		if (isKeywordAllowed(text)) {
			return text;
		}

		return defaultText;
	}

	public static String sanitizeInput(String input) {
		if (input != null) {
			if (!isKeywordAllowed(input)) {
				return HtmlUtils.htmlEscape(input);
			}
		}

		return input;
	}

	public static PruquoteParm sanitizedPruquoteParm(PruquoteParm pruquoteparm) {
		pruquoteparm.setLa1name(sanitizeInput(pruquoteparm.getLa1name()));
		pruquoteparm.setLa2name(sanitizeInput(pruquoteparm.getLa2name()));
		pruquoteparm.setPoName(sanitizeInput(pruquoteparm.getPoName()));

		return pruquoteparm;
	}

	public static boolean isDangerous(String text) {
		return text.toLowerCase().contains("script");
	}

	public static void main(String[] args) {
		System.out.println(isPwdValid("Abc@123456"));
	}

	public static int getLapseDays(long sendDateTime) {
		int days = 0;

		long current = new Date().getTime();

		days = (int) ((((current - sendDateTime) / 1000) / 3600) / 24);

		return days;
	}

	public static int getLapseDays(long compareDateTime, long targetDateTime) {
		int days = 0;

		days = (int) ((((compareDateTime - targetDateTime) / 1000) / 3600) / 24);

		return days;
	}

	public static Date getFormattedDate(String date, String format) throws ParseException {
		return new SimpleDateFormat(format)
				.parse(new SimpleDateFormat(format).format(new SimpleDateFormat(DATE_FORMAT).parse(date)));
	}

	public static String getYesterdayDate(Date today) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, -1);
		return dateFormat.format(cal.getTime());
	}

	public static boolean isMonthToDate(Date today, Date date) {
		Calendar caltoday = Calendar.getInstance();
		caltoday.setTime(today);

		Calendar caldate = Calendar.getInstance();
		caldate.setTime(date);

		return caltoday.YEAR == caldate.YEAR && caltoday.MONTH == caldate.MONTH;
	}

	public static <T> Map<String, Object[]> getDistinctValueForSummary(List<T> list, String... methNames)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		HashMap<String, Object[]> result = new HashMap<String, Object[]>(2668);

		List<T> tList = (List<T>) list;
		Object[] objArray = new Object[methNames.length - 1];

		for (T t : tList) {
			Method methodToInvoke = t.getClass().getMethod(methNames[0], (Class<?>[]) null);
			String key = methodToInvoke.invoke(t).toString();
			if (!result.containsKey(key)) {
				result.put(key, objArray);
			}
		}

		result.forEach((k, v) -> {
			List<Object[]> valueList = new ArrayList<Object[]>();

			for (T t : tList) {
				try {
					Method methodToInvoke = t.getClass().getMethod(methNames[1], (Class<?>[]) null);
					String keyTest = methodToInvoke.invoke(t).toString();

					if (keyTest.equals(k)) {
						Object[] value = new Object[methNames.length - 1];
						value[0] = keyTest;

						for (int i = 2; i < methNames.length; i++) {
							Method methodToInvoke2 = t.getClass().getMethod(methNames[i], (Class<?>[]) null);
							value[i - 1] = methodToInvoke2.invoke(t);
						}

						valueList.add(value);
					}
				} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			result.replace(k, valueList.toArray());
		});

		return result;
	}

	public static String replaceNullString(Object mightBeNull, String replaceWith) {
		if (String.valueOf(mightBeNull).equals("null"))
			return replaceWith;

		return mightBeNull.toString();
	}

	public static <T> String getServerSideDatasource(HttpServletRequest req, List<T> list) {
		String result = "";
		ObjectMapper mapper = new ObjectMapper();
		int draw = Integer.valueOf(req.getParameter("draw"));
		int start = Integer.valueOf(req.getParameter("start"));
		int length = Integer.valueOf(req.getParameter("length"));
		int totalRecords = 0;

		int pageSize = (list.size() - start) >= length ? length : (list.size() - start);
		int skip = start;

		List<T> filteredList = list.subList(skip, skip + pageSize);
		totalRecords = list.size();

		try {
			ObjectNode objNode = mapper.createObjectNode();
			objNode.put("draw", draw);
			objNode.put("recordsFiltered", totalRecords);
			objNode.put("recordsTotal", totalRecords);
			ArrayNode arrayNode = mapper.valueToTree(filteredList);
			objNode.putArray("data").addAll(arrayNode);
			result = mapper.writeValueAsString(objNode);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	public static boolean CheckNull(String value1) {
		boolean isvalid = false;
		if (!value1.equals("") || !value1.equals(null)) {
			isvalid = true;
		}
		return isvalid;
	}

	public static boolean CheckNull(String value1, String value2) {
		boolean isvalid = false;
		if (!value1.equals("") || !value1.equals(null) || !value2.equals("") || !value2.equals(null)) {
			isvalid = true;
		}
		return isvalid;
	}

	public static boolean CheckNull(String value1, String value2, String value3) {
		boolean isvalid = false;
		if (value1.equals("") || value1.equals(null) || value2.equals("") || value2.equals(null) || value3.equals("")
				|| value3.equals(null)) {
			isvalid = true;
		}
		return isvalid;
	}

	public static boolean CheckNull(String value1, String value2, String value3, String value4) {
		boolean isvalid = false;
		if (!value1.equals("") || !value1.equals(null) || !value2.equals("") || !value2.equals(null)
				|| !value3.equals("") || !value3.equals(null) || !value4.equals("") || !value4.equals(null)) {
			isvalid = true;
		}
		return isvalid;
	}

	public static boolean CheckNull(String value1, String value2, String value3, String value4, String value5) {
		boolean isvalid = false;
		if (!value1.equals("") || !value1.equals(null) || !value2.equals("") || !value2.equals(null)
				|| !value3.equals("") || !value3.equals(null) || !value4.equals("") || !value4.equals(null)
				|| !value5.equals("") || !value5.equals(null)) {
			isvalid = true;
		}
		return isvalid;
	}

	public static boolean CheckNull(String value1, String value2, String value3, String value4, String value5,
			String value6) {
		boolean isvalid = false;
		if (!value1.equals("") || !value1.equals(null) || !value2.equals("") || !value2.equals(null)
				|| !value3.equals("") || !value3.equals(null) || !value4.equals("") || !value4.equals(null)
				|| !value5.equals("") || !value5.equals(null) || !value6.equals("") || !value6.equals(null)) {
			isvalid = true;
		}
		return isvalid;
	}

	public static boolean CheckNull(String value1, String value2, String value3, String value4, String value5,
			String value6, String value7) {
		boolean isvalid = false;
		if (!value1.equals("") || !value1.equals(null) || !value2.equals("") || !value2.equals(null)
				|| !value3.equals("") || !value3.equals(null) || !value4.equals("") || !value4.equals(null)
				|| !value5.equals("") || !value5.equals(null) || !value6.equals("") || !value6.equals(null)
				|| !value7.equals("") || !value7.equals(null)) {
			isvalid = true;
		}
		return isvalid;
	}

	public static boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	public static <T> void ExportCSV(HttpServletResponse response, List<T> objects, T object, String reportname)
			throws IOException {

		response.setContentType("text/csv");
		response.setHeader("Content-disposition", "attachment;filename=" + reportname + ".csv");

		ArrayList<String> rows = new ArrayList<String>();

		List<String> columns = GetExportColumns(object);
		String header = "";
		for (int i = 0; i < columns.size(); i++) {
			header += columns.get(i) + (i + 1 == columns.size() ? "" : ",");
		}

		rows.add(header);
		rows.add("\n");

		objects.stream().map(s -> {
			try {
				String cells = GetExportCells(s);
				rows.add(cells);
				rows.add("\n");
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return s;
		}).collect(Collectors.toList());

		Iterator<String> iter = rows.iterator();
		while (iter.hasNext()) {
			String outputString = (String) iter.next();
			response.getOutputStream().print(outputString);
		}

		response.getOutputStream().flush();
	}
	// Table : tabpersistency_raw, splist_persistency_raw

	private static String GetExportColumn(Field field) {
		com.pru.pruquote.utility.ExportBinding annotationValue = field
				.getAnnotation(com.pru.pruquote.utility.ExportBinding.class);
		if (annotationValue != null) {
			return annotationValue.value();
		}
		return null;
	}

	public static String GetExportCells(Object object) throws IllegalArgumentException, IllegalAccessException {
		Class<?> setEntity = object.getClass();
		String row = "";

		for (int i = 0; i < setEntity.getDeclaredFields().length; i++) {
			Field setField = setEntity.getDeclaredFields()[i];
			setField.setAccessible(true);
			if (setField.isAnnotationPresent(com.pru.pruquote.utility.ExportBinding.class)) {
				String value = "";
				if (setField.get(object) != null) {
					value = setField.get(object).toString();

					if (value.contains("\r\n")) {
						value = value.replace('\r', ' ');
						value = value.replace('\n', ' ');
					}
					
					if (value.contains(",")) {
						value = value.replace(',', ';');
					}
				}
				row += value + (i + 1 == setEntity.getDeclaredFields().length ? "" : ",");
			}
		}
		return row;
	}

	public static List<String> GetExportColumns(Object object) {
		Class<?> setEntity = object.getClass();
		List<String> exportColumns = new ArrayList<String>();
		for (Field setField : setEntity.getDeclaredFields()) {
			setField.setAccessible(true);
			String column = GetExportColumn(setField);
			if (column != null)
				exportColumns.add(GetExportColumn(setField));
		}

		return exportColumns;
	}
}
