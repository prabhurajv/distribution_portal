package com.pru.pruquote.gluster;

public class GlusterRequestFailedException extends Exception {

	public GlusterRequestFailedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GlusterRequestFailedException(String arg0, Throwable arg1,
			boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	public GlusterRequestFailedException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public GlusterRequestFailedException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public GlusterRequestFailedException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6868654505109051489L;

}
