package com.pru.pruquote.helper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.pru.pruquote.model.PendingCase;
import com.pru.pruquote.service.PendingCaseService;

@Component
public class PendingCaseServiceHelper {
	@Autowired
	private PendingCaseService pendingCaseService;
	
	public synchronized List<PendingCase> pendingCaseSynchronised(String userid){
		return pendingCaseService.pendingCases(userid);
	}
}
