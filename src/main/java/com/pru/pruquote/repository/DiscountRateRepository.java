package com.pru.pruquote.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pru.pruquote.model.Discountrate;


public interface DiscountRateRepository extends JpaRepository<Discountrate,Long> {
	List<Discountrate> findByCoDiscountRate(Long id);
}
