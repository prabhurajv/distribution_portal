package com.pru.pruquote.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pru.pruquote.model.Modalfactor;


public interface ModalFactorRepository extends JpaRepository<Modalfactor,Long> {
	List<Modalfactor> findByCoModalFactor(Long id);
}
