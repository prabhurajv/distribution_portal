package com.pru.pruquote.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pru.pruquote.model.Log;


public interface LogRepository extends JpaRepository<Log,Integer> {
	List<Log> findByLogDateBetween(Date startTime,Date endTime);
}
