package com.pru.pruquote.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pru.pruquote.model.Itemitem;
import com.pru.pruquote.model.Itemtabl;


public interface ItemitemRepository extends JpaRepository<Itemitem,Long> {
	public Set<Itemitem> findItemitemByItemtabl(Itemtabl itemtabl);
	@Query("SELECT i FROM Itemitem i WHERE i.itemtabl.id=:itemTableId and i.validFlag=1 Order By id")
	public Set<Itemitem> findItemNameByItemtable(@Param("itemTableId") Long itemTableId);
	
	@Query("SELECT i FROM Itemitem i WHERE i.itemtabl.id=:itemTableId and i.validFlag=1 Order By id desc")
	public Set<Itemitem> findItemNameByItemtableDesc(@Param("itemTableId") Long itemTableId);
	
	@Query("SELECT i FROM Itemitem i WHERE i.id in(:param1,:param2,:param3) and i.validFlag=1")
	public Set<Itemitem> findItemNameByItemitemID(@Param("param1") Long param1,@Param("param2") Long param2,@Param("param3") Long param3);
	
	@Query("SELECT i FROM Itemitem i WHERE i.itemtabl.id=:itemTableId and i.reserve1 like :productCode and i.validFlag=1 Order By cast(i.itemLatin as float) desc")
	public Set<Itemitem> findItemNameByItemtablePro(@Param("itemTableId") Long itemTableId,@Param("productCode") String productCode);
	
	public List<Itemitem> findByReserve1AndValidFlag(String reserve1, int validFlag);
}
