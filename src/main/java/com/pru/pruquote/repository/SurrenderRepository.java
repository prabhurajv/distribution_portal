package com.pru.pruquote.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pru.pruquote.model.Surrender;


public interface SurrenderRepository extends JpaRepository<Surrender,Long> {
	List<Surrender> findByCoSurrender(Long id);
}
