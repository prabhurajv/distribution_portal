package com.pru.pruquote.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pru.pruquote.model.Label;


public interface LabelRepository extends JpaRepository<Label,Long> {
}
