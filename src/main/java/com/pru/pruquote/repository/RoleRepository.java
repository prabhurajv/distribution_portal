package com.pru.pruquote.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pru.pruquote.model.Role;


public interface RoleRepository extends JpaRepository<Role,Integer> {
	public Set<Role> findByStatus(int status);
	public Set<Role> findByRoleLVLessThanAndStatus(int roleLV,int status);
}
