package com.pru.pruquote.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.pru.pruquote.model.User;


public interface UserRepository extends JpaRepository<User,String> {	
	
}
