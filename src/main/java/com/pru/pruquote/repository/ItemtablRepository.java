package com.pru.pruquote.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pru.pruquote.model.Itemtabl;



public interface ItemtablRepository extends JpaRepository<Itemtabl,Long> {	
}
