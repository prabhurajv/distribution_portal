package com.pru.pruquote.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pru.pruquote.model.Premiumrate;


public interface PremiumRateRepository extends JpaRepository<Premiumrate,Long> {
	List<Premiumrate> findByCoPremiumRate(int id);
}
