package com.pru.pruquote.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pru.pruquote.model.Mbrate;


public interface MbRateRepository extends JpaRepository<Mbrate,Long> {
	List<Mbrate> findByCoMbrate(Long idLong);
}
