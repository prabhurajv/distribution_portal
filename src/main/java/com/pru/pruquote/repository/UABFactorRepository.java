package com.pru.pruquote.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pru.pruquote.model.UABFactor;


public interface UABFactorRepository extends JpaRepository<UABFactor,Long> {
	List<UABFactor> findByCoUABFactor(Long id);
}
