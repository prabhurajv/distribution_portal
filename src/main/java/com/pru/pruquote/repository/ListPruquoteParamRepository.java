package com.pru.pruquote.repository;

import java.util.List;
import java.util.Set;

import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.StoredProcedureParameter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import com.pru.pruquote.model.User;
import com.pru.pruquote.model.listPruquote;

public interface ListPruquoteParamRepository extends JpaRepository<listPruquote,Long> {	
	@Procedure("spList_Quote")
    List<listPruquote> findByIdAndCoUser(@Param("user") String user,@Param("id") Long id); 
}
