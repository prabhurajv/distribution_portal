package com.pru.pruquote.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pru.pruquote.model.UserHistory;


public interface UserHistoryRepository extends JpaRepository<UserHistory,Integer> {
//	Set<UserHistory> findByUsername(String username);
	Set<UserHistory> findByUserIdPk(long userIdPk);
}
