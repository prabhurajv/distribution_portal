package com.pru.pruquote.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pru.pruquote.model.Role;
import com.pru.pruquote.model.User;
import com.pru.pruquote.model.UserRole;


public interface UserRoleRepository extends JpaRepository<UserRole,Integer> {	
	public Set<UserRole> findByUser(User user);
	public Set<UserRole> findByRole(Role role);
}
