package com.pru.pruquote.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pru.pruquote.model.PruquoteParm;
import com.pru.pruquote.model.listPruquote;


public interface PruquoteParamRepository extends JpaRepository<PruquoteParm,Long> {	
	List<PruquoteParm> findByIdAndProductAndCoUserAndValidFlag(Long id,String product,String user,Integer valid);
	List<PruquoteParm> findByIdAndCoUserAndValidFlag(Long id,String user,Integer valid);
	List<PruquoteParm> findById(Long id);
	
	//public Set<listPruquote> findByIdAndGroupCosUser(Long id,String user);
}
