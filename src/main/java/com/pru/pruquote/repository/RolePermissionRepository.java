package com.pru.pruquote.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pru.pruquote.model.Permission;
import com.pru.pruquote.model.Role;
import com.pru.pruquote.model.RolePermission;


public interface RolePermissionRepository extends JpaRepository<RolePermission,Integer> {	
	public Set<RolePermission> findByPermission(Permission permission);
	public Set<RolePermission> findByRole(Role role);
	
	@Query("SELECT rp FROM RolePermission rp WHERE rp.role.roleId=:roleId "
			+ "AND rp.valid=true")
	public Set<RolePermission> findByRoleValid(@Param("roleId") Integer roleId);
	
	@Query("SELECT rp FROM RolePermission rp WHERE rp.permission.permissionId=:permissionId "
			+ "AND rp.valid=true")
	public Set<RolePermission> findByPermissionValid(@Param("permissionId") Integer permissionId);
}
