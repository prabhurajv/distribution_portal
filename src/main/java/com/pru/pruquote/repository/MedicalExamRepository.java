package com.pru.pruquote.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pru.pruquote.model.Medicalexam;


public interface MedicalExamRepository extends JpaRepository<Medicalexam,Long> {
}
