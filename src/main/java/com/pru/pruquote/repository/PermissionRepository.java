package com.pru.pruquote.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pru.pruquote.model.Permission;


public interface PermissionRepository extends JpaRepository<Permission,Integer> {
	@Query("SELECT p FROM Permission p WHERE p.permissionId "
			+ "Not In(Select rp.permission.permissionId FROM RolePermission rp WHERE rp.role.roleId=:roleId AND rp.valid=true) "
			+ "AND p.status=1")
    public Set<Permission> findNotExistInRole(@Param("roleId") Integer roleId);
}
