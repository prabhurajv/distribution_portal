$(document).ready(function() {
	CountCollectionAndLapsed();
	CountAlteration();
	GetImmediateAction();
	CountPendingAck();
	GetAllPendingSummary();
	SelectSubAndIssue();
	GetPersistency();
	GetFailWelcomeCall();
});
function EasyChart(params) {
	$(params).easyPieChart({
		delay : 3000,
		barColor : '#6170C3',
		trackColor : '#EDEDED',
		scaleColor : false,
		lineWidth : 10,
		trackWidth : 10,
		size : 80,
		onStep : function(from, to, percent) {
			$(this.el).find('.percent').text(Math.round(percent));
		}
	});
}
function GetPersistency() {
	$.ajax({
		url: urlgetpersistenciessumarry,
		type: 'get',
		success: function (result) {
			var persistencysummary = JSON.parse(result);
			$('#includetransferpol').html(persistencysummary[0][0]);
			$('#includetransferpolexposure').html(FormatCurrency('-',persistencysummary[0][1]));
			$('#includetransferpolinfource').html(FormatCurrency('-',persistencysummary[0][2]));
			$('#includetransferpollapseandsurrender').html(FormatCurrency('-',persistencysummary[0][3]));
			
			$('#excludetransferpol').html(persistencysummary[1][0]);
			$('#excludetransferpolexposure').html(FormatCurrency('-',persistencysummary[1][1]));
			$('#excludetransferpolfource').html(FormatCurrency('-',persistencysummary[1][2]));
			$('#excludetransferpollapsedandsurrender').html(FormatCurrency('-',persistencysummary[1][3]));
			
			$('#persistencyforq1').html(FormatPercent('0',persistencysummary[2][0] * 100) + '%');	
			$('#persistencyforq2').html(FormatPercent('0',persistencysummary[2][1] * 100) + '%');
			$('#persistencyforq3').html(FormatPercent('0',persistencysummary[2][2] * 100) + '%');
			$('#persistencyforq4').html(FormatPercent('0',persistencysummary[2][3] * 100) + '%');
		}
	});
	
	$.ajax({
		url: urlpersistencies,
		type: 'get',
		success: function (result) {
			var d = new Date();
			var year = d.getFullYear().toString().substr(2,2);;
			var dataSet = JSON.parse(result);
			if(dataSet[4].length > 0) {
				$('#persistency-q').html('('+ dataSet[4][0].persistencyForQ +')');
				$('#persistency-qwy').html(dataSet[4][0].persistencyForQ +' - ' + year);
				if($('#persistencyforq1').html() === '0%') {
					$('#persistencyforq1').removeClass('lapsed-persistencyq1');
				} else {
					$('#persistencyforq1').click(function() {
						logViewPersistency();	
					});
				}
				$('.lapsed-persistencyq1').attr('href', urlcollectionandlapse +'?ipc=Q1&type=lapsed');
				
				if($('#persistencyforq2').html() === '0%') {
					$('#persistencyforq2').removeClass('lapsed-persistencyq2');
				} else {
					$('#persistencyforq2').click(function() {
						logViewPersistency();	
					});
				}
				
				$('.lapsed-persistencyq2').attr('href', urlcollectionandlapse +'?ipc=Q2&type=lapsed');
				if($('#persistencyforq3').html() === '0%') {
					$('#persistencyforq3').removeClass('lapsed-persistencyq3');
				} else {
					$('#persistencyforq3').click(function() {
						logViewPersistency();	
					});
				}
				
				$('.lapsed-persistencyq3').attr('href', urlcollectionandlapse +'?ipc=Q3&type=lapsed');
				if($('#persistencyforq4').html() === '0%') {
					$('#persistencyforq4').removeClass('lapsed-persistencyq4');
				} else {
					$('#persistencyforq4').click(function() {    
						logViewPersistency();	
					});
				}
				$('.lapsed-persistencyq4').attr('href', urlcollectionandlapse +'?ipc=Q4&type=lapsed');
			}
			$('.years-q').html(year);
			var sumif = dataSet[0];
			var sumla = dataSet[1];
			var sumsu = dataSet[2];
			var percentpersistency = dataSet[3];
			var dataSetTable = dataSet[4];
			var totalpers = sumif + sumla + sumsu;
			$('#chart-persistency').attr('data-percent', FormatPercent('0', percentpersistency));
			EasyChart('#chart-persistency');
			
			$('#sumif').html(FormatCurrency('-',totalpers));
			$('#sumla').html(FormatCurrency('-',sumla));
			$('#sumsu').html(FormatCurrency('-',sumsu));
			var tablepersistency = $('#table-persistency').DataTable();
			tablepersistency.destroy();
			tablepersistency = $('#table-persistency').DataTable({
				data: dataSetTable,
				dom: 'TC<"clearfix">lfrt<"bottom"ip>',
				"columns": [{
						data: 'agntNum'
					},
					{
						data: 'agntName'
					},
					{
						data: 'ifApe',
						render: $.fn.dataTable.render.number( ',', '.', 2 )
					},
					{
						data: 'laApe',
						render: $.fn.dataTable.render.number( ',', '.', 2 )
					},
					{
						data: 'suApe',
						render: $.fn.dataTable.render.number( ',', '.', 2 )
					},
					{
						data: 'percentForQ'
					},
					{
						data: 'persistencyForQ'
					}
				],
				"columnDefs": [
				               { className: "currency", "targets": [ 2,3,4 ] },
				               { className: "pru-percent", "targets": [ 5,6 ] }
				             ],
				"fnDrawCallback": function (oSettings, json) {
					//showLoading(false);
				}
			});
		}
	});
}
function GetAllPendingSummary() {
	$.ajax({
		url: urlgetallpendingsummary,
		type: 'get',
		data: {summaryby: 'code'},
		success: function(result) {
			var obj = JSON.parse(result);
			var totalcountpending = "";
			var refreshtime = "";
			var totalape = "";
			
			$.each(obj, function( key, value) {
				if(value[0] == "refreshtime") {
					refreshtime = value[1];
				} else if(value[0] == "totalpending") {
					totalcountpending = value[1];
					totalape = value[2];
				}
			});
			$('#myAllPending').html('<span class="counter">'+totalcountpending+'</span>');
			$('#totalAPE').html('$ <span class="counter">' + (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '</span>');
			$('#refershAt').html(refreshtime);
			SelectCodeDes();
			$('.right_col').loading('stop');
		}
	});
}
function CountPendingAck() {
	$.ajax({
		url: urlcountpendingacks,
		type: 'get',
		success: function (result) {
			var pendingack = JSON.parse(result);
			$('#my-pending-ack').html(pendingack);
		}
	});
}
function GetImmediateAction() {
	$.ajax({
		url: urlgetimmediateaction,
		type: 'get',
		success: function (result) {
			var objimmediateaction = JSON.parse(result);
			immediateaction = '';
			$.each(objimmediateaction, function (key, value) {
				if(value[3] === 'REFFLUP') {
					immediateaction += '<a style="cursor:pointer;" OnClick="ShowDialogCustomertoFollowUp()" class="list-group-item">' +
					'<span class="badge" style="background-color:#ed1b2e;">'+ value[1] +'</span>'+ value[0] +''+
				'</a>';
				}
				else if(value[3] == 'IF' || value[3] == 'LA')
				{
					var type = value[3] == 'IF' ? 'collection' : 'lapsed';
					immediateaction += '<a  href="'+urlcollectionandlapse+'?podcsn='+value[0]+'&type='+ type +'" class="list-group-item">' +
					'<span class="badge" style="background-color:#ed1b2e;">'+ value[1] +'</span>'+ value[0] +''+
					'</a>';
				}
				else {
					immediateaction += '<a  href="'+urlmypendingstatus+'?status='+value[3]+'&day='+ value[2] +'" class="list-group-item">' +
					'<span class="badge" style="background-color:#ed1b2e;">'+ value[1] +'</span>'+ value[0] +''+
				'</a>';
				}
			});
			$('.immediate-action').append(immediateaction);
		}
	});
}
function ShowDialogCustomertoFollowUp() {
	showModalCustomerToFollowUpDialog.show();
}
function CountAlteration() {
	$.ajax({
		url : urlcountalteration,
		type : 'get',
		success : function(result) {
			var objalteration = JSON.parse(result);
			var completed = objalteration[0];
			var pending = objalteration[1];
			$('#alcompleted').html(completed);
			$('#alpending').html(pending);
			$('.label-completed').html('<i class="fa fa-square green"></i> Completed');
			$('.label-pending').html('<i class="fa fa-square blue "></i> Pending');

			// Donut Chart
			var options = {
				legend : false,
				responsive : false,
				tooltips: {
			         enabled: false
			    }
			};
			new Chart(document.getElementById("donutalteration"), {
				type : 'doughnut',
				tooltipFillColor : "rgba(51, 51, 51, 0.55)",
				data : {
					labels : [ "Completed", "Pending" ],
					datasets : [ {
						data : [ completed, pending ],
						backgroundColor : [ "#26B99A", "#3498DB" ],
						hoverBackgroundColor : [ "#36CAAB", "#49A9EA" ]
					} ]
				},
				options : options
			});
		}
	});
}
function CountCollectionAndLapsed() {
	$.ajax({
		url : urlcountcollectionandlapse,
		type : 'get',
		success : function(result) {
			var countcollectionandlaps = JSON.parse(result);
			var countcollection = countcollectionandlaps[0];
			var countlapse = countcollectionandlaps[1];
			var sumapecollectioin = countcollectionandlaps[2];
			var sumapelapse = countcollectionandlaps[3];
			$('#policy-collection').html(countcollection);
			$('#policy-lapse').html(countlapse);
			$('#ape-collection').html(FormatCurrency('-', sumapecollectioin));
			$('#ape-lapse').html(FormatCurrency('-', sumapelapse));
		}
	});
}
function SelectSubAndIssue() {
	$.ajax({
		url: urlgetsubandiss,
		type: 'get',
		success: function (result) {
				var dataSet = JSON.parse(result);
				var subandissue = [];
				var sub = [];
				var issue = [];

				$.each(dataSet, function (key, value) {
					if(value[0] === 'Today' || value[0] === 'Yesterday' || value[0] === 'Month to Date') {
						subandissue.push({
							rowheader: value[0],
							issuepolicy: value[1],
							totalissueape: value[2],
							targetissue: value[3],
							submission: value[4],
							totalsubmissioinape: value[5],
							targetsub: value[6]
						});
					} else if(value[0].substr(0,4) === 'Sub.' && value[1] !== "W6") {
						sub.push({
							rowheader: value[1],
							subpolicy: value[2],
							totalsubape: value[3],
							targetape: value[4],
							achievement: value[5]
						});
					} else if(value[0].substr(0,4) === 'Iss.'  && value[1] !== "W6") {
						issue.push({
							rowheader: value[1],
							issuepolicy: value[2],
							totalissueape: value[3],
							targetape: value[4],
							achievement: value[5]
						});
					}
				});
				
				var trsub = '';
				var trissue = '';
				var targetsubmission = '';
				var targetissuance = '';
				$.each(subandissue, function (key, value) {
					trsub += '<a style="cursor: pointer;" onClick="showSubmissionPolicyDetail(\''+value.rowheader+'\')">' +
								'<div class="pending-dashboard" style="padding: 0;">'+
									'<div class="ibox-title">'+
										'<h5 class="label label-danger lbl-color">'+ value.rowheader +'</h5>'+
									'</div>'+
									'<div class="ibox-content">'+
										'<div class="ibox-content-detail-left">'+
											'<p>#Case</p>'+
											'<h1>'+ value.submission +'</h1>'+
										'</div>'+
										'<div class="ibox-content-detail-right">'+
											'<p>Total APE</p>'+
											'<h1>'+ FormatCurrency('-',value.totalsubmissioinape) +'</h1>'+
										'</div>'+
										'<div class="clearfix"></div>'+
									'</div>'+
								'</div>'+
							'</a>';
					
					trissue += '<a style="cursor: pointer;" onClick="showIssuePolicyDetail(\''+value.rowheader+'\')">' +
									'<div class="pending-dashboard" style="padding: 0;">'+
										'<div class="ibox-title">'+
											'<h5 class="label label-danger lbl-color">'+ value.rowheader +'</h5>'+
										'</div>'+
										'<div class="ibox-content">'+
											'<div class="ibox-content-detail-left">'+
												'<p>#Case</p>'+
												'<h1>'+ value.issuepolicy +'</h1>'+
											'</div>'+
											'<div class="ibox-content-detail-right">'+
												'<p>Total APE</p>'+
												'<h1>'+ FormatCurrency('-',value.totalissueape) +'</h1>'+
											'</div>'+
											'<div class="clearfix"></div>'+
										'</div>'+
									'</div>'+
								'</a>';
							
					if(value.rowheader === 'Month to Date') {
						targetissuance = value.targetissue;
						targetsubmission = value.targetsub;
						$('#ac-submission').html(FormatCurrency('-',value.totalsubmissioinape));
						$('#ac-issuance').html(FormatCurrency('-',value.totalissueape));
					}
				});
				
				var trsubmission = '';
				$.each(sub, function (key, value) {
					trsubmission += '<tr>' +
											'<td>'+value.rowheader+'</td>' +
											'<td>'+value.subpolicy+'</td>' +
											'<td class="currency">'+ FormatCurrency('-', value.totalsubape)+'</td>' +
											'<td class="currency">'+FormatCurrency('-',value.targetape)+'</td>' +
											'<td>'+value.achievement+'%</td>' +
											'<td><span class="modal-sub label label-danger span-cursor">Detail</span></td>' +
									'</tr>';
				});
				
				var trnetissue = '';
				$.each(issue, function (key, value) {
					trnetissue += '<tr>' +
											'<td>'+value.rowheader+'</td>' +
											'<td>'+value.issuepolicy+'</td>' +
											'<td class="currency">'+FormatCurrency('-',value.totalissueape)+'</td>' +
											'<td class="currency">'+FormatCurrency('-',value.targetape)+'</td>' +
											'<td>'+value.achievement+'%</td>' +
											'<td><span class="modal-iss label label-danger span-cursor">Detail</span></td>' +
									'</tr>';
				});
				
				$('#tar-submission').html(FormatCurrency('-',targetsubmission));
				$('#tar-issuance').html(FormatCurrency('-',targetissuance));
				$('#row-sub').append(trsub);
				$('#row-issuenance').append(trissue);
				$('#table-submission>tbody').html(trsubmission);
				$('#table-netissue>tbody').html(trnetissue);
				
				var subpercentage = ((subandissue[2].totalsubmissioinape* 1) / (subandissue[2].targetsub * 1)) * 100;
				var isspercentage = ((subandissue[2].totalissueape * 1) / (subandissue[2].targetissue * 1)) * 100;
				$('#sub-percent').attr('data-percent', FormatPercent('0',subpercentage));
				EasyChart('#sub-percent');
				$('#iss-percent').attr('data-percent', FormatPercent('0',isspercentage));
				EasyChart('#iss-percent');
			
//					$('.showIssuePolicyDetail').click(function() {
//						var iss = $(this);
//						var weekth = iss.parent().parent().children().first().text().trim();
//						showModalIssuanceDialog.show(weekth);
//					});
				
//					$('.showSubmissionPolicyDetail').click(function() {
//						var sub = $(this);
//						var weekth = sub.parent().parent().children().first().text().trim();
//						showModalSubmissionDialog.show(weekth);
//					});
			
				
				$('.modal-sub').click(function() {
					var sub = $(this);
					var weekth = sub.parent().parent().children().first().text().trim();
					showModalSubmissionDialog.show(weekth);
				});
				
				$('.modal-iss').click(function() {
					var iss = $(this);
					var weekth = iss.parent().parent().children().first().text().trim();
					showModalIssuanceDialog.show(weekth);
				});
		},
		error: function () {
			console.log("Error");
			SelectListSubmission();
		}
	});
}
function SelectCodeDes() {
	$.ajax({
		url : urlcodedesc,
		type : 'get',
		success : function(result) {
			var dataSet = JSON.parse(result);
			var tabcodedecs = "";
			$.each(dataSet, function(key, value) {
				tabcodedecs += '<tr><td>' + value.code + '</td><td>'
						+ value.codeDesc + '</td><td>' + value.codeDetail
						+ '</td></tr>';
			});
			$('#codedesc_grid>tbody').append(tabcodedecs);
		}
	});
}
function showAlterationDetail(params) {
	//Show Alteration Detail
	$.ajax({
		url : urlgetallalteration,
		type : 'get',
		success : function(result) {
			var dataSet = JSON.parse(result);

			if (params === 'Pending') {
				var tablealteration = dataSet[1];
			} else {
				var tablealteration = dataSet[0];
			}
			showModalAlterationDialog.show(tablealteration);
		}
	});

}
function ShowListPendingAck() {
	showModalPendingAckDialog.show();
}
function showSubmissionPolicyDetail(params) {
	showModalSubmissionDialog.show(params);
}
function showIssuePolicyDetail(params) {
	showModalIssuanceDialog.show(params);
}
function ShowHelpCollectionandLapse() {
	showModalHelpCollectionandLapseCommentDialog.show();
}
function ShowHelpMySalePerformUpdate() {
	showModalHelpMySalePerformanceDialog.show();
}
function GetFailWelcomeCall() {
	$.ajax({
		url: urlfailwelcomecall,
		type: 'get',
		success: function(result) {
			var fwc = JSON.parse(result);
			var closed = 0;
			var pending = 0;
			$.each(fwc, function(key, value) {
				if (value.finalCalledStatus === 'Closed') {
					closed += 1;
				} else {
					pending += 1;
				}
			});
			$('#fwcClosed').html(closed);
			$('#fwcPending').html(pending);
			
			$('.label-fwc-closed').html('<i class="fa fa-square green"></i> Closed');
			$('.label-fwc-pending').html('<i class="fa fa-square blue "></i> Pending');

			// Donut Chart
			var options = {
				legend : false,
				responsive : false,
				tooltips: {
			         enabled: false
			    }
			};
			new Chart(document.getElementById("donutfwc"), {
				type : 'doughnut',
				tooltipFillColor : "rgba(51, 51, 51, 0.55)",
				data : {
					labels : [ "Closed", "Pending" ],
					datasets : [ {
						data : [ closed, pending ],
						backgroundColor : [ "#26B99A", "#3498DB" ],
						hoverBackgroundColor : [ "#36CAAB", "#49A9EA" ]
					} ]
				},
				options : options
			});
		}
	});
}
function showFwcDetail(param) {
	showModalFailedWelcomeCall.show(param);
}
function logViewPersistency() {
	$.ajax({
		url: urllogviewpersistency,
		type: 'GET'
	});
}