$(function() {
		if(typeof String.prototype.trim !== 'function') {
		  String.prototype.trim = function() {
		    return this.replace(/^\s+|\s+$/g, ''); 
		  }
		}
		function convertDate(dateString){
			var p = dateString.split(/\D/g)
			return [p[2],p[1],p[0] ].join("/")
		}

		var currentDate = new Date();
	    $('#trnDateTime').datepicker({
	    	useCurrent: true,
		    pickTime: false,
		    dateFormat: "dd/mm/yy"
		});
	    
	    if(!$('#trnDateTime').val()){
	    	$('#trnDateTime').datepicker("setDate", currentDate);
	    }else{
	    	$('#trnDateTime').val(convertDate($('#trnDateTime').val()));
	    }
});

function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode != 46 && charCode > 31 
     && (charCode < 48 || charCode > 57))
      return false;

   return true;
}

function checkSubmit(){
	Err.innerHTML="";
	
	var staffId="";
	staffId=$("#staffId").val();
	
	var staffName="";
	staffName=$("#staffName").val();
	
	if(!staffName){
		Err.innerHTML="";
    	Err.innerHTML=Err.innerHTML+"<br/>Please fill in Staff Name!";
    	document.getElementById("staffName").style.background = "yellow";
    	document.getElementById("staffName").focus();
    	$('html, body').animate({ scrollTop: 0 }, 'slow');
    	$('[href="#info"]').tab('show');
    	return false;
	}
	else{
		document.getElementById("staffName").style.background = "white";
	}
	
	var staffBranch = "";
	staffBranch = $("#staffBranch").val();
	if(!staffBranch){
		Err.innerHTML="";
    	Err.innerHTML=Err.innerHTML+"<br/>Please select Staff Branch!";
    	document.getElementById("staffBranch").style.background = "yellow";
    	$('html, body').animate({ scrollTop: 0 }, 'slow');
    	$('[href="#info"]').tab('show');
    	return false;
	}else{document.getElementById("staffBranch").style.background = "white"; }
	
	var staffPosition = "";
	staffPosition = $("#staffPosition").val();
	if(!staffPosition){
		Err.innerHTML="";
    	Err.innerHTML=Err.innerHTML+"<br/>Please fill in Staff Position!";
    	document.getElementById("staffPosition").style.background = "yellow";
    	$('html, body').animate({ scrollTop: 0 }, 'slow');
    	$('[href="#info"]').tab('show');
    	return false;
	}else{document.getElementById("staffPosition").style.background = "white"; }
	
    
	if(staffName && staffBranch && staffPosition){
    	//document.form.submit();
    	return true;
    }else{
    	return false;
    }
}

function checkSubmitComment(){
	var cusname="";
	Err.innerHTML="";
	var appNum = "";
	var appDate = "";
	var remark = "";
	var cid = "";
	
	cusname=document.getElementById("cusName").value;
	appNum = document.getElementById("appNum").value;
	appDate = document.getElementById("appDate").value;
	remark = document.getElementById("remark").value;
	cid = document.getElementById("cid").value;
	
//	if(cusname==""){
//		Err.innerHTML="";
//    	Err.innerHTML=Err.innerHTML+"<br/>Please input customer name and phone number ";
//    	document.getElementById("cusName").style.background = "yellow";
//    	document.getElementById("cusPhone").style.background = "yellow";
//    	$('html, body').animate({ scrollTop: 0 }, 'slow');
//    	return false;
//	}
//    else if(appNum == ""){
//    	Err.innerHTML="";
//    	Err.innerHTML=Err.innerHTML+"<br/>Please input application number!";
//    	document.getElementById("appNum").style.background = "yellow";
//    	$('html, body').animate({ scrollTop: 0 }, 'slow');
//    	return false;
//    }
//    if(appDate == ""){
//    	Err.innerHTML="";
//    	Err.innerHTML=Err.innerHTML+"<br/>Please select appointment date!";
//    	document.getElementById("appDate").style.background = "yellow";
//    	$('html, body').animate({ scrollTop: 0 }, 'slow');
//    	return false;
//    }
   if(cid == "0" || cid == ""){
    	Err.innerHTML="";
    	Err.innerHTML=Err.innerHTML+"<br/>Please select comment type!";
    	document.getElementById("cid").style.background = "yellow";
    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
    }
    else if(remark == ""){
    	Err.innerHTML="";
    	Err.innerHTML=Err.innerHTML+"<br/>Please input remark!";
    	document.getElementById("remark").style.background = "yellow";
    	$('html, body').animate({ scrollTop: 0 }, 'slow');
    	return false;
    }
    else{
    	document.form.submit();
    	return true;
    }
}