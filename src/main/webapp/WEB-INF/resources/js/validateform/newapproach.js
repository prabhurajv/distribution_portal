$(document).ready(function() {
	// Auto Format New Date Today
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1;
	var yyyy = today.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}
	if (mm < 10) {
		mm = '0' + mm;
	}
	var today = dd + '/' + mm + '/' + yyyy;
	
	$(".select2_multiple").select2({
		placeholder: "Related Bank Products",
		allowClear: true
	});
	
	// Filed Select Introducer on Information Tab
	$('#referralBy').select2();
	
	//Field Select Branch on Information Tab
	$(".select2_single").select2({
      placeholder: "Branch Code - Name"
    });
	
	// Call GetReferrer Function
	getReferrer($('#branch').val(), formstatus, $('#referralByHidden').val());
	
	// Event OnChange on Branch on Information Tab
	$('#branch').change(function() {
		getReferrer($('#branch').val(), formstatus, $('#referralByHidden').val());
	});
	
	// Event OnChange on Marital Status on Information Tab
	$('#cusMStatus').change(function() {
		var cusMStatus = "";
		cusMStatus = $('#cusMStatus').val();
		if (cusMStatus == "Single") {
			$('#cusNochd').val('0');
			$('#cusNochd').prop('disabled', true);
		} else {
			$('#cusNochd').prop('disabled', false);
		}
	});
	
	// Disable Bank Product Introduced by FC in Information Tab
	var referralAcbProductNew = [];
	var value1 = [];
	var value2 = [];
	$.each(nameDescFormat, function(index, value) {
		var tmp = value.split('-')[0];
		if (tmp === 'cusappby') {
			value1.push(value);
		}
		if (tmp === 'comment') {
			value2.push(value);
		}
	});
	$.each(value1, function(index, value) {
		if ($('#' + value).val() === 'No' || $('#' + value).val() === "") {
			$.each(value2, function(index1, value3) {
				if (value3.split('-')[1] === value.split('-')[1]) {
					$('#' + value3).val('0');
					$('#' + value3).attr("disabled", true); 
				}
			});
		} else {
			$.each(value2, function(index1, value3) {
				if (value3.split('-')[1] === value.split('-')[1]) {
					$('#' + value3).attr("disabled", false); 
				}
			});
		}
		$('#' + value).change(function(){
			if ($('#' + value).val() === 'No' || $('#' + value).val() === "") {
				$.each(value2, function(index1, value3) {
					if (value3.split('-')[1] === value.split('-')[1]) {
						$('#' + value3).val('0');
						$('#' + value3).attr("disabled", true); 
					}
				});
			} else {
				$.each(value2, function(index1, value3) {
					if (value3.split('-')[1] === value.split('-')[1]) {
						$('#' + value3).attr("disabled", false); 
					}
				});
			}
		});
	});
});

// Submit the Form (Save)

$('#btnsave').click(function(){
	checkSubmit();
});
function checkSubmit() {
	if (!checkValidationInfoTab()) {return false;}
	if (!checkValidationProduct()) {return false;}
	$("#btnsave").prop("disabled", true);
	var referralAcbProductNew = [];
	$.each(nameDescFormat, function(index, value) {
		referralAcbProductNew.push($('#'+value).val());
	});
	$('form[name = form]').append('<input type="hidden" value="' + referralAcbProductNew + '" name="referralAcbProductNew" />');
	document.form.submit();
}

function getReferrer(branchCode, formstatus, referralby) {
	$.ajax({ 
		url: urlgetreferrersbyagntnumandbranchcode,
		type: 'get',
		data: { branchCode: branchCode},
		success: function(result) {
			var obj = [];
			obj.push({
				id: '0',
				text: '-- Select Referrer --'
			});
			var referrers = JSON.parse(result);
			for (var i = 0; i < referrers.length; i++) {
			    var obj2 = {
			    		id: referrers[i][0],
			    		text: referrers[i][1]
			    };
			    obj.push(obj2);
			}
			$(".select2-referrer").html('');
			$(".select2-referrer").select2('data', null);
			$(".select2-referrer").select2({
				data: obj
			}).trigger('change');
			if (referralby != null && referralby != '') {
				$('.select2-referrer').val(referralby).trigger('change');
				if ($('.select2-referrer').val() == null) {
					$('.select2-referrer').val('0').trigger('change');
				}
			}
		}
	});
}
function checkValidationProduct() {
	isvalid = true;
	var value1 = [];
	var value2 = [];
	$.each(nameDescFormat, function(index, value) {
		var tmp = value.split('-')[0];
		if (tmp === 'comment') {
			value2.push(value);
		}
		if (tmp === 'cusappby') {
			value1.push(value);
		}
	});

	$.each(value1, function(index, value4) {
		if ($('#'+value4).val() === '') {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please select the filed";
			document.getElementById(value4).style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			$('[href="#info"]').tab('show');
			isvalid = false;
		} else {
			document.getElementById(value4).style.background = "white";
		}
		if ($('#'+value4).val() === 'Yes') {
			$.each(value2, function(index, value5) {
				if (value4.split('-')[1] === value5.split('-')[1]) {
					if ($('#' + value5).val() === '0') {
						Err.innerHTML = "";
						Err.innerHTML = Err.innerHTML
								+ "<br/>Please select the filed";
						document.getElementById(value5).style.background = "yellow";
						$('html, body').animate({
							scrollTop : 0
						}, 'slow');
						$('[href="#info"]').tab('show');
						isvalid = false;
					} else {
						document.getElementById(value5).style.background = "white";
					}
				}
			});
		}
	});
	return isvalid;
}
function checkValidationInfoTab() {
	var isvalid = false;
	// Validate Referral Type
	var rType = "";
	rType = $("#rType").val();
	if (rType == "0") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please select referral type";
		document.getElementById("rType").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("rType").style.background = "white";
	}
	
	// Validate Branch
	var branch = "";
	branch = $('#branch').val();
	if (branch == "0") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please select branch";
		document.getElementById("select2-branch-container").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("select2-branch-container").style.background = "white";
	}
	
	// Validate Introducer
	var referralBy = "";
	referralBy = $('#referralBy').val();
	if (referralBy == "0") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please select introducer";
		document.getElementById("select2-referralBy-container").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("select2-referralBy-container").style.background = "white";
	}
	
	// Validate Customer Full Name
	var cusname = "";
	cusname = document.getElementById("cusName").value.trim();
	if (cusname == "") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML + "<br/>Please fill customer full name!";
		document.getElementById("cusName").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("cusName").style.background = "white";
	}
	
	// Validate Customer Age
	var cusage = "";
	cusage = document.getElementById("cusAge").value;
	if (cusage == "") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML + "<br/>Please fill customer age";
		document.getElementById("cusAge").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("cusAge").style.background = "white";
	}

	// Validate Mobile Number
	var cusPhone = "";
	cusPhone = document.getElementById("cusPhone").value;
	if (cusPhone == "") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please fill customer phone number!";
		document.getElementById("cusPhone").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else if (cusPhone.length < 9 || cusPhone.length > 10) {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML + "<br/>Phone number is not valid !";
		document.getElementById("cusPhone").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("cusPhone").style.background = "white";
	}

	// Validate Occupation
	var cusOcc = "";
	cusOcc = document.getElementById("cusOcc").value;
	if (cusOcc == "0") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please select customer occupation!";
		document.getElementById("cusOcc").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("cusOcc").style.background = "white";
	}
	
	if(rType == "0" || branch == "0" || referralBy == "0" || cusname == "" || cusage == "" || cusPhone == "" || cusPhone.length < 9 || cusPhone.length > 10 || cusOcc == "0") {
		isvalid = false;
	} else {
		isvalid = true;
	}
	return isvalid;
}