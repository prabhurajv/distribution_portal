$(document).ready(function() {
	// GetCollectionAndLapse();
	if (type == 'collection') {
		$('[href="#renewalpremiumcollection"]').tab('show');
	}

	$('#dob').datepicker({
		useCurrent : true,
		pickTime : false,
		dateFormat : "dd/mm/yy"
	});

	GetCollection(urlgetothercollection, 'init', null);
});

$('#dob,#poappnum').keypress(function(e) {
	var key = e.which;
	if (key == 13) // the enter key code
	{
		$('#btnclsearch').click();
		return false;
	}
});

$('#btnclsearch').click(function(e) {
	GetCollection(urladvancesearchcollection, 'reload', $('form#colform'));
});
$('#btnclclear').click(function(e) {
	document.getElementById("colform").reset();
	GetCollection(urlgetothercollection, 'clear', $('form#colform'));
});


var handledAction = "init";
function GetCollection(urlgetcollections, action, form) {
	handledAction = action;
	if (action == 'reload') {
		var poappnum = $(form).find('#poappnum').val();

		if (poappnum == null || poappnum == '') {
			alert("Policy Number or Application Number is invalid");
			return;
		}
		urlgetcollections = urlgetcollections + '?poappnum=' + poappnum;

		var dob = $(form).find('#dob').val();
		if (dob == null || dob == '') {
			alert("Date of birth is invalid");
			return;
		} else {
			var m = moment(dob, 'DD-MM-YYYY');
			if (!m.isValid()) {
				alert("Date of birth is invalid");
				return;
			}
		}

		urlgetcollections = urlgetcollections + '&dob=' + dob;

		var collectiontable = $('#collection_grid').DataTable();
		collectiontable.ajax.url(urlgetcollections).load();

		return;
	}

	if (action == 'clear') {
		var poappnum = $(form).find('#poappnum').val();
		urlgetcollections = urlgetcollections + '?poappnum=' + poappnum;

		var dob = $(form).find('#dob').val();
		urlgetcollections = urlgetcollections + '&dob=' + dob;

		var collectiontable = $('#collection_grid').DataTable();
		collectiontable.ajax.url(urlgetcollections).load();

		return;
	}

	var collectiontable = $('#collection_grid')
			.DataTable(
					{
						processing : true,
						serverSide : true,
						"searching" : false,
						"ordering" : false,
						ajax : {
							url : urlgetcollections,
							type : 'get'
						},
						"columns" : [
								{
									data : 'chdrNum'
								},
								{
									data : 'chdrAppnum'
								},
								{
									data : 'poName'
								},
								{
									data : 'dob'
								},
								{
									data : 'next_prem'
								},
								{
									data : 'out_amt'
								},
								{
									data : 'occ_date'
								},
								{
									data : 'next_payment_date'
								},
								{
									data : 'stnd_ord_end_date'
								},
								{
									data : 'so_cancel_status'
								},
								{
									data : 'tele_reinstate'
								},
								{
									data : 'objKey'
								},
								{
									data : 'objType'
								},
								{
									data : '',
									defaultContent : '<a class="DownloadCsv" id="download_csv" style="cursor:pointer;">'
											+ '<span class="label label-danger">'
											+ 'Download' + '</span>' + '</a>'
								} ],
						columnDefs : [
								{
									targets : -1,
									className : "middle-algin",
									createdCell : function(td, cellData,
											rowData, row, col) {
										var btnDownload = $(td).children()
												.first();
										$(btnDownload)
												.click(
														function(e) {
															e.preventDefault();
															var objKey = $(this)
																	.parent()
																	.parent()
																	.find(
																			'.objKey')
																	.text()
																	.trim();
															var url = urlcsvadvancesearchothercollection
																	+ "?objkey="
																	+ objKey;
															window.open(url);
														});
									}
								},
								{
									targets : 0,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('chdrNum');
									}
								},
								{
									targets : 1,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('chdrAppnum');
									}
								},
								{
									targets : 2,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('poName');
									}
								},
								{
									targets : 3,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('dob');
									}
								},
								{
									targets : 4,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('next_prem');
									}
								},
								{
									targets : 5,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('out_amt');
									}
								},
								{
									targets : 6,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('occ_date');
									}
								},
								{
									targets : 7,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('next_payment_date');
									}
								},
								{
									targets : 8,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('stnd_ord_end_date');
									}
								},
								{
									targets : 9,
									className : "middle-algin",
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('so_cancel_status');
									}
								},
								{
									targets : 10,
									className : "middle-algin",
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('tele_reinstate');
									}
								},
								{
									targets : 11,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('objKey');
										$(td).css('display', 'none');
									}
								},
								{
									targets : 12,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('objType');
									}
								} ],
						"fnDrawCallback" : function(oSettings, json) {
							if (oSettings.aoData.length == 0 && handledAction == 'reload')
								alert('This policy is not found');
						}
					});
}
