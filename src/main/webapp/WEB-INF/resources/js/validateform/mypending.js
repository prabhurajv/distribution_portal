$(document).ready(function () {
	listpendings = []; 
	$('#colapeto').hide();
	$('#colchdrkeydateto').hide();
	$('#colntudateto').hide();
	$('#colpendingperiodto').hide();
	$('.collapse-link').trigger( "click" );
	$('#tags_search').tagsInput({
		width: 'auto'
	});
	$('#tags_search_addTag').hide();
	$('#tags_search_box').hide();
	
	$('#chdrkeydate').datepicker({
		useCurrent: true,
		pickTime: false,
		dateFormat: "dd/mm/yy"
	});

	$('#chdrkeydateto').datepicker({
		useCurrent: true,
		pickTime: false,
		dateFormat: "dd/mm/yy"
	});

	$('#ntudate').datepicker({
		useCurrent: true,
		pickTime: false,
		dateFormat: "dd/mm/yy"
	});

	$('#ntudateto').datepicker({
		useCurrent: true,
		pickTime: false,
		dateFormat: "dd/mm/yy"
	});

	$('#apeoperator').change(function () {
		if ($('#apeoperator').val() == '6') {
			$('#colapeto').show();
		} else {
			$('#colapeto').hide();
		}
	});

	$('#chdrkeydateoperator').change(function () {
		if ($('#chdrkeydateoperator').val() == '6') {
			$('#colchdrkeydateto').show();
		} else {
			$('#colchdrkeydateto').hide();
		}
	});

	$('#ntudateoperator').change(function () {
		if ($('#ntudateoperator').val() == '6') {
			$('#colntudateto').show();
		} else {
			$('#colntudateto').hide();
		}
	});

	$('#pendingperiodoperator').change(function () {
		if ($('#pendingperiodoperator').val() == '6') {
			$('#colpendingperiodto').show();
		} else {
			$('#colpendingperiodto').hide();
		}
	});

	$('#btnSearch').click(function () {
		listpendings = [];
		$('#tags_search_box').show();
		var policynumber = $('#policynumber').val();
		CheckPendingShort(policynumber,"PolicyNumber = ","policynumber");
		
		var applicationnumber =  $('#applicationnumber').val();
		CheckPendingShort(applicationnumber,"ApplicationNumber = ","applicationnumber");

		var clientname = $('#clientname').val();
		CheckPendingShort(clientname,"ClientName= ","clientname");
		
		var ape = $('#ape').val();
		var apeto = $('#apeto').val();
		var apeoperator = $('#apeoperator option:selected').text();
		CheckPending(ape,apeto,apeoperator,"APE from ","APE ","ape","apeoperator","apeto");
		
		var status = $('#status').val();
		CheckPendingShort(status,"Status = ","status");
		
		var policyphonenumber = $('#policyphonenumber').val();
		CheckPendingShort(policyphonenumber,"PolicyPhoneNumber = ","policyphonenumber");
		
		var agentnumber = $('#agentnumber').val();
		CheckPendingShort(agentnumber,"AgentNumber = ","agentnumber");
		
		var agentname = $('#agentname').val();
		CheckPendingShort(agentname,"AgentName = ","agentname");
		
		var agentphone = $('#agentphone').val();
		CheckPendingShort(agentphone,"AgentPhone = ","agentphone");
		
		var paneldoctor = $('#paneldoctor').val();
		CheckPendingShort(paneldoctor,"PanelDoctor = ","paneldoctor");
		
		var chdrkeydate = $('#chdrkeydate').val();
		var chdrkeydateto = $('#chdrkeydateto').val();
		var chdrkeydateoperator = $('#chdrkeydateoperator option:selected').text();
		CheckPending(chdrkeydate,chdrkeydateto,chdrkeydateoperator,"CHDRKeyDate from ","CHDRKeyDate ","chdrkeydate","chdrkeydateoperator","chdrkeydateto");
		
		var ntudate = $('#ntudate').val();
		var ntudateto = $('#ntudateto').val();
		var ntudateoperator = $('#ntudateoperator option:selected').text();
		CheckPending(ntudate,ntudateto,ntudateoperator,"NTUDATE from ","NTUDATE ","ntudate","ntudateoperator","ntudateto");
		
		var pendingperiod = $('#pendingperiod').val();
		var pendingperiodto = $('#pendingperiodto').val();
		var pendingperiodoperator = $('#pendingperiodoperator option:selected').text();
		CheckPending(pendingperiod,pendingperiodto,pendingperiodoperator,"PendingPeriod from ","PendingPeriod ","pendingperiod","pendingperiodoperator","pendingperiodto");
		
		$('#tags_search_tagsinput').find("span").remove();
		
		$.each( listpendings, function( key, value) {
			onAddTag(value.name , value.param, key);
		});
	});
	
	if(status != "null") {
		if(status === 'NTUDATE') {
			var arrdays = days.split('-');
			$('#ntudate').val(arrdays[0]);
			$('#ntudateto').val(arrdays[1]);
			$('#ntudateoperator').val('6');
		} else {
			$('#status').val(status);
			$('#pendingperiod').val(days);
			$('#pendingperiodoperator').val('4');
		}
		$('#btnSearch').trigger( "click" );
	} else {
		SelectListPending();
	}
});
function CheckPendingShort(value,tag,param) {
	if(!value == ""){
		var name = tag+value;
		var arrobj = {name: name, param: [param]};
		listpendings.push(arrobj);
	}
}
function CheckPending(value, valueto, operator, tag, name1,param,param1,param2) {
	if(!value == ""){
		if(operator == "between"){
			var name = tag + value +" to "+ valueto;
			var arrobj = {name: name, param: [param,param1,param2]};
			listpendings.push(arrobj);
		} else{
			var name = name1 + operator +" "+ value;
			var arrobj = {name: name, param: [param,param1]};
			listpendings.push(arrobj);
		}
	}
}
//=========Tags=========
function onAddTag(name,param,key) {
	$('#tags_search_tagsinput').append('<span id="'+key+'" class="tag"><span data-toggle="modal" data-target=".bs-example-modal-sm">' + name + ' </span> &nbsp;&nbsp; <a style="font-size:16px;" href="#" onclick="onRemoveTag(\''+ key + '\',\'' +param+'\')">x</a> </span>');
}

function onRemoveTag(key, param) {
	var arrparam = param.split(',');
	$('#'+key).remove();
	$.each( arrparam, function( key, value) {
		$('#'+value).val("");
	});
	$( "#btnSearch" ).trigger( "click" );
	
}
//=========End Tags=========