var occla1 = $('#occla1').val();
$(function() {
	if(typeof String.prototype.trim !== 'function') {
	  String.prototype.trim = function() {
	    return this.replace(/^\s+|\s+$/g, ''); 
	  }
	}

     var currentDate = new Date();
	$('#commDate').datepicker({
		useCurrent: true,
	    pickTime: false,
	    dateFormat: "dd/mm/yy"
    });
	if(!$('#commDate').val()){
		$('#commDate').datepicker("setDate", currentDate);
	}
	
	$('#la1dateOfBirth').datepicker({
		useCurrent: false,
        pickTime: false,
        dateFormat: "dd/mm/yy",
        viewMode:'years'
    });
	$('#syndate').datepicker({
		autoclose: true,
	    todayHighlight: true,
	    format: "dd/mm/yyyy"
	 });
	$("#syndate").datepicker("setDate", currentDate);
	$('#la2dateOfBirth').datepicker({
		useCurrent: false,
        pickTime: false,
        dateFormat: "dd/mm/yy",
        viewMode:'years'
    });
	var LAPO=$("#coLa1relationship option:selected").text();
	if (LAPO.trim()==="PO = LA")
		 {
		 document.getElementById("poName").readOnly=true;
		 }
	 else
		 {
		 document.getElementById("poName").readOnly=false;
		 }
	 
	 $("#coLa1relationship option[value='PO ≠ LA']").remove();
	 $("#coLa1relationship option[value='14']").remove();
	 
	 var loanAmount=document.getElementById("loanAmount").value;
	 if(loanAmount !=""){
	 document.getElementById("loanAmount").value =parseInt(loanAmount);
	 }
	 
	 //document.getElementById("l1Rtr1Term").value = $("#coPremiumTerm option:selected").text();
	 //document.getElementById("l1Rsr1Term").value = $("#coPremiumTerm option:selected").text();
	 //document.getElementById("l2Rtr1Term").value = $("#coPremiumTerm option:selected").text();
	 //document.getElementById("l2Rsr1Term").value = $("#coPremiumTerm option:selected").text();
	 //document.getElementById("l1Rtr2Term").value = $("#coPremiumTerm option:selected").text();
  });

$('#la1name').change(function(){
	//document.getElementById("life1").value = document.getElementById("la1name").value;
});

$('#coPremiumTerm').change(function(){
	 document.getElementById("l1Rtr1Term").value = $("#coPremiumTerm option:selected").text();
	 document.getElementById("l1Rsr1Term").value = $("#coPremiumTerm option:selected").text();
	 document.getElementById("l2Rtr1Term").value = $("#coPremiumTerm option:selected").text();
	 document.getElementById("l2Rsr1Term").value = $("#coPremiumTerm option:selected").text();
	 document.getElementById("l1Rtr2Term").value = $("#coPremiumTerm option:selected").text();
 });
 $('#coL1Rtr1').change(function(){
	 var l1Rtr1YN = "";
	 var SA=0;
	 var la1name=""
	 var la1dateOfBirth="";
	 l1Rtr1YN= $("#coL1Rtr1 option:selected").text();
	 SA=document.getElementById("basicPlanSa").value;
	 la1name=document.getElementById("la1name").value;
	 la1dateOfBirth=document.getElementById("la1dateOfBirth").value;
	 if(l1Rtr1YN==="Yes")
	 { 
		 //if(SA>=2000) {	
		 		document.getElementById("l1Rtr1").value =1000;				 		
		 //	}else{
		 		if(la1name===""){
		 			document.getElementById("la1name").style.background = "yellow";
		 			$('html, body').animate({ scrollTop: 0 }, 'slow');
		 			document.getElementById("coL1Rtr1").selectedIndex  = "Yes";
		 			document.getElementById("l1Rtr1").value =0;
		 			Err.innerHTML = "Input the mandatory field -LA1Name";
		 		}else{
		 			document.getElementById("la1name").style.background = "white";
		 		}
		 		if(la1dateOfBirth===""){
		 			document.getElementById("la1dateOfBirth").style.background = "yellow";
		 			$('html, body').animate({ scrollTop: 0 }, 'slow');
		 			document.getElementById("coL1Rtr1").selectedIndex  = "Yes";
		 			document.getElementById("l1Rtr1").value =0;
		 			Err.innerHTML =Err.innerHTML+"<br/>Input the mandatory field LA1DateOfBirth(It must be between 18 to 50 year-olds)";
		 		}else{
		 			document.getElementById("la1dateOfBirth").style.background = "white";
		 		}
		 		
		 		if(SA>0){
		 			document.getElementById("basicPlanSa").style.background = "white";
		 		}else {
		 			document.getElementById("basicPlanSa").style.background = "yellow";
			 		Err.innerHTML =Err.innerHTML+"<br/>Input the mandatory field BasicPlan_SA >0";
			 		document.getElementById("col1Rtr1").selectedIndex   = "No";
			 		document.getElementById("l1Rtr1").value = 0;
		 		s}
		 		if(SA==""){
		 			document.getElementById("basicPlanSa").style.background = "yellow";
			 		Err.innerHTML =Err.innerHTML+"<br/>Input the mandatory field BasicPlan_SA";
			 		document.getElementById("col1Rtr1").selectedIndex   = "No";
			 		document.getElementById("l1Rtr1").value = 0;
		 		}
		 		//document.getElementById("basicPlanSa").style.background = "yellow";
		 		//Err.innerHTML =Err.innerHTML+"<br/>Input the mandatory field BasicPlan_SA >=200";
		 		
		 //	}
	 }
	 else {
		 	document.getElementById("l1Rtr1").value =0;
		 	document.getElementById("basicPlanSa").style.background = "white";
		 	document.getElementById("la1dateOfBirth").style.background = "white";
		 	document.getElementById("la1name").style.background = "white";
		 	Err.innerHTML="";
	 } 
 });
 $('#coL1Rsr1').change(function(){
	 var l1Rtr1YN = "";
	 var SA=0;
	 var la1name=""
	 var la1dateOfBirth="";
	 l1Rtr1YN= $("#coL1Rsr1 option:selected").text();
	 SA=document.getElementById("basicPlanSa").value;
	 la1name=document.getElementById("la1name").value;
	 la1dateOfBirth=document.getElementById("la1dateOfBirth").value;
	 l1Rsr1YN= $("#coL1Rsr1 option:selected").text();
	 if(l1Rsr1YN==="Yes")
	 	{		
		    //if(SA>=2000) {	
		 			//document.getElementById("l1Rsr1").value =(document.getElementById("basicPlanSa").value*10)/100;
		 			document.getElementById("l1Rsr1").value =0;
		 	//}
		 	//else {
		 		if(la1name===""){
		 			document.getElementById("la1name").style.background = "yellow";
		 			$('html, body').animate({ scrollTop: 0 }, 'slow');
		 			document.getElementById("coL1Rsr1").selectedIndex  = "Yes";
		 			document.getElementById("l2Rsr1").value =0;
		 			Err.innerHTML = "Input the mandatory field -LA1Name";
		 		}else{
		 			document.getElementById("la1name").style.background = "white";
		 		}
		 		if(la1dateOfBirth===""){
		 			document.getElementById("la1dateOfBirth").style.background = "yellow";
		 			$('html, body').animate({ scrollTop: 0 }, 'slow');
		 			document.getElementById("coL1Rsr1").selectedIndex  = "Yes";
		 			document.getElementById("l2Rsr1").value =0;
		 			Err.innerHTML =Err.innerHTML+"<br/><br/>Please Input Value Agian!<br/>Min Age: 18<br/>Max Age: 50";
		 		}else{
		 			document.getElementById("la1dateOfBirth").style.background = "white";
		 		}
		 		//document.getElementById("basicPlanSa").style.background = "yellow";
		 		//Err.innerHTML =Err.innerHTML+"<br/>Input the mandatory field BasicPlan_SA >=2000";
		 	//}
	 	}
	 else {document.getElementById("l1Rsr1").value =0;} 
 });
 $('#coL2Rtr1').change(function(){
	 var l2Rtr1YN = "";
	 var SA=0;
	 var la2name=""
	 var la2dateOfBirth="";
	 l2Rtr1YN= $("#coL2Rtr1 option:selected").text();
	 SA=document.getElementById("basicPlanSa").value;
	 la2name=document.getElementById("la2name").value;
	 la2dateOfBirth=document.getElementById("la2dateOfBirth").value;
	 if(l2Rtr1YN==="Yes")			 
	 { 
	 		document.getElementById("l2Rtr1").value =1000;
	 		if(la2name==="" || la2dateOfBirth==="" || SA===""){
		 		if(la2name===""){
		 			document.getElementById("la2name").style.background = "yellow";
		 			$('html, body').animate({ scrollTop: 0 }, 'fast');
		 			document.getElementById("coL2Rtr1").selectedIndex  = "No";
		 			document.getElementById("l2Rtr1").value = 0;
		 			Err.innerHTML = "Input the mandatory field -LA2Name";
		 		}else{
		 			document.getElementById("la2name").style.background = "white";
		 		}
		 		if(la2dateOfBirth===""){
		 			document.getElementById("la2dateOfBirth").style.background = "yellow";
		 			$('html, body').animate({ scrollTop: 0 }, 'fast');
		 			document.getElementById("coL2Rtr1").selectedIndex  = "No";
		 			document.getElementById("l2Rtr1").value =0;
		 			Err.innerHTML =Err.innerHTML+"<br/><br/>Please Input Value Agian!<br/>Min Age: 18<br/>Max Age: 50";
		 		}else{
		 			document.getElementById("la2dateOfBirth").style.background = "white";
		 		}
		 		if (SA>0){				 			
			 		document.getElementById("basicPlanSa").style.background = "white";
		 		}else{
			 		document.getElementById("basicPlanSa").style.background = "yellow";
			 		Err.innerHTML =Err.innerHTML+"<br/>Input the mandatory field BasicPlan_SA>0";
			 		document.getElementById("coL2Rtr1").selectedIndex  = "No";
			 		document.getElementById("l2Rtr1").value = 0;
		 		}
		 		if(SA==""){
		 			document.getElementById("basicPlanSa").style.background = "yellow";
			 		Err.innerHTML =Err.innerHTML+"<br/>Input the mandatory field BasicPlan_SA";
			 		document.getElementById("coL2Rtr1").selectedIndex  = "No";
			 		document.getElementById("l2Rtr1").value = 0;
		 		}
		 	}
	 		else { 
	 			document.getElementById("l2Rtr1").value =1000; 
	 			document.getElementById("basicPlanSa").style.background = "white";
			 	document.getElementById("la2dateOfBirth").style.background = "white";
			 	document.getElementById("la2name").style.background = "white";
	 		}
	 }
	 else {
		 	document.getElementById("l2Rtr1").value =0;
		 	document.getElementById("basicPlanSa").style.background = "white";
		 	document.getElementById("la2dateOfBirth").style.background = "white";
		 	document.getElementById("la2name").style.background = "white";
		 	Err.innerHTML="";
	} 
 });
 $('#coL2Rsr1').change(function(){
	 var l2Rtr1YN = "";
	 var SA=0;
	 var la2name=""
	 var la2dateOfBirth="";
	 l2Rtr1YN= $("#coL2Rsr1 option:selected").text();
	 SA=document.getElementById("basicPlanSa").value;
	 la2name=document.getElementById("la2name").value;
	 la2dateOfBirth=document.getElementById("la2dateOfBirth").value;
	 l2Rsr1YN= $("#coL2Rsr1 option:selected").text();
	 if(l2Rsr1YN==="Yes")
	 	{		
		    //if(SA>=2000) {	
		 			//document.getElementById("l2Rsr1").value =(document.getElementById("basicPlanSa").value*10)/100;
		 			document.getElementById("l2Rsr1").value =0;				 		
		 	//}
		 	//else {
		 		if(la2name===""){
		 			document.getElementById("la2name").style.background = "yellow";
		 			$('html, body').animate({ scrollTop: 0 }, 'slow');
		 			document.getElementById("coL2Rsr1").selectedIndex  = "No";
		 			document.getElementById("l2Rsr1").value =0;
		 			Err.innerHTML = "Input the mandatory field -LA2Name";
		 		}else{
		 			document.getElementById("la2name").style.background = "white";
		 		}
		 		if(la2dateOfBirth===""){
		 			document.getElementById("la2dateOfBirth").style.background = "yellow";
		 			$('html, body').animate({ scrollTop: 0 }, 'slow');
		 			document.getElementById("coL2Rsr1").selectedIndex  = "No";
		 			document.getElementById("l2Rsr1").value =0;
		 			Err.innerHTML =Err.innerHTML+"<br/><br/>Please Input Value Agian!<br/>Min Age: 18<br/>Max Age: 50";
		 		}else{
		 			document.getElementById("la2dateOfBirth").style.background = "white";
		 		}
		 		//document.getElementById("basicPlanSa").style.background = "yellow";
		 		//Err.innerHTML =Err.innerHTML+"<br/>Input the mandatory field BasicPlan_SA >=2000";
		 		//document.getElementById("coL2Rsr1").selectedIndex  = "No";
		 	//}
	 	}
	 else {document.getElementById("l2Rsr1").value =0;} 
 });
$('#coL1Rtr2').change(function(){		
	 var l1Rtr2YN = "";
	 var SA=0;
	 var la1name=""
	 var la1dateOfBirth="";
	 l1Rtr2YN= $("#coL1Rtr2 option:selected").text();
	 SA=document.getElementById("basicPlanSa").value;
	 la1name=document.getElementById("la1name").value;
	 la1dateOfBirth=document.getElementById("la1dateOfBirth").value;
	 //alert(l1Rtr2YN);
	 if(l1Rtr2YN==="Yes")
	 { 
	 
	 	if(la1name===""){
 			document.getElementById("la1name").style.background = "yellow";
 			$('html, body').animate({ scrollTop: 0 }, 'slow');
 			document.getElementById("coL1Rtr1").selectedIndex  = "Yes";
 			document.getElementById("l1Rtr2").value =0;
 			Err.innerHTML = "Input the mandatory field -LA1Name";
 		}else{
 			document.getElementById("la1name").style.background = "white";
 		}
 		if(la1dateOfBirth===""){
 			document.getElementById("la1dateOfBirth").style.background = "yellow";
 			$('html, body').animate({ scrollTop: 0 }, 'slow');
 			document.getElementById("coL1Rtr1").selectedIndex  = "Yes";
 			document.getElementById("l1Rtr2").value =0;
 			Err.innerHTML =Err.innerHTML+"<br/>Input the mandatory field LA1DateOfBirth(It must be between 18 to 50 year-olds)";
 		}else{
 			document.getElementById("la1dateOfBirth").style.background = "white";
 		}
 		if (SA>0){				 			
	 		document.getElementById("basicPlanSa").style.background = "white";
 		}else{
	 		document.getElementById("basicPlanSa").style.background = "yellow";
	 		Err.innerHTML =Err.innerHTML+"<br/>Input the mandatory field BasicPlan_SA>0";
	 		document.getElementById("coL1Rtr2").selectedIndex  = "No";
	 		document.getElementById("l1Rtr2").value = 0;
 		}
 		if(SA==""){
 			document.getElementById("basicPlanSa").style.background = "yellow";
	 		Err.innerHTML =Err.innerHTML+"<br/>Input the mandatory field BasicPlan_SA";
	 		document.getElementById("coL1Rtr2").selectedIndex  = "No";
	 		document.getElementById("l1Rtr2").value = 0;
 		}
	 }
	 else {
		 	document.getElementById("l1Rtr2").value =0;
		 	document.getElementById("basicPlanSa").style.background = "white";
		 	document.getElementById("la1dateOfBirth").style.background = "white";
		 	document.getElementById("la1name").style.background = "white";
		 	Err.innerHTML="";
	 } 
});

 $('#coLa1relationship').change(function(){
	 var LAPO=$("#coLa1relationship option:selected").text();
	 
	 if (LAPO.trim()==="PO = LA")
		 {
		 document.getElementById("poName").readOnly=true;
		 document.getElementById("poName").value="";
		 }
	 else
		 {
		 document.getElementById("poName").readOnly=false;
		 document.getElementById("poName").value="";
		 
		 }
 });
 
 $("#la2name,#la1name").keypress(function(event){
	    var ew = event.which;
	    if(ew == 32 || ew==8)
	     {  return true;}
	    else if(ew >=65 && ew <= 90)
	    {	return true;}
	    else if(ew >= 97 && ew <= 122)
	    {  return true;}
	    else{return false;}
	});
 function Age(monthDob,dayDob,yearDob) {
	 
	}
function checkSubmit(){
		var clName="";
		var clName2="";
		var SA="";
		var DOB1="";
		var DOB2="";
		Err.innerHTML="";
		var l1Rtr1="";
		var l1Rsr1="";
		var l2Rtr1="";
		var l2Rsr1="";
		var l1Rtr2="";
		var l1Rtr1YN = "";
		var l1Rsr1YN = "";
		var l2Rtr1YN = "";
		var l2Rsr1YN = "";
		var l1Rtr2YN = "";
		var LAPO="";
		var poName="";
		var comDate="";
		var policyTerm="";
		var premuimTerm="";
		var inRate = "";
		var loanAmount = "";
		
		clName=document.getElementById("la1name").value;
		clName2=document.getElementById("la2name").value;
		SA=document.getElementById("basicPlanSa").value;
		loanAmount = document.getElementById("loanAmount").value;
		DOB1=document.getElementById("la1dateOfBirth").value;
		DOB2=document.getElementById("la2dateOfBirth").value;
		
		LAPO=$("#coLa1relationship option:selected").text();
		policyTerm=$("#coPolicyTerm option:selected").text();
		premuimTerm=$("#coPremiumTerm option:selected").text();
		poName=document.getElementById("poName").value;
		comDate=document.getElementById("commDate").value;
		inRate=$.trim(document.getElementById("inRate").value);
		
		/* Calculate Age 1*/
		var dayC=0;
		var monthC=0;
		var yearC=0;
		
		var today = comDate;
        var comDateArray=today.split("/");
        var newComDate=comDateArray[2] + '/' + comDateArray[1] + '/' + comDateArray[0];
        var ComDateTo=new Date(newComDate);
        ComDateTo.setDate(ComDateTo.getDate() +1);
        var Age1=0;
        var Age2=0;
        var date = DOB1;
        var datearray = date.split("/");
        var birthDate = new Date(datearray[2], datearray[1]*1-1, datearray[0]);
        Age1 = Math.floor((ComDateTo.getTime() - birthDate.getTime()) / (365.25 * 24 * 60 * 60 * 1000));
       
        /* Calculate Age 2 */
		var date = DOB2;
        var datearray2 = date.split("/");
        var birthDate = new Date(datearray2[2], datearray2[1]*1-1, datearray2[0]);
        
	    Age2 = Math.floor((ComDateTo.getTime() - birthDate.getTime()) / (365.25 * 24 * 60 * 60 * 1000));
		
	    if(clName2==="" && DOB2 ==="" ){$("#la2dateOfBirth").datepicker("setDate", "01/01/1900");}
		if(l1Rtr1YN==="No"){document.getElementById("l1Rtr1").value=0;}
		if(l2Rtr1YN==="No"){document.getElementById("l2Rtr1").value=0;}
		if (LAPO.trim()==="PO = LA"){ document.getElementById("poName").value=clName;}
		/*if ((parseInt(policyTerm)== 10  && parseInt(premuimTerm) == 10) || (parseInt(policyTerm)== 10  && parseInt(premuimTerm) == 5))  {}
		else{
			if (parseInt(policyTerm)== 10){
			Err.innerHTML="";
	    	Err.innerHTML=Err.innerHTML+"<br/>Premuim Term can select only 5 and 10!";
	    	document.getElementById("coPremiumTerm").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;}
		}
		if ((parseInt(policyTerm)== 12  && parseInt(premuimTerm) == 12) || (parseInt(policyTerm)== 12  && parseInt(premuimTerm) == 5))  {}
		else{
			if (parseInt(policyTerm)== 12){
			Err.innerHTML="";
	    	Err.innerHTML=Err.innerHTML+"<br/>Premuim Term can select only 5 and 12!";
	    	document.getElementById("coPremiumTerm").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;}
		}
		if ((parseInt(policyTerm)== 15  && parseInt(premuimTerm) == 15) || (parseInt(policyTerm)== 15  && parseInt(premuimTerm) == 5))  {}
		else{
			if (parseInt(policyTerm)== 15){
			Err.innerHTML="";
	    	Err.innerHTML=Err.innerHTML+"<br/>Premuim Term can select only 5 and 15!";
	    	document.getElementById("coPremiumTerm").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;}
		}*/
		
		// validate LA age and policy term
		var age = 0;
		var allowedPolicyTerm = 0;
		age = Age1*1 + policyTerm*1;
		allowedPolicyTerm = 60 - Age1*1;
		
		if(age > 60){
			if(allowedPolicyTerm < 3){
	    		Err.innerHTML="";
	    		Err.innerHTML=Err.innerHTML+"<br/>LA is not elegible to purchase this product!";
	    		
	    		document.getElementById("la1dateOfBirth").style.background = "yellow";
	    		$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	}else{
	    		Err.innerHTML="";
	    		Err.innerHTML=Err.innerHTML+"<br/>Policy term can select only " + allowedPolicyTerm + " and below!";
	    		
	    		document.getElementById("coPolicyTerm").style.background = "yellow";
	    		$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	}
	    	
	    	return false;
		}
		// end of validate LA age and policy term
		
		if(LAPO.trim() !="PO = LA" && poName==="" ){
			Err.innerHTML="";
	    	Err.innerHTML=Err.innerHTML+"<br/>Input the mandatory Policy Owner";
	    	document.getElementById("poName").style.background = "yellow";
	    	document.getElementById("la1dateOfBirth").style.background = "white";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
			
		if(clName===""){
	    	Err.innerHTML="";
	    	Err.innerHTML="<br/>Input the mandatory field -LA1Name";
	    	document.getElementById("la1name").style.background = "yellow";
	    	document.getElementById("basicPlanSa").style.background = "white";
	    	document.getElementById("la1dateOfBirth").style.background = "white";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
		else if(DOB1===""){
	    	Err.innerHTML="";
	    	Err.innerHTML="<br/><br/>Please Input Value Agian!<br/>Min Age: 18<br/>Max Age: 50";
	    	document.getElementById("la1dateOfBirth").style.background = "yellow";
	    	document.getElementById("basicPlanSa").style.background = "white";
	    	document.getElementById("la1name").style.background = "white";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
		else if(comDate===""){
	    	Err.innerHTML="";
	    	Err.innerHTML="<br/>Input the mandatory Policy commencement date";
	    	document.getElementById("commDate").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
		
		else if(DOB1===""){
	    	Err.innerHTML="";
	    	Err.innerHTML=Err.innerHTML+"<br/><br/>Please Input Value Agian!<br/>Min Age: 18<br/>Max Age: 50";
	    	document.getElementById("la1dateOfBirth").style.background = "yellow";
	    	document.getElementById("basicPlanSa").style.background = "white";
	    	document.getElementById("la1name").style.background = "white";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    	} else if (occla1 == '0' || occla1 == ''){
				Err.innerHTML="";
		    	Err.innerHTML=Err.innerHTML+"<br/>Please select Occupation of LA1";
		    	document.getElementById("occla1").style.background = "yellow";
		    	$('html, body').animate({ scrollTop: 0 }, 'slow');
		    	return false;
	    	}
	    else if (SA===""){
	    	Err.innerHTML=Err.innerHTML+"<br/>Please input Sum Assured again!<br/>1. Min requested loan amount $10000<br/>2. Base plan and rider SA should be whole number.";
	    	document.getElementById("basicPlanSa").style.background = "yellow";
	    	document.getElementById("la1name").style.background = "white";
	    	document.getElementById("la1dateOfBirth").style.background = "white";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
	    else if((inRate === "") || isNaN(inRate)){
	    	Err.innerHTML = "";
    		Err.innerHTML = "<br/><br/>Please Input correct number value";
    		document.getElementById("inRate").style.background = "yellow";
    		$('html, body').animate({ scrollTop : 0 }, 'slow');
    		console.log(inRate);
    		return false;
	    }
	    else if (SA<10000 && isUnlimitedSA=='false'){
	    	Err.innerHTML=Err.innerHTML+"<br/>Please input Sum Assured again!<br/>1. Min requested loan amount $10000<br/>2. Base plan and rider SA should be whole number.";
	    	document.getElementById("basicPlanSa").style.background = "yellow";
	    	document.getElementById("la1name").style.background = "white";
	    	document.getElementById("la1dateOfBirth").style.background = "white";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
	    else if (SA<=0 && isUnlimitedSA=='true'){
	    	Err.innerHTML=Err.innerHTML+"<br/>Please input Sum Assured again!<br/>1. Min SA > 0$ US<br/>2. Base plan and rider SA should be whole number.";
	    	document.getElementById("basicPlanSa").style.background = "yellow";
	    	document.getElementById("la1name").style.background = "white";
	    	document.getElementById("la1dateOfBirth").style.background = "white";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
	    else if (SA < loanAmount){
	    	Err.innerHTML=Err.innerHTML+"<br/>Please check Sum Assured again!<br/>.";
	    	document.getElementById("basicPlanSa").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
			    	return false;
			    }
			   else if(l1Rtr1YN==="Yes" && (parseInt(l1Rtr1)<parseInt(1000) || parseInt(l1Rtr1)>parseInt(SA*5) || l1Rtr1=="") && isUnlimitedRider=='false')
	    {   			    	
	    	Err.innerHTML=Err.innerHTML+"<br/>Please input Sum Assured again!<br/>Min SA = 1000$ US<br/>Max SA = 500% Basic Sum Assured";
	    	document.getElementById("l1Rtr1").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
	    else if(l1Rtr1YN==="Yes" && (parseInt(l1Rtr1)<=parseInt(0) || l1Rtr1=="") && isUnlimitedRider=='true')
	    {   			    	
	    	Err.innerHTML=Err.innerHTML+"<br/>Please input Sum Assured again!<br/>Min SA = 1000$ US<br/>Max SA = 500% Basic Sum Assured";
	    	document.getElementById("l1Rtr1").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
	    else if((l1Rsr1YN==="Yes" && (parseInt((SA*0.5))< parseInt(l1Rsr1)))){
	    	Err.innerHTML=Err.innerHTML+"<br/>Please input Sum Assured again!<br/>SA <=50% Basic Sum Assured.";
	    	document.getElementById("l1Rsr1").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
	    else if (l1Rtr1YN==="Yes" && (l1Rtr1%1 !==0)){
	    	Err.innerHTML=Err.innerHTML+"<br/>Please input Sum Assured again!<br/>SA can not content with decimal";
	    	document.getElementById("l1Rtr1").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
	    else if(l2Rtr1YN==="Yes" && (parseInt(l2Rtr1)<parseInt(1000) || parseInt(l2Rtr1)>parseInt(SA) || l2Rtr1=="") && isUnlimitedRider=='false')
	    {   			    	
	    	Err.innerHTML=Err.innerHTML+"<br/>Please input Sum Assured again!<Br/>Min SA = 1000$ US<br/>Max SA = 100% Basic Sum Assured";
	    	document.getElementById("l2Rtr1").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
	    else if(l2Rtr1YN==="Yes" && (parseInt(l2Rtr1)<=parseInt(0) || l2Rtr1=="") && isUnlimitedRider=='true')
	    {   			    	
	    	Err.innerHTML=Err.innerHTML+"<br/>Please input Sum Assured again!<Br/>Min SA = 1000$ US<br/>Max SA = 100% Basic Sum Assured";
	    	document.getElementById("l2Rtr1").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
	    /*else if((l2Rtr1YN==="Yes" && parseInt(l2Rtr1)>parseInt(SA)))
	    {   Err.innerHTML=Err.innerHTML+"<br/>Please input Sum Assured again!<Br/>Min SA = 1000$ US<br/>Max SA = 100% Basic Sum Assured";
	    	document.getElementById("l2Rtr1").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }*/
	    else if((l2Rsr1YN==="Yes" && (parseInt((SA*0.5))< parseInt(l2Rsr1)))){
	    	Err.innerHTML=Err.innerHTML+"<br/>Please input Sum Assured again!<br/>SA <=50% Basic Sum Assured.";
	    	document.getElementById("l2Rsr1").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
	    else if (l2Rtr1YN==="Yes" && (l2Rtr1%1 !==0)){
	    	Err.innerHTML=Err.innerHTML+"<br/>Please input Sum Assured again!<br/>SA can not content with decimal";
	    	document.getElementById("l2Rtr1").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
	    else if(l1Rtr2YN==="Yes" && l1Rtr2=="")
	    {   			    				    
	    	Err.innerHTML=Err.innerHTML+"<br/>PruSaver ride value required!";
	    	document.getElementById("l1Rtr2").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
	    else if (l1Rtr2YN==="Yes" && (l1Rtr2%1 !==0)){
	    	Err.innerHTML=Err.innerHTML+"<br/>Please input premium again!<br/>PruSaver premium can not content with decimal";
	    	document.getElementById("l1Rtr2").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
	    else if (l1Rtr2YN==="Yes" && (SA<10000)){
	    	Err.innerHTML=Err.innerHTML+"<br/>Please input PRU MYFamily (Basic Plan) again!<br/>PRU MYFamily (Basic Plan) must be >=10000$";
	    	document.getElementById("basicPlanSa").style.background = "yellow";
	    	$('html, body').animate({ scrollTop: 0 }, 'slow');
	    	return false;
	    }
	    else if (l2Rtr1YN==="Yes" || l2Rsr1YN==="Yes" )
		{   
	    	if(clName2===""){
	    		Err.innerHTML="";
		    	Err.innerHTML="<br/>Input the mandatory field -LA2Name";
		    	document.getElementById("la2name").style.background = "yellow";
		    	document.getElementById("basicPlanSa").style.background = "white";
		    	document.getElementById("la2dateOfBirth").style.background = "white";
		    	$('html, body').animate({ scrollTop: 0 }, 'slow');
		    	return false;
	    	}
	    	else if(Age2<18 || Age2>50){
	        	Err.innerHTML="";
		    	Err.innerHTML="<br/><br/>Please Input Value Agian!<br/>Min Age: 18<br/>Max Age: 50";
		    	document.getElementById("la2dateOfBirth").style.background = "yellow";
		    	document.getElementById("basicPlanSa").style.background = "white";
		    	document.getElementById("la2name").style.background = "white";
		    	$('html, body').animate({ scrollTop: 0 }, 'slow');
		    	return false;
	        }
	    	if(parseInt(Age1)<parseInt(18) || parseInt(Age1)>parseInt(57)){
	        	Err.innerHTML="";
		    	Err.innerHTML="<br/><br/>Please Input Value Agian!<br/>Min Age: 18<br/>Max Age: 57";
		    	document.getElementById("la1dateOfBirth").style.background = "yellow";
		    	document.getElementById("la2dateOfBirth").style.background = "white";
		    	document.getElementById("basicPlanSa").style.background = "white";
		    	document.getElementById("la1name").style.background = "white";
		    	$('html, body').animate({ scrollTop: 0 }, 'slow');
		    	return false;
	        }
	    	else if(parseInt(Age1)+parseInt(policyTerm)>60){
	        	Err.innerHTML="";
		    	Err.innerHTML="<br/><br/>Life 1 can not choose policy term="+policyTerm+" becuase life1's <b>Age</b> + <b>Policy Term</b>  is over 60 years!";
		    	document.getElementById("la1dateOfBirth").style.background = "yellow";
		    	document.getElementById("la2dateOfBirth").style.background = "white";
		    	document.getElementById("basicPlanSa").style.background = "white";
		    	document.getElementById("la1name").style.background = "white";
		    	$('html, body').animate({ scrollTop: 0 }, 'slow');
		    	return false;
			}
			 
	 	}
	    else if(clName2!="" && DOB2 ==="" ){
	    		Err.innerHTML="";
		    	Err.innerHTML="<br/><br/>Please Input Value Agian!<br/>Min Age: 18<br/>Max Age: 50";
		    	document.getElementById("la2dateOfBirth").style.background = "yellow";
		    	document.getElementById("basicPlanSa").style.background = "white";
		    	document.getElementById("la2name").style.background = "white";
		    	$('html, body').animate({ scrollTop: 0 }, 'slow');
		    	return false;
	    }
	    else if(DOB1!=""){
	    
	    	if(parseInt(Age1)<parseInt(18) || parseInt(Age1)>parseInt(57)){
	        	Err.innerHTML="";
		    	Err.innerHTML="<br/><br/>Please Input Value Agian!<br/>Min Age: 18<br/>Max Age: 57";
		    	document.getElementById("la1dateOfBirth").style.background = "yellow";
		    	document.getElementById("la2dateOfBirth").style.background = "white";
		    	document.getElementById("basicPlanSa").style.background = "white";
		    	document.getElementById("la1name").style.background = "white";
		    	$('html, body').animate({ scrollTop: 0 }, 'slow');
		    	return false;
	        }
	    	else if(parseInt(Age1)+parseInt(policyTerm)>60){
	        	Err.innerHTML="";
		    	Err.innerHTML="<br/><br/>Life 1 can not choose policy term="+policyTerm+" becuase life1's <b>Age</b> + <b>Policy Term</b>  is over 60 years old!";
		    	document.getElementById("la1dateOfBirth").style.background = "yellow";
		    	document.getElementById("la2dateOfBirth").style.background = "white";
		    	document.getElementById("basicPlanSa").style.background = "white";
		    	document.getElementById("la1name").style.background = "white";
		    	$('html, body').animate({ scrollTop: 0 }, 'slow');
		    	return false;
			}
		    else{
	        	 document.form.submit();
	  		     return true;
	        }
	    }
	    else{
		    document.form.submit();
		    return true;
	    }
 }
$('#occla1').change(function () {
	occla1 = $(this).val();
});