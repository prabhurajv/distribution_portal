
	$(function() {
			if(typeof String.prototype.trim !== 'function') {
			  String.prototype.trim = function() {
			    return this.replace(/^\s+|\s+$/g, ''); 
			  }
			}
			<sec:authorize access="hasRole('ACCESS_UNLIMITED_RIDER')" var="isUnlimitedRider">
			</sec:authorize>
			
			<sec:authorize access="hasRole('ACCESS_UNLIMITED_SA')" var="isUnlimitedSA">
			</sec:authorize>
	
		    var currentDate = new Date();4
			$('#txtoccdate').datetimepicker({
				useCurrent: true,
			    pickTime: false,
			    defaultDate:currentDate
		    });
			$('#txtbendob1').datetimepicker({
				useCurrent: false,
		        pickTime: false,
		        viewMode:'years'
		    });
			$('#txtbendob2').datetimepicker({
				useCurrent: false,
		        pickTime: false,
		        viewMode:'years'
		    });
			$('#txtbendob3').datetimepicker({
				useCurrent: false,
		        pickTime: false,
		        viewMode:'years'
		    });
			$('#txtbendob4').datetimepicker({
				useCurrent: false,
		        pickTime: false,
		        viewMode:'years'
		    });
			$("#syndate").datepicker("setDate", currentDate);
			$('#la2dateOfBirth').datetimepicker({
				useCurrent: false,
		        pickTime: false,
		        viewMode:'years'
		    });
		    
		    document.getElementById("txtbenname1").readOnly=true;
		    document.getElementById("txtbennamelatin1").readOnly=true;
		    $('#txtbensex1').attr('disabled',true);
		    document.getElementById("txtbendob1").disabled=true;
		    document.getElementById("txtbencon1").readOnly=true;
		    document.getElementById("txtbenper1").readOnly=true;
		    
		    document.getElementById("txtbenname2").readOnly=true;
		    document.getElementById("txtbennamelatin2").readOnly=true;
		    $('#txtbensex2').attr('disabled',true);
		    document.getElementById("txtbendob2").disabled=true;
		    document.getElementById("txtbencon2").readOnly=true;
		    document.getElementById("txtbenper2").readOnly=true;
		    
		    document.getElementById("txtbenname3").readOnly=true;
		    document.getElementById("txtbennamelatin3").readOnly=true;
		    $('#txtbensex3').attr('disabled',true);
		    document.getElementById("txtbendob3").disabled=true;
		    document.getElementById("txtbencon3").readOnly=true;
		    document.getElementById("txtbenper3").readOnly=true;
		    
		    document.getElementById("txtbenname4").readOnly=true;
		    document.getElementById("txtbennamelatin4").readOnly=true;
		    $('#txtbensex4').attr('disabled',true);
		    document.getElementById("txtbendob4").disabled=true;
		    document.getElementById("txtbencon4").readOnly=true;
		    document.getElementById("txtbenper4").readOnly=true;
		 });
		 
		 $('#txtchk1').change(function(){
		 	var txtchk1=document.getElementById("txtchk1").value;
		 	if(txtchk1=="Yes"){
			 	document.getElementById("txtbenname1").readOnly=false;
			    document.getElementById("txtbennamelatin1").readOnly=false;
			    $('#txtbensex1').attr('disabled',false);
			    document.getElementById("txtbendob1").disabled=false;
			    document.getElementById("txtbencon1").readOnly=false;
			    document.getElementById("txtbenper1").readOnly=false;
		 	}
		 	else{
		 		document.getElementById("txtbenname1").readOnly=true;
			    document.getElementById("txtbennamelatin1").readOnly=true;
			    $('#txtbensex1').attr('disabled',true);
			    document.getElementById("txtbendob1").disabled=true;
			    document.getElementById("txtbencon1").readOnly=true;
			    document.getElementById("txtbenper1").readOnly=true;
		 	}
		 });
		 
		 $('#txtchk2').change(function(){
		 	var txtchk2=document.getElementById("txtchk2").value;
		 	if(txtchk2=="Yes"){
			 	document.getElementById("txtbenname2").readOnly=false;
			    document.getElementById("txtbennamelatin2").readOnly=false;
			    $('#txtbensex2').attr('disabled',false);
			    document.getElementById("txtbendob2").disabled=false;
			    document.getElementById("txtbencon2").readOnly=false;
			    document.getElementById("txtbenper2").readOnly=false;
		 	}
		 	else{
		 		document.getElementById("txtbenname2").readOnly=true;
			    document.getElementById("txtbennamelatin2").readOnly=true;
			    $('#txtbensex2').attr('disabled',true);
			    document.getElementById("txtbendob2").disabled=true;
			    document.getElementById("txtbencon2").readOnly=true;
			    document.getElementById("txtbenper2").readOnly=true;
		 	}
		 });
		 
		 $('#txtchk3').change(function(){
		 	var txtchk3=document.getElementById("txtchk3").value;
		 	if(txtchk3=="Yes"){
			 	document.getElementById("txtbenname3").readOnly=false;
			    document.getElementById("txtbennamelatin3").readOnly=false;
			    $('#txtbensex3').attr('disabled',false);
			    document.getElementById("txtbendob3").disabled=false;
			    document.getElementById("txtbencon3").readOnly=false;
			    document.getElementById("txtbenper3").readOnly=false;
		 	}
		 	else{
		 		document.getElementById("txtbenname3").readOnly=true;
			    document.getElementById("txtbennamelatin3").readOnly=true;
			    $('#txtbensex3').attr('disabled',true);
			    document.getElementById("txtbendob3").disabled=true;
			    document.getElementById("txtbencon3").readOnly=true;
			    document.getElementById("txtbenper3").readOnly=true;
		 	}
		 });
		 $('#txtchk4').change(function(){
		 	var txtchk4=document.getElementById("txtchk4").value;
		 	if(txtchk4=="Yes"){
			 	document.getElementById("txtbenname4").readOnly=false;
			    document.getElementById("txtbennamelatin4").readOnly=false;
			    $('#txtbensex4').attr('disabled',false);
			    document.getElementById("txtbendob4").disabled=false;
			    document.getElementById("txtbencon4").readOnly=false;
			    document.getElementById("txtbenper4").readOnly=false;
		 	}
		 	else{
		 		document.getElementById("txtbenname4").readOnly=true;
			    document.getElementById("txtbennamelatin4").readOnly=true;
			    $('#txtbensex4').attr('disabled',true);
			    document.getElementById("txtbendob4").disabled=true;
			    document.getElementById("txtbencon4").readOnly=true;
			    document.getElementById("txtbenper4").readOnly=true;
		 	}
		 });
		
	 	 function checkSubmit(){
				 var txtappnum="";
				 var txtoccdate="";
				 var txtagntname="";
				 var txtchdrnum="";
				 var txtagntnum="";
				 var txttrndate="";
				 var txtagntphone="";
				 var txtponamekh="";
				 var txtponameen="";
				 var txtponid="";
				 var txtpodob="";
				 var txtpopob="";
				 var txtponat="";
				 var txtpostatus="";
				 var txtpoocc="";
				 var txtpocurocc="";
				 var txtposal="";
				 var txtaddr="";
				 var txtpophone="";
				 var txtpoemail="";
				 var txtpoheight="";
				 var txtpoweight="";
				 var txtpocig="";
				 var txttrq1="";
				 var txttrq2="";
				 var txttrq3="";
				 var txttrq4="";
				 var txttrq51="";
				 var txttrq52="";
				 var txttrq6="";
				 var txtbenname1="";
				 var txtbennamelatin1="";
				 var txtbensex1="";
				 var txtbendob1="";
				 var txtbencon1="";
				 var txtbenper1="";
				 var txtbenname2="";
				 var txtbennamelatin2="";
				 var txtbensex2="";
				 var txtbendob2="";
				 var txtbencon2="";
				 var txtbenper2="";
				 var txtbenname3="";
				 var txtbennamelatin3="";
				 var txtbensex3="";
				 var txtbendob3="";
				 var txtbencon3="";
				 var txtbenper3="";
				 var txtbenname4="";
				 var txtbennamelatin4="";
				 var txtbensex4="";
				 var txtbendob4="";
				 var txtbencon4="";
				 var txtbenper4="";
				
				
				 txtponamekh =document.getElementById("txtponamekh").value;
				 txtponameen =document.getElementById("txtponameen").value;
				 txtponid =document.getElementById("txtponid").value;
				 txtpopob =document.getElementById("txtpopob").value;
				 txtponat =document.getElementById("txtponat").value;
				 txtpostatus =document.getElementById("txtpostatus").value;
				 txtpoocc =document.getElementById("txtpoocc").value;
				 txtpocurocc =document.getElementById("txtpocurocc").value;
				 txtposal=document.getElementById("txtposal").value;
				 txtaddr=document.getElementById("txtaddr").value;
				 txtpophone=document.getElementById("txtpophone").value;
				 txtpoemail=document.getElementById("txtpoemail").value;
				 txtpoheight=document.getElementById("txtpoheight").value;
				 txtpoweight=document.getElementById("txtpoweight").value;
				 txtpocig=document.getElementById("txtpocig").value;
				
				var txtchk1=document.getElementById("txtchk1").value;
				txtbenname1=document.getElementById("txtbenname1").value;
				txtbennamelatin1=document.getElementById("txtbennamelatin1").value;
				txtbensex1=document.getElementById("txtbensex1").value;
				txtbendob1=document.getElementById("txtbendob1").value;
				txtbencon1=document.getElementById("txtbencon1").value;
				txtbenper1=document.getElementById("txtbenper1").value;
				
				var txtchk2=document.getElementById("txtchk2").value;
				txtbenname2=document.getElementById("txtbenname2").value;
				txtbennamelatin2=document.getElementById("txtbennamelatin2").value;
				txtbensex2=document.getElementById("txtbensex2").value;
				txtbendob2=document.getElementById("txtbendob2").value;
				txtbencon2=document.getElementById("txtbencon2").value;
				txtbenper2=document.getElementById("txtbenper2").value;
				
				var txtchk3=document.getElementById("txtchk3").value;
				txtbenname3=document.getElementById("txtbenname3").value;
				txtbennamelatin3=document.getElementById("txtbennamelatin3").value;
				txtbensex3=document.getElementById("txtbensex3").value;
				txtbendob3=document.getElementById("txtbendob3").value;
				txtbencon3=document.getElementById("txtbencon3").value;
				txtbenper3=document.getElementById("txtbenper3").value;
				
				var txtchk4=document.getElementById("txtchk4").value;
				txtbenname4=document.getElementById("txtbenname4").value;
				txtbennamelatin4=document.getElementById("txtbennamelatin4").value;
				txtbensex4=document.getElementById("txtbensex4").value;
				txtbendob4=document.getElementById("txtbendob4").value;
				txtbencon4=document.getElementById("txtbencon4").value;
				txtbenper4=document.getElementById("txtbenper4").value;
				
				if(txtchk1=="Yes"){
			 		if(txtbenname1==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ឈ្មោះពេញ អ្នកទី1";
				    	document.getElementById("txtbenname1").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
					else if(txtbennamelatin1==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ឈ្មោះជាអក្សរឡាតាំង អ្នកទី1";
				    	document.getElementById("txtbennamelatin1").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
					else if(txtbensex1==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ភេទ អ្នកទី1";
				    	document.getElementById("txtbensex1").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
					else if(txtbendob1==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ថ្ងៃខែឆ្នាំកំណើត អ្នកទី1";
				    	document.getElementById("txtbendob1").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
					else if(txtbencon1==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ទំនាក់ទំនងជាមួយបុគ្គលដែលជាកម្មវត្ថុនៃការធានា អ្នកទី1";
				    	document.getElementById("txtbencon1").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
					else if(txtbenper1==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ចំណែក(%) អ្នកទី1";
				    	document.getElementById("txtbenper1").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				}
			 	
			 	if(txtchk2=="Yes"){
			 		if(txtbenname2==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ឈ្មោះពេញ អ្នកទី2";
				    	document.getElementById("txtbenname2").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
					else if(txtbennamelatin2==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ឈ្មោះជាអក្សរឡាតាំង អ្នកទី2";
				    	document.getElementById("txtbennamelatin2").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
					else if(txtbensex2==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ភេទ អ្នកទី2";
				    	document.getElementById("txtbensex2").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
					else if(txtbendob2==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ថ្ងៃខែឆ្នាំកំណើត អ្នកទី3";
				    	document.getElementById("txtbendob2").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
					else if(txtbencon2==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ទំនាក់ទំនងជាមួយបុគ្គលដែលជាកម្មវត្ថុនៃការធានា អ្នកទី3";
				    	document.getElementById("txtbencon2").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
					else if(txtbenper2==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ចំណែក(%) អ្នកទី3";
				    	document.getElementById("txtbenper3").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				}
				
				if(txtponamekh===""){
			    	Err.innerHTML="";
			    	Err.innerHTML="<br/>សូមបំពេញ ឈ្មោះពេញតាមអត្តសញ្ញាណប័ណ្ណ/លិខិតឆ្លងដែន (ជាអក្សរខ្មែរ)";
			    	document.getElementById("txtponamekh").style.background = "yellow";
			    	$('html, body').animate({ scrollTop: 0 }, 'slow');
			    	return false;
			    }
			     else if(txtponameen===""){
			    	Err.innerHTML="";
			    	Err.innerHTML="<br/>សូមបំពេញ ឈ្មោះពេញតាមអត្តសញ្ញាណប័ណ្ណ/លិខិតឆ្លងដែន (ជាអក្សរឡាតាំង)";
			    	document.getElementById("txtponameen").style.background = "yellow";
			    	$('html, body').animate({ scrollTop: 0 }, 'slow');
			    	return false;
			    }
			    else if(txtponid===""){
			    	Err.innerHTML="";
			    	Err.innerHTML="<br/>សូមបំពេញ លេខអត្តសញ្ញាណប័ណ្ណ/លិខិតឆ្លងដែន";
			    	document.getElementById("txtponid").style.background = "yellow";
			    	$('html, body').animate({ scrollTop: 0 }, 'slow');
			    	return false;
			    }
			    else if(txtpopob===""){
			    	Err.innerHTML="";
			    	Err.innerHTML="<br/>សូមបំពេញ ទីកន្លែងកំណើត(ប្រទេស)";
			    	document.getElementById("txtpopob").style.background = "yellow";
			    	$('html, body').animate({ scrollTop: 0 }, 'slow');
			    	return false;
			    }
			    else if(txtponat===""){
			    	Err.innerHTML="";
			    	Err.innerHTML="<br/>សូមបំពេញ សញ្ជាតិ";
			    	document.getElementById("txtponat").style.background = "yellow";
			    	$('html, body').animate({ scrollTop: 0 }, 'slow');
			    	return false;
			    }
			    else if(txtpostatus===""){
			    	Err.innerHTML="";
			    	Err.innerHTML="<br/>សូមបំពេញ ស្ថានភាពគ្រួសារ";
			    	document.getElementById("txtpostatus").style.background = "yellow";
			    	$('html, body').animate({ scrollTop: 0 }, 'slow');
			    	return false;
			    }
			    else if(txtpoocc===""){
			    	Err.innerHTML="";
			    	Err.innerHTML="<br/>សូមបំពេញ មុខរបរ";
			    	document.getElementById("txtpoocc").style.background = "yellow";
			    	$('html, body').animate({ scrollTop: 0 }, 'slow');
			    	return false;
			    }
			    else if(txtpocurocc===""){
			    	Err.innerHTML="";
			    	Err.innerHTML="<br/>សូមបំពេញ ភារកិច្ចពិតប្រាកដ";
			    	document.getElementById("txtpocurocc").style.background = "yellow";
			    	$('html, body').animate({ scrollTop: 0 }, 'slow');
			    	return false;
			    }
			    else if(txtposal===""){
			    	Err.innerHTML="";
			    	Err.innerHTML="<br/>សូមបំពេញ ចំណូលផ្ទាល់ខ្លួនប្រចាំខែជាមធ្យមក្នុងរយៈពេល១២ខែចុងក្រោយ";
			    	document.getElementById("txtposal").style.background = "yellow";
			    	$('html, body').animate({ scrollTop: 0 }, 'slow');
			    	return false;
			    }
			    else if(txtaddr===""){
			    	Err.innerHTML="";
			    	Err.innerHTML="<br/>សូមបំពេញ អាសយដ្ឋានទំនាក់ទំនង";
			    	document.getElementById("txtaddr").style.background = "yellow";
			    	$('html, body').animate({ scrollTop: 0 }, 'slow');
			    	return false;
			    }
			    else if(txtpophone===""){
			    	Err.innerHTML="";
			    	Err.innerHTML="<br/>សូមបំពេញ លេខទូរស័ព្ទ";
			    	document.getElementById("txtpophone").style.background = "yellow";
			    	$('html, body').animate({ scrollTop: 0 }, 'slow');
			    	return false;
			    }
			    else if(txtpoheight===""){
			    	Err.innerHTML="";
			    	Err.innerHTML="<br/>សូមបំពេញ កំពស់(ម)";
			    	document.getElementById("txtpoheight").style.background = "yellow";
			    	$('html, body').animate({ scrollTop: 0 }, 'slow');
			    	return false;
			    }
			    else if(txtpoweight===""){
			    	Err.innerHTML="";
			    	Err.innerHTML="<br/>សូមបំពេញ ទំងន់(គ.ក)";
			    	document.getElementById("txtpoweight").style.background = "yellow";
			    	$('html, body').animate({ scrollTop: 0 }, 'slow');
			    	return false;
			    }
			    else if(txtpocig===""){
			    	Err.innerHTML="";
			    	Err.innerHTML="<br/>សូមបំពេញ ជក់បារីប៉ុន្មានដើមក្នុងមួយថ្ងៃ";
			    	document.getElementById("txtpocig").style.background = "yellow";
			    	$('html, body').animate({ scrollTop: 0 }, 'slow');
			    	return false;
			    }
			     else if(txtchk1=="Yes" && txtbenname1==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/><p style='font-family:Kh-Metal-Chrieng;'>សូមបំពេញ ឈ្មោះពេញ អ្នកទី1</p>";
				    	document.getElementById("txtbenname1").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk1=="Yes" && txtbennamelatin1==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ឈ្មោះជាអក្សរឡាតាំង អ្នកទី1";
				    	document.getElementById("txtbennamelatin1").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk1=="Yes" && txtbensex1==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ភេទ អ្នកទី1";
				    	document.getElementById("txtbensex1").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk1=="Yes" && txtbendob1==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ថ្ងៃខែឆ្នាំកំណើត អ្នកទី1";
				    	document.getElementById("txtbendob1").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk1=="Yes" && txtbencon1==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ទំនាក់ទំនងជាមួយបុគ្គលដែលជាកម្មវត្ថុនៃការធានា អ្នកទី1";
				    	document.getElementById("txtbencon1").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk1=="Yes" && txtbenper1==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ចំណែក(%) អ្នកទី1";
				    	document.getElementById("txtbenper1").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk2=="Yes" && txtbenname2==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ឈ្មោះពេញ អ្នកទី2";
				    	document.getElementById("txtbenname2").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk2=="Yes" && txtbennamelatin2==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ឈ្មោះជាអក្សរឡាតាំង អ្នកទី2";
				    	document.getElementById("txtbennamelatin2").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk2=="Yes" && txtbensex2==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ភេទ អ្នកទី2";
				    	document.getElementById("txtbensex2").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk2=="Yes" && txtbendob2==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ថ្ងៃខែឆ្នាំកំណើត អ្នកទី2";
				    	document.getElementById("txtbendob2").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk2=="Yes" && txtbencon2==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ទំនាក់ទំនងជាមួយបុគ្គលដែលជាកម្មវត្ថុនៃការធានា អ្នកទី2";
				    	document.getElementById("txtbencon2").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk2=="Yes" && txtbenper2==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ចំណែក(%) អ្នកទី2";
				    	document.getElementById("txtbenper2").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk3=="Yes" && txtbenname3==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ឈ្មោះពេញ អ្នកទី3";
				    	document.getElementById("txtbenname3").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk3=="Yes" && txtbennamelatin3==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ឈ្មោះជាអក្សរឡាតាំង អ្នកទី3";
				    	document.getElementById("txtbennamelatin3").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk3=="Yes" && txtbensex3==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ភេទ អ្នកទី3";
				    	document.getElementById("txtbensex3").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk3=="Yes" && txtbendob3==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ថ្ងៃខែឆ្នាំកំណើត អ្នកទី3";
				    	document.getElementById("txtbendob3").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk3=="Yes" && txtbencon3==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ទំនាក់ទំនងជាមួយបុគ្គលដែលជាកម្មវត្ថុនៃការធានា អ្នកទី3";
				    	document.getElementById("txtbencon3").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk3=="Yes" && txtbenper3==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ចំណែក(%) អ្នកទី3";
				    	document.getElementById("txtbenper3").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk4=="Yes" && txtbenname4==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ឈ្មោះពេញ អ្នកទី4";
				    	document.getElementById("txtbenname4").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk4=="Yes" && txtbennamelatin4==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ឈ្មោះជាអក្សរឡាតាំង អ្នកទី4";
				    	document.getElementById("txtbennamelatin4").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk4=="Yes" && txtbensex4==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ភេទ អ្នកទី4";
				    	document.getElementById("txtbensex4").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk4=="Yes" && txtbendob4==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ថ្ងៃខែឆ្នាំកំណើត អ្នកទី4";
				    	document.getElementById("txtbendob4").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk4=="Yes" && txtbencon4==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ទំនាក់ទំនងជាមួយបុគ្គលដែលជាកម្មវត្ថុនៃការធានា អ្នកទី4";
				    	document.getElementById("txtbencon4").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk4=="Yes" && txtbenper4==""){
						Err.innerHTML="";
				    	Err.innerHTML="<br/>សូមបំពេញ ចំណែក(%) អ្នកទី4";
				    	document.getElementById("txtbenper4").style.background = "yellow";
				    	$('html, body').animate({ scrollTop: 0 }, 'slow');
				    	return false;
					}
				else if(txtchk1=="Yes" || txtchk2=="Yes" || txtchk3=="Yes" || txtchk4=="Yes"){
						if(txtbenper1==""){txtbenper1=0;}
						if(txtbenper2==""){txtbenper2=0;}
						if(txtbenper3==""){txtbenper3=0;}
						if(txtbenper4==""){txtbenper4=0;}
						if (parseFloat(txtbenper1)+parseFloat(txtbenper2)+parseFloat(txtbenper3)+parseFloat(txtbenper4) != parseFloat(100)){
							Err.innerHTML="";
					    	Err.innerHTML="<br/>ចំណែក(%)សរុប គឺ 100%";
					    	$('html, body').animate({ scrollTop: 0 }, 'slow');
					    	return false;
					    	alert()
						}
				}	
			 	else{
			    document.form.submit();
			    return true;
			    }
		
			 }