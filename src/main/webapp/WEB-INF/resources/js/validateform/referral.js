 //From New Referral Page
$(document).ready(function() {
//	$("#btnsave").hide();
	$('#referralBy').select2();
	checkSuccessfulPresentation();
	checkSuccessfullAppointment();
	validateCloseSale();
	/* AllowSaleAct(); */
	$(".select2_multiple").select2({
	      placeholder: "Related Bank Products",
	      allowClear: true
	 });

	$(".select2_single").select2({
      placeholder: "Branch Code - Name"
    });
	
	$('#salSucSalPreStatus').change(function(){
		checkSuccessfulPresentation();
	});

	$('#sucAppStatus').change(function() {
		checkSuccessfullAppointment();
	});

	$('#cusSalaryMonthly').change(function() {
		validateCloseSale();
	});

	$('#salMthBasExp').change(function() {
		validateCloseSale();
	});

	$('#salRecPrdPkgUsd').change(function() {
		validateCloseSale();
	});
	
	if (formstatus == 'update') {
		if(isclosesale){
			$('#referralBy').attr('disabled', 'disabled');
			$('#branch').attr('disabled', 'disabled');
		}
	}

//	For Both
	DisableCustomerApproach();
//	HideSaveDraftAndCloseSale();
	if (formstatus)
	getReferrer($('#branch').val(), formstatus, $('#referralByHidden').val());
	$.each(nameDescFormat, function( index, value ) {
		$('#'+value).change(function() {
			DisableCustomerApproach();
		});
	});
	
	$('#rType').change(function() {
		DisableCustomerApproach();
		AllowSaleAct();
	});

	$('#referralBy').change(function() {
		var referralby = $('#referralBy').val();
		if (referralby != null && referralby != '' && referralby != '0') {
			$('#referralByHidden').val(referralby);	
		}
		
		if(formstatus === 'update') {
			DisableCustomerApproach();
		}
		AllowSaleAct();
	});

	$('#cusName').change(function() {
		DisableCustomerApproach();
		AllowSaleAct();
	});

	$('#cusAge').change(function() {
		DisableCustomerApproach();
		AllowSaleAct();
	});

	$('#cusPhone').change(function() {
		DisableCustomerApproach();
		AllowSaleAct();
	});

	$('#cusOcc').change(function() {
		DisableCustomerApproach();
		AllowSaleAct();
	});
	
	allowUpload();
	if (typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, '');
		}
	}

	$('#clsBankSoStatus').change(function() {
		allowUpload();
	});
	
	$('#branch').change(function() {
		getReferrer($('#branch').val(), formstatus, $('#referralByHidden').val());
		AllowSaleAct();
	});

	var currentDate = new Date();
	$('#cdate').datepicker({
		useCurrent : true,
		pickTime : false,
		dateFormat : "dd/mm/yy"
	});
	if (!$('#cdate').val()) {
		$('#cdate').datepicker("setDate", currentDate);
	} else {
		$('#cdate').val(convertDate($('#cdate').val()));
	}

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1;
	var yyyy = today.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}
	if (mm < 10) {
		mm = '0' + mm;
	}
	var today = dd + '/' + mm + '/' + yyyy;

	$('#fappDate').datepicker({
		useCurrent : false,
		pickTime : false,
		dateFormat : "dd/mm/yy"
	});
 
	$('#sucsaledate').datepicker({
		useCurrent : false,
		pickTime : false,
		viewMode : 'years',
		dateFormat : "dd/mm/yy"
	});

	$('#salFlwupNxtDate').datepicker({
		useCurrent : false,
		pickTime : false,
		dateFormat : "dd/mm/yy"
	});

	$('#appDate').datepicker({
		useCurrent : false,
		pickTime : false,
		dateFormat : "dd/mm/yy"
	});

//	$('#cusAprDate').val(today);
//	var cusAprDate = document.getElementById("cusAprDate").value;
//	if (cusAprDate != "") {
//		document.getElementById("cusAprDate").value = convertDate(cusAprDate);
//	}
	var salSucSalPreDate = document.getElementById("salSucSalPreDate").value;
	if (salSucSalPreDate != "") {
		document.getElementById("salSucSalPreDate").value = convertDate(salSucSalPreDate);
	}

	if (document.getElementById("cusMStatus").value
			&& (document.getElementById("cusMStatus").value == "Single")) {
		document.getElementById("cusNochd").disabled = true;
//		document.getElementById("cusAgeChd").disabled = true;
	}
	
	$('#cusMStatus').change(function() {
		var cusMStatus = "";
		cusMStatus = document.getElementById("cusMStatus").value;
		if (cusMStatus == "Single") {
			document.getElementById("cusNochd").disabled = true;
//			document.getElementById("cusAgeChd").disabled = true;
		} else {
			document.getElementById("cusNochd").disabled = false;
//			document.getElementById("cusAgeChd").disabled = false;
		}
	});
	
	$('#rType').change(function() {
		var rType = "";
		rType = document.getElementById("rType").value;
		if (rType != "0") {
			$('#rDate').val(today);
		} else {
			document.getElementById("rDate").value = "";
		}
	});
	
	$('#salSucSalPreStatus').change(
		function() {
			var salSucSalPreStatus = "";
			salSucSalPreStatus = document
					.getElementById("salSucSalPreStatus").value;
			if (salSucSalPreStatus == "Yes") {
				$('#salSucSalPreDate').val(today);
			} else {
				document.getElementById("salSucSalPreDate").value = "";
			}
		});
	$('#cusSalaryMonthly')
		.change(
			function() {
				var cusSalaryMonthly = "";
				cusSalaryMonthly = document
						.getElementById("cusSalaryMonthly").value;
				if (cusSalaryMonthly == "") {
					cusSalaryMonthly = 0;
				}
				document.getElementById("cusSalaryYearly").value = cusSalaryMonthly * 12;
				document.getElementById("salRecAffApe").value = (cusSalaryMonthly * 12) * 10 / 100;
			});

	$('#sucAppStatus').change(function() {
		var sucAppStatus = "";
		sucAppStatus = document.getElementById("sucAppStatus").value;
		if (sucAppStatus == "Yes") {
			$('#sucAppDate').val(today);
		} else {
			document.getElementById("sucAppDate").value = "";
		}
	});

	$('#salMthBasExp').change(
		function() {
			var salMthBasExp = "";
			salMthBasExp = document.getElementById("salMthBasExp").value;
			var salRecPolTerm = "";
			salRecPolTerm = document.getElementById("salRecPolTerm").value;
			if (salMthBasExp == "") {
				salMthBasExp = 0;
			}
			document.getElementById("salRecSA").value = salMthBasExp * 12
					* salRecPolTerm;
		});
	$('#salRecPolTerm').change(
		function() {
			var salMthBasExp = "";
			salMthBasExp = document.getElementById("salMthBasExp").value;
			var salRecPolTerm = "";
			salRecPolTerm = document.getElementById("salRecPolTerm").value;
			if (salMthBasExp == "") {
				salMthBasExp = 0;
			}
			document.getElementById("salRecSA").value = salMthBasExp * 12
					* salRecPolTerm;
			document.getElementById("salPolTerm").value = document
					.getElementById("salRecPolTerm").value
		});
	$('#clsBankSoStatus').change(function() {
		var clsBankSoStatus = "";
		clsBankSoStatus = document.getElementById("clsBankSoStatus").value;
		if (clsBankSoStatus == "Yes") {
			$('#clsBankSoDate').val(today);
		} else {
			document.getElementById("clsBankSoDate").value = "";
		}
	});
	$('#clsPrdType').change(function() {
		var clsPrdType = document.getElementById("clsPrdType").value;
		if (clsPrdType == 0) {
			$('#clsBankUploadDate').val("");
		} else {
			$('#clsBankUploadDate').val(today);
		}	
	});
//	End For Both
	$('#btnclosesale').hide();	
});
$("#linkClickInfo").click(function(){
	$('#btnclosesale').hide();
});
$("#linkClickSale").click(function(){
	if(!isclosesale) {
		$('#btnclosesale').hide();	
	} else {
		$('#btnclosesale').hide();	
	}
	validateCloseSale();
});
$("#linkClickFollowup").click(function(){
	$('#btnclosesale').hide();
	if(isclick) {
		$('#linkClickCloseSale').css({'pointer-events': "auto"});
	}
});
$("#linkClickCloseSale").click(function(){
	$('#btnclosesale').hide();
});
$('#btnsaveclosesale').click(function(){
	isclick = true;
	$('#clickCloseSale').addClass('active');
	$('#clickSale').removeClass('active');
	$('#btnclosesale').hide();
	$('input[name=hclosedsale]').val(true);
	$('#modal-closesale').modal('hide');
	if($('input[name=hclosedsale]').val()) {
		isclosesale = true;
		checkCloseSale();
	}
	if(!isclosesale) {
		$('#btnsavedraft').show();
		$('#btnsave').hide();
	} else {
		$('#btnsavedraft').hide();
		$('#btnsave').show();
	}
});

function checkCloseSale() {
	if(isclosesale){
		//Information
		$('#btnclosesale').hide();
		$('#linkClickCloseSale').css({'pointer-events': "auto"});
		$('#rType').after('<input type="hidden" name="rType" value="'+ $('#rType').val() + '" />');
		$('#rType').attr('disabled','disabled');
		$('#rDate').after('<input type="hidden" name="rDate" value="'+ $('#rDate').val() + '" />');
		$('#rDate').attr('disabled','disabled');
		$('#cusAprStatus').after('<input type="hidden" name="cusAprStatus" value="'+ $('#cusAprStatus').val() + '" />');
		$('#cusAprStatus').attr('disabled','disabled');
		//Sale Activity
		$('#sucAppStatus').after('<input type="hidden" name="sucAppStatus" value="'+ $('#sucAppStatus').val() + '" />');
		$('#sucAppStatus').attr('disabled','disabled');
		$('#salSucSalPreStatus').after('<input type="hidden" name="salSucSalPreStatus" value="'+ $('#salSucSalPreStatus').val() + '" />');
		$('#salSucSalPreStatus').attr('disabled','disabled');
		$('#cusSalaryMonthly').after('<input type="hidden" name="cusSalaryMonthly" value="'+ $('#cusSalaryMonthly').val() + '" />');
		$('#cusSalaryMonthly').attr('readonly','readonly');
		$('#salMthBasExp').after('<input type="hidden" name="salMthBasExp" value="'+ $('#salMthBasExp').val() + '" />');
		$('#salMthBasExp').attr('readonly','readonly');
		$('#salRecPrdPkgUsd').after('<input type="hidden" name="salRecPrdPkgUsd" value="'+ $('#salRecPrdPkgUsd').val() + '" />');
		$('#salRecPrdPkgUsd').attr('readonly','readonly');
	}
}

function validateCloseSale() {
	 var recpropack = $('#salRecPrdPkgUsd').val();
	 var monavgincome = $('#cusSalaryMonthly').val();
	 var monbasexp = $('#salMthBasExp').val();
	
	 var statusSuccessfullAppointment = $('#sucAppStatus').find(":selected").val();
	 var statusSuccessfullPresentation = $('#salSucSalPreStatus').find(":selected").val();
	if(statusSuccessfullPresentation == "No" ||  statusSuccessfullAppointment == "No") {
		$('#btnclosesale').hide();
	} else {
		if(monavgincome == "" || monbasexp == "0" || monbasexp == "" || recpropack == "0" || recpropack == "") {
			$('#btnclosesale').hide();
		} else {
			$('#btnclosesale').show();
		}
	}
	
}

function AllowSaleAct() {
//	var aprstatus = $('#cusAprStatus').val();
//	if(aprstatus == "No") {
//		$('#linkClickSale').css({'pointer-events': "none"});
//	} else {
//		$('#linkClickSale').css({'pointer-events': "auto"});
//	}
	var isvalid = checkAllowSaleActivity();
	if (isvalid) {
		$('#linkClickSale').css({'pointer-events': "auto"});
	} else {
		$('#linkClickSale').css({'pointer-events': "none"});
	}
}
//End From New Ferral Page
function allowUpload() {
	if ($('#clsBankSoStatus').val() == "Yes") {
		$('#clsBankUploadStatus').attr('disabled', false);
	} else {
		$('#clsBankUploadStatus').attr('disabled', 'disabled');
	}
}
function convertDate(dateString) {
	var p = dateString.split(/\D/g)
	return [ p[2], p[1], p[0] ].join("/")
}
function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
		return false;

	return true;
}

$('#btnsavedraft').click(
		function() {
			if(!checkValidation()) {
				return false;
			}
			$("#btnsavedraft").prop("disabled", true);
			var cusName = $('#cusName').val();
			var cusSex = $('#cusSex').val();
			var cusAge = $('#cusAge').val();
			var cusMStatus = $('#cusMStatus').val();
			var cusNochd = $('#cusNochd').val();
			var cusPhone = $('#cusPhone').val();
			var cusFacebook = $('#cusFacebook').val();
			var cusSalaryMonthly = $('#cusSalaryMonthly').val();
			var cusSalaryYearly = $('#cusSalaryYearly').val();
			var salRecAffApe = $('#salRecAffApe').val();
			var rType = $('#rType').val();
			var rDate = $('#rDate').val();
			var rRemark = $('#rRemark').val();
			var sucAppStatus = $('#sucAppStatus').val();
			var sucAppDate = $('#sucAppDate').val();
			var sucAppRemark = $('#sucAppRemark').val();
			var clsBankSoStatus = $('#clsBankSoStatus').val();
			var clsBankSoDate = $('#clsBankSoDate').val();
			var clsBankAppNum = $('#clsBankAppNum').val();
			var clsBankUploadStatus = $('#clsBankUploadStatus').val();
			var clsBankUploadDate = $('#clsBankUploadDate').val();
			var clsBankUploadRemark = $('#clsBankUploadRemark').val();
			var APE = $('#APE').val();
			var exisCusStatus = $('#exisCusStatus').val();
			var cusAgeChd = $('#cusAgeChd').val();
			var cusPhone2 = $('#cusPhone2').val();
			var cusOcc = $('#cusOcc').val();
			var cusEmail = $('#cusEmail').val();
			var referralAcbProductNew = [];
			$.each(nameDescFormat, function(index, value) {
				referralAcbProductNew.push($('#'+value).val());
			});
			var cusAprStatus = 'Yes';
			var cusAprDate = $('#cusAprDate').val();
			var salSucSalPreStatus = $('#salSucSalPreStatus').val();
			var salSucSalPreDate = $('#salSucSalPreDate').val();
			var salTypRecPrd = $('#salTypRecPrd').val();
			var salDurPre = $('#salDurPre').val();
			var salMthBasExp = $('#salMthBasExp').val();
			var salRecSA = $('#salRecSA').val();
			var salRecPolTerm = $('#salRecPolTerm').val();
			var salRecPrdPkgUsd = $('#salRecPrdPkgUsd').val();
			var salPolTerm = $('#salPolTerm').val();
			var salEpcClsSal = $('#salEpcClsSal').val();
			var salPolMode = $('#salPolMode').val();
			var salEpcClsPeriod = $('#salEpcClsPeriod').val();
			var customerFrom = $('#customerFrom').val();
			var hclosedsale = $('#hclosedsale').val();
			var clsPrdType = $('#clsPrdType').val();
			var clsPolTerm = $('#clsPolTerm').val();
			var clsPolMode = $('#clsPolMode').val();
			if (formstatus === 'create') {
				var referralBy = $('#referralBy').val();	
			} else {
				var referralBy = $('#referralByHidden').val();
			}
			
			var fappDate = $('#fappDate').val();
			var cid = $('#cid').val();
			var remark = $('#remark').val();
			var draft = "";
			if ($('#sucAppStatus').val() == "No"
					|| $('#sucAppStatus').val() == "Yes"
					&& $('#sucAppStatus').val() == "No") {
				draft = "1";
			} else {
				draft = "2";
			}
			saveDraft(url, cusName, cusSex, cusAge, cusMStatus, cusNochd,
					cusPhone, cusFacebook, cusSalaryMonthly, cusSalaryYearly,
					salRecAffApe, rType, rDate, rRemark, sucAppStatus,
					sucAppDate, sucAppRemark, clsBankSoStatus, clsBankSoDate,
					clsBankAppNum, clsBankUploadStatus, clsBankUploadDate,
					clsBankUploadRemark, APE, exisCusStatus, cusAgeChd,
					cusPhone2, cusOcc, cusEmail, referralAcbProductNew,cusAprStatus, cusAprDate,
					salSucSalPreStatus, salSucSalPreDate, salTypRecPrd,
					salDurPre, salMthBasExp, salRecSA, salRecPolTerm,
					salRecPrdPkgUsd, salPolTerm, salEpcClsSal, salPolMode,
					salEpcClsPeriod, customerFrom, hclosedsale, clsPrdType,
					clsPolTerm, clsPolMode, referralBy, fappDate, cid, remark,
					draft);
		});
function saveDraft(url, cusName, cusSex, cusAge, cusMStatus, cusNochd,
		cusPhone, cusFacebook, cusSalaryMonthly, cusSalaryYearly, salRecAffApe,
		rType, rDate, rRemark, sucAppStatus, sucAppDate, sucAppRemark,
		clsBankSoStatus, clsBankSoDate, clsBankAppNum, clsBankUploadStatus,
		clsBankUploadDate, clsBankUploadRemark, APE, exisCusStatus, cusAgeChd,
		cusPhone2, cusOcc, cusEmail, referralAcbProductNew, cusAprStatus, cusAprDate,
		salSucSalPreStatus, salSucSalPreDate, salTypRecPrd, salDurPre,
		salMthBasExp, salRecSA, salRecPolTerm, salRecPrdPkgUsd, salPolTerm,
		salEpcClsSal, salPolMode, salEpcClsPeriod, customerFrom, hclosedsale,
		clsPrdType, clsPolTerm, clsPolMode, referralBy, fappDate, cid, remark,
		draft) {
	$.ajax({
		url : url,
		type : 'GET',
		data : {
			cusName : cusName,
			cusSex : cusSex,
			cusAge : cusAge,
			cusMStatus : cusMStatus,
			cusNochd : cusNochd,
			cusPhone : cusPhone,
			cusFacebook : cusFacebook,
			cusSalaryMonthly : cusSalaryMonthly,
			cusSalaryYearly : cusSalaryYearly,
			salRecAffApe : salRecAffApe,
			rType : rType,
			rDate : rDate,
			rRemark : rRemark,
			sucAppStatus : sucAppStatus,
			sucAppDate : sucAppDate,
			sucAppRemark : sucAppRemark,
			clsBankSoStatus : clsBankSoStatus,
			clsBankSoDate : clsBankSoDate,
			clsBankAppNum : clsBankAppNum,
			clsBankUploadStatus : clsBankUploadStatus,
			clsBankUploadDate : clsBankUploadDate,
			clsBankUploadRemark : clsBankUploadRemark,
			APE : APE,
			exisCusStatus : exisCusStatus,
			cusAgeChd : cusAgeChd,
			cusPhone2 : cusPhone2,
			cusOcc : cusOcc,
			cusEmail : cusEmail,
			referralAcbProductNew: referralAcbProductNew,
			cusAprStatus : cusAprStatus,
			cusAprDate : cusAprDate,
			salSucSalPreStatus : salSucSalPreStatus,
			salSucSalPreDate : salSucSalPreDate,
			salTypRecPrd : salTypRecPrd,
			salDurPre : salDurPre,
			salMthBasExp : salMthBasExp,
			salRecSA : salRecSA,
			salRecPolTerm : salRecPolTerm,
			salRecPrdPkgUsd : salRecPrdPkgUsd,
			salPolTerm : salPolTerm,
			salEpcClsSal : salEpcClsSal,
			salPolMode : salPolMode,
			salEpcClsPeriod : salEpcClsPeriod,
			customerFrom : customerFrom,
			hclosedsale : hclosedsale,
			clsPrdType : clsPrdType,
			clsPolTerm : clsPolTerm,
			clsPolMode : clsPolMode,
			referralBy : referralBy,
			fappDate : fappDate,
			cid : cid,
			remark : remark,
			draft : draft
		},
		success : function(result) {
			if (result == true) {
				window.location = urlRedirect;
			}
		},
		error : function() {
			console.log(eror);
			$("#btnsavedraft").prop("disabled", false);
		}
	});
}
$('#btnupdatedraft').click(
		function() {
			$("#btnupdatedraft").prop("disabled", true);
			var cusName = $('#cusName').val();
			var cusSex = $('#cusSex').val();
			var cusAge = $('#cusAge').val();
			var cusMStatus = $('#cusMStatus').val();
			var cusNochd = $('#cusNochd').val();
			var cusPhone = $('#cusPhone').val();
			var cusFacebook = $('#cusFacebook').val();
			var cusSalaryMonthly = $('#cusSalaryMonthly').val();
			var cusSalaryYearly = $('#cusSalaryYearly').val();
			var salRecAffApe = $('#salRecAffApe').val();
			var rType = $('#rType').val();
			var rDate = $('#rDate').val();
			var rRemark = $('#rRemark').val();
			var sucAppStatus = $('#sucAppStatus').val();
			var sucAppDate = $('#sucAppDate').val();
			var sucAppRemark = $('#sucAppRemark').val();
			var clsBankSoStatus = $('#clsBankSoStatus').val();
			var clsBankSoDate = $('#clsBankSoDate').val();
			var clsBankAppNum = $('#clsBankAppNum').val();
			var clsBankUploadStatus = $('#clsBankUploadStatus').val();
			var clsBankUploadDate = $('#clsBankUploadDate').val();
			var clsBankUploadRemark = $('#clsBankUploadRemark').val();
			var APE = $('#APE').val();
			var exisCusStatus = $('#exisCusStatus').val();
			var cusAgeChd = $('#cusAgeChd').val();
			var cusPhone2 = $('#cusPhone2').val();
			var cusOcc = $('#cusOcc').val();
			var cusEmail = $('#cusEmail').val();
			var referralAcbProductNew = [];
			$.each(nameDescFormat, function(index, value) {
				referralAcbProductNew.push($('#'+value).val());
			});
			var cusAprStatus = 'Yes';
			var cusAprDate = $('#cusAprDate').val();
			var salSucSalPreStatus = $('#salSucSalPreStatus').val();
			var salSucSalPreDate = $('#salSucSalPreDate').val();
			var salTypRecPrd = $('#salTypRecPrd').val();
			var salDurPre = $('#salDurPre').val();
			var salMthBasExp = $('#salMthBasExp').val();
			var salRecSA = $('#salRecSA').val();
			var salRecPolTerm = $('#salRecPolTerm').val();
			var salRecPrdPkgUsd = $('#salRecPrdPkgUsd').val();
			var salPolTerm = $('#salPolTerm').val();
			var salEpcClsSal = $('#salEpcClsSal').val();
			var salPolMode = $('#salPolMode').val();
			var salEpcClsPeriod = $('#salEpcClsPeriod').val();
			var customerFrom = $('#customerFrom').val();
			var hclosedsale = $('#hclosedsale').val();
			var clsPrdType = $('#clsPrdType').val();
			var clsPolTerm = $('#clsPolTerm').val();
			var clsPolMode = $('#clsPolMode').val();
			var referralBy = $('#referralBy').val();
			var fappDate = $('#fappDate').val();
			var cid = $('#cid').val();
			var remark = $('#remark').val();
			if ($('#sucAppStatus').val() == "No"
				|| $('#sucAppStatus').val() == "Yes"
				&& $('#sucAppStatus').val() == "No") {
				draft = "1";
			} else {
				draft = "2";
			}

			updateDraft(rid, urldraft, draft, cusName, cusSex, cusAge,
					cusMStatus, cusNochd, cusPhone, cusFacebook,
					cusSalaryMonthly, cusSalaryYearly, salRecAffApe, rType,
					rDate, rRemark, sucAppStatus, sucAppDate, sucAppRemark,
					clsBankSoStatus, clsBankSoDate, clsBankAppNum,
					clsBankUploadStatus, clsBankUploadDate,
					clsBankUploadRemark, APE, exisCusStatus, cusAgeChd,
					cusPhone2, cusOcc, cusEmail, referralAcbProductNew, cusAprStatus, cusAprDate,
					salSucSalPreStatus, salSucSalPreDate, salTypRecPrd,
					salDurPre, salMthBasExp, salRecSA, salRecPolTerm,
					salRecPrdPkgUsd, salPolTerm, salEpcClsSal, salPolMode,
					salEpcClsPeriod, customerFrom, hclosedsale, clsPrdType,
					clsPolTerm, clsPolMode, referralBy, fappDate, cid, remark);
		});
function updateDraft(rid, urldraft, draft, cusName, cusSex, cusAge, cusMStatus,
		cusNochd, cusPhone, cusFacebook, cusSalaryMonthly, cusSalaryYearly,
		salRecAffApe, rType, rDate, rRemark, sucAppStatus, sucAppDate,
		sucAppRemark, clsBankSoStatus, clsBankSoDate, clsBankAppNum,
		clsBankUploadStatus, clsBankUploadDate, clsBankUploadRemark, APE,
		exisCusStatus, cusAgeChd, cusPhone2, cusOcc, cusEmail, referralAcbProductNew, cusAprStatus,
		cusAprDate, salSucSalPreStatus, salSucSalPreDate, salTypRecPrd,
		salDurPre, salMthBasExp, salRecSA, salRecPolTerm, salRecPrdPkgUsd,
		salPolTerm, salEpcClsSal, salPolMode, salEpcClsPeriod, customerFrom,
		hclosedsale, clsPrdType, clsPolTerm, clsPolMode, referralBy, fappDate,
		cid, remark) {
	$.ajax({
		url : urldraft,
		type : 'get',
		data : {
			rid : rid,
			cusName : cusName,
			cusSex : cusSex,
			cusAge : cusAge,
			cusMStatus : cusMStatus,
			cusNochd : cusNochd,
			cusPhone : cusPhone,
			cusFacebook : cusFacebook,
			cusSalaryMonthly : cusSalaryMonthly,
			cusSalaryYearly : cusSalaryYearly,
			salRecAffApe : salRecAffApe,
			rType : rType,
			rDate : rDate,
			rRemark : rRemark,
			sucAppStatus : sucAppStatus,
			sucAppDate : sucAppDate,
			sucAppRemark : sucAppRemark,
			clsBankSoStatus : clsBankSoStatus,
			clsBankSoDate : clsBankSoDate,
			clsBankAppNum : clsBankAppNum,
			clsBankUploadStatus : clsBankUploadStatus,
			clsBankUploadDate : clsBankUploadDate,
			clsBankUploadRemark : clsBankUploadRemark,
			APE : APE,
			exisCusStatus : exisCusStatus,
			cusAgeChd : cusAgeChd,
			cusPhone2 : cusPhone2,
			cusOcc : cusOcc,
			cusEmail : cusEmail,
			referralAcbProductNew : referralAcbProductNew,
			cusAprStatus : cusAprStatus,
			cusAprDate : cusAprDate,
			salSucSalPreStatus : salSucSalPreStatus,
			salSucSalPreDate : salSucSalPreDate,
			salTypRecPrd : salTypRecPrd,
			salDurPre : salDurPre,
			salMthBasExp : salMthBasExp,
			salRecSA : salRecSA,
			salRecPolTerm : salRecPolTerm,
			salRecPrdPkgUsd : salRecPrdPkgUsd,
			salPolTerm : salPolTerm,
			salEpcClsSal : salEpcClsSal,
			salPolMode : salPolMode,
			salEpcClsPeriod : salEpcClsPeriod,
			customerFrom : customerFrom,
			hclosedsale : hclosedsale,
			clsPrdType : clsPrdType,
			clsPolTerm : clsPolTerm,
			clsPolMode : clsPolMode,
			referralBy : referralBy,
			fappDate : fappDate,
			cid : cid,
			remark : remark,
			draft : draft
		},
		success : function(result) {
			if (result == true) {
				window.location = urlRedirect;
			}
		},
		error : function() {
			console.log('error');
			$("#btnupdatedraft").prop("disabled", false);
		}
	});
}

function checkSubmit() {
	var isvalid = true;
	if (isclosesale) {
		// Validate APE tab Close Sale
		var ape = $('#APE').val();
		if (ape == "" || ape == "0" || ape == "0.0") {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML + "<br/>Please input APE";
			document.getElementById("APE").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			$('[href="#close"]').tab('show');
			isvalid = false;
		} else {
			document.getElementById("APE").style.background = "white";
		}
		// Validate Application Number tab Close Sale
		var appNum = $('#clsBankAppNum').val();
		if (appNum == "") {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please input application number";
			document.getElementById("clsBankAppNum").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			$('[href="#close"]').tab('show');
			isvalid = false;
		} else {
			document.getElementById("clsBankAppNum").style.background = "white";
		}
		if (appNum.length < 9 || appNum.length > 9) {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please input application number in 9 digits";
			document.getElementById("clsBankAppNum").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			$('[href="#close"]').tab('show');
			isvalid = false;
		} else {
			document.getElementById("clsBankAppNum").style.background = "white";
		}
		// Validate Product Mode
		var clsPolMode = $('#clsPolMode').val();
		if (clsPolMode == "0") {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please select product mode";
			document.getElementById("clsPolMode").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			$('[href="#close"]').tab('show');
			isvalid = false;
		} else {
			document.getElementById("clsPolMode").style.background = "white";
		}
		// Validate Product type tab Close Sale
		var clsPrdType = $('#clsPrdType').val().trim();
		var clsPolTerm = $('#clsPolTerm').val();
		if (clsPrdType === "0") {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please select product type";
			document.getElementById("clsPrdType").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			$('[href="#close"]').tab('show');
			isvalid = false;
		} else {
			document.getElementById("clsPrdType").style.background = "white";
		}
		if (clsPrdType === "BTR1") {
			if (clsPolTerm !== "10" && clsPolTerm !== "12" && clsPolTerm !== "15") {
				Err.innerHTML = "";
				Err.innerHTML = Err.innerHTML
						+ "<br/>Policy Term for this product must be either 10, 12, or 15";
				document.getElementById("clsPolTerm").style.background = "yellow";
				$('html, body').animate({
					scrollTop : 0
				}, 'slow');
				isvalid = false;
			} else {
				document.getElementById("clsPolTerm").style.background = "white";
			}
		}
		if (clsPrdType === "BTR2") {
			if (clsPolTerm !== "10" && clsPolTerm !== "12"
				&& clsPolTerm !== "15") {
				Err.innerHTML = "";
				Err.innerHTML = Err.innerHTML
						+ "<br/>Policy Term for this product must be either 10, 12, or 15";
				document.getElementById("clsPolTerm").style.background = "yellow";
				$('html, body').animate({
					scrollTop : 0
				}, 'slow');
				isvalid = false;	
			} else {
				document.getElementById("clsPolTerm").style.background = "white";
			}
		}
		if (clsPrdType === "BTR3") {
			if (clsPolTerm !== "15") {
				Err.innerHTML = "";
				Err.innerHTML = Err.innerHTML
						+ "<br/>Policy Term for this product must be 15";
				document.getElementById("clsPolTerm").style.background = "yellow";
				$('html, body').animate({
					scrollTop : 0
				}, 'slow');
				isvalid = false;				
			} else {
				document.getElementById("clsPolTerm").style.background = "white";
			}
		}
		if (clsPrdType === "BTR4") {
			if (clsPolTerm !== "10" && clsPolTerm !== "12" && clsPolTerm !== "15") {
				Err.innerHTML = "";
				Err.innerHTML = Err.innerHTML
						+ "<br/>Policy Term for this product must be either 10, 12, or 15";
				document.getElementById("clsPolTerm").style.background = "yellow";
				$('html, body').animate({
					scrollTop : 0
				}, 'slow');
				isvalid = false;	
			} else {
				document.getElementById("clsPolTerm").style.background = "white";
			}
		}
		if (isvalid) {
			$("#btnsave").prop("disabled", true);
			var referralAcbProductNew = [];
			$.each(nameDescFormat, function(index, value) {
				referralAcbProductNew.push($('#'+value).val());
			});
			$('form[name = form]').append('<input type="hidden" value="' + referralAcbProductNew + '" name="referralAcbProductNew" />');
			 document.form.submit();
		} else {
			return false;
		}
		
	}
	else {
		var isvalidinfo = true;
		
		// Validate Referral Type in Tab Information
		var rType = $('#rType').val();
		if (rType == "" || rType == 0) {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please select Referral Type";
			$('#rType').css('background-color', 'yellow');
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			isvalidinfo = false;
		} else {
			$('#rType').css('background-color', 'white');
		}
		
		// Validate Branch in Tab Information
		var branch = $('#branch').val();
		if (branch == "" || branch == 0) {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please select Branch";
			$('#select2-branch-container').css('background-color', 'yellow');
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			isvalidinfo = false;
		} else {
			$('#select2-branch-container').css('background-color', 'white');
		}
		
		// Validate Introducer (Referral By) in Tab Information
		var referralBy = $('#referralBy').val();
		if (referralBy == "" || referralBy == 0) {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please select Introducer";
			$('#select2-referralBy-container').css('background-color', 'yellow');
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			isvalidinfo = false;
		} else {
			$('#select2-referralBy-container').css('background-color', 'white');
		}
		
		// Validate Customer Full Name in Tab Information
		var cusName = $('#cusName').val();
		if (cusName == "" || cusName == 0) {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please input Customer Name";
			$('#cusName').css('background-color', 'yellow');
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			isvalidinfo = false;
		} else {
			$('#cusName').css('background-color', 'white');
		}
		
		// Validate Estimate Age in Tab Information
		var cusAge = $('#cusAge').val();
		if (cusAge == "" || cusAge == 0) {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please input Estimate Age";
			$('#cusAge').css('background-color', 'yellow');
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			isvalidinfo = false;
		} else {
			$('#cusAge').css('background-color', 'white');
		}
		
		// Validate Mobile Number Line (1) in Tab Information
		var cusPhone = $('#cusPhone').val();
		if (cusPhone == "" || cusPhone == 0) {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please input Mobile Number Line (1)";
			$('#cusPhone').css('background-color', 'yellow');
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			isvalidinfo = false;
		} else if (cusPhone.length < 9 || cusPhone.length > 10) {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML + "<br/>Phone number is not valid !";
			$('#cusPhone').css('background-color', 'yellow');
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			isvalidinfo = false;
		} else {
			$('#cusPhone').css('background-color', 'white');
		}
		
		// Validate Occupation in Tab Information
		var cusOcc = $('#cusOcc').val();
		if (cusOcc == '0') {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please select Customer Occupation";
			$('#cusOcc').css('background-color', 'yellow');
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			isvalidinfo = false;
		} else {
			$('#cusOcc').css('background-color', 'white');
		}
		
		if(rType == "0" || branch == "0" || referralBy == "0" || cusName == "" || cusAge == "" || cusPhone == "" || cusPhone.length < 9 || cusPhone.length > 10 || cusOcc == "0") {
			isvalidinfo = false;
		} else {
			isvalidinfo = true;
		}
		
		var referralAcbProductNew = [];
		$.each(nameDescFormat, function(index, value) {
			referralAcbProductNew.push($('#'+value).val());
		});
		return isvalidinfo;
	}
}
function getReferrer(branchCode, formstatus, referralby) {
	$.ajax({ 
		url: urlgetreferrersbyagntnumandbranchcode,
		type: 'get',
		data: { branchCode: branchCode},
		success: function(result) {
			var obj = [];
			obj.push({
				id: '0',
				text: '-- Select Referrer --'
			});
			var referrers = JSON.parse(result);
			for (var i = 0; i < referrers.length; i++) {
			    var obj2 = {
			    		id: referrers[i][0],
			    		text: referrers[i][1]
			    };
			    obj.push(obj2);
			}
			$(".select2-referrer").html('');
			$(".select2-referrer").select2('data', null);
			$(".select2-referrer").select2({
				data: obj
			}).trigger('change');
			if (formstatus === "update" && (referralby != null && referralby != '')) {
				$('.select2-referrer').val(referralby).trigger('change');
				if ($('.select2-referrer').val() == null) {
					$('.select2-referrer').val('0').trigger('change');
				}
			}
		}
	});
}

function HideSaveDraftAndCloseSale() {
	if (!isclosesale) {
		$('#btnsavedraft').show();
		$('#btnsave').hide();
	} else {
		$('#btnsavedraft').hide();
		$('#btnsave').show();
	}
}
function CheckVal() {
	var istrue;
	$.each(nameDescFormat, function(index, value) {
		$('#'+value).val();
		if($('#'+value).val() == '' || $('#'+value).val() == '0') {
			istrue = false;
			return istrue;
		} else {
			istrue = true
			return istrue;
		}
	});
	return istrue;
}
// Function Disable Customer Approach
function DisableCustomerApproach() {
	var checkval = CheckVal();
	var checkval = true; // comment or remove this to enable validation on Other Services
	var rtype = $('#rType').val();
	var rby = $('#referralBy').val();
	var cusname = $('#cusName').val();
	var cusage = $('#cusAge').val();
	var cusphone = $('#cusPhone').val();
	var cusocc = $('#cusOcc').val();
	if (rtype == '0' || rby == '0' || cusname == '' || cusage == ''
			|| cusphone == '' || cusocc == '0' || checkval == false) {
		$('#cusAprStatus').attr('disabled', 'disabled');
		$('#cusAprStatus').after(
				'<input type="hidden" name="cusAprStatus" value="'
						+ $('#cusAprStatus').val() + '" />');
	} else {
		if (!isclosesale) {
			$('#cusAprStatus').attr('disabled', false);
		}
	}
}

// Function Tab Sale Activity Check Successful Presentaion and App
function checkSuccessfulPresentation() {
	var statusSuccessfullPresentation = $('#salSucSalPreStatus').find(
			":selected").val();
	if (statusSuccessfullPresentation == "No") {
		$('#salTypRecPrd').attr('disabled', 'disabled');
		$('#salDurPre').attr('readonly', 'readonly');
		$('#salRecPolTerm').attr('disabled', 'disabled');
		$('#salRecPrdPkgUsd').attr('readonly', 'readonly');
		$('#salPolTerm').attr('disabled', 'disabled');
		$('#salEpcClsSal').attr('readonly', 'readonly');
		$('#salPolMode').attr('disabled', 'disabled');
		$('#salEpcClsPeriod').attr('readonly', 'readonly');
		$('#sucAppRemark').attr('readonly', 'readonly');
		$('#cusSalaryMonthly').attr('readonly', 'readonly');
		$('#salMthBasExp').attr('readonly', 'readonly');
	} else {
		$('#salTypRecPrd').attr('disabled', false);
		$('#salDurPre').attr('readonly', false);
		$('#salRecPolTerm').attr('disabled', false);
		$('#salRecPrdPkgUsd').attr('readonly', false);
		$('#salPolTerm').attr('disabled', false);
		$('#salEpcClsSal').attr('readonly', false);
		$('#salPolMode').attr('disabled', false);
		$('#salEpcClsPeriod').attr('readonly', false);
		$('#sucAppRemark').attr('readonly', false);
		$('#cusSalaryMonthly').attr('readonly', false);
		$('#salMthBasExp').attr('readonly', false);
	}
	validateCloseSale();
}

function checkSuccessfullAppointment() {
	var statusSuccessfullAppointment = $('#sucAppStatus').find(":selected")
			.val();
	if (statusSuccessfullAppointment == "No") {
		$('#salSucSalPreStatus').attr('disabled', 'disabled');
	} else {
		$('#salSucSalPreStatus').attr('disabled', false);
	}
	validateCloseSale();
}

function checkSubmitComment() {
	var cusname = "";
	Err.innerHTML = "";
	var appNum = "";
	var appDate = "";
	var remark = "";
	var cid = "";

	cusname = document.getElementById("cusName").value;
	appNum = document.getElementById("appNum").value;
	appDate = document.getElementById("appDate").value;
	remark = document.getElementById("remark").value;
	cid = document.getElementById("cid").value;

	if (cid == "0" || cid == "") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML + "<br/>Please select comment type!";
		document.getElementById("cid").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} else if (remark.trim() == "") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML + "<br/>Please input remark!";
		document.getElementById("remark").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} else {
		$('#btnSubmit').prop('disabled', true);
		document.form.submit();
		return true;
	}
}

function updateClosedSale(url, rid) {
	$.ajax({
		url : url,
		type : 'get',
		data : {
			rid : rid
		},
		success : function(result) {
			if (result > 0) {
				console.log('Successfull updated');
			}
		},
		error : function() {
			console.log('error');
		}
	});
}

$('#btnclosesale').click(function() {
	var isvalid = checkValidation();
	if(!isvalid) {
		$('#btnclosesale').hide();
	}
	return isvalid;
});

function checkValidation() {
	var isvalid = false;
	// Validate Referral Type
	var rType = "";
	rType = $("#rType").val();
	if (rType == "0") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please select referral type";
		document.getElementById("rType").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("rType").style.background = "white";
	}
	
	// Validate Branch
	var branch = "";
	branch = $('#branch').val();
	if (branch == "0") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please select branch";
		document.getElementById("select2-branch-container").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("select2-branch-container").style.background = "white";
	}
	
	// Validate Introducer
	var referralBy = "";
	referralBy = $('#referralBy').val();
	if (referralBy == "0") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please select introducer";
		document.getElementById("select2-referralBy-container").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("select2-referralBy-container").style.background = "white";
	}
	
	// Validate Customer Full Name
	var cusname = "";
	cusname = document.getElementById("cusName").value;
	if (cusname == "") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML + "<br/>Please fill customer full name!";
		document.getElementById("cusName").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("cusName").style.background = "white";
	}
	
	// Validate Customer Age
	var cusage = "";
	cusage = document.getElementById("cusAge").value;
	if (cusage == "") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML + "<br/>Please fill customer age";
		document.getElementById("cusAge").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("cusAge").style.background = "white";
	}

	// Validate Mobile Number
	var cusPhone = "";
	cusPhone = document.getElementById("cusPhone").value;
	if (cusPhone == "") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please fill customer phone number!";
		document.getElementById("cusPhone").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else if (cusPhone.length < 9 || cusPhone.length > 10) {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML + "<br/>Phone number is not valid !";
		document.getElementById("cusPhone").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("cusPhone").style.background = "white";
	}

	// Validate Occupation
	var cusOcc = "";
	cusOcc = document.getElementById("cusOcc").value;
	if (cusOcc == "0") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please select customer occupation!";
		document.getElementById("cusOcc").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("cusOcc").style.background = "white";
	}
	
	if(rType == "0" || branch == "0" || referralBy == "0" || cusname == "" || cusage == "" || cusPhone == "" || cusPhone.length < 9 || cusPhone.length > 10 || cusOcc == "0") {
		isvalid = false;
	} else {
		isvalid = true;
	}
	return isvalid;
}

function checkAllowSaleActivity() {
	var isvalid = false;
	var rType = $("#rType").val();
	var branch = $('#branch').val();
	var referralBy = $('#referralBy').val();
	var cusname = document.getElementById("cusName").value;
	var cusage = document.getElementById("cusAge").value;
	var cusPhone = document.getElementById("cusPhone").value;
	var cusOcc = document.getElementById("cusOcc").value;
	if(rType == "0" || branch == "0" || referralBy == "0" || cusname == "" || cusage == "" || cusPhone == "" || cusPhone.length < 9 || cusPhone.length > 10 || cusOcc == "0") {
		isvalid = false;
	} else {
		isvalid = true;
	}
	return isvalid;
}

// From Edit Page
function HideUpdateDraft() {
	if(draft) {
		$('#btnupdatedraft').show();
	} else {
		$('#btnupdatedraft').hide();
	}
}
function checkIsCloseSale() {
	if(!isclosesale) {
		$('#btnupdatedraft').show();
		$('#btnsave').hide();
	} else {
		$('#btnupdatedraft').hide();
		$('#btnsave').show();
	}
}
function hideSaveDraft() {
	if(draft) {
		$('#btnupdatedraft').show();
	} else {
		$('#btnupdatedraft').hide();
	}
}
// End From Edit Page