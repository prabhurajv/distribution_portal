// Auto Format New Date Today
	var today = getFormatDate(new Date());
	var referraldate = getFormatDate(new Date(rdate));
	if (clsbankuploaddate === null) {
		clsbankuploaddate = new Date();
	}
	var cbuld = getFormatDate(new Date(clsbankuploaddate));
$(document).ready(function() {
	
	// Start up with Close Tab Close Sale
	$('#linkClickCloseSale').css({'pointer-events': "none"});
	
	// First Check to make which tab to stand for
	if (draft === '1') {
		$('#linkClickSale').trigger('click');
		// Check Disable and Readonly on Sale Activity
		allowSelectPresentation();
		allowSuccessfulyPresentation();
	}
	if (draft === '2') {
		if ($('#salSucSalPreStatus').val() === 'No') {
			allowSuccessfulyPresentation();
		}
		if ($('#sucAppStatus').val() === 'Yes' && $('#salSucSalPreStatus').val() === 'Yes') {
			DisableAndReadOnlyAfterClosed();
			$('#linkClickCloseSale').css({'pointer-events': "auto"});
			$('#linkClickCloseSale').trigger('click');
		} else {
			$('#sucAppStatus').after('<input type="hidden" name="sucAppStatus" value="'+ $('#sucAppStatus').val() + '" />');
			$('#sucAppStatus').attr('disabled', true);
			$('#sucAppDate').attr('readonly', true);
			$('#linkClickSale').trigger('click');
		}
	}
	if (draft === '3') {
		DisableAndReadOnlyAfterClosed();
		$('#linkClickCloseSale').css({'pointer-events': "auto"});
		$('#linkClickCloseSale').trigger('click');
	}
	
	// Information Tab
	
	$(".select2_multiple").select2({
	      placeholder: "Related Bank Products",
	      allowClear: true
	 });
	
	// Filed Select Introducer on Information Tab
	$('#referralBy').select2();

	$(".select2_single").select2({
	  placeholder: "Branch Code - Name"
	});
	
	getReferrer($('#branch').val(), formstatus, $('#referralByHidden').val());
	validateMarital();
	
	// Event OnChange on Branch on Information Tab
	$('#branch').change(function() {
		getReferrer($('#branch').val(), formstatus, $('#referralByHidden').val());
	});
	
	$('#referralBy').change(function() {
		var referralby = $('#referralBy').val();
		if (referralby != null && referralby != '' && referralby != '0') {
			$('#referralByHidden').val(referralby);	
		}
	});
	
	
	// Event OnChange on Referral Type on Information Tab
	$('#rType').change(function() {
		var rType = "";
		rType = $('#rType').val();
		if (rType != "0") {
			$('#rDate').val(referraldate);
		} else {
			$('#rDate').val("");
		}
	});

	// Event OnChange on Marital Status on Information Tab
	$('#cusMStatus').change(function() {
		validateMarital();
	});
	
	// Disable Bank Product Introduced by FC in Information Tab
	var referralAcbProductNew = [];
	var value1 = [];
	var value2 = [];
	$.each(nameDescFormat, function(index, value) {
		var tmp = value.split('-')[0];
		if (tmp === 'cusappby') {
			value1.push(value);
		}
		if (tmp === 'comment') {
			value2.push(value);
		}
	});
	$.each(value1, function(index, value) {
		if ($('#' + value).val() === 'No' || $('#' + value).val() === "") {
			$.each(value2, function(index1, value3) {
				if (value3.split('-')[1] === value.split('-')[1]) {
					$('#' + value3).val('0');
					$('#' + value3).attr("disabled", true); 
				}
			});
		} else {
			$.each(value2, function(index1, value3) {
				if (value3.split('-')[1] === value.split('-')[1]) {
					$('#' + value3).attr("disabled", false);
				}
			});
		}
		$('#' + value).change(function(){
			if ($('#' + value).val() === 'No' || $('#' + value).val() === "") {
				$.each(value2, function(index1, value3) {
					if (value3.split('-')[1] === value.split('-')[1]) {
						$('#' + value3).val('0');
						$('#' + value3).attr("disabled", true); 
					}
				});
			} else {
				$.each(value2, function(index1, value3) {
					if (value3.split('-')[1] === value.split('-')[1]) {
						$('#' + value3).attr("disabled", false); 
					}
				});
			}
		});
	});
	//	End of Information Tab
	
	//	For Sale Activity Tab
	
	// On Change on Successful Appointment
	$('#sucAppStatus').change(function() {
		allowSelectPresentation();
	});

	 $('#datetimepicker1').datetimepicker({
		format: "DD/MM/YYYY hh:mm a",
		minDate: sucAppDate,
    	defaultDate: sucAppDate
     });
	 
	// On Change on Successful Presentation
	$('#salSucSalPreStatus').change(function() {
		allowSuccessfulyPresentation();
	});
	
	// On Change on Monthly Average Income Sale Activity Tab
	$('#cusSalaryMonthly').change(function() {
		var cusSalaryMonthly = $('#cusSalaryMonthly').val();
		if (cusSalaryMonthly == "") { cusSalaryMonthly = 0;}
		$('#cusSalaryYearly').val(cusSalaryMonthly * 12);
		$('#cusAFPremium').val((cusSalaryMonthly * 12) * 10 / 100)
	});
	//	End of Sale Activity Tab
	
	// For Close Sale
	$('#clsPrdType').change(function() {
		if($('#clsPrdType').val() === "0") {
			$('#clsBankUploadDate').val("");	
		} else {
			$('#clsBankUploadDate').val(cbuld);
		}
		
	});
	// End Close Sale
	
});

// Function to CheckMatial Status On Information Tab
function validateMarital() {
	var cusMStatus = $('#cusMStatus').val();
	if (cusMStatus == "Single") {
		$('#cusNochd').val('0');
		$('#cusNochd').prop('disabled', true);
	} else {
		$('#cusNochd').prop('disabled', false);
	}
}

// Function to Submit the Form
function checkSubmit(btnaction) {
	if (btnaction == 'saveinfo') {
		if (!checkValidationInfoTab()) {return false;}
		if (!checkValidationProduct()) {return false;}
		$('#btnsaveinfo').attr('disabled', true);
		var referralAcbProductNew = [];
		$.each(nameDescFormat, function(index, value) {
			referralAcbProductNew.push($('#'+value).val());
		});
		$('form[name = form]').append('<input type="hidden" value="' + referralAcbProductNew + '" name="referralAcbProductNew" />');
	} else if (btnaction == 'savesale') { 
		if (!checkValidationSaleTab()) {return false;}
		$('#btnsavesale').attr('disabled', true);
	} else if(btnaction == 'savefinal') {
		if (!checkValidationClosedSale()) {return false;}
		$('#btnsavefinal').attr('disabled', true);
	} else {
		return false;
	}
	
	$('form[name = form]').append('<input type="hidden" value="' + btnaction + '" name="btnaction" />');
	document.form.submit();
}

// Function to get Referrer into Introducer
function getReferrer(branchCode, formstatus, referralby) {
	$.ajax({ 
		url: urlgetreferrersbyagntnumandbranchcode,
		type: 'get',
		data: { branchCode: branchCode},
		success: function(result) {
			var obj = [];
			obj.push({
				id: '0',
				text: '-- Select Referrer --'
			});
			var referrers = JSON.parse(result);
			for (var i = 0; i < referrers.length; i++) {
			    var obj2 = {
			    		id: referrers[i][0],
			    		text: referrers[i][1]
			    };
			    obj.push(obj2);
			}
			$(".select2-referrer").html('');
			$(".select2-referrer").select2('data', null);
			$(".select2-referrer").select2({
				data: obj
			}).trigger('change');
			if (formstatus === "update" && (referralby != null && referralby != '')) {
				$('.select2-referrer').val(referralby).trigger('change');
				if ($('.select2-referrer').val() == null) {
					$('.select2-referrer').val('0').trigger('change');
				}
			}
		}
	});
}
function checkValidationInfoTab() {
	var isvalid = false;
	// Validate Referral Type
	var rType = "";
	rType = $("#rType").val();
	if (rType == "0") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please select referral type";
		document.getElementById("rType").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("rType").style.background = "white";
	}
	
	// Validate Branch
	var branch = "";
	branch = $('#branch').val();
	if (branch == "0") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please select branch";
		document.getElementById("select2-branch-container").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("select2-branch-container").style.background = "white";
	}
	
	// Validate Introducer
	var referralBy = "";
	referralBy = $('#referralBy').val();
	if (referralBy == "0") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please select introducer";
		document.getElementById("select2-referralBy-container").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("select2-referralBy-container").style.background = "white";
	}
	
	// Validate Customer Full Name
	var cusname = "";
	cusname = document.getElementById("cusName").value.trim();
	if (cusname == "") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML + "<br/>Please fill customer full name!";
		document.getElementById("cusName").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("cusName").style.background = "white";
	}
	
	// Validate Customer Age
	var cusage = "";
	cusage = document.getElementById("cusAge").value;
	if (cusage == "") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML + "<br/>Please fill customer age";
		document.getElementById("cusAge").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("cusAge").style.background = "white";
	}

	// Validate Mobile Number
	var cusPhone = "";
	cusPhone = document.getElementById("cusPhone").value;
	if (cusPhone == "") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please fill customer phone number!";
		document.getElementById("cusPhone").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else if (cusPhone.length < 9 || cusPhone.length > 10) {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML + "<br/>Phone number is not valid !";
		document.getElementById("cusPhone").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("cusPhone").style.background = "white";
	}

	// Validate Occupation
	var cusOcc = "";
	cusOcc = document.getElementById("cusOcc").value;
	if (cusOcc == "0") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please select customer occupation!";
		document.getElementById("cusOcc").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#info"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("cusOcc").style.background = "white";
	}
	
	if(rType == "0" || branch == "0" || referralBy == "0" || cusname == "" || cusage == "" || cusPhone == "" || cusPhone.length < 9 || cusPhone.length > 10 || cusOcc == "0") {
		isvalid = false;
	} else {
		isvalid = true;
	}
	return isvalid;
}
function checkValidationSaleTab() {
	var isvalid = true;
	// Validate Successful Appointment
	var sucAppStatus = $("#sucAppStatus").val();
	if (sucAppStatus == "No") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please Select Successful Appointment";
		document.getElementById("sucAppStatus").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#sale"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("sucAppStatus").style.background = "white";
	}
	
	// Validate Successfult Appointment Date
	var sucAppDate = $('#sucAppDate').val();
	if (sucAppDate == null || sucAppDate == '') {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please Select Successful Appointment Date";
		document.getElementById("sucAppDate").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#sale"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("sucAppDate").style.background = "white";
	}
	
	// Validate Successful Presentation
	var salSucSalPreStatus = $('#salSucSalPreStatus').val();
	if (salSucSalPreStatus === "Yes") {
		var cusSalaryMonthly = $('#cusSalaryMonthly').val();
		if (cusSalaryMonthly === null || cusSalaryMonthly === '' || cusSalaryMonthly === '0' || cusSalaryMonthly === '0.0') {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please Input Monthly Average Income.";
			document.getElementById("cusSalaryMonthly").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			$('[href="#sale"]').tab('show');
			isvalid = false;
		} else {
			document.getElementById("cusSalaryMonthly").style.background = "white";
		}
		
		var salMthBasExp = $('#salMthBasExp').val();
		if (salMthBasExp === null || salMthBasExp === '' || salMthBasExp === '0' || salMthBasExp === '0.0') {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please Input Monthly Basic Expense.";
			document.getElementById("salMthBasExp").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			$('[href="#sale"]').tab('show');
			isvalid = false;
		} else {
			document.getElementById("salMthBasExp").style.background = "white";
		}
		
		var salRecPrdPkgUsd = $('#salRecPrdPkgUsd').val();
		if (salRecPrdPkgUsd === null || salRecPrdPkgUsd === '' || salRecPrdPkgUsd === '0' || salRecPrdPkgUsd === '0.0') {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please Input Recommended Product Package.";
			document.getElementById("salRecPrdPkgUsd").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			$('[href="#sale"]').tab('show');
			isvalid = false;
		} else {
			document.getElementById("salRecPrdPkgUsd").style.background = "white";
		}
	}
	return isvalid;
}
function checkValidationClosedSale() {
	var isvalid = true;
	
	var clsPrdType = $('#clsPrdType').val();
	if (clsPrdType === null || clsPrdType === '' || clsPrdType === '0') {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please Select the Product Type";
		document.getElementById("clsPrdType").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#close"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("clsPrdType").style.background = "white";
	}
	
	var clsPolMode = $('#clsPolMode').val();
	if (clsPolMode === null || clsPolMode === '' || clsPolMode === '0') {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please Select Payment Mode";
		document.getElementById("clsPolMode").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#close"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("clsPolMode").style.background = "white";
	}
	
	var APE = $('#APE').val();
	if (APE === null || APE === '' || APE === '0' || APE === '0.0') {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please Input APE";
		document.getElementById("APE").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#close"]').tab('show');
		isvalid = false;
	} else {
		document.getElementById("APE").style.background = "white";
	}
	
	var clsBankAppNum = $('#clsBankAppNum').val();
	if (clsBankAppNum === null || clsBankAppNum === '' || clsBankAppNum === '0') {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please Input Application Number";
		document.getElementById("clsBankAppNum").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		$('[href="#close"]').tab('show');
		isvalid = false;
	} else {
		if (clsBankAppNum.length < 9 || clsBankAppNum.length > 9) {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please input application number in 9 digits";
			document.getElementById("clsBankAppNum").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			$('[href="#close"]').tab('show');
			isvalid = false;
		} else {
			document.getElementById("clsBankAppNum").style.background = "white";
		}
		document.getElementById("clsBankAppNum").style.background = "white";
	}
	 
	return isvalid;
}
function allowSelectPresentation() {
	var sucAppStatus = $('#sucAppStatus').val();
	if(sucAppStatus === 'No') {
		$('#sucAppDate').val("");
		$('#sucAppDate').attr('disabled', true);
		$('#salSucSalPreStatus').attr('disabled', true);
	} else {
		$('#sucAppDate').attr('disabled', false);
		$('#salSucSalPreStatus').attr('disabled', false);
	}
} 
function allowSuccessfulyPresentation() {
	var salSucSalPreStatus = $('#salSucSalPreStatus').val();
	if(salSucSalPreStatus === 'No') {
		$('#salSucSalPreDate').val('');
		$('#cusSalaryMonthly').val('0.0');
		$('#salMthBasExp').val('0');
		$('#salRecPrdPkgUsd').val('0');
		$('#cusSalaryMonthly').attr('readonly', true);
		$('#salMthBasExp').attr('readonly', true);
		$('#salRecPrdPkgUsd').attr('readonly', true);
	} else {
		$('#salSucSalPreDate').val(today);
		$('#cusSalaryMonthly').attr('readonly', false);
		$('#salMthBasExp').attr('readonly', false);
		$('#salRecPrdPkgUsd').attr('readonly', false);
	}
}
//To Disable and Readonly after the Draft to Closed.
function DisableAndReadOnlyAfterClosed() {
	$('#sucAppStatus').attr('disabled', true);
	$('#sucAppStatus').after('<input type="hidden" name="sucAppStatus" value="'+ $('#sucAppStatus').val() + '" />');
	$('#sucAppDate').attr('readonly', true);
	$('#salSucSalPreStatus').attr('disabled', true);
	$('#salSucSalPreStatus').after('<input type="hidden" name="salSucSalPreStatus" value="'+ $('#salSucSalPreStatus').val() + '" />');
}
function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
		return false;

	return true;
}
function getFormatDate(date) {
	var dd = date.getDate();
	var mm = date.getMonth() + 1;
	var yyyy = date.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	}
	if (mm < 10) {
		mm = '0' + mm;
	}
	var today = dd + '/' + mm + '/' + yyyy;
	return today;
}
function checkValidationProduct() {
	isvalid = true;
	var value1 = [];
	var value2 = [];
	$.each(nameDescFormat, function(index, value) {
		var tmp = value.split('-')[0];
		if (tmp === 'comment') {
			value2.push(value);
		}
		if (tmp === 'cusappby') {
			value1.push(value);
		}
	});
	$.each(value1, function(index, value4) {
		if ($('#'+value4).val() === '') {
			Err.innerHTML = "";
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please select the filed";
			document.getElementById(value4).style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			$('[href="#info"]').tab('show');
			isvalid = false;
		} else {
			document.getElementById(value4).style.background = "white";
		}
		if ($('#'+value4).val() === 'Yes') {
			$.each(value2, function(index, value5) {
				if (value4.split('-')[1] === value5.split('-')[1]) {
					if ($('#' + value5).val() === '0') {
						Err.innerHTML = "";
						Err.innerHTML = Err.innerHTML
								+ "<br/>Please select the filed";
						document.getElementById(value5).style.background = "yellow";
						$('html, body').animate({
							scrollTop : 0
						}, 'slow');
						$('[href="#info"]').tab('show');
						isvalid = false;
					} else {
						document.getElementById(value5).style.background = "white";
					}
				}
			});
		}
	});
	return isvalid;
}