$(document).ready(function () {
	GetIssuances();
});
function GetIssuanceBranchSummary() {
	$.ajax({
		url: urlissuancesummary,
		type: 'get',
		data: { summaryby: 'branch' },
		success: function(result) {
			if(result !== '') {
				var totalape = 0;
				var dataSet = JSON.parse(result);
				var tablesummarybranch = $('#branch_grid').DataTable();
				tablesummarybranch.destroy();
				tablesummarybranch = $('#branch_grid').DataTable({
					data: dataSet,
					dom: 'TC<"clearfix">lfrt<"bottom"ip>',
					"columns": [{
							data: '0'
						},
						{
							data: '1'
						},
						{
							data: '2'
						},
						{
							data: '3'
						}
					],
					"columnDefs": [
					               {
					            	   "targets": 0,
					            	   	createdCell: function (td, cellData, rowData, row, col) {
					            	   		var btnDetail = $(td);
					            	   		$(td).css('cursor', 'pointer');
					            	   		$(btnDetail).click(function (e) {
					            	   			var status = $(this).text().trim();
					            	   		  	var tablapsed = $('#issuance_grid').dataTable();
						            	  	    if(status != null) {
						            	  	    	tablapsed.fnFilter(status);
						            	  	    } else {
						            	  	    	tablapsed.fnFilter('');
						            	  	    }
						            	  	  	$('html, body').animate({
						            	        	scrollTop: $("#move-filter").offset().top
						            	      	}, 500);
					            	   		});
										} 
					               },
					               { 
					            	   className: "currency",
					            	   "targets": [3] ,
					            	   createdCell: function (td, cellData, rowData, row, col) {
											totalape += cellData * 1;
										} 
					               },
					               { className: "middle-algin", "targets": [2] }
					             ],
					"fnDrawCallback": function (oSettings, json) {
						//showLoading(false);
					}
				});
				$('#branch_grid').on( 'draw.dt', function () {
				    var info = tablesummarybranch.page.info();
				    var arr = $('#branch_grid_info').text().trim().split('-');
				    $('#branch_grid_info').html(arr[0] + "- (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
				} );
				var arr = $('#branch_grid_info').text().trim().split('-');
				$('#branch_grid_info').html(arr[0] +" - (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
			}
		}
	});
}
function GetIssuanceAgentSummary() {
	$.ajax({
		url: urlissuancesummary,
		type: 'get',
		data: { summaryby: 'agent' },
		success: function(result) {
			if(result !== '') {
				var totalape = 0;
				var dataSet = JSON.parse(result);
				var tablesummarybranch = $('#agent_grid').DataTable();
				tablesummarybranch.destroy();
				tablesummarybranch = $('#agent_grid').DataTable({
					data: dataSet,
					dom: 'TC<"clearfix">lfrt<"bottom"ip>',
					"columns": [{
							data: '0'
						},
						{
							data: '1'
						},
						{
							data: '2'
						},
						{
							data: '3'
						}
					],
					"columnDefs": [
					               { 
					            	   className: "currency",
					            	   "targets": [3] ,
					            	   createdCell: function (td, cellData, rowData, row, col) {
											totalape += cellData * 1;
										} 
					               },
					               { className: "middle-algin", "targets": [2] }
					             ],
					"fnDrawCallback": function (oSettings, json) {
						//showLoading(false);
					}
				});
				$('#agent_grid').on( 'draw.dt', function () {
				    var info = tablesummarybranch.page.info();
				    var arr = $('#agent_grid_info').text().trim().split('-');
				    $('#agent_grid_info').html(arr[0] + "- (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
				} );
				var arr = $('#agent_grid_info').text().trim().split('-');
				$('#agent_grid_info').html(arr[0] +" - (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
			}
		}
	});
}
function GetIssuances() {
	//Function Get Issuances
	$('.right_col').loading({
		message: 'Loading Please wait...'
	});
	GetIssuanceAgentSummary();
	GetIssuanceBranchSummary();
	$.ajax({
		url: urlissuance,
		type: 'get',
		success: function(result) {
			var totalape = 0;
			var dataSet = JSON.parse(result);
			var tablecollection = $('#issuance_grid').DataTable();
			tablecollection.destroy();
			tablecollection = $('#issuance_grid').DataTable({
				data: dataSet,
				dom: 'TC<"clearfix">lfrt<"bottom"ip>',
				"columns": [{
						data: 'chdrNum'
					},
					{
						data: 'chdrAppNum'
					},
					{
						data: 'poName'
					},
					{
						data: 'subDate'
					},
					{
						data: 'chdrKeyDate'
					},
					{
						data: 'chdrIssDate'
					},
					{
						data: 'chdrApe'
					},
					{
						data: 'agntNum'
					},
					{
						data: 'agntName'
					},
					{
						data: 'weekth'
					},
					{
						data: 'branchCode'
					},
					{
						data: 'branchName'
					}
				],
				columnDefs: [
							{
								targets: -2,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).css('display', 'none');
								}
							},
							{
								targets: -5,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).css('display', 'none');
								}
							},
							{
								targets: 6,
								createdCell: function (td, cellData, rowData, row, col) {
									totalape += cellData * 1;
								}
							}
						],
				"fnDrawCallback": function (oSettings, json) {
					//showLoading(false);
				}
			});
			$('#issuance_grid').on( 'draw.dt', function () {
			    var info = tablecollection.page.info();
			    var arr = $('#issuance_grid_info').text().trim().split('-');
			    $('#issuance_grid_info').html(arr[0] + "- (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
			} );
			var arr = $('#issuance_grid_info').text().trim().split('-');
			$('#issuance_grid_info').html(arr[0] +" - (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
			$('.right_col').loading('stop');
		}
	});
	//End Functioin Get Issuances
}