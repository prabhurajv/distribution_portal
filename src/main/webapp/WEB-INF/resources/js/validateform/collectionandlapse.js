$(".branch").select2({
     
});
$(document).ready(function () {	
//	GetCollectionAndLapse();
	$('#thcolduedateto').hide();
	$('#thladurationto').hide();
	$('#thladateto').hide();
	$('#thcolapeto').hide();
	$('#thlaapeto').hide();
	if(type == 'collection') {
		$('[href="#renewalpremiumcollection"]').tab('show');
	} else {
		$('[href="#lapsedreport"]').tab('show');
	}
	$('#colduedate').datepicker({
		useCurrent: true,
		pickTime: false,
		dateFormat: "dd/mm/yy"
	});

	$('#colduedateto').datepicker({
		useCurrent: true,
		pickTime: false,
		dateFormat: "dd/mm/yy"
	});
	
	$('#ladate').datepicker({
		useCurrent: true,
		pickTime: false,
		dateFormat: "dd/mm/yy"
	});
	
	$('#ladateto').datepicker({
		useCurrent: true,
		pickTime: false,
		dateFormat: "dd/mm/yy"
	});
	
	
	$('#colapeoperator').change(function () {
		if ($('#colapeoperator').val() == '6') {
			$('#thcolapeto').show();
		} else {
			$('#thcolapeto').hide();
			$('#colapeto').val('');
		}
		GetCollection(urladvancesearchcollection, true, $('form#colform'));
	});
	
	$('#colape').change(function() {
		$('#colape').val();
		GetCollection(urladvancesearchcollection, true, $('form#colform'));
	});
	
	$('#colapeto').change(function() {
		$('#colapeto').val();
		GetCollection(urladvancesearchcollection, true, $('form#colform'));
	});
	
	$('#colduedateoperator').change(function () {
		if ($('#colduedateoperator').val() == '6') {
			$('#thcolduedateto').show();
		} else {
			$('#thcolduedateto').hide();
			$('#colduedateto').val('');
		}
		GetCollection(urladvancesearchcollection, true, $('form#colform'));
	});
	
	$('#clbranchcodeandname').change(function() {
		$('#clbranchcodeandname').val();
		GetCollection(urladvancesearchcollection, true, $('form#colform'));
	});
	$('#clagentcodeandname').change(function() {
		$('#clagentcodeandname').val();
		GetCollection(urladvancesearchcollection, true, $('form#colform'));
	});
	$('#clbdmcode').change(function() {
		$('#clbdmcode').val();
		GetCollection(urladvancesearchcollection, true, $('form#colform'));
	});
	$('#clagentstatus').change(function() {
		$('#clagentstatus').val();
		GetCollection(urladvancesearchcollection, true, $('form#colform'));
	});
	$('#clbcucallstatus').change(function() {
		$('#clbcucallstatus').val();
		GetCollection(urladvancesearchcollection, true, $('form#colform'));
	});
	
	$('#colduedate').change(function() {
		$('#colduedate').val();
		GetCollection(urladvancesearchcollection, true, $('form#colform'));
	});
	
	$('#colduedateto').change(function() {
		$('#colduedateoperator option:selected').text();
		GetCollection(urladvancesearchcollection, true, $('form#colform'));
	});
	
	$('#clproduct').change(function() {
		$('#clproduct').val();
		GetCollection(urladvancesearchcollection, true, $('form#colform'));
	});

	// for lapsed
	$('#thladuedateto').hide();	
	
	$('#laduedate').datepicker({
		useCurrent: true,
		pickTime: false,
		dateFormat: "dd/mm/yy"
	});

	$('#laduedateto').datepicker({
		useCurrent: true,
		pickTime: false,
		dateFormat: "dd/mm/yy"
	});
	
	$('#laduedateoperator').change(function () {
		if ($('#laduedateoperator').val() == '6') {
			$('#thladuedateto').show();
		} else {
			$('#thladuedateto').hide();
			$('#laduedateto').val('');
		}
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	
	$('#laapeoperator').change(function () {
		if ($('#laapeoperator').val() == '6') {
			$('#thlaapeto').show();
		} else {
			$('#thlaapeto').hide();
			$('#laapeto').val('');
		}
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	
	$('#laape').change(function() {
		$('#laape').val();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	
	$('#laapeto').change(function() {
		$('#laape').val();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	
	$('#labranchcodeandname').change(function() {
		$('#labranchcodeandname').val();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	$('#laagentcodeandname').change(function() {
		$('#laagentcodeandname').val();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	$('#labdmcode').change(function() {
		$('#labdmcode').val();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	$('#laagentstatus').change(function() {
		$('#laagentstatus').val();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	$('#labcucallstatus').change(function() {
		$('#labcucallstatus').val();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	
	$('#laduedate').change(function() {
		$('#laduedate').val();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	
	$('#laduedateto').change(function() {
		$('#laduedateoperator option:selected').text();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	
	$('#laisimpactpersistency').change(function() {
		$('#laisimpactpersistency').val();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	
	$('#lareason').change(function() {
		$('#lareason').val();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	
	$('#laduration').change(function() {
		$('#laduration').val();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	
	$('#ladurationto').change(function() {
		$('#ladurationto option:selected').text();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	$('#laproduct').change(function() {
		$('#laproduct').val();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	

	$('#download_csv').click(function(e) {
		 e.preventDefault(); 
		 var url =  GetLapsedURL(urlcsvadvancesearchlapsed, true, $('form#laform'));
		 window.open(url);
	});
	
	$('#ladurationoperator').change(function () {
		if ($('#ladurationoperator').val() == '6') {
			$('#thladurationto').show();
		} else {
			$('#thladurationto').hide();
			$('#ladurationto').val('');
		}
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	
	$('#ladate').change(function() {
		$('#ladate').val();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	
	$('#ladateto').change(function() {
		$('#ladateto option:selected').text();
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	});
	
	$('#ladateoperator').change(function () {
		if ($('#ladateoperator').val() == '6') {
			$('#thladateto').show();
		} else {
			$('#thladateto').hide();
			$('#ladateto').val('');
		}
		GetLapsed(urladvancesearchlapsed, true, $('form#laform'));
	}); 
	
	// Select Data into dropdown	
	getDropdownCollectionBranchAndCode();
	getDropdownCollectionAgentCodeAndName();
	getDropdownCollectionBdmCode();
	getDropdownCollectionAgentStatus();
	getDropdownCollectionBcuCallStatus();
	getDropdownCollectionProductCode();
	
	GetCollection(urlgetcollection, false, null);
	GetLapsed(urlgetlapsed, false, null);
	GetSummaryBCUCallStatusCol();
	GetSummaryBCUCallStatusLa();
	GetUncontactablityLa();
	getDropdownLapseIsImpactPersistency();
	getDropdownLapseLapsedReason();
});
//Button Advance Search of Renewal Premium Collection 
$('#btnAdvanceSearchRenewalPremiumCollection').click(function (e) {
	showModalAdvanceSearchRenewalPremiumCollectionDialog.show();
});
$('#btnAdvanceSearchLapsed').click(function (e) {
	showModalAdvanceSearchLapsedDialog.show();
});
// End Button Advance Search of Renewal Premium Collection
function GetCollectionAgentSummary() {
	$.ajax({
		url: urlcollectionsummary,
		type: 'get',
		data: { summaryby: 'agent' },
		success: function(result) {
			var totalape = 0;
			var dataSet = JSON.parse(result);
			var tablesummarybranch = $('#agent_grid').DataTable();
			tablesummarybranch.destroy();
			tablesummarybranch = $('#agent_grid').DataTable({
				"lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
				data: dataSet,
				dom: 'TC<"clearfix">lfrt<"bottom"ip>',
				"columns": [{
						data: '0'
					},
					{
						data: '1'
					},
					{
						data: '2'
					},
					{
						data: '3',
					    render: $.fn.dataTable.render.number( ',', '.', 2 )
					}
				],
				"columnDefs": [
				               { 
				            	   className: "currency", 
				            	   "targets": [3],
				            	   createdCell: function (td, cellData, rowData, row, col) {
										totalape += cellData * 1;
									} 
				               },
				               { className: "middle-algin", "targets": [2] }
				             ],
				"fnDrawCallback": function (oSettings, json) {
					//showLoading(false);
				}
			});
			$('#agent_grid').on( 'draw.dt', function () {
			    var info = tablesummarybranch.page.info();
			    var arr = $('#agent_grid_info').text().trim().split('-');
			    $('#agent_grid_info').html(arr[0] + "- (EPA = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
			} );
			var arr = $('#agent_grid_info').text().trim().split('-');
			$('#agent_grid_info').html(arr[0] +" - (EPA = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
		}
	});
}
function GetCollectionBranchSummary() {
	$.ajax({
		url: urlcollectionsummary,
		type: 'get',
		data: { summaryby: 'branch' },
		success: function(result) {
			var totalape = 0;
			var dataSet = JSON.parse(result);
			var tablesummarybranch = $('#branch_grid').DataTable();
			tablesummarybranch.destroy();
			tablesummarybranch = $('#branch_grid').DataTable({
				"lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
				data: dataSet,
				dom: 'TC<"clearfix">lfrt<"bottom"ip>',
				"columns": [{
						data: '0'
					},
					{
						data: '1'
					},
					{
						data: '2'
					},
					{
						data: '3',
						render: $.fn.dataTable.render.number( ',', '.', 2 )
					}
				],
				"columnDefs": [
				               {
				            	   className: "currency",
				            	   "targets": [3],
				            	   createdCell: function (td, cellData, rowData, row, col) {
										totalape += cellData * 1;
									} 
				               },
				               { className: "middle-algin", "targets": [2] }
				             ],
				"fnDrawCallback": function (oSettings, json) {
					//showLoading(false);
				}
			});
			$('#branch_grid').on( 'draw.dt', function () {
			    var info = tablesummarybranch.page.info();
			    var arr = $('#branch_grid_info').text().trim().split('-');
			    $('#branch_grid_info').html(arr[0] + "- (EPA = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
			} );
			var arr = $('#branch_grid_info').text().trim().split('-');
			$('#branch_grid_info').html(arr[0] +" - (EPA = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
		}
	});
}
function GetLapseAgentSummary() {
	$.ajax({
		url: urllapsesummary,
		type: 'get',
		data: { summaryby: 'agent' },
		success: function(result) {
			var totalape = 0;
			var dataSet = JSON.parse(result);
			var tablesummarybranch = $('#agent_grid_la').DataTable();
			tablesummarybranch.destroy();
			tablesummarybranch = $('#agent_grid_la').DataTable({
				"lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
				data: dataSet,
				dom: 'TC<"clearfix">lfrt<"bottom"ip>',
				"columns": [{
						data: '0'
					},
					{
						data: '1'
					},
					{
						data: '2'
					},
					{
						data: '3',
						render: $.fn.dataTable.render.number( ',', '.', 2 )
					}
				],
				"columnDefs": [
				               { 
				            	   className: "currency",
				            	   "targets": [3] ,
				            	   createdCell: function (td, cellData, rowData, row, col) {
										totalape += cellData * 1;
									} 
				               },
				               { className: "middle-algin", "targets": [2] }
				             ],
				"fnDrawCallback": function (oSettings, json) {
					//showLoading(false);
				}
			});
			$('#agent_grid_la').on( 'draw.dt', function () {
			    var info = tablesummarybranch.page.info();
			    var arr = $('#agent_grid_la_info').text().trim().split('-');
			    $('#agent_grid_la_info').html(arr[0] + "- (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
			} );
			var arr = $('#agent_grid_la_info').text().trim().split('-');
			$('#agent_grid_la_info').html(arr[0] +" - (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
		}
	});
}
function GetLapseBranchSummary() {
	$.ajax({
		url: urllapsesummary,
		type: 'get',
		data: { summaryby: 'branch' },
		success: function(result) {
			var totalape = 0;
			var dataSet = JSON.parse(result);
			var tablesummarybranch = $('#branch_grid_la').DataTable();
			tablesummarybranch.destroy();
			tablesummarybranch = $('#branch_grid_la').DataTable({
				"lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
				data: dataSet,
				dom: 'TC<"clearfix">lfrt<"bottom"ip>',
				"columns": [{
						data: '0'
					},
					{
						data: '1'
					},
					{
						data: '2'
					},
					{
						data: '3',
						render: $.fn.dataTable.render.number( ',', '.', 2 )
					}
				],
				"columnDefs": [
				               {
				            	   className: "currency", 
				            	   "targets": [3] ,
				            	   createdCell: function (td, cellData, rowData, row, col) {
										totalape += cellData * 1;
									} 
				               },
				               { className: "middle-algin", "targets": [2] }
				             ],
				"fnDrawCallback": function (oSettings, json) {
					//showLoading(false);
				}
			});
			$('#branch_grid_la').on( 'draw.dt', function () {
			    var info = tablesummarybranch.page.info();
			    var arr = $('#branch_grid_la_info').text().trim().split('-');
			    $('#branch_grid_la_info').html(arr[0] + "- (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
			} );
			var arr = $('#branch_grid_la_info').text().trim().split('-');
			$('#branch_grid_la_info').html(arr[0] +" - (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
		}
	});
}
function GetSummaryBCUCallStatusCol() {
	$.ajax({
		url: urlbcucallstatussummary,
		type: 'get',
		data: { type: 'CL' },
		success: function(result) {
			var totalape = 0;
			var totalcase = 0;
			var dataSet = JSON.parse(result);
			var tablesummarybcu = $('#bcu_grid').DataTable();
			tablesummarybcu.destroy();
			tablesummarybcu = $('#bcu_grid').DataTable({
				"lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
				data: dataSet,
				dom: 'TC<"clearfix">lfrt<"bottom"ip>',
				"columns": [{
						data: '0'
					},
					{
						data: '1'
					},
					{
						data: '2',
						render: $.fn.dataTable.render.number( ',', '.', 2 )
					}
				],
				"columnDefs": [
				               {
				            	   className: "currency", 
				            	   "targets": [2] ,
				            	   createdCell: function (td, cellData, rowData, row, col) {
										totalape += cellData * 1;
									} 
				               },
				               { 
				            	   className: "middle-algin",
				            	   "targets": [1],
				            	   createdCell: function (td, cellData, rowData, row, col) {
				            		   totalcase += cellData * 1;
									} 
				               }
				             ],
				"fnDrawCallback": function (oSettings, json) {
					//showLoading(false);
				}
			});
			
			$('#bcu_grid').on( 'draw.dt', function () {
			    var info = tablesummarybcu.page.info();
			    var arr = $('#bcu_grid_info').text().trim().split('-');
			    $('#bcu_grid_info').html(arr[0] + " - (CASE = " + totalcase + ") - (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
			} );
			var arr = $('#bcu_grid_info').text().trim().split('-');
			$('#bcu_grid_info').html(arr[0] +" - (CASE = " + totalcase + ") - (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
		}
	});
}
function GetSummaryBCUCallStatusLa() {
	$.ajax({
		url: urlbcucallstatussummary,
		type: 'get',
		data: { type: 'LA' },
		success: function(result) {
			var totalape = 0;
			var totalcase = 0;
			var dataSet = JSON.parse(result);
			var tablesummarybcula = $('#bcu_la_grid').DataTable();
			tablesummarybcula.destroy();
			tablesummarybcula = $('#bcu_la_grid').DataTable({
				"lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
				data: dataSet,
				dom: 'TC<"clearfix">lfrt<"bottom"ip>',
				"columns": [{
						data: '0'
					},
					{
						data: '1'
					},
					{
						data: '2',
						render: $.fn.dataTable.render.number( ',', '.', 2 )
					}
				],
				"columnDefs": [
				               {
				            	   className: "currency", 
				            	   "targets": [2] ,
				            	   createdCell: function (td, cellData, rowData, row, col) {
										totalape += cellData * 1;
									} 
				               },
				               { 
				            	   className: "middle-algin",
				            	   "targets": [1],
				            	   createdCell: function (td, cellData, rowData, row, col) {
				            		   totalcase += cellData * 1;
									}
				               }
				             ],
				"fnDrawCallback": function (oSettings, json) {
					//showLoading(false);
				}
			});
			$('#bcu_la_grid').on( 'draw.dt', function () {
			    var info = tablesummarybcula.page.info();
			    var arr = $('#bcu_la_grid_info').text().trim().split('-');
			    $('#bcu_la_grid_info').html(arr[0] + " - (CASE = " + totalcase + ") - (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
			} );
			var arr = $('#bcu_la_grid_info').text().trim().split('-');
			$('#bcu_la_grid_info').html(arr[0] +" - (CASE = " + totalcase + ") - (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
		}
	});
}
function GetUncontactablityLa() {
	$.ajax({
		url: urllapseducsummary,
		type: 'get',
		data: { type: 'LA' },
		success: function(result) {
			var totalape = 0;
			var totalcase = 0;
			var totalpercent = 0;
			var dataSet = JSON.parse(result);
			var tableuncontactablityla = $('#uncontact_la_grid').DataTable();
			tableuncontactablityla.destroy();
			tableuncontactablityla = $('#uncontact_la_grid').DataTable({
				"lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
				data: dataSet,
				dom: 'TC<"clearfix">lfrt<"bottom"ip>',
				"columns": [{
						data: '0'
					},
					{
						data: '1'
					},
					{
						data: '2'
					},
					{
						data: '3',
						render: $.fn.dataTable.render.number( ',', '.', 2 )
					}
				],
				"columnDefs": [
				               {
				            	   className: "currency", 
				            	   "targets": [3] ,
				            	   createdCell: function (td, cellData, rowData, row, col) {
										totalape += cellData * 1;
									} 
				               },
				               { 
				            	   className: "middle-algin",
				            	   "targets": [2],
				            	   createdCell: function (td, cellData, rowData, row, col) {
				            		   totalcase += cellData * 1;
									} 
				               },
				               {
				            	   className: "middle-algin",
				            	   "targets": [1],
				            	   createdCell: function (td, cellData, rowData, row, col) {
				            		   totalpercent += cellData * 1;
				            		   $(td).html(cellData * 100);
									}
				               }
				             ],
				"fnDrawCallback": function (oSettings, json) {
					//showLoading(false);
				}
			});
			$('#uncontact_la_grid').on( 'draw.dt', function () {
			    var info = tableuncontactablityla.page.info();
			    var arr = $('#uncontact_la_grid_info').text().trim().split('-');
			    $('#uncontact_la_grid_info').html(arr[0] + "- (% = "+ (totalpercent * 100).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "1,")+") - (CASE = " + totalcase + ") - (APE = $ " + (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ")");
			} );
			var arr = $('#uncontact_la_grid_info').text().trim().split('-');
			$('#uncontact_la_grid_info').html(arr[0] +" - (% =  "+ (totalpercent * 100).toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "1,")+") - (CASE = " + totalcase + ") - (APE = $ " + (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ")");
		}
	});
}
function GetCollection(urlgetcollections, reload, form) {
		
	if (reload === true){
		var branchcodeandname = $(form).find('#clbranchcodeandname').val();
		urlgetcollections = urlgetcollections + '?branchcodeandname=' + branchcodeandname;
		
		var agentcodeandname = $(form).find('#clagentcodeandname').val();
		urlgetcollections = urlgetcollections + '&agentcodeandname=' + agentcodeandname;
		
		var bdmcode = $(form).find('#clbdmcode').val();
		if (bdmcode === null) { bdmcode = "0";}
		urlgetcollections = urlgetcollections + '&bdmcode=' + bdmcode;

		var product = $(form).find('#clproduct').val();
		if (product === null) { product = "0";}
		urlgetcollections = urlgetcollections + '&product=' + product;
		
		var agentstatus = $(form).find('#clagentstatus').val();
		urlgetcollections = urlgetcollections + '&agentstatus=' + agentstatus;
		
		var bcucallstatus = $(form).find('#clbcucallstatus').val();
		urlgetcollections = urlgetcollections + '&bcucallstatus=' + bcucallstatus;
		
		var duedate = $(form).find('#colduedate').val();
		urlgetcollections = urlgetcollections + '&duedate=' + duedate;
		
		var duedateto = $(form).find('#colduedateto').val();
		urlgetcollections = urlgetcollections + '&duedateto=' + duedateto;
		
		var duedateoperator = $(form).find('#colduedateoperator option:selected').text();
		urlgetcollections = urlgetcollections + '&duedateoperator=' + duedateoperator;
		
		var ape = $(form).find('#colape').val();
		urlgetcollections = urlgetcollections + '&ape=' + ape;
		
		var apeto = $(form).find('#colapeto').val();
		urlgetcollections = urlgetcollections + '&apeto=' + apeto;
		
		var apeoperator = $(form).find('#colapeoperator option:selected').text();
		urlgetcollections = urlgetcollections + '&apeoperator=' + apeoperator;
		
		var collectiontable = $('#collection_grid').DataTable();
		collectiontable.ajax.url(urlgetcollections).load();
		
		return ;
	}
	
	var totalprem = 0;
	GetCollectionAgentSummary();
	GetCollectionBranchSummary();
	
	var collectiontable = $('#collection_grid').DataTable({
        processing: true,
        serverSide: true,
        ajax:{
               url: urlgetcollections,
               type: 'get'
        },
        "columns": [
                    {
						data: 'chdrNum'
					},
					{
						data: 'chdrAppnum'
					},
					{
						data: 'poName'
					},
					{
						data: 'gender'
					},
					{
						data: 'clntMobile1'
					},
					{
						data: 'clntMobile2'
					},
					{
						data: 'product'
					},
					{
						data: 'chdrPwithTax'
					},
					{
						data: 'chdrBillfreq'
					},
					{
						data: 'chdrDueDate'
					},
					{
						data: 'chdrGraceperiod'
					},
					{
						data: 'agntNum'
					},
					{
						data: 'agntName'
					},
					{
						data: 'agntSupcode'
					},
					{
						data: 'agntSupname'
					},
					{
						data: 'ccStatus'
					},
					{
						data: 'agntStatus'
					},
					{
						data: 'chdrAge'
					},
					{
						data: 'chdrApe'
					},
					{
						data: 'chdrLapseDate'
					},
					{
						data: 'chdrStatus'
					},
					{
						data: 'isImpactPersistency'
					},
					{
						data: 'lapsedReason'
					},
					{
						data: 'bcuRemark'
					},
					{
						data: 'customerResponse'
					},
					{
						data: 'branchCode'
					},
					{
						data: 'branchName'
					},
					{
						data: 'objKey'
					},
					{
						data: 'objType'
					},
					{
						data: 'ccRemark'
					},
					{
						data: '',
						defaultContent: '<a class="showCollectioniAndLapseDetail" style="cursor:pointer;">' +
											'<span class="label label-danger">'+
												'Detail'+
											'</span>'+
										'</a>'+
										'<a class="AddComment">'+
											'<span class="label label-warning" style="margin-left:5px;">'+
												'<i class="fa fa-plus" aria-hidden="true"></i>'+
											'</span>'+
										'</a>'
					}
        ],
        columnDefs: [
				     {
						targets: -1,
						className: "middle-algin",
						createdCell: function (td, cellData, rowData, row, col) {
							var btnDetail = $(td).children().first();
							var btnComment = $(td).children().last();
							$(btnComment).click(function (e) {
								var tr = $(this).parent().parent();
//								$(tr).addClass('hight-light');
								var objKey = $(this).parent().parent().find('.objKey').text().trim();
								var objType = $(this).parent().parent().find('.objType').text().trim();
								showModalCollectionandLapseCommentDialog.show('Collection Comment',objKey,objType,urlgetcollectionandlapsecomment);
							});
							$(btnDetail).click(function (e) {
								var chdrNum = $(this).parent().parent().find('.chdrNum').text().trim();
								var chdrAppnum = $(this).parent().parent().find('.chdrAppnum').text().trim();
								var poName = $(this).parent().parent().find('.poName').text().trim();
								var gender = $(this).parent().parent().find('.gender').text().trim();
								var clntMobile1 = $(this).parent().parent().find('.clntMobile1').text().trim();
								var clntMobile2 = $(this).parent().parent().find('.clntMobile2').text().trim();
								var chdrPwithTax = $(this).parent().parent().find('.chdrPwithTax').text().trim();
								var chdrBillfreq = $(this).parent().parent().find('.chdrBillfreq').text().trim();
								var chdrDueDate = $(this).parent().parent().find('.chdrDueDate').text().trim();
								var chdrGraceperiod = $(this).parent().parent().find('.chdrGraceperiod').text().trim();
								var agntNum = $(this).parent().parent().find('.agntNum').text().trim();
								var agntName = $(this).parent().parent().find('.agntName').text().trim();
								var agntSupcode = $(this).parent().parent().find('.agntSupcode').text().trim();
								var agntSupname = $(this).parent().parent().find('.agntSupname').text().trim();
								var ccStatus = $(this).parent().parent().find('.ccStatus').text().trim();
								var agntStatus = $(this).parent().parent().find('.agntStatus').text().trim();
								var chdrAge = $(this).parent().parent().find('.chdrAge').text().trim();
								var chdrLapseDate = $(this).parent().parent().find('.chdrLapseDate').text().trim();
								var chdrStatus = $(this).parent().parent().find('.chdrStatus').text().trim();
								var isImpactPersistency = $(this).parent().parent().find('.isImpactPersistency').text().trim();
								var lapsedReason = $(this).parent().parent().find('.lapsedReason').text().trim();
								var bcuRemark = $(this).parent().parent().find('.bcuRemark').text().trim();
								var customerResponse = $(this).parent().parent().find('.customerResponse').text().trim();
								var branchCode = $(this).parent().parent().find('.branchCode').text().trim();
								var branchName = $(this).parent().parent().find('.branchName').text().trim();
								
								var objcollectionandlapsedetail = {
										chdrNum: chdrNum,
										chdrAppnum: chdrAppnum,
										poName: poName,
										gender: gender,
										clntMobile1: clntMobile1,
										clntMobile2: clntMobile2,
										chdrPwithTax: chdrPwithTax,
										chdrBillfreq: chdrBillfreq,
										chdrDueDate: chdrDueDate,
										chdrGraceperiod: chdrGraceperiod,
										agntNum: agntNum,
										agntName: agntName,
										agntSupcode: agntSupcode,
										agntSupname: agntSupname,
										ccStatus: ccStatus,
										agntStatus: agntStatus,
										chdrAge: chdrAge,
										chdrLapseDate: chdrLapseDate,
										chdrStatus: chdrStatus,
										isImpactPersistency: isImpactPersistency,
										lapsedReason: lapsedReason,
										bcuRemark: bcuRemark,
										customerResponse: customerResponse,
										branchCode: branchCode,
										branchName: branchName
								}
								
								showModalCollectionAndLapseDialog.show(objcollectionandlapsedetail,'collection');
							});
						}
					},
					{
						targets: 0,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrNum');
						}
					},
					{
						targets: 1,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrAppnum');
						}
					},
					{
						targets: 2,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('poName');
						}
					},
					{
						targets: 3,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('gender');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 4,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('clntMobile1');
						}
					},
					{
						targets: 5,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('clntMobile2');
						}
					},
					{
						targets: 6,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('product');
						}
					},
					{
						targets: 7,
						className: "currency",
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrPwithTax');
						}
					},
					{
						targets: 8,
						className: "middle-algin",
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrBillfreq');
						}
					},
					{
						targets: 9,
						className: "middle-algin",
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrDueDate');
						}
					},
					{
						targets: 10,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrGraceperiod');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 11,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntNum');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 12,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntName');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 13,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntSupcode');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 14,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntSupname');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 15,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('ccStatus');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 16,
						className: "middle-algin",
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntStatus');
						}
					},
					{
						targets: 17,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrAge');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 18,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrApe');
						}
					},
					{
						targets: 19,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrLapseDate');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 20,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrStatus');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 21,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('isImpactPersistency');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 22,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('lapsedReason');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 23,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('bcuRemark');
						}
					},
					{
						targets: 24,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('customerResponse');
						}
					},
					{
						targets: 25,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('branchCode');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 26,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('branchName');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 27,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('objKey');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 28,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('objType');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 29,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('ccRemark');
						}
					}
				],
				"fnDrawCallback": function (oSettings, json) {
					//showLoading(false);
				}
 });
}

function GetLapsedURL(urlgetlapseds, reload, form)
{
	var branchcodeandname = $(form).find('#labranchcodeandname').val();
	urlgetlapseds = urlgetlapseds + '?branchcodeandname=' + branchcodeandname;
	
	var agentcodeandname = $(form).find('#laagentcodeandname').val();
	urlgetlapseds = urlgetlapseds + '&agentcodeandname=' + agentcodeandname;
	
	var bdmcode = $(form).find('#labdmcode').val();
	if (bdmcode === null) { bdmcode = '0';}
	urlgetlapseds = urlgetlapseds + '&bdmcode=' + bdmcode;

	var product = $(form).find('#laproduct').val();
	if (product === null) { product = '0';}
	urlgetlapseds = urlgetlapseds + '&product=' + product;
	
	var agentstatus = $(form).find('#laagentstatus').val();
	urlgetlapseds = urlgetlapseds + '&agentstatus=' + agentstatus;
	
	var bcucallstatus = $(form).find('#labcucallstatus').val();
	urlgetlapseds = urlgetlapseds + '&bcucallstatus=' + bcucallstatus;
	
	var duedate = $(form).find('#laduedate').val();
	urlgetlapseds = urlgetlapseds + '&duedate=' + duedate;
	
	var duedateto = $(form).find('#laduedateto').val();
	urlgetlapseds = urlgetlapseds + '&duedateto=' + duedateto;
	
	var duedateoperator = $(form).find('#laduedateoperator option:selected').text();
	urlgetlapseds = urlgetlapseds + '&duedateoperator=' + duedateoperator;
	
	var isimpactpersistency = $(form).find('#laisimpactpersistency').val();
	urlgetlapseds = urlgetlapseds + '&isimpactpersistency=' + isimpactpersistency;
	
	var duration = $(form).find('#laduration').val();
	urlgetlapseds = urlgetlapseds + '&duration=' + duration;
	
	var durationto = $(form).find('#ladurationto').val();
	urlgetlapseds = urlgetlapseds + '&durationto=' + durationto;
	
	var durationoperator = $(form).find('#ladurationoperator option:selected').text();
	urlgetlapseds = urlgetlapseds + '&durationoperator=' + durationoperator;
	
	var reason = $(form).find('#lareason').val();
	urlgetlapseds = urlgetlapseds + '&reason=' + reason;
	
	var date = $(form).find('#ladate').val();
	urlgetlapseds = urlgetlapseds + '&date=' + date;
	
	var dateto = $(form).find('#ladateto').val();
	urlgetlapseds = urlgetlapseds + '&dateto=' + dateto;
	
	var dateoperator = $(form).find('#ladateoperator option:selected').text();
	urlgetlapseds = urlgetlapseds + '&dateoperator=' + dateoperator;
	
	var ape = $(form).find('#laape').val();
	urlgetlapseds = urlgetlapseds + '&ape=' + ape;
	
	var apeto = $(form).find('#laapeto').val();
	urlgetlapseds = urlgetlapseds + '&apeto=' + apeto;
	
	var apeoperator = $(form).find('#laapeoperator option:selected').text();
	urlgetlapseds = urlgetlapseds + '&apeoperator=' + apeoperator;
	
	return urlgetlapseds;
}

function GetLapsed(urlgetlapseds, reload, form) {

	if (reload === true){
		urlgetlapseds = GetLapsedURL(urlgetlapseds, reload, form);
		var lapsetable = $('#lapse_grid').DataTable();
		lapsetable.ajax.url(urlgetlapseds).load();
		return ;
	}
	
	
	GetLapseAgentSummary();
	GetLapseBranchSummary();
    var lapsetable = $('#lapse_grid').DataTable({
        processing: true,
        serverSide: true,
        ajax:{
               url: urlgetlapseds,
               type: 'get'
        },
        "columns": [
			{
				data: 'chdrNum'
			},
			{
				data: 'chdrAppnum'
			},
			{
				data: 'poName'
			},
			{
				data: 'gender'
			},
			{
				data: 'clntMobile1'
			},
			{
				data: 'clntMobile2'
			},
			{
				data: 'product'
			},
			{
				data: 'chdrPwithTax'
			},
			{
				data: 'chdrBillfreq'
			},
			{
				data: 'chdrDueDate'
			},
			{
				data: 'chdrGraceperiod'
			},
			{
				data: 'agntNum'
			},
			{
				data: 'agntName'
			},
			{
				data: 'agntSupcode'
			},
			{
				data: 'agntSupname'
			},
			{
				data: 'ccStatus'
			},
			{
				data: 'agntStatus'
			},
			{
				data: 'chdrAge'
			},
			{
				data: 'chdrApe'
			},
			{
				data: 'chdrLapseDate'
			},
			{
				data: 'chdrStatus'
			},
			{
				data: 'isImpactPersistency'
			},
			{
				data: 'lapsedReason'
			},
			{
				data: 'bcuRemark'
			},
			{
				data: 'customerResponse'
			},
			{
				data: 'branchCode'
			},
			{
				data: 'branchName'
			},
			{
				data: 'objKey'
			},
			{
				data: 'objType'
			},
			{
				data: 'ccRemark'
			},
			{
				data: '',
				defaultContent: '<a class="showCollectioniAndLapseDetail" style="cursor:pointer;">' +
									'<span class="label label-danger">Detail</span></a>'+
									'<a class="AddComment">'+
									'<span class="label label-warning" style="margin-left:5px;">'+
										'<i class="fa fa-plus" aria-hidden="true"></i>'+
									'</span>'+
								'</a>'
			},
			{
				data: '',
				defaultContent: '<a id="loadStandingOrder" style="cursor:pointer;">' +
									'<span class="label label-success">'+
										'Standing Order'+
									'</span>'+
								'</a>'+
								'<a id="loadDirectDebit" style="cursor:pointer;">'+
									'<span class="label label-primary" style="margin-left:5px;">'+
										'Direct Debit'+
									'</span>'+
								'</a>'
			}
        ],
        columnDefs: [
				     {
						targets: -2,
						createdCell: function (td, cellData, rowData, row, col) {
							var btnDetail = $(td).children().first();
							var btnComment = $(td).children().last();
							$(btnComment).click(function (e) {
								var objKey = $(this).parent().parent().find('.objKey').text().trim();
								var objType = $(this).parent().parent().find('.objType').text().trim();
								showModalCollectionandLapseCommentDialog.show('Lapsed Comment',objKey,objType,urlgetcollectionandlapsecomment);
							});
							$(btnDetail).click(function (e) {
								var chdrNum = $(this).parent().parent().find('.chdrNum').text().trim();
								var chdrAppnum = $(this).parent().parent().find('.chdrAppnum').text().trim();
								var poName = $(this).parent().parent().find('.poName').text().trim();
								var gender = $(this).parent().parent().find('.gender').text().trim();
								var clntMobile1 = $(this).parent().parent().find('.clntMobile1').text().trim();
								var clntMobile2 = $(this).parent().parent().find('.clntMobile2').text().trim();
								var chdrPwithTax = $(this).parent().parent().find('.chdrPwithTax').text().trim();
								var chdrBillfreq = $(this).parent().parent().find('.chdrBillfreq').text().trim();
								var chdrDueDate = $(this).parent().parent().find('.chdrDueDate').text().trim();
								var chdrGraceperiod = $(this).parent().parent().find('.chdrGraceperiod').text().trim();
								var agntNum = $(this).parent().parent().find('.agntNum').text().trim();
								var agntName = $(this).parent().parent().find('.agntName').text().trim();
								var agntSupcode = $(this).parent().parent().find('.agntSupcode').text().trim();
								var agntSupname = $(this).parent().parent().find('.agntSupname').text().trim();
								var ccStatus = $(this).parent().parent().find('.ccStatus').text().trim();
								var agntStatus = $(this).parent().parent().find('.agntStatus').text().trim();
								var chdrAge = $(this).parent().parent().find('.chdrAge').text().trim();
								var chdrLapseDate = $(this).parent().parent().find('.chdrLapseDate').text().trim();
								var chdrStatus = $(this).parent().parent().find('.chdrStatus').text().trim();
								var isImpactPersistency = $(this).parent().parent().find('.isImpactPersistency').text().trim();
								var lapsedReason = $(this).parent().parent().find('.lapsedReason').text().trim();
								var bcuRemark = $(this).parent().parent().find('.bcuRemark').text().trim();
								var customerResponse = $(this).parent().parent().find('.customerResponse').text().trim();
								var branchCode = $(this).parent().parent().find('.branchCode').text().trim();
								var branchName = $(this).parent().parent().find('.branchName').text().trim();
								
								var objcollectionandlapsedetail = {
										chdrNum: chdrNum,
										chdrAppnum: chdrAppnum,
										poName: poName,
										gender: gender,
										clntMobile1: clntMobile1,
										clntMobile2: clntMobile2,
										chdrPwithTax: chdrPwithTax,
										chdrBillfreq: chdrBillfreq,
										chdrDueDate: chdrDueDate,
										chdrGraceperiod: chdrGraceperiod,
										agntNum: agntNum,
										agntName: agntName,
										agntSupcode: agntSupcode,
										agntSupname: agntSupname,
										ccStatus: ccStatus,
										agntStatus: agntStatus,
										chdrAge: chdrAge,
										chdrLapseDate: chdrLapseDate,
										chdrStatus: chdrStatus,
										isImpactPersistency: isImpactPersistency,
										lapsedReason: lapsedReason,
										bcuRemark: bcuRemark,
										customerResponse: customerResponse,
										branchCode: branchCode,
										branchName: branchName
								}
								
								showModalCollectionAndLapseDialog.show(objcollectionandlapsedetail,'lapsed');
							});
						}
					},
					{
						targets: -1,
						createdCell: function (td, cellData, rowData, row, col) {
							var btnStdOrder = $(td).children().first();
							var btnDDebit = $(td).children().last();
							$(btnStdOrder).click(function (e) {
								var objKey = $(this).parent().parent().find('.objKey').text().trim();
								 e.preventDefault();
					        	 var url = urlReinstatementSOForm+'/'+objKey;
					        	 showPopup(url);
							});
							$(btnDDebit).click(function (e) {
								var objKey = $(this).parent().parent().find('.objKey').text().trim();
								 var url = urlReinstatementDDForm+'/'+objKey;
								 showPopup(url);
							});
						}
					},
					{
						targets: 0,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrNum');
						}
					},
					{
						targets: 1,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrAppnum');
						}
					},
					{
						targets: 2,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('poName');
						}
					},
					{
						targets: 3,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('gender');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 4,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('clntMobile1');
						}
					},
					{
						targets: 5,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('clntMobile2');
						}
					},
					{
						targets: 6,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('product');
						}
					},
					{
						targets: 7,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrPwithTax');
						}
					},
					{
						targets: 8,
						className: "middle-algin",
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrBillfreq');
						}
					},
					{
						targets: 9,
						className: "middle-algin",
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrDueDate');
						}
					},
					{
						targets: 10,
						className: "middle-algin",
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrGraceperiod');
						}
					},
					{
						targets: 11,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntNum');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 12,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntName');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 13,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntSupcode');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 14,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntSupname');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 15,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('ccStatus');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 16,
						className: "middle-algin",
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntStatus');
						}
					},
					{
						targets: 17,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrAge');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 18,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrApe');
						}
					},
					{
						targets: 19,
						className: "middle-algin",
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrLapseDate');
						}
					},
					{
						targets: 20,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrStatus');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 21,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('isImpactPersistency');
						}
					},
					{
						targets: 22,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('lapsedReason');
						}
					},
					{
						targets: 23,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('bcuRemark');
						}
					},
					{
						targets: 24,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('customerResponse');
						}
					},
					{
						targets: 25,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('branchCode');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 26,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('branchName');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 27,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('objKey');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 28,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('objType');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 29,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('ccRemark');
						}
					}
				],
				"fnDrawCallback": function (oSettings, json) {
					//showLoading(false);
				}
    	});
	    var tablapsed = $('#lapse_grid').dataTable();
	    if(ipc != null) {
	    	tablapsed.fnFilter(ipc);
	    } else {
	    	tablapsed.fnFilter('');
	    }
	    
}

function GetCollectionAndLapse() {
	//get Collection and Lapse
	$('.right_col').loading({
		message: 'Loading Please wait...'
	});
	GetCollectionAgentSummary();
	GetCollectionBranchSummary();
	GetLapseAgentSummary();
	GetLapseBranchSummary();
	$.ajax({
		url: urlcollectionandlapse,
		type: 'get',
		success: function(result) {
			var dataSet = JSON.parse(result);
			var dataSetCollection = dataSet[0];
			var dataSetLapse = dataSet[1];
			var tablecollection = $('#collection_grid').DataTable();
			tablecollection.destroy();
			tablecollection = $('#collection_grid').DataTable({
				data: dataSetCollection,
				dom: 'TC<"clearfix">lfrt<"bottom"ip>',
				"columns": [
				    {
						data: 'chdrNum'
					},
					{
						data: 'chdrAppnum'
					},
					{
						data: 'poName'
					},
					{
						data: 'gender'
					},
					{
						data: 'clntMobile1'
					},
					{
						data: 'clntMobile2'
					},
					{
						data: 'chdrPwithTax'
					},
					{
						data: 'chdrBillfreq'
					},
					{
						data: 'chdrDueDate'
					},
					{
						data: 'chdrGraceperiod'
					},
					{
						data: 'agntNum'
					},
					{
						data: 'agntName'
					},
					{
						data: 'agntSupcode'
					},
					{
						data: 'agntSupname'
					},
					{
						data: 'ccStatus'
					},
					{
						data: 'agntStatus'
					},
					{
						data: 'chdrAge'
					},
					{
						data: 'chdrLapseDate'
					},
					{
						data: 'chdrStatus'
					},
					{
						data: 'isImpactPersistency'
					},
					{
						data: 'lapsedReason'
					},
					{
						data: 'bcuRemark'
					},
					{
						data: 'customerResponse'
					},
					{
						data: 'branchCode'
					},
					{
						data: 'branchName'
					},
					{
						data: 'objKey'
					},
					{
						data: 'objType'
					},
					{
						data: '',
						defaultContent: '<a class="showCollectioniAndLapseDetail" style="cursor:pointer;">' +
											'<span class="label label-danger">'+
												'Detail'+
											'</span>'+
										'</a>'+
										'<a class="AddComment">'+
											'<span class="label label-warning" style="margin-left:5px;">'+
												'<i class="fa fa-plus" aria-hidden="true"></i>'+
											'</span>'+
										'</a>'
					}
				],
				columnDefs: [
				     {
						targets: -1,
						createdCell: function (td, cellData, rowData, row, col) {
							var btnDetail = $(td).children().first();
							var btnComment = $(td).children().last();
							$(btnComment).click(function (e) {
								var objKey = $(this).parent().parent().find('.objKey').text().trim();
								var objType = $(this).parent().parent().find('.objType').text().trim();
								showModalCollectionandLapseCommentDialog.show('Collection Comment',objKey,objType,urlgetcollectionandlapsecomment);
							});
							$(btnDetail).click(function (e) {
								var chdrNum = $(this).parent().parent().find('.chdrNum').text().trim();
								var chdrAppnum = $(this).parent().parent().find('.chdrAppnum').text().trim();
								var poName = $(this).parent().parent().find('.poName').text().trim();
								var gender = $(this).parent().parent().find('.gender').text().trim();
								var clntMobile1 = $(this).parent().parent().find('.clntMobile1').text().trim();
								var clntMobile2 = $(this).parent().parent().find('.clntMobile2').text().trim();
								var chdrPwithTax = $(this).parent().parent().find('.chdrPwithTax').text().trim();
								var chdrBillfreq = $(this).parent().parent().find('.chdrBillfreq').text().trim();
								var chdrDueDate = $(this).parent().parent().find('.chdrDueDate').text().trim();
								var chdrGraceperiod = $(this).parent().parent().find('.chdrGraceperiod').text().trim();
								var agntNum = $(this).parent().parent().find('.agntNum').text().trim();
								var agntName = $(this).parent().parent().find('.agntName').text().trim();
								var agntSupcode = $(this).parent().parent().find('.agntSupcode').text().trim();
								var agntSupname = $(this).parent().parent().find('.agntSupname').text().trim();
								var ccStatus = $(this).parent().parent().find('.ccStatus').text().trim();
								var agntStatus = $(this).parent().parent().find('.agntStatus').text().trim();
								var chdrAge = $(this).parent().parent().find('.chdrAge').text().trim();
								var chdrLapseDate = $(this).parent().parent().find('.chdrLapseDate').text().trim();
								var chdrStatus = $(this).parent().parent().find('.chdrStatus').text().trim();
								var isImpactPersistency = $(this).parent().parent().find('.isImpactPersistency').text().trim();
								var lapsedReason = $(this).parent().parent().find('.lapsedReason').text().trim();
								var bcuRemark = $(this).parent().parent().find('.bcuRemark').text().trim();
								var customerResponse = $(this).parent().parent().find('.customerResponse').text().trim();
								var branchCode = $(this).parent().parent().find('.branchCode').text().trim();
								var branchName = $(this).parent().parent().find('.branchName').text().trim();
								
								var objcollectionandlapsedetail = {
										chdrNum: chdrNum,
										chdrAppnum: chdrAppnum,
										poName: poName,
										gender: gender,
										clntMobile1: clntMobile1,
										clntMobile2: clntMobile2,
										chdrPwithTax: chdrPwithTax,
										chdrBillfreq: chdrBillfreq,
										chdrDueDate: chdrDueDate,
										chdrGraceperiod: chdrGraceperiod,
										agntNum: agntNum,
										agntName: agntName,
										agntSupcode: agntSupcode,
										agntSupname: agntSupname,
										ccStatus: ccStatus,
										agntStatus: agntStatus,
										chdrAge: chdrAge,
										chdrLapseDate: chdrLapseDate,
										chdrStatus: chdrStatus,
										isImpactPersistency: isImpactPersistency,
										lapsedReason: lapsedReason,
										bcuRemark: bcuRemark,
										customerResponse: customerResponse,
										branchCode: branchCode,
										branchName: branchName
								}
								
								showModalCollectionAndLapseDialog.show(objcollectionandlapsedetail,'collection');
							});
						}
					},
					{
						targets: 0,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrNum');
						}
					},
					{
						targets: 1,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrAppnum');
						}
					},
					{
						targets: 2,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('poName');
						}
					},
					{
						targets: 3,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('gender');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 4,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('clntMobile1');
						}
					},
					{
						targets: 5,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('clntMobile2');
						}
					},
					{
						targets: 6,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrPwithTax');
						}
					},
					{
						targets: 7,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrBillfreq');
						}
					},
					{
						targets: 8,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrDueDate');
						}
					},
					{
						targets: 9,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrGraceperiod');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 10,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntNum');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 11,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntName');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 12,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntSupcode');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 13,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntSupname');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 14,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('ccStatus');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 15,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntStatus');
						}
					},
					{
						targets: 16,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrAge');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 17,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrLapseDate');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 18,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrStatus');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 19,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('isImpactPersistency');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 20,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('lapsedReason');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 21,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('bcuRemark');
						}
					},
					{
						targets: 22,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('customerResponse');
						}
					},
					{
						targets: 23,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('branchCode');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 24,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('branchName');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 25,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('objKey');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 26,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('objType');
							$(td).css('display', 'none');
						}
					}
				],
				"fnDrawCallback": function (oSettings, json) {
					//showLoading(false);
				}
			});
			
			var tablelapse = $('#lapse_grid').DataTable();
			tablelapse.destroy();
			tablelapse = $('#lapse_grid').DataTable({
				data: dataSetLapse,
				dom: 'TC<"clearfix">lfrt<"bottom"ip>',
				"columns": [
				    {
						data: 'chdrNum'
					},
					{
						data: 'chdrAppnum'
					},
					{
						data: 'poName'
					},
					{
						data: 'gender'
					},
					{
						data: 'clntMobile1'
					},
					{
						data: 'clntMobile2'
					},
					{
						data: 'chdrPwithTax'
					},
					{
						data: 'chdrBillfreq'
					},
					{
						data: 'chdrDueDate'
					},
					{
						data: 'chdrGraceperiod'
					},
					{
						data: 'agntNum'
					},
					{
						data: 'agntName'
					},
					{
						data: 'agntSupcode'
					},
					{
						data: 'agntSupname'
					},
					{
						data: 'ccStatus'
					},
					{
						data: 'agntStatus'
					},
					{
						data: 'chdrAge'
					},
					{
						data: 'chdrLapseDate'
					},
					{
						data: 'chdrStatus'
					},
					{
						data: 'isImpactPersistency'
					},
					{
						data: 'lapsedReason'
					},
					{
						data: 'bcuRemark'
					},
					{
						data: 'customerResponse'
					},
					{
						data: 'branchCode'
					},
					{
						data: 'branchName'
					},
					{
						data: 'objKey'
					},
					{
						data: 'objType'
					},
					{
						data: '',
						defaultContent: '<a class="showCollectioniAndLapseDetail" style="cursor:pointer;">' +
											'<span class="label label-danger">Detail</span></a>'+
											'<a class="AddComment">'+
											'<span class="label label-warning" style="margin-left:5px;">'+
												'<i class="fa fa-plus" aria-hidden="true"></i>'+
											'</span>'+
										'</a>'
					}
				],
				columnDefs: [
				     {
						targets: -1,
						createdCell: function (td, cellData, rowData, row, col) {
							var btnDetail = $(td).children().first();
							var btnComment = $(td).children().last();
							$(btnComment).click(function (e) {
								var objKey = $(this).parent().parent().find('.objKey').text().trim();
								var objType = $(this).parent().parent().find('.objType').text().trim();
								showModalCollectionandLapseCommentDialog.show('Lapsed Comment',objKey,objType,urlgetcollectionandlapsecomment);
							});
							$(btnDetail).click(function (e) {
								var chdrNum = $(this).parent().parent().find('.chdrNum').text().trim();
								var chdrAppnum = $(this).parent().parent().find('.chdrAppnum').text().trim();
								var poName = $(this).parent().parent().find('.poName').text().trim();
								var gender = $(this).parent().parent().find('.gender').text().trim();
								var clntMobile1 = $(this).parent().parent().find('.clntMobile1').text().trim();
								var clntMobile2 = $(this).parent().parent().find('.clntMobile2').text().trim();
								var chdrPwithTax = $(this).parent().parent().find('.chdrPwithTax').text().trim();
								var chdrBillfreq = $(this).parent().parent().find('.chdrBillfreq').text().trim();
								var chdrDueDate = $(this).parent().parent().find('.chdrDueDate').text().trim();
								var chdrGraceperiod = $(this).parent().parent().find('.chdrGraceperiod').text().trim();
								var agntNum = $(this).parent().parent().find('.agntNum').text().trim();
								var agntName = $(this).parent().parent().find('.agntName').text().trim();
								var agntSupcode = $(this).parent().parent().find('.agntSupcode').text().trim();
								var agntSupname = $(this).parent().parent().find('.agntSupname').text().trim();
								var ccStatus = $(this).parent().parent().find('.ccStatus').text().trim();
								var agntStatus = $(this).parent().parent().find('.agntStatus').text().trim();
								var chdrAge = $(this).parent().parent().find('.chdrAge').text().trim();
								var chdrLapseDate = $(this).parent().parent().find('.chdrLapseDate').text().trim();
								var chdrStatus = $(this).parent().parent().find('.chdrStatus').text().trim();
								var isImpactPersistency = $(this).parent().parent().find('.isImpactPersistency').text().trim();
								var lapsedReason = $(this).parent().parent().find('.lapsedReason').text().trim();
								var bcuRemark = $(this).parent().parent().find('.bcuRemark').text().trim();
								var customerResponse = $(this).parent().parent().find('.customerResponse').text().trim();
								var branchCode = $(this).parent().parent().find('.branchCode').text().trim();
								var branchName = $(this).parent().parent().find('.branchName').text().trim();
								
								var objcollectionandlapsedetail = {
										chdrNum: chdrNum,
										chdrAppnum: chdrAppnum,
										poName: poName,
										gender: gender,
										clntMobile1: clntMobile1,
										clntMobile2: clntMobile2,
										chdrPwithTax: chdrPwithTax,
										chdrBillfreq: chdrBillfreq,
										chdrDueDate: chdrDueDate,
										chdrGraceperiod: chdrGraceperiod,
										agntNum: agntNum,
										agntName: agntName,
										agntSupcode: agntSupcode,
										agntSupname: agntSupname,
										ccStatus: ccStatus,
										agntStatus: agntStatus,
										chdrAge: chdrAge,
										chdrLapseDate: chdrLapseDate,
										chdrStatus: chdrStatus,
										isImpactPersistency: isImpactPersistency,
										lapsedReason: lapsedReason,
										bcuRemark: bcuRemark,
										customerResponse: customerResponse,
										branchCode: branchCode,
										branchName: branchName
								}
								
								showModalCollectionAndLapseDialog.show(objcollectionandlapsedetail,'lapsed');
							});
						}
					},
					{
						targets: 0,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrNum');
						}
					},
					{
						targets: 1,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrAppnum');
						}
					},
					{
						targets: 2,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('poName');
						}
					},
					{
						targets: 3,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('gender');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 4,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('clntMobile1');
						}
					},
					{
						targets: 5,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('clntMobile2');
						}
					},
					{
						targets: 6,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrPwithTax');
						}
					},
					{
						targets: 7,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrBillfreq');
						}
					},
					{
						targets: 8,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrDueDate');
						}
					},
					{
						targets: 9,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrGraceperiod');
						}
					},
					{
						targets: 10,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntNum');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 11,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntName');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 12,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntSupcode');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 13,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntSupname');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 14,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('ccStatus');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 15,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('agntStatus');
						}
					},
					{
						targets: 16,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrAge');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 17,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrLapseDate');
						}
					},
					{
						targets: 18,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('chdrStatus');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 19,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('isImpactPersistency');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 20,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('lapsedReason');
						}
					},
					{
						targets: 21,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('bcuRemark');
						}
					},
					{
						targets: 22,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('customerResponse');
						}
					},
					{
						targets: 23,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('branchCode');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 24,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('branchName');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 25,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('objKey');
							$(td).css('display', 'none');
						}
					},
					{
						targets: 26,
						createdCell: function (td, cellData, rowData, row, col) {
							$(td).addClass('objType');
							$(td).css('display', 'none');
						}
					}
				],
				"fnDrawCallback": function (oSettings, json) {
					//showLoading(false);
				}
			});
			$('.right_col').loading('stop');
		}
	});
	//end get Collection And Lapse
}

function CheckPendingShort(value,tag,param) {
	if(!value == ""){
		var name = tag+value;
		var arrobj = {name: name, param: [param]};
		listpendings.push(arrobj);
	}
}
function CheckPending(value, valueto, operator, tag, name1,param,param1,param2) {
	if(!value == ""){
		console.log(operator);
		if(operator == "between"){
			var name = tag + value +" to "+ valueto;
			var arrobj = {name: name, param: [param,param1,param2]};
			listpendings.push(arrobj);
		} else{
			var name = name1 + operator +" "+ value;
			var arrobj = {name: name, param: [param,param1]};
			listpendings.push(arrobj);
		}
	}
}
function CheckPendingShortLa(value,tag,param) {
	if(!value == ""){
		var name = tag+value;
		var arrobj = {name: name, param: [param]};
		listpendingsla.push(arrobj);
	}
}
function CheckPendingLa(value, valueto, operator, tag, name1,param,param1,param2) {
	if(!value == ""){
		if(operator == "between"){
			var name = tag + value +" to "+ valueto;
			var arrobj = {name: name, param: [param,param1,param2]};
			listpendingsla.push(arrobj);
		} else{
			var name = name1 + operator +" "+ value;
			var arrobj = {name: name, param: [param,param1]};
			listpendingsla.push(arrobj);
		}
	}
}
function onAddTag(name,param,key) {
	$('.lbl-advance-search').append('<label id="'+key+'" class="label label-danger label-tag"><span onclick="myFunction()">' + name + '</span> &nbsp;&nbsp; <a style="font-size:16px;" href="#" onclick="onRemoveTag(\''+ key + '\',\'' +param+'\')">x</a></label>');
}

function onRemoveTag(key, param) {
	var arrparam = param.split(',');
	$('#'+key).remove();
	$.each( arrparam, function( key, value) {
		$('#'+value).val("");
	});
	
	$( "#btnreadvancesearch" ).trigger( "click" );
}
function onAddTagLa(name,param,key) {
	$('.lbl-advance-searchla').append('<label id="'+key+'" class="label label-danger label-tag"><span onclick="myFunctionLa()">' + name + '</span> &nbsp;&nbsp; <a style="font-size:16px;" href="#" onclick="onRemoveTagLa(\''+ key + '\',\'' +param+'\')">x</a></label>');
}

function onRemoveTagLa(key, param) {
	var arrparam = param.split(',');
	$('#'+key + 'la').remove();
	$.each( arrparam, function( key, value) {
		$('#'+value).val("");
	});
	
	$( "#btnreadvancesearchla" ).trigger( "click" );
}
function myFunction() {
	$( "#btnAdvanceSearchRenewalPremiumCollection" ).trigger( "click" );
}
function myFunctionLa() {
	$( "#btnAdvanceSearchLapsed" ).trigger( "click" );
}

// Drupdown Collection Branch Code clbranchcodeandname by Collection
function getDropdownCollectionBranchAndCode() {
	$('#clbranchcodeandname').append('<option value="0"> -- select the branch code and name here -- </option>');
	for(i = 0; i < branchCodeAndName.length;  i++ ) {
		$('#clbranchcodeandname').append('<option value="'+branchCodeAndName[i].split('-')[0]+'">'+branchCodeAndName[i]+'</option>');
	}
	
	$('#labranchcodeandname').append('<option value="0"> -- select the branch code and name here -- </option>');
	for(i = 0; i < branchCodeAndNamela.length;  i++ ) {
		$('#labranchcodeandname').append('<option value="'+branchCodeAndNamela[i].split('-')[0]+'">'+branchCodeAndNamela[i]+'</option>');
	}
}
// Drupdown for product both Renewal and Lapse
function getDropdownCollectionProductCode() {
	$('#clproduct').append('<option value="0"> -- select the product here -- </option>');
	for(i = 0; i < productList.length;  i++ ) {
		$('#clproduct').append('<option value="'+productList[i]+'">'+productList[i]+'</option>');
	}
	
	$('#laproduct').append('<option value="0"> -- select the product here -- </option>');
	for(i = 0; i < productListLA.length;  i++ ) {
		$('#laproduct').append('<option value="'+productListLA[i]+'">'+productListLA[i]+'</option>');
	}
}
function getDropdownCollectionAgentCodeAndName() {
	$('#clagentcodeandname').append('<option value="0"> -- select agent code and name here -- </option>');
	for(i = 0; i < agentCodeAndName.length;  i++ ) {
		$('#clagentcodeandname').append('<option value="'+agentCodeAndName[i].split('-')[0]+'">'+agentCodeAndName[i]+'</option>');
	}
	
	$('#laagentcodeandname').append('<option value="0"> -- select agent code and name here -- </option>');
	for(i = 0; i < agentCodeAndNamela.length;  i++ ) {
		$('#laagentcodeandname').append('<option value="'+agentCodeAndNamela[i].split('-')[0]+'">'+agentCodeAndNamela[i]+'</option>');
	}
}
function getDropdownCollectionBdmCode() {
	$.ajax({
		url: urlgetbdmcode,
		type: 'get',
		success: function(result) {
			if(result) {
				result = JSON.parse(result);
				$('#clbdmcode').append('<option value="0"> -- select the BDM code here -- </option>');
				for(i = 0; i < result.length;  i++ ) {
					$('#clbdmcode').append('<option value="'+result[i][0]+'">'+result[i][0]+'-' +result[i][1]+'</option>');
				}
				
				$('#labdmcode').append('<option value="0"> -- select the BDM code here -- </option>');
				for(i = 0; i < result.length;  i++ ) {
					$('#labdmcode').append('<option value="'+result[i][0]+'">'+result[i][0]+'-'+result[i][1]+'</option>');
				}
			}
		}
	});
}
function getDropdownCollectionAgentStatus() {
	$('#clagentstatus').append('<option value="0"> -- select the policy transfer here -- </option>');
	for(i = 0; i < agentStatus.length;  i++ ) {
		$('#clagentstatus').append('<option value="'+agentStatus[i]+'">'+agentStatus[i]+'</option>');
	}
	
	$('#laagentstatus').append('<option value="0"> -- select the agent status here -- </option>');
	for(i = 0; i < agentStatusla.length;  i++ ) {
		$('#laagentstatus').append('<option value="'+agentStatusla[i]+'">'+agentStatusla[i]+'</option>');
	}
}
function getDropdownCollectionBcuCallStatus() {
	$('#clbcucallstatus').append('<option value="0"> -- select the BCU call statuss here -- </option>');
	for(i = 0; i < bcuCallStatus.length;  i++ ) {
		$('#clbcucallstatus').append('<option value="'+bcuCallStatus[i]+'">'+bcuCallStatus[i]+'</option>');
	}
	
	$('#labcucallstatus').append('<option value="0"> -- select the BCU call statuss here -- </option>');
	for(i = 0; i < bcuCallStatusla.length;  i++ ) {
		$('#labcucallstatus').append('<option value="'+bcuCallStatusla[i]+'">'+bcuCallStatusla[i]+'</option>');
	}
}
function getDropdownLapseIsImpactPersistency() {
	$('#laisimpactpersistency').append('<option value="0"> -- select the is impact persistency here -- </option>');
	for(i = 0; i < isImpactPersistencyla.length;  i++ ) {
		$('#laisimpactpersistency').append('<option value="'+isImpactPersistencyla[i]+'">'+isImpactPersistencyla[i]+'</option>');
	}
}
function getDropdownLapseLapsedReason() {
	$('#lareason').append('<option value="0"> -- select the lapsed reason here -- </option>');
	for(i = 0; i < lapsedReasonla.length;  i++ ) {
		$('#lareason').append('<option value="'+lapsedReasonla[i]+'">'+lapsedReasonla[i]+'</option>');
	}
}

function showPopup(url)
{
	var winHeight = "innerHeight" in window ? window.innerHeight
			: document.documentElement.offsetHeight;

	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i
			.test(navigator.userAgent)) {
		var popup = window.open(url, "_blank");
		if (!popup
				|| popup.closed
				|| typeof popup.closed == 'undefined') {
			//POPUP BLOCKED
			alert('Please enable popup blocker in setting!');
		}
	} else {
		var popup = window.open(url, 'mywindow'
				+ (new Date()).getTime(),
				'width=865,height=' + winHeight
						+ ',scrollbars=yes');
		if (!popup
				|| popup.closed
				|| typeof popup.closed == 'undefined') {
			//POPUP BLOCKED
			alert('Please enable popup blocker in broswer setting!');
		}
	}
}

