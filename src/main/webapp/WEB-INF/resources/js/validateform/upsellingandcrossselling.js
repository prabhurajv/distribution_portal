$(document).ready(function() {

	GetUpsellingAndCrossselling(urlgetupsellingandcrossselling, 'init', null);
});

function GetUpsellingAndCrossselling(urlgetupsellingandcrossselling, action,
		form) {

	if (action == 'reload') {
		var upsellingandcrosssellingtable = $('#upselling_crossselling_grid')
				.DataTable();
		upsellingandcrosssellingtable.ajax.url(urlgetupsellingandcrossselling)
				.load();
		return;
	}

	var upsellingandcrosssellingtable = $('#upselling_crossselling_grid')
			.DataTable(
					{
						processing : true,
						serverSide : true,
						ajax : {
							url : urlgetupsellingandcrossselling,
							type : 'get'
						},
						"columns" : [ {
							data : 'objKey'
						}, {
							data : 'agentNum'
						}, {
							data : 'agentName'
						}, {
							data : 'poNumber'
						}, {
							data : 'cusName'
						}, {
							data : 'cusSex'
						}, {
							data : 'cusMobile1'
						}, {
							data : 'cusMobile2'
						}, {
							data : 'cusMstatus'
						}, {
							data : 'cusNoChild'
						}, {
							data : 'cusOccupation'
						}, {
							data : 'cusSalaryMonthly'
						}, {
							data : 'issDate'
						}, {
							data : 'bdmCode'
						}, {
							data : 'bdmName'
						}, {
							data : 'branchCode'
						}, {
							data : 'ape'
						}, {
							data : 'sumAssure'
						}, {
							data : 'uab'
						}, {
							data : 'poTransferStatus'
						}, {
							data : 'approachStatus'
						}, {
							data : 'chdrStatus'
						}, {
							data : 'poCusage'
						}, {
							data : 'incomeRank'
						}, {
							data : 'approachStatusId'
						} ],
						columnDefs : [
								{
									targets : -1,
									createdCell : function(td, cellData,
											rowData, row, col) {

										if (rowData.approachStatusId == "0") // new
										{
											$(td)
													.html(
															'<button class="btn btn-warning btn-xs btnEdit" id="btnApproach"> Approach </button>');
											var btnEdit = $(td).children()
													.first();
											$(btnEdit)
													.click(
															function(e) {

																var btn = $(this);
																var id = $
																		.trim(btn
																				.parent()
																				.parent()
																				.children()
																				.first()
																				.text());

																url = urlreferral
																		+ '/'
																		+ id;
																// url =
																// urlreferral;
																window.location.href = url;
															});

										}else // approached 
										{
											$(td).html('');
										}
									}
								},
								{
									targets : 0,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('objKey');
										$(td).css('display', 'none');
									}
								},
								// {
								// targets: -1,
								// createdCell: function(td, cellData, rowData,
								// row, col){
								// // if(cellData == "New") {
								// // $(td).html('<div style="width: 100%;
								// display:inline-block;" class="label
								// label-success">New</div>');
								// // } else if(cellData == "Approached") {
								// // $(td).html('<div style="width: 100%;
								// display:inline-block;" class="label
								// label-warning">Approached</div>');
								// // }
								//					            	
								//					            		
								// }
								// },
								{
									targets : 1,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('agentNum');
									}
								},
								{
									targets : 2,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('agentName');
									}
								},
								{
									targets : 3,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('poNumber');
									}
								},
								{
									targets : 4,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('cusName');
									}
								},
								{
									targets : 5,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('cusSex');
									}
								},
								{
									targets : 6,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('cusMobile1');
									}
								},
								{
									targets : 7,
									className : "currency",
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('cusMobile2');
									}
								},
								{
									targets : 8,
									className : "middle-algin",
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('cusMstatus');
									}
								},
								{
									targets : 9,
									className : "middle-algin",
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('cusNoChild');
									}
								},
								{
									targets : 10,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('cusOccupation');
									}
								},
								{
									targets : 11,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('cusSalaryMonthly');
									}

								},
								{
									targets : 12,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('issDate');
									}

								},
								{
									targets : 13,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('bdmCode');
									}

								},
								{
									targets : 14,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('bdmName');
									}

								},
								{
									targets : 15,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('branchCode');
									}

								},
								{
									targets : 16,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('ape');
									}

								},
								{
									targets : 17,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('sumAssure');
									}

								},
								{
									targets : 18,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('uab');
									}

								},
								{
									targets : 19,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('poTransferStatus');
									}

								},
								{
									targets : 20,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('approachStatus');
									}

								},
								{
									targets : 21,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('chdrStatus');
										$(td).css('display', 'none');
									}

								},
								{
									targets : 22,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('poCusage');
										$(td).css('display', 'none');
									}

								},
								{
									targets : 23,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('incomeRank');
										$(td).css('display', 'none');
									}

								},
								{
									targets : 24,
									createdCell : function(td, cellData,
											rowData, row, col) {
										$(td).addClass('rId');
										$(td).css('display', 'none');
									}

								} ],
						"fnDrawCallback" : function(oSettings, json) {
							// showLoading(false);
						}
					});
}
