$(document).ready(function () {
	GetSubmission();
});
function GetSubmissionAgentSummary() {
	$.ajax({
		url: urlsubmissionsummary,
		type: 'get',
		data: { summaryby: 'agent' },
		success: function(result) {
			if(result !== '') {
				var totalape = 0;
				var dataSet = JSON.parse(result);
				var tablesummarybranch = $('#agent_grid').DataTable();
				tablesummarybranch.destroy();
				tablesummarybranch = $('#agent_grid').DataTable({
					data: dataSet,
					dom: 'TC<"clearfix">lfrt<"bottom"ip>',
					"columns": [{
							data: '0'
						},
						{
							data: '1'
						},
						{
							data: '2'
						},
						{
							data: '3'
						}
					],
					"columnDefs": [
					               { 
					            	   className: "currency",
					            	   "targets": [3] ,
					            	   createdCell: function (td, cellData, rowData, row, col) {
											totalape += cellData * 1;
										} 
					               },
					               { className: "middle-algin", "targets": [2] }
					             ],
					"fnDrawCallback": function (oSettings, json) {
						//showLoading(false);
					}
				});
				$('#agent_grid').on( 'draw.dt', function () {
				    var info = tablesummarybranch.page.info();
				    var arr = $('#agent_grid_info').text().trim().split('-');
				    $('#agent_grid_info').html(arr[0] + "- (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
				} );
				var arr = $('#agent_grid_info').text().trim().split('-');
				$('#agent_grid_info').html(arr[0] +" - (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
			}
		}
	});
}
function GetSubmissionSummary() {
	$.ajax({
		url: urlsubmissionsummary,
		type: 'get',
		data: { summaryby: 'branch' },
		success: function(result) {
			if(result !== '') {
				var totalape = 0;
				var dataSet = JSON.parse(result);
				var tablesummarybranch = $('#branch_grid').DataTable();
				tablesummarybranch.destroy();
				tablesummarybranch = $('#branch_grid').DataTable({
					data: dataSet,
					dom: 'TC<"clearfix">lfrt<"bottom"ip>',
					"columns": [{
							data: '0'
						},
						{
							data: '1'
						},
						{
							data: '2'
						},
						{
							data: '3'
						}
					],
					"columnDefs": [
					               {
					            	   "targets": 0,
					            	   	createdCell: function (td, cellData, rowData, row, col) {
					            	   		var btnDetail = $(td);
					            	   		$(td).css('cursor', 'pointer');
					            	   		$(btnDetail).click(function (e) {
					            	   			var status = $(this).text().trim();
					            	   		  	var tablapsed = $('#submission_grid').dataTable();
						            	  	    if(status != null) {
						            	  	    	tablapsed.fnFilter(status);
						            	  	    } else {
						            	  	    	tablapsed.fnFilter('');
						            	  	    }
						            	  	  	$('html, body').animate({
						            	        	scrollTop: $("#move-filter").offset().top
						            	      	}, 500);
					            	   		});
										} 
					               },
					               { 
					            	   className: "currency",
					            	   "targets": [3] ,
					            	   createdCell: function (td, cellData, rowData, row, col) {
											totalape += cellData * 1;
										} 
					               },
					               { className: "middle-algin", "targets": [2] }
					             ],
					"fnDrawCallback": function (oSettings, json) {
						//showLoading(false);
					}
				});
				$('#branch_grid').on( 'draw.dt', function () {
				    var info = tablesummarybranch.page.info();
				    var arr = $('#branch_grid_info').text().trim().split('-');
				    $('#branch_grid_info').html(arr[0] + "- (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
				} );
				var arr = $('#branch_grid_info').text().trim().split('-');
				$('#branch_grid_info').html(arr[0] +" - (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");	
			}
		}
	});
}
function GetSubmission() {
	//Function Get Submission
	$('.right_col').loading({
		message: 'Loading Please wait...'
	});
	
	GetSubmissionSummary();
	GetSubmissionAgentSummary();
	
	$.ajax({
		url: urlsubmission,
		type: 'get',
		success: function(result) {
			var totalape = 0;
			var dataSet = JSON.parse(result);
			var tablecollection = $('#submission_grid').DataTable();
			tablecollection.destroy();
			tablecollection = $('#submission_grid').DataTable({
				data: dataSet,
				dom: 'TC<"clearfix">lfrt<"bottom"ip>',
				"columns": [{
						data: 'chdrNum'
					},
					{
						data: 'chdrAppNum'
					},
					{
						data: 'poName'
					},
					{
						data: 'subDate'
					},
					{
						data: 'chdrKeyDate'
					},
					{
						data: 'chdrIssDate'
					},
					{
						data: 'chdrStatCode'
					},
					{
						data: 'chdrNtuDate'
					},
					{
						data: 'chdrWdDate'
					},
					{
						data: 'chdrPoDate'
					},
					{
						data: 'chdrCfDate'
					},
					{
						data: 'chdrApe'
					},
					{
						data: 'agntNum'
					},
					{
						data: 'agntName'
					},
					{
						data: 'weekth'
					},
					{
						data: 'branchCode'
					},
					{
						data: 'branchName'
					}
				],
				columnDefs: [
							{
								targets: 0,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('chdrNum');
								}
							},
							{
								targets: 1,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('chdrAppnum');
								}
							},
							{
								targets: 2,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('poName');
								}
							},
							{
								targets: 3,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('subDate');
								}
							},
							{
								targets: 4,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('chdrKeyDate');
								}
							},
							{
								targets: 5,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('chdrIssDate');
									$(td).css('display', 'none');
								}
							},
							{
								targets: 6,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('chdrStatCode');
								}
							},
							{
								targets: 7,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('chdrNtuDate');
									$(td).css('display', 'none');
								}
							},
							{
								targets: 8,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('chdrWdDate');
									$(td).css('display', 'none');
								}
							},
							{
								targets: 9,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('chdrPoDate');
									$(td).css('display', 'none');
								}
							},
							{
								targets: 10,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('chdrCfDate');
									$(td).css('display', 'none');
								}
							},
							{
								targets: 11,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('chdrApe');
									totalape += cellData * 1;
								}
							},
							{
								targets: 12,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('agntNum');
									$(td).css('display', 'none');
								}
							},
							{
								targets: 13,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('agntName');
								}
							},
							{
								targets: 14,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('weekth');
								}
							},
							{
								targets: 15,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('branchCode');
									$(td).css('display', 'none');
								}
							},
							{
								targets: 16,
								createdCell: function (td, cellData, rowData, row, col) {
									$(td).addClass('branchName');
								}
							}
						],
				"fnDrawCallback": function (oSettings, json) {
					//showLoading(false);
				}
			});
			$('#submission_grid').on( 'draw.dt', function () {
			    var info = tablecollection.page.info();
			    var arr = $('#submission_grid_info').text().trim().split('-');
			    $('#submission_grid_info').html(arr[0] + "- (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
			} );
			var arr = $('#submission_grid_info').text().trim().split('-');
			$('#submission_grid_info').html(arr[0] +" - (APE = $ "+ (totalape * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")+")");
			$('.right_col').loading('stop');
		}
	});
	//End Functioin Get Submission
}