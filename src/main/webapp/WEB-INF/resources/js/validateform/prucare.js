var occla1 = $('#occla1').val();
var occpo = $('#occpo').val();
var occla2 = $('#occla2').val();
$(function() {
	if (typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, '');
		}
	}

	$(document)
			.ready(
					function() {
						$('#coPremiumTerm').attr('disabled', 'disabled');
						$('#coPremiumTerm').after(
								'<input type="hidden" name="coPremiumTerm" value="'
										+ $('#coPremiumTerm').val() + '" />');

						var tmpCoPaymentMode = $('#coPaymentMode').val();
						if (tmpCoPaymentMode == '19') {
							$("#coPaymentType option[value='18']").prop(
									"disabled", true);
							$('#coPaymentType')
									.parent()
									.parent()
									.parent()
									.parent()
									.before(
											'<p style="margin-left: 20px; color: orange;">*Note: This product cannot be sold in <b>Cash</b> with <b>Monthly mode!</b></p>');
						}
						var tmpCoPaymentType = $('#coPaymentType').val();
						if (tmpCoPaymentType == '18') {
							$("#coPaymentMode option[value='19'").prop(
									"disabled", true);
							$('#coPaymentType')
									.parent()
									.parent()
									.parent()
									.parent()
									.before(
											'<p style="margin-left: 20px; color: orange;">*Note: This product cannot be sold in <b>Cash</b> with <b>Monthly mode!</b></p>');
						}

						$('#coPaymentType')
								.change(
										function() {
											var tmpCoPaymentType = $(
													'#coPaymentType').val();
											if (tmpCoPaymentType == '18') {
												$(
														"#coPaymentMode option[value='19']")
														.prop("disabled", true);
												$('#coPaymentType')
														.parent()
														.parent()
														.parent()
														.parent()
														.before(
																'<p style="margin-left: 20px; color: orange;">*Note: This product cannot be sold in <b>Cash</b> with <b>Monthly mode!</b></p>');
											} else if (tmpCoPaymentType == '17') {
												$(
														"#coPaymentMode option[value='19']")
														.prop("disabled", false);
												$('#coPaymentType').parent()
														.parent().parent()
														.parent().parent()
														.children('p').remove();
											}
										});

						$('#coPaymentMode')
								.change(
										function() {
											var tmpCoPaymentMode = $(
													'#coPaymentMode').val();
											if (tmpCoPaymentMode == '19') {
												$(
														"#coPaymentType option[value='18']")
														.prop("disabled", true);
												$('#coPaymentType')
														.parent()
														.parent()
														.parent()
														.parent()
														.before(
																'<p style="margin-left: 20px; color: orange;">*Note: This product cannot be sold in <b>Cash</b> with <b>Monthly mode!</b></p>');
											} else if (tmpCoPaymentMode == '20'
													|| tmpCoPaymentMode == '21') {
												$(
														"#coPaymentType option[value='18']")
														.prop("disabled", false);
												$('#coPaymentType').parent()
														.parent().parent()
														.parent().parent()
														.children('p').remove();
											}

										});
						
										
						renderLA1DateOfBirth(null);
						renderLA2DateOfBirth(null);
						
						if(hasRSR1)
						{
							var l1Rsr1YN = $("#coL1Rsr1 option:selected").text();
							if (l1Rsr1YN != null && l1Rsr1YN != "Yes") {
								$('#l1Rsr1').attr('disabled', 'disabled');
								document.getElementById("l1Rsr1").value = 0;
							}
						}
						if(hasRSR1)
						{
							var l2Rsr1YN = $("#coL2Rsr1 option:selected").text();
							if (l2Rsr1YN != null && l2Rsr1YN != "Yes") {
								$('#l2Rsr1').attr('disabled', 'disabled');
								document.getElementById("l2Rsr1").value = 0;
							}
						}
						if(hasRTR1)
						{
							var l2Rtr1YN = $("#coL2Rtr1 option:selected").text();
							if (l2Rtr1YN != null && l2Rtr1YN != "Yes") {
								$('#l2Rtr1').attr('disabled', 'disabled');
								document.getElementById("l2Rtr1").value = 0;
							}
						}
						
					});

	$('#basicPlanSa').change(function() {
		if ($('#basicPlanSa').val() != "") {
			var fixed = parseFloat($('#basicPlanSa').val()).toFixed();
			fixed = Math.round(fixed / 10) * 10;
			$('#basicPlanSa').val(fixed);
		}
	});

	// Custom Function to Detect Policy Term with Premium term
	$('#coPolicyTerm').change(
			function() {

				validatePolicytermWithRider(true);

				var indexpre = $("select[name='coPolicyTerm'] option:selected")
						.index();
				var indexpol = indexpre + 1;
				$('#coPremiumTerm :nth-child(' + indexpol + ')').prop(
						'selected', true);
				$('#coPremiumTerm').after(
						'<input type="hidden" name="coPremiumTerm" value="'
								+ $('#coPremiumTerm').val() + '" />');
				if(hasRSR1)
				{
					document.getElementById("l1Rsr1Term").value = $(
							"#coPremiumTerm option:selected").text();
				}
				if(hasRTR1)
				{
					document.getElementById("l2Rtr1Term").value = $(
							"#coPremiumTerm option:selected").text();
				}
				if(hasRSR1)
				{
					document.getElementById("l2Rsr1Term").value = $(
					"#coPremiumTerm option:selected").text();
				}

				renderLA1DateOfBirth(true);
				renderLA2DateOfBirth(true);
			})

	// End SA auto calulation
	var currentDate = new Date();
	$('#commDate').datepicker({
		useCurrent : true,
		pickTime : false,
		dateFormat : "dd/mm/yy"
	});
	if (!$('#commDate').val()) {
		$('#commDate').datepicker("setDate", currentDate);
	}
	// $('#commDate').datetimepicker("setDate",currentDate);
	$('#la1dateOfBirth').datepicker({
		useCurrent : false,
		pickTime : false,
		dateFormat : "dd/mm/yy",
		viewMode : 'years'
	});
	$('#syndate').datepicker({
		autoclose : true,
		todayHighlight : true,
		format : "dd/mm/yyyy"
	});
	$("#syndate").datepicker("setDate", currentDate);
	$('#la2dateOfBirth').datepicker({
		useCurrent : false,
		pickTime : false,
		dateFormat : "dd/mm/yy",
		viewMode : 'years'
	});
	var LAPO = $("#coLa1relationship option:selected").text();
	if (LAPO.trim() === "PO = LA") {
		document.getElementById("poName").readOnly = true;
		document.getElementById("droccupationpo").hidden = true;
	} else {
		document.getElementById("poName").readOnly = false;
		document.getElementById("droccupationpo").hidden = false;
	}

	var coPremiumTerm = document.getElementById("coPremiumTerm");
	// coPremiumTerm.remove(coPremiumTerm.length-1);
	if(hasRSR1)
	{
		document.getElementById("l1Rsr1Term").value = $(
				"#coPremiumTerm option:selected").text();
	}
	if(hasRTR1)
	{
		document.getElementById("l2Rtr1Term").value = $(
			"#coPremiumTerm option:selected").text();
	}
	if(hasRSR1)
	{
		document.getElementById("l2Rsr1Term").value = $(
			"#coPremiumTerm option:selected").text();
	}

});

$('#la1dateOfBirth').change(function() {
	// 1. If age at entry of main LA is larger than 50, automatically choose
	// “No” for Family Income Benefit, and cannot choose “Yes” and cannot fill
	// in the SA at all.
	renderLA1DateOfBirth(true);
});

function renderLA1DateOfBirth(isTrigger)
{
	var policyTerm = $("#coPolicyTerm option:selected").text();
	var age = CalculateAge(document.getElementById("la1dateOfBirth").value);
	if (parseInt(age) + parseInt(policyTerm) > 60) {
		$('#coL1Rsr1').prop("disabled", true);
		$("#coL1Rsr1").val("16");
		if(isTrigger)
			$('#coL1Rsr1').trigger( "change" );
	} else if(isTrigger) {
		$('#coL1Rsr1').prop("disabled", false);
		// $('#coL1Rsr1').trigger( "change" ); // no trigger case la1 in term
	}
}


$('#la2dateOfBirth').change(function() {
	// 1. If age at entry of main LA is larger than 50, automatically choose
	// “No” for Family Income Benefit, and cannot choose “Yes” and cannot fill
	// in the SA at all.
	renderLA2DateOfBirth(true);
});

function renderLA2DateOfBirth(isTrigger)
{
	if(!hasRSR1 && !hasRTR1) return;
	var policyTerm = $("#coPolicyTerm option:selected").text();
	var age = CalculateAge(document.getElementById("la2dateOfBirth").value);
	if (parseInt(age) + parseInt(policyTerm) > 60) {
		$("#coL2Rsr1").val("16");
		$('#coL2Rsr1').prop("disabled", true);

		$("#coL2Rtr1").val("16");
		$('#coL2Rtr1').prop("disabled", true);

		if(isTrigger)
		{
			$('#coL2Rsr1').trigger( "change" );
			$('#coL2Rtr1').trigger( "change" );
		}
		
	} else if(isTrigger) {
		$('#coL2Rsr1').prop("disabled", false);
		$('#coL2Rtr1').prop("disabled", false);
		// $('#coL2Rsr1').trigger( "change" );  // no trigger case la2 in term
		// $('#coL2Rtr1').trigger( "change" );
	}
}

$('#coPremiumTerm').change(
		function() {
			if(hasRSR1)
			{
				document.getElementById("l1Rsr1Term").value = $(
						"#coPremiumTerm option:selected").text();
			}

			if(hasRTR1)
			{
				document.getElementById("l2Rtr1Term").value = $(
					"#coPremiumTerm option:selected").text();
			}
			if(hasRSR1)
			{
				document.getElementById("l2Rsr1Term").value = $(
					"#coPremiumTerm option:selected").text();
			}
		});

function validatePolicytermWithRider(selectedPolicyTerm) {
	var policyTerm = $("select[name='coPolicyTerm'] option:selected").text();
	l1Rsr1YN = $("#coL1Rsr1 option:selected").text();
	l2Rtr1YN = $("#coL2Rtr1 option:selected").text();
	l2Rsr1YN = $("#coL2Rsr1 option:selected").text();

	if (selectedPolicyTerm) {
		Err.innerHTML = "";
	}

	if ((l1Rsr1YN == 'Yes' || l2Rsr1YN == 'Yes' || l2Rtr1YN == 'Yes')
			&& policyTerm != '10' && policyTerm != '12' && policyTerm != '15') {
		document.getElementById("coPolicyTerm").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		Err.innerHTML = Err.innerHTML
				+ "<br/><br/>Please Input Value Again!<br/> "
				+ " For all riders, the applicable Policy Term is only 10, 12, or 15.";

		return false;
	} else {
		
		document.getElementById("coPolicyTerm").style.background = "white";
		return true;
	}
}

$('#coL1Rsr1')
		.change(
				function() {
					if(!hasRSR1) return;

					$('#l1Rsr1').prop("disabled", false);

					var l1Rtr1YN = "";
					var SA = 0;
					var la1name = ""
					var la1dateOfBirth = "";
					l1Rtr1YN = $("#coL1Rsr1 option:selected").text();
					SA = document.getElementById("basicPlanSa").value;
					la1name = document.getElementById("la1name").value;
					la1dateOfBirth = document.getElementById("la1dateOfBirth").value;
					l1Rsr1YN = $("#coL1Rsr1 option:selected").text();
					if (l1Rsr1YN === "Yes") {
						// if(SA>=2000) {
						document.getElementById("l1Rsr1").value = (document
								.getElementById("basicPlanSa").value * 25) / 100;
						// document.getElementById("l1Rsr1").value =0;
						// }
						// else {
						if (la1name === "") {
							document.getElementById("la1name").style.background = "yellow";
							$('html, body').animate({
								scrollTop : 0
							}, 'slow');
							document.getElementById("coL1Rsr1").selectedIndex = "Yes";
							document.getElementById("l2Rsr1").value = 0;
							Err.innerHTML = "Input the mandatory field -LA1Name";
						} else {
							document.getElementById("la1name").style.background = "white";
						}
						if (la1dateOfBirth === "") {
							document.getElementById("la1dateOfBirth").style.background = "yellow";
							$('html, body').animate({
								scrollTop : 0
							}, 'slow');
							document.getElementById("coL1Rsr1").selectedIndex = "Yes";
							document.getElementById("l2Rsr1").value = 0;
							Err.innerHTML = Err.innerHTML
									+ "<br/><br/>Please Input Value Again!<br/>For Policy Term "
									+ $('#coPolicyTerm option:selected').text()
									+ ":<br/>Min Age: 18<br/>Max Age: "
									+ (60 - ($('#coPolicyTerm option:selected')
											.text() * 1));
						} else {
							document.getElementById("la1dateOfBirth").style.background = "white";
						}

						validatePolicytermWithRider(false);
						// document.getElementById("basicPlanSa").style.background
						// = "yellow";
						// Err.innerHTML =Err.innerHTML+"<br/>Input the
						// mandatory field BasicPlan_SA >=2000";
						// }
					} else {

						$('#l1Rsr1').attr('disabled', 'disabled');
						document.getElementById("l1Rsr1").value = 0;
					}
				});

$('#coL2Rtr1')
		.change(
				function() {
					if(!hasRTR1) return;
					var l2Rtr1YN = "";
					var SA = 0;
					var la2name = ""
					l2Rtr1YN = $("#coL2Rtr1 option:selected").text();
					SA = document.getElementById("basicPlanSa").value;
					la2name = document.getElementById("la2name").value;
					la2dateOfBirth = document.getElementById("la2dateOfBirth").value;
					if (l2Rtr1YN === "Yes") {
						$('#l2Rtr1').prop("disabled", false);

						document.getElementById("l2Rtr1").value = 2000;
						if (la2name === "" || la2dateOfBirth === ""
								|| SA === "" || occla2 == '0' || occla2 == '') {
							if (la2name === "") {
								document.getElementById("la2name").style.background = "yellow";
								$('html, body').animate({
									scrollTop : 0
								}, 'fast');
								document.getElementById("coL2Rtr1").selectedIndex = "No";
								document.getElementById("l2Rtr1").value = 0;
								Err.innerHTML = "Input the mandatory field -LA2Name";
							} else {
								document.getElementById("la2name").style.background = "white";
							}
							if (la2dateOfBirth === "") {
								document.getElementById("la2dateOfBirth").style.background = "yellow";
								$('html, body').animate({
									scrollTop : 0
								}, 'fast');
								document.getElementById("coL2Rtr1").selectedIndex = "No";
								document.getElementById("l2Rtr1").value = 0;
								Err.innerHTML = Err.innerHTML
										+ "<br/><br/>Please Input Value Again!<br/>For Policy Term "
										+ $('#coPolicyTerm option:selected')
												.text()
										+ ":<br/>Min Age: 18<br/>Max Age: "
										+ (60 - ($(
												'#coPolicyTerm option:selected')
												.text() * 1));
							} else {
								document.getElementById("la2dateOfBirth").style.background = "white";
							}
							if (occla2 == '0' || occla2 == '') {
								document.getElementById("occla2").style.background = "yellow";
								Err.innerHTML = Err.innerHTML
										+ "<br/>Please select Occupation of LA2";
								document.getElementById("coL2Rtr1").selectedIndex = "No";
								document.getElementById("l2Rtr1").value = 0;
							} else {
								document.getElementById("occla2").style.background = "white";
							}
							if (SA > 0) {
								document.getElementById("basicPlanSa").style.background = "#eee";
							} else {
								document.getElementById("basicPlanSa").style.background = "yellow";
								Err.innerHTML = Err.innerHTML
										+ "<br/>Input the mandatory field BasicPlan_SA>0";
								document.getElementById("coL2Rtr1").selectedIndex = "No";
								document.getElementById("l2Rtr1").value = 0;
							}
							if (SA == "") {
								document.getElementById("basicPlanSa").style.background = "yellow";
								Err.innerHTML = Err.innerHTML
										+ "<br/>Input the mandatory field BasicPlan_SA";
								document.getElementById("coL2Rtr1").selectedIndex = "No";
								document.getElementById("l2Rtr1").value = 0;
							}
						}
						validatePolicytermWithRider(false);

					} else {
						$('#l2Rtr1').attr('disabled', 'disabled');

						document.getElementById("l2Rtr1").value = 0;
						document.getElementById("basicPlanSa").style.background = "#eee";
						document.getElementById("la2dateOfBirth").style.background = "white";
						document.getElementById("la2name").style.background = "white";
						Err.innerHTML = "";
					}
				});
$('#coL2Rsr1')
		.change(
				function() {
					if(!hasRSR1) return;
					var l2Rtr1YN = "";
					var SA = 0;
					var la2name = ""
					var la2dateOfBirth = "";
					l2Rtr1YN = $("#coL2Rsr1 option:selected").text();
					SA = document.getElementById("basicPlanSa").value;
					la2name = document.getElementById("la2name").value;
					la2dateOfBirth = document.getElementById("la2dateOfBirth").value;
					l2Rsr1YN = $("#coL2Rsr1 option:selected").text();
					if (l2Rsr1YN === "Yes") {
						$('#l2Rsr1').prop("disabled", false);
						// if(SA>=2000) {
						document.getElementById("l2Rsr1").value = (document
								.getElementById("basicPlanSa").value * 25) / 100;
						// document.getElementById("l2Rsr1").value =0;
						// }
						// else {
						if (la2name === "") {
							document.getElementById("la2name").style.background = "yellow";
							$('html, body').animate({
								scrollTop : 0
							}, 'slow');
							document.getElementById("coL2Rsr1").selectedIndex = "No";
							document.getElementById("l2Rsr1").value = 0;
							Err.innerHTML = "Input the mandatory field -LA2Name";
						} else {
							document.getElementById("la2name").style.background = "white";
						}
						if (la2dateOfBirth === "") {
							document.getElementById("la2dateOfBirth").style.background = "yellow";
							$('html, body').animate({
								scrollTop : 0
							}, 'slow');
							document.getElementById("coL2Rsr1").selectedIndex = "No";
							document.getElementById("l2Rsr1").value = 0;
							Err.innerHTML = Err.innerHTML
									+ "<br/><br/>Please Input Value Again!<br/>For Policy Term "
									+ $('#coPolicyTerm option:selected').text()
									+ ":<br/>Min Age: 18<br/>Max Age: "
									+ (60 - ($('#coPolicyTerm option:selected')
											.text() * 1));
						} else {
							document.getElementById("la2dateOfBirth").style.background = "white";
						}
						if (occla2 == '0' || occla2 == '') {
							document.getElementById("occla2").style.background = "yellow";
							Err.innerHTML = Err.innerHTML
									+ "<br/>Please select Occupation of LA2";
							document.getElementById("coL2Rsr1").selectedIndex = "No";
							document.getElementById("l2Rsr1").value = 0;
						} else {
							document.getElementById("occla2").style.background = "white";
						}
						// document.getElementById("basicPlanSa").style.background
						// = "yellow";
						// Err.innerHTML =Err.innerHTML+"<br/>Input the
						// mandatory field BasicPlan_SA >=2000";
						// document.getElementById("coL2Rsr1").selectedIndex =
						// "No";
						// }

						validatePolicytermWithRider(false);
					} else {
						$('#l2Rsr1').attr('disabled', 'disabled');
						document.getElementById("l2Rsr1").value = 0;
					}
				});

$('#coLa1relationship').change(function() {
	var LAPO = $("#coLa1relationship option:selected").text();

	if (LAPO.trim() === "PO = LA") {
		document.getElementById("poName").readOnly = true;
		document.getElementById("poName").value = $('#la1name').val();
		document.getElementById("droccupationpo").hidden = true;
		$('#occpo').val('0');
	} else {
		document.getElementById("poName").readOnly = false;
		document.getElementById("poName").value = "";
		document.getElementById("droccupationpo").hidden = false;
	}
});

$('#la1name').change(function() {
	if ($("#coLa1relationship option:selected").text() === "PO = LA") {
		$("#poName").val($('#la1name').val());
	}
});

$("#la2name,#la1name").keypress(function(event) {
	var ew = event.which;
	if (ew == 32 || ew == 8) {
		return true;
	} else if (ew >= 65 && ew <= 90) {
		return true;
	} else if (ew >= 97 && ew <= 122) {
		return true;
	} else {
		return false;
	}
});

$("#la2name").on('input', function(event) {
	if ($(this).val() == '') {
		$('#occla2').val('0');
	}
});

function CalculateAge(dob) {

	var comDate = document.getElementById("commDate").value;
	var today = comDate;
	var comDateArray = today.split("/");
	var newComDate = comDateArray[2] + '/' + comDateArray[1] + '/'
			+ comDateArray[0];
	var ComDateTo = new Date(newComDate);
	ComDateTo.setDate(ComDateTo.getDate() + 1);
	
	var date = dob;
	var datearray = date.split("/");
	var birthDate = new Date(datearray[2], datearray[1] * 1 - 1, datearray[0]);
	age = Math.floor((ComDateTo.getTime() - birthDate.getTime())
			/ (365.25 * 24 * 60 * 60 * 1000));

	return age;
}

function checkSubmit() {
	return validate();
}

function validate() {
	var clName = "";
	var clName2 = "";
	var SA = "";
	var DOB1 = "";
	var DOB2 = "";
	Err.innerHTML = "";
	var l1Rsr1 = "";
	var l2Rtr1 = "";
	var l2Rsr1 = "";
	var l1Rsr1YN = "";
	var l2Rtr1YN = "";
	var l2Rsr1YN = "";
	var LAPO = "";
	var poName = "";
	var comDate = "";
	var policyTerm = "";
	var premuimTerm = "";

	clName = document.getElementById("la1name").value;

	if(hasRSR1 || hasRTR1)
		clName2 = document.getElementById("la2name").value;
	
	SA = document.getElementById("basicPlanSa").value;
	DOB1 = document.getElementById("la1dateOfBirth").value;
	
	if(hasRSR1 || hasRTR1)
		DOB2 = document.getElementById("la2dateOfBirth").value;
	if(hasRSR1)
		l1Rsr1 = document.getElementById("l1Rsr1").value;
	if(hasRTR1)
		l2Rtr1 = document.getElementById("l2Rtr1").value;
	if(hasRSR1)
		l2Rsr1 = document.getElementById("l2Rsr1").value;
	l1Rsr1YN = $("#coL1Rsr1 option:selected").text();
	l2Rtr1YN = $("#coL2Rtr1 option:selected").text();
	l2Rsr1YN = $("#coL2Rsr1 option:selected").text();
	LAPO = $("#coLa1relationship option:selected").text();
	policyTerm = $("#coPolicyTerm option:selected").text();
	premuimTerm = $("#coPremiumTerm option:selected").text();
	poName = document.getElementById("poName").value;
	comDate = document.getElementById("commDate").value;

	/* Calculate Age 1 */
	var Age1 = CalculateAge(DOB1);
	var Age2 = CalculateAge(DOB2);

	if (!validatePolicytermWithRider(true)) {
		return false;
	}

	if (clName2 === "" && DOB2 === "") {
		$("#la2dateOfBirth").datepicker("setDate", "01/01/1900");
	}
	if (hasRTR1 && l2Rtr1YN === "No") {
		document.getElementById("l2Rtr1").value = 0;
	}
	if (LAPO.trim() === "PO = LA") {
		document.getElementById("poName").value = clName;
	}
	// if (policyTerm !== premuimTerm) {
	// Err.innerHTML="";
	// Err.innerHTML=Err.innerHTML+"<br/><b>Premuim Term</b> and <b>Policy
	// Term</b> must be same value!";
	// document.getElementById("coPremiumTerm").style.background = "yellow";
	// document.getElementById("coPolicyTerm").style.background = "yellow";
	// $('html, body').animate({ scrollTop: 0 }, 'slow');
	// return false;
	// }
	if (parseInt(premuimTerm) == 5) {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Premuim Term can select only 10, 12 and 15 Term!";
		document.getElementById("coPremiumTerm").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	}
	if (LAPO.trim() != "PO = LA" && poName === "") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Input the mandatory <b>Policy Owner</b>";
		document.getElementById("poName").style.background = "yellow";
		document.getElementById("la1dateOfBirth").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	}
	if (LAPO.trim() != "PO = LA" && (occpo == '0' || occpo == '')) {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please select Occupation of PO</b>";
		document.getElementById("occpo").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	}
	if (clName2 != "" && DOB2 != "") {
		if (Age2 < 18 || Age2 > 60) {
			Err.innerHTML = "<br/><br/>Please Input Value Again!<br/>For Policy Term "
					+ $('#coPolicyTerm option:selected').text()
					+ ":<br/>Min Age: 18<br/>Max Age: "
					+ (70 - ($('#coPolicyTerm option:selected').text() * 1));
			document.getElementById("la2dateOfBirth").style.background = "yellow";
			document.getElementById("basicPlanSa").style.background = "white";
			document.getElementById("la2name").style.background = "white";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false;
		} else if (parseInt(Age2) + parseInt(policyTerm) > 70) {
			Err.innerHTML = "";
			Err.innerHTML = "<br/><br/>Please Input Value Again!<br/>For Policy Term "
					+ $('#coPolicyTerm option:selected').text()
					+ ":<br/>Min Age: 18<br/>Max Age: "
					+ (70 - ($('#coPolicyTerm option:selected').text() * 1));
			document.getElementById("la2dateOfBirth").style.background = "yellow";
			document.getElementById("la1dateOfBirth").style.background = "white";
			document.getElementById("basicPlanSa").style.background = "white";
			document.getElementById("la1name").style.background = "white";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false;
		}
	}
	if (clName === "") {
		Err.innerHTML = "";
		Err.innerHTML = "<br/>Input the mandatory field -LA1Name";
		document.getElementById("la1name").style.background = "yellow";
		document.getElementById("basicPlanSa").style.background = "white";
		document.getElementById("la1dateOfBirth").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	}

	if (DOB1 === "") {
		Err.innerHTML = "";
		Err.innerHTML = "<br/><br/>Please Input Value Again!<br/>For Policy Term "
				+ $('#coPolicyTerm option:selected').text()
				+ ":<br/>Min Age: 18<br/>Max Age: "
				+ (70 - ($('#coPolicyTerm option:selected').text() * 1));
		document.getElementById("la1dateOfBirth").style.background = "yellow";
		document.getElementById("basicPlanSa").style.background = "white";
		document.getElementById("la1name").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	}
	if (comDate === "") {
		Err.innerHTML = "";
		Err.innerHTML = "<br/>Input the mandatory Policy commencement date";
		document.getElementById("commDate").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	}

	if (DOB1 != "") {
		if (parseInt(Age1) < parseInt(18) || parseInt(Age1) > parseInt(60)) {
			Err.innerHTML = "";
			Err.innerHTML = "<br/><br/>Please Input Value Again!<br/>For Policy Term "
					+ $('#coPolicyTerm option:selected').text()
					+ ":<br/>Min Age: 18<br/>Max Age: "
					+ (70 - ($('#coPolicyTerm option:selected').text() * 1));
			document.getElementById("la1dateOfBirth").style.background = "yellow";
			
			if(document.getElementById("la2dateOfBirth"))
				document.getElementById("la2dateOfBirth").style.background = "white";
			
			document.getElementById("basicPlanSa").style.background = "white";
			document.getElementById("la1name").style.background = "white";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false;
		} 
		 if (parseInt(Age1) + parseInt(policyTerm) > 70) {
			Err.innerHTML = "";
			Err.innerHTML = "<br/><br/>Please Input Value Again!<br/>For Policy Term "
					+ $('#coPolicyTerm option:selected').text()
					+ ":<br/>Min Age: 18<br/>Max Age: "
					+ (70 - ($('#coPolicyTerm option:selected').text() * 1));
			document.getElementById("la1dateOfBirth").style.background = "yellow";

			if(document.getElementById("la2dateOfBirth"))
				document.getElementById("la2dateOfBirth").style.background = "white";
			
			document.getElementById("basicPlanSa").style.background = "white";
			document.getElementById("la1name").style.background = "white";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false;
		}
	} 

	 if (occla1 == '0' || occla1 == '') {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML + "<br/>Please select Occupation of LA1";
		document.getElementById("occla1").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} 
	 if (SA === "") {
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please input SA again! <br/>Basic plan’s SA ≥ US$20,000";
		document.getElementById("basicPlanSa").style.background = "yellow";
		document.getElementById("la1name").style.background = "white";
		document.getElementById("la1dateOfBirth").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} 
	 if (isUnlimitedSA == 'false' && SA < 20000) {

		Err.innerHTML = Err.innerHTML
				+ "<br/>Please input SA again! <br/>Basic plan’s SA ≥ US$20,000";
		document.getElementById("basicPlanSa").style.background = "yellow";
		document.getElementById("la1name").style.background = "white";
		document.getElementById("la1dateOfBirth").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} 
	 if (isUnlimitedSA == 'true' && SA <= 0) {
		Err.innerHTML = Err.innerHTML
				+ "<br/>Input the mandatory field <b>BasicPlan_SA > 0</b>";
		document.getElementById("basicPlanSa").style.background = "yellow";
		document.getElementById("la1name").style.background = "white";
		document.getElementById("la1dateOfBirth").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} 

	if(l1Rsr1YN == "Yes")
	{
		document.getElementById("l1Rsr1").style.background = "white";
		if((parseInt(l1Rsr1) <= parseInt(0) || l1Rsr1 == "") && isUnlimitedRider == 'false') {
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please input Sum Assured again!";
			document.getElementById("l1Rsr1").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false
		}

		if((parseInt((SA * 0.5)) < parseInt(l1Rsr1)))
		{
			Err.innerHTML = Err.innerHTML
			+ "<br/>Please input Sum Assured again!<br/>SA <=50% Basic Sum Assured.";
			document.getElementById("l1Rsr1").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false;
		}
		if(l1Rsr1 % 1 !== 0)
		{
			Err.innerHTML = Err.innerHTML
			+ "<br/>Please input Sum Assured again!<br/>Sum Assured cannot contain decimal number.";
			document.getElementById("l1Rsr1").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false;
		}
		if (parseInt(Age1) < parseInt(18) || parseInt(Age1) > parseInt(60)) {
			Err.innerHTML = "";
			Err.innerHTML = "<br/><br/>Please Input Value Again!<br/>For Policy Term "
					+ $('#coPolicyTerm option:selected').text()
					+ ":<br/>Min Age: 18<br/>Max Age: "
					+ (70 - ($('#coPolicyTerm option:selected').text() * 1));
			document.getElementById("la1dateOfBirth").style.background = "yellow";
			document.getElementById("la2dateOfBirth").style.background = "white";
			document.getElementById("basicPlanSa").style.background = "white";
			document.getElementById("la1name").style.background = "white";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false;
		}
		 if (parseInt(Age1) + parseInt(policyTerm) > 60) {
			Err.innerHTML = "";
			Err.innerHTML = "<br/><br/>Please Input Value Again!<br/>For Policy Term "
					+ $('#coPolicyTerm option:selected').text()
					+ ":<br/>Min Age: 18<br/>Max Age: "
					+ (70 - ($('#coPolicyTerm option:selected').text() * 1));
			document.getElementById("la1dateOfBirth").style.background = "yellow";
			if(document.getElementById("la2dateOfBirth"))
				document.getElementById("la2dateOfBirth").style.background = "white";
			document.getElementById("basicPlanSa").style.background = "white";
			document.getElementById("la1name").style.background = "white";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false;
		}
	}

	 if (l2Rtr1YN === "Yes")
	 {	 	 
		document.getElementById("l2Rtr1").style.background = "white";
		if((parseInt(l2Rtr1) < parseInt(2000)
				|| parseInt(l2Rtr1) > parseInt(SA) || l2Rtr1 == "") 
				&& isUnlimitedRider == 'false') {
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please input Sum Assured again!<Br/>Min SA = 2000$ US<br/>Max SA = 100% Basic Sum Assured";
			document.getElementById("l2Rtr1").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false;
		} 
		if((parseInt(l2Rtr1) <= parseInt(0) || l2Rtr1 == "") && isUnlimitedRider == 'true') {
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please input Sum Assured again!<Br/>Min SA = 2000$ US<br/>Max SA = 100% Basic Sum Assured";
			document.getElementById("l2Rtr1").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false
		}
	}
	/*
	 * else if((l2Rtr1YN==="Yes" && parseInt(l2Rtr1)>parseInt(SA))) {
	 * Err.innerHTML=Err.innerHTML+"<br/>Please input Sum Assured again!<Br/>Min
	 * SA = 2000$ US<br/>Max SA = 100% Basic Sum Assured";
	 * document.getElementById("l2Rtr1").style.background = "yellow"; $('html,
	 * body').animate({ scrollTop: 0 }, 'slow'); return false; }
	 */
	if (l2Rsr1YN === "Yes")
	{
		document.getElementById("l2Rsr1").style.background = "white";
		if((parseInt(l2Rsr1) <= parseInt(0) || l2Rsr1 == "") && isUnlimitedRider == 'false') {
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please input Sum Assured again!";
			document.getElementById("l2Rsr1").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false
		}

		if (parseInt((SA * 0.5)) < parseInt(l2Rsr1)) {
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please input Sum Assured again!<br/>SA <=50% Basic Sum Assured.";
			document.getElementById("l2Rsr1").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false;
		} 
		 if (l1Rsr1 % 1 !== 0) {
			Err.innerHTML = Err.innerHTML
					+ "<br/>Please input Sum Assured again!<br/>Sum Assured cannot contain decimal number.";
			document.getElementById("l1Rsr1").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false;
		}
	}
	// else if(l1Rtr2YN==="Yes" && l1Rtr2=="")
	// {
	// Err.innerHTML=Err.innerHTML+"<br/>PruSaver ride value required!";
	// document.getElementById("l1Rtr2").style.background = "yellow";
	// $('html, body').animate({ scrollTop: 0 }, 'slow');
	// return false;
	// }
	
	if (clName2 != "" && DOB2 === "") {
		var today = new Date();

		Err.innerHTML = "";
		Err.innerHTML = "<br/><br/>Please Input Value Again!<br/>For Policy Term "
				+ $('#coPolicyTerm option:selected').text()
				+ ":<br/>Min Age: 18<br/>Max Age: "
				+ (60 - ($('#coPolicyTerm option:selected').text() * 1));
		document.getElementById("la2dateOfBirth").style.background = "yellow";
		document.getElementById("basicPlanSa").style.background = "white";
		document.getElementById("la2name").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} 
	 if (clName2 != '' && DOB2 != '' && (occla2 == '0' || occla2 == '')) {
			Err.innerHTML = "";
			Err.innerHTML = "<br/>Please select Occupation of LA2";
			document.getElementById("occla2").style.background = "yellow";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false;
	} 

	 if (l2Rsr1YN === "Yes" && (l2Rsr1 % 1 !== 0)) {
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please input Sum Assured again!<br/>Sum Assured cannot contain decimal number.";
		document.getElementById("l2Rsr1").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} 
	 if (l2Rtr1YN === "Yes" && (l2Rtr1 % 1 !== 0)) {
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please input Sum Assured again!<br/>Sum Assured cannot contain decimal number.";
		document.getElementById("l2Rtr1").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} 
	 if (l2Rtr1YN === "Yes" || l2Rsr1YN === "Yes") {
	
		if (clName2 === "") {
			Err.innerHTML = "";
			Err.innerHTML = "<br/>Input the mandatory field -LA2Name";
			document.getElementById("la2name").style.background = "yellow";
			document.getElementById("basicPlanSa").style.background = "white";
			document.getElementById("la2dateOfBirth").style.background = "white";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false;
		} 
		 if (Age2 < 18 || Age2 > 60) {
			Err.innerHTML = "";
			Err.innerHTML = "<br/><br/>Please Input Value Again!<br/>For Policy Term "
					+ $('#coPolicyTerm option:selected').text()
					+ ":<br/>Min Age: 18<br/>Max Age: "
					+ (70 - ($('#coPolicyTerm option:selected').text() * 1));
			document.getElementById("la2dateOfBirth").style.background = "yellow";
			document.getElementById("basicPlanSa").style.background = "white";
			document.getElementById("la2name").style.background = "white";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false;
		}

		if (parseInt(Age2) + parseInt(policyTerm) > 60) {
			Err.innerHTML = "";
			Err.innerHTML = "<br/><br/>Please Input Value Again!<br/>For Policy Term "
					+ $('#coPolicyTerm option:selected').text()
					+ ":<br/>Min Age: 18<br/>Max Age: "
					+ (70 - ($('#coPolicyTerm option:selected').text() * 1));
					document.getElementById("la2dateOfBirth").style.background = "yellow";
					document.getElementById("basicPlanSa").style.background = "white";
					document.getElementById("la2name").style.background = "white";
			$('html, body').animate({
				scrollTop : 0
			}, 'slow');
			return false;
		}
	} 

		document.form.submit();
		return true;
}

$('#occla1').change(function() {
	occla1 = $(this).val();
});
$('#occla2').change(function() {
	occla2 = $(this).val();
});
$('#occpo').change(function() {
	occpo = $(this).val();
});