var occla1 = $('#occla1').val();
var occpo = $('#occpo').val();
$(function() {
	if (typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, '');
		}
	}

	$(document)
			.ready(
					function() {

						$('#coLa1relationship option[value="14"]').remove();
						//$('#coPaymentType option[value="18"]').remove(); // BASED ON REQUEST KUNTHEA UPDATED 28-12-2018 , ASK TO ADD PAYMENT MODE AS CASH
						$('#coPremiumTerm').attr('disabled', 'disabled');
						$('#coPremiumTerm').after(
								'<input type="hidden" name="coPremiumTerm" value="'
										+ $('#coPremiumTerm').val() + '" />');

						var tmpCoPaymentMode = $('#coPaymentMode').val();
						if (tmpCoPaymentMode == '19') {
							$("#coPaymentType option[value='18']").prop(
									"disabled", true);
							$('#coPaymentType')
									.parent()
									.parent()
									.parent()
									.parent()
									.before(
											'<p style="margin-left: 20px; color: orange;">*Note: This product cannot be sold in <b>Cash</b> with <b>Monthly mode!</b></p>');
						}
						var tmpCoPaymentType = $('#coPaymentType').val();
						if (tmpCoPaymentType == '18') {
							$("#coPaymentMode option[value='19'").prop(
									"disabled", true);
							$('#coPaymentType')
									.parent()
									.parent()
									.parent()
									.parent()
									.before(
											'<p style="margin-left: 20px; color: orange;">*Note: This product cannot be sold in <b>Cash</b> with <b>Monthly mode!</b></p>');
						}

						$('#coPaymentType')
								.change(
										function() {
											var tmpCoPaymentType = $(
													'#coPaymentType').val();
											if (tmpCoPaymentType == '18') {
												$(
														"#coPaymentMode option[value='19']")
														.prop("disabled", true);
												$('#coPaymentType')
														.parent()
														.parent()
														.parent()
														.parent()
														.before(
																'<p style="margin-left: 20px; color: orange;">*Note: This product cannot be sold in <b>Cash</b> with <b>Monthly mode!</b></p>');
											} else if (tmpCoPaymentType == '17') {
												$(
														"#coPaymentMode option[value='19']")
														.prop("disabled", false);
												$('#coPaymentType').parent()
														.parent().parent()
														.parent().parent()
														.children('p').remove();
											}
										});

						$('#coPaymentMode')
								.change(
										function() {
											var tmpCoPaymentMode = $(
													'#coPaymentMode').val();
											if (tmpCoPaymentMode == '19') {
												$(
														"#coPaymentType option[value='18']")
														.prop("disabled", true);
												$('#coPaymentType')
														.parent()
														.parent()
														.parent()
														.parent()
														.before(
																'<p style="margin-left: 20px; color: orange;">*Note: This product cannot be sold in <b>Cash</b> with <b>Monthly mode!</b></p>');
											} else if (tmpCoPaymentMode == '20'
													|| tmpCoPaymentMode == '21') {
												$(
														"#coPaymentType option[value='18']")
														.prop("disabled", false);
												$('#coPaymentType').parent()
														.parent().parent()
														.parent().parent()
														.children('p').remove();
											}

										});
					});

	// Custom Function to Detect Policy Term with Premium term
	$('#coPolicyTerm').change(
			function() {

				var indexpre = $("select[name='coPolicyTerm'] option:selected")
						.index();
				var indexpol = indexpre + 1;
				$('#coPremiumTerm :nth-child(' + indexpol + ')').prop(
						'selected', true);
				$('#coPremiumTerm').after(
						'<input type="hidden" name="coPremiumTerm" value="'
								+ $('#coPremiumTerm').val() + '" />');
				
				validateAgeDOBLA1();
				
			})

	// End SA auto calulation
	var currentDate = new Date();
	$('#commDate').datepicker({
		useCurrent : true,
		pickTime : false,
		dateFormat : "dd/mm/yy"
	});

	$('#commDate').change(function(){
		validateAgeDOBLA1();
	});
	
	if (!$('#commDate').val()) {
		$('#commDate').datepicker("setDate", currentDate);
	}
	// $('#commDate').datetimepicker("setDate",currentDate);
	$('#la1dateOfBirth').datepicker({
		useCurrent : false,
		pickTime : false,
		dateFormat : "dd/mm/yy",
		viewMode : 'years'
	});
	
	$('#la1dateOfBirth').change(function(){
		validateAgeDOBLA1();
	});
	
	$('#syndate').datepicker({
		autoclose : true,
		todayHighlight : true,
		format : "dd/mm/yyyy"
	});
	$("#syndate").datepicker("setDate", currentDate);
	$('#la2dateOfBirth').datepicker({
		useCurrent : false,
		pickTime : false,
		dateFormat : "dd/mm/yy",
		viewMode : 'years'
	});
	var LAPO = $("#coLa1relationship option:selected").text();
	if (LAPO.trim() === "PO = LA") {
		document.getElementById("poName").readOnly = true;
		document.getElementById("droccupationpo").hidden = true;
	} else {
		document.getElementById("poName").readOnly = false;
		document.getElementById("droccupationpo").hidden = false;
	}

	var coPremiumTerm = document.getElementById("coPremiumTerm");
	// coPremiumTerm.remove(coPremiumTerm.length-1);

});

$('#coPremiumTerm').change(
		function() {
			document.getElementById("l1Rsr1Term").value = $(
					"#coPremiumTerm option:selected").text();
			document.getElementById("l2Rtr1Term").value = $(
					"#coPremiumTerm option:selected").text();
			document.getElementById("l2Rsr1Term").value = $(
					"#coPremiumTerm option:selected").text();
		});

$('#coLa1relationship').change(function() {
	var LAPO = $("#coLa1relationship option:selected").text();

	if (LAPO.trim() === "PO = LA") {
		document.getElementById("poName").readOnly = true;
		document.getElementById("poName").value = $('#la1name').val();
		document.getElementById("droccupationpo").hidden = true;
		$('#occpo').val('0');
	} else {
		document.getElementById("poName").readOnly = false;
		document.getElementById("poName").value = "";
		document.getElementById("droccupationpo").hidden = false;
	}
});

$('#la1name').change(function() {
	if ($("#coLa1relationship option:selected").text() === "PO = LA") {
		$("#poName").val($('#la1name').val());
	}
});

$("#la1name").keypress(function(event) {
	var ew = event.which;
	if (ew == 32 || ew == 8) {
		return true;
	} else if (ew >= 65 && ew <= 90) {
		return true;
	} else if (ew >= 97 && ew <= 122) {
		return true;
	} else {
		return false;
	}
});

$("#la2name").on('input', function(event) {
	if ($(this).val() == '') {
		$('#occla2').val('0');
	}
});
function Age(monthDob, dayDob, yearDob) {

}
function checkSubmit() {
	return validate();
}

function validateAgeDOBLA1() {
	var policyTerm = $("#coPolicyTerm option:selected").text();
	var premuimTerm = $("#coPremiumTerm option:selected").text();
	var DOB1 = document.getElementById("la1dateOfBirth").value;
	comDate = document.getElementById("commDate").value;

	if (DOB1 === "") {
		Err.innerHTML = "";
		Err.innerHTML = "<br/><br/>Please Input Value Again!<br/>For Policy Term "
				+ $('#coPolicyTerm option:selected').text()
				+ ":<br/>Min Age: 18<br/>Max Age: "
				+ (65 - ($('#coPolicyTerm option:selected').text() * 1));
		document.getElementById("la1dateOfBirth").style.background = "yellow";
		document.getElementById("basicPlanSa").style.background = "white";
		document.getElementById("la1name").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	}

	var today = comDate;
	var comDateArray = today.split("/");
	var newComDate = comDateArray[2] + '/' + comDateArray[1] + '/'
			+ comDateArray[0];
	var ComDateTo = new Date(newComDate);
	ComDateTo.setDate(ComDateTo.getDate() + 1);
	var Age1 = 0;
	var date = DOB1;
	var datearray = date.split("/");
	var birthDate = new Date(datearray[2], datearray[1] * 1 - 1, datearray[0]);
	Age1 = Math.floor((ComDateTo.getTime() - birthDate.getTime())
			/ (365.25 * 24 * 60 * 60 * 1000));

	if (!(Age1 >= 18 && Age1 <= 45)) {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/><br/>Please Input Value Again!<br/>For Policy Term "
				+ $('#coPolicyTerm option:selected').text()
				+ ":<br/>Min Age: 18<br/>Max Age: "
				+ (65 - ($('#coPolicyTerm option:selected').text() * 1));
		document.getElementById("la1dateOfBirth").style.background = "yellow";
		document.getElementById("basicPlanSa").style.background = "white";
		document.getElementById("la1name").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} else if (parseInt(Age1) + parseInt(policyTerm) > 65) {
		Err.innerHTML = "";
		Err.innerHTML = "<br/><br/>Please Input Value Again!<br/>For Policy Term "
				+ $('#coPolicyTerm option:selected').text()
				+ ":<br/>Min Age: 18<br/>Max Age: "
				+ (65 - ($('#coPolicyTerm option:selected').text() * 1));
		document.getElementById("la1dateOfBirth").style.background = "yellow";
		document.getElementById("la2dateOfBirth").style.background = "white";
		document.getElementById("basicPlanSa").style.background = "white";
		document.getElementById("la1name").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	}
	Err.innerHTML = "";
	document.getElementById("la1dateOfBirth").style.background = "white";
	
	return true;
}

function validate() {
	var clName = "";
	var SA = "";
	var DOB1 = "";
	Err.innerHTML = "";
	var LAPO = "";
	var poName = "";
	var comDate = "";
	var policyTerm = "";
	var premuimTerm = "";

	clName = document.getElementById("la1name").value;
	SA = document.getElementById("basicPlanSa").value;
	DOB1 = document.getElementById("la1dateOfBirth").value;
	LAPO = $("#coLa1relationship option:selected").text();
	policyTerm = $("#coPolicyTerm option:selected").text();
	premuimTerm = $("#coPremiumTerm option:selected").text();
	poName = document.getElementById("poName").value;
	comDate = document.getElementById("commDate").value;

	if (LAPO.trim() === "PO = LA") {
		document.getElementById("poName").value = clName;
	}
	// if (policyTerm !== premuimTerm) {
	// Err.innerHTML="";
	// Err.innerHTML=Err.innerHTML+"<br/><b>Premuim Term</b> and <b>Policy
	// Term</b> must be same value!";
	// document.getElementById("coPremiumTerm").style.background = "yellow";
	// document.getElementById("coPolicyTerm").style.background = "yellow";
	// $('html, body').animate({ scrollTop: 0 }, 'slow');
	// return false;
	// }

	if (!validateAgeDOBLA1()) {
		return false;
	}
	if (LAPO.trim() != "PO = LA" && poName === "") {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Input the mandatory <b>Policy Owner</b>";
		document.getElementById("poName").style.background = "yellow";
		document.getElementById("la1dateOfBirth").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	}
	if (LAPO.trim() != "PO = LA" && (occpo == '0' || occpo == '')) {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please select Occupation of PO</b>";
		document.getElementById("occpo").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	}

	if (clName === "") {
		Err.innerHTML = "";
		Err.innerHTML = "<br/>Input the mandatory field -LA1Name";
		document.getElementById("la1name").style.background = "yellow";
		document.getElementById("basicPlanSa").style.background = "white";
		document.getElementById("la1dateOfBirth").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} else if (comDate === "") {
		Err.innerHTML = "";
		Err.innerHTML = "<br/>Input the mandatory Policy commencement date";
		document.getElementById("commDate").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} else if (occla1 == '0' || occla1 == '') {
		Err.innerHTML = "";
		Err.innerHTML = Err.innerHTML + "<br/>Please select Occupation of LA1";
		document.getElementById("occla1").style.background = "yellow";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} else if (SA === "") {
		Err.innerHTML = Err.innerHTML
				+ "<br/>Please input SA again! <br/>Basic plan’s SA ≥ US$5,000";
		document.getElementById("basicPlanSa").style.background = "yellow";
		document.getElementById("la1name").style.background = "white";
		document.getElementById("la1dateOfBirth").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} else if (isUnlimitedSA == 'false' && SA < 5000) {

		Err.innerHTML = Err.innerHTML
				+ "<br/>Please input SA again! <br/>Basic plan’s SA ≥ US$5,000";
		document.getElementById("basicPlanSa").style.background = "yellow";
		document.getElementById("la1name").style.background = "white";
		document.getElementById("la1dateOfBirth").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} else if (isUnlimitedSA == 'false' && SA > 10000) {

		Err.innerHTML = Err.innerHTML
				+ "<br/>Please input SA again! <br/>Basic plan’s SA <= US$10,000";
		document.getElementById("basicPlanSa").style.background = "yellow";
		document.getElementById("la1name").style.background = "white";
		document.getElementById("la1dateOfBirth").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;
	} else if (isUnlimitedSA == 'true' && SA <= 0) {
		Err.innerHTML = Err.innerHTML
				+ "<br/>Input the mandatory field <b>BasicPlan_SA > 0</b>";
		document.getElementById("basicPlanSa").style.background = "yellow";
		document.getElementById("la1name").style.background = "white";
		document.getElementById("la1dateOfBirth").style.background = "white";
		$('html, body').animate({
			scrollTop : 0
		}, 'slow');
		return false;

	} else {
		document.form.submit();
		return true;
	}
}

$('#occla1').change(function() {
	occla1 = $(this).val();
});
$('#occla2').change(function() {
	occla2 = $(this).val();
});
$('#occpo').change(function() {
	occpo = $(this).val();
});