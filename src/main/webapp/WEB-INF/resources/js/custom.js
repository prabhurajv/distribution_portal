/**
 * Resize function without multiple trigger
 * 
 * Usage:
 * $(window).smartresize(function(){  
 *     // code here
 * });
 */
(function($,sr){
    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function (func, threshold, execAsap) {
      var timeout;

        return function debounced () {
            var obj = this, args = arguments;
            function delayed () {
                if (!execAsap)
                    func.apply(obj, args); 
                timeout = null; 
            }

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100); 
        };
    };

    // smartresize 
    jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var CURRENT_URL = window.location.href.split('?')[0],
    $BODY = $('body'),
    $MENU_TOGGLE = $('#menu_toggle'),
    $SIDEBAR_MENU = $('#sidebar-menu'),
    $SIDEBAR_FOOTER = $('.sidebar-footer'),
    $LEFT_COL = $('.left_col'),
    $RIGHT_COL = $('.right_col'),
    $NAV_MENU = $('.nav_menu'),
    $FOOTER = $('footer'),
	$TOGGLED = true;

// Sidebar
$(document).ready(function() {
    // TODO: This is some kind of easy fix, maybe we can improve this
    var setContentHeight = function () {
        // reset height
        $RIGHT_COL.css('min-height', $(window).height());

        var bodyHeight = $BODY.outerHeight(),
            footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
            leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
            contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

        // normalize content
        contentHeight -= $NAV_MENU.height() + footerHeight;

        $RIGHT_COL.css('min-height', contentHeight);
    };

    $SIDEBAR_MENU.find('a').on('click', function(ev) {
        var $li = $(this).parent();

        if ($li.is('.active')) {
            $li.removeClass('active active-sm');
            $('ul:first', $li).slideUp(function() {
                setContentHeight();
            });
        } else {
            // prevent closing menu if we are on child menu
            if (!$li.parent().is('.child_menu')) {
                $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                $SIDEBAR_MENU.find('li ul').slideUp();
            }
            
            $li.addClass('active');

            $('ul:first', $li).slideDown(function() {
                setContentHeight();
            });
        }
    });

    // toggle small or large menu
//    $MENU_TOGGLE.on('click', function() {
//        if ($BODY.hasClass('nav-md')) {
//            $SIDEBAR_MENU.find('li.active ul').hide();
//            $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
//            
//            var url = /*[[@{/resources/images/Prudential Cambodia-Logo.png}]]*/'';
//            $('.site_title').attr('href', url);
//        } else {
//            $SIDEBAR_MENU.find('li.active-sm ul').show();
//            $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
//            
//            var url = /*[[@{/resources/images/Prudential.png}]]*/'';
//            $('.site_title').attr('href', url);
//        }
//
//        $BODY.toggleClass('nav-md nav-sm');
//        
//        if($TOGGLED === false){
//        	$TOGGLED = true;
//        }else{
//        	$TOGGLED = false;
//        }
//
//        setContentHeight();
//    });

    // check active menu
    $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

//    $SIDEBAR_MENU.find('a').filter(function () {
//        return this.href == CURRENT_URL;
//    }).parent('li').addClass('current-page').parents('ul').slideDown(function() {
//        setContentHeight();
//    }).parent().addClass('active');
    
    $SIDEBAR_MENU.find('a').filter(function () {
        return this.href == CURRENT_URL;
    }).parent('li').addClass('current-page').parents('ul').parent().addClass('active');

    // recompute content when resizing
    $(window).smartresize(function(){  
        setContentHeight();
    });

    setContentHeight();

    // fixed sidebar
    if ($.fn.mCustomScrollbar) {
        $('.menu_fixed').mCustomScrollbar({
            autoHideScrollbar: true,
            theme: 'minimal',
            mouseWheel:{ preventDefault: true }
        });
    }
});
// /Sidebar

// Panel toolbox
$(document).ready(function() {
    $('.collapse-link').on('click', function() {
        var $BOX_PANEL = $(this).closest('.x_panel'),
            $ICON = $(this).find('i'),
            $BOX_CONTENT = $BOX_PANEL.find('.x_content');
        
        // fix for some div with hardcoded fix class
        if ($BOX_PANEL.attr('style')) {
            $BOX_CONTENT.slideToggle(200, function(){
                $BOX_PANEL.removeAttr('style');
            });
        } else {
            $BOX_CONTENT.slideToggle(200); 
            $BOX_PANEL.css('height', 'auto');  
        }

        $ICON.toggleClass('fa-chevron-up fa-chevron-down');
    });

    $('.close-link').click(function () {
        var $BOX_PANEL = $(this).closest('.x_panel');

        $BOX_PANEL.remove();
    });
});
// /Panel toolbox

// Tooltip
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });
});
// /Tooltip

// Progressbar
if ($(".progress .progress-bar")[0]) {
    $('.progress .progress-bar').progressbar();
}
// /Progressbar

// Switchery
$(document).ready(function() {
    if ($(".js-switch")[0]) {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html, {
                color: '#26B99A'
            });
        });
    }
});
// /Switchery

// iCheck
$(document).ready(function() {
    if ($("input.flat")[0]) {
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    }
});
// /iCheck

// Table
$('table input').on('ifChecked', function () {
    checkState = '';
    $(this).parent().parent().parent().addClass('selected');
    countChecked();
});
$('table input').on('ifUnchecked', function () {
    checkState = '';
    $(this).parent().parent().parent().removeClass('selected');
    countChecked();
});

var checkState = '';

$('.bulk_action input').on('ifChecked', function () {
    checkState = '';
    $(this).parent().parent().parent().addClass('selected');
    countChecked();
});
$('.bulk_action input').on('ifUnchecked', function () {
    checkState = '';
    $(this).parent().parent().parent().removeClass('selected');
    countChecked();
});
$('.bulk_action input#check-all').on('ifChecked', function () {
    checkState = 'all';
    countChecked();
});
$('.bulk_action input#check-all').on('ifUnchecked', function () {
    checkState = 'none';
    countChecked();
});

function countChecked() {
    if (checkState === 'all') {
        $(".bulk_action input[name='table_records']").iCheck('check');
    }
    if (checkState === 'none') {
        $(".bulk_action input[name='table_records']").iCheck('uncheck');
    }

    var checkCount = $(".bulk_action input[name='table_records']:checked").length;

    if (checkCount) {
        $('.column-title').hide();
        $('.bulk-actions').show();
        $('.action-cnt').html(checkCount + ' Records Selected');
    } else {
        $('.column-title').show();
        $('.bulk-actions').hide();
    }
}

// Accordion
$(document).ready(function() {
    $(".expand").on("click", function () {
        $(this).next().slideToggle(200);
        $expand = $(this).find(">:first-child");

        if ($expand.text() == "+") {
            $expand.text("-");
        } else {
            $expand.text("+");
        }
    });
});

// NProgress
if (typeof NProgress != 'undefined') {
//    $(document).ready(function () {
//        NProgress.start();
//    });

//    $(window).load(function () {
//        NProgress.done();
//    });
}


//==============================================================================
/*jshint browser:true*/

//
// jquery.sessionTimeout.js
//
// After a set amount of time, a dialog is shown to the user with the option
// to either log out now, or stay connected. If log out now is selected,
// the page is redirected to a logout URL. If stay connected is selected,
// a keep-alive URL is requested through AJAX. If no options is selected
// after another set amount of time, the page is automatically redirected
// to a timeout URL.
//
//
// USAGE
//
//   1. Include jQuery
//   2. Include jQuery UI (for dialog)
//   3. Include jquery.sessionTimeout.js
//   4. Call $.sessionTimeout(); after document ready
//
//
// OPTIONS
//
//   message
//     Text shown to user in dialog after warning period.
//     Default: 'Your session is about to expire.'
//
//   keepAliveUrl
//     URL to call through AJAX to keep session alive. This resource should do something innocuous that would keep the session alive, which will depend on your server-side platform.
//     Default: '/keep-alive'
//
//   keepAliveAjaxRequestType
//     How should we make the call to the keep-alive url? (GET/POST/PUT)
//     Default: 'POST'
//
//   redirUrl
//     URL to take browser to if no action is take after warning period
//     Default: '/timed-out'
//
//   logoutUrl
//     URL to take browser to if user clicks "Log Out Now"
//     Default: '/log-out'
//
//   warnAfter
//     Time in milliseconds after page is opened until warning dialog is opened
//     Default: 900000 (15 minutes)
//
//   redirAfter
//     Time in milliseconds after page is opened until browser is redirected to redirUrl
//     Default: 1200000 (20 minutes)
//
//   appendTime
//     If true, appends the current time stamp to the Keep Alive url to prevent caching issues
//     Default: true
//
(function ($) {
    jQuery.sessionTimeout = function (options) {
        var defaults = {
            message: 'Your session is about to expire.',
            keepAliveUrl: '/keep-alive',
            keepAliveAjaxRequestType: 'GET',
            redirUrl: '/timed-out',
            logoutUrl: '/log-out',
            warnAfter: 900000, // 15 minutes
            redirAfter: 1200000, // 20 minutes
            appendTime: true, // appends time stamp to keep alive url to prevent caching
            csrfName: '',
            csrfToken: ''
        };

        // Extend user-set options over defaults
        var o = defaults,
				dialogTimer,
				redirTimer;

        if (options) { o = $.extend(defaults, options); }

        // Create timeout warning dialog
        $('body').append('<div title="Session Timeout" id="sessionTimeout-dialog">' + o.message + '</div>');
        $('#sessionTimeout-dialog').dialog({
            autoOpen: false,
            width: 400,
            modal: true,
            closeOnEscape: false,
            open: function () { $(".ui-dialog-titlebar-close").hide(); },
            buttons: {
                // Button one - takes user to logout URL
                "Log Out Now": function () {
//                    window.location = o.logoutUrl;
                    var form = document.createElement("FORM");
                    form.method = "POST";
                    form.action = o.logoutUrl;
                    form.style = "display: none";
                    
                    var csrfInput = document.createElement("INPUT");
                    csrfInput.type = "hidden";
                    csrfInput.name = o.csrfName;
                    csrfInput.value = o.csrfToken;
                    
                    form.appendChild(csrfInput);
                    
                    document.body.appendChild(form);
                    
                    form.submit();
                },
                // Button two - closes dialog and makes call to keep-alive URL
                "Stay Connected": function () {
                    $(this).dialog('close');

                    $.ajax({
                        type: o.keepAliveAjaxRequestType,
                        url: o.appendTime ? updateQueryStringParameter(o.keepAliveUrl, "_", new Date().getTime()) : o.keepAliveUrl
                    });

                    // Stop redirect timer and restart warning timer
                    controlRedirTimer('stop');
                    controlDialogTimer('start');
                }
            }
        });

        function controlDialogTimer(action) {
            switch (action) {
                case 'start':
                    // After warning period, show dialog and start redirect timer
                    dialogTimer = setTimeout(function () {
                        $('#sessionTimeout-dialog').dialog('open');
                        controlRedirTimer('start');
                    }, o.warnAfter);
                    break;

                case 'stop':
                    clearTimeout(dialogTimer);
                    break;
            }
        }

        function controlRedirTimer(action) {
            switch (action) {
                case 'start':
                    // Dialog has been shown, if no action taken during redir period, redirect
                    redirTimer = setTimeout(function () {
//                        window.location = o.redirUrl;
                    	var form = document.createElement("FORM");
                        form.method = "POST";
                        form.action = o.logoutUrl;
                        form.style = "display: none";
                        
                        var csrfInput = document.createElement("INPUT");
                        csrfInput.type = "hidden";
                        csrfInput.name = o.csrfName;
                        csrfInput.value = o.csrfToken;
                        
                        form.appendChild(csrfInput);
                        
                        document.body.appendChild(form);
                        
                        form.submit();
                    }, o.redirAfter - o.warnAfter);
                    break;

                case 'stop':
                    clearTimeout(redirTimer);
                    break;
            }
        }

        // Courtesy of http://stackoverflow.com/questions/5999118/add-or-update-query-string-parameter
        // Includes fix for angular ui-router as per comment by j_walker_dev
        function updateQueryStringParameter(uri, key, value) {
        	return uri;
            var re = new RegExp("([?|&])" + key + "=.*?(&|#|$)", "i");

            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            } else {
                var hash = '';

                if (uri.indexOf('#') !== -1) {
                    hash = uri.replace(/.*#/, '#');
                    uri = uri.replace(/#.*/, '');
                }

                var separator = uri.indexOf('?') !== -1 ? "&" : "?";
                return uri + separator + key + "=" + value + hash;
            }
        }

        $(document).ajaxComplete(function () {
            if (!$('#sessionTimeout-dialog').dialog("isOpen")) {
                controlRedirTimer('stop');
                controlDialogTimer('stop');
                controlDialogTimer('start');
            }
        });

        // Begin warning period
        controlDialogTimer('start');
    };
})(jQuery);

var showModalDialog = showModalDialog || (function ($) {
	var $dialog = $('<div id="modal" class="modal fade" role="dialog">' +
			'<div class="modal-dialog">' +
				'<div class="modal-content">' +
					'<div class="modal-header bg-danger">' +
						'<button type="button" class="close" data-dismiss="modal">&times;</button>' +
						'<h4 class="modal-title"><b>Alert</b></h4>' +
					'</div>' +
					'<div class="modal-body">' +
						'<form id="frmModalEmail" method="post">' +
							'<div class="row">' +
								'<p id="alertMsg" style="text-align: center;"></p>' +
							'</div>' +
						'</form>' +
					'</div>' +
					'<div class="modal-footer">' +
						'<button type="button" class="btn btn-danger" id="btnGoToChangePwd" data-dismiss="modal">Change Password</button>' +
						'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param view Custom view
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, context, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}

			if (typeof message === 'undefined') {
				message = 'Loading';
			}
			var settings = $.extend({
				dialogSize: 's',
				//progressType: '',
				onHide: clearModalInput // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);

			$dialog.find('#alertMsg').text(message);

			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			
			$dialog.find('#btnGoToChangePwd').on('click', function(e){
				e.preventDefault();
				
				window.location.href = context + 'changepwd';
			});

			// Opening dialog
			$dialog.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
			$dialog.modal('destroy').remove();
		},

		/**
		 * Destroy dialog
		 */
		destroy: function () {
			$dialog.modal('hide');
			$dialog.modal('destroy').remove();
		},
		setType: function (value) {
			type = value;
		}
	};

	function clearModalInput() {

	}
})(jQuery);


//========== Function Format Currency ==========
function FormatCurrency(defaultformat, value) {
	if((value * 1) > 0) {
		return '$ ' + (value * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	}
	return defaultformat;
}
//========== End Function Format Currency ==========
//========== Function Format Percent ==========
function FormatPercent(defaultformat, value) {
	if((value * 1) > 0) {
		return (value * 1).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	}
	return defaultformat;
}
//========== End Function Format Percent ==========
