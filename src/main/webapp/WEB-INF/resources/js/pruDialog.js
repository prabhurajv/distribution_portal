//FORMAT DATE JAVA

Date.prototype.customFormat = function(formatString) {
	var YYYY, YY, MMMM, MMM, MM, M, DDDD, DDD, DD, D, hhhh, hhh, hh, h, mm, m, ss, s, ampm, AMPM, dMod, th;
	YY = ((YYYY = this.getFullYear()) + "").slice(-2);
	MM = (M = this.getMonth() + 1) < 10 ? ('0' + M) : M;
	MMM = (MMMM = [ "January", "February", "March", "April", "May", "June",
			"July", "August", "September", "October", "November", "December" ][M - 1])
			.substring(0, 3);
	DD = (D = this.getDate()) < 10 ? ('0' + D) : D;
	DDD = (DDDD = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday",
			"Friday", "Saturday" ][this.getDay()]).substring(0, 3);
	th = (D >= 10 && D <= 20) ? 'th' : ((dMod = D % 10) == 1) ? 'st'
			: (dMod == 2) ? 'nd' : (dMod == 3) ? 'rd' : 'th';
	formatString = formatString.replace("#YYYY#", YYYY).replace("#YY#", YY)
			.replace("#MMMM#", MMMM).replace("#MMM#", MMM).replace("#MM#", MM)
			.replace("#M#", M).replace("#DDDD#", DDDD).replace("#DDD#", DDD)
			.replace("#DD#", DD).replace("#D#", D).replace("#th#", th);
	h = (hhh = this.getHours());
	if (h == 0)
		h = 24;
	if (h > 12)
		h -= 12;
	hh = h < 10 ? ('0' + h) : h;
	hhhh = hhh < 10 ? ('0' + hhh) : hhh;
	AMPM = (ampm = hhh < 12 ? 'am' : 'pm').toUpperCase();
	mm = (m = this.getMinutes()) < 10 ? ('0' + m) : m;
	ss = (s = this.getSeconds()) < 10 ? ('0' + s) : s;
	return formatString.replace("#hhhh#", hhhh).replace("#hhh#", hhh).replace(
			"#hh#", hh).replace("#h#", h).replace("#mm#", mm).replace("#m#", m)
			.replace("#ss#", ss).replace("#s#", s).replace("#ampm#", ampm)
			.replace("#AMPM#", AMPM);
};

// =========PopUpSubmission=========
var showModalSubmissionDialog = showModalSubmissionDialog
		|| (function($) {
			var $dialog = $('<div class="modal fade modal-submission" tabindex="-1" role="dialog" aria-hidden="true">'
					+ '<div class="modal-dialog modal-lg">'
					+ '<div class="modal-content">'
					+ '<div class="modal-header">'
					+ '<button type="button" class="close" data-dismiss="modal">'
					+ '<span aria-hidden="true">×</span>'
					+ '</button>'
					+ '<h4 class="modal-title" id="myModalLabel2">Submission Detail</h4>'
					+ '</div>'
					+ '<div class="modal-body" style="color:rgb(85, 85, 85);">'
					+ '<div class="table-responsive">'
					+ '<table style="width:100%;" id="submission_grid" class="table table-striped table-advance table-hover">'
					+ '<thead>'
					+ '<tr class="pending-tr">'
					+ '<th>Policy Number</th>'
					+ '<th>Application Number</th>'
					+ '<th>Policy Owner</th>'
					+ '<th>Submission Date</th>'
					+ '<th>APE ($USD)</th>'
					+ '<th>Agent Number</th>'
					+ '<th>Agent Name</th>'
					+ '<th>Week</th>'
					+ '</tr>'
					+ '</thead>'
					+ '<tbody>'
					+ '</tbody>'
					+ '</table>'
					+ '</div>'
					+ '</div>'
					+ '<div class="modal-footer">'
					+ '<a id="btnCloseDetail" data-toggle="tab" href="#close"'
					+ 'data-dismiss="modal"'
					+ 'style="background-color: #ED1B2E; color: #FFF;"'
					+ 'class="btn"> Close </a>'
					+ '</div>'
					+ '</div>'
					+ '</div>' + '</div>');

			return {
				show : function(weekth) {
					$dialog.modal();
					SelectListSubmission($dialog, weekth);
				},
				/**
				 * Closes dialog
				 */
				hide : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},

				/**
				 * Destroy dialog
				 */
				destroy : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},
				setType : function(value) {
					type = value;
				}
			};
		})(jQuery);

function SelectListSubmission(dialog, weekth) {
	var isAllowedLoading = true;
	if (isAllowedLoading === true) {
		$('.right_col').loading({
			message : 'Loading Please wait...'
		});
	}
	$.ajax({
		url : urlsubmission,
		type : 'get',
		data : {
			weekth : weekth,
			summaryby : 'dashboard'
		},
		success : function(result) {
			var dataSet = JSON.parse(result);
			var table = dialog.find('#submission_grid').DataTable();
			table.destroy();
			table = dialog.find('#submission_grid').DataTable({
				data : dataSet,
				"columns" : [ {
					data : 'chdrNum'
				}, {
					data : 'chdrAppNum'
				}, {
					data : 'poName'
				}, {
					data : 'subDate'
				}, {
					data : 'chdrApe',
					render : $.fn.dataTable.render.number(",", ".", 2)
				}, {
					data : 'agntNum'
				}, {
					data : 'agntName'
				}, {
					data : 'weekth'
				} ],
				"fnDrawCallback" : function(oSettings, json) {
				}
			});
			$('.right_col').loading('stop');
		},
		error : function() {
			$('.right_col').loading('stop');
			console.log("Error");
		}
	});
}
// =========End-PopUpSubmission=========

// =========PopUpIssuse=========
var showModalIssuanceDialog = showModalIssuanceDialog
		|| (function($) {
			var $dialog = $('<div class="modal fade modal-issuance" tabindex="-1" role="dialog" aria-hidden="true">'
					+ '<div class="modal-dialog modal-lg">'
					+ '<div class="modal-content">'
					+ '<div class="modal-header">'
					+ '<button type="button" class="close" data-dismiss="modal">'
					+ '<span aria-hidden="true">×</span>'
					+ '</button>'
					+ '<h4 class="modal-title" id="myModalLabel2">Issuance Detail</h4>'
					+ '</div>'
					+ '<div class="modal-body" style="color:rgb(85, 85, 85);">'
					+ '<div class="table-responsive">'
					+ '<table style="width:100%;" id="issuance_grid" class="table table-striped table-advance table-hover">'
					+ '<thead>'
					+ '<tr class="pending-tr">'
					+ '<th>Policy Number</th>'
					+ '<th>Application Number</th>'
					+ '<th>Policy Owner</th>'
					+ '<th>Issue Date</th>'
					+ '<th>APE ($USD)</th>'
					+ '<th>Agent Number</th>'
					+ '<th>Agent Name</th>'
					+ '<th>Week</th>'
					+ '</tr>'
					+ '</thead>'
					+ '<tbody>'
					+ '</tbody>'
					+ '</table>'
					+ '</div>'
					+ '</div>'
					+ '<div class="modal-footer">'
					+ '<a id="btnCloseDetail" data-toggle="tab" href="#close"'
					+ 'data-dismiss="modal"'
					+ 'style="background-color: #ED1B2E; color: #FFF;"'
					+ 'class="btn"> Close </a>'
					+ '</div>'
					+ '</div>'
					+ '</div>' + '</div>');

			return {
				show : function(weekth) {
					$dialog.modal();
					SelectListIssuance($dialog, weekth);
				},
				/**
				 * Closes dialog
				 */
				hide : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},

				/**
				 * Destroy dialog
				 */
				destroy : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},
				setType : function(value) {
					type = value;
				}
			};
		})(jQuery);

function SelectListIssuance(dialog, weekth) {
	var isAllowedLoading = true;
	if (isAllowedLoading === true) {
		$('.right_col').loading({
			message : 'Loading Please wait...'
		});
	}
	$.ajax({
		url : urlissuace,
		type : 'get',
		data : {
			weekth : weekth,
			summaryby : 'dashboard'
		},
		success : function(result) {
			var dataSet = JSON.parse(result);
			var table = dialog.find('#issuance_grid').DataTable();
			table.destroy();
			table = dialog.find('#issuance_grid').DataTable({
				data : dataSet,
				"columns" : [ {
					data : 'chdrNum'
				}, {
					data : 'chdrAppNum'
				}, {
					data : 'poName'
				}, {
					data : 'chdrIssDate'
				}, {
					data : 'chdrApe',
					render : $.fn.dataTable.render.number(",", ".", 2)
				}, {
					data : 'agntNum'
				}, {
					data : 'agntName'
				}, {
					data : 'weekth'
				} ],
				columnDefs : [ {
					targets : 4,
					className : "currency",
				}

				],
				"fnDrawCallback" : function(oSettings, json) {
				}
			});
			$('.right_col').loading('stop');
		},
		error : function() {
			$('.right_col').loading('stop');
			console.log("Error");
		}
	});
}
// =========End-PopUpNetIssuse=========

// =========PopUpCollectionAndLapse=========
var showModalCollectionAndLapseDialog = showModalCollectionAndLapseDialog
		|| (function($) {
			var $dialog = $('<div class="modal fade modal-collectionandlapse" tabindex="-1" role="dialog" aria-hidden="true">'
					+ '<div class="modal-dialog modal-lg">'
					+ '<div class="modal-content">'
					+ '<div class="modal-header">'
					+ '<button type="button" class="close" data-dismiss="modal">'
					+ '<span aria-hidden="true">×</span>'
					+ '</button>'
					+ '<h4 class="modal-title" id="myModalLabel2">Detail of Premium Collection Info</h4>'
					+ '</div>'
					+ '<div class="modal-body" style="color:rgb(85, 85, 85);">'
					+ '<div class="table-responsive"> '
					+ '<table id="collectionandlapse_detail_grid" class="table table-striped">'
					+ '<thead>'
					+ '<tr>'
					+ '<th>Title</th>'
					+ '<th>Description</th>'
					+ '</tr>'
					+ '</thead>'
					+ '<tbody>'
					+ '</tbody>'
					+ '</table>'
					+ '</div>'
					+ '</div>'
					+ '<div class="modal-footer">'
					+ '<a id="btnCloseDetail" data-toggle="tab" href="#close"'
					+ 'data-dismiss="modal"'
					+ 'style="background-color: #ED1B2E; color: #FFF;"'
					+ 'class="btn"> Close </a>'
					+ '</div>'
					+ '</div>'
					+ '</div>' + '</div>');

			return {
				show : function(params, title) {
					var collectionandlapse = '';
					if (title == 'collection') {
						$dialog.find('#myModalLabel2').html('Detail of Premium Collection Info');
						collectionandlapse = '<tr><td>Policy Number</td><td>'
								+ params.chdrNum + '</td></tr>'
								+ '<tr><td>Application Number</td><td>'
								+ params.chdrAppnum + '</td></tr>'
								+ '<tr><td>Customer Name</td><td>'+params.poName+'</td></tr>'
								+ '<tr><td>Gender</td><td>' + params.gender
								+ '</td></tr>' + '<tr><td>Mobile 1</td><td>'
								+ params.clntMobile1 + '</td></tr>'
								+ '<tr><td>Mobile 2</td><td>'
								+ params.clntMobile2 + '</td></tr>'
								+ '<tr><td>Premium</td><td>'
								+ FormatCurrency('-', params.chdrPwithTax)
								+ '</td></tr>'
								+ '<tr><td>Billing Frequency</td><td>'
								+ params.chdrBillfreq + '</td></tr>'
								+ '<tr><td>Due Date</td><td>'
								+ params.chdrDueDate + '</td></tr>'
								+ '<tr><td>Grace period</td><td>'
								+ params.chdrGraceperiod + '</td></tr>'
								+ '<tr><td>Agent Number</td><td>'
								+ params.agntNum + '</td></tr>'
								+ '<tr><td>Agent Name</td><td>'
								+ params.agntName + '</td></tr>'
								+ '<tr><td>Agent Sup Code</td><td>'
								+ params.agntSupcode + '</td></tr>'
								+ '<tr><td>Agent Sup Name</td><td>'
								+ params.agntSupname + '</td></tr>'
								+ '<tr><td>Call Center Status</td><td>'
								+ params.ccStatus + '</td></tr>'
								+ '<tr><td>Agent Status</td><td>'
								+ params.agntStatus + '</td></tr>'
								+ '<tr><td>Policy Duration</td><td>'
								+ params.chdrAge + '</td></tr>'
								+ '<tr><td>Policy Status</td><td>'
								+ params.chdrStatus + '</td></tr>'
								+ '<tr><td>Is Impact Persistency</td><td>'
								+ params.isImpactPersistency + '</td></tr>'
								+ '<tr><td>Remark</td><td>' + params.bcuRemark
								+ '</td></tr>'
								+ '<tr><td>Customer Response</td><td>'
								+ params.customerResponse + '</td></tr>'
								+ '<tr><td>Branch Code</td><td>'
								+ params.branchCode + '</td></tr>'
								+ '<tr><td>Branch Name</td><td>'
								+ params.branchName + '</td></tr>';
					} else if (title === 'lapsed') {
						$dialog.find('#myModalLabel2').html('Detail of Lapsed Policy');
						collectionandlapse = '<tr><td>Policy Number</td><td>'
								+ params.chdrNum + '</td></tr>'
								+ '<tr><td>Application Number</td><td>'
								+ params.chdrAppnum + '</td></tr>'
								+ '<tr><td>Customer Name</td><td>'+params.poName+'</td></tr>'
								+ '<tr><td>Gender</td><td>' + params.gender
								+ '</td></tr>' + '<tr><td>Mobile 1</td><td>'
								+ params.clntMobile1 + '</td></tr>'
								+ '<tr><td>Mobile 2</td><td>'
								+ params.clntMobile2 + '</td></tr>'
								+ '<tr><td>Premium</td><td>'
								+ FormatCurrency('-', params.chdrPwithTax)
								+ '</td></tr>'
								+ '<tr><td>Billing Frequency</td><td>'
								+ params.chdrBillfreq + '</td></tr>'
								+ '<tr><td>Due Date</td><td>'
								+ params.chdrDueDate + '</td></tr>'
								+ '<tr><td>Grace period</td><td>'
								+ params.chdrGraceperiod + '</td></tr>'
								+ '<tr><td>Agent Number</td><td>'
								+ params.agntNum + '</td></tr>'
								+ '<tr><td>Agent Name</td><td>'
								+ params.agntName + '</td></tr>'
								+ '<tr><td>Agent Sup Code</td><td>'
								+ params.agntSupcode + '</td></tr>'
								+ '<tr><td>Agent Sup Name</td><td>'
								+ params.agntSupname + '</td></tr>'
								+ '<tr><td>Call Center Status</td><td>'
								+ params.ccStatus + '</td></tr>'
								+ '<tr><td>Agent Status</td><td>'
								+ params.agntStatus + '</td></tr>'
								+ '<tr><td>Policy Duration</td><td>'
								+ params.chdrAge + '</td></tr>'
								+ '<tr><td>Lapsed Date</td><td>'
								+ params.chdrLapseDate + '</td></tr>'
								+ '<tr><td>Policy Status</td><td>'
								+ params.chdrStatus + '</td></tr>'
								+ '<tr><td>Is Impact Persistency</td><td>'
								+ params.isImpactPersistency + '</td></tr>'
								+ '<tr><td>Lapsed Reason</td><td>'
								+ params.lapsedReason + '</td></tr>'
								+ '<tr><td>Remark</td><td>' + params.bcuRemark
								+ '</td></tr>'
								+ '<tr><td>Customer Response</td><td>'
								+ params.customerResponse + '</td></tr>'
								+ '<tr><td>Branch Code</td><td>'
								+ params.branchCode + '</td></tr>'
								+ '<tr><td>Branch Name</td><td>'
								+ params.branchName + '</td></tr>';
					}
					$dialog.find('#collectionandlapse_detail_grid>tbody').html(
							collectionandlapse);
					$dialog.modal();
				},
				/**
				 * Closes dialog
				 */
				hide : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},

				/**
				 * Destroy dialog
				 */
				destroy : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},
				setType : function(value) {
					type = value;
				}
			};
		})(jQuery);
// =========End-PopUpCollectionAndLapse=========

// =========PopUpAlteration=========
var showModalAlterationDialog = showModalAlterationDialog
		|| (function($) {
			var $dialog = $('<div class="modal fade modal-collectionandlapse" tabindex="-1" role="dialog" aria-hidden="true">'
					+ '<div class="modal-dialog modal-lg">'
					+ '<div class="modal-content">'
					+ '<div class="modal-header">'
					+ '<button type="button" class="close" data-dismiss="modal">'
					+ '<span aria-hidden="true">×</span>'
					+ '</button>'
					+ '<h4 class="modal-title" id="myModalLabel2">My Alteration Detail</h4>'
					+ '</div>'
					+ '<div class="modal-body" style="color:rgb(85, 85, 85);">'
					+ '<div class="table-responsive"> '
					+ '<table id="alteration_detail_grid" class="table table-striped">'
					+ '<thead>'
					+ '<tr>'
					+ '<th>Application Number</th>'
					+ '<th>Policy Number</th>'
					+ '<th>Policy Name</th>'
					+ '<th>Type</th>'
					+ '<th>Status</th>'
					+ '<th>Pending Reason</th>'
					+ '<th>Request Date</th>'
					+ '<th>Recieve Date</th>'
					+ '<th>Transaction Date</th>'
					+ '<th>Agent Number</th>'
					+ '<th>Agent Name</th>'
					+ '</tr>'
					+ '</thead>'
					+ '<tbody>'
					+ '</tbody>'
					+ '</table>'
					+ '</div>'
					+ '</div>'
					+ '<div class="modal-footer">'
					+ '<a id="btnCloseDetail" data-toggle="tab" href="#close"'
					+ 'data-dismiss="modal"'
					+ 'style="background-color: #ED1B2E; color: #FFF;"'
					+ 'class="btn"> Close </a>'
					+ '</div>'
					+ '</div>'
					+ '</div>' + '</div>');

			return {
				show : function(tablealterationdataSet) {
					var tablealteration = $dialog.find(
							'#alteration_detail_grid').DataTable();
					tablealteration.destroy();
					tablealteration = $dialog.find('#alteration_detail_grid')
							.DataTable({
								data : tablealterationdataSet,
								dom : 'TC<"clearfix">lfrt<"bottom"ip>',
								"columns" : [ {
									data : 'chdrNum'
								}, {
									data : 'poNum'
								}, {
									data : 'poName'
								}, {
									data : 'alterationType'
								}, {
									data : 'status'
								}, {
									data : 'pendingReason'
								}, {
									data : 'requestDate'
								}, {
									data : 'requestDate'
								}, {
									data : 'transactionDate'
								}, {
									data : 'agntNum'
								}, {
									data : 'agntName'
								} ],
								"columnDefs": [
												{
												    targets: 5,
												    createdCell: function (td, cellData, rowData, row, col) {
												    	if(cellData === null) {
												    		cellData = '';
												    	}
												    	var link = $('<span class="pendingreason-elipse" data-display="'+ cellData +
												    			'" data-display-trigger="long" onclick="pendingReasonClick(this)"' +
												    			'style="cursor:pointer;">more</span>');
												    	var span = $('<span><span>'+ cellData.substring(0,15) +'...</span></span>');
												    	span.append(link);
												    	$(td).html(span.html());
													}
												},
								               { 
								            	   	targets: 8,
													createdCell: function (td, cellData, rowData, row, col) {
														var minDate = new Date('1900-01-01Z00:00:00:000');
														var cellData = new Date(cellData);
														if(cellData > minDate)
														{
															$(td).html(new Date(cellData).toLocaleDateString("en-GB"));
														}
														else
														{
															$(td).html('');
														}
													}
								               }
								             ],
								"fnDrawCallback" : function(oSettings, json) {
									// showLoading(false);
								}
							});

					$dialog.modal();
				},
				/**
				 * Closes dialog
				 */
				hide : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},

				/**
				 * Destroy dialog
				 */
				destroy : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},
				setType : function(value) {
					type = value;
				}
			};
		})(jQuery);
// =========End-PopUpAlteration=========

// =========Collection And Lapse Comment=========
var showModalCollectionandLapseCommentDialog = showModalCollectionandLapseCommentDialog
		|| (function($) {
			var $dialog = $('<div class="modal fade modal-collectionandlapse" tabindex="-1" role="dialog" aria-hidden="true">'
					+ '<div class="modal-dialog modal-md">'
					+ '<div class="modal-content">'
					+ '<div class="modal-header">'
					+ '<button type="button" class="close" data-dismiss="modal">'
					+ '<span aria-hidden="true">×</span>'
					+ '</button>'
					+ '<h4 class="modal-title" id="header-comment">Collection and Lapse Comment</h4>'
					+ '</div>'
					+ '<div class="modal-body" style="color:rgb(85, 85, 85);">'
						+ '<form id="form-comment">'
					+ '<div class="card">'
					+ '<div class="card-header">'
					+ '<textarea class="form-control" rows="3" id="commenttext" name="commenttext" placeholder="Text Comment"></textarea>'
					+ '<input type="hidden" name="objkey" />'
					+ '<input type="hidden" name="objtype" />'
					+ '<button class="btn btn-primary pull-right" style="margin-right: 0px; margin-top: 5px;" id="btnSaveComment">Save</button>'
					+ '<div class="clearfix"></div>'
					+ '</div>'
					+ '<div class="card-body media-style-body" id="comment-text">'
					+ '</div>'
					+ '</div>'
					+ '</form>'
					+ '</div>'
					+ '<div class="modal-footer">'
					+ '<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> '
					+ '</div>' + '</div>' + '</div>' + '</div>');

			return {
				show : function(title, objkey, objtype, ajaxurl) {
					$dialog.find("#btnSaveComment").attr("disabled", true);
					$dialog.find("#commenttext").on("change paste keyup",
						function() {
							if ($(this).val() == '') {
								$dialog.find("#btnSaveComment").attr(
										"disabled", true);
							} else {
								$dialog.find("#btnSaveComment").attr(
										"disabled", false);
							}
						});
					$dialog.find('#commenttext').val('');
					$dialog.find('#header-comment').html(title);
					$dialog.find('input[name = objkey]').val(objkey);
					$dialog.find('input[name = objtype]').val(objtype);
					ajaxurl += '?objkey=' + objkey + '&objtype=' + objtype;
						$.ajax({
							url : ajaxurl,
							type : 'get',
							success : function(result) {
								var dataSet = JSON.parse(result);
								var text = '';
								$.each(
									dataSet,
									function(key, value) {
										var date = new Date(value[3]);
											text += '<div class="media media-style">'
													+ '<div class="media-left media-middle">'
														+ '<a href="#">'
															+ '<img class="media-object user-profile-icon" src="https://www.iconexperience.com/_img/o_collection_png/green_dark_grey/512x512/plain/user.png" alt="user-icon">'
														+ '</a>'
													+ '</div>'
												+ '<div class="media-body">'
													+ '<h4 class="media-heading">'
														+ value[6]
													+ '</h4>'
													+ '<p>'
														+ value[4]
													+ '</p>'
													+ '<small>'
														+ date.customFormat("#DD#/#MM#/#YYYY# #hh#:#mm#:#ss# #ampm#")
													+ '</small>'
													+ '</div>'
												+ '</div>';
									});
								$dialog.find('#comment-text').html(text);
							}
						});
					$dialog.find('#btnSaveComment').click(
							function(e) {
								e.preventDefault();
								AddCollectionAndLapseComment($dialog.find('#form-comment'));
							});

					var settings = $.extend({
						dialogSize : 'md',
						// progressType: '',
						onHide : clearModalInput
					// This callback runs after the dialog was hidden
					});

					// Configuring dialog
					$dialog.find('.modal-dialog').attr('class', 'modal-dialog')
							.addClass('modal-' + settings.dialogSize);

					// Adding callbacks
					if (typeof settings.onHide === 'function') {
						$dialog.off('hidden.bs.modal').on('hidden.bs.modal',
								function(e) {
									settings.onHide.call($dialog);
								});
					}
					$dialog.modal();
				},
				/**
				 * Closes dialog
				 */
				hide : function() {
					$dialog.modal('hide').data('bs.modal', null);
				},

				/**
				 * Destroy dialog
				 */
				destroy : function() {
					$dialog.modal('destroy').remove();
				},
				setType : function(value) {
					type = value;
				}
			};
			function clearModalInput() {
				$(this).remove();
				// $(tr).removeClass('hight-light');
			}
		})(jQuery);
// =========End-Collection And Lapse Comment=========
function AddCollectionAndLapseComment(form) {
		$.ajax({
				url : urladdcollectionandlapsecomment,
				type : 'get',
				data : form.serialize(),
				success : function(result) {
					var dataSet = JSON.parse(result);
					var text = '';
					$.each(
						dataSet,
						function(key, value) {
							var date = new Date(value[3]);
							text += '<div class="media media-style">'
									+ '<div class="media-left media-middle">'
										+ '<a href="#">'
											+ '<img class="media-object user-profile-icon" src="https://www.iconexperience.com/_img/o_collection_png/green_dark_grey/512x512/plain/user.png" alt="user-icon">'
										+ '</a>'
									+ '</div>'
									+ '<div class="media-body">'
										+ '<h4 class="media-heading">'
											+ value[6]
										+ '</h4>'
										+ '<p>'
											+ value[4]
										+ '</p>'
										+ '<small>'
											+ date.customFormat("#DD#/#MM#/#YYYY# #hh#:#mm#:#ss# #ampm#")
										+ '</small>'
										+ '</div>'
									+ '</div>';
						});
					$('#comment-text').html(text);
					$('#commenttext').val('');
					$('#commenttext').focus();
				}
			});
}

// =========Collection And Lapse Help=========
var showModalHelpCollectionandLapseCommentDialog = showModalHelpCollectionandLapseCommentDialog
		|| (function($) {
			var $dialog = $('<div class="modal fade modal-collectionandlapse" tabindex="-1" role="dialog" aria-hidden="true">'
					+ '<div class="modal-dialog modal-lg">'
					+ '<div class="modal-content">'
					+ '<div class="modal-header">'
					+ '<button type="button" class="close" data-dismiss="modal">'
					+ '<span aria-hidden="true">×</span>'
					+ '</button>'
					+ '<h4 class="modal-title" id="header-comment">Help On</h4>'
					+ '</div>'
					+ '<div class="modal-body" style="color:rgb(85, 85, 85);">'
					+ '<div class="table-responsive"> '
					+ '<table id="help_comment_grid" class="table table-striped">'
					+ '<thead>'
					+ '<tr>'
					+ '<th>Code</th>'
					+ '<th>Code Stand For</th>'
					+ '</tr>'
					+ '</thead>'
					+ '<tbody>'
					+ '<tr><td>TPC</td><td>Total Policy to be Collected</td></tr>'
					+ '<tr><td>EPA</td><td>Expected Premium Amount</td></tr>'
					+ '<tr><td>TLP</td><td>Total Lapsed Policy</td></tr>'
					+ '<tr><td>LPA</td><td>Lapsed Premium Amount (Total)</td></tr>'
					+ '</tbody>'
					+ '</table>'
					+ '</div>'
					+ '</div>'
					+ '<div class="modal-footer">'
					+ '<a id="btnCloseDetail" data-toggle="tab" href="#close"'
					+ 'data-dismiss="modal"'
					+ 'style="background-color: #ED1B2E; color: #FFF;"'
					+ 'class="btn"> Close </a>'
					+ '</div>'
					+ '</div>'
					+ '</div>' + '</div>');

			return {
				show : function() {
					$dialog.modal();
				},
				/**
				 * Closes dialog
				 */
				hide : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},

				/**
				 * Destroy dialog
				 */
				destroy : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},
				setType : function(value) {
					type = value;
				}
			};
		})(jQuery);
// =========End Collection And Lapse Help=========

// =========My Sale Performance Update Help=========
var showModalHelpMySalePerformanceDialog = showModalHelpMySalePerformanceDialog
		|| (function($) {
			var $dialog = $('<div class="modal fade modal-collectionandlapse" tabindex="-1" role="dialog" aria-hidden="true">'
					+ '<div class="modal-dialog modal-lg">'
					+ '<div class="modal-content">'
					+ '<div class="modal-header">'
					+ '<button type="button" class="close" data-dismiss="modal">'
					+ '<span aria-hidden="true">×</span>'
					+ '</button>'
					+ '<h4 class="modal-title" id="header-comment">Help On My Sale Perform Update</h4>'
					+ '</div>'
					+ '<div class="modal-body" style="color:rgb(85, 85, 85);">'
					+ '<div class="table-responsive"> '
					+ '<table id="help_comment_grid" class="table table-striped">'
					+ '<thead>'
					+ '<tr>'
					+ '<th>Code</th>'
					+ '<th>Code Stand For</th>'
					+ '</tr>'
					+ '</thead>'
					+ '<tbody>'
					+ '<tr><td>TG.Iss</td><td>Target Issuance of current Month</td></tr>'
					+ '<tr><td>AC.Iss</td><td>Actual Issaunce(MTD)</td></tr>'
					+ '<tr><td>TG.Sub</td><td>Target Submission for current Month</td></tr>'
					+ '<tr><td>SC.Sub</td><td>Actual Submission for Current Month</td></tr>'
					+ '</tbody>'
					+ '</table>'
					+ '</div>'
					+ '</div>'
					+ '<div class="modal-footer">'
					+ '<a id="btnCloseDetail" data-toggle="tab" href="#close"'
					+ 'data-dismiss="modal"'
					+ 'style="background-color: #ED1B2E; color: #FFF;"'
					+ 'class="btn"> Close </a>'
					+ '</div>'
					+ '</div>'
					+ '</div>' + '</div>');

			return {
				show : function() {
					$dialog.modal();
				},
				/**
				 * Closes dialog
				 */
				hide : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},

				/**
				 * Destroy dialog
				 */
				destroy : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},
				setType : function(value) {
					type = value;
				}
			};
		})(jQuery);
// =========End My Sale Performance Update Help=========

// =========My Pending Acknowledgement=========
var showModalPendingAckDialog = showModalPendingAckDialog || (function($) {
			var $dialog = $('<div class="modal fade modal-pendingack" tabindex="-1" role="dialog" aria-hidden="true">'
								+ '<div class="modal-dialog modal-lg">'
									+ '<div class="modal-content">'
										+ '<div class="modal-header">'
											+ '<button type="button" class="close" data-dismiss="modal">'
												+ '<span aria-hidden="true">×</span>'
											+ '</button>'
											+ '<h4 class="modal-title" id="myModalLabel2">My Pending Acknowledgement</h4>'
										+ '</div>'
										+ '<div class="modal-body" style="color:rgb(85, 85, 85);">'
											+ '<div class="table-responsive"> '
												+ '<table id="pendingack_detail_grid" class="table table-striped">'
													+ '<thead>'
														+ '<tr>'
															+ '<th>Application Number</th>'
															+ '<th>Policy Number</th>'
															+ '<th>Policy Name</th>'
															+ '<th>Mobile 1</th>'
															+ '<th>Mobile 2</th>'
															+ '<th>Agent Number</th>'
															+ '<th>Agent Name</th>'
															+ '<th>Supervisor Name</th>'
															+ '<th>Branch Code</th>'
															+ '<th>Branch Name</th>'
															+ '<th>Pending Period</th>'
														+ '</tr>'
													+ '</thead>'
													+ '<tbody>'
													+ '</tbody>'
												+ '</table>'
											+ '</div>'
										+ '</div>'
										+ '<div class="modal-footer">'
											+ '<a id="btnCloseDetail" data-toggle="tab" href="#close"'
												+ 'data-dismiss="modal"'
												+ 'style="background-color: #ED1B2E; color: #FFF;"'
												+ 'class="btn"> Close </a>'
										+ '</div>'
									+ '</div>'
								+ '</div>' 
							+ '</div>');

			return {
				show : function() {
					$.ajax({
							url : urlgetpendingacks,
							type : 'get',
							success : function(result) {
								var dataSet = JSON.parse(result);
								var tabpendingack = $dialog.find('#pendingack_detail_grid').DataTable();
								tabpendingack.destroy();
								tabpendingack = $dialog.find('#pendingack_detail_grid').DataTable({
									dom : 'TC<"clearfix">lfrt<"bottom"ip>',
									data: dataSet,
									"columns" : [ 
									   {
										data : 'chdrNum'
									   }, 
									   {
										data : 'poNnum'
									   }, 
									   {
										data : 'poName'
									   }, 
									   {
										data : 'clntMobile1'
									   }, 
									   {
										data : 'clntMobile2'
									   }, 
									   {
										data : 'agntNum'
									   }, 
									   {
										data : 'agntName'
									   }, 
									   {
										data : 'supName'
									   }, 
									   {
										data : 'branchCode'
									   }, 
									   {
										data : 'branchName'
									   }, 
									   {
										data : 'pendingPeriod'
									   } ],
									"fnDrawCallback" : function(oSettings, json) {
										// showLoading(false);
									}
								});
							}
						});
					$dialog.modal();
				},
				/**
				 * Closes dialog
				 */
				hide : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},

				/**
				 * Destroy dialog
				 */
				destroy : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},
				setType : function(value) {
					type = value;
				}
			};
		})(jQuery);
// =========End My Pending Acknowledgement=========


//=========Customer to Follow Up=========
var showModalCustomerToFollowUpDialog = showModalCustomerToFollowUpDialog || (function($) {
			var $dialog = $('<div class="modal fade modal-pendingack" tabindex="-1" role="dialog" aria-hidden="true">'
								+ '<div class="modal-dialog modal-lg">'
									+ '<div class="modal-content">'
										+ '<div class="modal-header">'
											+ '<button type="button" class="close" data-dismiss="modal">'
												+ '<span aria-hidden="true">×</span>'
											+ '</button>'
											+ '<h4 class="modal-title" id="myModalLabel2">Cutomer to follow-up</h4>'
										+ '</div>'
										+ '<div class="modal-body" style="color:rgb(85, 85, 85);">'
											+ '<div class="table-responsive"> '
												+ '<table style="width:100%;" id="customer_to_followup_grid" class="table table-striped">'
													+ '<thead>'
														+ '<tr>'
															+ '<th>Customer Name</th>'
															+ '<th>Mobile 1</th>'
															+ '<th>Mobile 2</th>'
															+ '<th>Referral Date</th>'
															+ '<th>Appointment Date</th>'
															+ '<th>Agent Name</th>'
														+ '</tr>'
													+ '</thead>'
													+ '<tbody>'
													+ '</tbody>'
												+ '</table>'
											+ '</div>'
										+ '</div>'
										+ '<div class="modal-footer">'
											+ '<a id="btnCloseDetail" data-toggle="tab" href="#close"'
												+ 'data-dismiss="modal"'
												+ 'style="background-color: #ED1B2E; color: #FFF;"'
												+ 'class="btn"> Close </a>'
										+ '</div>'
									+ '</div>'
								+ '</div>' 
							+ '</div>');

			return {
				show : function() {
					$.ajax({
							url : urlgetcustomertofollowup,
							type : 'get',
							success : function(result) {
								var dataSet = JSON.parse(result);
								var tabcustomertofollowup = $dialog.find('#customer_to_followup_grid').DataTable();
								tabcustomertofollowup.destroy();
								tabcustomertofollowup = $dialog.find('#customer_to_followup_grid').DataTable({
									dom : 'TC<"clearfix">lfrt<"bottom"ip>',
									data: dataSet,
									"columns" : [ 
									   {
										data : '0'
									   }, 
									   {
										data : '1'
									   }, 
									   {
										data : '2'
									   }, 
									   {
										data : '3'
									   }, 
									   {
										data : '4'
									   }, 
									   {
										data : '5'
									   }],
									"fnDrawCallback" : function(oSettings, json) {
										// showLoading(false);
									}
								});
							}
						});
					$dialog.modal();
				},
				/**
				 * Closes dialog
				 */
				hide : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},

				/**
				 * Destroy dialog
				 */
				destroy : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},
				setType : function(value) {
					type = value;
				}
			};
		})(jQuery);
// =========End Customer to Follow Up=========

//========= Advance Search of Renewal Premium Collection =========
var showModalAdvanceSearchRenewalPremiumCollectionDialog = showModalAdvanceSearchRenewalPremiumCollectionDialog || (function($) {
			var $dialog = $('<div class="modal fade modal-pendingack" tabindex="-1" role="dialog" aria-hidden="true">'
								+ '<div class="modal-dialog modal-lg">'
									+ '<div class="modal-content">'
										+ '<div class="modal-header">'
											+ '<button type="button" class="close" data-dismiss="modal">'
												+ '<span aria-hidden="true">×</span>'
											+ '</button>'
											+ '<h4 class="modal-title" id="myModalLabel2">Renewal Premium Collection</h4>'
										+ '</div>'
										+ '<div class="modal-body" style="color:rgb(85, 85, 85);">'
											+ '<form method="post" id="colform">'
											+ '<div class="row">'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Banch Code :</label>'
														+'<input type="text" id="colbranchcode" name="colbranchcode" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Banch Name :</label>'
														+'<input type="text" id="colbranchname" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Agent Code :</label>'
														+'<input type="text" id="colagentcode" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Agent Name :</label>'
														+'<input type="text" id="colagentname" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>BCU Status :</label>'
														+'<input type="text" id="colbcustatus" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Agent Status :</label>'
														+'<input type="text" id="colagentstatus" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>BDM code :</label>'
														+'<input type="text" id="colbdmcode" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-8 col-lg-8 col-md-8 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Customer Name :</label>'
														+'<input type="text" id="colcustomername" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Due Date :</label>'
														+'<input type="text" id="colduedate" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" />'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12" id="thcolduedateto">'
													+'<div class="form-group">'
														+'<label>To :</label>'
														+'<input type="text" id="colduedateto" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" />'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Operator :</label>'
														+'<select class="form-control" name="colduedateoperator" id="colduedateoperator">'
															+'<option value="1">=</option>'
															+'<option value="2">&gt;</option>'
															+'<option value="3">&lt;</option>'
															+'<option value="4">&lt;=</option>'
															+'<option value="5">&gt;=</option>'
															+'<option value="6">between</option>'
														+'</select>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-12 col-lg-12 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Saleforce Comment/Update & Remark :</label>'
														+'<input type="text" id="colsaleforcecomment" class="form-control"/>'
													+'</div>'
												+'</div>'
											+ '</div>'
											+ '</form>'
										+ '</div>'
										+ '<div class="modal-footer">'
											+'<button type="button" id="btnreadvancesearch" class="btn btn-primary">Search</button>'
											+ '<a id="btnCloseDetail" data-toggle="tab" href="#close"'
												+ 'data-dismiss="modal" style="background-color: #ED1B2E; color: #FFF;" class="btn"> Close </a>'
										+ '</div>'
									+ '</div>'
								+ '</div>' 
							+ '</div>');

			return {
				show : function() {
					$('#tags_search_box').show();
					if ($dialog.find('#colduedateoperator').val() != '6') {
						$dialog.find('#thcolduedateto').hide();	
					}
					$dialog.modal()
					$dialog.find('#btnreadvancesearch').click(function (e) {
						listpendings = []
						var colbranchcode = $dialog.find('#colbranchcode').val();
						CheckPendingShort(colbranchcode,"BranchCode = ","colbranchcode");
						
						var colbranchname = $dialog.find('#colbranchname').val();
						CheckPendingShort(colbranchname,"BranchName = ","colbranchname");
						
						var colagentcode = $dialog.find('#colagentcode').val();
						CheckPendingShort(colagentcode,"Agent Code = ","colagentcode");
						
						var colagentname = $dialog.find('#colagentname').val();
						CheckPendingShort(colagentname,"Agent Name = ","colagentname");
						
						var colbcustatus = $dialog.find('#colbcustatus').val();
						CheckPendingShort(colbcustatus,"Customer Status = ","colbcustatus");
						
						var colagentstatus = $dialog.find('#colagentstatus').val();
						CheckPendingShort(colagentstatus,"Agent Status = ","colagentstatus");
						
						var colbdmcode = $dialog.find('#colbdmcode').val();
						CheckPendingShort(colbdmcode,"BDM Code = ","colbdmcode");
						
						var colcustomername = $dialog.find('#colcustomername').val();
						CheckPendingShort(colcustomername,"Customer Name = ","colcustomername");
						
						var colsaleforcecomment = $dialog.find('#colsaleforcecomment').val();
						CheckPendingShort(colsaleforcecomment,"Sale force comment = ","colsaleforcecomment");
						
						var colduedate = $dialog.find('#colduedate').val();
						var colduedateto = $dialog.find('#colduedateto').val();
						var colduedateoperator = $dialog.find('#colduedateoperator option:selected').text();
						CheckPending(colduedate,colduedateto,colduedateoperator,"DueDate from ","DueDate ","colduedate","colduedateoperator","colduedateto");
						
						GetCollection(urladvancesearchcollection, true, $dialog.find('form'));
						
						$('.lbl-advance-search').find("label").remove();
						
						$.each(listpendings, function( key, value) {
							onAddTag(value.name , value.param, key);
						});
						
						$dialog.modal('hide');
					});
					
					$dialog.find('#colduedate').datepicker({
						useCurrent: true,
						pickTime: false,
						dateFormat: "dd/mm/yy"
					});
				
					$dialog.find('#colduedateto').datepicker({
						useCurrent: true,
						pickTime: false,
						dateFormat: "dd/mm/yy"
					});
					
					$dialog.find('#colduedateoperator').change(function () {
						if ($('#colduedateoperator').val() == '6') {
							$('#thcolduedateto').show();
						} else {
							$('#thcolduedateto').hide();
						}
					});
				},
				/**
				 * Closes dialog
				 */
				hide : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},

				/**
				 * Destroy dialog
				 */
				destroy : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},
				setType : function(value) {
					type = value;
				}
			};
		})(jQuery);
// ========= End Advance Search of Renewal Premium Collection =========

//========= Advance Search of Lapsed =========
var showModalAdvanceSearchLapsedDialog = showModalAdvanceSearchLapsedDialog || (function($) {
			var $dialog = $('<div class="modal fade modal-pendingack" tabindex="-1" role="dialog" aria-hidden="true">'
								+ '<div class="modal-dialog modal-lg">'
									+ '<div class="modal-content">'
										+ '<div class="modal-header">'
											+ '<button type="button" class="close" data-dismiss="modal">'
												+ '<span aria-hidden="true">×</span>'
											+ '</button>'
											+ '<h4 class="modal-title" id="myModalLabel2">Lapsed</h4>'
										+ '</div>'
										+ '<div class="modal-body" style="color:rgb(85, 85, 85);">'
											+ '<form method="post" id="colform">'
											+ '<div class="row">'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Banch Code :</label>'
														+'<input type="text" id="labranchcode" name="labranchcode" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Banch Name :</label>'
														+'<input type="text" id="labranchname" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Agent Code :</label>'
														+'<input type="text" id="laagentcode" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Agent Name :</label>'
														+'<input type="text" id="laagentname" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>BCU Status :</label>'
														+'<input type="text" id="labcustatus" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Agent Status :</label>'
														+'<input type="text" id="laagentstatus" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>BDM code :</label>'
														+'<input type="text" id="labdmcode" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-8 col-lg-8 col-md-8 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Customer Name :</label>'
														+'<input type="text" id="lacustomername" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Due Date :</label>'
														+'<input type="text" id="laduedate" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" />'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12" id="thladuedateto">'
													+'<div class="form-group">'
														+'<label>To :</label>'
														+'<input type="text" id="laduedateto" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" />'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Operator :</label>'
														+'<select class="form-control" name="laduedateoperator" id="laduedateoperator">'
															+'<option value="1">=</option>'
															+'<option value="2">&gt;</option>'
															+'<option value="3">&lt;</option>'
															+'<option value="4">&lt;=</option>'
															+'<option value="5">&gt;=</option>'
															+'<option value="6">between</option>'
														+'</select>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-12 col-lg-12 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Saleforce Comment/Update & Remark :</label>'
														+'<input type="text" id="lasaleforcecomment" class="form-control"/>'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Lapsed Duration</label>'
														+'<input type="text" id="laduration" class="form-control" />'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Is Impact Persistency</label>'
														+'<input type="text" id="laisimpactpersistency" class="form-control" />'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Lapsed Reason </label>'
														+'<input type="text" id="lareason" class="form-control" />'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Lapsed Date :</label>'
														+'<input type="text" id="ladate" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" />'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12" id="thladateto">'
													+'<div class="form-group">'
														+'<label>To :</label>'
														+'<input type="text" id="ladateto" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" class="form-control" />'
													+'</div>'
												+'</div>'
												+'<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-xs-12">'
													+'<div class="form-group">'
														+'<label>Operator :</label>'
														+'<select class="form-control" name="ladateoperator" id="ladateoperator">'
															+'<option value="1">=</option>'
															+'<option value="2">&gt;</option>'
															+'<option value="3">&lt;</option>'
															+'<option value="4">&lt;=</option>'
															+'<option value="5">&gt;=</option>'
															+'<option value="6">between</option>'
														+'</select>'
													+'</div>'
												+'</div>'
											+ '</div>'
											+ '</form>'
										+ '</div>'
										+ '<div class="modal-footer">'
											+'<button type="button" id="btnreadvancesearchla" class="btn btn-primary">Search</button>'
											+ '<a id="btnCloseDetail" data-toggle="tab" href="#close"'
												+ 'data-dismiss="modal" style="background-color: #ED1B2E; color: #FFF;" class="btn"> Close </a>'
										+ '</div>'
									+ '</div>'
								+ '</div>' 
							+ '</div>');

			return {
				show : function() {
					$('#tags_search_box').show();
					if ($dialog.find('#laduedateoperator').val() != '6') {
						$dialog.find('#thladuedateto').hide();	
					}
					if ($dialog.find('#ladateoperator').val() != '6') {
						$dialog.find('#thladateto').hide();	
					}
					$dialog.modal()
					$dialog.find('#btnreadvancesearchla').click(function (e) {
						listpendingsla = []
						var labranchcode = $dialog.find('#labranchcode').val();
						CheckPendingShortLa(labranchcode,"BranchCode = ","labranchcode");
						
						var labranchname = $dialog.find('#labranchname').val();
						CheckPendingShortLa(labranchname,"BranchName = ","labranchname");
						
						var laagentcode = $dialog.find('#laagentcode').val();
						CheckPendingShortLa(laagentcode,"Agent Code = ","laagentcode");
						
						var laagentname = $dialog.find('#laagentname').val();
						CheckPendingShortLa(laagentname,"Agent Name = ","laagentname");
						
						var labcustatus = $dialog.find('#labcustatus').val();
						CheckPendingShortLa(labcustatus,"Customer Status = ","labcustatus");
						
						var laagentstatus = $dialog.find('#laagentstatus').val();
						CheckPendingShortLa(laagentstatus,"Agent Status = ","laagentstatus");
						
						var labdmcode = $dialog.find('#labdmcode').val();
						CheckPendingShortLa(labdmcode,"BDM Code = ","labdmcode");
						
						var lacustomername = $dialog.find('#lacustomername').val();
						CheckPendingShortLa(lacustomername,"Customer Name = ","lacustomername");
						
						var lasaleforcecomment = $dialog.find('#lasaleforcecomment').val();
						CheckPendingShortLa(lasaleforcecomment,"Sale force comment = ","lasaleforcecomment");
						
						var laduedate = $dialog.find('#laduedate').val();
						var laduedateto = $dialog.find('#laduedateto').val();
						var laduedateoperator = $dialog.find('#laduedateoperator option:selected').text();
						CheckPendingLa(laduedate,laduedateto,laduedateoperator,"DueDate from ","DueDate ","laduedate","laduedateoperator","laduedateto");
						
						var laduration = $dialog.find('#laduration').val();
						CheckPendingShortLa(laduration,"Lapased Duration = ","laduration");
						
						var laisimpactpersistency = $dialog.find('#laisimpactpersistency').val();
						CheckPendingShortLa(laisimpactpersistency,"Is Impact Persistency = ","laisimpactpersistency");
						
						var lareason = $dialog.find('#lareason').val();
						CheckPendingShortLa(lareason,"Lapsed Reason = ","lareason");
						
						var ladate = $dialog.find('#ladate').val();
						var ladateto = $dialog.find('#ladateto').val();
						var ladateoperator = $dialog.find('#ladateoperator option:selected').text();
						CheckPendingLa(ladate,ladateto,ladateoperator,"Lapsed Date from ","Lapsed Date ","ladate","ladateto","ladateoperator");
						
						GetLapsed(urladvancesearchlapsed, true, $dialog.find('form'));
						
						$('.lbl-advance-searchla').find("label").remove();
						
						$.each(listpendingsla, function( key, value) {
							onAddTagLa(value.name , value.param, key + 'la');
						});
						
						$dialog.modal('hide');
					});
					
					$dialog.find('#laduedate').datepicker({
						useCurrent: true,
						pickTime: false,
						dateFormat: "dd/mm/yy"
					});
				
					$dialog.find('#laduedateto').datepicker({
						useCurrent: true,
						pickTime: false,
						dateFormat: "dd/mm/yy"
					});
					
					$dialog.find('#ladate').datepicker({
						useCurrent: true,
						pickTime: false,
						dateFormat: "dd/mm/yy"
					});
				
					$dialog.find('#ladateto').datepicker({
						useCurrent: true,
						pickTime: false,
						dateFormat: "dd/mm/yy"
					});
					
					$dialog.find('#laduedateoperator').change(function () {
						if ($('#laduedateoperator').val() == '6') {
							$('#thladuedateto').show();
						} else {
							$('#thladuedateto').hide();
						}
					});
					
					$dialog.find('#ladateoperator').change(function () {
						if ($('#ladateoperator').val() == '6') {
							$('#thladateto').show();
						} else {
							$('#thladateto').hide();
						}
					});
				},
				/**
				 * Closes dialog
				 */
				hide : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},

				/**
				 * Destroy dialog
				 */
				destroy : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},
				setType : function(value) {
					type = value;
				}
			};
		})(jQuery);
// ========= End Advance Search of Lapsed =========

//=========Fail Welcome Call=========
var showModalFailedWelcomeCall = showModalFailedWelcomeCall
		|| (function($) {
			var $dialog = $('<div class="modal fade modal-collectionandlapse" tabindex="-1" role="dialog" aria-hidden="true">'
					+ '<div class="modal-dialog modal-lg">'
					+ '<div class="modal-content">'
					+ '<div class="modal-header">'
					+ '<button type="button" class="close" data-dismiss="modal">'
					+ '<span aria-hidden="true">×</span>'
					+ '</button>'
					+ '<h4 class="modal-title" id="myModalLabel2">List of Failed Welcome Call (FWC)</h4>'
					+ '</div>'
					+ '<div class="modal-body" style="color:rgb(85, 85, 85);">'
					+ '<div class="table-responsive"> '
					+ '<table id="fwc_grid" class="table table-striped">'
					+ '<thead>'
					+ '<tr>'
					+ '<th>Application Number</th>'
					+ '<th>Policy Number</th>'
					+ '<th>Superivor Name</th>'
					+ '<th>Agent Code</th>'
					+ '<th>Agent Name</th>'
					+ '<th>Call Center Called Date</th>'
					+ '<th>SMS Policy Date</th>'
					+ '<th>Remark From Call Center</th>'
					+ '<th>Called Status</th>'
					+ '<th>Final Called Status</th>'
					+ '<th>Branch Code</th>'
					+ '<th>Branch Name</th>'
					+ '<th>Customer Name</th>'
					+ '<th>Phone Number (1)</th>'
					+ '<th>Phone Number (2)</th>'
					+ '<th>Saleforce Feedback</th>'
					+ '<th>Feedback</th>'
					+ '</tr>'
					+ '</thead>'
					+ '<tbody>'
					+ '</tbody>'
					+ '</table>'
					+ '</div>'
					+ '</div>'
					+ '<div class="modal-footer">'
					+ '<a id="btnCloseDetail" data-toggle="tab" href="#close"'
					+ 'data-dismiss="modal"'
					+ 'style="background-color: #ED1B2E; color: #FFF;"'
					+ 'class="btn"> Close </a>'
					+ '</div>'
					+ '</div>'
					+ '</div>' + '</div>');

			return {
				show : function(params) {
					$.ajax({
						url : urlfailwelcomecall,
						success : function(result) {
							var dataSet = JSON.parse(result);
							obj = [];
							if (params === 'Closed') {
								$.each(dataSet, function(key, value) {
									if (value.finalCalledStatus === 'Closed') {
										obj.push(dataSet[key]);
									}
								});
							} else {
								$.each(dataSet, function(key, value) {
									if (value.finalCalledStatus === 'Pending') {
										obj.push(dataSet[key]);
									}
								});
							}
							var tabfwc = $dialog.find('#fwc_grid').DataTable();
							tabfwc.destroy();
							tabfwc = $dialog.find('#fwc_grid').DataTable({
								"lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
								dom : 'TC<"clearfix">lfrt<"bottom"ip>',
								data: obj,
								"columns" : [ 
								   {
									data : 'chdrAppNum'
								   }, 
								   {
									data : 'chdrNum'
								   }, 
								   {
									data : 'supvName'
								   },
								   {
									   data : 'agntNum'
								   }, 
								   {
									data : 'agntName'
								   }, 
								   {
									data : 'ccCalledDate'
								   }, 
								   {
									data : 'smsPoDate'
								   }, 
								   {
									data : 'ccRemark'
								   }, 
								   {
									data : 'calledStatus'
								   }, 
								   {
									data : 'finalCalledStatus'
								   }, 
								   {
									data : 'branchCode'
								   },
								   {
									data : 'branchName'
								   },
								   {
									data : 'poName'
								   },
								   {
									   data: 'poPhone1'
								   },
								   {
									   data: 'poPhone2'
								   },
								   {
									   data: 'saleForceComment'
								   },
								   {
									   data: '',
									   defaultContent: '<a style="cursor:pointer;" class="addcomment"><span class="label label-warning"><i class="fa fa-plus" aria-hidden="true"></i></span></a>'
								   }
								   ],
								   columnDefs: [
										{
											targets: 0,
											createdCell: function (td, cellData, rowData, row, col) {
												$(td).addClass('chdrAppnum');													
											}
										},
										{
											targets: 1,
											createdCell: function (td, cellData, rowData, row, col) {
												$(td).addClass('chdrNum');
											}
										},
								      {
								    	  targets: -1,
								    	  createdCell: function (td, cellData, rowData, row, col) {
								    		  var chdrNum = $(td).parent().find('.chdrNum').text().trim();
								    		  var chdrAppnum = $(td).parent().find('.chdrAppnum').text().trim();
								    		  var object_key = chdrNum + chdrAppnum;
								    		  $(td).find('.addcomment').click(function() {
								    			  showModalCollectionandLapseCommentDialog.show('Fail Welcome Call Comment',object_key,'FWC',urlgetcollectionandlapsecomment);
								    		  });
								    	  }
								      }
								      
								   ],
								"fnDrawCallback" : function(oSettings, json) {
									// showLoading(false);
								}
							});
						}
					});
					$dialog.modal();
				},
				/**
				 * Closes dialog
				 */
				hide : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},

				/**
				 * Destroy dialog
				 */
				destroy : function() {
					$dialog.modal('hide');
					$dialog.modal('destroy').remove();
				},
				setType : function(value) {
					type = value;
				}
			};
		})(jQuery);
// =========End-Faild Welcome Call=========

function pendingReasonClick(ele){
	var trigger = $(ele).attr('data-display-trigger');
	var text = $(ele).attr('data-display');

	if(trigger === "short"){
		$(ele).attr('data-display', $(ele).parent().children().first().text().trim());
		$(ele).attr('data-display-trigger', 'long');
		$(ele).parent().children().first().html(text);	
		$(ele).html('more');
	}else{
		$(ele).attr('data-display', $(ele).parent().children().first().text().trim());
		$(ele).attr('data-display-trigger', 'short');
		$(ele).parent().children().first().html(text);
		$(ele).html('   less');
	}
}