## Setup Jboss Datasource
* Name: dp_postgres 
* JNDI: java:jboss/datasources/dp_postgres 
* Driver: postgresql-9.4.1209.jre6.jar 
* Driver Class: org.postgresql.Driver 
* Connection URL: jdbc:postgresql://localhost:5432/db_name

## Setup Maven
* How to install maven : https://www.mkyong.com/maven/how-to-install-maven-in-windows/
# How to install Maven
* Right click on project in eclipse and then select on maven click on install maven

## Deploy to Jboss
* Add Server Jboss EAP 6.4 and then add the web project install the server
* Click to start the JBoss EAP

## How to restore Postgres
# Before restore need to create role as below :
* pruquote
* eaps