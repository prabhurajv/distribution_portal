
-- alter table DP_DB..TEMP_RenewalLapse
-- add CALL_DATE date;

-- alter table DP_DB..TEMP_RenewalLapse
-- add POCDN  varchar(30);


alter table ap.tabcollection
add column podcsn  character varying(30); -- po decision
alter table ap.tabcollection
add column call_date date

Insert into ap.tabitemitem values (868, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'PFWD|LA',null, 1, '340830', NOW());
Insert into ap.tabitemitem values (869, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'IPCD|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (870, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'RCPCD|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (871, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'RCPAD|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (872, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'IPAD|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (873, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'ILWR|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (874, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'RCLWR|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (875, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'RCPAR|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (876, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'IPAR|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (877, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'AR|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (878, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'CFACLD|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (879, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'CFUCLD|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (880, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'SKWD|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (881, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'WD|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (882, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'WR|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (883, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'PFWR|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (884, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'SKWR|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (885, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'CFAWRE|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (886, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'CFUWRE|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (887, 29, 'PO will reinstate (Date < a month)', 'PO will reinstate', 'ADEP|LA', null, 1, '340830', NOW());

Insert into ap.tabitemitem values (888, 29, 'PO will deposit (Date < a month)', 'PO will deposit', 'PFWD|IF', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (889, 29, 'PO will deposit (Date < a month)', 'PO will deposit', 'SKWD|IF', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (890, 29, 'PO will deposit (Date < a month)', 'PO will deposit', 'IPCD|IF', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (891, 29, 'PO will deposit (Date < a month)', 'PO will deposit', 'RCPCD|IF', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (892, 29, 'PO will deposit (Date < a month)', 'PO will deposit', 'CFACLD|IF', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (893, 29, 'PO will deposit (Date < a month)', 'PO will deposit', 'CFUCLD|IF', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (894, 29, 'PO will deposit (Date < a month)', 'PO will deposit', 'SKWD|IF', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (895, 29, 'PO will deposit (Date < a month)', 'PO will deposit', 'WD|IF', null, 1, '340830', NOW());

Insert into ap.tabitemitem values (896, 29, 'PO said already deposited (Date < a month)', 'PO said already deposited', 'RCPAD|IF', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (897, 29, 'PO said already deposited (Date < a month)', 'PO said already deposited', 'IPAD|IF', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (898, 29, 'PO said already deposited (Date < a month)', 'PO said already deposited', 'ADEP|IF', null, 1, '340830', NOW());

Insert into ap.tabitemitem values (899, 29, 'PO already reinstated (Date < a month)', 'PO already reinstated', 'RCPAR|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (900, 29, 'PO already reinstated (Date < a month)', 'PO already reinstated', 'IPAR|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (901, 29, 'PO already reinstated (Date < a month)', 'PO already reinstated', 'AR|LA', null, 1, '340830', NOW());
Insert into ap.tabitemitem values (902, 29, 'PO already reinstated (Date < a month)', 'PO already reinstated', 'ARIN|LA', null, 1, '340830', NOW());



 DROP FUNCTION ap.spsearch_collection(character varying);
 
CREATE OR REPLACE FUNCTION ap.spsearch_collection(IN userid character varying)
  RETURNS TABLE(obj_key text, obj_type text, chdrnum character varying, chdr_appnum character varying, po_name character varying, gender character varying, clnt_mobile1 text, clnt_mobile2 text, clnt_mobile3 text, chdr_p_with_tax numeric, chdr_billfreq character varying, chdr_due_date date, chdr_graceperiod integer, agntnum character varying, agnt_name character varying, agnt_supcode character varying, agnt_supname character varying, chdr_age integer, cc_status character varying, agnt_status character varying, chdr_ape numeric, chdr_lapse_date date, chdr_status character varying, is_impact_persistency character varying, lapsed_reason character varying, bcu_remark text, customer_response text, branch_code character varying, branch_name character varying, cc_remark text, cc_by character varying, cc_date timestamp without time zone, bdmcode character varying, bdmname character varying, sbdmcode character varying, sbdmname character varying, rbdmcode character varying, rbdmname character varying, salezone character varying, product character varying, sub_lapsed_reason character varying, occ_date date, tele_reinstate character varying, out_amt character varying, next_prem character varying, payment_method character varying, next_payment_date character varying, stnd_ord_end_date character varying, bank_acc character varying, so_cancel_status character varying, ben character varying, po_occ character varying, val_la_and_su integer, ape_exc_tax_chg numeric, podcsn character varying, call_date date) AS
$BODY$
BEGIN
	create temporary table tmpagnts on commit drop as
	select * from ap.splist_heirarchies(userid);

	RETURN QUERY
	select cl.obj_key
	,cl.obj_type
	,cl.chdrnum
	,cl.chdr_appnum
	,cl.po_name
	,cl.gender 	
	,(CASE WHEN LEFT(cl.clnt_mobile1, 1) = '0' THEN CONCAT('(855)', RIGHT(cl.clnt_mobile1,LENGTH(cl.clnt_mobile1)-1)) WHEN cl.clnt_mobile1 is not null THEN CONCAT('(855)', cl.clnt_mobile1) ELSE '' END) as clnt_mobile1
 	,(CASE WHEN LEFT(cl.clnt_mobile2, 1) = '0' THEN CONCAT('(855)', RIGHT(cl.clnt_mobile2,LENGTH(cl.clnt_mobile2)-1)) WHEN cl.clnt_mobile2 is not null THEN  CONCAT('(855)', cl.clnt_mobile2) ELSE '' END) as clnt_mobile2
	,(CASE WHEN LEFT(cl.clnt_mobile3, 1) = '0' THEN CONCAT('(855)', RIGHT(cl.clnt_mobile3,LENGTH(cl.clnt_mobile2)-1)) WHEN cl.clnt_mobile3 is not null THEN  CONCAT('(855)', cl.clnt_mobile3) ELSE '' END) as clnt_mobile3
	,cl.chdr_p_with_tax
	,cl.chdr_billfreq
	,cl.chdr_due_date
	,cl.chdr_graceperiod
	,cl.agntnum
	,cl.agnt_name
	,cl.agnt_supcode
	,cl.agnt_supname
	,cl.chdr_age
	,cl.cc_status
	,cl.agnt_status
	,cl.chdr_ape
	,cl.chdr_lapse_date
	,cl.chdr_status
	,cl.is_impact_persistency
	,cl.lapsed_reason
	,cl.bcu_remark
	,cl.customer_response
	,cl.branch_code
	,cl.branch_name
	,com.cc_text
	,com.cc_by
	,com.cc_date
	,cl.bdmcode
	,cl.bdmname
	,cl.sbdmcode
	,cl.sbdmname
	,cl.rbdmcode
	,cl.rbdmname
	,cl.salezone
	,cl.product
	,cl.sub_lapsed_reason
	,cl.occ_date
	,cl.tele_reinstate
	,cl.out_amt
	,cl.next_prem
	,cl.payment_method
	,(CASE WHEN cl.next_payment_date = '1900-01-01' THEN '' ELSE cl.next_payment_date END) as  next_payment_date
	,cl.stnd_ord_end_date
	,(CASE WHEN cl.bank_acc is null or cl.bank_acc = '' then ''
	     WHEN LENGTH(cl.bank_acc) < 4 then  cl.bank_acc
	     WHEN LENGTH(cl.bank_acc) >= 6 and LENGTH(cl.bank_acc) <= 7 then Concat(SUBSTRING(cl.bank_acc, 1, LENGTH(cl.bank_acc)-5) ,repeat('*',  3), SUBSTRING(cl.bank_acc, LENGTH(cl.bank_acc)-1, LENGTH(cl.bank_acc))) 
	     WHEN LENGTH(cl.bank_acc) >= 8 and LENGTH(cl.bank_acc) <= 10 then Concat(SUBSTRING(cl.bank_acc, 1, LENGTH(cl.bank_acc)-6) ,repeat('*',  4), SUBSTRING(cl.bank_acc, LENGTH(cl.bank_acc)-1, LENGTH(cl.bank_acc)))
	     WHEN LENGTH(cl.bank_acc) >= 11 and LENGTH(cl.bank_acc) <= 17 then Concat(SUBSTRING(cl.bank_acc, 1, LENGTH(cl.bank_acc)-8) ,repeat('*',  6), SUBSTRING(cl.bank_acc, LENGTH(cl.bank_acc)-1, LENGTH(cl.bank_acc)))
	      end) as bank_acc  
	,cl.so_cancel_status
	,cl.ben
	,cl.po_occ
	,cl.val_la_and_su
	,cl.ape_exc_tax_chg
	,cl.podcsn
	,cl.call_date
	from ap.tabcollection cl
	inner join tmpagnts b
	on cl.agntnum = b.agntnum
	left join (
		 SELECT a.tab_key, a.obj_type, a.cc_date , a.cc_text, a.cc_by
		, ROW_NUMBER() OVER(PARTITION BY a.tab_key, a.obj_type ORDER BY a.cc_id DESC) AS rownum
		FROM ap.tabcomment a
	) com
	on cl.obj_key = com.tab_key
	AND cl.obj_type = com.obj_type
	AND com.rownum = 1;

-- 	SELECT * FROM ap.spsearch_collection('281549')
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ap.spsearch_collection(character varying)
  OWNER TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO public;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO pruquotedb_svc;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO pruquotedb_supp;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO pruquotedb_deploy;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO pruquote_svc;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO pruquote_supp;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO pruquote_deploy;

-- Function: ap.splist_actions(character varying)
-- Function: ap.splist_actions(character varying)

-- DROP FUNCTION ap.splist_actions(character varying);

CREATE OR REPLACE FUNCTION ap.splist_actions(IN userid character varying)
  RETURNS TABLE(action_cat character varying, total_pol integer, number_of_day integer, status_code character varying, sort integer, code character varying) AS
$BODY$
		DECLARE
				days_ntu integer;
				days_cal integer;
				days_me integer;
				days_is integer;
				days_ia integer;
				days_app integer;
				todo_templateIA text;
				todo_templateIS text;
				todo_templateME text;
				todo_templateCAL text;
				startntu date;
				endntu date;
				--ntuday text;
		BEGIN
				days_ntu = (select reserve2 from ap.tabitemitem where cotabl = 27 and reserve1 = 'DuNTU')::integer;
				days_cal = (select reserve2 from ap.tabitemitem where cotabl = 27 and reserve1 = 'DaCAL')::integer;
				days_me = (select reserve2 from ap.tabitemitem where cotabl = 27 and reserve1 = 'DaME')::integer;
				days_is = (select reserve2 from ap.tabitemitem where cotabl = 27 and reserve1 = 'DaIS')::integer;
				days_ia = (select reserve2 from ap.tabitemitem where cotabl = 27 and reserve1 = 'DaIA')::integer;
				days_app = (select reserve2 from ap.tabitemitem where cotabl = 27 and reserve1 = 'DuAPP')::integer;
				--todo_templateIA = (select reserve2 from ap.tabitemitem where cotabl = 27 and reserve1 = 'ToDoIA')::text;
				--todo_templateIS = (select reserve2 from ap.tabitemitem where cotabl = 27 and reserve1 = 'ToDoIS')::text;
				--todo_templateME = (select reserve2 from ap.tabitemitem where cotabl = 27 and reserve1 = 'ToDoME')::text;
				--todo_templateCAL = (select reserve2 from ap.tabitemitem where cotabl = 27 and reserve1 = 'ToDoCAL')::text;
				startntu=now()::date;
				--ntuday = days_ntu::integer + ' day';
				endntu = startntu +  days_ntu * INTERVAL  '1 day';
				
				create temporary table tmppendings on commit drop as
				select * from ap.spsearch_pending(userid);

				-- Enhance enhance immediate action 14-02-2019 by Malen Sok
				create temporary table tmp_collection_lapse on commit drop as
				select cl.* from ap.tabcollection cl inner join tmpagnts b on cl.agntnum = b.agntnum;
				
				RETURN QUERY
				--select * from tmppendings
				-- ME
				 Select
				'Not Taken Up'::character varying as Action_Cat  ,
				count(*)::integer as Total_Pol,
				days_ntu as Number_of_day,
				'NTUDATE'::character varying as Status_Code,
				1 as sort
				,'NTU'::character varying  as code
				from tmppendings tm where tm.ntu_date between startntu and endntu
				union
				Select
				'Medical Examination'::character varying as Action_Cat  ,
				count(*)::integer as Total_Pol,
				days_me as Number_of_day,
				'ME'::character varying as Status_Code,
				2 as sort,
				'ME'::character varying  as code
				from tmppendings tm where tm.pending_period<=days_me and left(tm.status,2) = 'ME'
				union
				Select
				'Incomplete Application'::character varying as Action_Cat  ,
				count(*)::integer as Total_Pol,
				days_ia as Number_of_day,
				'IA'::character varying as Status_Code,
				3 as sort,
				'IA'::character varying  as code
				from tmppendings tm where tm.pending_period<=days_ia and left(tm.status,2) = 'IA'
				union
				Select
				'Conditional Acceptance Letter(CAL)'::character varying as Action_Cat  ,
				count(*)::integer as Total_Pol,
				days_cal as Number_of_day,
				'CAL'::character varying as Status_Code,
				4 as sort,
				'CAL'::character varying  as code
				from tmppendings tm where tm.pending_period<=days_cal and left(tm.status,3) = 'CAL'
					union
				Select
				'Inquiring for suppliment information(IS)'::character varying as Action_Cat  ,
				count(*)::integer as Total_Pol,
				days_is as Number_of_day,
				'IS'::character varying as Status_Code,
				5 as sort,
				'IS'::character varying  as code
				from tmppendings tm where tm.pending_period<=days_is and left(tm.status,2) = 'IS'
				union
				select 'Customer to follow-up'::character varying as Action_Cat
				, count(*)::integer as Total_Ref
				, days_app as Number_of_day
				, 'REFFLUP'::character varying as Status_Code
				, 6 as sort
				,'REFFLUP'::character varying  as code
						from ap.tabreferal a
						inner join tmpagnts b
						on a.agntnum = b.agntnum
						where (a.appdate::date - now()::date) >= 0 and (a.appdate::date - now()::date) <= days_app
						

				-- Enhance enhance immediate action 14-02-2019 by Malen Sok
				union
				select i.itemkhmer as Action_Cat,
					(select count(*)   from ap.tabcollection cl inner join tmpagnts b on cl.agntnum = b.agntnum where podcsn=split_part(i.reserve1, '|', 1) 
								and chdr_status like ('%' || split_part(i.reserve1, '|', 2) || '%')
								and call_date is not NULL and extract(year from age(call_date, now()))*12 + extract(month from age(call_date, now())) = 0 -- need to return within a month
					)::integer as Total_Pol, -- NOTE reserve1 First sub string is Call reason code and second is policy status argument
					0::integer as Number_of_day,
					split_part(i.reserve1, '|', 2)::character varying as Status_Code,
					7 as sort,
					split_part(i.reserve1, '|', 1)::character varying as code
				from ap.tabitemitem i where i.cotabl=29 and i.validflag=1;
		--             To test
		--             SELECT * FROM ap.splist_actions('336758')
		END;
		$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ap.splist_actions(character varying)
  OWNER TO pruquote;
GRANT EXECUTE ON FUNCTION ap.splist_actions(character varying) TO public;
GRANT EXECUTE ON FUNCTION ap.splist_actions(character varying) TO pruquote;
GRANT EXECUTE ON FUNCTION ap.splist_actions(character varying) TO pruquotedb_svc;
GRANT EXECUTE ON FUNCTION ap.splist_actions(character varying) TO pruquotedb_supp;
GRANT EXECUTE ON FUNCTION ap.splist_actions(character varying) TO pruquotedb_deploy;
GRANT EXECUTE ON FUNCTION ap.splist_actions(character varying) TO pruquote_svc;
GRANT EXECUTE ON FUNCTION ap.splist_actions(character varying) TO pruquote_supp;
GRANT EXECUTE ON FUNCTION ap.splist_actions(character varying) TO pruquote_deploy;
GRANT EXECUTE ON FUNCTION ap.splist_actions(character varying) TO postgres;
