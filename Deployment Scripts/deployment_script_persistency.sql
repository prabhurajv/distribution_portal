alter table ap.tabcollection add po_occ character varying(100);
alter table ap.tabcollection add val_la_and_su integer;
alter table ap.tabcollection add ape_exc_tax_chg numeric(10,2);

alter table ap.tabcollection
alter column payment_method type character varying(30);

alter table ap.tabcollection
alter column is_impact_persistency type character varying(50);
  
-- Function: ap.spsearch_collection(character varying)

-- DROP FUNCTION ap.spsearch_collection(character varying);

-- 	,(CASE WHEN LEFT(cl.clnt_mobile1, 1) = '0' THEN CONCAT('(855)', RIGHT(cl.clnt_mobile1,LENGTH(cl.clnt_mobile1)-1)) ELSE CONCAT('(855)', cl.clnt_mobile1) END) as clnt_mobile1
-- 	,(CASE WHEN LEFT(cl.clnt_mobile2, 1) = '0' THEN CONCAT('(855)', RIGHT(cl.clnt_mobile2,LENGTH(cl.clnt_mobile2)-1)) ELSE CONCAT('(855)', cl.clnt_mobile2) END) as clnt_mobile2

	CREATE OR REPLACE FUNCTION ap.spsearch_collection(IN userid character varying)
  RETURNS TABLE(obj_key text, obj_type text, chdrnum character varying, chdr_appnum character varying, po_name character varying, gender character varying, clnt_mobile1 text, clnt_mobile2 text, clnt_mobile3 text, chdr_p_with_tax numeric, chdr_billfreq character varying, chdr_due_date date, chdr_graceperiod integer, agntnum character varying, agnt_name character varying, agnt_supcode character varying, agnt_supname character varying, chdr_age integer, cc_status character varying, agnt_status character varying, chdr_ape numeric, chdr_lapse_date date, chdr_status character varying, is_impact_persistency character varying, lapsed_reason character varying, bcu_remark text, customer_response text, branch_code character varying, branch_name character varying, cc_remark text, cc_by character varying, cc_date timestamp without time zone, bdmcode character varying, bdmname character varying, sbdmcode character varying, sbdmname character varying, rbdmcode character varying, rbdmname character varying, salezone character varying, product character varying, sub_lapsed_reason character varying, occ_date date, tele_reinstate character varying, out_amt character varying, next_prem character varying, payment_method character varying, next_payment_date character varying, stnd_ord_end_date character varying, bank_acc character varying, so_cancel_status character varying, ben character varying, po_occ character varying, val_la_and_su integer, ape_exc_tax_chg numeric) AS
$BODY$
BEGIN
	create temporary table tmpagnts on commit drop as
	select * from ap.splist_heirarchies(userid);

	RETURN QUERY
	select cl.obj_key
	,cl.obj_type
	,cl.chdrnum
	,cl.chdr_appnum
	,cl.po_name
	,cl.gender 	
	,(CASE WHEN LEFT(cl.clnt_mobile1, 1) = '0' THEN CONCAT('(855)', RIGHT(cl.clnt_mobile1,LENGTH(cl.clnt_mobile1)-1)) WHEN cl.clnt_mobile1 is not null THEN CONCAT('(855)', cl.clnt_mobile1) ELSE '' END) as clnt_mobile1
 	,(CASE WHEN LEFT(cl.clnt_mobile2, 1) = '0' THEN CONCAT('(855)', RIGHT(cl.clnt_mobile2,LENGTH(cl.clnt_mobile2)-1)) WHEN cl.clnt_mobile2 is not null THEN  CONCAT('(855)', cl.clnt_mobile2) ELSE '' END) as clnt_mobile2
	,(CASE WHEN LEFT(cl.clnt_mobile3, 1) = '0' THEN CONCAT('(855)', RIGHT(cl.clnt_mobile3,LENGTH(cl.clnt_mobile2)-1)) WHEN cl.clnt_mobile3 is not null THEN  CONCAT('(855)', cl.clnt_mobile3) ELSE '' END) as clnt_mobile3
	,cl.chdr_p_with_tax
	,cl.chdr_billfreq
	,cl.chdr_due_date
	,cl.chdr_graceperiod
	,cl.agntnum
	,cl.agnt_name
	,cl.agnt_supcode
	,cl.agnt_supname
	,cl.chdr_age
	,cl.cc_status
	,cl.agnt_status
	,cl.chdr_ape
	,cl.chdr_lapse_date
	,cl.chdr_status
	,cl.is_impact_persistency
	,cl.lapsed_reason
	,cl.bcu_remark
	,cl.customer_response
	,cl.branch_code
	,cl.branch_name
	,com.cc_text
	,com.cc_by
	,com.cc_date
	,cl.bdmcode
	,cl.bdmname
	,cl.sbdmcode
	,cl.sbdmname
	,cl.rbdmcode
	,cl.rbdmname
	,cl.salezone
	,cl.product
	,cl.sub_lapsed_reason
	,cl.occ_date
	,cl.tele_reinstate
	,cl.out_amt
	,cl.next_prem
	,cl.payment_method
	,(CASE WHEN cl.next_payment_date = '1900-01-01' THEN '' ELSE cl.next_payment_date END) as  next_payment_date
	,cl.stnd_ord_end_date
	,(CASE WHEN cl.bank_acc is null or cl.bank_acc = '' then ''
	     WHEN LENGTH(cl.bank_acc) < 4 then  cl.bank_acc
	     WHEN LENGTH(cl.bank_acc) >= 6 and LENGTH(cl.bank_acc) <= 7 then Concat(SUBSTRING(cl.bank_acc, 1, LENGTH(cl.bank_acc)-5) ,repeat('*',  3), SUBSTRING(cl.bank_acc, LENGTH(cl.bank_acc)-1, LENGTH(cl.bank_acc))) 
	     WHEN LENGTH(cl.bank_acc) >= 8 and LENGTH(cl.bank_acc) <= 10 then Concat(SUBSTRING(cl.bank_acc, 1, LENGTH(cl.bank_acc)-6) ,repeat('*',  4), SUBSTRING(cl.bank_acc, LENGTH(cl.bank_acc)-1, LENGTH(cl.bank_acc)))
	     WHEN LENGTH(cl.bank_acc) >= 11 and LENGTH(cl.bank_acc) <= 17 then Concat(SUBSTRING(cl.bank_acc, 1, LENGTH(cl.bank_acc)-8) ,repeat('*',  6), SUBSTRING(cl.bank_acc, LENGTH(cl.bank_acc)-1, LENGTH(cl.bank_acc)))
	      end) as bank_acc  
	,cl.so_cancel_status
	,cl.ben
	,cl.po_occ
	,cl.val_la_and_su
	,cl.ape_exc_tax_chg
	from ap.tabcollection cl
	inner join tmpagnts b
	on cl.agntnum = b.agntnum
	left join (
		 SELECT a.tab_key, a.obj_type, a.cc_date , a.cc_text, a.cc_by
		, ROW_NUMBER() OVER(PARTITION BY a.tab_key, a.obj_type ORDER BY a.cc_id DESC) AS rownum
		FROM ap.tabcomment a
	) com
	on cl.obj_key = com.tab_key
	AND cl.obj_type = com.obj_type
	AND com.rownum = 1;

-- 	SELECT * FROM ap.spsearch_collection('281549')
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ap.spsearch_collection(character varying)
  OWNER TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO public;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO pruquotedb_svc;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO pruquotedb_supp;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO pruquotedb_deploy;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO pruquote_svc;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO pruquote_supp;
GRANT EXECUTE ON FUNCTION ap.spsearch_collection(character varying) TO pruquote_deploy;

-- Function: ap.spsearch_othercollectionbyobjkey(character varying)

-- DROP FUNCTION ap.spsearch_othercollectionbyobjkey(character varying);

CREATE OR REPLACE FUNCTION ap.spsearch_othercollectionbyobjkey(IN objkey character varying)
  RETURNS TABLE(obj_key text, obj_type text, chdrnum character varying, chdr_appnum character varying, po_name character varying, gender character varying, clnt_mobile1 character varying, clnt_mobile2 character varying, chdr_p_with_tax numeric, chdr_billfreq character varying, chdr_due_date date, chdr_graceperiod integer, agntnum character varying, agnt_name character varying, agnt_supcode character varying, agnt_supname character varying, chdr_age integer, cc_status character varying, agnt_status character varying, chdr_ape numeric, chdr_lapse_date date, chdr_status character varying, is_impact_persistency character varying, lapsed_reason character varying, bcu_remark text, customer_response text, branch_code character varying, branch_name character varying, cc_remark text, cc_by character varying, cc_date timestamp without time zone, bdmcode character varying, bdmname character varying, sbdmcode character varying, sbdmname character varying, rbdmcode character varying, rbdmname character varying, salezone character varying, product character varying, sub_lapsed_reason character varying, occ_date date, tele_reinstate character varying, out_amt character varying, next_prem character varying, payment_method character varying, clnt_mobile3 character varying, next_payment_date character varying, stnd_ord_end_date character varying, bank_acc character varying, so_cancel_status character varying, ben character varying, dob date) AS
$BODY$
BEGIN	
	RETURN QUERY
	select cl.obj_key
	,cl.obj_type
	,cl.chdrnum
	,cl.chdr_appnum
	,cl.po_name
	,cl.gender
	,cl.clnt_mobile1
	,cl.clnt_mobile2
	,cl.chdr_p_with_tax
	,cl.chdr_billfreq
	,cl.chdr_due_date
	,cl.chdr_graceperiod
	,cl.agntnum
	,cl.agnt_name
	,cl.agnt_supcode
	,cl.agnt_supname
	,cl.chdr_age
	,cl.cc_status
	,cl.agnt_status
	,cl.chdr_ape
	,cl.chdr_lapse_date
	,cl.chdr_status
	,cl.is_impact_persistency
	,cl.lapsed_reason
	,cl.bcu_remark
	,cl.customer_response
	,cl.branch_code
	,cl.branch_name
	,com.cc_text
	,com.cc_by
	,com.cc_date
	,cl.bdmcode
	,cl.bdmname
	,cl.sbdmcode
	,cl.sbdmname
	,cl.rbdmcode
	,cl.rbdmname
	,cl.salezone
	,cl.product
	,cl.sub_lapsed_reason
	,cl.occ_date
	,cl.tele_reinstate
	,cl.out_amt
	,cl.next_prem
	,cl.payment_method
	,cl.clnt_mobile3
	,cl.next_payment_date 
	,cl.stnd_ord_end_date
	,(CASE WHEN cl.bank_acc is null or cl.bank_acc = '' then ''
	     WHEN LENGTH(cl.bank_acc) < 4 then  cl.bank_acc
	     WHEN LENGTH(cl.bank_acc) >= 6 and LENGTH(cl.bank_acc) <= 7 then Concat(SUBSTRING(cl.bank_acc, 1, LENGTH(cl.bank_acc)-5) ,repeat('*',  3), SUBSTRING(cl.bank_acc, LENGTH(cl.bank_acc)-1, LENGTH(cl.bank_acc))) 
	     WHEN LENGTH(cl.bank_acc) >= 8 and LENGTH(cl.bank_acc) <= 10 then Concat(SUBSTRING(cl.bank_acc, 1, LENGTH(cl.bank_acc)-6) ,repeat('*',  4), SUBSTRING(cl.bank_acc, LENGTH(cl.bank_acc)-1, LENGTH(cl.bank_acc)))
	     WHEN LENGTH(cl.bank_acc) >= 11 and LENGTH(cl.bank_acc) <= 17 then Concat(SUBSTRING(cl.bank_acc, 1, LENGTH(cl.bank_acc)-7) ,repeat('*',  6), SUBSTRING(cl.bank_acc, LENGTH(cl.bank_acc)-1, LENGTH(cl.bank_acc)))
	      end) as bank_acc  
	,cl.so_cancel_status
	,cl.ben
	,cl.dob
	from ap.tabcollection cl
	left join (
		 SELECT a.tab_key, a.obj_type, a.cc_date , a.cc_text, a.cc_by
		, ROW_NUMBER() OVER(PARTITION BY a.tab_key, a.obj_type ORDER BY a.cc_id DESC) AS rownum
		FROM ap.tabcomment a
	) com
	on cl.obj_key = com.tab_key
	AND cl.obj_type = com.obj_type
	AND com.rownum = 1 
	WHERE (1=1)
        AND cl.obj_key=objkey;
	--SELECT * FROM ap.spsearch_othercollectionbyobjkey('7000015520180724');
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ap.spsearch_othercollectionbyobjkey(character varying)
  OWNER TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollectionbyobjkey(character varying) TO pruquotedb_svc;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollectionbyobjkey(character varying) TO pruquotedb_supp;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollectionbyobjkey(character varying) TO pruquotedb_deploy;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollectionbyobjkey(character varying) TO pruquote_svc;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollectionbyobjkey(character varying) TO pruquote_supp;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollectionbyobjkey(character varying) TO pruquote_deploy;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollectionbyobjkey(character varying) TO postgres;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollectionbyobjkey(character varying) TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollectionbyobjkey(character varying) TO public;


-- Function: ap.spsearch_othercollection(character varying, character varying)

-- DROP FUNCTION ap.spsearch_othercollection(character varying, character varying);

CREATE OR REPLACE FUNCTION ap.spsearch_othercollection(
    IN poappnum character varying,
    IN dobdate character varying)
  RETURNS TABLE(obj_key text, obj_type text, chdrnum character varying, chdr_appnum character varying, po_name character varying, gender character varying, clnt_mobile1 character varying, clnt_mobile2 character varying, chdr_p_with_tax numeric, chdr_billfreq character varying, chdr_due_date date, chdr_graceperiod integer, agntnum character varying, agnt_name character varying, agnt_supcode character varying, agnt_supname character varying, chdr_age integer, cc_status character varying, agnt_status character varying, chdr_ape numeric, chdr_lapse_date date, chdr_status character varying, is_impact_persistency character varying, lapsed_reason character varying, bcu_remark text, customer_response text, branch_code character varying, branch_name character varying, cc_remark text, cc_by character varying, cc_date timestamp without time zone, bdmcode character varying, bdmname character varying, sbdmcode character varying, sbdmname character varying, rbdmcode character varying, rbdmname character varying, salezone character varying, product character varying, sub_lapsed_reason character varying, occ_date date, tele_reinstate character varying, out_amt character varying, next_prem character varying, payment_method character varying, clnt_mobile3 character varying, next_payment_date character varying, stnd_ord_end_date character varying, bank_acc character varying, so_cancel_status character varying, ben character varying, dob date) AS
$BODY$
BEGIN	
	RETURN QUERY
	select cl.obj_key
	,cl.obj_type
	,cl.chdrnum
	,cl.chdr_appnum
	,cl.po_name
	,cl.gender
	,cl.clnt_mobile1
	,cl.clnt_mobile2
	,cl.chdr_p_with_tax
	,cl.chdr_billfreq
	,cl.chdr_due_date
	,cl.chdr_graceperiod
	,cl.agntnum
	,cl.agnt_name
	,cl.agnt_supcode
	,cl.agnt_supname
	,cl.chdr_age
	,cl.cc_status
	,cl.agnt_status
	,cl.chdr_ape
	,cl.chdr_lapse_date
	,cl.chdr_status
	,cl.is_impact_persistency
	,cl.lapsed_reason
	,cl.bcu_remark
	,cl.customer_response
	,cl.branch_code
	,cl.branch_name
	,com.cc_text
	,com.cc_by
	,com.cc_date
	,cl.bdmcode
	,cl.bdmname
	,cl.sbdmcode
	,cl.sbdmname
	,cl.rbdmcode
	,cl.rbdmname
	,cl.salezone
	,cl.product
	,cl.sub_lapsed_reason
	,cl.occ_date
	,cl.tele_reinstate
	,cl.out_amt
	,cl.next_prem
	,cl.payment_method
	,cl.clnt_mobile3
	,cl.next_payment_date 
	,cl.stnd_ord_end_date
	,(CASE WHEN cl.bank_acc is null or cl.bank_acc = '' then ''
	     WHEN LENGTH(cl.bank_acc) < 4 then  cl.bank_acc
	     WHEN LENGTH(cl.bank_acc) >= 6 and LENGTH(cl.bank_acc) <= 7 then Concat(SUBSTRING(cl.bank_acc, 1, LENGTH(cl.bank_acc)-5) ,repeat('*',  3), SUBSTRING(cl.bank_acc, LENGTH(cl.bank_acc)-1, LENGTH(cl.bank_acc))) 
	     WHEN LENGTH(cl.bank_acc) >= 8 and LENGTH(cl.bank_acc) <= 10 then Concat(SUBSTRING(cl.bank_acc, 1, LENGTH(cl.bank_acc)-6) ,repeat('*',  4), SUBSTRING(cl.bank_acc, LENGTH(cl.bank_acc)-1, LENGTH(cl.bank_acc)))
	     WHEN LENGTH(cl.bank_acc) >= 11 and LENGTH(cl.bank_acc) <= 17 then Concat(SUBSTRING(cl.bank_acc, 1, LENGTH(cl.bank_acc)-7) ,repeat('*',  6), SUBSTRING(cl.bank_acc, LENGTH(cl.bank_acc)-1, LENGTH(cl.bank_acc)))
	      end) as bank_acc  
	,cl.so_cancel_status
	,cl.ben
	,cl.dob
	from ap.tabcollection cl
	left join (
		 SELECT a.tab_key, a.obj_type, a.cc_date , a.cc_text, a.cc_by
		, ROW_NUMBER() OVER(PARTITION BY a.tab_key, a.obj_type ORDER BY a.cc_id DESC) AS rownum
		FROM ap.tabcomment a
	) com
	on cl.obj_key = com.tab_key
	AND cl.obj_type = com.obj_type
	AND com.rownum = 1 
	WHERE (1=1)
        AND
            (
                CASE WHEN LEFT(poappnum,1) IN('7','8','9') AND LENGTH(poappnum)=8 THEN cl.chdrnum=LEFT(poappnum,8)
                     WHEN LEFT(poappnum,2) IN('E0','13','14','15','16','17','18','19','20') AND LENGTH(poappnum)=9 THEN cl.chdr_appnum=poappnum
                ELSE 1=2
                END
            )
        AND cl.dob=dobdate::date;
	--SELECT * FROM ap.spsearch_othercollection('70000155','1970-07-17');
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ap.spsearch_othercollection(character varying, character varying)
  OWNER TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollection(character varying, character varying) TO pruquotedb_svc;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollection(character varying, character varying) TO pruquotedb_supp;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollection(character varying, character varying) TO pruquotedb_deploy;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollection(character varying, character varying) TO pruquote_svc;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollection(character varying, character varying) TO pruquote_supp;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollection(character varying, character varying) TO pruquote_deploy;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollection(character varying, character varying) TO postgres;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollection(character varying, character varying) TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spsearch_othercollection(character varying, character varying) TO public;


alter table ap.tabagnt_fc_hk
add column agntsbdm character(10)
-- Function: ap.splist_heirarchies(character)

-- DROP FUNCTION ap.splist_heirarchies(character);


CREATE OR REPLACE FUNCTION ap.splist_heirarchies(IN userid character)
  RETURNS TABLE(agntnum character varying) AS
$BODY$
	declare plascode varchar(10);
		pposition varchar(50);
	BEGIN	
		pposition=ltrim((select "position" FROM ap.tabagnt_mgt_infor a where a.user_id=userid limit 1));
		plascode = (select lascode from ap.tabagnt_mgt_infor where user_id = userid limit 1);
		if userid IN('326625', '336758') then 
			plascode = 'all';
		end if;
		RAISE NOTICE '%',plascode;

		create temporary table tmpagnt on commit drop as	
		SELECT a.agntnum::character varying(10) AS pagntnum FROM ap.tabagnt_fc_hk a WHERE (1=1)
			AND(
				CASE WHEN userid in('326625','336758') THEN 1=1
					 WHEN lower(pposition)=lower('EXCO') THEN 1=1
					 WHEN lower(pposition)=lower('Head of Banca') then ltrim(rtrim(lower(hdbanca)))=ltrim(rtrim(lower(plascode)))
					 WHEN lower(pposition)=lower('Manager Banca') then ltrim(rtrim(lower(mgbanca)))=ltrim(rtrim(lower(plascode)))
					 WHEN ltrim(rtrim(lower(pposition)))=ltrim(rtrim(lower('RBDM'))) then ltrim(rtrim(lower(agntrbdm)))=ltrim(rtrim(lower(plascode)))
					 WHEN lower(pposition)=lower('SBDM') then ltrim(rtrim(lower(agntsbdm)))=ltrim(rtrim(lower(plascode)))
					 WHEN lower(pposition)=lower('BDM') then ltrim(rtrim(lower(agntbdm)))=ltrim(rtrim(lower(plascode)))
					 WHEN lower(pposition)=lower('Sale Supervisor') then ltrim(rtrim(lower(agntsup)))=ltrim(rtrim(lower(plascode)))
					 WHEN lower(pposition)=lower('UM') then ltrim(rtrim(lower(agnt_branch)))=ltrim(rtrim(lower(plascode)))
					 WHEN lower(pposition)=lower('LD') then ltrim(rtrim(lower(agntsup)))=ltrim(rtrim(lower(plascode)))
					 WHEN lower(pposition)=lower('Head of LIC') then ltrim(rtrim(lower(hdbanca)))=ltrim(rtrim(lower(plascode)))
					 WHEN left(userid, 2) IN ('60', '69') then ltrim(rtrim(lower(a.agntnum)))=ltrim(rtrim(lower(userid)))
				ELSE 1=2
				END
			)
		UNION (SELECT userid);
		
		

		RETURN QUERY
		SELECT  Distinct *
		FROM tmpagnt
		;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ap.splist_heirarchies(character)
  OWNER TO pruquote;
GRANT EXECUTE ON FUNCTION ap.splist_heirarchies(character) TO pruquotedb_svc;
GRANT EXECUTE ON FUNCTION ap.splist_heirarchies(character) TO pruquotedb_supp;
GRANT EXECUTE ON FUNCTION ap.splist_heirarchies(character) TO pruquotedb_deploy;
GRANT EXECUTE ON FUNCTION ap.splist_heirarchies(character) TO pruquote_svc;
GRANT EXECUTE ON FUNCTION ap.splist_heirarchies(character) TO pruquote_supp;
GRANT EXECUTE ON FUNCTION ap.splist_heirarchies(character) TO pruquote_deploy;
GRANT EXECUTE ON FUNCTION ap.splist_heirarchies(character) TO postgres;
GRANT EXECUTE ON FUNCTION ap.splist_heirarchies(character) TO pruquote;
GRANT EXECUTE ON FUNCTION ap.splist_heirarchies(character) TO public;

-- Function: ap.splist_persistency_summary(character varying)

-- DROP FUNCTION ap.splist_persistency_summary(character varying);

CREATE OR REPLACE FUNCTION ap.splist_persistency_summary(IN userid character varying)
  RETURNS TABLE(portfolio character varying, ape_exposure character varying, ape_if character varying, ape_la_su character varying) AS
$BODY$
declare
    current_q character varying(2);
BEGIN
    current_q = ap.getquarter(date_part('month',now()::date)::text);
    
    create temporary table tmpagnts on commit drop as
    select * from ap.splist_heirarchies(userid);
 
    create temporary table tmppersistency on commit drop as
    select ''::character varying(50) as portfolio
    , ''::character varying(20) as ape_exposure
    , ''::character varying(20) as ape_if
    , ''::character varying(20) as ape_la_su
    WHERE 1=2;
 
    INSERT INTO tmppersistency
    SELECT 'Included Transfer Policy'::character varying
    , SUM(ae_ape_itp)::character varying
    , SUM(if_ape_itp)::character varying
    , SUM(la_ape_itp + su_ape_itp)::character varying
    FROM ap.tabpersistency
    WHERE persistency_for_q = current_q
    AND agntnum IN (SELECT agntnum FROM tmpagnts);
 
    INSERT INTO tmppersistency
    SELECT 'Excluded Transfer Policy'::character varying
    , SUM(ae_ape_etp)::character varying
    , SUM(if_ape_etp)::character varying
    , SUM(la_ape_etp + su_ape_etp)::character varying
    FROM ap.tabpersistency
    WHERE persistency_for_q = current_q
    AND agntnum IN (SELECT agntnum FROM tmpagnts);
 
    -- Create new row for persistency percentage with Q1 percent
    INSERT INTO tmppersistency
    -- 04.02.2019 Vichet: add COALESCE to set default 80% when ape is null
    -- 05.02.2019 Vichet: change LEFT(agntnum,2) to LEFT(userid,2) because it should be base on login user
    SELECT COALESCE((1 - (SUM(CASE WHEN LEFT(userid,2)='69' THEN la_ape_etp 
                ELSE la_ape_itp END + 
                CASE WHEN LEFT(userid,2)='69' THEN su_ape_etp 
                    ELSE su_ape_itp END)/(CASE WHEN SUM(CASE WHEN LEFT(userid,2)='69' THEN ae_ape_etp 
                                            ELSE ae_ape_itp END) = 0 THEN NULL 
                                    ELSE SUM(
                                    CASE WHEN LEFT(userid,2)='69' THEN ae_ape_etp
                                        ELSE ae_ape_itp END) END)))::numeric(18,4)::character varying, '0.8')
    , '0'::character varying
    , '0'::character varying
    , '0'::character varying
    FROM ap.tabpersistency
    WHERE persistency_for_q = 'Q1'
    AND agntnum IN (SELECT agntnum FROM tmpagnts);
 
    -- Update Q2 percent
    UPDATE tmppersistency p
    -- 04.02.2019 Vichet: add COALESCE to set default 80% when ape is null
    -- 05.02.2019 Vichet: change LEFT(agntnum,2) to LEFT(userid,2) because it should be base on login user
    SET ape_exposure = (SELECT COALESCE((1 - (SUM(CASE WHEN LEFT(userid,2)='69' THEN la_ape_etp 
                ELSE la_ape_itp END + 
                CASE WHEN LEFT(userid,2)='69' THEN su_ape_etp 
                    ELSE su_ape_itp END)/(CASE WHEN SUM(CASE WHEN LEFT(userid,2)='69' THEN ae_ape_etp 
                                            ELSE ae_ape_itp END) = 0 THEN NULL
                                    ELSE SUM(
                                    CASE WHEN LEFT(userid,2)='69' THEN ae_ape_etp
                                        ELSE ae_ape_itp END) END)))::numeric(18,4)::character varying, '0.8')
                FROM ap.tabpersistency
                WHERE persistency_for_q = 'Q2'
                AND agntnum IN (SELECT agntnum FROM tmpagnts))
    WHERE p.ape_exposure = '0';
 
    -- Update Q3 percent
    UPDATE tmppersistency p
    -- 04.02.2019 Vichet: add COALESCE to set default 80% when ape is null
    -- 05.02.2019 Vichet: change LEFT(agntnum,2) to LEFT(userid,2) because it should be base on login user
    SET ape_if = (SELECT COALESCE((1 - (SUM(CASE WHEN LEFT(userid,2)='69' THEN la_ape_etp 
                ELSE la_ape_itp END + 
                CASE WHEN LEFT(userid,2)='69' THEN su_ape_etp 
                    ELSE su_ape_itp END)/(CASE WHEN SUM(CASE WHEN LEFT(userid,2)='69' THEN ae_ape_etp 
                                            ELSE ae_ape_itp END) = 0 THEN NULL
                                    ELSE SUM(
                                    CASE WHEN LEFT(userid,2)='69' THEN ae_ape_etp
                                        ELSE ae_ape_itp END) END)))::numeric(18,4)::character varying, '0.8')
                FROM ap.tabpersistency
                WHERE persistency_for_q = 'Q3'
                AND agntnum IN (SELECT agntnum FROM tmpagnts))
    WHERE p.ape_if = '0';
 
    -- Update Q4 percent
    UPDATE tmppersistency p
    -- 04.02.2019 Vichet: add COALESCE to set default 80% when ape is null
    -- 05.02.2019 Vichet: change LEFT(agntnum,2) to LEFT(userid,2) because it should be base on login user
    SET ape_la_su = (SELECT COALESCE((1 - (SUM(CASE WHEN LEFT(userid,2)='69' THEN la_ape_etp 
                ELSE la_ape_itp END + 
                CASE WHEN LEFT(userid,2)='69' THEN su_ape_etp 
                    ELSE su_ape_itp END)/(CASE WHEN SUM(CASE WHEN LEFT(userid,2)='69' THEN ae_ape_etp 
                                            ELSE ae_ape_itp END) = 0 THEN NULL
                                    ELSE SUM(
                                    CASE WHEN LEFT(userid,2)='69' THEN ae_ape_etp
                                        ELSE ae_ape_itp END) END)))::numeric(18,4)::character varying, '0.8')
                FROM ap.tabpersistency
                WHERE persistency_for_q = 'Q4'
                AND agntnum IN (SELECT agntnum FROM tmpagnts))
    WHERE p.ape_la_su = '0';
 
    RETURN QUERY
    SELECT *
    FROM tmppersistency;
--     SELECT * FROM ap.splist_persistency_summary('69001483')
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ap.splist_persistency_summary(character varying)
  OWNER TO pruquote;
GRANT EXECUTE ON FUNCTION ap.splist_persistency_summary(character varying) TO public;
GRANT EXECUTE ON FUNCTION ap.splist_persistency_summary(character varying) TO pruquote;
GRANT EXECUTE ON FUNCTION ap.splist_persistency_summary(character varying) TO postgres;
GRANT EXECUTE ON FUNCTION ap.splist_persistency_summary(character varying) TO pruquotedb_svc;
GRANT EXECUTE ON FUNCTION ap.splist_persistency_summary(character varying) TO pruquotedb_supp;
GRANT EXECUTE ON FUNCTION ap.splist_persistency_summary(character varying) TO pruquotedb_deploy;
GRANT EXECUTE ON FUNCTION ap.splist_persistency_summary(character varying) TO pruquote_svc;
GRANT EXECUTE ON FUNCTION ap.splist_persistency_summary(character varying) TO pruquote_supp;
GRANT EXECUTE ON FUNCTION ap.splist_persistency_summary(character varying) TO pruquote_deploy;
