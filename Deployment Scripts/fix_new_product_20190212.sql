-- Function: ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor)

-- DROP FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor);

CREATE OR REPLACE FUNCTION ap.spgen_pruquoteuab(
    aid bigint,
    c_r1 refcursor,
    c_r2 refcursor,
    c_r3 refcursor,
    c_r4 refcursor)
  RETURNS SETOF refcursor AS
$BODY$
    declare uabl1 decimal(18,8);
    uabl2 decimal(18,8);
    sa decimal(18,2);
    vpolicyterm varchar(20); -- DECIMAL(18,2);
    premiumterm varchar(20); -- DECIMAL(18,2);        
    
    abasicplanprd varchar(4);
    uabbase decimal(18,8);
    l1rtr1 decimal(13,5);
    l1rtr2 decimal(13,5);
    l1rsr1 decimal(13,5);
    l2rtr1 decimal(13,5);
    l2rsr1 decimal(13,5);
    -- prutect
--   la1pad1 decimal(13,5);
--     la2pad1 decimal(13,5);
    -- end of prutect
    l1age int;
    l2age int;
    examinel1 varchar(1000);
    examinel2 varchar(1000);
 
    i integer;
    len integer;
    
begin
--     DROP TEMPORARY TABLE IF EXISTS PARAM;
    create temporary table param on commit drop as
    select * from ap.vpruquote_parms a where a.id =aid;
 
    update param set product=lower('BTL1') where product2=lower('BTL1');
 
    abasicplanprd = (select l.product from param l);
    premiumterm =(select l.premiumterm from param l);
    vpolicyterm =(select l.policyterm from param l);    
    sa =(select l.basicplan_sa from param l);    
    l1age =(select l.la1age from param l);    
    l2age =(select l.la2age from param l);    
        
    
    uabbase=sa*(case when lower(abasicplanprd) in(lower('BTR1'),lower('BTR2'),lower('BTR4'),lower('BTR5')) then (select rate from ap.vuabfactor v where lower(v.uabfactor)=lower(abasicplanprd||vpolicyterm) and v.policyyear=0 limit 1) else 1 end);    
    l1rsr1=(select (l.l1_rsr1*(select rate from ap.vuabfactor v where lower(v.uabfactor)=lower('RSR1'||vpolicyterm) and v.policyyear=0 limit 1))
    from param l
    where lower(l.l1_rsr1_flag)=lower('Yes'));
    l1rtr1=(select (l.l1_rtr1*(select rate from ap.vuabfactor v where lower(v.uabfactor)=lower('RTR1'||vpolicyterm) and v.policyyear=0 limit 1))
    from param l
    where lower(l.l1_rtr1_flag)=lower('Yes'));
    l1rtr2=(select (l.l1_rtr2*(select rate from ap.vuabfactor v where lower(v.uabfactor)=lower('RTR2'||vpolicyterm) and v.policyyear=0 limit 1))
    from param l
    where lower(l.l1_rtr2_flag)=lower('Yes'));
    l2rsr1=(select (l.l2_rsr1*(select rate from ap.vuabfactor v where lower(v.uabfactor)=lower('RSR1'||vpolicyterm) and v.policyyear=0 limit 1))
    from param l
    where lower(l.l2_rsr1_flag)=lower('Yes'));
    l2rtr1=(select (l.l2_rtr1*(select rate from ap.vuabfactor v where lower(v.uabfactor)=lower('RTR1'||vpolicyterm) and v.policyyear=0 limit 1))
    from param l
    where lower(l.l2_rtr1_flag)=lower('Yes'));
    --We would request for the calculation to change to remove Safety+ SA from UAB calculation 11-02-2019 By Actuarial Team
    -- prutect
  --   la1pad1=(select (l.la1_pad1_sa*(select rate from ap.vuabfactor v where lower(v.uabfactor)=lower('PAD1'||vpolicyterm) and v.policyyear=0 limit 1))
--     from param l
--     where lower(l.la1_pad1_flag)=lower('Yes'));
--     la2pad1=(select (l.la2_pad1_sa*(select rate from ap.vuabfactor v where lower(v.uabfactor)=lower('PAD1'||vpolicyterm) and v.policyyear=0 limit 1))
--     from param l
--     where lower(l.la2_pad1_flag)=lower('Yes'));
    -- end of prutect
-- SELECT UAB;
-- SELECT L1RTR1;
-- SELECT L1RSR1;
-- SELECT L1RTR2;
-- SELECT L2RSR1;
-- SELECT L2RTR1;
 
uabl1=(trunc(uabbase::decimal,2) + trunc(coalesce(l1rtr1,0),2) + trunc(coalesce(l1rtr2,0),2) + trunc(coalesce(l1rsr1,0),2)); --+ trunc(coalesce(la1pad1,0),2));
uabl2=trunc(coalesce(l2rtr1,0),2)+trunc(coalesce(l2rsr1,0),2); --+ trunc(coalesce(la2pad1,0),2);
examinel1=(select examine from ap.tabmedicalexam where (uabl1>=uabfrom and uabl1<=uabto) and (l1age>=agefrom and l1age<=ageto) and lower(product) like '%'||lower(abasicplanprd)||'%' limit 1);
examinel2=(select examine from ap.tabmedicalexam where (uabl2>=uabfrom and uabl2<=uabto) and (l2age>=agefrom and l2age<=ageto) and uabl2<>0 and lower(product) like '%'||lower(abasicplanprd)||'%' limit 1);
 

create temporary table examine on commit drop as(select null::varchar(20) as element);
 
len=length(examinel1) - length(replace(examinel1, '|', ''))+1;
 
if len=0 then
  len=1;
end if;
  i = 1;
  while i <= len loop
    insert into examine 
     select split_part(examinel1, '|', i) as element;
    i = i + 1;    
  end loop;
 
OPEN c_r1 FOR
select l.la1name,l.la1age,la1sex,trunc(uabl1,2) as uabl1 from param l;
RETURN NEXT c_r1;
 
OPEN c_r2 FOR
select element,english,khmer from examine join ap.tablabel on element=code;
RETURN NEXT c_r2;
 
create temporary table examine2 on commit drop as(select ''::varchar(20) as element);
len=length(examinel2) - length(replace(examinel2, '|', ''))+1;
 
if len=0 then
  len=1;
end if;
i = 1;
  while i <= len loop
    insert into examine2 
     select split_part(examinel2, '|', i) as element;
    i = i + 1;
  end loop;
 
OPEN c_r3 FOR
select 
    (case when uabl2=0 then null else l.la2name end) as la2name,
    (case when uabl2=0 then null else l.la2age end) as la2age,
    (case when uabl2=0 then null else l.la2sex end) as la2sex,
    (case when uabl2=0 then null else trunc(uabl2,2) end) as uabl2
    from param l;
RETURN NEXT c_r3;
OPEN c_r4 FOR
select element,english,khmer from examine2 join ap.tablabel on element=code;
RETURN NEXT c_r4;
 

end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor)
  OWNER TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO pruquotedb_svc;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO pruquotedb_supp;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO pruquotedb_deploy;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO postgres;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO pruquote_svc;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO pruquote_supp;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO pruquote_deploy;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO public;

Update ap.tablabel set khmer = 'ជាតិលឿងសរុប' where code ='TBB';