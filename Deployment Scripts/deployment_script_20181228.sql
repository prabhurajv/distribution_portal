-- START NEW PRODUCT
-- CREATE TABLE ap.tabprintquotesettings
-- (
--   product character varying(20) NOT NULL,
--   query character varying NOT NULL,
--   html character varying NOT NULL,
--   htmlkh character varying NOT NULL,
--   validflag numeric(11,0) DEFAULT NULL::numeric,
--   couser character varying(45) DEFAULT NULL::character varying,
--   syndate timestamp with time zone,
--   CONSTRAINT tabprintquotesettings_pkey PRIMARY KEY (product)
-- )
-- WITH (
--   OIDS=FALSE
-- );

-- ALTER TABLE ap.tabprintquotesettings
--   OWNER TO pruquote;
-- GRANT ALL ON TABLE ap.tabprintquotesettings TO pruquote;
-- GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ap.tabprintquotesettings TO pruquotedb_svc;
-- GRANT SELECT ON TABLE ap.tabprintquotesettings TO pruquotedb_supp;
-- GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ap.tabprintquotesettings TO pruquotedb_deploy;
-- GRANT SELECT ON TABLE ap.tabprintquotesettings TO pruquote_supp;
-- GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ap.tabprintquotesettings TO pruquote_svc;
-- GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ap.tabprintquotesettings TO pruquote_deploy;


-- ALTER TABLE ap.tabpruquote_parms
-- 	ADD COLUMN la1_pad1 int default null::numeric,
-- 	ADD COLUMN la1_pad1_sa numeric(18,2),
-- 	ADD COLUMN la2_pad1 int default null::numeric,
-- 	ADD COLUMN la2_pad1_sa numeric(18,2);

-- CREATE OR REPLACE VIEW ap.vpruquote_parms AS 
--  SELECT l.id, 
--         CASE
--             WHEN l.product::text = 'BTR1'::text AND "right"(vpremiumterm.reserve2::text, 2)::character(1) = 5::character(1) THEN 'BTL1'::character varying
--             ELSE l.product
--         END AS product2, 
--         CASE
--             WHEN l.product::text = 'BTR1'::text THEN 'Pru Myfamily'::character varying
--             WHEN l.product::text = 'BTR2'::text THEN 'Edusave'::character varying
--             WHEN l.product::text = 'BTR3'::text THEN 'easylife'::character varying
--             WHEN l.product::text = 'BTR4'::text THEN 'eduCARE'::text::character varying
--             WHEN l.product::text = 'MTR1'::text THEN 'MRTA'::character varying
--             WHEN l.product::text = 'MTR2'::text THEN 'MLTA'::character varying
-- 			WHEN l.product::text = 'BTR5'::text THEN 'PureProtect'::character varying
--             WHEN l.product::text = 'BTR6'::text THEN 'SafeLife'::text::character varying
--             ELSE ''::character varying
--         END AS productdesc, l.product, l.commdate, l.cola1relationship, vrelationship.itemlatin AS relationship, l.poname, l.la1name, l.la1dateofbirth, ( SELECT date_part('year'::text, f.f) AS date_part
--            FROM age(l.commdate::date::timestamp with time zone, l.la1dateofbirth::date::timestamp with time zone) f(f)) AS la1age, l.cola1sex, vla1sex.itemlatin AS la1sex, vla1sex.reserve2 AS la1sex2, l.cola1occupationclass, vla1occupationclass.itemlatin AS la1occupationclass, l.copolicyterm, vpolicyterm.itemlatin AS policyterm, l.copremiumterm, "right"(vpremiumterm.reserve2::text, 2) AS premiumterm, l.la2name, l.la2dateofbirth, ( SELECT date_part('year'::text, f.f) AS date_part
--            FROM age(l.commdate::date::timestamp with time zone, l.la2dateofbirth::date::timestamp with time zone) f(f)) AS la2age, l.cola2sex, vla2sex.itemlatin AS la2sex, vla2sex.reserve2 AS la2sex2, l.cola2occupationclass, vla2occupationclass.itemlatin AS la2occupationclass, l.copaymenttype, vpaymenttype.itemlatin AS paymenttype, vpaymenttype.reserve2 AS paymenttype2, l.copaymentmode, vpaymentmode.itemlatin AS paymentmode, vpaymentmode.reserve2 AS paymentmode2, l.basicplan_sa, l.col1_rtr1, 
--         CASE
--             WHEN l.col1_rtr1 = 15::numeric THEN 'yes'::text
--             ELSE 'no'::text
--         END AS l1_rtr1_flag, l.l1_rtr1, l.col1_rsr1, 
--         CASE
--             WHEN l.col1_rsr1 = 15::numeric THEN 'yes'::text
--             ELSE 'no'::text
--         END AS l1_rsr1_flag, l.l1_rsr1, l.col2_rtr1, 
--         CASE
--             WHEN l.col2_rtr1 = 15::numeric THEN 'yes'::text
--             ELSE 'no'::text
--         END AS l2_rtr1_flag, l.l2_rtr1, l.col2_rsr1, 
--         CASE
--             WHEN l.col2_rsr1 = 15::numeric THEN 'yes'::text
--             ELSE 'no'::text
--         END AS l2_rsr1_flag, l.l2_rsr1, l.validflag, l.couser, ''::character varying(50) AS fullname, l.syndate, 'view report'::character varying(20) AS nextprog, 1::numeric AS nextprog_link, l.col1_rtr2, 
--         CASE
--             WHEN l.col1_rtr2 = 15::numeric THEN 'yes'::text
--             ELSE 'no'::text
--         END AS l1_rtr2_flag, l.l1_rtr2, l.pruretirement, l.isfundedbybank, l.inrate, l.transfer, l.loanamount,
--         CASE WHEN l.la1_pad1 = 15 THEN 'yes'::character varying
-- 			ELSE 'no'::character varying
-- 		END AS la1_pad1_flag,
-- 		la1_pad1_sa,
-- 		CASE WHEN l.la2_pad1 = 15 THEN 'yes'::character varying
-- 			ELSE 'no'::character varying
-- 		END AS la2_pad1_flag,
-- 		la2_pad1_sa
--    FROM ap.tabpruquote_parms l, ap.vitem2 vla1sex, ap.vitem2 vrelationship, ap.vitem2 vla1occupationclass, ap.vitem2 vpolicyterm, ap.vitem2 vpremiumterm, ap.vitem2 vla2sex, ap.vitem2 vla2occupationclass, ap.vitem2 vpaymenttype, ap.vitem2 vpaymentmode
--   WHERE l.cola1sex = vla1sex.itemid AND vla1sex.tablid = 2::numeric AND l.cola1relationship = vrelationship.itemid AND vrelationship.tablid = 1::numeric AND l.cola1occupationclass = vla1occupationclass.itemid AND vla1occupationclass.tablid = 7::numeric AND l.copolicyterm = vpolicyterm.itemid AND vpolicyterm.tablid = 3::numeric AND l.copremiumterm = vpremiumterm.itemid AND vpremiumterm.tablid = 4::numeric AND l.cola2sex = vla2sex.itemid AND vla2sex.tablid = 2::numeric AND l.cola2occupationclass = vla2occupationclass.itemid AND vla2occupationclass.tablid = 7::numeric AND l.copaymenttype = vpaymenttype.itemid AND vpaymenttype.tablid = 8::numeric AND l.copaymentmode = vpaymentmode.itemid AND vpaymentmode.tablid = 9::numeric AND (now()::date - l.commdate::date) <= 90
--   ORDER BY l.id DESC;
-- ALTER TABLE ap.vpruquote_parms
--   OWNER TO pruquote;
-- GRANT ALL ON TABLE ap.vpruquote_parms TO pruquote;
-- GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ap.vpruquote_parms TO pruquotedb_svc;
-- GRANT SELECT ON TABLE ap.vpruquote_parms TO pruquotedb_supp;
-- GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ap.vpruquote_parms TO pruquotedb_deploy;
-- GRANT SELECT ON TABLE ap.vpruquote_parms TO pruquote_supp;
-- GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ap.vpruquote_parms TO pruquote_svc;
-- GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ap.vpruquote_parms TO pruquote_deploy;

-- -- Function: ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor)

-- -- DROP FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor);

-- CREATE OR REPLACE FUNCTION ap.spgen_pruquoteuab(
--     aid bigint,
--     c_r1 refcursor,
--     c_r2 refcursor,
--     c_r3 refcursor,
--     c_r4 refcursor)
--   RETURNS SETOF refcursor AS
-- $BODY$
--     declare uabl1 decimal(18,8);
--     uabl2 decimal(18,8);
--     sa decimal(18,2);
--     vpolicyterm varchar(20); -- DECIMAL(18,2);
--     premiumterm varchar(20); -- DECIMAL(18,2);        
    
--     abasicplanprd varchar(4);
--     uabbase decimal(18,8);
--     l1rtr1 decimal(13,5);
--     l1rtr2 decimal(13,5);
--     l1rsr1 decimal(13,5);
--     l2rtr1 decimal(13,5);
--     l2rsr1 decimal(13,5);
--     -- prutect
--     la1pad1 decimal(13,5);
--     la2pad1 decimal(13,5);
--     -- end of prutect
--     l1age int;
--     l2age int;
--     examinel1 varchar(1000);
--     examinel2 varchar(1000);
 
--     i integer;
--     len integer;
    
-- begin
-- --     DROP TEMPORARY TABLE IF EXISTS PARAM;
--     create temporary table param on commit drop as
--     select * from ap.vpruquote_parms a where a.id =aid;
 
--     update param set product=lower('BTL1') where product2=lower('BTL1');
 
--     abasicplanprd = (select l.product from param l);
--     premiumterm =(select l.premiumterm from param l);
--     vpolicyterm =(select l.policyterm from param l);    
--     sa =(select l.basicplan_sa from param l);    
--     l1age =(select l.la1age from param l);    
--     l2age =(select l.la2age from param l);    
        
    
--     uabbase=sa*(case when lower(abasicplanprd) in(lower('BTR1'),lower('BTR2'),lower('BTR4'),lower('BTR5')) then (select rate from ap.vuabfactor v where lower(v.uabfactor)=lower(abasicplanprd||vpolicyterm) and v.policyyear=0 limit 1) else 1 end);    
--     l1rsr1=(select (l.l1_rsr1*(select rate from ap.vuabfactor v where lower(v.uabfactor)=lower('RSR1'||vpolicyterm) and v.policyyear=0 limit 1))
--     from param l
--     where lower(l.l1_rsr1_flag)=lower('Yes'));
--     l1rtr1=(select (l.l1_rtr1*(select rate from ap.vuabfactor v where lower(v.uabfactor)=lower('RTR1'||vpolicyterm) and v.policyyear=0 limit 1))
--     from param l
--     where lower(l.l1_rtr1_flag)=lower('Yes'));
--     l1rtr2=(select (l.l1_rtr2*(select rate from ap.vuabfactor v where lower(v.uabfactor)=lower('RTR2'||vpolicyterm) and v.policyyear=0 limit 1))
--     from param l
--     where lower(l.l1_rtr2_flag)=lower('Yes'));
--     l2rsr1=(select (l.l2_rsr1*(select rate from ap.vuabfactor v where lower(v.uabfactor)=lower('RSR1'||vpolicyterm) and v.policyyear=0 limit 1))
--     from param l
--     where lower(l.l2_rsr1_flag)=lower('Yes'));
--     l2rtr1=(select (l.l2_rtr1*(select rate from ap.vuabfactor v where lower(v.uabfactor)=lower('RTR1'||vpolicyterm) and v.policyyear=0 limit 1))
--     from param l
--     where lower(l.l2_rtr1_flag)=lower('Yes'));
--     -- prutect
--     la1pad1=(select (l.la1_pad1_sa*(select rate from ap.vuabfactor v where lower(v.uabfactor)=lower('PAD1'||vpolicyterm) and v.policyyear=0 limit 1))
--     from param l
--     where lower(l.la1_pad1_flag)=lower('Yes'));
--     la2pad1=(select (l.la2_pad1_sa*(select rate from ap.vuabfactor v where lower(v.uabfactor)=lower('PAD1'||vpolicyterm) and v.policyyear=0 limit 1))
--     from param l
--     where lower(l.la2_pad1_flag)=lower('Yes'));
--     -- end of prutect
-- -- SELECT UAB;
-- -- SELECT L1RTR1;
-- -- SELECT L1RSR1;
-- -- SELECT L1RTR2;
-- -- SELECT L2RSR1;
-- -- SELECT L2RTR1;
 
-- uabl1=(trunc(uabbase::decimal,2) + trunc(coalesce(l1rtr1,0),2) + trunc(coalesce(l1rtr2,0),2) + trunc(coalesce(l1rsr1,0),2) + trunc(coalesce(la1pad1,0),2));
-- uabl2=trunc(coalesce(l2rtr1,0),2)+trunc(coalesce(l2rsr1,0),2) + trunc(coalesce(la2pad1,0),2);
-- examinel1=(select examine from ap.tabmedicalexam where (uabl1>=uabfrom and uabl1<=uabto) and (l1age>=agefrom and l1age<=ageto) and lower(product) like '%'||lower(abasicplanprd)||'%' limit 1);
-- examinel2=(select examine from ap.tabmedicalexam where (uabl2>=uabfrom and uabl2<=uabto) and (l2age>=agefrom and l2age<=ageto) and uabl2<>0 and lower(product) like '%'||lower(abasicplanprd)||'%' limit 1);
 

-- create temporary table examine on commit drop as(select null::varchar(20) as element);
 
-- len=length(examinel1) - length(replace(examinel1, '|', ''))+1;
 
-- if len=0 then
--   len=1;
-- end if;
--   i = 1;
--   while i <= len loop
--     insert into examine 
--      select split_part(examinel1, '|', i) as element;
--     i = i + 1;    
--   end loop;
 
-- OPEN c_r1 FOR
-- select l.la1name,l.la1age,la1sex,trunc(uabl1,2) as uabl1 from param l;
-- RETURN NEXT c_r1;
 
-- OPEN c_r2 FOR
-- select element,english,khmer from examine join ap.tablabel on element=code;
-- RETURN NEXT c_r2;
 
-- create temporary table examine2 on commit drop as(select ''::varchar(20) as element);
-- len=length(examinel2) - length(replace(examinel2, '|', ''))+1;
 
-- if len=0 then
--   len=1;
-- end if;
-- i = 1;
--   while i <= len loop
--     insert into examine2 
--      select split_part(examinel2, '|', i) as element;
--     i = i + 1;
--   end loop;
 
-- OPEN c_r3 FOR
-- select 
--     (case when uabl2=0 then null else l.la2name end) as la2name,
--     (case when uabl2=0 then null else l.la2age end) as la2age,
--     (case when uabl2=0 then null else l.la2sex end) as la2sex,
--     (case when uabl2=0 then null else trunc(uabl2,2) end) as uabl2
--     from param l;
-- RETURN NEXT c_r3;
-- OPEN c_r4 FOR
-- select element,english,khmer from examine2 join ap.tablabel on element=code;
-- RETURN NEXT c_r4;
 

-- end;
-- $BODY$
--   LANGUAGE plpgsql VOLATILE
--   COST 100
--   ROWS 1000;
-- ALTER FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor)  OWNER TO pruquote;
-- GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO public;
-- GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO pruquote;
-- GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO pruquotedb_svc;
-- GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO pruquotedb_supp;
-- GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO pruquotedb_deploy;
-- GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO pruquote_svc;
-- GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO pruquote_supp;
-- GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO pruquote_deploy;
-- GRANT EXECUTE ON FUNCTION ap.spgen_pruquoteuab(bigint, refcursor, refcursor, refcursor, refcursor) TO postgres;


-- END PRODUCT

-- START TAX CHANGE
DROP SEQUENCE tabtaxrate_id_seq;

CREATE SEQUENCE ap.tabtaxrate_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE ap.tabtaxrate_id_seq
  OWNER TO pruquote;
GRANT ALL ON SEQUENCE ap.tabtaxrate_id_seq TO pruquotedb_svc;
GRANT SELECT, USAGE ON SEQUENCE ap.tabtaxrate_id_seq TO pruquotedb_supp;
GRANT ALL ON SEQUENCE ap.tabtaxrate_id_seq TO pruquotedb_deploy;
GRANT ALL ON SEQUENCE ap.tabtaxrate_id_seq TO pruquote_svc;
GRANT ALL ON SEQUENCE ap.tabtaxrate_id_seq TO pruquote;

CREATE TABLE ap.tabtaxrate
(
  id numeric(20,0) NOT NULL DEFAULT nextval('ap.tabtaxrate_id_seq'::regclass),
  prd_comp_code character varying(250) NOT NULL,
  itm_frm character varying(8) NOT NULL,
  itm_to character varying(8) NOT NULL,
  rate numeric(18,4) NOT NULL,
  validflag numeric(11,0) DEFAULT NULL::numeric,
  couser character varying(45) DEFAULT NULL::character varying,
  syndate timestamp with time zone,
  CONSTRAINT tabtaxrate_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
GRANT ALL ON TABLE ap.tabtaxrate TO pruquote;
GRANT SELECT, UPDATE, INSERT, TRUNCATE, DELETE ON TABLE ap.tabtaxrate TO pruquotedb_svc;
GRANT SELECT ON TABLE ap.tabtaxrate TO pruquotedb_supp;
GRANT SELECT, UPDATE, INSERT, TRUNCATE, DELETE ON TABLE ap.tabtaxrate TO pruquotedb_deploy;
GRANT SELECT ON TABLE ap.tabtaxrate TO pruquote_supp;
GRANT SELECT, UPDATE, INSERT, TRUNCATE, DELETE ON TABLE ap.tabtaxrate TO pruquote_svc;
GRANT SELECT, UPDATE, INSERT, TRUNCATE, DELETE ON TABLE ap.tabtaxrate TO pruquote_deploy;


CREATE OR REPLACE FUNCTION ap.get_tax_rate(
    rcd timestamp with time zone,
    prd_code text)
  RETURNS numeric  AS
$BODY$
DECLARE tax_rate  numeric;
BEGIN
    tax_rate = COALESCE((SELECT rate from ap.tabtaxrate where CAST(rcd as date) >= CAST(itm_frm as date) and CAST(rcd as date) <= CAST(itm_to as date) and lower(prd_comp_code) = lower(prd_code) and validflag = 1 order by syndate desc limit 1),0);
    RETURN tax_rate;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE STRICT
  COST 100;
ALTER FUNCTION ap.get_tax_rate(timestamp with time zone, text)
  OWNER TO pruquote;
GRANT EXECUTE ON FUNCTION ap.get_tax_rate(timestamp with time zone, text) TO public;
GRANT EXECUTE ON FUNCTION ap.get_tax_rate(timestamp with time zone, text) TO pruquote;
GRANT EXECUTE ON FUNCTION ap.get_tax_rate(timestamp with time zone, text) TO pruquotedb_svc;
GRANT EXECUTE ON FUNCTION ap.get_tax_rate(timestamp with time zone, text) TO pruquotedb_supp;
GRANT EXECUTE ON FUNCTION ap.get_tax_rate(timestamp with time zone, text) TO pruquotedb_deploy;
GRANT EXECUTE ON FUNCTION ap.get_tax_rate(timestamp with time zone, text) TO pruquote_svc;
GRANT EXECUTE ON FUNCTION ap.get_tax_rate(timestamp with time zone, text) TO pruquote_supp;
GRANT EXECUTE ON FUNCTION ap.get_tax_rate(timestamp with time zone, text) TO pruquote_deploy;
GRANT EXECUTE ON FUNCTION ap.get_tax_rate(timestamp with time zone, text) TO postgres;
-- Function: ap.spgen_edusmart(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor)

-- DROP FUNCTION ap.spgen_edusmart(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor);

CREATE OR REPLACE FUNCTION ap.spgen_edusmart(
    aid bigint,
    alang character,
    atype character,
    c_r1 refcursor,
    c_r2 refcursor,
    c_r3 refcursor,
    c_r4 refcursor,
    c_r5 refcursor,
    c_r6 refcursor,
    c_r7 refcursor,
    c_r8 refcursor)
  RETURNS SETOF refcursor AS
$BODY$

declare mb decimal(18,2);
	sa decimal(18,2);
	mbsa decimal(18,2);
		
	policyterm varchar(20); -- decimal(18,2); 
	premiumterm varchar(20); -- decimal(18,2);
	 
	abasicplanprd varchar(4);
	mb_method varchar(12);
	
	basicplanname varchar(1000);
	rtr1name varchar(1000);
	rsr1name varchar(1000);
	rtr1name_ben varchar(1000);
 	rsr1name_ben varchar(1000);
 	-- prutect
 	pad1name varchar(1000);
 	pad1name_ben varchar(1000);
 	-- end of prutect
	tt1 varchar(1000);
	tt2 varchar(1000);
	tt3 varchar(1000);
	tt4 varchar(1000);
	tt5 varchar(1000);
	proname varchar(1000);
	large_sa_rebate decimal(18,2);
	prusaver varchar(1000);
        lblpruretirement int;
	edusavelogo varchar(100);
	educarelogo varchar(200);
	pdiscountrate numeric(18,2);
	rcd  timestamp with time zone;

BEGIN

	--drop table if exists param;
        create temporary table param on commit drop as			
        select * from ap.vpruquote_parms a where a.id =aid;
        
-- 	update param set product=lower('BTL1') WHERE lower(Product2)=lower('BTL1'); 

	rcd =(select l.commdate from param l);
	abasicplanprd = (select l.product from param l);
	premiumterm =(select l.premiumterm from param l);
	policyterm =(select (l.policyterm::int - 5) as policyterm from param l);
	
	mb_method = (case when lower(abasicplanprd) like 'btr4%' then 'lum' else '***' end);

	create temporary table temp on commit drop as			
        select (product||l.paymenttype||l.paymentmode)::varchar(20) as  modalfactor
			,(l.product||mb_method||l.policyterm) as mbcode
			,(cast((select l1.rate from ap.vmbrate l1 where lower(mbrate)=lower(l.product||mb_method||l.policyterm) order by l1.rate limit 1 ) as decimal(18,2))/100.0) as mbrate
			,abasicplanprd::char(10) as discountcode
			,(select t1.rate from ap.vdiscountrate t1 where lower(t1.discountrate)=lower(abasicplanprd) and l.basicplan_sa::decimal(18,2) between t1.fromsa and t1.tosa) as discountrate
			,l.id			
			,l.product
			,l.commdate
			,l.cola1relationship
			,l.relationship
			,upper(l.poname) as poname
			,cast((case when cola1relationship::char(2)='13' then cola1sex::char(20) else 'n/a'::char(20) end) as char(20)) as posex
			,cast((case when cola1relationship::char(2)='13' then la1age::char(20) else 'n/a'::char(20) end) as char(20)) as poage
			,upper(l.la1name) as la1name
			,l.la1dateofbirth
			,cast(l.la1age as char(50)) as la1age
			,l.cola1sex
			,l.la1sex
			,l.la1sex2
			,l.cola1occupationclass
			,l.la1occupationclass
			,l.copolicyterm
			,l.policyterm
			,l.copremiumterm
			,right(('000'||cast(l.premiumterm as char(3))),2) as premiumterm
 			,upper(l.la2name) as la2name
			,l.la2dateofbirth
			,cast(l.la2age as char(50)) as la2age
			,l.cola2sex
			,l.la2sex
			,l.la2sex2
			,l.cola2occupationclass
			,l.la2occupationclass
			,l.copaymenttype
			,l.paymenttype
			,l.paymenttype2
			,l.copaymentmode
			,l.paymentmode
			,l.paymentmode2 as "PaymentMode2"
			,l.basicplan_sa
			,l.col1_rtr1
			,l.l1_rtr1_flag
			,l.l1_rtr1
			,l.col1_rsr1
			,l.l1_rsr1_flag
			,l.l1_rsr1
			,l.col2_rtr1
			,l.l2_rtr1_flag
			,l.l2_rtr1
			,l.col2_rsr1
			,l.l2_rsr1_flag
			,l.l2_rsr1
			,l.validflag
			,l.couser
			,l.syndate
			,l.col1_rtr2
			,l.l1_rtr2_flag
			,l.l1_rtr2
			,l.pruretirement
			-- prutect
			,l.la1_pad1_sa
			,l.la1_pad1_flag
			,l.la2_pad1_sa
			,l.la2_pad1_flag
			-- end of prutect
			from param l;

	edusavelogo='<img src="/PruQuote/resources/images/edusave_log_small.jpg" style="width: 53px; height: 13px" />';
	educarelogo='<img src="/PruQuote/resources/images/edusmart_logo.png" style="height:30px;vertical-align:middle;" />';
	

	if lower(alang)=lower('khmer') then
		basicplanname=(select (l.reserve2||'') from ap.vitem2 l where lower(l.tablname)=lower('productcode') and lower(l.itemlatin)=lower(abasicplanprd));
		rtr1name=(select l.itemkhmer from ap.vitem2 l where lower(l.tablname)=lower('productcode') and lower(l.itemlatin)=lower('rtr1'));
		rsr1name=(select l.itemkhmer from ap.vitem2 l where lower(l.tablname)=lower('productcode') and lower(l.itemlatin)=lower('rsr1'));
		rtr1name_ben=' អត្ថប្រយោជន៍សរុបនឹងផ្តល់ជូនក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍ ';
		rsr1name_ben='អត្ថប្រយោជន៍នឹងផ្តល់ជូនរៀងរាល់ឆ្នាំរហូតដល់កាលបរិចេ្ឆទផុតកំណត់​នៃបណ្ណសន្យារ៉ាប់រង ករណីទទួលមរណភាពឬពិការភាព​ទាំងស្រុង និងជាអចិន្រ្តៃយ៍';
		-- prutect
		pad1name = (select l.itemkhmer from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)='pad1'); 
		pad1name_ben='អត្ថប្រយោជន៏សរុបនឹងត្រូវ​ទូទាត់​ជូន​ ក្នុងករណីអ្នក​ត្រូវបាន​ធានា​រ៉ាប់រង​នៃ​លក្ខខណ្ឌ​បន្ថែម​នេះទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍​ ដែល​បណ្តាល​មក​ពី​គ្រោះថ្នាក់ចៃដន្យ​ ក្នុង​រយៈពេល​កំណត់​នៃ​បណ្ណសន្យារ៉ាប់រង​ ឬ​រយៈពេល​ធានារ៉ាប់រង​អាយុ​ជីវិត​បន្ថែម។';
		-- end of prutect
	    
		update temp t1 set la1sex2=lower(t2.itemkhmer) from ap.vitem2 t2 where lower(t1.cola1sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set posex=lower(t2.itemkhmer) from ap.vitem2 t2 where lower(t1.posex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set la2sex2=lower(t2.itemkhmer) from ap.vitem2 t2  where lower(t1.cola2sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set "PaymentMode2"=lower(t2.itemkhmer) from ap.vitem2 t2 where lower(t1.copaymentmode::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymentmode';
		update temp t1 set paymenttype2=lower(t2.itemkhmer) from ap.vitem2 t2  where lower(t1.copaymenttype::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymenttype';
		
		tt1='បុព្វលាភធានារ៉ាប់រងសរុបមុនពេលបញ្ចុះតម្លៃ (US$)';
		tt2='ការបញ្ចុះតម្លៃសម្រាប់ទឹកប្រាក់ត្រូវបានធានារ៉ាប់រងសរុបខ្នាតធំ (%)';
		tt3='បុព្វលាភធានារ៉ាប់រងសរុបក្រោយពេលបញ្ចុះតម្លៃ (US$)';
		tt4='ពន្ធអាករ (US$)';
		tt5='បុព្វលាភធានារ៉ាប់រងជាដំណាក់កាលដែលត្រូវបង់​ រួមទាំងពន្ធ  (US$)';
	
	else	
		basicplanname=(select (l.reserve2||'') from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)=lower(abasicplanprd));
		rtr1name=(select l.reserve2 from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)='rtr1');
		rsr1name=(select l.reserve2 from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)='rsr1');
		rtr1name_ben='The benefit amount is payable once on death or TPD.';
		rsr1name_ben='The benefit amount is payable at every policy anniversary till maturity, starting from the anniversary immediately after the insured event occurs.';
		-- prutect
		pad1name = (select l.reserve2 from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)='pad1'); 
		pad1name_ben='The benefit amount is payable once, in case of death/TPD of Life Assured for this rider caused due to an accident during the Policy Term or Extended Life Coverage Term.';
		-- end of prutect
		
		update temp t1 set la1sex2=t2.reserve2 from ap.vitem2 t2  where lower(t1.cola1sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set posex=t2.reserve2 from ap.vitem2 t2  where lower(t1.posex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set la2sex2=t2.reserve2 from ap.vitem2 t2   where lower(t1.cola2sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set "PaymentMode2"=t2.reserve2 from ap.vitem2 t2  where lower(t1.copaymentmode::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymentmode';
		update temp t1 set paymenttype2=t2.reserve2 from ap.vitem2 t2 where lower(t1.copaymenttype::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymenttype';
	
		tt1='Total Premium Before Discount (US$)';
		tt2='Large Sum Assured Rebates (%)';
		tt3='Total Premium After Discount (US$)';
		tt4='Applicable Tax (US$)';
		tt5='Total Installment Premium Payable (US$)';


	end if;	

	update temp set la2name='',la2sex='',la2sex2='',la2age=''  
	where l2_rtr1_flag='no' and l2_rsr1_flag='no' and la2_pad1_flag='no'; -- add prutect condition

--Result R1
	if atype='R1' or atype='RN' then 
		open c_R1 FOR
		select * from temp;
		return next c_R1;
        end if;	

	create temporary table tmpto on commit drop as
	select aid as coparms
		,'01'::char(2) as orderno
		,l.product as product
		,cast(basicplanname as char(1000)) as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.basicplan_sa as sa
		,cast((l.basicplan_sa * l.mbrate) as decimal(18,2)) as mb
		,l.policyterm
		,l.premiumterm
 		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode::char(10) as discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product::char(10)||l.policyterm::char(10)||'c'||l.la1sex::char(10))::char(10) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(l.product||l.premiumterm||'c'||l.la1sex) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,l.l1_rtr2 as sa1
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower('rtr2'||l.premiumterm||'c'||l.la1sex) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate1	-- to calc the premium rate
		,educarelogo as logo
	from temp l;

	insert into tmpto
	select aid as coparms
		,'02'::char(2) as orderno
		,'rtr1'::char(4) as product
		,rtr1name as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.l1_rtr1 as sa
		,(l.l1_rtr1 * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la1sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rtr1'||l.premiumterm||'c'||l.la1sex)) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate		 
		,0.0
		,0.0
	from temp l where lower(l.l1_rtr1_flag)='yes';	

	insert into tmpto
	select aid as coparms
		,'03'::char(2) as orderno
		,'rsr1'::char(4) as product
		,rsr1name as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate  as effective
		,l.la1sex as sex
		,l.l1_rsr1 as sa
		,(l.l1_rsr1 * l.mbrate) as mb 
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la1sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rsr1'||l.premiumterm||'c'||l.la1sex)) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,0.0
		,0.0
	from temp l where lower(l.l1_rsr1_flag)='yes';

	-- prutect life 1
	insert into tmpto
	select aid as coparms
		,'04'::char(2) as orderno
		,'pad1'::char(4) as product
		,pad1name as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.la1_pad1_sa as sa
		,(l.la1_pad1_sa * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la1sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('pad1'||l.premiumterm||'c'||l.la1sex)) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate		 
		,0.0
		,0.0
	from temp l where lower(l.la1_pad1_flag)='yes';
	-- end of prutect life 1

	insert into tmpto
	select aid as coparms
		,'05'::char(2) as orderno
		,'rtr1'::char(4) as product
		,rtr1name as productname
		,'la2'::char(3) as life
		,l.la2name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.l2_rtr1 as sa
		,(l.l2_rtr1 * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la2sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rtr1'||l.premiumterm||'c'||l.la2sex)) and l.la2age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,0.0
		,0.0
	from temp l where lower(l.l2_rtr1_flag)='yes';

	insert into tmpto
	select aid as coparms
		,'06'::char(2)  as orderno
		,'rsr1'::char(4) as product
		,rsr1name as productname
		,'la2'::char(3) as life
		,l.la2name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.l2_rsr1 as sa
		,(l.l2_rsr1 * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la2sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rsr1'||l.premiumterm||'c'||l.la2sex)) and l.la2age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,0.0
		,0.0
	from temp l where lower(l.l2_rsr1_flag)='yes';

	-- prutect life 2
	insert into tmpto
	select aid as coparms
		,'07'::char(2) as orderno
		,'pad1'::char(4) as product
		,pad1name as productname
		,'la1'::char(3) as life
		,l.la2name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.la2_pad1_sa as sa
		,(l.la2_pad1_sa * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la1sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('pad1'||l.premiumterm||'c'||l.la1sex)) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate		 
		,0.0
		,0.0
	from temp l where lower(l.la2_pad1_flag)='yes';
	-- end of prutect life 2
--Result 2
	if atype='R2' or atype='RN' then
		open c_R2 FOR
		select l.coparms,l.orderno
		,(case when lower(l.productname)='edusave' then edusavelogo
					   else  productname end)::char(300) as productname
		,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa	
		,cast(round(l.sa *(premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2)) as premiumamt
		--,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as premiumamt
		-- monthly
		,(l.product||l.paymenttype||'12 | '||l.product||'c'||l.sex)::char(20) as p12_code
		,(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),null),0) * premiumrate) as p12_rate
		,(cast(round(l.sa *(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),null),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p12_amt
		-- semi-annually
		,(l.product||l.paymenttype||'02 | '||l.product||'c'||l.sex)::char(20) as p02_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(l.product||l.paymenttype||'02')),0) * premiumrate) as p02_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p02_amt
		-- annually
		,(l.product||l.paymenttype||'01 | '||l.product||'c'||l.sex) as p01_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate) as p01_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p01_amt
		-- annualize before discount (c,01)
		,(l.product||'c'||'01 | '||l.product||'c'||l.sex) as pa1_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate) as pa1_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as pa1_amt
		,logo
		from tmpto l
		where lower(l.product) like lower('B%')
		order by coparms,orderno;
		return next c_R2;
	end if;

	if prusaver::char(3) = 'yes'::char(3) then
		-- pru saver
		create temporary table tmpsaver1 on commit drop as
		select l.coparms,l.orderno
			,(case when lower(l.productname)='pru myfamily' and lower(alang)='khmer' then '<span class="Pru"><b>pru</b></span><i style="text-transform: lowercase; font-size:10px;color:black;">គ្រួសារខ្ញុំ</i>' 
						when lower(l.productname)='pru myfamily' and lower(alang)='latin' then '<span class="Pru"><b>pru</b></span><i class="product">my family</i>'
						when l.productname='edusave' then '<img src="/PruQuote/resources/images/edusave_log_small.jpg" style="width: 53px; height: 13px" />'
					else  productname end)::char(300) as productname
			,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa1
			,cast(round(l.sa1 *(premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2)) as premiumamt
			-- monthly
			,(l.product||l.paymenttype||'12 | '||l.product||'c'||l.sex)::char(20) as p12_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower('rtr2'||l.paymenttype||'12')),0) * premiumrate1) as p12_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower('rtr2'||l.paymenttype||'12')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p12_amt
			-- semi-annually
			,(l.product||l.paymenttype||'02 | '||l.product||'c'||l.sex)::char(20) as p02_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(l.product||l.paymenttype||'02')),0) * premiumrate1) as p02_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower('rtr2'||l.paymenttype||'02')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p02_amt
			-- annually
			,(l.product||l.paymenttype||'01 | '||l.product||'c'||l.sex)::char(20) as p01_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate1) as p01_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower('rtr2'||l.paymenttype||'01')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p01_amt
			-- annualize before discount (c,01)
			,(l.product||'c'||'01 | '||l.product||'c'||l.sex)::char(20) as pa1_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate1) as pa1_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower('rtr2'||'c'||'01')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as pa1_amt

		from tmpto l
		where lower(l.product) like lower('b%')
		order by coparms,orderno;
	end if;

	large_sa_rebate=0.00;
	large_sa_rebate=(select discountrate from tmpto limit 1);

	create temporary table tmpto_tt on commit drop as
	select l.coparms,l.orderno,l.productname,product,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa	
		-- ap.vmodalfactor : to get the rate for premium modal loading factor (monthlt, semi-annully, yearly)
		-- vpremiumratelisting : to get the rate for calc 1 year premium reference to product
		,cast(l.sa *(premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)) as decimal(18,2)) as premiumamt
		--,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as premiumamt
		-- monthly
		,(l.product||l.paymenttype||'12 | '||l.product||'c'||l.sex) as p12_code
		,(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),null),0) * premiumrate) as p12_rate
		,round((l.sa *(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),null),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end))),2) as p12_amt
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as sad12
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end))* ((100-large_sa_rebate)/100) * 0.000 ) as ftax12
		-- semi-annually
		,(l.product||l.paymenttype||'02 | '||l.product||'c'||l.sex) as p02_code
		,(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(l.product||l.paymenttype||'02')),null),0) * premiumrate) as p02_rate
		,(cast(round(l.sa *(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),null),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p02_amt
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as sad02
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100) * 0.000 ) as ftax02

		-- annually
		,(l.product||l.paymenttype||'01 | '||l.product||'c'||l.sex) as p01_code
		,(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),null),0) * premiumrate) as p01_rate
		,(cast(round(l.sa::decimal(18,2) *(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower((abasicplanprd::char(5)||l.paymenttype::char(5)||'01'::char(2)))),null),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p01_amt
		-- ,(cast(l.sa as char),'-',cast(l.premiumrate as char),'-',l.product )as  p01_amt 
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as sad
		-- ,round((round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=(abasicplanprd,l.paymenttype,'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) * ((100-large_sa_rebate)/100) * 0.000 ),2) as ftax			
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100) * 0.000 ) as ftax			

		-- annualize before discount (c,01)
		,(l.product||'c'||'01 | '||l.product||'c'||l.sex) as pa1_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate) as pa1_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as pa1_amt
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as aad
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100) * 0.000 ) as atax			
		-- ,round((round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=(abasicplanprd,'c','01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) * ((100-large_sa_rebate)/100) * 0.000 ),2) as atax
	from tmpto l
	-- where l.product not like 'b%'
	order by coparms,orderno;

	create temporary table tmpdata1 on commit drop as
	select l.coparms
		,'11'::char(2) as orderno
		,tt1 as productname
		,'TT'::char(2) as product
		,''::char(1) as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb 
		,0 as sa
		,0 as premiumamt
		,'t12'::char(3) as p12_code
		,0 as p12_rate
		,sum(p12_amt) as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02' as p02_code
		,0 as p02_rate
		,sum(p02_amt) as p02_amt
		,0 as sad02
		,0 as ftax02
		,'t01' as p01_code
		,0 as p01_rate
		,SUM(p01_amt) as p01_amt
		,0 as sad
		,0 as stax
		
		,'ta1' as pa1_code
		,0 as pa1_rate
		,sum(pa1_amt) as pa1_amt
		,0 as aad
		,0 as atax
	from tmpto_tt l 
	where lower(l.product) not like LOWER('TT%') 
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata1;
	
	create temporary table tmpdata2 on commit drop as
	select l.coparms
		,'12'::char(2) as orderno
		,tt2 as productname
		,'TT'::char(2) as product
		,'' as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa
		,0 as premiumamt
		,'t12'::char(3) as p12_code
		,0 as p12_rate
		,large_sa_rebate as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02'::char(3) as p02_code
		,0 as p02_rate
		,large_sa_rebate as p02_amt
		,0 as sad02
		,0 as ftax02
		,'t01'::char(3) as p01_code
		,0 as p01_rate
		,large_sa_rebate as p01_amt
		,0 as sad
		,0 as ftax
		,'ta1'::char(3) as pa1_code
		,0 as pa1_rate
		,large_sa_rebate as pa1_amt
		,0 as aad
		,0 as atax		
	from tmpto_tt l 
	where lower(l.product) not like LOWER('TT%') 
	group by coparms;

	insert into tmpto_tt
	select * from tmpdata2;

	create temporary table tmpdata3 on commit drop as
	select l.coparms
		,'13'::char(2) as orderno
		,tt3 as productname
		,'TT'::char(2) as product
		,''::char(1) as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa
		,0 as premiumamt
		,'t12'::char(3) as p12_code
		,0 as p12_rate
		-- ,(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)) as p12_amt
		,round(sum(round(sad12,2)),2) as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02'::char(3) as p02_code
		,0 as p02_rate
		-- ,(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)) as p02_amt
		,round(sum(round(sad02,2)),2) as p02_amt
		,0 as sad02
		,0 as ftax02
		,'t01'::char(3) as p01_code
		,0 as p01_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,round(sum(round(sad,2)),2) as p01_amt
		,0 as sad
		,0 as ftax
		,'ta1'::char(3) as pa1_code
		,0 as pa1_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,round(sum(round(aad,2)),2) as pa1_amt
		,0 as aad
		,0 as atax
	from tmpto_tt l 
	where l.product not like LOWER('TT%') 
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata3;

	if prusaver::char(3) = 'yes'::char(3) then
		-- prusaver header
		-- select * from tmpsaver1;
		create temporary table tmpdata6 on commit drop as
		select l.coparms
		,'14'::char(2) as orderno
		,'<tr><td class="border2" style="width:170px;text-align:left;"><u>additional rider:</u></td><th class="border2" style="width:"><u>life assured</u></th><th class="border2" style="width:40px"><u>policy term</u></th><th class="border2" style="width:75px"><u>premium payment term</u></th><th class="border2" style="width:75px"><u>maturity benefit (us$)</u></th><th class="border2" style="width:70px"><u>monthly (us$)</u></th><th class="border2" style="width:100px"><u>semi-annual (us$)</u></th><th class="border2" style="width:70px"><u>annual (us$)</u></th></tr>'::char(3000) as productname
		,'TH_RTR2'::char(10) as product
		,''::char(1) as lifeassured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa1
		,0 as premiumamt
		,'t12'::char(20) as p12_code
		,0 as p12_rate
		-- ,(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)) as p12_amt
		,0 as p12_amt
		,0 as sad12 -- for calculation tax
		,0 as ftax12
		,'t02'::char(20) as p02_code
		,0 as p02_rate
		-- ,(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)) as p02_amt
		,0 as p02_amt
		,0 as sad02 -- for calculation tax
		,0 as ftax02
		,'t01'::char(20) as p01_code
		,0 as p01_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,0 as p01_amt
		,0 as sad -- for calculation tax
		,0 as ftax

		,'ta1'::char(20) as pa1_code
		,0 as pa1_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,0 as pa1_amt
		,0 as aad
		,0 as atax
		from tmpsaver1 l; 
		
		insert into tmpto_tt
		select * from tmpdata6;

		create temporary table tmpdata7 on commit drop as
		select l.coparms
		,'15'::char(2) as orderno
		,case when lower(alang)='khmer' then 'អត្ថប្រយោជន៍​​​  Long-Term Savings Builder' 
			  else ' Long-Term Savings Builder' end as productname
		,'RTR2'::char(4) as product
		, life_assured
		, age
		,l.policyterm
		,l.premiumterm
		,l.mb
		,sa1
		,premiumamt
		,'t12'::char(3) as p12_code
		,p12_rate
		-- ,(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)) as p12_amt
		,p12_amt
		,p12_amt as sad12 -- for calculation tax
		,0 as ftax12
		,'t02'::char(3) as p02_code
		,p02_rate
		-- ,(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)) as p02_amt
		,p02_amt
		,p02_amt as sad02 -- for calculation tax
		,0 as ftax02
		,'t01'::char(3) as p01_code
		,p01_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,p01_amt
		,p01_amt as sad -- for calculation tax
		,0 as ftax

		,'ta1'::char(3) as pa1_code
		,0 as pa1_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,pa1_amt
		,0 as aad
		,0 as atax
		from tmpsaver1 l; 
		
		insert into tmpto_tt
		select * from tmpdata7;	
	end if;

	create temporary table tmpdata4 on commit drop as
	select l.coparms
	,'16'::char(2) as orderno
	,tt4 as productname
	,'TT'::char(2) as product
	,'' as life_assured
	,0 as age
	,0 as policyterm
	,0 as premiumterm
	,0 as mb
	,0 as sa
	,0 as premiumamt
	,'t12'::char(3) as  p12_code
	,0 as p12_rate
	-- ,(round(sum(round((p12_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p12_amt
	,round(sum(round(round(sad12,2)*(case when l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2)),2) as p12_amt
	-- ,cast(sum(ftax12) as char) as p12_amt
	,0 as sad12
	,0 as ftax12
	,'t02'::char(3) as p02_code
	,0 as p02_ratea
	-- ,(round(sum(round((p02_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p02_amt
	,round(sum(round(round(sad02,2)*(case when  l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2)),2) as p02_amt
	,0 as sad02
	,0 as ftax02
	,'t01'::char(3) as p01_code
	,0 as p01_rate
	-- ,(round(sum(round((p02_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p02_amt
	,round(sum(round(round(sad,2)*(case when  l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2)),2) as p01_amt
	,0 as sad
	,0 as ftax

	,'ta1'::char(3) as pa1_code
	,0 as pa1_rate
	-- ,(round(sum(round((p01_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p01_amt
	,round(sum(round(atax,2)),2) as pa1_amt
	,0 as aad
	,0 as atax
	from tmpto_tt l 
	where l.product not like LOWER('TT%') 
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata4;

	create temporary table tmpdata5 on commit drop as
	select l.coparms
	,'17'::char(2) as orderno
	,tt5 as productname
	,'TT'::char(2) as product
	,'' as life_assured
	,0 as age
	,0 as policyterm
	,0 as premiumterm
	,0 as mb
	,0 as sa
	,0 as premiumamt
	,'t12'::char(3) as p12_code
	,0 as p12_rate
	-- ,(round((sum(p12_amt)*(100-large_sa_rebate)/100)*1.0,2) + round(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)*0.000 ,2)) as p12_amt
	-- ,(round(sum(round(sad12,2)),2))+(round((sum(round(ftax12,2))),2)) as p12_amt
	,(round(sum(round(sad12,2)),2))+(round((sum(round(round(sad12,2)*(case when  l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2))),2)) as p12_amt
	,0 as sad12
	,0 as ftax12
	,'t02'::char(3) as p02_code
	,0 as p02_rate
	-- ,(round((sum(p02_amt)*(100-large_sa_rebate)/100)*1.0,2) + round(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)*0.000 ,2)) as p02_amt
	-- ,(round(sum(round(sad02,2)),2))+(round((sum(round(ftax02,2))),2)) as p02_amt
	,(round(sum(round(sad02,2)),2))+(round((sum(round(round(sad02,2)*(case when  l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2))),2)) as p02_amt
	,0 as sad02
	,0 as ftax02

	,'t01'::char(3) as p01_code		
	,0 as p01_rate
	-- ,(round((sum(p01_amt)*(100-large_sa_rebate)/100)*1.0,2) + (sum(ftax)*(100-large_sa_rebate)/100)) as p01_amt
	-- ,(round(sum(round(sad,2)),2))+(round((sum(round(ftax,2))),2)) as p01_amt
	,(round(sum(round(sad,2)),2))+(round((sum(round(round(sad,2)*(case when  l.product<>'rtr2'  then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2))),2)) as p01_amt
	-- ,concat(cast(sum(sad) as char),'-',cast(sum(ftax) as char)) as p01_amt
	,0 as sad
	,0 as ftax
	,'ta1'::char(3) as pa1_code		
	,0 as pa1_rate
	-- ,(round((sum(p01_amt)*(100-large_sa_rebate)/100)*1.0,2) + (sum(ftax)*(100-large_sa_rebate)/100)) as p01_amt
	,(round(sum(round(aad,2)),2))+(round((sum(round(atax,2))),2)) as pa1_amt
	-- ,concat(cast(sum(aad) as char),'-',cast(sum(atax) as char)) as a1
	,0 as aad
	,0 as atax

	from tmpto_tt l 
	where l.product not like LOWER('TT%')
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata5;

	if (atype='R3' or atype='RN') then
		pdiscountrate = (select p01_amt from tmpto_tt where orderno = '12' limit 1)::numeric(18,2);
		RAISE NOTICE 'discount rate: %', pdiscountrate;
		-- Requested by Actuarial 20171018
		-- Remove related discount rows if discount rate is 0
		if pdiscountrate = 0.00 then
			update tmpto_tt
			set productname = CASE WHEN lower(alang) = 'khmer' THEN 'បុព្វលាភធានារ៉ាប់រងសរុប (US$)' ELSE 'Total Premium (US$)' END
			where orderno = '11';

			delete from tmpto_tt
			where orderno in('12','13');
		end if;
		
		OPEN c_r3 FOR
		select l.coparms,l.orderno,l.productname,product,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa	
		,premiumamt
		, p12_code
		,p12_rate
		,p12_amt
		,p02_code
		,p02_rate
		,p02_amt
		-- annually
		,p01_code
		,p01_rate
		,p01_amt
		-- annually
		,pa1_code
		,pa1_rate
		,pa1_amt
		from tmpto_tt l order by coparms,orderno;
		RETURN NEXT c_r3;	
	end if;

	create temporary table tmpmbrate on commit drop as
	select t.tablid ,t.tablname,s.*
	from ap.vmbrate s,ap.vitem2 t 
	where lower(s.combrate::char(10))=lower(t.itemid::char(10));

	if(atype='R4' or atype='RN') then
		OPEN c_r4 FOR
		select t.coparms
		,t.policyterm::integer+l.year::integer as policyterm
		,t.age::integer+l.year::integer+(t.policyterm::integer-1) as age
		,round((case when l.year::integer=0 then t.mb else 0.0 end),2) as opt1_lumpsum
		,round((case when l.year::integer=0 then t.mb else 0.0 end),2) as opt1_tt
		,round(cast(t.sa*l.rate/100.0 as decimal(18,2)),2) as opt2_inst
		,cast( round(case when l.year::integer=0 then t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr4lum'||t.policyterm))/100.0
							 when l.year=1 then t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr4lum'||t.policyterm))/100.0
							 when l.year=2 then t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr4lum'||t.policyterm))/100.0
							 else t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr4lum'||t.policyterm))/100.0 end 
							 ,2)
							 as decimal(18,2)) as opt1_inst_tt
		from tmpmbrate l,tmpto t
		where lower(l.mbrate) like lower('btr4lum'||t.policyterm::char(2)) and lower(t.product) like 'btr4'
		order by coparms,year;
		RETURN NEXT c_r4;
	end if; 

	create temporary table tmpsurrender on commit drop as
	select t.tablid ,t.tablname,s.*
	from ap.vsurrender s,ap.vitem2 t 
	where s.cosurrender::char(10)=t.itemid::char(10)
	and t.tablid::integer=21;

	create temporary table tmplf_ben on commit drop as
	select t.coparms
	,cast(yearinforce as integer) as policyyear
	,(cast(yearinforce::integer+t.age::integer as integer)-1) as age
-- 	,cast((case when lower(product)=lower('btl1')
-- 	then (case when yearinforce::integer<=5  
-- 		  then (cast(round(t.premiumamt,2) as decimal(18,4))) else 0 end) 
-- 	else (cast(round(t.premiumamt,2) as decimal(18,4))) 
-- 	end
-- 	) as char(50)) as p_ape
-- 	,(cast(round(round(t.premiumamt,2) * (  case when lower(product)=lower('btl1')
-- 									then (case when yearinforce::integer<=5 
-- 											      then yearinforce::integer else 5 
-- 										  end) 
-- 								 else yearinforce::integer				
-- 							end) 
-- 	,2) as decimal(18,2)
-- 	)) as p_total
	,cast((case when lower(product)=lower('btl1')
		then (case when yearinforce::integer<=5  
			  then (cast(round(t.p01_amt,2) as decimal(18,4))) else 0 end) 
		else (cast(round(t.p01_amt,2) as decimal(18,4))) 
		end
		) as char(50)) as p_ape
	,(cast(round(round(t.p01_amt,2) * (  case when lower(product)=lower('btl1')
						then (case when yearinforce::integer<=5 
							then yearinforce::integer else 5 end) 
							else yearinforce::integer end),2) as decimal(18,2)
		)) as p_total
	,(cast(round(t.sa,2) as decimal(18,2))) as clam_non_accident
	,(cast(round(t.sa*0.1,2) as decimal(18,2))) as aib_non_accident
	,(cast(round(t.sa*2,2) as decimal(18,2))) as clam_accident
	,(cast(round(t.sa*0.1,2) as decimal(18,2))) as aib_accident
	,(cast(l.rate as decimal(18,2))) as surrender_rate
	,(trunc(case when yearinforce::integer <> t.policyterm::integer then (t.premiumamt*( case when lower(product)='btl1' 
				   then (case when yearinforce::integer<=5 then yearinforce::integer else 5 end)
			   else yearinforce::integer
		  end) * l.rate/100.00) else t.sa*1 end, 2)) as surrender
	from tmpsurrender l,tmpto_tt t
	where l.surrender like (abasicplanprd::char(10)||t.policyterm::char(10)) and lower(t.product) like 'bt%' and l.yearinforce::integer<=t.policyterm::integer
	order by yearinforce;

	update tmplf_ben set p_ape='' where p_ape::decimal(18,2)=0.00;

	if (atype='R5' or atype='RN') then
		OPEN c_r5 for
		select * from tmplf_ben order by coparms,policyyear ;
		RETURN NEXT c_r5;
		-- print '3. benefits with additional riders, that help the family in the further securing child''s education and future '
		create temporary table tmprider_ben on commit drop as
		select l.coparms,l.orderno,productname,life_assured
		,(cast(round(l.sa,2) as decimal(18,2))) as clam_non_accident
		,(cast(round(l.sa*2,2) as decimal(18,2))) as clam_accident
		,cast(rtr1name_ben as char(500)) as payment_terms
		from tmpto_tt l
		where l.product like 'rtr1';
	end if ;

	insert into tmprider_ben
	select l.coparms,l.orderno,productname,life_assured
	,cast(round(l.sa,2) as decimal(18,2)) as clam_non_accident
	,cast(round(l.sa,2) as decimal(18,2)) as clam_accident
	,cast(rsr1name_ben as char(500)) as payment_terms
	from tmpto_tt l
	where lower(l.product) like 'rsr1';

	insert into tmprider_ben
	select l.coparms,l.orderno,productname,life_assured
	,cast(round(0,2) as decimal(18,2)) as clam_non_accident
	,cast(round(l.sa,2) as decimal(18,2)) as clam_accident
	,cast(pad1name_ben as char(500)) as payment_terms
	from tmpto_tt l
	where lower(l.product) like 'pad1';

	update tmprider_ben set productname = replace(productname,'(sum assured)','') where 1=1;

	open c_r6 for
	select * from tmprider_ben order by coparms,orderno;
	return next c_r6;

	-- Extended life coverage calculation
	create temporary table tmplf_ben_last on commit drop as
		select * from tmplf_ben order by coparms desc, policyyear desc limit 1;
	
	create temporary table tmplf_ext on commit drop as
		select coparms,1 as policyyear,(age+1) as age,'0.00' as p_ape,p_total,clam_non_accident,0::numeric(18,2) as aib_non_accident
		,clam_accident,0::numeric(18,2) as aib_accident,0::numeric(18,2) as surrender_rate,0::numeric(18,2) as surrender
		from tmplf_ben_last
		union
		select coparms,2 as policyyear,(age+2) as age,'0.00' as p_ape,p_total,clam_non_accident,0::numeric(18,2) as aib_non_accident
		,clam_accident,0::numeric(18,2) as aib_accident,0::numeric(18,2) as surrender_rate,0::numeric(18,2) as surrender
		from tmplf_ben_last
		union
		select coparms,3 as policyyear,(age+3) as age,'0.00' as p_ape,p_total,clam_non_accident,0::numeric(18,2) as aib_non_accident
		,clam_accident,0::numeric(18,2) as aib_accident,0::numeric(18,2) as surrender_rate,0::numeric(18,2) as surrender
		from tmplf_ben_last
		union
		select coparms,4 as policyyear,(age+4) as age,'0.00' as p_ape,p_total,clam_non_accident,0::numeric(18,2) as aib_non_accident
		,clam_accident,0::numeric(18,2) as aib_accident,0::numeric(18,2) as surrender_rate,0::numeric(18,2) as surrender
		from tmplf_ben_last
		union
		select coparms,5 as policyyear,(age+5) as age,'0.00' as p_ape,p_total,clam_non_accident,0::numeric(18,2) as aib_non_accident
		,clam_accident,0::numeric(18,2) as aib_accident,0::numeric(18,2) as surrender_rate,0::numeric(18,2) as surrender
		from tmplf_ben_last;

	open c_r7 for
	    select * from tmplf_ext order by coparms,policyyear ;
	return next c_r7;

	-- end of Extended life coverage calculation

	if lower(prusaver::char(3)) = lower('yes') then
		-- print '1. guaranteed maturity benefit with pru saver'
		if (atype='R7' or atype='RN') then
			open c_r7 for
			select t.coparms
				,t.policyterm::integer+l.year::integer as policyterm
				,t.age::integer+l.year::integer+t.policyterm::integer-1 as age
				,round((case when l.year=0 then t.sa1 else 0.0 end),2) as opt1_lumpsum
				,round((case when l.year=0 then t.sa1 else 0.0 end),2) as opt1_tt
				,round(cast(t.sa1*l.rate/100.0 as decimal(18,2)),2) as opt2_inst
				,cast( round(case when l.year=0 then t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0
									 when l.year=1 then t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0
									 when l.year=2 then t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0
									 else t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0 end 
									 ,2)
									 as decimal(18,2)) as opt1_inst_tt
			from tmpmbrate l,tmpto t
			where lower(l.mbrate) like ('rtr2set'||t.policyterm) and lower(t.product) like 'btr%'
			order by coparms,year;
			return next c_r7;
		end if;

		mbsa=(select sum(sa1) as mbsa from tmpto);
		-- select policyterm;
		-- select * from tmpto_tt;
		open c_r8 for
		select t.coparms
			,cast(yearinforce as integer) as policyyear
			,(cast(yearinforce::integer+t.age::integer as integer)-1) as age
			,cast((case when lower(product)='btl1' 
					   then (case when yearinforce::integer<=5  
								  then (cast(round(t.premiumamt,2) as decimal(18,4))) else 0 end) 
				   else (cast(round(t.premiumamt,2) as decimal(18,4))) 
			  end
			 ) as char(50)) as p_ape 
			,(cast(round(round(t.premiumamt,2) * (  case	when lower(product)='btl1' then (case when yearinforce::integer<=5 
														then yearinforce::integer else 5 
													end) 
									else yearinforce::integer				
								end) 
						,2) as decimal(18,2)
				)) as p_total
			,(t.premiumamt*1.1*yearinforce::decimal(18,2) ) as clam_non_accident
			,(t.premiumamt*1*1.1*yearinforce::decimal(18,2)) as clam_accident
			,(cast(l.rate as decimal(18,2))) as surrender_rate
			,( case when yearinforce::integer=t.policyterm::integer then mbsa
				else
					(trunc(t.premiumamt*( case when lower(product)='btl1' 
											   then (case when yearinforce::integer<=5 then yearinforce::integer else 5 end)
										   else yearinforce::integer
									  end) * l.rate/100.00 ,2)
					)
				end
			) as surrender
		from tmpsurrender l,tmpto_tt t
		where lower(l.surrender) like lower('rtr2'||t.policyterm::char(3)) and lower(t.product) like 'rtr2%' --and l.yearinforce::integer<=t.policyterm::integer
		order by yearinforce;
		return next c_r8;
	--else
	--	open c_r7 for
	--	select 'No value return' as result;
	--	return next c_r7;
		
	--	open c_r8 for
	--	select 'No value return' as result;
	--	return next c_r8;
			
	end if;

	--CALL IN JAVA PROJECT OR PGSQL
	--***********************************************************************************************
	--	select ap.spgen_edusmart(6,'Khmer','RN','R1','R2','R3','R4','R5','R6','R7','R8');	*
	--	FETCH ALL IN "R1";									*
	--	FETCH ALL IN "R2";									*
	--	FETCH ALL IN "R3";									*
	--	FETCH ALL IN "R4";									*
	--	FETCH ALL IN "R5";									*
	--	FETCH ALL IN "R6";									*
	--	FETCH ALL IN "R7";									*
	--	FETCH ALL IN "R8";									*
	--***********************************************************************************************
	
	--open c_sql2 FOR
	--SELECT * from tmpto;
        --return next c_sql2;
 
	--OPEN c_sql1 FOR
	--    SELECT *
	--    FROM vpruquote_parms;
	--RETURN NEXT c_r3;

	--OPEN c_sql2 FOR
	--    SELECT *
	--    FROM vpruquote_parms;
	--RETURN NEXT c_sql1;
    	
	--RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ap.spgen_edusmart(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor)
  OWNER TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spgen_edusmart(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO public;
GRANT EXECUTE ON FUNCTION ap.spgen_edusmart(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spgen_edusmart(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquotedb_svc;
GRANT EXECUTE ON FUNCTION ap.spgen_edusmart(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquotedb_supp;
GRANT EXECUTE ON FUNCTION ap.spgen_edusmart(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquotedb_deploy;
GRANT EXECUTE ON FUNCTION ap.spgen_edusmart(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquote_svc;
GRANT EXECUTE ON FUNCTION ap.spgen_edusmart(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquote_supp;
GRANT EXECUTE ON FUNCTION ap.spgen_edusmart(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquote_deploy;
GRANT EXECUTE ON FUNCTION ap.spgen_edusmart(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO postgres;

-- Function: ap.spgen_pruquote_mrml(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor)

-- DROP FUNCTION ap.spgen_pruquote_mrml(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor);

CREATE OR REPLACE FUNCTION ap.spgen_pruquote_mrml(
    aid bigint,
    alang character,
    atype character,
    c_r1 refcursor,
    c_r2 refcursor,
    c_r3 refcursor,
    c_r4 refcursor,
    c_r5 refcursor,
    c_r6 refcursor,
    c_r7 refcursor,
    c_r8 refcursor)
  RETURNS SETOF refcursor AS
$BODY$

declare mb decimal(18,2);
	sa decimal(18,2);
	mbsa decimal(18,2);
		
	policyterm varchar(20); -- decimal(18,2); 
	premiumterm varchar(20); -- decimal(18,2);
	 
	abasicplanprd varchar(4);
	mb_method varchar(12);
	
	basicplanname varchar(1000);
	rtr1name varchar(1000);
	rsr1name varchar(1000);
	rtr1name_ben varchar(1000);
 	rsr1name_ben varchar(1000);
	tt1 varchar(1000);
	tt2 varchar(1000);
	tt3 varchar(1000);
	tt4 varchar(1000);
	tt5 varchar(1000);
	proname varchar(1000);
	large_sa_rebate decimal(18,2);
	prusaver varchar(1000);
        lblpruretirement int;
	edusavelogo varchar(100);
	loanassure varchar(200);
	rateinthemonth decimal(18,4);
	rcd  timestamp with time zone;
	
BEGIN

	--drop table if exists param;
        create temporary table param on commit drop as			
        select * from ap.vpruquote_parms a where a.id =aid;
        
	update param set product=lower('BTL1') WHERE lower(Product2)=lower('BTL1'); 

	rcd =(select l.commdate from param l);
	abasicplanprd = (select l.product from param l);
	premiumterm =(select l.premiumterm from param l);
	policyterm =(select l.policyterm from param l);
	policyterm = (case when length(policyterm)=1 then '0'||policyterm else policyterm end);
	
	mb_method = (case when lower(abasicplanprd) like 'btr2%' then 'lum' else '***' end);

	rateinthemonth=(select l.inrate from param l)/12/100;
	
	create temporary table temp on commit drop as			
        select (product||l.paymenttype||l.paymentmode)::varchar(20) as  modalfactor
			,(l.product||mb_method||l.policyterm) as mbcode
			,(cast((select l1.rate from ap.vmbrate l1 where lower(mbrate)=lower(l.product||mb_method||l.policyterm) order by l1.rate limit 1 ) as decimal(18,2))/100.0) as mbrate
			,abasicplanprd::char(10) as discountcode
			,(select t1.rate from ap.vdiscountrate t1 where lower(t1.discountrate)=lower(abasicplanprd) and l.loanamount::decimal(18,2) between t1.fromsa and t1.tosa) as discountrate
			,l.id			
			,l.product
			,l.commdate
			,l.cola1relationship
			,l.relationship
			,upper(l.poname) as poname
			,cast((case when cola1relationship::char(2)='13' then cola1sex::char(20) else 'n/a'::char(20) end) as char(20)) as posex
			,cast((case when cola1relationship::char(2)='13' then la1age::char(20) else 'n/a'::char(20) end) as char(20)) as poage
			,upper(l.la1name) as la1name
			,l.la1dateofbirth
			,cast(l.la1age as char(50)) as la1age
			,l.cola1sex
			,l.la1sex
			,l.la1sex2
			,l.cola1occupationclass
			,l.la1occupationclass
			,l.copolicyterm
			,case when length(l.policyterm)=1 then ('0'||l.policyterm::char(3)) else l.policyterm end as policyterm
			,l.copremiumterm
			,right(('000'||cast(l.premiumterm as char(3))),2) as premiumterm
 			,upper(l.la2name) as la2name
			,l.la2dateofbirth
			,cast(l.la2age as char(50)) as la2age
			,l.cola2sex
			,l.la2sex
			,l.la2sex2
			,l.cola2occupationclass
			,l.la2occupationclass
			,l.copaymenttype
			,l.paymenttype
			,l.paymenttype2
			,l.copaymentmode
			,l.paymentmode
			,l.paymentmode2 as "PaymentMode2"
			,l.basicplan_sa
			,l.col1_rtr1
			,l.l1_rtr1_flag
			,l.l1_rtr1
			,l.col1_rsr1
			,l.l1_rsr1_flag
			,l.l1_rsr1
			,l.col2_rtr1
			,l.l2_rtr1_flag
			,l.l2_rtr1
			,l.col2_rsr1
			,l.l2_rsr1_flag
			,l.l2_rsr1
			,l.validflag
			,l.couser
			,l.syndate
			-- ,nextprog
			-- ,nextprog_link
			,l.col1_rtr2
			,l.l1_rtr2_flag
			,l.l1_rtr2
			,l.pruretirement
			,inrate
			from param l;
			
	prusaver=(select l1_rtr2_flag from temp)::char(20);
	lblpruretirement=(select pruretirement from temp)::char(20);
	if lblpruretirement=16 then
	   	edusavelogo='<img src="/PruQuote/resources/images/edusave_log_small.jpg" style="width: 53px; height: 13px" />';
	else
		edusavelogo='edusave';
	end if;
	loanassure='<img src="/PruQuote/resources/images/mortgage.png" style="width: 53px; height: 13px" />';
	if lower(alang)=lower('khmer') then
		basicplanname=(select (l.reserve2||'') from ap.vitem2 l where lower(l.tablname)=lower('productcode') and lower(l.itemlatin)=lower(abasicplanprd));
		rtr1name=(select l.itemkhmer from ap.vitem2 l where lower(l.tablname)=lower('productcode') and lower(l.itemlatin)=lower('rtr1'));
		rsr1name=(select l.itemkhmer from ap.vitem2 l where lower(l.tablname)=lower('productcode') and lower(l.itemlatin)=lower('rsr1'));
		rtr1name_ben=' អត្ថប្រយោជន៍សរុបនឹងផ្តល់ជូនក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍ ';
		rsr1name_ben='អត្ថប្រយោជន៍នឹងផ្តល់ជូនរៀងរាល់ឆ្នាំរហូតដល់កាលបរិចេ្ឆទផុតកំណត់​នៃបណ្ណសន្យារ៉ាប់រងករណីទទួលមរណភាពឬពិការភាព​ទាំងស្រុង និងជាអចិន្រ្តៃយ៍';
	    
		update temp t1 set la1sex2=t2.itemkhmer from ap.vitem2 t2 where lower(t1.cola1sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set posex=t2.itemkhmer from ap.vitem2 t2 where lower(t1.posex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set la2sex2=t2.itemkhmer from ap.vitem2 t2  where lower(t1.cola2sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set "PaymentMode2"=t2.itemkhmer from ap.vitem2 t2 where lower(t1.copaymentmode::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymentmode';
		update temp t1 set paymenttype2=t2.itemkhmer from ap.vitem2 t2  where lower(t1.copaymenttype::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymenttype';
		
		tt1='បុព្វលាភរ៉ាប់រងសរុបមុន​ពេល​បញ្ចុះតម្លៃ (US$)';
		tt2=case when lower(basicplanname)='pru myfamily' and lower(alang)='khmer' THEN 'ការបញ្ចុះតម្លៃសម្រាប់ទឹកប្រាក់ធានារ៉ាប់រងសរុបខ្នាតធំ(%)' ELSE 'ការបញ្ចុះតម្លៃសម្រាប់ទឹកប្រាក់ធានារ៉ាប់រងសរុបខ្នាតធំ(%)' END;
		tt3='បុព្វលាភរ៉ាប់រងសរុបក្រោយពេលបញ្ចុះតម្លៃ (US$)';
		tt4=(case when lower(basicplanname)='pru myfamily' and lower(alang)='khmer' THEN 'ពន្ធអាករ​លើបុព្វលាភរ៉ាប់រង(US$)' ELSE 'ពន្ធ​អាករ (US$)' END);
		tt5='បុព្វលាភរ៉ាប់រងដែលត្រូវបង់​ (US$)';
	
	else	
		basicplanname=(select (l.reserve2||'') from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)=lower(abasicplanprd));
		rtr1name=(select l.reserve2 from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)='rtr1');
		rsr1name=(select l.reserve2 from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)='rsr1');
		rtr1name_ben='The benefit amount is payable once on death or TPD.';
		rsr1name_ben='The benefit amount is payable at every policy anniversary till maturity, starting from the anniversary immediately after the insured event occurs.';
		
		update temp t1 set la1sex2=t2.reserve2 from ap.vitem2 t2  where lower(t1.cola1sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set posex=t2.reserve2 from ap.vitem2 t2  where lower(t1.posex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set la2sex2=t2.reserve2 from ap.vitem2 t2   where lower(t1.cola2sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set "PaymentMode2"=t2.reserve2 from ap.vitem2 t2  where lower(t1.copaymentmode::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymentmode';
		update temp t1 set paymenttype2=t2.reserve2 from ap.vitem2 t2 where lower(t1.copaymenttype::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymenttype';
	
		tt1='Total Premium Before Discount (US$)';
		tt2='Large Sum Assured Rebates (%)';
		tt3='Total Premium After Discount (US$)';
		tt4=case when lower(basicplanname)='pru myfamily' and lower(alang)=lower('latin') THEN 'Premium Tax (US$)' ELSE 'Applicable Tax (US$)' END ;
		tt5=case when lower(basicplanname)='pru myfamily' and lower(alang)='latin' THEN 'Total Premium After Tax (US$)' ELSE 'Total Premium Payable (US$)' END;

	end if;	

	update temp set la2name='',la2sex='',la2sex2='',la2age=''  
	where l2_rtr1_flag='no' and l2_rsr1_flag='no';

--Result R1
	if atype='R1' or atype='RN' then 
		open c_R1 FOR
		select * from temp;
		return next c_R1;
        end if;	

	create temporary table tmpto on commit drop as
	select aid as coparms
		,'01'::char(2) as orderno
		,l.product as product
		,cast(basicplanname as char(1000)) as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.basicplan_sa as sa
		,cast((l.basicplan_sa * l.mbrate) as decimal(18,2)) as mb
		,l.policyterm
		,l.premiumterm
 		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode::char(10) as discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product::char(10)||l.policyterm::char(10)||'c'||l.la1sex::char(10))::char(10) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(l.product||l.policyterm||'c'||l.la1sex) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,l.l1_rtr2 as sa1
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower('rtr2'||l.policyterm||'c'||l.la1sex) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate1	-- to calc the premium rate
	from temp l;

	insert into tmpto
	select aid as coparms
		,'02'::char(2) as orderno
		,'rtr1'::char(4) as product
		,rtr1name as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.l1_rtr1 as sa
		,(l.l1_rtr1 * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la1sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rtr1'||l.premiumterm||'c'||l.la1sex)) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate		 
		,0.0
		,0.0
	from temp l where lower(l.l1_rtr1_flag)='yes';	

	insert into tmpto
	select aid as coparms
		,'03'::char(2) as orderno
		,'rsr1'::char(4) as product
		,rsr1name as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate  as effective
		,l.la1sex as sex
		,l.l1_rsr1 as sa
		,(l.l1_rsr1 * l.mbrate) as mb 
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la1sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rsr1'||l.premiumterm||'c'||l.la1sex)) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,0.0
		,0.0
	from temp l where lower(l.l1_rsr1_flag)='yes';

	insert into tmpto
	select aid as coparms
		,'04'::char(2) as orderno
		,'rtr1'::char(4) as product
		,rtr1name as productname
		,'la2'::char(3) as life
		,l.la2name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.l2_rtr1 as sa
		,(l.l2_rtr1 * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la2sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rtr1'||l.premiumterm||'c'||l.la2sex)) and l.la2age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,0.0
		,0.0
	from temp l where lower(l.l2_rtr1_flag)='yes';

	insert into tmpto
	select aid as coparms
		,'05'::char(2)  as orderno
		,'rsr1'::char(4) as product
		,rsr1name as productname
		,'la2'::char(3) as life
		,l.la2name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.l2_rsr1 as sa
		,(l.l2_rsr1 * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la2sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rsr1'||l.premiumterm||'c'||l.la2sex)) and l.la2age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,0.0
		,0.0
	from temp l where lower(l.l2_rsr1_flag)='yes';
--Result 2
	if atype='R2' or atype='RN' then
		open c_R2 FOR
		select l.coparms,l.orderno
		,(case when lower(l.productname)='pru myfamily' and lower(alang)='khmer' then  '<span class="Pru"><b>pru</b></span><i style="text-transform: lowercase; font-size:10px;color:black;">គ្រួសារខ្ញុំ</i>' 
						when lower(l.productname)='pru myfamily' and lower(alang)='latin' then '<span class="Pru"><b>pru</b></span><i class="product">my family</i>'
						when l.productname='edusave' then edusavelogo
					   else  loanassure end)::char(300) as productname
		,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa	
		,cast(round(l.sa *(premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2)) as premiumamt
		-- monthly
		,(l.product||l.paymenttype||'12 | '||l.product||'c'||l.sex)::char(20) as p12_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate) as p12_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p12_amt
		-- semi-annually
		,(l.product||l.paymenttype||'02 | '||l.product||'c'||l.sex)::char(20) as p02_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(l.product||l.paymenttype||'02')),0) * premiumrate) as p02_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p02_amt
		-- annually
		,(l.product||l.paymenttype||'01 | '||l.product||'c'||l.sex) as p01_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate) as p01_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p01_amt
		-- annualize before discount (c,01)
		,(l.product||'c'||'01 | '||l.product||'c'||l.sex) as pa1_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate) as pa1_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as pa1_amt
		from tmpto l
		where lower(l.product) like lower('m%')
		order by coparms,orderno;
		return next c_R2;
	end if;

	if prusaver::char(3) = 'yes'::char(3) then
		-- pru saver
		create temporary table tmpsaver1 on commit drop as
		select l.coparms,l.orderno
			,(case when lower(l.productname)='pru myfamily' and lower(alang)='khmer' then '<span class="pru"><b>pru</b></span><i style="text-transform: lowercase; font-size:10px;color:black;">គ្រួសារខ្ញុំ</i>' 
						when lower(l.productname)='pru myfamily' and lower(alang)='latin' then '<span class="pru"><b>pru</b></span><i class="product">my family</i>'
						when l.productname='edusave' then '<img src="/PruQuote/resources/images/edusave_log_small.jpg" style="width: 53px; height: 13px" />'
					else  productname end)::char(300) as productname
			,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa1
			,cast(round(l.sa1 *(premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2)) as premiumamt
			-- monthly
			,(l.product||l.paymenttype||'12 | '||l.product||'c'||l.sex)::char(20) as p12_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=('rtr2'||l.paymenttype||'12')),0) * premiumrate1) as p12_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=('rtr2'||l.paymenttype||'12')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p12_amt
			-- semi-annually
			,(l.product||l.paymenttype||'02 | '||l.product||'c'||l.sex)::char(20) as p02_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=(l.product||l.paymenttype||'02')),0) * premiumrate1) as p02_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=('rtr2'||l.paymenttype||'02')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p02_amt
			-- annually
			,(l.product||l.paymenttype||'01 | '||l.product||'c'||l.sex)::char(20) as p01_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=(abasicplanprd||l.paymenttype||'01')),0) * premiumrate1) as p01_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=('rtr2'||l.paymenttype||'01')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p01_amt
			-- annualize before discount (c,01)
			,(l.product||'c'||'01 | '||l.product||'c'||l.sex)::char(20) as pa1_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=(abasicplanprd||'c'||'01')),0) * premiumrate1) as pa1_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=('rtr2'||'c'||'01')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as pa1_amt

		from tmpto l
		where lower(l.product) like lower('b%')
		order by coparms,orderno;
	end if;

	large_sa_rebate=0.00;
	large_sa_rebate=(select discountrate from tmpto limit 1);

	create temporary table tmpto_tt on commit drop as
	select l.coparms,l.orderno,l.productname,product,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa	
		-- ap.vmodalfactor : to get the rate for premium modal loading factor (monthlt, semi-annully, yearly)
		-- vpremiumratelisting : to get the rate for calc 1 year premium reference to product
		,cast(l.sa *(premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)) as decimal(18,4)) as premiumamt
		-- monthly
		,(l.product||l.paymenttype||'12 | '||l.product||'c'||l.sex) as p12_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate) as p12_rate
		,round((l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end))),2) as p12_amt
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as sad12
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end))* ((100-large_sa_rebate)/100) * (SELECT ap.get_tax_rate(rcd, l.product)::numeric) ) as ftax12
		-- semi-annually
		,(l.product||l.paymenttype||'02 | '||l.product||'c'||l.sex) as p02_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(l.product||l.paymenttype||'02')),0) * premiumrate) as p02_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p02_amt
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as sad02
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100) * (SELECT ap.get_tax_rate(rcd, l.product)::numeric) ) as ftax02

		-- annually
		,(l.product||l.paymenttype||'01 | '||l.product||'c'||l.sex) as p01_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate) as p01_rate
		,(cast(round(l.sa::decimal(18,2) *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower((abasicplanprd::char(5)||l.paymenttype::char(5)||'01'::char(2)))),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p01_amt
		-- ,(cast(l.sa as char),'-',cast(l.premiumrate as char),'-',l.product )as  p01_amt 
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as sad
		-- ,round((round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=(abasicplanprd,l.paymenttype,'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) * ((100-large_sa_rebate)/100) * 0.000 ),2) as ftax			
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100) * (SELECT ap.get_tax_rate(rcd, l.product)::numeric) ) as ftax			

		-- annualize before discount (c,01)
		,(l.product||'c'||'01 | '||l.product||'c'||l.sex) as pa1_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=(abasicplanprd||'c'||'01')),0) * premiumrate) as pa1_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as pa1_amt
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as aad
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100) * (SELECT ap.get_tax_rate(rcd, l.product)::numeric) ) as atax			
		-- ,round((round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=(abasicplanprd,'c','01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) * ((100-large_sa_rebate)/100) * 0.000 ),2) as atax
	from tmpto l
	-- where l.product not like 'b%'
	order by coparms,orderno;

	create temporary table tmpdata1 on commit drop as
	select l.coparms
		,'11'::char(2) as orderno
		,tt1 as productname
		,'TT'::char(2) as product
		,''::char(1) as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb 
		,0 as sa
		,0 as premiumamt
		,'t12'::char(3) as p12_code
		,0 as p12_rate
		,sum(p12_amt) as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02' as p02_code
		,0 as p02_rate
		,sum(p02_amt) as p02_amt
		,0 as sad02
		,0 as ftax02
		,'t01' as p01_code
		,0 as p01_rate
		,SUM(p01_amt) as p01_amt
		,0 as sad
		,0 as stax
		
		,'ta1' as pa1_code
		,0 as pa1_rate
		,sum(pa1_amt) as pa1_amt
		,0 as aad
		,0 as atax
	from tmpto_tt l 
	where lower(l.product) not like lower('TT%') 
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata1;
	
	create temporary table tmpdata2 on commit drop as
	select l.coparms
		,'12'::char(2) as orderno
		,tt2 as productname
		,'TT'::char(2) as product
		,'' as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa
		,0 as premiumamt
		,'t12'::char(3) as p12_code
		,0 as p12_rate
		,large_sa_rebate as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02'::char(3) as p02_code
		,0 as p02_rate
		,large_sa_rebate as p02_amt
		,0 as sad02
		,0 as ftax02
		,'t01'::char(3) as p01_code
		,0 as p01_rate
		,large_sa_rebate as p01_amt
		,0 as sad
		,0 as ftax
		,'ta1'::char(3) as pa1_code
		,0 as pa1_rate
		,large_sa_rebate as pa1_amt
		,0 as aad
		,0 as atax		
	from tmpto_tt l 
	where lower(l.product) not like lower('TT%') 
	group by coparms;

	insert into tmpto_tt
	select * from tmpdata2;

	create temporary table tmpdata3 on commit drop as
	select l.coparms
		,'13'::char(2) as orderno
		,tt3 as productname
		,'TT'::char(2) as product
		,''::char(1) as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa
		,0 as premiumamt
		,'t12'::char(3) as p12_code
		,0 as p12_rate
		-- ,(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)) as p12_amt
		,round(sum(round(sad12,2)),2) as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02'::char(3) as p02_code
		,0 as p02_rate
		-- ,(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)) as p02_amt
		,round(sum(round(sad02,2)),2) as p02_amt
		,0 as sad02
		,0 as ftax02
		,'t01'::char(3) as p01_code
		,0 as p01_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,round(sum(round(sad,2)),2) as p01_amt
		,0 as sad
		,0 as ftax
		,'ta1'::char(3) as pa1_code
		,0 as pa1_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,round(sum(round(aad,2)),2) as pa1_amt
		,0 as aad
		,0 as atax
	from tmpto_tt l 
	where lower(l.product) not like lower('TT%') 
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata3;

	if prusaver::char(3) = 'yes'::char(3) then
		-- prusaver header
		-- select * from tmpsaver1;
		create temporary table tmpdata6 on commit drop as
		select l.coparms
		,'14'::char(2) as orderno
		,'<tr><td class="border2" style="width:170px;text-align:left;"><u>additional rider:</u></td><th class="border2" style="width:"><u>life assured</u></th><th class="border2" style="width:40px"><u>policy term</u></th><th class="border2" style="width:75px"><u>premium payment term</u></th><th class="border2" style="width:75px"><u>maturity benefit (us$)</u></th><th class="border2" style="width:70px"><u>monthly (us$)</u></th><th class="border2" style="width:100px"><u>semi-annual (us$)</u></th><th class="border2" style="width:70px"><u>annual (us$)</u></th></tr>'::char(3000) as productname
		,'th_rtr2'::char(10) as product
		,''::char(1) as lifeassured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa1
		,0 as premiumamt
		,'t12'::char(20) as p12_code
		,0 as p12_rate
		-- ,(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)) as p12_amt
		,0 as p12_amt
		,0 as sad12 -- for calculation tax
		,0 as ftax12
		,'t02'::char(20) as p02_code
		,0 as p02_rate
		-- ,(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)) as p02_amt
		,0 as p02_amt
		,0 as sad02 -- for calculation tax
		,0 as ftax02
		,'t01'::char(20) as p01_code
		,0 as p01_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,0 as p01_amt
		,0 as sad -- for calculation tax
		,0 as ftax

		,'ta1'::char(20) as pa1_code
		,0 as pa1_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,0 as pa1_amt
		,0 as aad
		,0 as atax
		from tmpsaver1 l; 
		
		insert into tmpto_tt
		select * from tmpdata6;

		create temporary table tmpdata7 on commit drop as
		select l.coparms
		,'15'::char(2) as orderno
		,case when alang='khmer' then 'អត្ថប្រយោជន៍​​​  long-term savings builder' 
			  else ' long-term savings builder' end as productname
		,'rtr2'::char(4) as product
		, life_assured
		, age
		,l.policyterm
		,l.premiumterm
		,l.mb
		,sa1
		,premiumamt
		,'t12'::char(3) as p12_code
		,p12_rate
		-- ,(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)) as p12_amt
		,p12_amt
		,p12_amt as sad12 -- for calculation tax
		,0 as ftax12
		,'t02'::char(3) as p02_code
		,p02_rate
		-- ,(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)) as p02_amt
		,p02_amt
		,p02_amt as sad02 -- for calculation tax
		,0 as ftax02
		,'t01'::char(3) as p01_code
		,p01_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,p01_amt
		,p01_amt as sad -- for calculation tax
		,0 as ftax

		,'ta1'::char(3) as pa1_code
		,0 as pa1_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,pa1_amt
		,0 as aad
		,0 as atax
		from tmpsaver1 l; 
		
		insert into tmpto_tt
		select * from tmpdata7;	
	end if;

	create temporary table tmpdata4 on commit drop as
	select l.coparms
	,'16'::char(2) as orderno
	,tt4 as productname
	,'TT'::char(2) as product
	,'' as life_assured
	,0 as age
	,0 as policyterm
	,0 as premiumterm
	,0 as mb
	,0 as sa
	,0 as premiumamt
	,'t12'::char(3) as  p12_code
	,0 as p12_rate
	-- ,(round(sum(round((p12_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p12_amt
	--,round(sum(round(round(sad12,2)*(case when l.product like 'r%' and l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2)),2) as p12_amt
	,round(SUM(round(ROUND(sad12,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2)),2) AS p12_amt
	-- ,cast(sum(ftax12) as char) as p12_amt
	,0 as sad12
	,0 as ftax12
	,'t02'::char(3) as p02_code
	,0 as p02_rate
	-- ,(round(sum(round((p02_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p02_amt
	--,round(sum(round(round(sad02,2)*(case when l.product like 'r%' and l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2)),2) as p02_amt
	,round(SUM(round(ROUND(sad02,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2)),2) AS p02_amt
	,0 as sad02
	,0 as ftax02
	,'t01'::char(3) as p01_code
	,0 as p01_rate
	-- ,(round(sum(round((p02_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p02_amt
	--,round(sum(round(round(sad,2)*(case when l.product like 'r%' and l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2)),2) as p01_amt
	,round(SUM(round(ROUND(sad,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2)),2) AS p01_amt
	,0 as sad
	,0 as ftax

	,'ta1'::char(3) as pa1_code
	,0 as pa1_rate
	-- ,(round(sum(round((p01_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p01_amt
	,round(sum(round(atax,2)),2) as pa1_amt
	,0 as aad
	,0 as atax
	from tmpto_tt l 
	where lower(l.product) not like lower('TT%') 
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata4;

	create temporary table tmpdata5 on commit drop as
	select l.coparms
		,'17' as orderno
		,tt5 as productname
		,'TT' as product
		,'' as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa
		,0 as premiumamt
		,'t12' as p12_code
		,0 as p12_rate
		-- ,(round((sum(p12_amt)*(100-large_sa_rebate)/100)*1.0,2) + round(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2)) as p12_amt
		-- ,(round(sum(round(sad12,2)),2))+(round((sum(round(ftax12,2))),2)) as p12_amt
		,(round(sum(round(sad12,2)),2))+(round((sum(round(round(sad12,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2))),2)) as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02' as p02_code
		,0 as p02_rate
		-- ,(round((sum(p02_amt)*(100-large_sa_rebate)/100)*1.0,2) + round(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2)) as p02_amt
		-- ,(round(sum(round(sad02,2)),2))+(round((sum(round(ftax02,2))),2)) as p02_amt
		,(round(sum(round(sad02,2)),2))+(round((sum(round(round(sad02,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2))),2)) as p02_amt
		,0 as sad02
		,0 as ftax02

		,'t01' as p01_code		
		,0 as p01_rate
		-- ,(round((sum(p01_amt)*(100-large_sa_rebate)/100)*1.0,2) + (sum(ftax)*(100-large_sa_rebate)/100)) as p01_amt
		-- ,(round(sum(round(sad,2)),2))+(round((sum(round(ftax,2))),2)) as p01_amt
		,(round(sum(round(sad,2)),2))+(round((sum(round(round(sad,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2))),2)) as p01_amt
		-- ,concat(cast(sum(sad) as char),'-',cast(sum(ftax) as char)) as p01_amt
		,0 as sad
		,0 as ftax
		,'ta1' as pa1_code		
		,0 as pa1_rate
		-- ,(round((sum(p01_amt)*(100-large_sa_rebate)/100)*1.0,2) + (sum(ftax)*(100-large_sa_rebate)/100)) as p01_amt
		,(round(sum(round(aad,2)),2))+(round((sum(round(atax,2))),2)) as pa1_amt
		-- ,concat(cast(sum(aad) as char),'-',cast(sum(atax) as char)) as a1
		,0 as aad
		,0 as atax

	from tmpto_tt l 
	where lower(l.product) not like lower('TT%')
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata5;

	if (atype='R3' or atype='RN') then
		OPEN c_r3 FOR
		select l.coparms,l.orderno,l.productname,product,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa	
		,premiumamt
		, p12_code
		,p12_rate
		,p12_amt
		,p02_code
		,p02_rate
		,p02_amt
		-- annually
		,p01_code
		,p01_rate
		,p01_amt
		-- annually
		,pa1_code
		,pa1_rate
		,pa1_amt
		from tmpto_tt l order by coparms,orderno;
		RETURN NEXT c_r3;	
	end if;

	create temporary table tmpmbrate on commit drop as
	select t.tablid ,t.tablname,s.*
	from ap.vmbrate s,ap.vitem2 t 
	where lower(s.combrate::char(10))=lower(t.itemid::char(10));

	if(atype='R4' or atype='RN') then
		OPEN c_r4 FOR
		select t.coparms
		,t.policyterm::integer+l.year::integer as policyterm
		,t.age::integer+l.year::integer+(t.policyterm::integer-1) as age
		,round((case when l.year::integer=0 then t.mb else 0.0 end),2) as opt1_lumpsum
		,round((case when l.year::integer=0 then t.mb else 0.0 end),2) as opt1_tt
		,round(cast(t.sa*l.rate/100.0 as decimal(18,2)),2) as opt2_inst
		,cast( round(case when l.year::integer=0 then t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr2set'||t.policyterm))/100.0
							 when l.year=1 then t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr2set'||t.policyterm))/100.0
							 when l.year=2 then t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr2set'||t.policyterm))/100.0
							 else t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr2set'||t.policyterm))/100.0 end 
							 ,2)
							 as decimal(18,2)) as opt1_inst_tt
		from tmpmbrate l,tmpto t
		where lower(l.mbrate) like lower('btr2set'||t.policyterm::char(2)) and lower(t.product) like 'btr2'
		order by coparms,year;
		RETURN NEXT c_r4;
	end if; 

	create temporary table tmpsurrender on commit drop as
	select t.tablid ,t.tablname,s.*
	from ap.vsurrender s,ap.vitem2 t 
	where s.cosurrender::char(10)=t.itemid::char(10)
	and t.tablid::integer=21;

	create temporary table tmplf_ben on commit drop as
	select t.coparms
	,yearinforce::integer as policyyear
	,((yearinforce::integer+t.age::integer)::integer - 1) as age
	,cast((case when lower(product)=lower('btl1')
	then (case when yearinforce::integer<=5  
		  then (round(t.premiumamt,2)::decimal(18,4)) else 0 end) 
	when yearinforce::integer=1 then (round(t.premiumamt,2)::decimal(18,4)) 
	else 0::decimal(18,2)
	end
	) as char(50)) as p_ape 
	,(round(round(t.premiumamt,2) * (  case when lower(product)=lower('btl1')
									then (case when yearinforce::integer<=5 
											      then yearinforce::integer else 5 
										  end) 
								 else yearinforce::integer				
							end) 
	,2)::decimal(18,2)
	) as p_total
	,case when lower(t.product)=lower('mtr1') then (t.sa::decimal(18,2)*(1-(power(1+rateinthemonth::decimal(18,4),-(t.policyterm::integer * 12-(yearinforce::integer*12-11)+1)))))/(1-(power(1+rateinthemonth::decimal(18,4),-(t.policyterm::integer*12))))  else t.sa end  as clam_non_accident
	,0 as clam_accident
	,(l.rate::decimal(18,2)) as surrender_rate
	,(trunc(t.premiumamt* l.rate/100.00 ,2)) as surrender
	from tmpsurrender l,tmpto_tt t
	where l.surrender like (abasicplanprd::char(10)||t.policyterm::char(10)) and lower(t.product) like lower('MTR%') and l.yearinforce::integer<=t.policyterm::integer
	order by yearinforce;

	--update tmplf_ben set p_ape='' where p_ape::decimal(18,2)=0.00;

	if (atype='R5' or atype='RN') then
		OPEN c_r5 for
		select * from tmplf_ben order by coparms,policyyear ;
		RETURN NEXT c_r5;
		-- print '3. benefits with additional riders, that help the family in the further securing child''s education and future '
		create temporary table tmprider_ben on commit drop as
		select l.coparms,l.orderno,productname,life_assured
		,(cast(round(l.sa,2) as decimal(18,2))) as clam_non_accident
		,(cast(round(l.sa*2,2) as decimal(18,2))) as clam_accident
		,cast(rtr1name_ben as char(200)) as payment_terms
		from tmpto_tt l
		where lower(l.product) like lower('rtr1');
	end if ;

	insert into tmprider_ben
	select l.coparms,l.orderno,productname,life_assured
	,cast(round(l.sa,2) as decimal(18,2)) as clam_non_accident
	,cast(round(l.sa,2) as decimal(18,2)) as clam_accident
	,cast(rsr1name_ben as char(200)) as payment_terms
	from tmpto_tt l
	where lower(l.product) like 'rsr1';

	update tmprider_ben set productname = replace(productname,'(sum assured)','') where 1=1;

	open c_r6 for
	select * from tmprider_ben order by coparms,orderno;
	return next c_r6;

	if lower(prusaver::char(3)) = lower('yes') then
		-- print '1. guaranteed maturity benefit with pru saver'
		if (atype='R7' or atype='RN') then
			open c_r7 for
			select t.coparms
				,t.policyterm::integer+l.year::integer as policyterm
				,t.age::integer+l.year::integer+t.policyterm::integer-1 as age
				,round((case when l.year=0 then t.sa1 else 0.0 end),2) as opt1_lumpsum
				,round((case when l.year=0 then t.sa1 else 0.0 end),2) as opt1_tt
				,round(cast(t.sa1*l.rate/100.0 as decimal(18,2)),2) as opt2_inst
				,cast( round(case when l.year=0 then t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0
									 when l.year=1 then t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0
									 when l.year=2 then t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0
									 else t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0 end 
									 ,2)
									 as decimal(18,2)) as opt1_inst_tt
			from tmpmbrate l,tmpto t
			where lower(l.mbrate) like ('rtr2set'||t.policyterm) and lower(t.product) like 'btr%'
			order by coparms,year;
			return next c_r7;
		end if;

		mbsa=(select sum(sa1) as mbsa from tmpto);
		-- select policyterm;
		-- select * from tmpto_tt;
		open c_r8 for
		select t.coparms
			,cast(yearinforce as integer) as policyyear
			,(cast(yearinforce::integer+t.age::integer as integer)-1) as age
			,cast((case when lower(product)='btl1' 
					   then (case when yearinforce::integer<=5  
								  then (cast(round(t.premiumamt,2) as decimal(18,4))) else 0 end) 
				   else (cast(round(t.premiumamt,2) as decimal(18,4))) 
			  end
			 ) as char(50)) as p_ape 
			,(cast(round(round(t.premiumamt,2) * (  case	when lower(product)='btl1' then (case when yearinforce::integer<=5 
														then yearinforce::integer else 5 
													end) 
									else yearinforce::integer				
								end) 
						,2) as decimal(18,2)
				)) as p_total
			,(t.premiumamt*1.1*yearinforce::decimal(18,2) ) as clam_non_accident
			,(t.premiumamt*1*1.1*yearinforce::decimal(18,2)) as clam_accident
			,(cast(l.rate as decimal(18,2))) as surrender_rate
			,( case when yearinforce::integer=t.policyterm::integer then mbsa
				else
					(trunc(t.premiumamt*( case when lower(product)='btl1' 
											   then (case when yearinforce::integer<=5 then yearinforce::integer else 5 end)
										   else yearinforce::integer
									  end) * l.rate/100.00 ,2)
					)
				end
			) as surrender
		from tmpsurrender l,tmpto_tt t
		where lower(l.surrender) like lower('rtr2'||t.policyterm::char(3)) and lower(t.product) like 'rtr2%' --and l.yearinforce::integer<=t.policyterm::integer
		order by yearinforce;
		return next c_r8;
	--else
	--	open c_r7 for
	--	select 'No value return' as result;
	--	return next c_r7;
		
	--	open c_r8 for
	--	select 'No value return' as result;
	--	return next c_r8;
			
	end if;

	--CALL IN JAVA PROJECT OR PGSQL
	--***********************************************************************************************
	--	select ap.spgen_pruquote(6,'Khmer','RN','R1','R2','R3','R4','R5','R6','R7','R8');	*
	--	FETCH ALL IN "R1";									*
	--	FETCH ALL IN "R2";									*
	--	FETCH ALL IN "R3";									*
	--	FETCH ALL IN "R4";									*
	--	FETCH ALL IN "R5";									*
	--	FETCH ALL IN "R6";									*
	--	FETCH ALL IN "R7";									*
	--	FETCH ALL IN "R8";									*
	--***********************************************************************************************
	
	--open c_sql2 FOR
	--SELECT * from tmpto;
        --return next c_sql2;
 
	--OPEN c_sql1 FOR
	--    SELECT *
	--    FROM vpruquote_parms;
	--RETURN NEXT c_r3;

	--OPEN c_sql2 FOR
	--    SELECT *
	--    FROM vpruquote_parms;
	--RETURN NEXT c_sql1;
    	
	--RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ap.spgen_pruquote_mrml(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor)
  OWNER TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquote_mrml(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO public;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquote_mrml(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquote_mrml(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquotedb_svc;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquote_mrml(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquotedb_supp;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquote_mrml(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquotedb_deploy;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquote_mrml(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquote_svc;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquote_mrml(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquote_supp;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquote_mrml(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquote_deploy;
GRANT EXECUTE ON FUNCTION ap.spgen_pruquote_mrml(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO postgres;

-- Function: ap.spgen_pruquote(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor)

-- DROP FUNCTION ap.spgen_pruquote(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor);

CREATE OR REPLACE FUNCTION ap.spgen_pruquote(
    aid bigint,
    alang character,
    atype character,
    c_r1 refcursor,
    c_r2 refcursor,
    c_r3 refcursor,
    c_r4 refcursor,
    c_r5 refcursor,
    c_r6 refcursor,
    c_r7 refcursor,
    c_r8 refcursor)
  RETURNS SETOF refcursor AS
$BODY$

declare mb decimal(18,2);
	sa decimal(18,2);
	mbsa decimal(18,2);
		
	policyterm varchar(20); -- decimal(18,2); 
	premiumterm varchar(20); -- decimal(18,2);
	 
	abasicplanprd varchar(4);
	mb_method varchar(12);
	
	basicplanname varchar(1000);
	rtr1name varchar(1000);
	rsr1name varchar(1000);
	rtr1name_ben varchar(1000);
 	rsr1name_ben varchar(1000);
	tt1 varchar(1000);
	tt2 varchar(1000);
	tt3 varchar(1000);
	tt4 varchar(1000);
	tt5 varchar(1000);
	proname varchar(1000);
	large_sa_rebate decimal(18,2);
	prusaver varchar(1000);
        lblpruretirement int;
	edusavelogo varchar(100);
	rcd  timestamp with time zone;

BEGIN

	--drop table if exists param;
        create temporary table param on commit drop as			
        select * from ap.vpruquote_parms a where a.id =aid;
        
	update param set product=lower('BTL1') WHERE lower(Product2)=lower('BTL1'); 

	rcd =(select l.commdate from param l);
	abasicplanprd = (select l.product from param l);
	premiumterm =(select l.premiumterm from param l);
	policyterm =(select l.policyterm from param l);
	
	mb_method = (case when lower(abasicplanprd) like 'btr2%' then 'lum' else '***' end);

	create temporary table temp on commit drop as			
        select (product||l.paymenttype||l.paymentmode)::varchar(20) as  modalfactor
			,(l.product||mb_method||l.policyterm) as mbcode
			,(cast((select l1.rate from ap.vmbrate l1 where lower(mbrate)=lower(l.product||mb_method||l.policyterm) order by l1.rate limit 1 ) as decimal(18,2))/100.0) as mbrate
			,abasicplanprd::char(10) as discountcode
			,(select t1.rate from ap.vdiscountrate t1 where lower(t1.discountrate)=lower(abasicplanprd) and l.basicplan_sa::decimal(18,2) between t1.fromsa and t1.tosa) as discountrate
			,l.id			
			,l.product
			,l.commdate
			,l.cola1relationship
			,l.relationship
			,upper(l.poname) as poname
			,cast((case when cola1relationship::char(2)='13' then cola1sex::char(20) else 'n/a'::char(20) end) as char(20)) as posex
			,cast((case when cola1relationship::char(2)='13' then la1age::char(20) else 'n/a'::char(20) end) as char(20)) as poage
			,upper(l.la1name) as la1name
			,l.la1dateofbirth
			,cast(l.la1age as char(50)) as la1age
			,l.cola1sex
			,l.la1sex
			,l.la1sex2
			,l.cola1occupationclass
			,l.la1occupationclass
			,l.copolicyterm
			,l.policyterm
			,l.copremiumterm
			,right(('000'||cast(l.premiumterm as char(3))),2) as premiumterm
 			,upper(l.la2name) as la2name
			,l.la2dateofbirth
			,cast(l.la2age as char(50)) as la2age
			,l.cola2sex
			,l.la2sex
			,l.la2sex2
			,l.cola2occupationclass
			,l.la2occupationclass
			,l.copaymenttype
			,l.paymenttype
			,l.paymenttype2
			,l.copaymentmode
			,l.paymentmode
			,l.paymentmode2 as "PaymentMode2"
			,l.basicplan_sa
			,l.col1_rtr1
			,l.l1_rtr1_flag
			,l.l1_rtr1
			,l.col1_rsr1
			,l.l1_rsr1_flag
			,l.l1_rsr1
			,l.col2_rtr1
			,l.l2_rtr1_flag
			,l.l2_rtr1
			,l.col2_rsr1
			,l.l2_rsr1_flag
			,l.l2_rsr1
			,l.validflag
			,l.couser
			,l.syndate
			-- ,nextprog
			-- ,nextprog_link
			,l.col1_rtr2
			,l.l1_rtr2_flag
			,l.l1_rtr2
			,l.pruretirement
			from param l;
			
	prusaver=(select l1_rtr2_flag from temp)::char(20);
	lblpruretirement=(select pruretirement from temp)::char(20);
	if lblpruretirement=16 then
	   	edusavelogo='<img src="/PruQuote/resources/images/edusave_log_small.jpg" style="width: 53px; height: 13px" />';
	else
		edusavelogo='edusave';
	end if;

	if lower(alang)=lower('khmer') then
		basicplanname=(select (l.reserve2||'') from ap.vitem2 l where lower(l.tablname)=lower('productcode') and lower(l.itemlatin)=lower(abasicplanprd));
		rtr1name=(select l.itemkhmer from ap.vitem2 l where lower(l.tablname)=lower('productcode') and lower(l.itemlatin)=lower('rtr1'));
		rsr1name=(select l.itemkhmer from ap.vitem2 l where lower(l.tablname)=lower('productcode') and lower(l.itemlatin)=lower('rsr1'));
		rtr1name_ben=' អត្ថប្រយោជន៍សរុបនឹងផ្តល់ជូនក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍ ';
		rsr1name_ben='អត្ថប្រយោជន៍នឹងផ្តល់ជូនរៀងរាល់ឆ្នាំរហូតដល់កាលបរិចេ្ឆទផុតកំណត់​នៃបណ្ណសន្យារ៉ាប់រងករណីទទួលមរណភាពឬពិការភាព​ទាំងស្រុង និងជាអចិន្រ្តៃយ៍';
	    
		update temp t1 set la1sex2=lower(t2.itemkhmer) from ap.vitem2 t2 where lower(t1.cola1sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set posex=lower(t2.itemkhmer) from ap.vitem2 t2 where lower(t1.posex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set la2sex2=lower(t2.itemkhmer) from ap.vitem2 t2  where lower(t1.cola2sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set "PaymentMode2"=lower(t2.itemkhmer) from ap.vitem2 t2 where lower(t1.copaymentmode::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymentmode';
		update temp t1 set paymenttype2=lower(t2.itemkhmer) from ap.vitem2 t2  where lower(t1.copaymenttype::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymenttype';
		
		tt1='បុព្វលាភរ៉ាប់រងសរុបមុនពេលបញ្ចុះតម្លៃ (US$)';
		tt2=case when lower(basicplanname)='pru myfamily' and lower(alang)='khmer' then 'ការបញ្ចុះតម្លៃសម្រាប់ទឹកប្រាក់ធានារ៉ាប់រងសរុបខ្នាតធំ(%)' else 'ការបញ្ចុះតម្លៃសម្រាប់ទឹកប្រាក់ធានារ៉ាប់រងសរុបខ្នាតធំ(%)' end;
		tt3='បុព្វលាភរ៉ាប់រងសរុបក្រោយពេលបញ្ចុះតម្លៃ (US$)';
		tt4=(case when lower(basicplanname)='pru myfamily' and lower(alang)='khmer' then 'ពន្ធ​អាករ (US$)' else 'ពន្ធអាករ​លើបុព្វលាភរ៉ាប់រង(US$)' end);
		tt5='បុព្វលាភរ៉ាប់រងជាដំណាក់កាលដែលត្រូវបង់​  (US$)';
	
	else	
		basicplanname=(select (l.reserve2||'') from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)=lower(abasicplanprd));
		rtr1name=(select l.reserve2 from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)='rtr1');
		rsr1name=(select l.reserve2 from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)='rsr1');
		rtr1name_ben='The benefit amount is payable once on death or TPD.';
		rsr1name_ben='The benefit amount is payable at every policy anniversary till maturity, starting from the anniversary immediately after the insured event occurs.';
		
		update temp t1 set la1sex2=t2.reserve2 from ap.vitem2 t2  where lower(t1.cola1sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set posex=t2.reserve2 from ap.vitem2 t2  where lower(t1.posex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set la2sex2=t2.reserve2 from ap.vitem2 t2   where lower(t1.cola2sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set "PaymentMode2"=t2.reserve2 from ap.vitem2 t2  where lower(t1.copaymentmode::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymentmode';
		update temp t1 set paymenttype2=t2.reserve2 from ap.vitem2 t2 where lower(t1.copaymenttype::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymenttype';
	
		tt1='Total Premium Before Discount (US$)';
		tt2='Large Sum Assured Rebates (%)';
		tt3='Total Premium After Discount (US$)';
		tt4=case when lower(basicplanname)='pru myfamily' and lower(alang)=lower('latin') then 'Applicable Tax (US$)' else 'Premium Related Tax (US$)' end ;
		tt5=case when lower(basicplanname)='pru myfamily' and lower(alang)='latin' then'Total Premium After Tax (US$)' else 'Total installment Premium Payable(US$)' end;


	end if;	

	update temp set la2name='',la2sex='',la2sex2='',la2age=''  
	where l2_rtr1_flag='no' and l2_rsr1_flag='no';

--Result R1
	if atype='R1' or atype='RN' then 
		open c_R1 FOR
		select * from temp;
		return next c_R1;
        end if;	

	create temporary table tmpto on commit drop as
	select aid as coparms
		,'01'::char(2) as orderno
		,l.product as product
		,cast(basicplanname as char(1000)) as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.basicplan_sa as sa
		,cast((l.basicplan_sa * l.mbrate) as decimal(18,2)) as mb
		,l.policyterm
		,l.premiumterm
 		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode::char(10) as discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product::char(10)||l.policyterm::char(10)||'c'||l.la1sex::char(10))::char(10) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(l.product||l.premiumterm||'c'||l.la1sex) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,l.l1_rtr2 as sa1
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower('rtr2'||l.premiumterm||'c'||l.la1sex) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate1	-- to calc the premium rate
	from temp l;

	insert into tmpto
	select aid as coparms
		,'02'::char(2) as orderno
		,'rtr1'::char(4) as product
		,rtr1name as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.l1_rtr1 as sa
		,(l.l1_rtr1 * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la1sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rtr1'||l.premiumterm||'c'||l.la1sex)) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate		 
		,0.0
		,0.0
	from temp l where lower(l.l1_rtr1_flag)='yes';	

	insert into tmpto
	select aid as coparms
		,'03'::char(2) as orderno
		,'rsr1'::char(4) as product
		,rsr1name as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate  as effective
		,l.la1sex as sex
		,l.l1_rsr1 as sa
		,(l.l1_rsr1 * l.mbrate) as mb 
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la1sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rsr1'||l.premiumterm||'c'||l.la1sex)) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,0.0
		,0.0
	from temp l where lower(l.l1_rsr1_flag)='yes';

	insert into tmpto
	select aid as coparms
		,'04'::char(2) as orderno
		,'rtr1'::char(4) as product
		,rtr1name as productname
		,'la2'::char(3) as life
		,l.la2name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.l2_rtr1 as sa
		,(l.l2_rtr1 * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la2sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rtr1'||l.premiumterm||'c'||l.la2sex)) and l.la2age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,0.0
		,0.0
	from temp l where lower(l.l2_rtr1_flag)='yes';

	insert into tmpto
	select aid as coparms
		,'05'::char(2)  as orderno
		,'rsr1'::char(4) as product
		,rsr1name as productname
		,'la2'::char(3) as life
		,l.la2name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.l2_rsr1 as sa
		,(l.l2_rsr1 * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la2sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rsr1'||l.premiumterm||'c'||l.la2sex)) and l.la2age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,0.0
		,0.0
	from temp l where lower(l.l2_rsr1_flag)='yes';
--Result 2
	if atype='R2' or atype='RN' then
		open c_R2 FOR
		select l.coparms,l.orderno
		,(case when lower(l.productname)='pru myfamily' and lower(alang)='khmer' then '<span class="Pru"><b>pru</b></span><i style="text-transform: lowercase; font-size:10px;color:black;">គ្រួសារខ្ញុំ</i>' 
						when lower(l.productname)='pru myfamily' and lower(alang)='latin' then '<span class="Pru"><b>pru</b></span><i class="product">my family</i>'
						when lower(l.productname)='edusave' then edusavelogo
					   else  productname end)::char(300) as productname
		,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa	
		,cast(round(l.sa *(premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2)) as premiumamt
		-- monthly
		,(l.product||l.paymenttype||'12 | '||l.product||'c'||l.sex)::char(20) as p12_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate) as p12_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p12_amt
		-- semi-annually
		,(l.product||l.paymenttype||'02 | '||l.product||'c'||l.sex)::char(20) as p02_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(l.product||l.paymenttype||'02')),0) * premiumrate) as p02_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p02_amt
		-- annually
		,(l.product||l.paymenttype||'01 | '||l.product||'c'||l.sex) as p01_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate) as p01_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p01_amt
		-- annualize before discount (c,01)
		,(l.product||'c'||'01 | '||l.product||'c'||l.sex) as pa1_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate) as pa1_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as pa1_amt
		from tmpto l
		where lower(l.product) like lower('B%')
		order by coparms,orderno;
		return next c_R2;
	end if;

	if prusaver::char(3) = 'yes'::char(3) then
		-- pru saver
		create temporary table tmpsaver1 on commit drop as
		select l.coparms,l.orderno
			,(case when lower(l.productname)='pru myfamily' and lower(alang)='khmer' then '<span class="Pru"><b>pru</b></span><i style="text-transform: lowercase; font-size:10px;color:black;">គ្រួសារខ្ញុំ</i>' 
						when lower(l.productname)='pru myfamily' and lower(alang)='latin' then '<span class="Pru"><b>pru</b></span><i class="product">my family</i>'
						when l.productname='edusave' then '<img src="/PruQuote/resources/images/edusave_log_small.jpg" style="width: 53px; height: 13px" />'
					else  productname end)::char(300) as productname
			,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa1
			,cast(round(l.sa1 *(premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2)) as premiumamt
			-- monthly
			,(l.product||l.paymenttype||'12 | '||l.product||'c'||l.sex)::char(20) as p12_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower('rtr2'||l.paymenttype||'12')),0) * premiumrate1) as p12_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower('rtr2'||l.paymenttype||'12')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p12_amt
			-- semi-annually
			,(l.product||l.paymenttype||'02 | '||l.product||'c'||l.sex)::char(20) as p02_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(l.product||l.paymenttype||'02')),0) * premiumrate1) as p02_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower('rtr2'||l.paymenttype||'02')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p02_amt
			-- annually
			,(l.product||l.paymenttype||'01 | '||l.product||'c'||l.sex)::char(20) as p01_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate1) as p01_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower('rtr2'||l.paymenttype||'01')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p01_amt
			-- annualize before discount (c,01)
			,(l.product||'c'||'01 | '||l.product||'c'||l.sex)::char(20) as pa1_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate1) as pa1_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower('rtr2'||'c'||'01')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as pa1_amt

		from tmpto l
		where lower(l.product) like lower('b%')
		order by coparms,orderno;
	end if;

	large_sa_rebate=0.00;
	large_sa_rebate=(select discountrate from tmpto limit 1);

	create temporary table tmpto_tt on commit drop as
	select l.coparms,l.orderno,l.productname,product,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa	
		-- ap.vmodalfactor : to get the rate for premium modal loading factor (monthlt, semi-annully, yearly)
		-- vpremiumratelisting : to get the rate for calc 1 year premium reference to product
		,cast(l.sa *(premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)) as decimal(18,4)) as premiumamt
		-- monthly
		,(l.product||l.paymenttype||'12 | '||l.product||'c'||l.sex) as p12_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate) as p12_rate
		,round((l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end))),2) as p12_amt
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as sad12
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end))* ((100-large_sa_rebate)/100) * 0.000 ) as ftax12
		-- semi-annually
		,(l.product||l.paymenttype||'02 | '||l.product||'c'||l.sex) as p02_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(l.product||l.paymenttype||'02')),0) * premiumrate) as p02_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p02_amt
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as sad02
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100) * 0.000 ) as ftax02

		-- annually
		,(l.product||l.paymenttype||'01 | '||l.product||'c'||l.sex) as p01_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate) as p01_rate
		,(cast(round(l.sa::decimal(18,2) *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower((abasicplanprd::char(5)||l.paymenttype::char(5)||'01'::char(2)))),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p01_amt
		-- ,(cast(l.sa as char),'-',cast(l.premiumrate as char),'-',l.product )as  p01_amt 
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as sad
		-- ,round((round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=(abasicplanprd,l.paymenttype,'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) * ((100-large_sa_rebate)/100) * 0.000 ),2) as ftax			
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100) * 0.000 ) as ftax			

		-- annualize before discount (c,01)
		,(l.product||'c'||'01 | '||l.product||'c'||l.sex) as pa1_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate) as pa1_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as pa1_amt
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as aad
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100) * 0.000 ) as atax			
		-- ,round((round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=(abasicplanprd,'c','01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) * ((100-large_sa_rebate)/100) * 0.000 ),2) as atax
	from tmpto l
	-- where l.product not like 'b%'
	order by coparms,orderno;

	create temporary table tmpdata1 on commit drop as
	select l.coparms
		,'11'::char(2) as orderno
		,tt1 as productname
		,'TT'::char(2) as product
		,''::char(1) as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb 
		,0 as sa
		,0 as premiumamt
		,'t12'::char(3) as p12_code
		,0 as p12_rate
		,sum(p12_amt) as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02' as p02_code
		,0 as p02_rate
		,sum(p02_amt) as p02_amt
		,0 as sad02
		,0 as ftax02
		,'t01' as p01_code
		,0 as p01_rate
		,SUM(p01_amt) as p01_amt
		,0 as sad
		,0 as stax
		
		,'ta1' as pa1_code
		,0 as pa1_rate
		,sum(pa1_amt) as pa1_amt
		,0 as aad
		,0 as atax
	from tmpto_tt l 
	where lower(l.product) not like LOWER('TT%') 
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata1;
	
	create temporary table tmpdata2 on commit drop as
	select l.coparms
		,'12'::char(2) as orderno
		,tt2 as productname
		,'TT'::char(2) as product
		,'' as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa
		,0 as premiumamt
		,'t12'::char(3) as p12_code
		,0 as p12_rate
		,large_sa_rebate as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02'::char(3) as p02_code
		,0 as p02_rate
		,large_sa_rebate as p02_amt
		,0 as sad02
		,0 as ftax02
		,'t01'::char(3) as p01_code
		,0 as p01_rate
		,large_sa_rebate as p01_amt
		,0 as sad
		,0 as ftax
		,'ta1'::char(3) as pa1_code
		,0 as pa1_rate
		,large_sa_rebate as pa1_amt
		,0 as aad
		,0 as atax		
	from tmpto_tt l 
	where lower(l.product) not like LOWER('TT%') 
	group by coparms;

	insert into tmpto_tt
	select * from tmpdata2;

	create temporary table tmpdata3 on commit drop as
	select l.coparms
		,'13'::char(2) as orderno
		,tt3 as productname
		,'TT'::char(2) as product
		,''::char(1) as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa
		,0 as premiumamt
		,'t12'::char(3) as p12_code
		,0 as p12_rate
		-- ,(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)) as p12_amt
		,round(sum(round(sad12,2)),2) as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02'::char(3) as p02_code
		,0 as p02_rate
		-- ,(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)) as p02_amt
		,round(sum(round(sad02,2)),2) as p02_amt
		,0 as sad02
		,0 as ftax02
		,'t01'::char(3) as p01_code
		,0 as p01_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,round(sum(round(sad,2)),2) as p01_amt
		,0 as sad
		,0 as ftax
		,'ta1'::char(3) as pa1_code
		,0 as pa1_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,round(sum(round(aad,2)),2) as pa1_amt
		,0 as aad
		,0 as atax
	from tmpto_tt l 
	where l.product not like LOWER('TT%') 
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata3;

	if prusaver::char(3) = 'yes'::char(3) then
		-- prusaver header
		-- select * from tmpsaver1;
		create temporary table tmpdata6 on commit drop as
		select l.coparms
		,'14'::char(2) as orderno
		,'<tr><td class="border2" style="width:170px;text-align:left;"><u>additional rider:</u></td><th class="border2" style="width:"><u>life assured</u></th><th class="border2" style="width:40px"><u>policy term</u></th><th class="border2" style="width:75px"><u>premium payment term</u></th><th class="border2" style="width:75px"><u>maturity benefit (us$)</u></th><th class="border2" style="width:70px"><u>monthly (us$)</u></th><th class="border2" style="width:100px"><u>semi-annual (us$)</u></th><th class="border2" style="width:70px"><u>annual (us$)</u></th></tr>'::char(3000) as productname
		,'TH_RTR2'::char(10) as product
		,''::char(1) as lifeassured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa1
		,0 as premiumamt
		,'t12'::char(20) as p12_code
		,0 as p12_rate
		-- ,(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)) as p12_amt
		,0 as p12_amt
		,0 as sad12 -- for calculation tax
		,0 as ftax12
		,'t02'::char(20) as p02_code
		,0 as p02_rate
		-- ,(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)) as p02_amt
		,0 as p02_amt
		,0 as sad02 -- for calculation tax
		,0 as ftax02
		,'t01'::char(20) as p01_code
		,0 as p01_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,0 as p01_amt
		,0 as sad -- for calculation tax
		,0 as ftax

		,'ta1'::char(20) as pa1_code
		,0 as pa1_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,0 as pa1_amt
		,0 as aad
		,0 as atax
		from tmpsaver1 l; 
		
		insert into tmpto_tt
		select * from tmpdata6;

		create temporary table tmpdata7 on commit drop as
		select l.coparms
		,'15'::char(2) as orderno
		,case when lower(alang)='khmer' then 'អត្ថប្រយោជន៍​​​  Long-Term Savings Builder' 
			  else ' Long-Term Savings Builder' end as productname
		,'RTR2'::char(4) as product
		, life_assured
		, age
		,l.policyterm
		,l.premiumterm
		,l.mb
		,sa1
		,premiumamt
		,'t12'::char(3) as p12_code
		,p12_rate
		-- ,(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)) as p12_amt
		,p12_amt
		,p12_amt as sad12 -- for calculation tax
		,0 as ftax12
		,'t02'::char(3) as p02_code
		,p02_rate
		-- ,(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)) as p02_amt
		,p02_amt
		,p02_amt as sad02 -- for calculation tax
		,0 as ftax02
		,'t01'::char(3) as p01_code
		,p01_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,p01_amt
		,p01_amt as sad -- for calculation tax
		,0 as ftax

		,'ta1'::char(3) as pa1_code
		,0 as pa1_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,pa1_amt
		,0 as aad
		,0 as atax
		from tmpsaver1 l; 
		
		insert into tmpto_tt
		select * from tmpdata7;	
	end if;

	create temporary table tmpdata4 on commit drop as
	select l.coparms
	,'16'::char(2) as orderno
	,tt4 as productname
	,'TT'::char(2) as product
	,'' as life_assured
	,0 as age
	,0 as policyterm
	,0 as premiumterm
	,0 as mb
	,0 as sa
	,0 as premiumamt
	,'t12'::char(3) as  p12_code
	,0 as p12_rate
	-- ,(round(sum(round((p12_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p12_amt
	,round(sum(round(round(sad12,2)*(case when  l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2)),2) as p12_amt
	-- ,cast(sum(ftax12) as char) as p12_amt
	,0 as sad12
	,0 as ftax12
	,'t02'::char(3) as p02_code
	,0 as p02_rate
	-- ,(round(sum(round((p02_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p02_amt
	,round(sum(round(round(sad02,2)*(case when  l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2)),2) as p02_amt
	,0 as sad02
	,0 as ftax02
	,'t01'::char(3) as p01_code
	,0 as p01_rate
	-- ,(round(sum(round((p02_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p02_amt
	,round(sum(round(round(sad,2)*(case when  l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2)),2) as p01_amt
	,0 as sad
	,0 as ftax

	,'ta1'::char(3) as pa1_code
	,0 as pa1_rate
	-- ,(round(sum(round((p01_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p01_amt
	,round(sum(round(atax,2)),2) as pa1_amt
	,0 as aad
	,0 as atax
	from tmpto_tt l 
	where l.product not like LOWER('TT%') 
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata4;

	create temporary table tmpdata5 on commit drop as
	select l.coparms
	,'17'::char(2) as orderno
	,tt5 as productname
	,'TT'::char(2) as product
	,'' as life_assured
	,0 as age
	,0 as policyterm
	,0 as premiumterm
	,0 as mb
	,0 as sa
	,0 as premiumamt
	,'t12'::char(3) as p12_code
	,0 as p12_rate
	-- ,(round((sum(p12_amt)*(100-large_sa_rebate)/100)*1.0,2) + round(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)*0.000 ,2)) as p12_amt
	-- ,(round(sum(round(sad12,2)),2))+(round((sum(round(ftax12,2))),2)) as p12_amt
	,(round(sum(round(sad12,2)),2))+(round((sum(round(round(sad12,2)*(case when  l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2))),2)) as p12_amt
	,0 as sad12
	,0 as ftax12
	,'t02'::char(3) as p02_code
	,0 as p02_rate
	-- ,(round((sum(p02_amt)*(100-large_sa_rebate)/100)*1.0,2) + round(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)*0.000 ,2)) as p02_amt
	-- ,(round(sum(round(sad02,2)),2))+(round((sum(round(ftax02,2))),2)) as p02_amt
	,(round(sum(round(sad02,2)),2))+(round((sum(round(round(sad02,2)*(case when  l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2))),2)) as p02_amt
	,0 as sad02
	,0 as ftax02

	,'t01'::char(3) as p01_code		
	,0 as p01_rate
	-- ,(round((sum(p01_amt)*(100-large_sa_rebate)/100)*1.0,2) + (sum(ftax)*(100-large_sa_rebate)/100)) as p01_amt
	-- ,(round(sum(round(sad,2)),2))+(round((sum(round(ftax,2))),2)) as p01_amt
	,(round(sum(round(sad,2)),2))+(round((sum(round(round(sad,2)*(case when  l.product<>'rtr2'  then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2))),2)) as p01_amt
	-- ,concat(cast(sum(sad) as char),'-',cast(sum(ftax) as char)) as p01_amt
	,0 as sad
	,0 as ftax
	,'ta1'::char(3) as pa1_code		
	,0 as pa1_rate
	-- ,(round((sum(p01_amt)*(100-large_sa_rebate)/100)*1.0,2) + (sum(ftax)*(100-large_sa_rebate)/100)) as p01_amt
	,(round(sum(round(aad,2)),2))+(round((sum(round(atax,2))),2)) as pa1_amt
	-- ,concat(cast(sum(aad) as char),'-',cast(sum(atax) as char)) as a1
	,0 as aad
	,0 as atax

	from tmpto_tt l 
	where l.product not like LOWER('TT%')
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata5;

	if (atype='R3' or atype='RN') then
		OPEN c_r3 FOR
		select l.coparms,l.orderno,l.productname,product,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa	
		,premiumamt
		, p12_code
		,p12_rate
		,p12_amt
		,p02_code
		,p02_rate
		,p02_amt
		-- annually
		,p01_code
		,p01_rate
		,p01_amt
		-- annually
		,pa1_code
		,pa1_rate
		,pa1_amt
		from tmpto_tt l order by coparms,orderno;
		RETURN NEXT c_r3;	
	end if;

	create temporary table tmpmbrate on commit drop as
	select t.tablid ,t.tablname,s.*
	from ap.vmbrate s,ap.vitem2 t 
	where lower(s.combrate::char(10))=lower(t.itemid::char(10));

	if(atype='R4' or atype='RN') then
		OPEN c_r4 FOR
		select t.coparms
		,t.policyterm::integer+l.year::integer as policyterm
		,t.age::integer+l.year::integer+(t.policyterm::integer-1) as age
		,round((case when l.year::integer=0 then t.mb else 0.0 end),2) as opt1_lumpsum
		,round((case when l.year::integer=0 then t.mb else 0.0 end),2) as opt1_tt
		,round(cast(t.sa*l.rate/100.0 as decimal(18,2)),2) as opt2_inst
		,cast( round(case when l.year::integer=0 then t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr2set'||t.policyterm))/100.0
							 when l.year=1 then t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr2set'||t.policyterm))/100.0
							 when l.year=2 then t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr2set'||t.policyterm))/100.0
							 else t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr2set'||t.policyterm))/100.0 end 
							 ,2)
							 as decimal(18,2)) as opt1_inst_tt
		from tmpmbrate l,tmpto t
		where lower(l.mbrate) like lower('btr2set'||t.policyterm::char(2)) and lower(t.product) like 'btr2'
		order by coparms,year;
		RETURN NEXT c_r4;
	end if; 

	create temporary table tmpsurrender on commit drop as
	select t.tablid ,t.tablname,s.*
	from ap.vsurrender s,ap.vitem2 t 
	where s.cosurrender::char(10)=t.itemid::char(10)
	and t.tablid::integer=21;

	create temporary table tmplf_ben on commit drop as
	select t.coparms
	,cast(yearinforce as integer) as policyyear
	,(cast(yearinforce::integer+t.age::integer as integer)-1) as age
	,cast((case when lower(product)=lower('btl1')
	then (case when yearinforce::integer<=5  
		  then (cast(round(t.premiumamt,2) as decimal(18,4))) else 0 end) 
	else (cast(round(t.premiumamt,2) as decimal(18,4))) 
	end
	) as char(50)) as p_ape 
	,(cast(round(round(t.premiumamt,2) * (  case when lower(product)=lower('btl1')
									then (case when yearinforce::integer<=5 
											      then yearinforce::integer else 5 
										  end) 
								 else yearinforce::integer				
							end) 
	,2) as decimal(18,2)
	)) as p_total
	,(cast(round(t.sa,2) as decimal(18,2))) as clam_non_accident
	,(cast(round(t.sa*2,2) as decimal(18,2))) as clam_accident
	,(cast(l.rate as decimal(18,2))) as surrender_rate
	,(trunc(t.premiumamt*( case when lower(product)='btl1' 
				   then (case when yearinforce::integer<=5 then yearinforce::integer else 5 end)
			   else yearinforce::integer
		  end) * l.rate/100.00 ,2)) as surrender
	from tmpsurrender l,tmpto_tt t
	where l.surrender like (abasicplanprd::char(10)||t.policyterm::char(10)) and lower(t.product) like 'bt%' and l.yearinforce::integer<=t.policyterm::integer
	order by yearinforce;

	update tmplf_ben set p_ape='' where p_ape::decimal(18,2)=0.00;

	if (atype='R5' or atype='RN') then
		OPEN c_r5 for
		select * from tmplf_ben order by coparms,policyyear ;
		RETURN NEXT c_r5;
		-- print '3. benefits with additional riders, that help the family in the further securing child''s education and future '
		create temporary table tmprider_ben on commit drop as
		select l.coparms,l.orderno,productname,life_assured
		,(cast(round(l.sa,2) as decimal(18,2))) as clam_non_accident
		,(cast(round(l.sa*2,2) as decimal(18,2))) as clam_accident
		,cast(rtr1name_ben as char(200)) as payment_terms
		from tmpto_tt l
		where l.product like 'rtr1';
	end if ;

	insert into tmprider_ben
	select l.coparms,l.orderno,productname,life_assured
	,cast(round(l.sa,2) as decimal(18,2)) as clam_non_accident
	,cast(round(l.sa,2) as decimal(18,2)) as clam_accident
	,cast(rsr1name_ben as char(200)) as payment_terms
	from tmpto_tt l
	where lower(l.product) like 'rsr1';

	update tmprider_ben set productname = replace(productname,'(sum assured)','') where 1=1;

	open c_r6 for
	select * from tmprider_ben order by coparms,orderno;
	return next c_r6;

	if lower(prusaver::char(3)) = lower('yes') then
		-- print '1. guaranteed maturity benefit with pru saver'
		if (atype='R7' or atype='RN') then
			open c_r7 for
			select t.coparms
				,t.policyterm::integer+l.year::integer as policyterm
				,t.age::integer+l.year::integer+t.policyterm::integer-1 as age
				,round((case when l.year=0 then t.sa1 else 0.0 end),2) as opt1_lumpsum
				,round((case when l.year=0 then t.sa1 else 0.0 end),2) as opt1_tt
				,round(cast(t.sa1*l.rate/100.0 as decimal(18,2)),2) as opt2_inst
				,cast( round(case when l.year=0 then t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0
									 when l.year=1 then t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0
									 when l.year=2 then t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0
									 else t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0 end 
									 ,2)
									 as decimal(18,2)) as opt1_inst_tt
			from tmpmbrate l,tmpto t
			where lower(l.mbrate) like ('rtr2set'||t.policyterm) and lower(t.product) like 'btr%'
			order by coparms,year;
			return next c_r7;
		end if;

		mbsa=(select sum(sa1) as mbsa from tmpto);
		-- select policyterm;
		-- select * from tmpto_tt;
		open c_r8 for
		select t.coparms
			,cast(yearinforce as integer) as policyyear
			,(cast(yearinforce::integer+t.age::integer as integer)-1) as age
			,cast((case when lower(product)='btl1' 
					   then (case when yearinforce::integer<=5  
								  then (cast(round(t.premiumamt,2) as decimal(18,4))) else 0 end) 
				   else (cast(round(t.premiumamt,2) as decimal(18,4))) 
			  end
			 ) as char(50)) as p_ape 
			,(cast(round(round(t.premiumamt,2) * (  case	when lower(product)='btl1' then (case when yearinforce::integer<=5 
														then yearinforce::integer else 5 
													end) 
									else yearinforce::integer				
								end) 
						,2) as decimal(18,2)
				)) as p_total
			,(t.premiumamt*1.1*yearinforce::decimal(18,2) ) as clam_non_accident
			,(t.premiumamt*1*1.1*yearinforce::decimal(18,2)) as clam_accident
			,(cast(l.rate as decimal(18,2))) as surrender_rate
			,( case when yearinforce::integer=t.policyterm::integer then mbsa
				else
					(trunc(t.premiumamt*( case when lower(product)='btl1' 
											   then (case when yearinforce::integer<=5 then yearinforce::integer else 5 end)
										   else yearinforce::integer
									  end) * l.rate/100.00 ,2)
					)
				end
			) as surrender
		from tmpsurrender l,tmpto_tt t
		where lower(l.surrender) like lower('rtr2'||t.policyterm::char(3)) and lower(t.product) like 'rtr2%' --and l.yearinforce::integer<=t.policyterm::integer
		order by yearinforce;
		return next c_r8;
	--else
	--	open c_r7 for
	--	select 'No value return' as result;
	--	return next c_r7;
		
	--	open c_r8 for
	--	select 'No value return' as result;
	--	return next c_r8;
			
	end if;

	--CALL IN JAVA PROJECT OR PGSQL
	--***********************************************************************************************
	--	select ap.spgen_pruquote(6,'Khmer','RN','R1','R2','R3','R4','R5','R6','R7','R8');	*
	--	FETCH ALL IN "R1";									*
	--	FETCH ALL IN "R2";									*
	--	FETCH ALL IN "R3";									*
	--	FETCH ALL IN "R4";									*
	--	FETCH ALL IN "R5";									*
	--	FETCH ALL IN "R6";									*
	--	FETCH ALL IN "R7";									*
	--	FETCH ALL IN "R8";									*
	--***********************************************************************************************
	
	--open c_sql2 FOR
	--SELECT * from tmpto;
        --return next c_sql2;
 
	--OPEN c_sql1 FOR
	--    SELECT *
	--    FROM vpruquote_parms;
	--RETURN NEXT c_r3;

	--OPEN c_sql2 FOR
	--    SELECT *
	--    FROM vpruquote_parms;
	--RETURN NEXT c_sql1;
    	
	--RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ap.spgen_pruquote(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor)
  OWNER TO pruquote;
GRANT EXECUTE ON FUNCTION spgen_pruquote(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO public;
GRANT EXECUTE ON FUNCTION spgen_pruquote(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquote;
GRANT EXECUTE ON FUNCTION spgen_pruquote(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquotedb_svc;
GRANT EXECUTE ON FUNCTION spgen_pruquote(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquotedb_supp;
GRANT EXECUTE ON FUNCTION spgen_pruquote(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquotedb_deploy;
GRANT EXECUTE ON FUNCTION spgen_pruquote(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO postgres;
GRANT EXECUTE ON FUNCTION spgen_pruquote(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquote_svc;
GRANT EXECUTE ON FUNCTION spgen_pruquote(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquote_supp;
GRANT EXECUTE ON FUNCTION spgen_pruquote(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor) TO pruquote_deploy;

-- Function: ap.spgensa_by_ld(character, character, character, character, character, character, character, character)

-- DROP FUNCTION ap.spgensa_by_ld(character, character, character, character, character, character, character, character);

CREATE OR REPLACE FUNCTION ap.spgensa_by_ld(
    ppldamt character,
    pproduct character,
    pterm character,
    ppaymenttype character,
    psex character,
    ppoccdate character,
    ppdob character,
    fundbybank character)
  RETURNS SETOF numeric AS
$BODY$
declare premiumamt decimal(18,2);
	sa decimal(18,2);
	prmrate decimal(18,2);
	mdlfactor decimal(18,2);
	discount decimal(18,2);
	product varchar(5);
	term varchar(5);
	paymenttype varchar(5);
	sex varchar(5);
	age int;
	agedesc character(50);
	pldamt decimal(18,2);
	poccdate date;
	pdob date;
begin
	pldamt=ppldamt::decimal(18,2);
	poccdate=ppoccdate::date;
	pdob=ppdob::date;
	if lower(fundbybank)=lower('YES') then
	
		product=(select itemlatin from ap.vitem2 where lower(tablname)='productcode' and itemid::character(20)=pproduct);
		term=(select itemlatin from ap.vitem2 where lower(tablname)='policy term' and itemid::character(20)=pterm);
		term=case when length(term)=1 then ('0'||term) else term end;
		paymenttype = (select itemlatin from ap.vitem2 where lower(tablname)='paymenttype' and itemid::character(20)=ppaymenttype);
		sex=(select itemlatin from ap.vitem2 where lower(tablname)='sex' and itemid::character(20)=psex);
		--age=(timestampdiff(year,pdob,poccdate));

		--agedesc=age(poccdate,pdob);
		--agedesc=left(agedesc,2);
		--age:=agedesc::int;
			
		age=(SELECT date_part('year'::text, f.f) AS date_part FROM age(poccdate::date::timestamp with time zone, pdob::date::timestamp with time zone) f(f));
		--age=extract(year from interval ''+age(poccdate,pdob)+'');
		
		prmrate=(select rate from ap.vpremiumratelisting  where lower(premiumrate)=lower((product||term||'C'||sex)) and age between fromage and toage);
		mdlfactor=(select rate from ap.vmodalfactor where lower(modalfactor)=lower((product||paymenttype||'01')));
		discount=(select rate from ap.vdiscountrate where lower(discountrate)=lower(product) and pldamt between fromsa and tosa)	;
		premiumamt=(select round(round(pldamt/(1000-prmrate*mdlfactor*(1-(discount/100))*(1+(SELECT ap.get_tax_rate(poccdate, 'MTR1')::numeric)))*prmrate*mdlfactor*(1-(discount/100)),2)* (1+(SELECT ap.get_tax_rate(poccdate, 'MTR1')::numeric)),2) as data);
		sa=pldamt+premiumamt;
		return query
		--select sa::varchar(20);--||term::varchar(20)||sex::varchar(20);
		select ceiling(sa);

	else
		return query
		select pldamt;
	end if;
	-- Test Script
	-- SELECT ap.spgensa_by_ld('10000','230','5','17','1','2017-03-03','1998-07-13','YES');	
	-- select rate from ap.vpremiumratelisting where premiumrate='mtr115cm' and 27 between fromage and toage
	--select concat('product:',product,' -term:',term,' -paymenttype:',paymenttype,' -sex:',sex,' -age:',age,' -prmrate:',prmrate,' -mdlfactor:',mdlfactor,' -discount:',discount);

end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ap.spgensa_by_ld(character, character, character, character, character, character, character, character)
  OWNER TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spgensa_by_ld(character, character, character, character, character, character, character, character) TO public;
GRANT EXECUTE ON FUNCTION ap.spgensa_by_ld(character, character, character, character, character, character, character, character) TO pruquote;
GRANT EXECUTE ON FUNCTION ap.spgensa_by_ld(character, character, character, character, character, character, character, character) TO pruquotedb_svc;
GRANT EXECUTE ON FUNCTION ap.spgensa_by_ld(character, character, character, character, character, character, character, character) TO pruquotedb_supp;
GRANT EXECUTE ON FUNCTION ap.spgensa_by_ld(character, character, character, character, character, character, character, character) TO pruquotedb_deploy;
GRANT EXECUTE ON FUNCTION ap.spgensa_by_ld(character, character, character, character, character, character, character, character) TO pruquote_svc;
GRANT EXECUTE ON FUNCTION ap.spgensa_by_ld(character, character, character, character, character, character, character, character) TO pruquote_supp;
GRANT EXECUTE ON FUNCTION ap.spgensa_by_ld(character, character, character, character, character, character, character, character) TO pruquote_deploy;
GRANT EXECUTE ON FUNCTION ap.spgensa_by_ld(character, character, character, character, character, character, character, character) TO postgres;



-- END TAX CHANGE

CREATE OR REPLACE VIEW ap.vproduct_type AS 
 SELECT vitemitem.id,
    vitemitem.itemlatin::character(10) AS productcode,
    vitemitem.itemkhmer::character(50) AS productname
   FROM ap.vitemitem
  WHERE vitemitem.cotabl = 12::numeric AND ("left"(vitemitem.itemlatin, 3) = ANY (ARRAY['BTR'::text, 'MTR'::text])) AND validflag = '1';

ALTER TABLE ap.vproduct_type
  OWNER TO pruquote;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ap.vproduct_type TO pruquotedb_svc;
GRANT SELECT ON TABLE ap.vproduct_type TO pruquotedb_supp;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ap.vproduct_type TO pruquotedb_deploy;
GRANT SELECT ON TABLE ap.vproduct_type TO pruquote_supp;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ap.vproduct_type TO pruquote_svc;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ap.vproduct_type TO pruquote_deploy;
GRANT ALL ON TABLE ap.vproduct_type TO pruquote;
