-- Function: ap.spgensa_by_ld(character, character, character, character, character, character, character, character)

-- DROP FUNCTION ap.spgensa_by_ld(character, character, character, character, character, character, character, character);

CREATE OR REPLACE FUNCTION ap.spgensa_by_ld(
    ppldamt character,
    pproduct character,
    pterm character,
    ppaymenttype character,
    psex character,
    ppoccdate character,
    ppdob character,
    fundbybank character)
  RETURNS SETOF numeric AS
$BODY$
declare premiumamt decimal(18,2);
	sa decimal(18,2);
	prmrate decimal(18,2);
	mdlfactor decimal(18,2);
	discount decimal(18,2);
	product varchar(5);
	term varchar(5);
	paymenttype varchar(5);
	sex varchar(5);
	age int;
	agedesc character(50);
	pldamt decimal(18,2);
	poccdate date;
	pdob date;
begin
	pldamt=ppldamt::decimal(18,2);
	poccdate=ppoccdate::date;
	pdob=ppdob::date;
	if lower(fundbybank)=lower('YES') then
	
		product=(select itemlatin from ap.vitem2 where lower(tablname)='productcode' and itemid::character(20)=pproduct);
		term=(select itemlatin from ap.vitem2 where lower(tablname)='policy term' and itemid::character(20)=pterm);
		term=case when length(term)=1 then ('0'||term) else term end;
		paymenttype = (select itemlatin from ap.vitem2 where lower(tablname)='paymenttype' and itemid::character(20)=ppaymenttype);
		sex=(select itemlatin from ap.vitem2 where lower(tablname)='sex' and itemid::character(20)=psex);
		--age=(timestampdiff(year,pdob,poccdate));

		--agedesc=age(poccdate,pdob);
		--agedesc=left(agedesc,2);
		--age:=agedesc::int;
			
		age=(SELECT date_part('year'::text, f.f) AS date_part FROM age(poccdate::date::timestamp with time zone, pdob::date::timestamp with time zone) f(f));
		--age=extract(year from interval ''+age(poccdate,pdob)+'');
		
		prmrate=(select rate from ap.vpremiumratelisting  where lower(premiumrate)=lower((product||term||'C'||sex)) and age between fromage and toage);
		mdlfactor=(select rate from ap.vmodalfactor where lower(modalfactor)=lower((product||paymenttype||'01')));
		discount=(select rate from ap.vdiscountrate where lower(discountrate)=lower(product) and pldamt between fromsa and tosa)	;
		premiumamt=(select round(round(pldamt/(1000-prmrate*mdlfactor*(1-(discount/100))*(1+(SELECT ap.get_tax_rate(poccdate, 'MTR1')::numeric)))*prmrate*mdlfactor*(1-(discount/100)),2)* (1+(SELECT ap.get_tax_rate(poccdate, 'MTR1')::numeric)),2) as data);
		sa=pldamt+premiumamt;
		return query
		--select sa::varchar(20);--||term::varchar(20)||sex::varchar(20);
		select ceiling(sa);

	else
		return query
		select pldamt;
	end if;
	-- Test Script
	-- SELECT ap.spgensa_by_ld('10000','230','5','17','1','2017-03-03','1998-07-13','YES');	
	-- select rate from ap.vpremiumratelisting where premiumrate='mtr115cm' and 27 between fromage and toage
	--select concat('product:',product,' -term:',term,' -paymenttype:',paymenttype,' -sex:',sex,' -age:',age,' -prmrate:',prmrate,' -mdlfactor:',mdlfactor,' -discount:',discount);

end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ap.spgensa_by_ld(character, character, character, character, character, character, character, character)
  OWNER TO pruquote;

