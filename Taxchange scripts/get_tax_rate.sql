﻿-- Function: ap.get_tax_rate(timestamp without time zone, text)

-- DROP FUNCTION ap.get_tax_rate(timestamp without time zone, text);

CREATE OR REPLACE FUNCTION ap.get_tax_rate(
    rcd timestamp  time zone,
    prd_code text)
  RETURNS numeric  AS
$BODY$
DECLARE tax_rate  numeric;
BEGIN
    tax_rate = COALESCE((SELECT rate from ap.tabtaxrate where CAST(rcd as date) >= CAST(itm_frm as date) and CAST(rcd as date) <= CAST(itm_to as date) and lower(prd_comp_code) = lower(prd_code) and validflag = 1 order by syndate desc limit 1),0);
    RETURN tax_rate;
END;
$BODY$
  LANGUAGE plpgsql IMMUTABLE STRICT
  COST 100;
ALTER FUNCTION ap.get_tax_rate(timestamp without time zone, text)
  OWNER TO postgres;

  
