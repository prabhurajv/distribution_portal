-- Function: ap.spgen_pruquote_mrml(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor)

-- DROP FUNCTION ap.spgen_pruquote_mrml(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor);

CREATE OR REPLACE FUNCTION ap.spgen_pruquote_mrml(
    aid bigint,
    alang character,
    atype character,
    c_r1 refcursor,
    c_r2 refcursor,
    c_r3 refcursor,
    c_r4 refcursor,
    c_r5 refcursor,
    c_r6 refcursor,
    c_r7 refcursor,
    c_r8 refcursor)
  RETURNS SETOF refcursor AS
$BODY$

declare mb decimal(18,2);
	sa decimal(18,2);
	mbsa decimal(18,2);
		
	policyterm varchar(20); -- decimal(18,2); 
	premiumterm varchar(20); -- decimal(18,2);
	 
	abasicplanprd varchar(4);
	mb_method varchar(12);
	
	basicplanname varchar(1000);
	rtr1name varchar(1000);
	rsr1name varchar(1000);
	rtr1name_ben varchar(1000);
 	rsr1name_ben varchar(1000);
	tt1 varchar(1000);
	tt2 varchar(1000);
	tt3 varchar(1000);
	tt4 varchar(1000);
	tt5 varchar(1000);
	proname varchar(1000);
	large_sa_rebate decimal(18,2);
	prusaver varchar(1000);
        lblpruretirement int;
	edusavelogo varchar(100);
	loanassure varchar(200);
	rateinthemonth decimal(18,4);
	rcd  timestamp with time zone;
	
BEGIN

	--drop table if exists param;
        create temporary table param on commit drop as			
        select * from ap.vpruquote_parms a where a.id =aid;
        
	update param set product=lower('BTL1') WHERE lower(Product2)=lower('BTL1'); 

	rcd =(select l.commdate from param l);
	abasicplanprd = (select l.product from param l);
	premiumterm =(select l.premiumterm from param l);
	policyterm =(select l.policyterm from param l);
	policyterm = (case when length(policyterm)=1 then '0'||policyterm else policyterm end);
	
	mb_method = (case when lower(abasicplanprd) like 'btr2%' then 'lum' else '***' end);

	rateinthemonth=(select l.inrate from param l)/12/100;
	
	create temporary table temp on commit drop as			
        select (product||l.paymenttype||l.paymentmode)::varchar(20) as  modalfactor
			,(l.product||mb_method||l.policyterm) as mbcode
			,(cast((select l1.rate from ap.vmbrate l1 where lower(mbrate)=lower(l.product||mb_method||l.policyterm) order by l1.rate limit 1 ) as decimal(18,2))/100.0) as mbrate
			,abasicplanprd::char(10) as discountcode
			,(select t1.rate from ap.vdiscountrate t1 where lower(t1.discountrate)=lower(abasicplanprd) and l.loanamount::decimal(18,2) between t1.fromsa and t1.tosa) as discountrate
			,l.id			
			,l.product
			,l.commdate
			,l.cola1relationship
			,l.relationship
			,upper(l.poname) as poname
			,cast((case when cola1relationship::char(2)='13' then cola1sex::char(20) else 'n/a'::char(20) end) as char(20)) as posex
			,cast((case when cola1relationship::char(2)='13' then la1age::char(20) else 'n/a'::char(20) end) as char(20)) as poage
			,upper(l.la1name) as la1name
			,l.la1dateofbirth
			,cast(l.la1age as char(50)) as la1age
			,l.cola1sex
			,l.la1sex
			,l.la1sex2
			,l.cola1occupationclass
			,l.la1occupationclass
			,l.copolicyterm
			,case when length(l.policyterm)=1 then ('0'||l.policyterm::char(3)) else l.policyterm end as policyterm
			,l.copremiumterm
			,right(('000'||cast(l.premiumterm as char(3))),2) as premiumterm
 			,upper(l.la2name) as la2name
			,l.la2dateofbirth
			,cast(l.la2age as char(50)) as la2age
			,l.cola2sex
			,l.la2sex
			,l.la2sex2
			,l.cola2occupationclass
			,l.la2occupationclass
			,l.copaymenttype
			,l.paymenttype
			,l.paymenttype2
			,l.copaymentmode
			,l.paymentmode
			,l.paymentmode2 as "PaymentMode2"
			,l.basicplan_sa
			,l.col1_rtr1
			,l.l1_rtr1_flag
			,l.l1_rtr1
			,l.col1_rsr1
			,l.l1_rsr1_flag
			,l.l1_rsr1
			,l.col2_rtr1
			,l.l2_rtr1_flag
			,l.l2_rtr1
			,l.col2_rsr1
			,l.l2_rsr1_flag
			,l.l2_rsr1
			,l.validflag
			,l.couser
			,l.syndate
			-- ,nextprog
			-- ,nextprog_link
			,l.col1_rtr2
			,l.l1_rtr2_flag
			,l.l1_rtr2
			,l.pruretirement
			,inrate
			from param l;
			
	prusaver=(select l1_rtr2_flag from temp)::char(20);
	lblpruretirement=(select pruretirement from temp)::char(20);
	if lblpruretirement=16 then
	   	edusavelogo='<img src="/PruQuote/resources/images/edusave_log_small.jpg" style="width: 53px; height: 13px" />';
	else
		edusavelogo='edusave';
	end if;
	loanassure='<img src="/PruQuote/resources/images/mortgage.png" style="width: 53px; height: 13px" />';
	if lower(alang)=lower('khmer') then
		basicplanname=(select (l.reserve2||'') from ap.vitem2 l where lower(l.tablname)=lower('productcode') and lower(l.itemlatin)=lower(abasicplanprd));
		rtr1name=(select l.itemkhmer from ap.vitem2 l where lower(l.tablname)=lower('productcode') and lower(l.itemlatin)=lower('rtr1'));
		rsr1name=(select l.itemkhmer from ap.vitem2 l where lower(l.tablname)=lower('productcode') and lower(l.itemlatin)=lower('rsr1'));
		rtr1name_ben=' អត្ថប្រយោជន៍សរុបនឹងផ្តល់ជូនក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍ ';
		rsr1name_ben='អត្ថប្រយោជន៍នឹងផ្តល់ជូនរៀងរាល់ឆ្នាំរហូតដល់កាលបរិចេ្ឆទផុតកំណត់​នៃបណ្ណសន្យារ៉ាប់រងករណីទទួលមរណភាពឬពិការភាព​ទាំងស្រុង និងជាអចិន្រ្តៃយ៍';
	    
		update temp t1 set la1sex2=t2.itemkhmer from ap.vitem2 t2 where lower(t1.cola1sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set posex=t2.itemkhmer from ap.vitem2 t2 where lower(t1.posex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set la2sex2=t2.itemkhmer from ap.vitem2 t2  where lower(t1.cola2sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set "PaymentMode2"=t2.itemkhmer from ap.vitem2 t2 where lower(t1.copaymentmode::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymentmode';
		update temp t1 set paymenttype2=t2.itemkhmer from ap.vitem2 t2  where lower(t1.copaymenttype::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymenttype';
		
		tt1='បុព្វលាភរ៉ាប់រងសរុបមុន​ពេល​បញ្ចុះតម្លៃ (US$)';
		tt2=case when lower(basicplanname)='pru myfamily' and lower(alang)='khmer' THEN 'ការបញ្ចុះតម្លៃសម្រាប់ទឹកប្រាក់ធានារ៉ាប់រងសរុបខ្នាតធំ(%)' ELSE 'ការបញ្ចុះតម្លៃសម្រាប់ទឹកប្រាក់ធានារ៉ាប់រងសរុបខ្នាតធំ(%)' END;
		tt3='បុព្វលាភរ៉ាប់រងសរុបក្រោយពេលបញ្ចុះតម្លៃ (US$)';
		tt4=(case when lower(basicplanname)='pru myfamily' and lower(alang)='khmer' THEN 'ពន្ធអាករ​លើបុព្វលាភរ៉ាប់រង(US$)' ELSE 'ពន្ធ​អាករ (US$)' END);
		tt5='បុព្វលាភរ៉ាប់រងដែលត្រូវបង់​ (US$)';
	
	else	
		basicplanname=(select (l.reserve2||'') from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)=lower(abasicplanprd));
		rtr1name=(select l.reserve2 from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)='rtr1');
		rsr1name=(select l.reserve2 from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)='rsr1');
		rtr1name_ben='The benefit amount is payable once on death or TPD.';
		rsr1name_ben='The benefit amount is payable at every policy anniversary till maturity, starting from the anniversary immediately after the insured event occurs.';
		
		update temp t1 set la1sex2=t2.reserve2 from ap.vitem2 t2  where lower(t1.cola1sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set posex=t2.reserve2 from ap.vitem2 t2  where lower(t1.posex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set la2sex2=t2.reserve2 from ap.vitem2 t2   where lower(t1.cola2sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set "PaymentMode2"=t2.reserve2 from ap.vitem2 t2  where lower(t1.copaymentmode::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymentmode';
		update temp t1 set paymenttype2=t2.reserve2 from ap.vitem2 t2 where lower(t1.copaymenttype::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymenttype';
	
		tt1='Total Premium Before Discount (US$)';
		tt2='Large Sum Assured Rebates (%)';
		tt3='Total Premium After Discount (US$)';
		tt4=case when lower(basicplanname)='pru myfamily' and lower(alang)=lower('latin') THEN 'Premium Tax (US$)' ELSE 'Applicable Tax (US$)' END ;
		tt5=case when lower(basicplanname)='pru myfamily' and lower(alang)='latin' THEN 'Total Premium After Tax (US$)' ELSE 'Total Premium Payable (US$)' END;

	end if;	

	update temp set la2name='',la2sex='',la2sex2='',la2age=''  
	where l2_rtr1_flag='no' and l2_rsr1_flag='no';

--Result R1
	if atype='R1' or atype='RN' then 
		open c_R1 FOR
		select * from temp;
		return next c_R1;
        end if;	

	create temporary table tmpto on commit drop as
	select aid as coparms
		,'01'::char(2) as orderno
		,l.product as product
		,cast(basicplanname as char(1000)) as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.basicplan_sa as sa
		,cast((l.basicplan_sa * l.mbrate) as decimal(18,2)) as mb
		,l.policyterm
		,l.premiumterm
 		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode::char(10) as discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product::char(10)||l.policyterm::char(10)||'c'||l.la1sex::char(10))::char(10) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(l.product||l.policyterm||'c'||l.la1sex) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,l.l1_rtr2 as sa1
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower('rtr2'||l.policyterm||'c'||l.la1sex) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate1	-- to calc the premium rate
	from temp l;

	insert into tmpto
	select aid as coparms
		,'02'::char(2) as orderno
		,'rtr1'::char(4) as product
		,rtr1name as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.l1_rtr1 as sa
		,(l.l1_rtr1 * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la1sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rtr1'||l.premiumterm||'c'||l.la1sex)) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate		 
		,0.0
		,0.0
	from temp l where lower(l.l1_rtr1_flag)='yes';	

	insert into tmpto
	select aid as coparms
		,'03'::char(2) as orderno
		,'rsr1'::char(4) as product
		,rsr1name as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate  as effective
		,l.la1sex as sex
		,l.l1_rsr1 as sa
		,(l.l1_rsr1 * l.mbrate) as mb 
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la1sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rsr1'||l.premiumterm||'c'||l.la1sex)) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,0.0
		,0.0
	from temp l where lower(l.l1_rsr1_flag)='yes';

	insert into tmpto
	select aid as coparms
		,'04'::char(2) as orderno
		,'rtr1'::char(4) as product
		,rtr1name as productname
		,'la2'::char(3) as life
		,l.la2name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.l2_rtr1 as sa
		,(l.l2_rtr1 * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la2sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rtr1'||l.premiumterm||'c'||l.la2sex)) and l.la2age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,0.0
		,0.0
	from temp l where lower(l.l2_rtr1_flag)='yes';

	insert into tmpto
	select aid as coparms
		,'05'::char(2)  as orderno
		,'rsr1'::char(4) as product
		,rsr1name as productname
		,'la2'::char(3) as life
		,l.la2name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.l2_rsr1 as sa
		,(l.l2_rsr1 * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la2sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rsr1'||l.premiumterm||'c'||l.la2sex)) and l.la2age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,0.0
		,0.0
	from temp l where lower(l.l2_rsr1_flag)='yes';
--Result 2
	if atype='R2' or atype='RN' then
		open c_R2 FOR
		select l.coparms,l.orderno
		,(case when lower(l.productname)='pru myfamily' and lower(alang)='khmer' then  '<span class="Pru"><b>pru</b></span><i style="text-transform: lowercase; font-size:10px;color:black;">គ្រួសារខ្ញុំ</i>' 
						when lower(l.productname)='pru myfamily' and lower(alang)='latin' then '<span class="Pru"><b>pru</b></span><i class="product">my family</i>'
						when l.productname='edusave' then edusavelogo
					   else  loanassure end)::char(300) as productname
		,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa	
		,cast(round(l.sa *(premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2)) as premiumamt
		-- monthly
		,(l.product||l.paymenttype||'12 | '||l.product||'c'||l.sex)::char(20) as p12_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate) as p12_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p12_amt
		-- semi-annually
		,(l.product||l.paymenttype||'02 | '||l.product||'c'||l.sex)::char(20) as p02_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(l.product||l.paymenttype||'02')),0) * premiumrate) as p02_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p02_amt
		-- annually
		,(l.product||l.paymenttype||'01 | '||l.product||'c'||l.sex) as p01_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate) as p01_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p01_amt
		-- annualize before discount (c,01)
		,(l.product||'c'||'01 | '||l.product||'c'||l.sex) as pa1_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate) as pa1_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as pa1_amt
		from tmpto l
		where lower(l.product) like lower('m%')
		order by coparms,orderno;
		return next c_R2;
	end if;

	if prusaver::char(3) = 'yes'::char(3) then
		-- pru saver
		create temporary table tmpsaver1 on commit drop as
		select l.coparms,l.orderno
			,(case when lower(l.productname)='pru myfamily' and lower(alang)='khmer' then '<span class="pru"><b>pru</b></span><i style="text-transform: lowercase; font-size:10px;color:black;">គ្រួសារខ្ញុំ</i>' 
						when lower(l.productname)='pru myfamily' and lower(alang)='latin' then '<span class="pru"><b>pru</b></span><i class="product">my family</i>'
						when l.productname='edusave' then '<img src="/PruQuote/resources/images/edusave_log_small.jpg" style="width: 53px; height: 13px" />'
					else  productname end)::char(300) as productname
			,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa1
			,cast(round(l.sa1 *(premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2)) as premiumamt
			-- monthly
			,(l.product||l.paymenttype||'12 | '||l.product||'c'||l.sex)::char(20) as p12_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=('rtr2'||l.paymenttype||'12')),0) * premiumrate1) as p12_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=('rtr2'||l.paymenttype||'12')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p12_amt
			-- semi-annually
			,(l.product||l.paymenttype||'02 | '||l.product||'c'||l.sex)::char(20) as p02_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=(l.product||l.paymenttype||'02')),0) * premiumrate1) as p02_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=('rtr2'||l.paymenttype||'02')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p02_amt
			-- annually
			,(l.product||l.paymenttype||'01 | '||l.product||'c'||l.sex)::char(20) as p01_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=(abasicplanprd||l.paymenttype||'01')),0) * premiumrate1) as p01_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=('rtr2'||l.paymenttype||'01')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p01_amt
			-- annualize before discount (c,01)
			,(l.product||'c'||'01 | '||l.product||'c'||l.sex)::char(20) as pa1_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=(abasicplanprd||'c'||'01')),0) * premiumrate1) as pa1_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=('rtr2'||'c'||'01')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as pa1_amt

		from tmpto l
		where lower(l.product) like lower('b%')
		order by coparms,orderno;
	end if;

	large_sa_rebate=0.00;
	large_sa_rebate=(select discountrate from tmpto limit 1);

	create temporary table tmpto_tt on commit drop as
	select l.coparms,l.orderno,l.productname,product,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa	
		-- ap.vmodalfactor : to get the rate for premium modal loading factor (monthlt, semi-annully, yearly)
		-- vpremiumratelisting : to get the rate for calc 1 year premium reference to product
		,cast(l.sa *(premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)) as decimal(18,4)) as premiumamt
		-- monthly
		,(l.product||l.paymenttype||'12 | '||l.product||'c'||l.sex) as p12_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate) as p12_rate
		,round((l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end))),2) as p12_amt
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as sad12
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end))* ((100-large_sa_rebate)/100) * (SELECT ap.get_tax_rate(rcd, l.product)::numeric) ) as ftax12
		-- semi-annually
		,(l.product||l.paymenttype||'02 | '||l.product||'c'||l.sex) as p02_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(l.product||l.paymenttype||'02')),0) * premiumrate) as p02_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p02_amt
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as sad02
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100) * (SELECT ap.get_tax_rate(rcd, l.product)::numeric) ) as ftax02

		-- annually
		,(l.product||l.paymenttype||'01 | '||l.product||'c'||l.sex) as p01_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate) as p01_rate
		,(cast(round(l.sa::decimal(18,2) *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower((abasicplanprd::char(5)||l.paymenttype::char(5)||'01'::char(2)))),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p01_amt
		-- ,(cast(l.sa as char),'-',cast(l.premiumrate as char),'-',l.product )as  p01_amt 
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as sad
		-- ,round((round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=(abasicplanprd,l.paymenttype,'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) * ((100-large_sa_rebate)/100) * 0.000 ),2) as ftax			
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100) * (SELECT ap.get_tax_rate(rcd, l.product)::numeric) ) as ftax			

		-- annualize before discount (c,01)
		,(l.product||'c'||'01 | '||l.product||'c'||l.sex) as pa1_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=(abasicplanprd||'c'||'01')),0) * premiumrate) as pa1_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as pa1_amt
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as aad
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100) * (SELECT ap.get_tax_rate(rcd, l.product)::numeric) ) as atax			
		-- ,round((round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=(abasicplanprd,'c','01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) * ((100-large_sa_rebate)/100) * 0.000 ),2) as atax
	from tmpto l
	-- where l.product not like 'b%'
	order by coparms,orderno;

	create temporary table tmpdata1 on commit drop as
	select l.coparms
		,'11'::char(2) as orderno
		,tt1 as productname
		,'TT'::char(2) as product
		,''::char(1) as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb 
		,0 as sa
		,0 as premiumamt
		,'t12'::char(3) as p12_code
		,0 as p12_rate
		,sum(p12_amt) as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02' as p02_code
		,0 as p02_rate
		,sum(p02_amt) as p02_amt
		,0 as sad02
		,0 as ftax02
		,'t01' as p01_code
		,0 as p01_rate
		,SUM(p01_amt) as p01_amt
		,0 as sad
		,0 as stax
		
		,'ta1' as pa1_code
		,0 as pa1_rate
		,sum(pa1_amt) as pa1_amt
		,0 as aad
		,0 as atax
	from tmpto_tt l 
	where lower(l.product) not like lower('TT%') 
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata1;
	
	create temporary table tmpdata2 on commit drop as
	select l.coparms
		,'12'::char(2) as orderno
		,tt2 as productname
		,'TT'::char(2) as product
		,'' as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa
		,0 as premiumamt
		,'t12'::char(3) as p12_code
		,0 as p12_rate
		,large_sa_rebate as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02'::char(3) as p02_code
		,0 as p02_rate
		,large_sa_rebate as p02_amt
		,0 as sad02
		,0 as ftax02
		,'t01'::char(3) as p01_code
		,0 as p01_rate
		,large_sa_rebate as p01_amt
		,0 as sad
		,0 as ftax
		,'ta1'::char(3) as pa1_code
		,0 as pa1_rate
		,large_sa_rebate as pa1_amt
		,0 as aad
		,0 as atax		
	from tmpto_tt l 
	where lower(l.product) not like lower('TT%') 
	group by coparms;

	insert into tmpto_tt
	select * from tmpdata2;

	create temporary table tmpdata3 on commit drop as
	select l.coparms
		,'13'::char(2) as orderno
		,tt3 as productname
		,'TT'::char(2) as product
		,''::char(1) as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa
		,0 as premiumamt
		,'t12'::char(3) as p12_code
		,0 as p12_rate
		-- ,(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)) as p12_amt
		,round(sum(round(sad12,2)),2) as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02'::char(3) as p02_code
		,0 as p02_rate
		-- ,(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)) as p02_amt
		,round(sum(round(sad02,2)),2) as p02_amt
		,0 as sad02
		,0 as ftax02
		,'t01'::char(3) as p01_code
		,0 as p01_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,round(sum(round(sad,2)),2) as p01_amt
		,0 as sad
		,0 as ftax
		,'ta1'::char(3) as pa1_code
		,0 as pa1_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,round(sum(round(aad,2)),2) as pa1_amt
		,0 as aad
		,0 as atax
	from tmpto_tt l 
	where lower(l.product) not like lower('TT%') 
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata3;

	if prusaver::char(3) = 'yes'::char(3) then
		-- prusaver header
		-- select * from tmpsaver1;
		create temporary table tmpdata6 on commit drop as
		select l.coparms
		,'14'::char(2) as orderno
		,'<tr><td class="border2" style="width:170px;text-align:left;"><u>additional rider:</u></td><th class="border2" style="width:"><u>life assured</u></th><th class="border2" style="width:40px"><u>policy term</u></th><th class="border2" style="width:75px"><u>premium payment term</u></th><th class="border2" style="width:75px"><u>maturity benefit (us$)</u></th><th class="border2" style="width:70px"><u>monthly (us$)</u></th><th class="border2" style="width:100px"><u>semi-annual (us$)</u></th><th class="border2" style="width:70px"><u>annual (us$)</u></th></tr>'::char(3000) as productname
		,'th_rtr2'::char(10) as product
		,''::char(1) as lifeassured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa1
		,0 as premiumamt
		,'t12'::char(20) as p12_code
		,0 as p12_rate
		-- ,(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)) as p12_amt
		,0 as p12_amt
		,0 as sad12 -- for calculation tax
		,0 as ftax12
		,'t02'::char(20) as p02_code
		,0 as p02_rate
		-- ,(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)) as p02_amt
		,0 as p02_amt
		,0 as sad02 -- for calculation tax
		,0 as ftax02
		,'t01'::char(20) as p01_code
		,0 as p01_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,0 as p01_amt
		,0 as sad -- for calculation tax
		,0 as ftax

		,'ta1'::char(20) as pa1_code
		,0 as pa1_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,0 as pa1_amt
		,0 as aad
		,0 as atax
		from tmpsaver1 l; 
		
		insert into tmpto_tt
		select * from tmpdata6;

		create temporary table tmpdata7 on commit drop as
		select l.coparms
		,'15'::char(2) as orderno
		,case when alang='khmer' then 'អត្ថប្រយោជន៍​​​  long-term savings builder' 
			  else ' long-term savings builder' end as productname
		,'rtr2'::char(4) as product
		, life_assured
		, age
		,l.policyterm
		,l.premiumterm
		,l.mb
		,sa1
		,premiumamt
		,'t12'::char(3) as p12_code
		,p12_rate
		-- ,(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)) as p12_amt
		,p12_amt
		,p12_amt as sad12 -- for calculation tax
		,0 as ftax12
		,'t02'::char(3) as p02_code
		,p02_rate
		-- ,(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)) as p02_amt
		,p02_amt
		,p02_amt as sad02 -- for calculation tax
		,0 as ftax02
		,'t01'::char(3) as p01_code
		,p01_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,p01_amt
		,p01_amt as sad -- for calculation tax
		,0 as ftax

		,'ta1'::char(3) as pa1_code
		,0 as pa1_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,pa1_amt
		,0 as aad
		,0 as atax
		from tmpsaver1 l; 
		
		insert into tmpto_tt
		select * from tmpdata7;	
	end if;

	create temporary table tmpdata4 on commit drop as
	select l.coparms
	,'16'::char(2) as orderno
	,tt4 as productname
	,'TT'::char(2) as product
	,'' as life_assured
	,0 as age
	,0 as policyterm
	,0 as premiumterm
	,0 as mb
	,0 as sa
	,0 as premiumamt
	,'t12'::char(3) as  p12_code
	,0 as p12_rate
	-- ,(round(sum(round((p12_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p12_amt
	--,round(sum(round(round(sad12,2)*(case when l.product like 'r%' and l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2)),2) as p12_amt
	,round(SUM(round(ROUND(sad12,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2)),2) AS p12_amt
	-- ,cast(sum(ftax12) as char) as p12_amt
	,0 as sad12
	,0 as ftax12
	,'t02'::char(3) as p02_code
	,0 as p02_rate
	-- ,(round(sum(round((p02_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p02_amt
	--,round(sum(round(round(sad02,2)*(case when l.product like 'r%' and l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2)),2) as p02_amt
	,round(SUM(round(ROUND(sad02,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2)),2) AS p02_amt
	,0 as sad02
	,0 as ftax02
	,'t01'::char(3) as p01_code
	,0 as p01_rate
	-- ,(round(sum(round((p02_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p02_amt
	--,round(sum(round(round(sad,2)*(case when l.product like 'r%' and l.product<>'rtr2' then (SELECT ap.get_tax_rate(rcd, l.product)::numeric) else 0.000 end) ,2)),2) as p01_amt
	,round(SUM(round(ROUND(sad,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2)),2) AS p01_amt
	,0 as sad
	,0 as ftax

	,'ta1'::char(3) as pa1_code
	,0 as pa1_rate
	-- ,(round(sum(round((p01_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p01_amt
	,round(sum(round(atax,2)),2) as pa1_amt
	,0 as aad
	,0 as atax
	from tmpto_tt l 
	where lower(l.product) not like lower('TT%') 
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata4;

	create temporary table tmpdata5 on commit drop as
	select l.coparms
		,'17' as orderno
		,tt5 as productname
		,'TT' as product
		,'' as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa
		,0 as premiumamt
		,'t12' as p12_code
		,0 as p12_rate
		-- ,(round((sum(p12_amt)*(100-large_sa_rebate)/100)*1.0,2) + round(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2)) as p12_amt
		-- ,(round(sum(round(sad12,2)),2))+(round((sum(round(ftax12,2))),2)) as p12_amt
		,(round(sum(round(sad12,2)),2))+(round((sum(round(round(sad12,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2))),2)) as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02' as p02_code
		,0 as p02_rate
		-- ,(round((sum(p02_amt)*(100-large_sa_rebate)/100)*1.0,2) + round(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2)) as p02_amt
		-- ,(round(sum(round(sad02,2)),2))+(round((sum(round(ftax02,2))),2)) as p02_amt
		,(round(sum(round(sad02,2)),2))+(round((sum(round(round(sad02,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2))),2)) as p02_amt
		,0 as sad02
		,0 as ftax02

		,'t01' as p01_code		
		,0 as p01_rate
		-- ,(round((sum(p01_amt)*(100-large_sa_rebate)/100)*1.0,2) + (sum(ftax)*(100-large_sa_rebate)/100)) as p01_amt
		-- ,(round(sum(round(sad,2)),2))+(round((sum(round(ftax,2))),2)) as p01_amt
		,(round(sum(round(sad,2)),2))+(round((sum(round(round(sad,2)*(SELECT ap.get_tax_rate(rcd, l.product)::numeric),2))),2)) as p01_amt
		-- ,concat(cast(sum(sad) as char),'-',cast(sum(ftax) as char)) as p01_amt
		,0 as sad
		,0 as ftax
		,'ta1' as pa1_code		
		,0 as pa1_rate
		-- ,(round((sum(p01_amt)*(100-large_sa_rebate)/100)*1.0,2) + (sum(ftax)*(100-large_sa_rebate)/100)) as p01_amt
		,(round(sum(round(aad,2)),2))+(round((sum(round(atax,2))),2)) as pa1_amt
		-- ,concat(cast(sum(aad) as char),'-',cast(sum(atax) as char)) as a1
		,0 as aad
		,0 as atax

	from tmpto_tt l 
	where lower(l.product) not like lower('TT%')
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata5;

	if (atype='R3' or atype='RN') then
		OPEN c_r3 FOR
		select l.coparms,l.orderno,l.productname,product,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa	
		,premiumamt
		, p12_code
		,p12_rate
		,p12_amt
		,p02_code
		,p02_rate
		,p02_amt
		-- annually
		,p01_code
		,p01_rate
		,p01_amt
		-- annually
		,pa1_code
		,pa1_rate
		,pa1_amt
		from tmpto_tt l order by coparms,orderno;
		RETURN NEXT c_r3;	
	end if;

	create temporary table tmpmbrate on commit drop as
	select t.tablid ,t.tablname,s.*
	from ap.vmbrate s,ap.vitem2 t 
	where lower(s.combrate::char(10))=lower(t.itemid::char(10));

	if(atype='R4' or atype='RN') then
		OPEN c_r4 FOR
		select t.coparms
		,t.policyterm::integer+l.year::integer as policyterm
		,t.age::integer+l.year::integer+(t.policyterm::integer-1) as age
		,round((case when l.year::integer=0 then t.mb else 0.0 end),2) as opt1_lumpsum
		,round((case when l.year::integer=0 then t.mb else 0.0 end),2) as opt1_tt
		,round(cast(t.sa*l.rate/100.0 as decimal(18,2)),2) as opt2_inst
		,cast( round(case when l.year::integer=0 then t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr2set'||t.policyterm))/100.0
							 when l.year=1 then t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr2set'||t.policyterm))/100.0
							 when l.year=2 then t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr2set'||t.policyterm))/100.0
							 else t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr2set'||t.policyterm))/100.0 end 
							 ,2)
							 as decimal(18,2)) as opt1_inst_tt
		from tmpmbrate l,tmpto t
		where lower(l.mbrate) like lower('btr2set'||t.policyterm::char(2)) and lower(t.product) like 'btr2'
		order by coparms,year;
		RETURN NEXT c_r4;
	end if; 

	create temporary table tmpsurrender on commit drop as
	select t.tablid ,t.tablname,s.*
	from ap.vsurrender s,ap.vitem2 t 
	where s.cosurrender::char(10)=t.itemid::char(10)
	and t.tablid::integer=21;

	create temporary table tmplf_ben on commit drop as
	select t.coparms
	,yearinforce::integer as policyyear
	,((yearinforce::integer+t.age::integer)::integer - 1) as age
	,cast((case when lower(product)=lower('btl1')
	then (case when yearinforce::integer<=5  
		  then (round(t.premiumamt,2)::decimal(18,4)) else 0 end) 
	when yearinforce::integer=1 then (round(t.premiumamt,2)::decimal(18,4)) 
	else 0::decimal(18,2)
	end
	) as char(50)) as p_ape 
	,(round(round(t.premiumamt,2) * (  case when lower(product)=lower('btl1')
									then (case when yearinforce::integer<=5 
											      then yearinforce::integer else 5 
										  end) 
								 else yearinforce::integer				
							end) 
	,2)::decimal(18,2)
	) as p_total
	,case when lower(t.product)=lower('mtr1') then (t.sa::decimal(18,2)*(1-(power(1+rateinthemonth::decimal(18,4),-(t.policyterm::integer * 12-(yearinforce::integer*12-11)+1)))))/(1-(power(1+rateinthemonth::decimal(18,4),-(t.policyterm::integer*12))))  else t.sa end  as clam_non_accident
	,0 as clam_accident
	,(l.rate::decimal(18,2)) as surrender_rate
	,(trunc(t.premiumamt* l.rate/100.00 ,2)) as surrender
	from tmpsurrender l,tmpto_tt t
	where l.surrender like (abasicplanprd::char(10)||t.policyterm::char(10)) and lower(t.product) like lower('MTR%') and l.yearinforce::integer<=t.policyterm::integer
	order by yearinforce;

	--update tmplf_ben set p_ape='' where p_ape::decimal(18,2)=0.00;

	if (atype='R5' or atype='RN') then
		OPEN c_r5 for
		select * from tmplf_ben order by coparms,policyyear ;
		RETURN NEXT c_r5;
		-- print '3. benefits with additional riders, that help the family in the further securing child''s education and future '
		create temporary table tmprider_ben on commit drop as
		select l.coparms,l.orderno,productname,life_assured
		,(cast(round(l.sa,2) as decimal(18,2))) as clam_non_accident
		,(cast(round(l.sa*2,2) as decimal(18,2))) as clam_accident
		,cast(rtr1name_ben as char(200)) as payment_terms
		from tmpto_tt l
		where lower(l.product) like lower('rtr1');
	end if ;

	insert into tmprider_ben
	select l.coparms,l.orderno,productname,life_assured
	,cast(round(l.sa,2) as decimal(18,2)) as clam_non_accident
	,cast(round(l.sa,2) as decimal(18,2)) as clam_accident
	,cast(rsr1name_ben as char(200)) as payment_terms
	from tmpto_tt l
	where lower(l.product) like 'rsr1';

	update tmprider_ben set productname = replace(productname,'(sum assured)','') where 1=1;

	open c_r6 for
	select * from tmprider_ben order by coparms,orderno;
	return next c_r6;

	if lower(prusaver::char(3)) = lower('yes') then
		-- print '1. guaranteed maturity benefit with pru saver'
		if (atype='R7' or atype='RN') then
			open c_r7 for
			select t.coparms
				,t.policyterm::integer+l.year::integer as policyterm
				,t.age::integer+l.year::integer+t.policyterm::integer-1 as age
				,round((case when l.year=0 then t.sa1 else 0.0 end),2) as opt1_lumpsum
				,round((case when l.year=0 then t.sa1 else 0.0 end),2) as opt1_tt
				,round(cast(t.sa1*l.rate/100.0 as decimal(18,2)),2) as opt2_inst
				,cast( round(case when l.year=0 then t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0
									 when l.year=1 then t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0
									 when l.year=2 then t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0
									 else t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0 end 
									 ,2)
									 as decimal(18,2)) as opt1_inst_tt
			from tmpmbrate l,tmpto t
			where lower(l.mbrate) like ('rtr2set'||t.policyterm) and lower(t.product) like 'btr%'
			order by coparms,year;
			return next c_r7;
		end if;

		mbsa=(select sum(sa1) as mbsa from tmpto);
		-- select policyterm;
		-- select * from tmpto_tt;
		open c_r8 for
		select t.coparms
			,cast(yearinforce as integer) as policyyear
			,(cast(yearinforce::integer+t.age::integer as integer)-1) as age
			,cast((case when lower(product)='btl1' 
					   then (case when yearinforce::integer<=5  
								  then (cast(round(t.premiumamt,2) as decimal(18,4))) else 0 end) 
				   else (cast(round(t.premiumamt,2) as decimal(18,4))) 
			  end
			 ) as char(50)) as p_ape 
			,(cast(round(round(t.premiumamt,2) * (  case	when lower(product)='btl1' then (case when yearinforce::integer<=5 
														then yearinforce::integer else 5 
													end) 
									else yearinforce::integer				
								end) 
						,2) as decimal(18,2)
				)) as p_total
			,(t.premiumamt*1.1*yearinforce::decimal(18,2) ) as clam_non_accident
			,(t.premiumamt*1*1.1*yearinforce::decimal(18,2)) as clam_accident
			,(cast(l.rate as decimal(18,2))) as surrender_rate
			,( case when yearinforce::integer=t.policyterm::integer then mbsa
				else
					(trunc(t.premiumamt*( case when lower(product)='btl1' 
											   then (case when yearinforce::integer<=5 then yearinforce::integer else 5 end)
										   else yearinforce::integer
									  end) * l.rate/100.00 ,2)
					)
				end
			) as surrender
		from tmpsurrender l,tmpto_tt t
		where lower(l.surrender) like lower('rtr2'||t.policyterm::char(3)) and lower(t.product) like 'rtr2%' --and l.yearinforce::integer<=t.policyterm::integer
		order by yearinforce;
		return next c_r8;
	--else
	--	open c_r7 for
	--	select 'No value return' as result;
	--	return next c_r7;
		
	--	open c_r8 for
	--	select 'No value return' as result;
	--	return next c_r8;
			
	end if;

	--CALL IN JAVA PROJECT OR PGSQL
	--***********************************************************************************************
	--	select ap.spgen_pruquote(6,'Khmer','RN','R1','R2','R3','R4','R5','R6','R7','R8');	*
	--	FETCH ALL IN "R1";									*
	--	FETCH ALL IN "R2";									*
	--	FETCH ALL IN "R3";									*
	--	FETCH ALL IN "R4";									*
	--	FETCH ALL IN "R5";									*
	--	FETCH ALL IN "R6";									*
	--	FETCH ALL IN "R7";									*
	--	FETCH ALL IN "R8";									*
	--***********************************************************************************************
	
	--open c_sql2 FOR
	--SELECT * from tmpto;
        --return next c_sql2;
 
	--OPEN c_sql1 FOR
	--    SELECT *
	--    FROM vpruquote_parms;
	--RETURN NEXT c_r3;

	--OPEN c_sql2 FOR
	--    SELECT *
	--    FROM vpruquote_parms;
	--RETURN NEXT c_sql1;
    	
	--RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ap.spgen_pruquote_mrml(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor)
  OWNER TO postgres;
