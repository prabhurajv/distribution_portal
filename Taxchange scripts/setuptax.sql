﻿CREATE SEQUENCE ap.tabtaxrate_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE ap.tabtaxrate_id_seq
  OWNER TO postgres;

CREATE TABLE ap.tabtaxrate
(
  id numeric(20,0) NOT NULL DEFAULT nextval('ap.tabtaxrate_id_seq'::regclass),
  prd_comp_code character varying(250) NOT NULL,
  itm_frm character varying(8) NOT NULL,
  itm_to character varying(8) NOT NULL,
  rate numeric(18,4) NOT NULL,
  validflag numeric(11,0) DEFAULT NULL::numeric,
  couser character varying(45) DEFAULT NULL::character varying,
  syndate timestamp with time zone,
  CONSTRAINT tabtaxrate_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ap.tabtaxrate
  OWNER TO pruquote;


 Insert into ap.tabtaxrate(prd_comp_code, itm_frm, itm_to,rate,validflag, couser, syndate)
 values('BTR1', '20170101','20171231', 0.05, 1, '340830', NOW());
 
 Insert into ap.tabtaxrate(prd_comp_code, itm_frm, itm_to,rate,validflag, couser, syndate)
 values('BTR3', '20170101','20171231', 0.05, 1, '340830', NOW());
 
 Insert into ap.tabtaxrate(prd_comp_code, itm_frm, itm_to,rate,validflag, couser, syndate)
 values('BTR4', '20170101','20171231', 0.05, 1, '340830', NOW());
 
 Insert into ap.tabtaxrate(prd_comp_code, itm_frm, itm_to,rate,validflag, couser, syndate)
 values('BTR5', '20180101','20181231', 0, 1, '340830', NOW());

 Insert into ap.tabtaxrate(prd_comp_code, itm_frm, itm_to,rate,validflag, couser, syndate)
 values('BTR6', '20180101','20181231', 0, 1, '340830', NOW());
 
 Insert into ap.tabtaxrate(prd_comp_code, itm_frm, itm_to,rate,validflag, couser, syndate)
 values('MTR1', '20180101','20181231', 0.05, 1, '340830', NOW());
 
 Insert into ap.tabtaxrate(prd_comp_code, itm_frm, itm_to,rate,validflag, couser, syndate)
 values('MTR2', '20180101','20181231', 0.05, 1, '340830', NOW());
 
 Insert into ap.tabtaxrate(prd_comp_code, itm_frm, itm_to,rate,validflag, couser, syndate)
 values('rtr1', '20180101','20181231', 0.05, 1, '340830', NOW());
 
 Insert into ap.tabtaxrate(prd_comp_code, itm_frm, itm_to,rate,validflag, couser, syndate)
 values('rsr1', '20180101','20181231', 0.05, 1, '340830', NOW());

 Insert into ap.tabtaxrate(prd_comp_code, itm_frm, itm_to,rate,validflag, couser, syndate)
 values('pad1', '20180101','20181231', 0, 1, '340830', NOW());
 
 Insert into ap.tabtaxrate(prd_comp_code, itm_frm, itm_to,rate,validflag, couser, syndate)
 values('BTL1', '20180101','20181231', 0, 1, '340830', NOW());

 