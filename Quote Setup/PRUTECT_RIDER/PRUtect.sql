-- Regular PRUtect
INSERT INTO ap.tabitemitem VALUES(801,12,'PAD1','អត្ថប្រយោជន៍​បន្ថែម Safety+','','Safety+',1,'326625',now()::timestamp without time zone);

	-- Male PremiumRate Term
INSERT INTO ap.tabitemitem VALUES(802,6,'PAD109CM','PAD109CM','PremiumRate','1',1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabitemitem VALUES(803,6,'PAD110CM','PAD110CM','PremiumRate','1',1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabitemitem VALUES(804,6,'PAD111CM','PAD111CM','PremiumRate','1',1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabitemitem VALUES(805,6,'PAD112CM','PAD112CM','PremiumRate','1',1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabitemitem VALUES(806,6,'PAD113CM','PAD113CM','PremiumRate','1',1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabitemitem VALUES(807,6,'PAD114CM','PAD114CM','PremiumRate','1',1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabitemitem VALUES(808,6,'PAD115CM','PAD115CM','PremiumRate','1',1,'326625',now()::timestamp without time zone);

	-- Female PremiumRate Term
INSERT INTO ap.tabitemitem VALUES(809,6,'PAD109CF','PAD109CF','PremiumRate','1',1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabitemitem VALUES(810,6,'PAD110CF','PAD110CF','PremiumRate','1',1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabitemitem VALUES(811,6,'PAD111CF','PAD111CF','PremiumRate','1',1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabitemitem VALUES(812,6,'PAD112CF','PAD112CF','PremiumRate','1',1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabitemitem VALUES(813,6,'PAD113CF','PAD113CF','PremiumRate','1',1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabitemitem VALUES(814,6,'PAD114CF','PAD114CF','PremiumRate','1',1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabitemitem VALUES(815,6,'PAD115CF','PAD115CF','PremiumRate','1',1,'326625',now()::timestamp without time zone);

SELECT setval('ap.tabitemitem_id', 815, true);

-- Policy Term Setup
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(3,'9','9','PAD1','9',1,'326625',now()::timestamp without time zone); --816
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(3,'10','10','PAD1','10',1,'326625',now()::timestamp without time zone); --817
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(3,'11','11','PAD1','11',1,'326625',now()::timestamp without time zone); --818
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(3,'12','12','PAD1','12',1,'326625',now()::timestamp without time zone); --819
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(3,'13','13','PAD1','13',1,'326625',now()::timestamp without time zone); --820
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(3,'14','14','PAD1','14',1,'326625',now()::timestamp without time zone); --821
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(3,'15','15','PAD1','15',1,'326625',now()::timestamp without time zone); --822

-- Premium Term Setup
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(4,'0909','0909','PAD1','9',1,'326625',now()::timestamp without time zone); --823
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(4,'1010','1010','PAD1','10',1,'326625',now()::timestamp without time zone); --824
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(4,'1111','1111','PAD1','11',1,'326625',now()::timestamp without time zone); --825
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(4,'1212','1212','PAD1','12',1,'326625',now()::timestamp without time zone); --826
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(4,'1313','1313','PAD1','13',1,'326625',now()::timestamp without time zone); --827
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(4,'1414','1414','PAD1','14',1,'326625',now()::timestamp without time zone); --828
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(4,'1515','1515','PAD1','15',1,'326625',now()::timestamp without time zone); --829

-- Premium Rate Setup
	-- Male
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,18,18,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,19,19,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,20,20,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,21,21,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,22,22,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,23,23,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,24,24,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,25,25,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,26,26,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,27,27,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,28,28,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,29,29,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,30,30,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,31,31,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,32,32,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,33,33,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,34,34,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,35,35,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,36,36,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,37,37,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,38,38,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,39,39,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,40,40,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,41,41,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,42,42,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,43,43,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,44,44,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,45,45,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,46,46,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,47,47,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,48,48,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,49,49,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,50,50,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(802,51,51,2.56,1,'326625',now());

INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,18,18,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,19,19,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,20,20,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,21,21,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,22,22,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,23,23,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,24,24,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,25,25,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,26,26,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,27,27,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,28,28,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,29,29,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,30,30,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,31,31,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,32,32,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,33,33,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,34,34,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,35,35,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,36,36,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,37,37,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,38,38,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,39,39,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,40,40,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,41,41,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,42,42,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,43,43,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,44,44,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,45,45,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,46,46,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,47,47,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,48,48,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,49,49,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(803,50,50,2.58,1,'326625',now());

INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,18,18,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,19,19,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,20,20,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,21,21,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,22,22,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,23,23,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,24,24,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,25,25,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,26,26,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,27,27,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,28,28,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,29,29,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,30,30,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,31,31,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,32,32,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,33,33,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,34,34,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,35,35,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,36,36,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,37,37,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,38,38,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,39,39,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,40,40,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,41,41,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,42,42,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,43,43,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,44,44,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,45,45,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,46,46,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,47,47,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,48,48,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(804,49,49,2.6,1,'326625',now());

INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,18,18,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,19,19,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,20,20,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,21,21,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,22,22,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,23,23,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,24,24,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,25,25,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,26,26,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,27,27,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,28,28,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,29,29,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,30,30,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,31,31,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,32,32,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,33,33,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,34,34,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,35,35,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,36,36,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,37,37,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,38,38,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,39,39,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,40,40,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,41,41,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,42,42,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,43,43,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,44,44,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,45,45,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,46,46,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,47,47,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(805,48,48,2.62,1,'326625',now());

INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,18,18,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,19,19,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,20,20,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,21,21,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,22,22,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,23,23,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,24,24,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,25,25,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,26,26,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,27,27,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,28,28,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,29,29,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,30,30,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,31,31,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,32,32,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,33,33,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,34,34,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,35,35,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,36,36,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,37,37,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,38,38,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,39,39,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,40,40,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,41,41,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,42,42,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,43,43,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,44,44,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,45,45,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,46,46,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(806,47,47,2.64,1,'326625',now());

INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,18,18,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,19,19,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,20,20,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,21,21,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,22,22,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,23,23,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,24,24,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,25,25,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,26,26,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,27,27,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,28,28,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,29,29,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,30,30,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,31,31,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,32,32,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,33,33,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,34,34,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,35,35,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,36,36,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,37,37,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,38,38,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,39,39,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,40,40,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,41,41,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,42,42,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,43,43,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,44,44,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,45,45,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(807,46,46,2.66,1,'326625',now());

INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,18,18,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,19,19,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,20,20,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,21,21,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,22,22,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,23,23,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,24,24,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,25,25,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,26,26,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,27,27,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,28,28,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,29,29,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,30,30,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,31,31,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,32,32,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,33,33,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,34,34,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,35,35,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,36,36,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,37,37,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,38,38,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,39,39,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,40,40,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,41,41,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,42,42,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,43,43,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,44,44,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(808,45,45,2.68,1,'326625',now());


-- Female PremiumRate
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,18,18,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,19,19,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,20,20,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,21,21,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,22,22,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,23,23,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,24,24,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,25,25,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,26,26,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,27,27,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,28,28,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,29,29,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,30,30,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,31,31,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,32,32,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,33,33,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,34,34,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,35,35,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,36,36,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,37,37,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,38,38,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,39,39,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,40,40,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,41,41,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,42,42,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,43,43,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,44,44,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,45,45,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,46,46,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,47,47,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,48,48,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,49,49,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,50,50,2.56,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(809,51,51,2.56,1,'326625',now());

INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,18,18,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,19,19,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,20,20,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,21,21,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,22,22,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,23,23,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,24,24,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,25,25,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,26,26,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,27,27,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,28,28,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,29,29,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,30,30,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,31,31,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,32,32,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,33,33,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,34,34,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,35,35,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,36,36,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,37,37,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,38,38,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,39,39,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,40,40,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,41,41,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,42,42,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,43,43,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,44,44,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,45,45,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,46,46,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,47,47,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,48,48,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,49,49,2.58,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(810,50,50,2.58,1,'326625',now());

INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,18,18,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,19,19,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,20,20,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,21,21,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,22,22,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,23,23,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,24,24,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,25,25,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,26,26,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,27,27,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,28,28,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,29,29,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,30,30,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,31,31,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,32,32,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,33,33,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,34,34,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,35,35,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,36,36,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,37,37,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,38,38,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,39,39,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,40,40,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,41,41,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,42,42,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,43,43,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,44,44,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,45,45,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,46,46,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,47,47,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,48,48,2.6,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(811,49,49,2.6,1,'326625',now());

INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,18,18,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,19,19,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,20,20,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,21,21,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,22,22,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,23,23,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,24,24,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,25,25,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,26,26,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,27,27,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,28,28,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,29,29,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,30,30,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,31,31,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,32,32,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,33,33,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,34,34,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,35,35,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,36,36,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,37,37,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,38,38,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,39,39,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,40,40,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,41,41,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,42,42,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,43,43,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,44,44,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,45,45,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,46,46,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,47,47,2.62,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(812,48,48,2.62,1,'326625',now());

INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,18,18,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,19,19,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,20,20,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,21,21,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,22,22,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,23,23,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,24,24,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,25,25,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,26,26,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,27,27,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,28,28,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,29,29,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,30,30,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,31,31,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,32,32,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,33,33,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,34,34,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,35,35,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,36,36,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,37,37,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,38,38,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,39,39,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,40,40,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,41,41,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,42,42,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,43,43,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,44,44,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,45,45,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,46,46,2.64,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(813,47,47,2.64,1,'326625',now());

INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,18,18,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,19,19,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,20,20,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,21,21,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,22,22,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,23,23,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,24,24,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,25,25,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,26,26,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,27,27,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,28,28,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,29,29,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,30,30,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,31,31,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,32,32,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,33,33,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,34,34,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,35,35,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,36,36,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,37,37,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,38,38,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,39,39,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,40,40,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,41,41,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,42,42,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,43,43,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,44,44,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,45,45,2.66,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(814,46,46,2.66,1,'326625',now());

INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,18,18,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,19,19,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,20,20,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,21,21,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,22,22,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,23,23,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,24,24,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,25,25,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,26,26,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,27,27,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,28,28,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,29,29,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,30,30,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,31,31,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,32,32,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,33,33,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,34,34,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,35,35,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,36,36,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,37,37,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,38,38,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,39,39,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,40,40,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,41,41,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,42,42,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,43,43,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,44,44,2.68,1,'326625',now());
INSERT INTO ap.tabpremiumrate(copremiumrate, fromage, toage, rate, validflag, couser, syndate) VALUES(815,45,45,2.68,1,'326625',now());


-- Modal Factor Setup
	-- Modal factor annually
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(15,'PAD1B01','PAD1B01','ModalFactor','',1,'326625',now()::timestamp without time zone); --830
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(15,'PAD1C01','PAD1C01','ModalFactor','',1,'326625',now()::timestamp without time zone); --831
		-- Rate setup
		-- Replace first column value to id of its respective modal factor
INSERT INTO ap.tabmodalfactor(comodalfactor, rate, validflag, couser, syndate) VALUES(830, 0.98, 1, '326625', now()::timestamp without time zone);
INSERT INTO ap.tabmodalfactor(comodalfactor, rate, validflag, couser, syndate) VALUES(831, 1, 1, '326625', now()::timestamp without time zone);
	-- Modal factor semi-annual
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(15,'PAD1B02','PAD1B02','ModalFactor','',1,'326625',now()::timestamp without time zone); --832
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(15,'PAD1C02','PAD1C02','ModalFactor','',1,'326625',now()::timestamp without time zone); --833
		-- Rate setup
		-- Replace first column value to id of its respective modal factor
INSERT INTO ap.tabmodalfactor(comodalfactor, rate, validflag, couser, syndate) VALUES(832, 0.51, 1, '326625', now()::timestamp without time zone);
INSERT INTO ap.tabmodalfactor(comodalfactor, rate, validflag, couser, syndate) VALUES(833, 0.525, 1, '326625', now()::timestamp without time zone);
	-- Modal factor monthly
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(15,'PAD1B12','PAD1B12','ModalFactor','',1,'326625',now()::timestamp without time zone); --834
		-- Rate setup
		-- Replace first column value to id of its respective modal factor
INSERT INTO ap.tabmodalfactor(comodalfactor, rate, validflag, couser, syndate) VALUES(834, 0.09166, 1, '326625', now()::timestamp without time zone);

-- UAB Factor Setup
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(10,'PAD109','PAD109','UABFactor','',1,'326625',now()::timestamp without time zone); --835
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(10,'PAD110','PAD110','UABFactor','',1,'326625',now()::timestamp without time zone); --836
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(10,'PAD111','PAD111','UABFactor','',1,'326625',now()::timestamp without time zone); --837
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(10,'PAD112','PAD112','UABFactor','',1,'326625',now()::timestamp without time zone); --838
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(10,'PAD113','PAD113','UABFactor','',1,'326625',now()::timestamp without time zone); --839
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(10,'PAD114','PAD114','UABFactor','',1,'326625',now()::timestamp without time zone); --840
INSERT INTO ap.tabitemitem(cotabl, itemlatin, itemkhmer, reserve1, reserve2, validflag, couser, syndate) VALUES(10,'PAD115','PAD115','UABFactor','',1,'326625',now()::timestamp without time zone); --841

	-- Rate Setup 15years
	-- Replace first column with its respective id
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(841,0,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(841,1,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(841,2,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(841,3,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(841,4,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(841,5,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(841,6,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(841,7,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(841,8,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(841,9,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(841,10,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(841,11,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(841,12,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(841,13,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(841,14,1,1,'326625',now()::timestamp without time zone);
	-- Rate Setup 14years
	-- Replace first column with its respective id
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(840,0,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(840,1,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(840,2,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(840,3,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(840,4,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(840,5,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(840,6,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(840,7,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(840,8,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(840,9,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(840,10,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(840,11,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(840,12,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(840,13,1,1,'326625',now()::timestamp without time zone);
	-- Rate Setup 13years
	-- Replace first column with its respective id
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(839,0,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(839,1,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(839,2,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(839,3,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(839,4,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(839,5,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(839,6,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(839,7,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(839,8,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(839,9,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(839,10,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(839,11,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(839,12,1,1,'326625',now()::timestamp without time zone);
	-- Rate Setup 12years
	-- Replace first column with its respective id
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(838,0,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(838,1,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(838,2,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(838,3,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(838,4,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(838,5,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(838,6,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(838,7,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(838,8,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(838,9,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(838,10,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(838,11,1,1,'326625',now()::timestamp without time zone);
	-- Rate Setup 11years
	-- Replace first column with its respective id
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(837,0,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(837,1,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(837,2,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(837,3,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(837,4,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(837,5,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(837,6,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(837,7,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(837,8,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(837,9,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(837,10,1,1,'326625',now()::timestamp without time zone);
	-- Rate Setup 10years
	-- Replace first column with its respective id
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(836,0,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(836,1,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(836,2,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(836,3,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(836,4,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(836,5,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(836,6,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(836,7,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(836,8,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(836,9,1,1,'326625',now()::timestamp without time zone);
	-- Rate Setup 9years
	-- Replace first column with its respective id
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(835,0,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(835,1,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(835,2,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(835,3,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(835,4,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(835,5,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(835,6,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(835,7,1,1,'326625',now()::timestamp without time zone);
INSERT INTO ap.tabuabfactor(couabfactor, policyyear, rate, validflag, couser, syndate) VALUES(835,8,1,1,'326625',now()::timestamp without time zone);

-- Alter table pruquote
ALTER TABLE ap.tabpruquote_parms
	ADD COLUMN la1_pad1 int default null::numeric,
	ADD COLUMN la1_pad1_sa numeric(18,2),
	ADD COLUMN la2_pad1 int default null::numeric,
	ADD COLUMN la2_pad1_sa numeric(18,2);
	
-- Insert new permission for PRUTECT
INSERT INTO ap.sec_permissions(
permission_name, permission_desc, controller, 
       action, path, status, created_date, created_by
)VALUES('ACCESS_PRUTECT','Access PRUTECT or Safety+ rider',null,null,null,1,now()::timestamp without time zone,'326625');

-- Add PRUTECT permission to System Admin role
INSERT INTO ap.sec_role_permissions(role_id,permission_id,is_valid,created_date,created_by)
VALUES(1,31,true,now()::timestamp without time zone,'326625');
	
-- View: ap.vpruquote_parms

-- DROP VIEW ap.vpruquote_parms;

CREATE OR REPLACE VIEW ap.vpruquote_parms AS 
 SELECT l.id, 
        CASE
            WHEN l.product::text = 'BTR1'::text AND "right"(vpremiumterm.reserve2::text, 2)::character(1) = 5::character(1) THEN 'BTL1'::character varying
            ELSE l.product
        END AS product2, 
        CASE
            WHEN l.product::text = 'BTR1'::text THEN 'Pru Myfamily'::character varying
            WHEN l.product::text = 'BTR2'::text THEN 'Edusave'::character varying
            WHEN l.product::text = 'BTR3'::text THEN 'easylife'::character varying
            WHEN l.product::text = 'BTR4'::text THEN 'eduCARE'::text::character varying
            WHEN l.product::text = 'MTR1'::text THEN 'MRTA'::character varying
            WHEN l.product::text = 'MTR2'::text THEN 'MLTA'::character varying
			WHEN l.product::text = 'BTR5'::text THEN 'PureProtect'::character varying
            WHEN l.product::text = 'BTR6'::text THEN 'SafeLife'::text::character varying
            ELSE ''::character varying
        END AS productdesc, l.product, l.commdate, l.cola1relationship, vrelationship.itemlatin AS relationship, l.poname, l.la1name, l.la1dateofbirth, ( SELECT date_part('year'::text, f.f) AS date_part
           FROM age(l.commdate::date::timestamp with time zone, l.la1dateofbirth::date::timestamp with time zone) f(f)) AS la1age, l.cola1sex, vla1sex.itemlatin AS la1sex, vla1sex.reserve2 AS la1sex2, l.cola1occupationclass, vla1occupationclass.itemlatin AS la1occupationclass, l.copolicyterm, vpolicyterm.itemlatin AS policyterm, l.copremiumterm, "right"(vpremiumterm.reserve2::text, 2) AS premiumterm, l.la2name, l.la2dateofbirth, ( SELECT date_part('year'::text, f.f) AS date_part
           FROM age(l.commdate::date::timestamp with time zone, l.la2dateofbirth::date::timestamp with time zone) f(f)) AS la2age, l.cola2sex, vla2sex.itemlatin AS la2sex, vla2sex.reserve2 AS la2sex2, l.cola2occupationclass, vla2occupationclass.itemlatin AS la2occupationclass, l.copaymenttype, vpaymenttype.itemlatin AS paymenttype, vpaymenttype.reserve2 AS paymenttype2, l.copaymentmode, vpaymentmode.itemlatin AS paymentmode, vpaymentmode.reserve2 AS paymentmode2, l.basicplan_sa, l.col1_rtr1, 
        CASE
            WHEN l.col1_rtr1 = 15::numeric THEN 'yes'::text
            ELSE 'no'::text
        END AS l1_rtr1_flag, l.l1_rtr1, l.col1_rsr1, 
        CASE
            WHEN l.col1_rsr1 = 15::numeric THEN 'yes'::text
            ELSE 'no'::text
        END AS l1_rsr1_flag, l.l1_rsr1, l.col2_rtr1, 
        CASE
            WHEN l.col2_rtr1 = 15::numeric THEN 'yes'::text
            ELSE 'no'::text
        END AS l2_rtr1_flag, l.l2_rtr1, l.col2_rsr1, 
        CASE
            WHEN l.col2_rsr1 = 15::numeric THEN 'yes'::text
            ELSE 'no'::text
        END AS l2_rsr1_flag, l.l2_rsr1, l.validflag, l.couser, ''::character varying(50) AS fullname, l.syndate, 'view report'::character varying(20) AS nextprog, 1::numeric AS nextprog_link, l.col1_rtr2, 
        CASE
            WHEN l.col1_rtr2 = 15::numeric THEN 'yes'::text
            ELSE 'no'::text
        END AS l1_rtr2_flag, l.l1_rtr2, l.pruretirement, l.isfundedbybank, l.inrate, l.transfer, l.loanamount,
        CASE WHEN l.la1_pad1 = 15 THEN 'yes'::character varying
			ELSE 'no'::character varying
		END AS la1_pad1_flag,
		la1_pad1_sa,
		CASE WHEN l.la2_pad1 = 15 THEN 'yes'::character varying
			ELSE 'no'::character varying
		END AS la2_pad1_flag,
		la2_pad1_sa
   FROM ap.tabpruquote_parms l, ap.vitem2 vla1sex, ap.vitem2 vrelationship, ap.vitem2 vla1occupationclass, ap.vitem2 vpolicyterm, ap.vitem2 vpremiumterm, ap.vitem2 vla2sex, ap.vitem2 vla2occupationclass, ap.vitem2 vpaymenttype, ap.vitem2 vpaymentmode
  WHERE l.cola1sex = vla1sex.itemid AND vla1sex.tablid = 2::numeric AND l.cola1relationship = vrelationship.itemid AND vrelationship.tablid = 1::numeric AND l.cola1occupationclass = vla1occupationclass.itemid AND vla1occupationclass.tablid = 7::numeric AND l.copolicyterm = vpolicyterm.itemid AND vpolicyterm.tablid = 3::numeric AND l.copremiumterm = vpremiumterm.itemid AND vpremiumterm.tablid = 4::numeric AND l.cola2sex = vla2sex.itemid AND vla2sex.tablid = 2::numeric AND l.cola2occupationclass = vla2occupationclass.itemid AND vla2occupationclass.tablid = 7::numeric AND l.copaymenttype = vpaymenttype.itemid AND vpaymenttype.tablid = 8::numeric AND l.copaymentmode = vpaymentmode.itemid AND vpaymentmode.tablid = 9::numeric AND (now()::date - l.commdate::date) <= 90
  ORDER BY l.id DESC;

ALTER TABLE ap.vpruquote_parms
  OWNER TO pruquote;
  
-- Function: ap.spgen_edusmart(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor)

-- DROP FUNCTION ap.spgen_edusmart(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor);

CREATE OR REPLACE FUNCTION ap.spgen_edusmart(aid bigint, alang character, atype character, c_r1 refcursor, c_r2 refcursor, c_r3 refcursor, c_r4 refcursor, c_r5 refcursor, c_r6 refcursor, c_r7 refcursor, c_r8 refcursor)
  RETURNS SETOF refcursor AS
$BODY$

declare mb decimal(18,2);
	sa decimal(18,2);
	mbsa decimal(18,2);
		
	policyterm varchar(20); -- decimal(18,2); 
	premiumterm varchar(20); -- decimal(18,2);
	 
	abasicplanprd varchar(4);
	mb_method varchar(12);
	
	basicplanname varchar(1000);
	rtr1name varchar(1000);
	rsr1name varchar(1000);
	rtr1name_ben varchar(1000);
 	rsr1name_ben varchar(1000);
 	-- prutect
 	pad1name varchar(1000);
 	pad1name_ben varchar(1000);
 	-- end of prutect
	tt1 varchar(1000);
	tt2 varchar(1000);
	tt3 varchar(1000);
	tt4 varchar(1000);
	tt5 varchar(1000);
	proname varchar(1000);
	large_sa_rebate decimal(18,2);
	prusaver varchar(1000);
        lblpruretirement int;
	edusavelogo varchar(100);
	educarelogo varchar(200);
	pdiscountrate numeric(18,2);

BEGIN

	--drop table if exists param;
        create temporary table param on commit drop as			
        select * from ap.vpruquote_parms a where a.id =aid;
        
-- 	update param set product=lower('BTL1') WHERE lower(Product2)=lower('BTL1'); 

	abasicplanprd = (select l.product from param l);
	premiumterm =(select l.premiumterm from param l);
	policyterm =(select (l.policyterm::int - 5) as policyterm from param l);
	
	mb_method = (case when lower(abasicplanprd) like 'btr4%' then 'lum' else '***' end);

	create temporary table temp on commit drop as			
        select (product||l.paymenttype||l.paymentmode)::varchar(20) as  modalfactor
			,(l.product||mb_method||l.policyterm) as mbcode
			,(cast((select l1.rate from ap.vmbrate l1 where lower(mbrate)=lower(l.product||mb_method||l.policyterm) order by l1.rate limit 1 ) as decimal(18,2))/100.0) as mbrate
			,abasicplanprd::char(10) as discountcode
			,(select t1.rate from ap.vdiscountrate t1 where lower(t1.discountrate)=lower(abasicplanprd) and l.basicplan_sa::decimal(18,2) between t1.fromsa and t1.tosa) as discountrate
			,l.id			
			,l.product
			,l.commdate
			,l.cola1relationship
			,l.relationship
			,upper(l.poname) as poname
			,cast((case when cola1relationship::char(2)='13' then cola1sex::char(20) else 'n/a'::char(20) end) as char(20)) as posex
			,cast((case when cola1relationship::char(2)='13' then la1age::char(20) else 'n/a'::char(20) end) as char(20)) as poage
			,upper(l.la1name) as la1name
			,l.la1dateofbirth
			,cast(l.la1age as char(50)) as la1age
			,l.cola1sex
			,l.la1sex
			,l.la1sex2
			,l.cola1occupationclass
			,l.la1occupationclass
			,l.copolicyterm
			,l.policyterm
			,l.copremiumterm
			,right(('000'||cast(l.premiumterm as char(3))),2) as premiumterm
 			,upper(l.la2name) as la2name
			,l.la2dateofbirth
			,cast(l.la2age as char(50)) as la2age
			,l.cola2sex
			,l.la2sex
			,l.la2sex2
			,l.cola2occupationclass
			,l.la2occupationclass
			,l.copaymenttype
			,l.paymenttype
			,l.paymenttype2
			,l.copaymentmode
			,l.paymentmode
			,l.paymentmode2 as "PaymentMode2"
			,l.basicplan_sa
			,l.col1_rtr1
			,l.l1_rtr1_flag
			,l.l1_rtr1
			,l.col1_rsr1
			,l.l1_rsr1_flag
			,l.l1_rsr1
			,l.col2_rtr1
			,l.l2_rtr1_flag
			,l.l2_rtr1
			,l.col2_rsr1
			,l.l2_rsr1_flag
			,l.l2_rsr1
			,l.validflag
			,l.couser
			,l.syndate
			,l.col1_rtr2
			,l.l1_rtr2_flag
			,l.l1_rtr2
			,l.pruretirement
			-- prutect
			,l.la1_pad1_sa
			,l.la1_pad1_flag
			,l.la2_pad1_sa
			,l.la2_pad1_flag
			-- end of prutect
			from param l;

	edusavelogo='<img src="/PruQuote/resources/images/edusave_log_small.jpg" style="width: 53px; height: 13px" />';
	educarelogo='<img src="/PruQuote/resources/images/edusmart_logo.png" style="height:30px;vertical-align:middle;" />';
	

	if lower(alang)=lower('khmer') then
		basicplanname=(select (l.reserve2||'') from ap.vitem2 l where lower(l.tablname)=lower('productcode') and lower(l.itemlatin)=lower(abasicplanprd));
		rtr1name=(select l.itemkhmer from ap.vitem2 l where lower(l.tablname)=lower('productcode') and lower(l.itemlatin)=lower('rtr1'));
		rsr1name=(select l.itemkhmer from ap.vitem2 l where lower(l.tablname)=lower('productcode') and lower(l.itemlatin)=lower('rsr1'));
		rtr1name_ben=' អត្ថប្រយោជន៍សរុបនឹងផ្តល់ជូនក្នុងករណីទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍ ';
		rsr1name_ben='អត្ថប្រយោជន៍នឹងផ្តល់ជូនរៀងរាល់ឆ្នាំរហូតដល់កាលបរិចេ្ឆទផុតកំណត់​នៃបណ្ណសន្យារ៉ាប់រង ករណីទទួលមរណភាពឬពិការភាព​ទាំងស្រុង និងជាអចិន្រ្តៃយ៍';
		-- prutect
		pad1name = (select l.itemkhmer from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)='pad1'); 
		pad1name_ben='អត្ថប្រយោជន៏សរុបនឹងត្រូវ​ទូទាត់​ជូន​ ក្នុងករណីអ្នក​ត្រូវបាន​ធានា​រ៉ាប់រង​នៃ​លក្ខខណ្ឌ​បន្ថែម​នេះទទួលមរណភាព ឬពិការភាពទាំងស្រុង និងជាអចិន្រ្តៃយ៍​ ដែល​បណ្តាល​មក​ពី​គ្រោះថ្នាក់ចៃដន្យ​ ក្នុង​រយៈពេល​កំណត់​នៃ​បណ្ណសន្យារ៉ាប់រង​ ឬ​រយៈពេល​ធានារ៉ាប់រង​អាយុ​ជីវិត​បន្ថែម។';
		-- end of prutect
	    
		update temp t1 set la1sex2=lower(t2.itemkhmer) from ap.vitem2 t2 where lower(t1.cola1sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set posex=lower(t2.itemkhmer) from ap.vitem2 t2 where lower(t1.posex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set la2sex2=lower(t2.itemkhmer) from ap.vitem2 t2  where lower(t1.cola2sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set "PaymentMode2"=lower(t2.itemkhmer) from ap.vitem2 t2 where lower(t1.copaymentmode::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymentmode';
		update temp t1 set paymenttype2=lower(t2.itemkhmer) from ap.vitem2 t2  where lower(t1.copaymenttype::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymenttype';
		
		tt1='បុព្វលាភធានារ៉ាប់រងសរុបមុនពេលបញ្ចុះតម្លៃ (US$)';
		tt2='ការបញ្ចុះតម្លៃសម្រាប់ទឹកប្រាក់ត្រូវបានធានារ៉ាប់រងសរុបខ្នាតធំ (%)';
		tt3='បុព្វលាភធានារ៉ាប់រងសរុបក្រោយពេលបញ្ចុះតម្លៃ (US$)';
		tt4='ពន្ធអាករ (US$)';
		tt5='បុព្វលាភធានារ៉ាប់រងជាដំណាក់កាលដែលត្រូវបង់​ រួមទាំងពន្ធ  (US$)';
	
	else	
		basicplanname=(select (l.reserve2||'') from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)=lower(abasicplanprd));
		rtr1name=(select l.reserve2 from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)='rtr1');
		rsr1name=(select l.reserve2 from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)='rsr1');
		rtr1name_ben='The benefit amount is payable once on death or TPD.';
		rsr1name_ben='The benefit amount is payable at every policy anniversary till maturity, starting from the anniversary immediately after the insured event occurs.';
		-- prutect
		pad1name = (select l.reserve2 from ap.vitem2 l where lower(l.tablname)='productcode' and lower(l.itemlatin)='pad1'); 
		pad1name_ben='The benefit amount is payable once, in case of death/TPD of Life Assured for this rider caused due to an accident during the Policy Term or Extended Life Coverage Term.';
		-- end of prutect
		
		update temp t1 set la1sex2=t2.reserve2 from ap.vitem2 t2  where lower(t1.cola1sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set posex=t2.reserve2 from ap.vitem2 t2  where lower(t1.posex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set la2sex2=t2.reserve2 from ap.vitem2 t2   where lower(t1.cola2sex::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='sex';
		update temp t1 set "PaymentMode2"=t2.reserve2 from ap.vitem2 t2  where lower(t1.copaymentmode::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymentmode';
		update temp t1 set paymenttype2=t2.reserve2 from ap.vitem2 t2 where lower(t1.copaymenttype::char(3))=lower(t2.itemid::char(3)) and lower(t2.tablname)='paymenttype';
	
		tt1='Total Premium Before Discount (US$)';
		tt2='Large Sum Assured Rebates (%)';
		tt3='Total Premium After Discount (US$)';
		tt4='Applicable Tax (US$)';
		tt5='Total Installment Premium Payable (US$)';


	end if;	

	update temp set la2name='',la2sex='',la2sex2='',la2age=''  
	where l2_rtr1_flag='no' and l2_rsr1_flag='no' and la2_pad1_flag='no'; -- add prutect condition

--Result R1
	if atype='R1' or atype='RN' then 
		open c_R1 FOR
		select * from temp;
		return next c_R1;
        end if;	

	create temporary table tmpto on commit drop as
	select aid as coparms
		,'01'::char(2) as orderno
		,l.product as product
		,cast(basicplanname as char(1000)) as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.basicplan_sa as sa
		,cast((l.basicplan_sa * l.mbrate) as decimal(18,2)) as mb
		,l.policyterm
		,l.premiumterm
 		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode::char(10) as discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product::char(10)||l.policyterm::char(10)||'c'||l.la1sex::char(10))::char(10) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(l.product||l.premiumterm||'c'||l.la1sex) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,l.l1_rtr2 as sa1
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower('rtr2'||l.premiumterm||'c'||l.la1sex) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate1	-- to calc the premium rate
		,educarelogo as logo
	from temp l;

	insert into tmpto
	select aid as coparms
		,'02'::char(2) as orderno
		,'rtr1'::char(4) as product
		,rtr1name as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.l1_rtr1 as sa
		,(l.l1_rtr1 * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la1sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rtr1'||l.premiumterm||'c'||l.la1sex)) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate		 
		,0.0
		,0.0
	from temp l where lower(l.l1_rtr1_flag)='yes';	

	insert into tmpto
	select aid as coparms
		,'03'::char(2) as orderno
		,'rsr1'::char(4) as product
		,rsr1name as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate  as effective
		,l.la1sex as sex
		,l.l1_rsr1 as sa
		,(l.l1_rsr1 * l.mbrate) as mb 
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la1sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rsr1'||l.premiumterm||'c'||l.la1sex)) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,0.0
		,0.0
	from temp l where lower(l.l1_rsr1_flag)='yes';

	-- prutect life 1
	insert into tmpto
	select aid as coparms
		,'04'::char(2) as orderno
		,'pad1'::char(4) as product
		,pad1name as productname
		,'la1'::char(3) as life
		,l.la1name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.la1_pad1_sa as sa
		,(l.la1_pad1_sa * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la1sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('pad1'||l.premiumterm||'c'||l.la1sex)) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate		 
		,0.0
		,0.0
	from temp l where lower(l.la1_pad1_flag)='yes';
	-- end of prutect life 1

	insert into tmpto
	select aid as coparms
		,'05'::char(2) as orderno
		,'rtr1'::char(4) as product
		,rtr1name as productname
		,'la2'::char(3) as life
		,l.la2name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.l2_rtr1 as sa
		,(l.l2_rtr1 * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la2sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rtr1'||l.premiumterm||'c'||l.la2sex)) and l.la2age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,0.0
		,0.0
	from temp l where lower(l.l2_rtr1_flag)='yes';

	insert into tmpto
	select aid as coparms
		,'06'::char(2)  as orderno
		,'rsr1'::char(4) as product
		,rsr1name as productname
		,'la2'::char(3) as life
		,l.la2name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.l2_rsr1 as sa
		,(l.l2_rsr1 * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la2sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('rsr1'||l.premiumterm||'c'||l.la2sex)) and l.la2age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate
		,0.0
		,0.0
	from temp l where lower(l.l2_rsr1_flag)='yes';

	-- prutect life 2
	insert into tmpto
	select aid as coparms
		,'07'::char(2) as orderno
		,'pad1'::char(4) as product
		,pad1name as productname
		,'la1'::char(3) as life
		,l.la2name as life_assured
		,l.la1age as age
		,cast(l.la1dateofbirth as date) as dob
		,l.commdate as effective
		,l.la1sex as sex
		,l.la2_pad1_sa as sa
		,(l.la2_pad1_sa * l.mbrate) as mb
		,l.policyterm
		,l.premiumterm
		,l.paymentmode
		,l.paymenttype
		,modalfactor
		,mbcode
		,mbrate -- to calc the maturity benefit
		,discountcode
		,discountrate -- to calc the large sa rebate is 5%
		,(l.product||l.policyterm||'c'||l.la1sex) as premiumcode
		,cast((select t1.rate from ap.vpremiumratelisting t1 where lower(t1.premiumrate)=lower(('pad1'||l.premiumterm||'c'||l.la1sex)) and l.la1age::integer between t1.fromage and t1.toage limit 1) as decimal(18,2)) as premiumrate	-- to calc the premium rate		 
		,0.0
		,0.0
	from temp l where lower(l.la2_pad1_flag)='yes';
	-- end of prutect life 2
--Result 2
	if atype='R2' or atype='RN' then
		open c_R2 FOR
		select l.coparms,l.orderno
		,(case when lower(l.productname)='edusave' then edusavelogo
					   else  productname end)::char(300) as productname
		,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa	
		,cast(round(l.sa *(premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2)) as premiumamt
		--,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as premiumamt
		-- monthly
		,(l.product||l.paymenttype||'12 | '||l.product||'c'||l.sex)::char(20) as p12_code
		,(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),null),0) * premiumrate) as p12_rate
		,(cast(round(l.sa *(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),null),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p12_amt
		-- semi-annually
		,(l.product||l.paymenttype||'02 | '||l.product||'c'||l.sex)::char(20) as p02_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(l.product||l.paymenttype||'02')),0) * premiumrate) as p02_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p02_amt
		-- annually
		,(l.product||l.paymenttype||'01 | '||l.product||'c'||l.sex) as p01_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate) as p01_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p01_amt
		-- annualize before discount (c,01)
		,(l.product||'c'||'01 | '||l.product||'c'||l.sex) as pa1_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate) as pa1_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as pa1_amt
		,logo
		from tmpto l
		where lower(l.product) like lower('B%')
		order by coparms,orderno;
		return next c_R2;
	end if;

	if prusaver::char(3) = 'yes'::char(3) then
		-- pru saver
		create temporary table tmpsaver1 on commit drop as
		select l.coparms,l.orderno
			,(case when lower(l.productname)='pru myfamily' and lower(alang)='khmer' then '<span class="Pru"><b>pru</b></span><i style="text-transform: lowercase; font-size:10px;color:black;">គ្រួសារខ្ញុំ</i>' 
						when lower(l.productname)='pru myfamily' and lower(alang)='latin' then '<span class="Pru"><b>pru</b></span><i class="product">my family</i>'
						when l.productname='edusave' then '<img src="/PruQuote/resources/images/edusave_log_small.jpg" style="width: 53px; height: 13px" />'
					else  productname end)::char(300) as productname
			,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa1
			,cast(round(l.sa1 *(premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2)) as premiumamt
			-- monthly
			,(l.product||l.paymenttype||'12 | '||l.product||'c'||l.sex)::char(20) as p12_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower('rtr2'||l.paymenttype||'12')),0) * premiumrate1) as p12_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower('rtr2'||l.paymenttype||'12')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p12_amt
			-- semi-annually
			,(l.product||l.paymenttype||'02 | '||l.product||'c'||l.sex)::char(20) as p02_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(l.product||l.paymenttype||'02')),0) * premiumrate1) as p02_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower('rtr2'||l.paymenttype||'02')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p02_amt
			-- annually
			,(l.product||l.paymenttype||'01 | '||l.product||'c'||l.sex)::char(20) as p01_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate1) as p01_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower('rtr2'||l.paymenttype||'01')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p01_amt
			-- annualize before discount (c,01)
			,(l.product||'c'||'01 | '||l.product||'c'||l.sex)::char(20) as pa1_code
			,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate1) as pa1_rate
			,(cast(round(l.sa1 *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower('rtr2'||'c'||'01')),0) * premiumrate1/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as pa1_amt

		from tmpto l
		where lower(l.product) like lower('b%')
		order by coparms,orderno;
	end if;

	large_sa_rebate=0.00;
	large_sa_rebate=(select discountrate from tmpto limit 1);

	create temporary table tmpto_tt on commit drop as
	select l.coparms,l.orderno,l.productname,product,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa	
		-- ap.vmodalfactor : to get the rate for premium modal loading factor (monthlt, semi-annully, yearly)
		-- vpremiumratelisting : to get the rate for calc 1 year premium reference to product
		,cast(l.sa *(premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)) as decimal(18,2)) as premiumamt
		--,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as premiumamt
		-- monthly
		,(l.product||l.paymenttype||'12 | '||l.product||'c'||l.sex) as p12_code
		,(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),null),0) * premiumrate) as p12_rate
		,round((l.sa *(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),null),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end))),2) as p12_amt
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as sad12
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'12')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end))* ((100-large_sa_rebate)/100) * 0.000 ) as ftax12
		-- semi-annually
		,(l.product||l.paymenttype||'02 | '||l.product||'c'||l.sex) as p02_code
		,(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(l.product||l.paymenttype||'02')),null),0) * premiumrate) as p02_rate
		,(cast(round(l.sa *(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),null),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p02_amt
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as sad02
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'02')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100) * 0.000 ) as ftax02

		-- annually
		,(l.product||l.paymenttype||'01 | '||l.product||'c'||l.sex) as p01_code
		,(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),null),0) * premiumrate) as p01_rate
		,(cast(round(l.sa::decimal(18,2) *(coalesce(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower((abasicplanprd::char(5)||l.paymenttype::char(5)||'01'::char(2)))),null),0) * premiumrate/(case when lower(l.product) like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as p01_amt
		-- ,(cast(l.sa as char),'-',cast(l.premiumrate as char),'-',l.product )as  p01_amt 
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as sad
		-- ,round((round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=(abasicplanprd,l.paymenttype,'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) * ((100-large_sa_rebate)/100) * 0.000 ),2) as ftax			
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||l.paymenttype||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100) * 0.000 ) as ftax			

		-- annualize before discount (c,01)
		,(l.product||'c'||'01 | '||l.product||'c'||l.sex) as pa1_code
		,(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate) as pa1_rate
		,(cast(round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) as decimal(18,2))) as pa1_amt
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100)) as aad
		,(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where lower(t1.modalfactor)=lower(abasicplanprd||'c'||'01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)) * ((100-large_sa_rebate)/100) * 0.000 ) as atax			
		-- ,round((round(l.sa *(nullif((select t1.rate from ap.vmodalfactor t1 where t1.modalfactor=(abasicplanprd,'c','01')),0) * premiumrate/(case when l.product like 'rsr%' then 100 else 1000 end)),2) * ((100-large_sa_rebate)/100) * 0.000 ),2) as atax
	from tmpto l
	-- where l.product not like 'b%'
	order by coparms,orderno;

	create temporary table tmpdata1 on commit drop as
	select l.coparms
		,'11'::char(2) as orderno
		,tt1 as productname
		,'TT'::char(2) as product
		,''::char(1) as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb 
		,0 as sa
		,0 as premiumamt
		,'t12'::char(3) as p12_code
		,0 as p12_rate
		,sum(p12_amt) as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02' as p02_code
		,0 as p02_rate
		,sum(p02_amt) as p02_amt
		,0 as sad02
		,0 as ftax02
		,'t01' as p01_code
		,0 as p01_rate
		,SUM(p01_amt) as p01_amt
		,0 as sad
		,0 as stax
		
		,'ta1' as pa1_code
		,0 as pa1_rate
		,sum(pa1_amt) as pa1_amt
		,0 as aad
		,0 as atax
	from tmpto_tt l 
	where lower(l.product) not like LOWER('TT%') 
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata1;
	
	create temporary table tmpdata2 on commit drop as
	select l.coparms
		,'12'::char(2) as orderno
		,tt2 as productname
		,'TT'::char(2) as product
		,'' as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa
		,0 as premiumamt
		,'t12'::char(3) as p12_code
		,0 as p12_rate
		,large_sa_rebate as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02'::char(3) as p02_code
		,0 as p02_rate
		,large_sa_rebate as p02_amt
		,0 as sad02
		,0 as ftax02
		,'t01'::char(3) as p01_code
		,0 as p01_rate
		,large_sa_rebate as p01_amt
		,0 as sad
		,0 as ftax
		,'ta1'::char(3) as pa1_code
		,0 as pa1_rate
		,large_sa_rebate as pa1_amt
		,0 as aad
		,0 as atax		
	from tmpto_tt l 
	where lower(l.product) not like LOWER('TT%') 
	group by coparms;

	insert into tmpto_tt
	select * from tmpdata2;

	create temporary table tmpdata3 on commit drop as
	select l.coparms
		,'13'::char(2) as orderno
		,tt3 as productname
		,'TT'::char(2) as product
		,''::char(1) as life_assured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa
		,0 as premiumamt
		,'t12'::char(3) as p12_code
		,0 as p12_rate
		-- ,(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)) as p12_amt
		,round(sum(round(sad12,2)),2) as p12_amt
		,0 as sad12
		,0 as ftax12
		,'t02'::char(3) as p02_code
		,0 as p02_rate
		-- ,(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)) as p02_amt
		,round(sum(round(sad02,2)),2) as p02_amt
		,0 as sad02
		,0 as ftax02
		,'t01'::char(3) as p01_code
		,0 as p01_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,round(sum(round(sad,2)),2) as p01_amt
		,0 as sad
		,0 as ftax
		,'ta1'::char(3) as pa1_code
		,0 as pa1_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,round(sum(round(aad,2)),2) as pa1_amt
		,0 as aad
		,0 as atax
	from tmpto_tt l 
	where l.product not like LOWER('TT%') 
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata3;

	if prusaver::char(3) = 'yes'::char(3) then
		-- prusaver header
		-- select * from tmpsaver1;
		create temporary table tmpdata6 on commit drop as
		select l.coparms
		,'14'::char(2) as orderno
		,'<tr><td class="border2" style="width:170px;text-align:left;"><u>additional rider:</u></td><th class="border2" style="width:"><u>life assured</u></th><th class="border2" style="width:40px"><u>policy term</u></th><th class="border2" style="width:75px"><u>premium payment term</u></th><th class="border2" style="width:75px"><u>maturity benefit (us$)</u></th><th class="border2" style="width:70px"><u>monthly (us$)</u></th><th class="border2" style="width:100px"><u>semi-annual (us$)</u></th><th class="border2" style="width:70px"><u>annual (us$)</u></th></tr>'::char(3000) as productname
		,'TH_RTR2'::char(10) as product
		,''::char(1) as lifeassured
		,0 as age
		,0 as policyterm
		,0 as premiumterm
		,0 as mb
		,0 as sa1
		,0 as premiumamt
		,'t12'::char(20) as p12_code
		,0 as p12_rate
		-- ,(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)) as p12_amt
		,0 as p12_amt
		,0 as sad12 -- for calculation tax
		,0 as ftax12
		,'t02'::char(20) as p02_code
		,0 as p02_rate
		-- ,(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)) as p02_amt
		,0 as p02_amt
		,0 as sad02 -- for calculation tax
		,0 as ftax02
		,'t01'::char(20) as p01_code
		,0 as p01_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,0 as p01_amt
		,0 as sad -- for calculation tax
		,0 as ftax

		,'ta1'::char(20) as pa1_code
		,0 as pa1_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,0 as pa1_amt
		,0 as aad
		,0 as atax
		from tmpsaver1 l; 
		
		insert into tmpto_tt
		select * from tmpdata6;

		create temporary table tmpdata7 on commit drop as
		select l.coparms
		,'15'::char(2) as orderno
		,case when lower(alang)='khmer' then 'អត្ថប្រយោជន៍​​​  Long-Term Savings Builder' 
			  else ' Long-Term Savings Builder' end as productname
		,'RTR2'::char(4) as product
		, life_assured
		, age
		,l.policyterm
		,l.premiumterm
		,l.mb
		,sa1
		,premiumamt
		,'t12'::char(3) as p12_code
		,p12_rate
		-- ,(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)) as p12_amt
		,p12_amt
		,p12_amt as sad12 -- for calculation tax
		,0 as ftax12
		,'t02'::char(3) as p02_code
		,p02_rate
		-- ,(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)) as p02_amt
		,p02_amt
		,p02_amt as sad02 -- for calculation tax
		,0 as ftax02
		,'t01'::char(3) as p01_code
		,p01_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,p01_amt
		,p01_amt as sad -- for calculation tax
		,0 as ftax

		,'ta1'::char(3) as pa1_code
		,0 as pa1_rate
		-- ,(round(sum(p01_amt)*(100-large_sa_rebate)/100,2)) as p01_amt
		,pa1_amt
		,0 as aad
		,0 as atax
		from tmpsaver1 l; 
		
		insert into tmpto_tt
		select * from tmpdata7;	
	end if;

	create temporary table tmpdata4 on commit drop as
	select l.coparms
	,'16'::char(2) as orderno
	,tt4 as productname
	,'TT'::char(2) as product
	,'' as life_assured
	,0 as age
	,0 as policyterm
	,0 as premiumterm
	,0 as mb
	,0 as sa
	,0 as premiumamt
	,'t12'::char(3) as  p12_code
	,0 as p12_rate
	-- ,(round(sum(round((p12_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p12_amt
	,round(sum(round(round(sad12,2)*(case when l.product like 'r%' and l.product<>'rtr2' then 0.05 else 0.000 end) ,2)),2) as p12_amt
	-- ,cast(sum(ftax12) as char) as p12_amt
	,0 as sad12
	,0 as ftax12
	,'t02'::char(3) as p02_code
	,0 as p02_rate
	-- ,(round(sum(round((p02_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p02_amt
	,round(sum(round(round(sad02,2)*(case when l.product like 'r%' and l.product<>'rtr2' then 0.05 else 0.000 end) ,2)),2) as p02_amt
	,0 as sad02
	,0 as ftax02
	,'t01'::char(3) as p01_code
	,0 as p01_rate
	-- ,(round(sum(round((p02_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p02_amt
	,round(sum(round(round(sad,2)*(case when l.product like 'r%' and l.product<>'rtr2' then 0.05 else 0.000 end) ,2)),2) as p01_amt
	,0 as sad
	,0 as ftax

	,'ta1'::char(3) as pa1_code
	,0 as pa1_rate
	-- ,(round(sum(round((p01_amt)*(100-large_sa_rebate)/100,2)*0.000 ),2)) as p01_amt
	,round(sum(round(atax,2)),2) as pa1_amt
	,0 as aad
	,0 as atax
	from tmpto_tt l 
	where l.product not like LOWER('TT%') 
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata4;

	create temporary table tmpdata5 on commit drop as
	select l.coparms
	,'17'::char(2) as orderno
	,tt5 as productname
	,'TT'::char(2) as product
	,'' as life_assured
	,0 as age
	,0 as policyterm
	,0 as premiumterm
	,0 as mb
	,0 as sa
	,0 as premiumamt
	,'t12'::char(3) as p12_code
	,0 as p12_rate
	-- ,(round((sum(p12_amt)*(100-large_sa_rebate)/100)*1.0,2) + round(round(sum(p12_amt)*(100-large_sa_rebate)/100,2)*0.000 ,2)) as p12_amt
	-- ,(round(sum(round(sad12,2)),2))+(round((sum(round(ftax12,2))),2)) as p12_amt
	,(round(sum(round(sad12,2)),2))+(round((sum(round(round(sad12,2)*(case when l.product like 'r%' and l.product<>'rtr2' then 0.05 else 0.000 end) ,2))),2)) as p12_amt
	,0 as sad12
	,0 as ftax12
	,'t02'::char(3) as p02_code
	,0 as p02_rate
	-- ,(round((sum(p02_amt)*(100-large_sa_rebate)/100)*1.0,2) + round(round(sum(p02_amt)*(100-large_sa_rebate)/100,2)*0.000 ,2)) as p02_amt
	-- ,(round(sum(round(sad02,2)),2))+(round((sum(round(ftax02,2))),2)) as p02_amt
	,(round(sum(round(sad02,2)),2))+(round((sum(round(round(sad02,2)*(case when l.product like 'r%' and l.product<>'rtr2' then 0.05 else 0.000 end) ,2))),2)) as p02_amt
	,0 as sad02
	,0 as ftax02

	,'t01'::char(3) as p01_code		
	,0 as p01_rate
	-- ,(round((sum(p01_amt)*(100-large_sa_rebate)/100)*1.0,2) + (sum(ftax)*(100-large_sa_rebate)/100)) as p01_amt
	-- ,(round(sum(round(sad,2)),2))+(round((sum(round(ftax,2))),2)) as p01_amt
	,(round(sum(round(sad,2)),2))+(round((sum(round(round(sad,2)*(case when l.product like 'r%' and l.product<>'rtr2'  then 0.05 else 0.000 end) ,2))),2)) as p01_amt
	-- ,concat(cast(sum(sad) as char),'-',cast(sum(ftax) as char)) as p01_amt
	,0 as sad
	,0 as ftax
	,'ta1'::char(3) as pa1_code		
	,0 as pa1_rate
	-- ,(round((sum(p01_amt)*(100-large_sa_rebate)/100)*1.0,2) + (sum(ftax)*(100-large_sa_rebate)/100)) as p01_amt
	,(round(sum(round(aad,2)),2))+(round((sum(round(atax,2))),2)) as pa1_amt
	-- ,concat(cast(sum(aad) as char),'-',cast(sum(atax) as char)) as a1
	,0 as aad
	,0 as atax

	from tmpto_tt l 
	where l.product not like LOWER('TT%')
	group by coparms;
	
	insert into tmpto_tt
	select * from tmpdata5;

	if (atype='R3' or atype='RN') then
		pdiscountrate = (select p01_amt from tmpto_tt where orderno = '12' limit 1)::numeric(18,2);
		RAISE NOTICE 'discount rate: %', pdiscountrate;
		-- Requested by Actuarial 20171018
		-- Remove related discount rows if discount rate is 0
		if pdiscountrate = 0.00 then
			update tmpto_tt
			set productname = CASE WHEN lower(alang) = 'khmer' THEN 'បុព្វលាភធានារ៉ាប់រងសរុប (US$)' ELSE 'Total Premium (US$)' END
			where orderno = '11';

			delete from tmpto_tt
			where orderno in('12','13');
		end if;
		
		OPEN c_r3 FOR
		select l.coparms,l.orderno,l.productname,product,l.life_assured,l.age,l.policyterm,l.premiumterm,l.mb,l.sa	
		,premiumamt
		, p12_code
		,p12_rate
		,p12_amt
		,p02_code
		,p02_rate
		,p02_amt
		-- annually
		,p01_code
		,p01_rate
		,p01_amt
		-- annually
		,pa1_code
		,pa1_rate
		,pa1_amt
		from tmpto_tt l order by coparms,orderno;
		RETURN NEXT c_r3;	
	end if;

	create temporary table tmpmbrate on commit drop as
	select t.tablid ,t.tablname,s.*
	from ap.vmbrate s,ap.vitem2 t 
	where lower(s.combrate::char(10))=lower(t.itemid::char(10));

	if(atype='R4' or atype='RN') then
		OPEN c_r4 FOR
		select t.coparms
		,t.policyterm::integer+l.year::integer as policyterm
		,t.age::integer+l.year::integer+(t.policyterm::integer-1) as age
		,round((case when l.year::integer=0 then t.mb else 0.0 end),2) as opt1_lumpsum
		,round((case when l.year::integer=0 then t.mb else 0.0 end),2) as opt1_tt
		,round(cast(t.sa*l.rate/100.0 as decimal(18,2)),2) as opt2_inst
		,cast( round(case when l.year::integer=0 then t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr4lum'||t.policyterm))/100.0
							 when l.year=1 then t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr4lum'||t.policyterm))/100.0
							 when l.year=2 then t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr4lum'||t.policyterm))/100.0
							 else t.sa*(select sum(r1.rate) from ap.vmbrate r1 where r1.year::integer<=l.year::integer and lower(r1.mbrate) like lower('btr4lum'||t.policyterm))/100.0 end 
							 ,2)
							 as decimal(18,2)) as opt1_inst_tt
		from tmpmbrate l,tmpto t
		where lower(l.mbrate) like lower('btr4lum'||t.policyterm::char(2)) and lower(t.product) like 'btr4'
		order by coparms,year;
		RETURN NEXT c_r4;
	end if; 

	create temporary table tmpsurrender on commit drop as
	select t.tablid ,t.tablname,s.*
	from ap.vsurrender s,ap.vitem2 t 
	where s.cosurrender::char(10)=t.itemid::char(10)
	and t.tablid::integer=21;

	create temporary table tmplf_ben on commit drop as
	select t.coparms
	,cast(yearinforce as integer) as policyyear
	,(cast(yearinforce::integer+t.age::integer as integer)-1) as age
-- 	,cast((case when lower(product)=lower('btl1')
-- 	then (case when yearinforce::integer<=5  
-- 		  then (cast(round(t.premiumamt,2) as decimal(18,4))) else 0 end) 
-- 	else (cast(round(t.premiumamt,2) as decimal(18,4))) 
-- 	end
-- 	) as char(50)) as p_ape
-- 	,(cast(round(round(t.premiumamt,2) * (  case when lower(product)=lower('btl1')
-- 									then (case when yearinforce::integer<=5 
-- 											      then yearinforce::integer else 5 
-- 										  end) 
-- 								 else yearinforce::integer				
-- 							end) 
-- 	,2) as decimal(18,2)
-- 	)) as p_total
	,cast((case when lower(product)=lower('btl1')
		then (case when yearinforce::integer<=5  
			  then (cast(round(t.p01_amt,2) as decimal(18,4))) else 0 end) 
		else (cast(round(t.p01_amt,2) as decimal(18,4))) 
		end
		) as char(50)) as p_ape
	,(cast(round(round(t.p01_amt,2) * (  case when lower(product)=lower('btl1')
						then (case when yearinforce::integer<=5 
							then yearinforce::integer else 5 end) 
							else yearinforce::integer end),2) as decimal(18,2)
		)) as p_total
	,(cast(round(t.sa,2) as decimal(18,2))) as clam_non_accident
	,(cast(round(t.sa*0.1,2) as decimal(18,2))) as aib_non_accident
	,(cast(round(t.sa*2,2) as decimal(18,2))) as clam_accident
	,(cast(round(t.sa*0.1,2) as decimal(18,2))) as aib_accident
	,(cast(l.rate as decimal(18,2))) as surrender_rate
	,(trunc(case when yearinforce::integer <> t.policyterm::integer then (t.premiumamt*( case when lower(product)='btl1' 
				   then (case when yearinforce::integer<=5 then yearinforce::integer else 5 end)
			   else yearinforce::integer
		  end) * l.rate/100.00) else t.sa*1 end, 2)) as surrender
	from tmpsurrender l,tmpto_tt t
	where l.surrender like (abasicplanprd::char(10)||t.policyterm::char(10)) and lower(t.product) like 'bt%' and l.yearinforce::integer<=t.policyterm::integer
	order by yearinforce;

	update tmplf_ben set p_ape='' where p_ape::decimal(18,2)=0.00;

	if (atype='R5' or atype='RN') then
		OPEN c_r5 for
		select * from tmplf_ben order by coparms,policyyear ;
		RETURN NEXT c_r5;
		-- print '3. benefits with additional riders, that help the family in the further securing child''s education and future '
		create temporary table tmprider_ben on commit drop as
		select l.coparms,l.orderno,productname,life_assured
		,(cast(round(l.sa,2) as decimal(18,2))) as clam_non_accident
		,(cast(round(l.sa*2,2) as decimal(18,2))) as clam_accident
		,cast(rtr1name_ben as char(500)) as payment_terms
		from tmpto_tt l
		where l.product like 'rtr1';
	end if ;

	insert into tmprider_ben
	select l.coparms,l.orderno,productname,life_assured
	,cast(round(l.sa,2) as decimal(18,2)) as clam_non_accident
	,cast(round(l.sa,2) as decimal(18,2)) as clam_accident
	,cast(rsr1name_ben as char(500)) as payment_terms
	from tmpto_tt l
	where lower(l.product) like 'rsr1';

	insert into tmprider_ben
	select l.coparms,l.orderno,productname,life_assured
	,cast(round(0,2) as decimal(18,2)) as clam_non_accident
	,cast(round(l.sa,2) as decimal(18,2)) as clam_accident
	,cast(pad1name_ben as char(500)) as payment_terms
	from tmpto_tt l
	where lower(l.product) like 'pad1';

	update tmprider_ben set productname = replace(productname,'(sum assured)','') where 1=1;

	open c_r6 for
	select * from tmprider_ben order by coparms,orderno;
	return next c_r6;

	-- Extended life coverage calculation
	create temporary table tmplf_ben_last on commit drop as
		select * from tmplf_ben order by coparms desc, policyyear desc limit 1;
	
	create temporary table tmplf_ext on commit drop as
		select coparms,1 as policyyear,(age+1) as age,'0.00' as p_ape,p_total,clam_non_accident,0::numeric(18,2) as aib_non_accident
		,clam_accident,0::numeric(18,2) as aib_accident,0::numeric(18,2) as surrender_rate,0::numeric(18,2) as surrender
		from tmplf_ben_last
		union
		select coparms,2 as policyyear,(age+2) as age,'0.00' as p_ape,p_total,clam_non_accident,0::numeric(18,2) as aib_non_accident
		,clam_accident,0::numeric(18,2) as aib_accident,0::numeric(18,2) as surrender_rate,0::numeric(18,2) as surrender
		from tmplf_ben_last
		union
		select coparms,3 as policyyear,(age+3) as age,'0.00' as p_ape,p_total,clam_non_accident,0::numeric(18,2) as aib_non_accident
		,clam_accident,0::numeric(18,2) as aib_accident,0::numeric(18,2) as surrender_rate,0::numeric(18,2) as surrender
		from tmplf_ben_last
		union
		select coparms,4 as policyyear,(age+4) as age,'0.00' as p_ape,p_total,clam_non_accident,0::numeric(18,2) as aib_non_accident
		,clam_accident,0::numeric(18,2) as aib_accident,0::numeric(18,2) as surrender_rate,0::numeric(18,2) as surrender
		from tmplf_ben_last
		union
		select coparms,5 as policyyear,(age+5) as age,'0.00' as p_ape,p_total,clam_non_accident,0::numeric(18,2) as aib_non_accident
		,clam_accident,0::numeric(18,2) as aib_accident,0::numeric(18,2) as surrender_rate,0::numeric(18,2) as surrender
		from tmplf_ben_last;

	open c_r7 for
	    select * from tmplf_ext order by coparms,policyyear ;
	return next c_r7;

	-- end of Extended life coverage calculation

	if lower(prusaver::char(3)) = lower('yes') then
		-- print '1. guaranteed maturity benefit with pru saver'
		if (atype='R7' or atype='RN') then
			open c_r7 for
			select t.coparms
				,t.policyterm::integer+l.year::integer as policyterm
				,t.age::integer+l.year::integer+t.policyterm::integer-1 as age
				,round((case when l.year=0 then t.sa1 else 0.0 end),2) as opt1_lumpsum
				,round((case when l.year=0 then t.sa1 else 0.0 end),2) as opt1_tt
				,round(cast(t.sa1*l.rate/100.0 as decimal(18,2)),2) as opt2_inst
				,cast( round(case when l.year=0 then t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0
									 when l.year=1 then t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0
									 when l.year=2 then t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0
									 else t.sa1*(select sum(r1.rate) from ap.vmbrate r1 where r1.year<=l.year and lower(r1.mbrate) like lower('rtr2set'||t.policyterm))/100.0 end 
									 ,2)
									 as decimal(18,2)) as opt1_inst_tt
			from tmpmbrate l,tmpto t
			where lower(l.mbrate) like ('rtr2set'||t.policyterm) and lower(t.product) like 'btr%'
			order by coparms,year;
			return next c_r7;
		end if;

		mbsa=(select sum(sa1) as mbsa from tmpto);
		-- select policyterm;
		-- select * from tmpto_tt;
		open c_r8 for
		select t.coparms
			,cast(yearinforce as integer) as policyyear
			,(cast(yearinforce::integer+t.age::integer as integer)-1) as age
			,cast((case when lower(product)='btl1' 
					   then (case when yearinforce::integer<=5  
								  then (cast(round(t.premiumamt,2) as decimal(18,4))) else 0 end) 
				   else (cast(round(t.premiumamt,2) as decimal(18,4))) 
			  end
			 ) as char(50)) as p_ape 
			,(cast(round(round(t.premiumamt,2) * (  case	when lower(product)='btl1' then (case when yearinforce::integer<=5 
														then yearinforce::integer else 5 
													end) 
									else yearinforce::integer				
								end) 
						,2) as decimal(18,2)
				)) as p_total
			,(t.premiumamt*1.1*yearinforce::decimal(18,2) ) as clam_non_accident
			,(t.premiumamt*1*1.1*yearinforce::decimal(18,2)) as clam_accident
			,(cast(l.rate as decimal(18,2))) as surrender_rate
			,( case when yearinforce::integer=t.policyterm::integer then mbsa
				else
					(trunc(t.premiumamt*( case when lower(product)='btl1' 
											   then (case when yearinforce::integer<=5 then yearinforce::integer else 5 end)
										   else yearinforce::integer
									  end) * l.rate/100.00 ,2)
					)
				end
			) as surrender
		from tmpsurrender l,tmpto_tt t
		where lower(l.surrender) like lower('rtr2'||t.policyterm::char(3)) and lower(t.product) like 'rtr2%' --and l.yearinforce::integer<=t.policyterm::integer
		order by yearinforce;
		return next c_r8;
	--else
	--	open c_r7 for
	--	select 'No value return' as result;
	--	return next c_r7;
		
	--	open c_r8 for
	--	select 'No value return' as result;
	--	return next c_r8;
			
	end if;

	--CALL IN JAVA PROJECT OR PGSQL
	--***********************************************************************************************
	--	select ap.spgen_edusmart(6,'Khmer','RN','R1','R2','R3','R4','R5','R6','R7','R8');	*
	--	FETCH ALL IN "R1";									*
	--	FETCH ALL IN "R2";									*
	--	FETCH ALL IN "R3";									*
	--	FETCH ALL IN "R4";									*
	--	FETCH ALL IN "R5";									*
	--	FETCH ALL IN "R6";									*
	--	FETCH ALL IN "R7";									*
	--	FETCH ALL IN "R8";									*
	--***********************************************************************************************
	
	--open c_sql2 FOR
	--SELECT * from tmpto;
        --return next c_sql2;
 
	--OPEN c_sql1 FOR
	--    SELECT *
	--    FROM vpruquote_parms;
	--RETURN NEXT c_r3;

	--OPEN c_sql2 FOR
	--    SELECT *
	--    FROM vpruquote_parms;
	--RETURN NEXT c_sql1;
    	
	--RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION ap.spgen_edusmart(bigint, character, character, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor, refcursor)
  OWNER TO postgres;
