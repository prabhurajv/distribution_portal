﻿WITH pruquote as(
	select  l.id
	,l.commdate
	,l.cola1relationship
	,l.relationship
	,upper(l.poname) as poname
	,la1sex
	,cast((case when cola1relationship=13 then CAST(la1sex2 as char(20)) else CAST('NA' as char(20)) end) as char(20)) as posex
	,cast((case when cola1relationship=13 then CAST(i.itemkhmer as char(20)) else CAST('NA' as char(20)) end) as char(20)) as posexkh
	,cast((case when cola1relationship=13 then CAST(la1age as char(20)) else CAST('NA' as char(20)) end) as char(20)) as poage
	,upper(l.la1name) as la1name
	,l.la1dateofbirth
	,l.la1age
	,l.la1sex2
	,i.itemkhmer as la1sex2kh
	,upper(l.la2name) as la2name
	, la2dateofbirth
	, la2age
	, la2sex2
	,i2.itemkhmer as la2sex2kh
	, la2sex
	,l.paymenttype2
	,l.policyterm
	,right(('000'||cast(l.premiumterm as char(3))),2) as premiumterm
	,l.basicplan_sa
	,l.copolicyterm
	,l.copremiumterm
	,l.copaymenttype
	,l.paymenttype
	, pt.itemkhmer as paymenttype2kh
	,l.copaymentmode
	,l.paymentmode
	,l.paymentmode2
	,(case when l.basicplan_sa >= 20000 and l.basicplan_sa < 50000 then 'T1'  when l.basicplan_sa >= 50000  then 'T2' end) as tablerate
	,l.col1_rtr1
	,l.l1_rtr1_flag
	,l.l1_rtr1
	,l.col1_rsr1
	,l.l1_rsr1_flag
	,l.l1_rsr1
	,l.col2_rtr1
	,l.l2_rtr1_flag
	,l.l2_rtr1
	,l.col2_rsr1
	,l.l2_rsr1_flag
	,l.l2_rsr1
	,l.validflag
	,l.syndate
	,l.col1_rtr2
	,l.l1_rtr2_flag
	,l.l1_rtr2
	,l.pruretirement
	,l.couser
	from ap.vpruquote_parms l
	join ap.tabitemitem i on l.cola1sex = i.id
	left join ap.tabitemitem i2 on l.cola2sex = i2.id
	join ap.tabitemitem pt on l.copaymenttype = pt.id
	where l.id = :quoteId),
 la1occ as (select occ_id from ap.quote_occupation occ
		join ap.occupation occp on occ.occ_id = occp.id
		join pruquote q on q.id = occ.quote_id
		where occ.remark in ('occpo','occla1') and occ_id <> 0 and occp.file_id <> 0 order by occ.remark desc limit 1),
 basicpremuimrate as (select pr.rate as basicpremuimrate from ap.vpremiumratelisting pr ,pruquote
					where pr.premiumrate = concat('BTR6',policyterm,'C',la1sex)
					 and pr.fromage = la1age and pr.toage = la1age limit 1),
 basicrate as(select 
		     (basicpremuimrate * basicplan_sa / 1000 * (select mf.rate from ap.vmodalfactor mf
												where mf.modalfactor = concat('BTR6', paymenttype, '12')
												limit 1)) as basicmontlyrate,
		     (basicpremuimrate * basicplan_sa / 1000 * (select mf.rate from ap.vmodalfactor mf 
												where mf.modalfactor = concat('BTR6', paymenttype, '01')
												limit 1)) as basicyearlyrate,
		     basicpremuimrate * basicplan_sa / 1000 as benfirstyear
		from basicpremuimrate,pruquote),

total as (select (basicmontlyrate) as totalmontly,
			(basicyearlyrate) as totalyearly,
			benfirstyear,
		COALESCE((select rate from ap.vdiscountrate where discountrate = 'BTR6' and fromsa <= basicplan_sa and tosa >= basicplan_sa),0) as discountrate
		from pruquote, basicrate),
otrtotal as (select round((totalmontly - (totalmontly * discountrate /100)),2) as totalmontlypremuimdiscount,
		     round((totalyearly - (totalyearly * discountrate /100)),2) as totalyearlypremuimdiscount,
		     round((benfirstyear - (benfirstyear * discountrate /100)),2) as totalbenfirstyear,
		     (basicplan_sa * 2) as basic_tpd_acc,
		     basicplan_sa as basic_tpd,
		     (l1_rsr1) as la1_rsr1_tpd_acc,
		     l1_rsr1 as la1_rsr1_tpd,
		     (l2_rsr1) as la2_rsr1_tpd_acc,
		     l2_rsr1 as la2_rsr1_tpd,
		     (l2_rtr1 * 2) as la2_rtr1_tpd_acc,
		     (l2_rtr1) as la2_rtr1_tpd
	    from total, pruquote)
--FINALRESULT--
select 
l.commdate --1
,l.relationship --2
,poname --3
,posex --4
,poage --5
,la1name -- 6
,to_char(la1dateofbirth, 'DMMYYYY') as la1dateofbirth --format version in printing
, CAST(la1age as char(20)) as la1age-- 12
,l.la1sex2 -- 9
,l.paymenttype2 --14
,l.policyterm -- 15
,l.premiumterm -- 16
,l.basicplan_sa -- 17
,l.paymentmode2 -- 18
,basicpremuimrate -- 19
,round(basicmontlyrate,2) as basicmontlyrate -- 20
,round(basicyearlyrate,2) as basicyearlyrate -- 21
,round(totalmontly,2) as totalmontly  -- 31
,round(totalyearly,2) as totalyearly  -- 32
,discountrate  -- 33
,totalmontlypremuimdiscount  -- 34
,totalyearlypremuimdiscount  -- 35
,basic_tpd_acc  -- 36
,basic_tpd  -- 37
,couser -- 44
,paymentmode
,0 as tax_rate
,0 as tax_rate_monthly
,0 as tax_rate_yearly
,paymenttype
,totalbenfirstyear
,posexkh
,la1sex2kh
,paymenttype2kh
,la2sex2kh
,(COALESCE((select occ_id from la1occ), 0)) as la1occ
from pruquote l,basicpremuimrate,basicrate,total, otrtotal