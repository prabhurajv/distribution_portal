﻿WITH pruquote as(
	select  l.id
	,l.commdate
	,l.cola1relationship
	,l.relationship
	,upper(l.poname) as poname
	,la1sex
	,cast((case when cola1relationship=13 then CAST(la1sex2 as char(20)) else CAST('NA' as char(20)) end) as char(20)) as posex
	,cast((case when cola1relationship=13 then CAST(i.itemkhmer as char(20)) else CAST('NA' as char(20)) end) as char(20)) as posexkh
	,cast((case when cola1relationship=13 then CAST(la1age as char(20)) else CAST('NA' as char(20)) end) as char(20)) as poage
	,upper(l.la1name) as la1name
	,l.la1dateofbirth
	,l.la1age
	,l.la1sex2
	,i.itemkhmer as la1sex2kh
	,upper(l.la2name) as la2name
	, la2dateofbirth
	, la2age
	, la2sex2
	,i2.itemkhmer as la2sex2kh
	, la2sex
	,l.paymenttype2
	,l.policyterm
	,right(('000'||cast(l.premiumterm as char(3))),2) as premiumterm
	,l.basicplan_sa
	,l.copolicyterm
	,l.copremiumterm
	,l.copaymenttype
	,l.paymenttype
	, pt.itemkhmer as paymenttype2kh
	,l.copaymentmode
	,l.paymentmode
	,l.paymentmode2
	,(case when l.basicplan_sa >= 20000 and l.basicplan_sa < 50000 then 'T1'  when l.basicplan_sa >= 50000  then 'T2' end) as tablerate
	,l.col1_rtr1
	,l.l1_rtr1_flag
	,l.l1_rtr1
	,l.col1_rsr1
	,l.l1_rsr1_flag
	,l.l1_rsr1
	,l.col2_rtr1
	,l.l2_rtr1_flag
	,l.l2_rtr1
	,l.col2_rsr1
	,l.l2_rsr1_flag
	,l.l2_rsr1
	,l.validflag
	,l.syndate
	,l.col1_rtr2
	,l.l1_rtr2_flag
	,l.l1_rtr2
	,l.pruretirement
	,l.couser
	from ap.vpruquote_parms l
	join ap.tabitemitem i on l.cola1sex = i.id
	left join ap.tabitemitem i2 on l.cola2sex = i2.id
	join ap.tabitemitem pt on l.copaymenttype = pt.id
	where l.id =:quoteId),
 la1occ as (select occ_id from ap.quote_occupation occ
		join ap.occupation occp on occ.occ_id = occp.id
		join pruquote q on q.id = occ.quote_id
		where occ.remark in ('occpo','occla1') and occ_id <> 0 and occp.file_id <> 0 order by occ.remark desc limit 1),
 la2occ as (select occ_id from ap.quote_occupation occ 
		join ap.occupation occp on occ.occ_id = occp.id
		join pruquote q on q.id = occ.quote_id
		where occ.remark in ('occla2') and occ_id <> 0 and occp.file_id <> 0 limit 1),
 basicpremuimrate as (select pr.rate as basicpremuimrate from ap.vpremiumratelisting pr ,pruquote
					where pr.premiumrate = concat('BTR5',policyterm,'C',la1sex,tablerate)
					 and pr.fromage = la1age and pr.toage = la1age limit 1),
 basicrate as(select 
		     basicpremuimrate * basicplan_sa / 1000 * (select mf.rate from ap.vmodalfactor mf
												where mf.modalfactor = concat('BTR5', paymenttype, '12')
												limit 1) as basicmontlyrate,
		     basicpremuimrate * basicplan_sa / 1000 * (select mf.rate from ap.vmodalfactor mf 
												where mf.modalfactor = concat('BTR5', paymenttype, '01')
												limit 1)as basicyearlyrate
		from basicpremuimrate,pruquote),
la1rsr1premuimrate as (select pr.rate as la1rsr1premuimrate from ap.vpremiumratelisting pr, pruquote 
					where  pr.premiumrate = concat('RSR1',policyterm,'C',la1sex) 
					and l1_rsr1_flag = 'yes'
					and pr.fromage = la1age and pr.toage = la1age limit 1),
la1rsr1rate as (select (la1rsr1premuimrate * l1_rsr1 / 100 * (select mf.rate from ap.vmodalfactor mf 
												where mf.modalfactor = concat('RSR1', paymenttype, '12')
												limit 1)
			) as la1rsr1montlypremuim,
		   (la1rsr1premuimrate * l1_rsr1 / 100 * (select mf.rate from ap.vmodalfactor mf 
												where mf.modalfactor = concat('RSR1', paymenttype, '01')
												limit 1)
			) as la1rsr1yearlypremuim
		from pruquote,la1rsr1premuimrate where l1_rsr1_flag = 'yes'),
la2rsr1premuimrate as (select pr.rate as la2rsr1premuimrate from ap.vpremiumratelisting pr ,pruquote
					where  pr.premiumrate = concat('RSR1',policyterm,'C',la2sex) 
					and l2_rsr1_flag = 'yes' and pr.fromage = la2age and pr.toage = la2age limit 1),
la2rsr1rate as (select (la2rsr1premuimrate * l2_rsr1 / 100 * (select mf.rate from ap.vmodalfactor mf 
												where mf.modalfactor = concat('RSR1', paymenttype, '12')
												limit 1)) as la2rsr1montlypremuim,
		  (la2rsr1premuimrate * l2_rsr1 / 100 * (select mf.rate from ap.vmodalfactor mf 
												where mf.modalfactor = concat('RSR1', paymenttype, '01')
												limit 1)) as la2rsr1yearlypremuim
		   from pruquote,la2rsr1premuimrate where l2_rsr1_flag = 'yes'),
la2rtr1premuimrate as (select pr.rate as la2rtr1premuimrate from ap.vpremiumratelisting pr ,pruquote
					where  pr.premiumrate = concat('RTR1',policyterm,'C',la2sex) 
					and l2_rtr1_flag = 'yes'
					and pr.fromage = la2age and pr.toage = la2age limit 1),
la2rtr1rate as (select  (la2rtr1premuimrate * l2_rtr1 / 1000 * (select mf.rate from ap.vmodalfactor mf 
												where mf.modalfactor = concat('RTR1', paymenttype, '12')
												limit 1)) as la2rtr1montlypremuim,
			(la2rtr1premuimrate * l2_rtr1 / 1000 * (select mf.rate from ap.vmodalfactor mf 
												where mf.modalfactor = concat('RTR1', paymenttype, '01')
												limit 1)) as la2rtr1yearlypremuim
from la2rtr1premuimrate,pruquote where l2_rtr1_flag = 'yes'),
componentTotal as 
		(select basicmontlyrate as basicmontlyrate,
		 COALESCE((select la1rsr1montlypremuim from la1rsr1rate),0) as la1rsr1montlypremuim,
		 COALESCE((select la2rsr1montlypremuim from la2rsr1rate),0) as la2rsr1montlypremuim,
		 COALESCE((select la2rtr1montlypremuim from la2rtr1rate),0) as la2rtr1montlypremuim,
		 basicyearlyrate as basicyearlyrate,
		 COALESCE((select la1rsr1yearlypremuim from la1rsr1rate),0) as la1rsr1yearlypremuim,
		 COALESCE((select la2rsr1yearlypremuim from la2rsr1rate),0) as la2rsr1yearlypremuim,
		 COALESCE((select la2rtr1yearlypremuim from la2rtr1rate),0) as la2rtr1yearlypremuim,
		 COALESCE((select rate from ap.vdiscountrate where discountrate = 'BTR5' and fromsa <= basicplan_sa and tosa >= basicplan_sa),0) as discountrate
		from pruquote, basicrate),
total as (select (round(basicmontlyrate,2) + round(la1rsr1montlypremuim,2) + round(la2rsr1montlypremuim,2) + round(la2rtr1montlypremuim,2)) as totalmontly,
				 (round(basicyearlyrate,2) + round(la1rsr1yearlypremuim,2) + round(la2rsr1yearlypremuim,2) + round(la2rtr1yearlypremuim,2)) as totalyearly,
				 round((basicmontlyrate - (basicmontlyrate * discountrate/100)),2) as basicmontlyafterdiscount,
				 round((la1rsr1montlypremuim - (la1rsr1montlypremuim * discountrate/100)),2) as la1rsr1montlyafterdiscount,
				 round((la2rsr1montlypremuim - (la2rsr1montlypremuim * discountrate/100)),2) as la2rsr1montlyafterdiscount,
				 round((la2rtr1montlypremuim - (la2rtr1montlypremuim * discountrate/100)),2) as la2rtr1montlyafterdiscount,
				 round((basicyearlyrate - (basicyearlyrate * discountrate/100)),2) as basicyearlyafterdiscount,
				 round((la1rsr1yearlypremuim - (la1rsr1yearlypremuim * discountrate/100)),2) as la1rsr1yearlyafterdiscount,
				 round((la2rsr1yearlypremuim - (la2rsr1yearlypremuim * discountrate/100)),2) as la2rsr1yearlyafterdiscount,
				 round((la2rtr1yearlypremuim - (la2rtr1yearlypremuim * discountrate/100)),2) as la2rtr1yearlyafterdiscount,
				 discountrate
		from componentTotal),
otrtotal as (select (basicmontlyafterdiscount + la1rsr1montlyafterdiscount + la2rsr1montlyafterdiscount + la2rtr1montlyafterdiscount) as totalmontlypremuimdiscount,
 			 (basicyearlyafterdiscount + la1rsr1yearlyafterdiscount + la2rsr1yearlyafterdiscount + la2rtr1yearlyafterdiscount) as totalyearlypremuimdiscount,
		     (basicplan_sa * 2) as basic_tpd_acc,
		     basicplan_sa as basic_tpd,
		     (l1_rsr1) as la1_rsr1_tpd_acc,
		     l1_rsr1 as la1_rsr1_tpd,
		     (l2_rsr1) as la2_rsr1_tpd_acc,
		     l2_rsr1 as la2_rsr1_tpd,
		     (l2_rtr1 * 2) as la2_rtr1_tpd_acc,
		     (l2_rtr1) as la2_rtr1_tpd
	    from total, pruquote)
--FINALRESULT--
select 
l.id
,l.commdate --1
,l.relationship --2
,poname --3
,posex --4
,poage --5
,la1name -- 6
,to_char(la1dateofbirth, 'DMMYYYY') as la1dateofbirth --format version in printing
, CAST(la1age as char(20)) as la1age-- 12
,l.la1sex2 -- 9
,l.la2name -- 10
, la2dateofbirth -- 11
, CAST(la2age as char(20)) as la2age-- 12
, la2sex2 -- 13
,l.paymenttype2 --14
,l.policyterm -- 15
,l.premiumterm -- 16
,l.basicplan_sa -- 17
,l.paymentmode2 -- 18
,basicpremuimrate -- 19
,round(basicmontlyrate,2) as basicmontlyrate -- 20
,round(basicyearlyrate,2) as basicyearlyrate-- 21
,(COALESCE((select la1rsr1premuimrate from la1rsr1premuimrate), NULL)) as la1rsr1premuimrate -- 22
,(COALESCE((select la2rtr1premuimrate from la2rtr1premuimrate), NULL)) as la2rtr1premuimrate  -- 23
,(COALESCE((select la2rsr1premuimrate from la2rsr1premuimrate), NULL)) as la2rsr1premuimrate -- 24
,(COALESCE((select round(la1rsr1montlypremuim,2) from la1rsr1rate), NULL)) as la1rsr1montlypremuim -- 25
,(COALESCE((select round(la2rtr1montlypremuim,2) from la2rtr1rate), NULL)) as la2rtr1montlypremuim -- 26 
,(COALESCE((select round(la2rsr1montlypremuim,2) from la2rsr1rate), NULL)) as la2rsr1montlypremuim -- 27
,(COALESCE((select round(la1rsr1yearlypremuim,2) from la1rsr1rate), NULL)) as la1rsr1yearlypremuim -- 28
,(COALESCE((select round(la2rtr1yearlypremuim,2) from la2rtr1rate), NULL)) as la2rtr1yearlypremuim  -- 29
,(COALESCE((select round(la2rsr1yearlypremuim,2) from la2rsr1rate), NULL)) as la2rsr1yearlypremuim  -- 30
,round(totalmontly,2) as totalmontly  -- 31
,round(totalyearly,2) as totalyearly  -- 32
,discountrate  -- 33
,round(totalmontlypremuimdiscount,2) as totalmontlypremuimdiscount -- 34
,round(totalyearlypremuimdiscount,2) as totalyearlypremuimdiscount-- 35
,basic_tpd_acc  -- 36
,basic_tpd  -- 37
,la1_rsr1_tpd_acc  -- 38
,la1_rsr1_tpd  -- 39
,la2_rsr1_tpd_acc  -- 40
,la2_rsr1_tpd  -- 41
,la2_rtr1_tpd_acc -- 42
,la2_rtr1_tpd -- 43
,couser -- 44
,CASE WHEN l2_rtr1 = 0.00 THEN 16 else (COALESCE((col2_rtr1), 16)) end as col2_rtr1
,CASE WHEN l2_rsr1 = 0.00 THEN 16 else (COALESCE((col2_rsr1), 16)) end as col2_rsr1
,CASE WHEN l1_rsr1 = 0.00 THEN 16 else (COALESCE((col1_rsr1), 16)) end as col1_rsr1
,l1_rsr1
,l2_rsr1
,l2_rtr1
,paymentmode
,paymenttype
,0 as tax_rate
,0 as tax_rate_monthly
,0 as tax_rate_yearly
,posexkh
,la1sex2kh
,paymenttype2kh
,la2sex2kh
,(COALESCE((select occ_id from la1occ), 0)) as la1occ
,(COALESCE((select occ_id from la2occ), 0)) as la2occ
from pruquote l,basicpremuimrate,basicrate,total, otrtotal